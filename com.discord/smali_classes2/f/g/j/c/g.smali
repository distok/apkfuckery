.class public Lf/g/j/c/g;
.super Ljava/lang/Object;
.source "BufferedDiskCache.java"


# instance fields
.field public final a:Lf/g/b/b/h;

.field public final b:Lf/g/d/g/g;

.field public final c:Lf/g/d/g/j;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:Lf/g/j/c/x;

.field public final g:Lf/g/j/c/r;


# direct methods
.method public constructor <init>(Lf/g/b/b/h;Lf/g/d/g/g;Lf/g/d/g/j;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lf/g/j/c/r;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/c/g;->a:Lf/g/b/b/h;

    iput-object p2, p0, Lf/g/j/c/g;->b:Lf/g/d/g/g;

    iput-object p3, p0, Lf/g/j/c/g;->c:Lf/g/d/g/j;

    iput-object p4, p0, Lf/g/j/c/g;->d:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lf/g/j/c/g;->e:Ljava/util/concurrent/Executor;

    iput-object p6, p0, Lf/g/j/c/g;->g:Lf/g/j/c/r;

    new-instance p1, Lf/g/j/c/x;

    invoke-direct {p1}, Lf/g/j/c/x;-><init>()V

    iput-object p1, p0, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    return-void
.end method

.method public static a(Lf/g/j/c/g;Lcom/facebook/cache/common/CacheKey;)Lcom/facebook/common/memory/PooledByteBuffer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    sget v0, Lf/g/d/e/a;->a:I

    iget-object v0, p0, Lf/g/j/c/g;->a:Lf/g/b/b/h;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    check-cast v0, Lf/g/b/b/d;

    :try_start_1
    invoke-virtual {v0, p1}, Lf/g/b/b/d;->b(Lcom/facebook/cache/common/CacheKey;)Lf/g/a/a;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    iget-object v0, p0, Lf/g/j/c/g;->g:Lf/g/j/c/r;

    check-cast v0, Lf/g/j/c/w;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    iget-object v1, p0, Lf/g/j/c/g;->g:Lf/g/j/c/r;

    check-cast v1, Lf/g/j/c/w;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, v0, Lf/g/a/a;->a:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v2, p0, Lf/g/j/c/g;->b:Lf/g/d/g/g;

    invoke-virtual {v0}, Lf/g/a/a;->b()J

    move-result-wide v3

    long-to-int v0, v3

    invoke-interface {v2, v1, v0}, Lf/g/d/g/g;->d(Ljava/io/InputStream;I)Lcom/facebook/common/memory/PooledByteBuffer;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    move-object p0, v0

    :goto_0
    return-object p0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const-class v1, Lf/g/j/c/g;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    const-string p1, "Exception reading from cache for %s"

    invoke-static {v1, v0, p1, v2}, Lf/g/d/e/a;->n(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p0, p0, Lf/g/j/c/g;->g:Lf/g/j/c/r;

    check-cast p0, Lf/g/j/c/w;

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public static b(Lf/g/j/c/g;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V
    .locals 2

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    sget v0, Lf/g/d/e/a;->a:I

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/g;->a:Lf/g/b/b/h;

    new-instance v1, Lf/g/j/c/i;

    invoke-direct {v1, p0, p2}, Lf/g/j/c/i;-><init>(Lf/g/j/c/g;Lf/g/j/j/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    check-cast v0, Lf/g/b/b/d;

    :try_start_1
    invoke-virtual {v0, p1, v1}, Lf/g/b/b/d;->d(Lcom/facebook/cache/common/CacheKey;Lf/g/b/a/g;)Lf/g/a/a;

    iget-object p0, p0, Lf/g/j/c/g;->g:Lf/g/j/c/r;

    check-cast p0, Lf/g/j/c/w;

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-class p2, Lf/g/j/c/g;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Failed to write to disk-cache for key %s"

    invoke-static {p2, p0, p1, v0}, Lf/g/d/e/a;->n(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public c(Lcom/facebook/cache/common/CacheKey;)V
    .locals 6

    iget-object v0, p0, Lf/g/j/c/g;->a:Lf/g/b/b/h;

    check-cast v0, Lf/g/b/b/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v1, v0, Lf/g/b/b/d;->o:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {p1}, Ls/a/b/b/a;->B(Lcom/facebook/cache/common/CacheKey;)Ljava/util/List;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, 0x0

    :goto_0
    move-object v4, v2

    check-cast v4, Ljava/util/ArrayList;

    :try_start_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, v0, Lf/g/b/b/d;->i:Lf/g/b/b/c;

    invoke-interface {v5, v4, p1}, Lf/g/b/b/c;->c(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v2, v0, Lf/g/b/b/d;->f:Ljava/util/Set;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    invoke-static {}, Lf/g/b/b/i;->a()Lf/g/b/b/i;

    move-result-object v1

    iput-object p1, v1, Lf/g/b/b/i;->a:Lcom/facebook/cache/common/CacheKey;

    iget-object p1, v0, Lf/g/b/b/d;->e:Lf/g/b/a/b;

    check-cast p1, Lf/g/b/a/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lf/g/b/b/i;->b()V

    :goto_1
    return-void
.end method

.method public final d(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)Lu/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/e;",
            ")",
            "Lu/g<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    sget p1, Lf/g/d/e/a;->a:I

    iget-object p1, p0, Lf/g/j/c/g;->g:Lf/g/j/c/r;

    check-cast p1, Lf/g/j/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lu/g;->h:Ljava/util/concurrent/Executor;

    instance-of p1, p2, Ljava/lang/Boolean;

    if-eqz p1, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lu/g;->j:Lu/g;

    goto :goto_0

    :cond_0
    sget-object p1, Lu/g;->k:Lu/g;

    goto :goto_0

    :cond_1
    new-instance p1, Lu/g;

    invoke-direct {p1}, Lu/g;-><init>()V

    invoke-virtual {p1, p2}, Lu/g;->h(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot set the result of a completed task."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public e(Lcom/facebook/cache/common/CacheKey;Ljava/util/concurrent/atomic/AtomicBoolean;)Lu/g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/cache/common/CacheKey;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "Lu/g<",
            "Lf/g/j/j/e;",
            ">;"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    invoke-virtual {v0, p1}, Lf/g/j/c/x;->a(Lcom/facebook/cache/common/CacheKey;)Lf/g/j/j/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0}, Lf/g/j/c/g;->d(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)Lu/g;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p1

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    new-instance v1, Lf/g/j/c/f;

    invoke-direct {v1, p0, v0, p2, p1}, Lf/g/j/c/f;-><init>(Lf/g/j/c/g;Ljava/lang/Object;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/facebook/cache/common/CacheKey;)V

    iget-object p2, p0, Lf/g/j/c/g;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, p2}, Lu/g;->a(Ljava/util/concurrent/Callable;Ljava/util/concurrent/Executor;)Lu/g;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p2

    :try_start_2
    const-class v0, Lf/g/j/c/g;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast p1, Lf/g/b/a/f;

    iget-object p1, p1, Lf/g/b/a/f;->a:Ljava/lang/String;

    aput-object p1, v1, v2

    const-string p1, "Failed to schedule disk-cache read for %s"

    invoke-static {v0, p2, p1, v1}, Lf/g/d/e/a;->n(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p2}, Lu/g;->c(Ljava/lang/Exception;)Lu/g;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public f(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V
    .locals 7

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lf/g/j/j/e;->m(Lf/g/j/j/e;)Z

    move-result v0

    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    iget-object v0, p0, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    invoke-virtual {v0, p1, p2}, Lf/g/j/c/x;->b(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    invoke-static {p2}, Lf/g/j/j/e;->a(Lf/g/j/j/e;)Lf/g/j/j/e;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    :try_start_1
    iget-object v2, p0, Lf/g/j/c/g;->e:Ljava/util/concurrent/Executor;

    new-instance v3, Lf/g/j/c/g$a;

    invoke-direct {v3, p0, v1, p1, v0}, Lf/g/j/c/g$a;-><init>(Lf/g/j/c/g;Ljava/lang/Object;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-class v2, Lf/g/j/c/g;

    const-string v3, "Failed to schedule disk-cache write for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/facebook/cache/common/CacheKey;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v1, v3, v4}, Lf/g/d/e/a;->n(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    invoke-virtual {v1, p1, p2}, Lf/g/j/c/x;->d(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)Z

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/g/j/j/e;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
