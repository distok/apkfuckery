.class public Lf/g/j/c/h;
.super Ljava/lang/Object;
.source "BufferedDiskCache.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/facebook/cache/common/CacheKey;

.field public final synthetic e:Lf/g/j/c/g;


# direct methods
.method public constructor <init>(Lf/g/j/c/g;Ljava/lang/Object;Lcom/facebook/cache/common/CacheKey;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/c/h;->e:Lf/g/j/c/g;

    iput-object p3, p0, Lf/g/j/c/h;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/h;->e:Lf/g/j/c/g;

    iget-object v0, v0, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    iget-object v1, p0, Lf/g/j/c/h;->d:Lcom/facebook/cache/common/CacheKey;

    invoke-virtual {v0, v1}, Lf/g/j/c/x;->c(Lcom/facebook/cache/common/CacheKey;)Z

    iget-object v0, p0, Lf/g/j/c/h;->e:Lf/g/j/c/g;

    iget-object v0, v0, Lf/g/j/c/g;->a:Lf/g/b/b/h;

    iget-object v1, p0, Lf/g/j/c/h;->d:Lcom/facebook/cache/common/CacheKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    check-cast v0, Lf/g/b/b/d;

    :try_start_1
    invoke-virtual {v0, v1}, Lf/g/b/b/d;->f(Lcom/facebook/cache/common/CacheKey;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    throw v0
.end method
