.class public Lf/g/j/c/s;
.super Ljava/lang/Object;
.source "InstrumentedMemoryCache.java"

# interfaces
.implements Lf/g/j/c/t;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf/g/j/c/t<",
        "TK;TV;>;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field public final b:Lf/g/j/c/u;


# direct methods
.method public constructor <init>(Lf/g/j/c/t;Lf/g/j/c/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/c/t<",
            "TK;TV;>;",
            "Lf/g/j/c/u;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/c/s;->a:Lf/g/j/c/t;

    iput-object p2, p0, Lf/g/j/c/s;->b:Lf/g/j/c/u;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/c/s;->b:Lf/g/j/c/u;

    invoke-interface {v0, p1}, Lf/g/j/c/u;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lf/g/j/c/s;->a:Lf/g/j/c/t;

    invoke-interface {v0, p1, p2}, Lf/g/j/c/t;->a(Ljava/lang/Object;Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    return-object p1
.end method

.method public b(Lf/g/d/d/j;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/d/j<",
            "TK;>;)I"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/c/s;->a:Lf/g/j/c/t;

    invoke-interface {v0, p1}, Lf/g/j/c/t;->b(Lf/g/d/d/j;)I

    move-result p1

    return p1
.end method

.method public get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lcom/facebook/common/references/CloseableReference<",
            "TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/c/s;->a:Lf/g/j/c/t;

    invoke-interface {v0, p1}, Lf/g/j/c/t;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lf/g/j/c/s;->b:Lf/g/j/c/u;

    invoke-interface {v1, p1}, Lf/g/j/c/u;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/g/j/c/s;->b:Lf/g/j/c/u;

    invoke-interface {v1, p1}, Lf/g/j/c/u;->a(Ljava/lang/Object;)V

    :goto_0
    return-object v0
.end method
