.class public final Lf/g/j/c/b;
.super Ljava/lang/Object;
.source "BitmapMemoryCacheFactory.java"

# interfaces
.implements Lf/g/j/c/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/j/c/u<",
        "Lcom/facebook/cache/common/CacheKey;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/c/r;


# direct methods
.method public constructor <init>(Lf/g/j/c/r;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/c/b;->a:Lf/g/j/c/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/facebook/cache/common/CacheKey;

    iget-object p1, p0, Lf/g/j/c/b;->a:Lf/g/j/c/r;

    check-cast p1, Lf/g/j/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/facebook/cache/common/CacheKey;

    iget-object p1, p0, Lf/g/j/c/b;->a:Lf/g/j/c/r;

    check-cast p1, Lf/g/j/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public c(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/facebook/cache/common/CacheKey;

    iget-object p1, p0, Lf/g/j/c/b;->a:Lf/g/j/c/r;

    check-cast p1, Lf/g/j/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
