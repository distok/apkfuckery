.class public Lf/g/j/c/g$a;
.super Ljava/lang/Object;
.source "BufferedDiskCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/c/g;->f(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/facebook/cache/common/CacheKey;

.field public final synthetic e:Lf/g/j/j/e;

.field public final synthetic f:Lf/g/j/c/g;


# direct methods
.method public constructor <init>(Lf/g/j/c/g;Ljava/lang/Object;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/c/g$a;->f:Lf/g/j/c/g;

    iput-object p3, p0, Lf/g/j/c/g$a;->d:Lcom/facebook/cache/common/CacheKey;

    iput-object p4, p0, Lf/g/j/c/g$a;->e:Lf/g/j/j/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lf/g/j/c/g$a;->f:Lf/g/j/c/g;

    iget-object v1, p0, Lf/g/j/c/g$a;->d:Lcom/facebook/cache/common/CacheKey;

    iget-object v2, p0, Lf/g/j/c/g$a;->e:Lf/g/j/j/e;

    invoke-static {v0, v1, v2}, Lf/g/j/c/g;->b(Lf/g/j/c/g;Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lf/g/j/c/g$a;->f:Lf/g/j/c/g;

    iget-object v0, v0, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    iget-object v1, p0, Lf/g/j/c/g$a;->d:Lcom/facebook/cache/common/CacheKey;

    iget-object v2, p0, Lf/g/j/c/g$a;->e:Lf/g/j/j/e;

    invoke-virtual {v0, v1, v2}, Lf/g/j/c/x;->d(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)Z

    iget-object v0, p0, Lf/g/j/c/g$a;->e:Lf/g/j/j/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/g/j/j/e;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lf/g/j/c/g$a;->f:Lf/g/j/c/g;

    iget-object v1, v1, Lf/g/j/c/g;->f:Lf/g/j/c/x;

    iget-object v2, p0, Lf/g/j/c/g$a;->d:Lcom/facebook/cache/common/CacheKey;

    iget-object v3, p0, Lf/g/j/c/g$a;->e:Lf/g/j/j/e;

    invoke-virtual {v1, v2, v3}, Lf/g/j/c/x;->d(Lcom/facebook/cache/common/CacheKey;Lf/g/j/j/e;)Z

    iget-object v1, p0, Lf/g/j/c/g$a;->e:Lf/g/j/j/e;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lf/g/j/j/e;->close()V

    :cond_1
    throw v0
.end method
