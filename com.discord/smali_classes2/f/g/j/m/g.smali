.class public Lf/g/j/m/g;
.super Ljava/lang/Object;
.source "BucketMap.java"


# annotations
.annotation build Lcom/facebook/infer/annotation/ThreadSafe;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/m/g$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lf/g/j/m/g$b<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public b:Lf/g/j/m/g$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/m/g$b<",
            "TT;>;"
        }
    .end annotation
.end field

.field public c:Lf/g/j/m/g$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/m/g$b<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lf/g/j/m/g;->a:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public final a(Lf/g/j/m/g$b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/m/g$b<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/m/g;->b:Lf/g/j/m/g$b;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lf/g/j/m/g;->b(Lf/g/j/m/g$b;)V

    iget-object v0, p0, Lf/g/j/m/g;->b:Lf/g/j/m/g$b;

    if-nez v0, :cond_1

    iput-object p1, p0, Lf/g/j/m/g;->b:Lf/g/j/m/g$b;

    iput-object p1, p0, Lf/g/j/m/g;->c:Lf/g/j/m/g$b;

    return-void

    :cond_1
    iput-object v0, p1, Lf/g/j/m/g$b;->d:Lf/g/j/m/g$b;

    iput-object p1, v0, Lf/g/j/m/g$b;->a:Lf/g/j/m/g$b;

    iput-object p1, p0, Lf/g/j/m/g;->b:Lf/g/j/m/g$b;

    return-void
.end method

.method public final declared-synchronized b(Lf/g/j/m/g$b;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/m/g$b<",
            "TT;>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lf/g/j/m/g$b;->a:Lf/g/j/m/g$b;

    iget-object v1, p1, Lf/g/j/m/g$b;->d:Lf/g/j/m/g$b;

    if-eqz v0, :cond_0

    iput-object v1, v0, Lf/g/j/m/g$b;->d:Lf/g/j/m/g$b;

    :cond_0
    if-eqz v1, :cond_1

    iput-object v0, v1, Lf/g/j/m/g$b;->a:Lf/g/j/m/g$b;

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p1, Lf/g/j/m/g$b;->a:Lf/g/j/m/g$b;

    iput-object v2, p1, Lf/g/j/m/g$b;->d:Lf/g/j/m/g$b;

    iget-object v2, p0, Lf/g/j/m/g;->b:Lf/g/j/m/g$b;

    if-ne p1, v2, :cond_2

    iput-object v1, p0, Lf/g/j/m/g;->b:Lf/g/j/m/g$b;

    :cond_2
    iget-object v1, p0, Lf/g/j/m/g;->c:Lf/g/j/m/g$b;

    if-ne p1, v1, :cond_3

    iput-object v0, p0, Lf/g/j/m/g;->c:Lf/g/j/m/g$b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
