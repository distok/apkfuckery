.class public Lf/g/j/m/b$a;
.super Ljava/lang/Object;
.source "BitmapCounter.java"

# interfaces
.implements Lf/g/d/h/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/g/j/m/b;-><init>(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/d/h/f<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/m/b;


# direct methods
.method public constructor <init>(Lf/g/j/m/b;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/m/b$a;->a:Lf/g/j/m/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public release(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Landroid/graphics/Bitmap;

    :try_start_0
    iget-object v0, p0, Lf/g/j/m/b$a;->a:Lf/g/j/m/b;

    invoke-virtual {v0, p1}, Lf/g/j/m/b;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    throw v0
.end method
