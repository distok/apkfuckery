.class public interface abstract Lf/g/j/m/r;
.super Ljava/lang/Object;
.source "MemoryChunk.java"


# virtual methods
.method public abstract a(ILf/g/j/m/r;II)V
.end method

.method public abstract b(I[BII)I
.end method

.method public abstract close()V
.end method

.method public abstract getByteBuffer()Ljava/nio/ByteBuffer;
.end method

.method public abstract getSize()I
.end method

.method public abstract getUniqueId()J
.end method

.method public abstract isClosed()Z
.end method

.method public abstract j(I)B
.end method

.method public abstract k(I[BII)I
.end method

.method public abstract p()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation
.end method
