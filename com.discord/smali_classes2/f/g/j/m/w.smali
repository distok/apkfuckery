.class public Lf/g/j/m/w;
.super Ljava/lang/Object;
.source "PoolConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/m/w$b;
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/m/y;

.field public final b:Lf/g/j/m/z;

.field public final c:Lf/g/j/m/y;

.field public final d:Lf/g/d/g/c;

.field public final e:Lf/g/j/m/y;

.field public final f:Lf/g/j/m/z;

.field public final g:Lf/g/j/m/y;

.field public final h:Lf/g/j/m/z;

.field public final i:Ljava/lang/String;

.field public final j:I


# direct methods
.method public constructor <init>(Lf/g/j/m/w$b;Lf/g/j/m/w$a;)V
    .locals 12

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {}, Lf/g/j/m/j;->a()Lf/g/j/m/y;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/m/w;->a:Lf/g/j/m/y;

    invoke-static {}, Lf/g/j/m/v;->h()Lf/g/j/m/v;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/m/w;->b:Lf/g/j/m/z;

    new-instance p1, Lf/g/j/m/y;

    sget p2, Lf/g/j/m/k;->a:I

    const/high16 v7, 0x400000

    mul-int v2, p2, v7

    new-instance v3, Landroid/util/SparseIntArray;

    invoke-direct {v3}, Landroid/util/SparseIntArray;-><init>()V

    const/high16 v8, 0x20000

    const/high16 v0, 0x20000

    :goto_0
    if-gt v0, v7, :cond_0

    invoke-virtual {v3, v0, p2}, Landroid/util/SparseIntArray;->put(II)V

    mul-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    const/high16 v4, 0x20000

    const/high16 v5, 0x400000

    sget v6, Lf/g/j/m/k;->a:I

    const/high16 v1, 0x400000

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lf/g/j/m/y;-><init>(IILandroid/util/SparseIntArray;III)V

    iput-object p1, p0, Lf/g/j/m/w;->c:Lf/g/j/m/y;

    invoke-static {}, Lf/g/d/g/d;->b()Lf/g/d/g/d;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    new-instance p1, Landroid/util/SparseIntArray;

    invoke-direct {p1}, Landroid/util/SparseIntArray;-><init>()V

    const/16 p2, 0x400

    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    const/16 p2, 0x800

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    const/16 p2, 0x1000

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    const/16 p2, 0x2000

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    const/16 p2, 0x4000

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    const v1, 0x8000

    invoke-virtual {p1, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    const/high16 v1, 0x10000

    invoke-virtual {p1, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    invoke-virtual {p1, v8, v0}, Landroid/util/SparseIntArray;->put(II)V

    const/high16 v1, 0x40000

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    const/high16 v1, 0x80000

    invoke-virtual {p1, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    const/high16 v1, 0x100000

    invoke-virtual {p1, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    new-instance v3, Lf/g/j/m/y;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    const-wide/32 v8, 0x7fffffff

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v5, v4

    const/high16 v4, 0x1000000

    if-ge v5, v4, :cond_1

    const/high16 v5, 0x300000

    goto :goto_1

    :cond_1
    const/high16 v6, 0x2000000

    if-ge v5, v6, :cond_2

    const/high16 v5, 0x600000

    goto :goto_1

    :cond_2
    const/high16 v5, 0xc00000

    :goto_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v10

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    long-to-int v6, v8

    if-ge v6, v4, :cond_3

    div-int/2addr v6, v2

    goto :goto_2

    :cond_3
    div-int/lit8 v6, v6, 0x4

    mul-int/lit8 v6, v6, 0x3

    :goto_2
    invoke-direct {v3, v5, v6, p1}, Lf/g/j/m/y;-><init>(IILandroid/util/SparseIntArray;)V

    iput-object v3, p0, Lf/g/j/m/w;->e:Lf/g/j/m/y;

    invoke-static {}, Lf/g/j/m/v;->h()Lf/g/j/m/v;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/m/w;->f:Lf/g/j/m/z;

    new-instance p1, Landroid/util/SparseIntArray;

    invoke-direct {p1}, Landroid/util/SparseIntArray;-><init>()V

    invoke-virtual {p1, p2, v0}, Landroid/util/SparseIntArray;->put(II)V

    new-instance p2, Lf/g/j/m/y;

    const v0, 0x14000

    invoke-direct {p2, v0, v1, p1}, Lf/g/j/m/y;-><init>(IILandroid/util/SparseIntArray;)V

    iput-object p2, p0, Lf/g/j/m/w;->g:Lf/g/j/m/y;

    invoke-static {}, Lf/g/j/m/v;->h()Lf/g/j/m/v;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/m/w;->h:Lf/g/j/m/z;

    const-string p1, "legacy"

    iput-object p1, p0, Lf/g/j/m/w;->i:Ljava/lang/String;

    iput v7, p0, Lf/g/j/m/w;->j:I

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method
