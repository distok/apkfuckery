.class public abstract Lf/g/j/m/s;
.super Lcom/facebook/imagepipeline/memory/BasePool;
.source "MemoryChunkPool.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/imagepipeline/memory/BasePool<",
        "Lf/g/j/m/r;",
        ">;"
    }
.end annotation


# instance fields
.field public final k:[I


# direct methods
.method public constructor <init>(Lf/g/d/g/c;Lf/g/j/m/y;Lf/g/j/m/z;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/imagepipeline/memory/BasePool;-><init>(Lf/g/d/g/c;Lf/g/j/m/y;Lf/g/j/m/z;)V

    iget-object p1, p2, Lf/g/j/m/y;->c:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result p2

    new-array p2, p2, [I

    iput-object p2, p0, Lf/g/j/m/s;->k:[I

    const/4 p2, 0x0

    :goto_0
    iget-object p3, p0, Lf/g/j/m/s;->k:[I

    array-length v0, p3

    if-ge p2, v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    aput v0, p3, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/imagepipeline/memory/BasePool;->i()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lf/g/j/m/s;->o(I)Lf/g/j/m/r;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lf/g/j/m/r;

    invoke-interface {p1}, Lf/g/j/m/r;->close()V

    return-void
.end method

.method public e(I)I
    .locals 4

    if-lez p1, :cond_2

    iget-object v0, p0, Lf/g/j/m/s;->k:[I

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget v3, v0, v2

    if-lt v3, p1, :cond_0

    return v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return p1

    :cond_2
    new-instance v0, Lcom/facebook/imagepipeline/memory/BasePool$InvalidSizeException;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/facebook/imagepipeline/memory/BasePool$InvalidSizeException;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public f(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lf/g/j/m/r;

    invoke-interface {p1}, Lf/g/j/m/r;->getSize()I

    move-result p1

    return p1
.end method

.method public g(I)I
    .locals 0

    return p1
.end method

.method public k(Ljava/lang/Object;)Z
    .locals 0

    check-cast p1, Lf/g/j/m/r;

    invoke-interface {p1}, Lf/g/j/m/r;->isClosed()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public abstract o(I)Lf/g/j/m/r;
.end method
