.class public Lf/g/j/m/p;
.super Ljava/lang/Object;
.source "LruBitmapPool.java"

# interfaces
.implements Lf/g/j/m/d;


# instance fields
.field public final a:Lf/g/j/m/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/m/q<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field public c:I

.field public final d:Lf/g/j/m/z;

.field public e:I


# direct methods
.method public constructor <init>(IILf/g/j/m/z;Lf/g/d/g/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p4, Lf/g/j/m/e;

    invoke-direct {p4}, Lf/g/j/m/e;-><init>()V

    iput-object p4, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    iput p1, p0, Lf/g/j/m/p;->b:I

    iput p2, p0, Lf/g/j/m/p;->c:I

    iput-object p3, p0, Lf/g/j/m/p;->d:Lf/g/j/m/z;

    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lf/g/j/m/p;->e:I

    iget v1, p0, Lf/g/j/m/p;->b:I

    if-le v0, v1, :cond_2

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    iget v0, p0, Lf/g/j/m/p;->e:I

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    invoke-virtual {v0}, Lf/g/j/m/q;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    invoke-virtual {v2, v0}, Lf/g/j/m/q;->b(Ljava/lang/Object;)I

    move-result v0

    iget v2, p0, Lf/g/j/m/p;->e:I

    sub-int/2addr v2, v0

    iput v2, p0, Lf/g/j/m/p;->e:I

    iget-object v2, p0, Lf/g/j/m/p;->d:Lf/g/j/m/z;

    invoke-interface {v2, v0}, Lf/g/j/m/z;->e(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    :goto_1
    :try_start_2
    monitor-exit p0

    goto :goto_2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_2
    :goto_2
    iget-object v0, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    invoke-virtual {v0, p1}, Lf/g/j/m/q;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object p1, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    invoke-virtual {p1, v0}, Lf/g/j/m/q;->b(Ljava/lang/Object;)I

    move-result p1

    iget v1, p0, Lf/g/j/m/p;->e:I

    sub-int/2addr v1, p1

    iput v1, p0, Lf/g/j/m/p;->e:I

    iget-object v1, p0, Lf/g/j/m/p;->d:Lf/g/j/m/z;

    invoke-interface {v1, p1}, Lf/g/j/m/z;->b(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    goto :goto_3

    :cond_3
    :try_start_3
    iget-object v0, p0, Lf/g/j/m/p;->d:Lf/g/j/m/z;

    invoke-interface {v0, p1}, Lf/g/j/m/z;->a(I)V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit p0

    :goto_3
    return-object v0

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public release(Ljava/lang/Object;)V
    .locals 10

    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    invoke-virtual {v0, p1}, Lf/g/j/m/q;->b(Ljava/lang/Object;)I

    move-result v0

    iget v1, p0, Lf/g/j/m/p;->c:I

    if-gt v0, v1, :cond_2

    iget-object v1, p0, Lf/g/j/m/p;->d:Lf/g/j/m/z;

    invoke-interface {v1, v0}, Lf/g/j/m/z;->g(I)V

    iget-object v1, p0, Lf/g/j/m/p;->a:Lf/g/j/m/q;

    check-cast v1, Lf/g/j/m/e;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, p1}, Lf/g/j/m/e;->d(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_1

    monitor-enter v1

    :try_start_0
    iget-object v2, v1, Lf/g/j/m/q;->a:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_1

    iget-object v2, v1, Lf/g/j/m/q;->b:Lf/g/j/m/g;

    invoke-virtual {v1, p1}, Lf/g/j/m/e;->b(Ljava/lang/Object;)I

    move-result v1

    monitor-enter v2

    :try_start_1
    iget-object v3, v2, Lf/g/j/m/g;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/g/j/m/g$b;

    if-nez v3, :cond_0

    new-instance v9, Lf/g/j/m/g$b;

    const/4 v4, 0x0

    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v9

    move v5, v1

    invoke-direct/range {v3 .. v8}, Lf/g/j/m/g$b;-><init>(Lf/g/j/m/g$b;ILjava/util/LinkedList;Lf/g/j/m/g$b;Lf/g/j/m/g$a;)V

    iget-object v3, v2, Lf/g/j/m/g;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, v9}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v3, v9

    :cond_0
    iget-object v1, v3, Lf/g/j/m/g$b;->c:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Lf/g/j/m/g;->a(Lf/g/j/m/g$b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1

    :catchall_1
    move-exception p1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw p1

    :cond_1
    :goto_0
    monitor-enter p0

    :try_start_3
    iget p1, p0, Lf/g/j/m/p;->e:I

    add-int/2addr p1, v0

    iput p1, p0, Lf/g/j/m/p;->e:I

    monitor-exit p0

    goto :goto_1

    :catchall_2
    move-exception p1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw p1

    :cond_2
    :goto_1
    return-void
.end method
