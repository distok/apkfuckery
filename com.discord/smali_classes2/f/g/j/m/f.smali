.class public Lf/g/j/m/f;
.super Ljava/lang/Object;
.source "Bucket.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/Queue;

.field public final d:Z

.field public e:I


# direct methods
.method public constructor <init>(IIIZ)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-lez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ls/a/b/b/a;->j(Z)V

    if-ltz p2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ls/a/b/b/a;->j(Z)V

    if-ltz p3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    iput p1, p0, Lf/g/j/m/f;->a:I

    iput p2, p0, Lf/g/j/m/f;->b:I

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lf/g/j/m/f;->c:Ljava/util/Queue;

    iput p3, p0, Lf/g/j/m/f;->e:I

    iput-boolean p4, p0, Lf/g/j/m/f;->d:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/m/f;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()V
    .locals 2

    iget v0, p0, Lf/g/j/m/f;->e:I

    const/4 v1, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    iget v0, p0, Lf/g/j/m/f;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Lf/g/j/m/f;->e:I

    return-void
.end method

.method public c()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/m/f;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lf/g/j/m/f;->d:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget v0, p0, Lf/g/j/m/f;->e:I

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-static {v2}, Ls/a/b/b/a;->j(Z)V

    iget v0, p0, Lf/g/j/m/f;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, Lf/g/j/m/f;->e:I

    invoke-virtual {p0, p1}, Lf/g/j/m/f;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lf/g/j/m/f;->e:I

    if-lez v0, :cond_2

    sub-int/2addr v0, v1

    iput v0, p0, Lf/g/j/m/f;->e:I

    invoke-virtual {p0, p1}, Lf/g/j/m/f;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-array v0, v1, [Ljava/lang/Object;

    aput-object p1, v0, v2

    sget p1, Lf/g/d/e/a;->a:I

    const-string p1, "Tried to release value %s from an empty bucket!"

    invoke-static {p1, v0}, Lf/g/d/e/a;->g(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x6

    const-string v1, "unknown:BUCKET"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method
