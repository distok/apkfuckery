.class public Lf/g/j/m/b;
.super Ljava/lang/Object;
.source "BitmapCounter.java"


# instance fields
.field public a:I

.field public b:J

.field public final c:I

.field public final d:I

.field public final e:Lf/g/d/h/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/d/h/f<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-lez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ls/a/b/b/a;->g(Z)V

    if-lez p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    iput p1, p0, Lf/g/j/m/b;->c:I

    iput p2, p0, Lf/g/j/m/b;->d:I

    new-instance p1, Lf/g/j/m/b$a;

    invoke-direct {p1, p0}, Lf/g/j/m/b$a;-><init>(Lf/g/j/m/b;)V

    iput-object p1, p0, Lf/g/j/m/b;->e:Lf/g/d/h/f;

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Landroid/graphics/Bitmap;)V
    .locals 9

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lf/g/k/a;->d(Landroid/graphics/Bitmap;)I

    move-result p1

    iget v0, p0, Lf/g/j/m/b;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "No bitmaps registered."

    invoke-static {v0, v3}, Ls/a/b/b/a;->h(ZLjava/lang/Object;)V

    int-to-long v3, p1

    iget-wide v5, p0, Lf/g/j/m/b;->b:J

    cmp-long v0, v3, v5

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const-string v5, "Bitmap size bigger than the total registered size: %d, %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v1

    iget-wide v7, p0, Lf/g/j/m/b;->b:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v6, v2

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lf/g/j/m/b;->b:J

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lf/g/j/m/b;->b:J

    iget p1, p0, Lf/g/j/m/b;->a:I

    sub-int/2addr p1, v2

    iput p1, p0, Lf/g/j/m/b;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-static {v5, v6}, Ls/a/b/b/a;->t(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lf/g/j/m/b;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
