.class public Lf/g/j/m/x;
.super Ljava/lang/Object;
.source "PoolFactory.java"


# instance fields
.field public final a:Lf/g/j/m/w;

.field public b:Lf/g/j/m/s;

.field public c:Lf/g/j/m/d;

.field public d:Lf/g/j/m/s;

.field public e:Lf/g/j/m/s;

.field public f:Lf/g/d/g/g;

.field public g:Lf/g/d/g/j;

.field public h:Lf/g/d/g/a;


# direct methods
.method public constructor <init>(Lf/g/j/m/w;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    return-void
.end method


# virtual methods
.method public a()Lf/g/j/m/d;
    .locals 7

    iget-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    if-nez v0, :cond_5

    iget-object v0, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v0, v0, Lf/g/j/m/w;->i:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v2, "dummy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "dummy_with_tracking"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "experimental"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "legacy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_4
    const-string v2, "legacy_default_params"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    :cond_0
    :goto_0
    if-eqz v1, :cond_4

    if-eq v1, v4, :cond_3

    if-eq v1, v5, :cond_2

    if-eq v1, v6, :cond_1

    new-instance v0, Lf/g/j/m/h;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v2, v1, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    iget-object v4, v1, Lf/g/j/m/w;->a:Lf/g/j/m/y;

    iget-object v1, v1, Lf/g/j/m/w;->b:Lf/g/j/m/z;

    invoke-direct {v0, v2, v4, v1, v3}, Lf/g/j/m/h;-><init>(Lf/g/d/g/c;Lf/g/j/m/y;Lf/g/j/m/z;Z)V

    iput-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    goto :goto_1

    :cond_1
    new-instance v0, Lf/g/j/m/h;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v1, v1, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    invoke-static {}, Lf/g/j/m/j;->a()Lf/g/j/m/y;

    move-result-object v2

    iget-object v4, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v4, v4, Lf/g/j/m/w;->b:Lf/g/j/m/z;

    invoke-direct {v0, v1, v2, v4, v3}, Lf/g/j/m/h;-><init>(Lf/g/d/g/c;Lf/g/j/m/y;Lf/g/j/m/z;Z)V

    iput-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    goto :goto_1

    :cond_2
    new-instance v0, Lf/g/j/m/p;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget v1, v1, Lf/g/j/m/w;->j:I

    invoke-static {}, Lf/g/j/m/v;->h()Lf/g/j/m/v;

    move-result-object v2

    iget-object v4, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {v0, v3, v1, v2, v4}, Lf/g/j/m/p;-><init>(IILf/g/j/m/z;Lf/g/d/g/c;)V

    iput-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    goto :goto_1

    :cond_3
    new-instance v0, Lf/g/j/m/m;

    invoke-direct {v0}, Lf/g/j/m/m;-><init>()V

    iput-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    goto :goto_1

    :cond_4
    new-instance v0, Lf/g/j/m/l;

    invoke-direct {v0}, Lf/g/j/m/l;-><init>()V

    iput-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    :cond_5
    :goto_1
    iget-object v0, p0, Lf/g/j/m/x;->c:Lf/g/j/m/d;

    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f64eb86 -> :sswitch_4
        -0x41f50c37 -> :sswitch_3
        -0x181d2318 -> :sswitch_2
        -0x17f85147 -> :sswitch_1
        0x5b804a8 -> :sswitch_0
    .end sparse-switch
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v0, v0, Lf/g/j/m/w;->c:Lf/g/j/m/y;

    iget v0, v0, Lf/g/j/m/y;->d:I

    return v0
.end method

.method public final c(I)Lf/g/j/m/s;
    .locals 11

    const-class v0, Lf/g/j/m/z;

    const-class v1, Lf/g/j/m/y;

    const-class v2, Lf/g/d/g/c;

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz p1, :cond_4

    if-eq p1, v7, :cond_2

    if-ne p1, v6, :cond_1

    iget-object p1, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    if-nez p1, :cond_0

    :try_start_0
    const-string p1, "com.facebook.imagepipeline.memory.AshmemMemoryChunkPool"

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    new-array v8, v4, [Ljava/lang/Class;

    aput-object v2, v8, v3

    aput-object v1, v8, v7

    aput-object v0, v8, v6

    invoke-virtual {p1, v8}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p1

    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v2, v1, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    aput-object v2, v0, v3

    iget-object v2, v1, Lf/g/j/m/w;->e:Lf/g/j/m/y;

    aput-object v2, v0, v7

    iget-object v1, v1, Lf/g/j/m/w;->f:Lf/g/j/m/z;

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/g/j/m/s;

    iput-object p1, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iput-object v5, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    goto :goto_0

    :catch_1
    iput-object v5, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    goto :goto_0

    :catch_2
    iput-object v5, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    goto :goto_0

    :catch_3
    iput-object v5, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    goto :goto_0

    :catch_4
    iput-object v5, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    :cond_0
    :goto_0
    iget-object p1, p0, Lf/g/j/m/x;->b:Lf/g/j/m/s;

    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid MemoryChunkType"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    iget-object p1, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    if-nez p1, :cond_3

    :try_start_1
    const-string p1, "com.facebook.imagepipeline.memory.BufferMemoryChunkPool"

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    new-array v8, v4, [Ljava/lang/Class;

    aput-object v2, v8, v3

    aput-object v1, v8, v7

    aput-object v0, v8, v6

    invoke-virtual {p1, v8}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p1

    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v2, v1, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    aput-object v2, v0, v3

    iget-object v2, v1, Lf/g/j/m/w;->e:Lf/g/j/m/y;

    aput-object v2, v0, v7

    iget-object v1, v1, Lf/g/j/m/w;->f:Lf/g/j/m/z;

    aput-object v1, v0, v6

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/g/j/m/s;

    iput-object p1, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_1

    :catch_5
    iput-object v5, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    goto :goto_1

    :catch_6
    iput-object v5, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    goto :goto_1

    :catch_7
    iput-object v5, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    goto :goto_1

    :catch_8
    iput-object v5, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    goto :goto_1

    :catch_9
    iput-object v5, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    :cond_3
    :goto_1
    iget-object p1, p0, Lf/g/j/m/x;->d:Lf/g/j/m/s;

    return-object p1

    :cond_4
    const-string p1, ""

    const-string v8, "PoolFactory"

    iget-object v9, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    if-nez v9, :cond_5

    :try_start_2
    const-string v9, "com.facebook.imagepipeline.memory.NativeMemoryChunkPool"

    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    new-array v10, v4, [Ljava/lang/Class;

    aput-object v2, v10, v3

    aput-object v1, v10, v7

    aput-object v0, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v4, v2, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    aput-object v4, v1, v3

    iget-object v3, v2, Lf/g/j/m/w;->e:Lf/g/j/m/y;

    aput-object v3, v1, v7

    iget-object v2, v2, Lf/g/j/m/w;->f:Lf/g/j/m/z;

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/m/s;

    iput-object v0, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_c
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_a

    goto :goto_2

    :catch_a
    move-exception v0

    invoke-static {v8, p1, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    goto :goto_2

    :catch_b
    move-exception v0

    invoke-static {v8, p1, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    goto :goto_2

    :catch_c
    move-exception v0

    invoke-static {v8, p1, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    goto :goto_2

    :catch_d
    move-exception v0

    invoke-static {v8, p1, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    goto :goto_2

    :catch_e
    move-exception v0

    invoke-static {v8, p1, v0}, Lf/g/d/e/a;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object v5, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    :cond_5
    :goto_2
    iget-object p1, p0, Lf/g/j/m/x;->e:Lf/g/j/m/s;

    return-object p1
.end method

.method public d(I)Lf/g/d/g/g;
    .locals 3

    iget-object v0, p0, Lf/g/j/m/x;->f:Lf/g/d/g/g;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lf/g/j/m/x;->c(I)Lf/g/j/m/s;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to get pool for chunk type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ls/a/b/b/a;->i(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lf/g/j/m/u;

    invoke-virtual {p0, p1}, Lf/g/j/m/x;->c(I)Lf/g/j/m/s;

    move-result-object p1

    invoke-virtual {p0}, Lf/g/j/m/x;->e()Lf/g/d/g/j;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lf/g/j/m/u;-><init>(Lf/g/j/m/s;Lf/g/d/g/j;)V

    iput-object v0, p0, Lf/g/j/m/x;->f:Lf/g/d/g/g;

    :cond_0
    iget-object p1, p0, Lf/g/j/m/x;->f:Lf/g/d/g/g;

    return-object p1
.end method

.method public e()Lf/g/d/g/j;
    .locals 2

    iget-object v0, p0, Lf/g/j/m/x;->g:Lf/g/d/g/j;

    if-nez v0, :cond_0

    new-instance v0, Lf/g/d/g/j;

    invoke-virtual {p0}, Lf/g/j/m/x;->f()Lf/g/d/g/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lf/g/d/g/j;-><init>(Lf/g/d/g/a;)V

    iput-object v0, p0, Lf/g/j/m/x;->g:Lf/g/d/g/j;

    :cond_0
    iget-object v0, p0, Lf/g/j/m/x;->g:Lf/g/d/g/j;

    return-object v0
.end method

.method public f()Lf/g/d/g/a;
    .locals 4

    iget-object v0, p0, Lf/g/j/m/x;->h:Lf/g/d/g/a;

    if-nez v0, :cond_0

    new-instance v0, Lf/g/j/m/o;

    iget-object v1, p0, Lf/g/j/m/x;->a:Lf/g/j/m/w;

    iget-object v2, v1, Lf/g/j/m/w;->d:Lf/g/d/g/c;

    iget-object v3, v1, Lf/g/j/m/w;->g:Lf/g/j/m/y;

    iget-object v1, v1, Lf/g/j/m/w;->h:Lf/g/j/m/z;

    invoke-direct {v0, v2, v3, v1}, Lf/g/j/m/o;-><init>(Lf/g/d/g/c;Lf/g/j/m/y;Lf/g/j/m/z;)V

    iput-object v0, p0, Lf/g/j/m/x;->h:Lf/g/d/g/a;

    :cond_0
    iget-object v0, p0, Lf/g/j/m/x;->h:Lf/g/d/g/a;

    return-object v0
.end method
