.class public Lf/g/j/h/a$a;
.super Ljava/lang/Object;
.source "DefaultImageDecoder.java"

# interfaces
.implements Lf/g/j/h/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/j/h/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/g/j/h/a;


# direct methods
.method public constructor <init>(Lf/g/j/h/a;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/h/a$a;->a:Lf/g/j/h/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;
    .locals 7

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    sget-object v1, Lf/g/i/b;->a:Lf/g/i/c;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lf/g/j/h/a$a;->a:Lf/g/j/h/a;

    iget-object v1, v0, Lf/g/j/h/a;->c:Lf/g/j/o/d;

    iget-object v3, p4, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v2, p1

    move v5, p2

    invoke-interface/range {v1 .. v6}, Lf/g/j/o/d;->b(Lf/g/j/j/e;Landroid/graphics/Bitmap$Config;Landroid/graphics/Rect;ILandroid/graphics/ColorSpace;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p2

    const/4 p4, 0x0

    :try_start_0
    invoke-virtual {v0, p4, p2}, Lf/g/j/h/a;->c(Lf/g/j/u/a;Lcom/facebook/common/references/CloseableReference;)V

    new-instance p4, Lf/g/j/j/d;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->g:I

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget p1, p1, Lf/g/j/j/e;->h:I

    invoke-direct {p4, p2, p3, v0, p1}, Lf/g/j/j/d;-><init>(Lcom/facebook/common/references/CloseableReference;Lf/g/j/j/i;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    return-object p4

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1

    :cond_0
    sget-object v1, Lf/g/i/b;->c:Lf/g/i/c;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lf/g/j/h/a$a;->a:Lf/g/j/h/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v1, p1, Lf/g/j/j/e;->i:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v1, p1, Lf/g/j/j/e;->j:I

    if-eq v1, v2, :cond_2

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lf/g/j/h/a;->a:Lf/g/j/h/b;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1, p2, p3, p4}, Lf/g/j/h/b;->a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1, p4}, Lf/g/j/h/a;->b(Lf/g/j/j/e;Lf/g/j/d/b;)Lf/g/j/j/d;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p2, Lcom/facebook/imagepipeline/decoder/DecodeException;

    const-string p3, "image width or height is incorrect"

    invoke-direct {p2, p3, p1}, Lcom/facebook/imagepipeline/decoder/DecodeException;-><init>(Ljava/lang/String;Lf/g/j/j/e;)V

    throw p2

    :cond_3
    sget-object v1, Lf/g/i/b;->j:Lf/g/i/c;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lf/g/j/h/a$a;->a:Lf/g/j/h/a;

    iget-object v0, v0, Lf/g/j/h/a;->b:Lf/g/j/h/b;

    invoke-interface {v0, p1, p2, p3, p4}, Lf/g/j/h/b;->a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;

    move-result-object p1

    return-object p1

    :cond_4
    sget-object p2, Lf/g/i/c;->b:Lf/g/i/c;

    if-eq v0, p2, :cond_5

    iget-object p2, p0, Lf/g/j/h/a$a;->a:Lf/g/j/h/a;

    invoke-virtual {p2, p1, p4}, Lf/g/j/h/a;->b(Lf/g/j/j/e;Lf/g/j/d/b;)Lf/g/j/j/d;

    move-result-object p1

    return-object p1

    :cond_5
    new-instance p2, Lcom/facebook/imagepipeline/decoder/DecodeException;

    const-string p3, "unknown image format"

    invoke-direct {p2, p3, p1}, Lcom/facebook/imagepipeline/decoder/DecodeException;-><init>(Ljava/lang/String;Lf/g/j/j/e;)V

    throw p2
.end method
