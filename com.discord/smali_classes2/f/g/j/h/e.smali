.class public Lf/g/j/h/e;
.super Ljava/lang/Object;
.source "SimpleProgressiveJpegConfig.java"

# interfaces
.implements Lf/g/j/h/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/h/e$b;,
        Lf/g/j/h/e$c;
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/h/e$c;


# direct methods
.method public constructor <init>()V
    .locals 2

    new-instance v0, Lf/g/j/h/e$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/g/j/h/e$b;-><init>(Lf/g/j/h/e$a;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/g/j/h/e;->a:Lf/g/j/h/e$c;

    return-void
.end method


# virtual methods
.method public a(I)Lf/g/j/j/i;
    .locals 3

    iget-object v0, p0, Lf/g/j/h/e;->a:Lf/g/j/h/e$c;

    invoke-interface {v0}, Lf/g/j/h/e$c;->b()I

    move-result v0

    const/4 v1, 0x0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v2, Lf/g/j/j/h;

    invoke-direct {v2, p1, v0, v1}, Lf/g/j/j/h;-><init>(IZZ)V

    return-object v2
.end method

.method public b(I)I
    .locals 3

    iget-object v0, p0, Lf/g/j/h/e;->a:Lf/g/j/h/e$c;

    invoke-interface {v0}, Lf/g/j/h/e$c;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, p1, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const p1, 0x7fffffff

    return p1

    :cond_3
    :goto_1
    add-int/lit8 p1, p1, 0x1

    return p1
.end method
