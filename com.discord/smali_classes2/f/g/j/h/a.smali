.class public Lf/g/j/h/a;
.super Ljava/lang/Object;
.source "DefaultImageDecoder.java"

# interfaces
.implements Lf/g/j/h/b;


# instance fields
.field public final a:Lf/g/j/h/b;

.field public final b:Lf/g/j/h/b;

.field public final c:Lf/g/j/o/d;

.field public final d:Lf/g/j/h/b;


# direct methods
.method public constructor <init>(Lf/g/j/h/b;Lf/g/j/h/b;Lf/g/j/o/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/g/j/h/a$a;

    invoke-direct {v0, p0}, Lf/g/j/h/a$a;-><init>(Lf/g/j/h/a;)V

    iput-object v0, p0, Lf/g/j/h/a;->d:Lf/g/j/h/b;

    iput-object p1, p0, Lf/g/j/h/a;->a:Lf/g/j/h/b;

    iput-object p2, p0, Lf/g/j/h/a;->b:Lf/g/j/h/b;

    iput-object p3, p0, Lf/g/j/h/a;->c:Lf/g/j/o/d;

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;
    .locals 2

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    if-eqz v0, :cond_0

    sget-object v1, Lf/g/i/c;->b:Lf/g/i/c;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lf/g/i/d;->b(Ljava/io/InputStream;)Lf/g/i/c;

    move-result-object v0

    iput-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    :cond_1
    iget-object v0, p0, Lf/g/j/h/a;->d:Lf/g/j/h/b;

    invoke-interface {v0, p1, p2, p3, p4}, Lf/g/j/h/b;->a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;

    move-result-object p1

    return-object p1
.end method

.method public b(Lf/g/j/j/e;Lf/g/j/d/b;)Lf/g/j/j/d;
    .locals 3

    iget-object v0, p0, Lf/g/j/h/a;->c:Lf/g/j/o/d;

    iget-object p2, p2, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1, v1}, Lf/g/j/o/d;->a(Lf/g/j/j/e;Landroid/graphics/Bitmap$Config;Landroid/graphics/Rect;Landroid/graphics/ColorSpace;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p2

    :try_start_0
    invoke-virtual {p0, v1, p2}, Lf/g/j/h/a;->c(Lf/g/j/u/a;Lcom/facebook/common/references/CloseableReference;)V

    new-instance v0, Lf/g/j/j/d;

    sget-object v1, Lf/g/j/j/h;->d:Lf/g/j/j/i;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v2, p1, Lf/g/j/j/e;->g:I

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget p1, p1, Lf/g/j/j/e;->h:I

    invoke-direct {v0, p2, v1, v2, p1}, Lf/g/j/j/d;-><init>(Lcom/facebook/common/references/CloseableReference;Lf/g/j/j/i;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    return-object v0

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1
.end method

.method public final c(Lf/g/j/u/a;Lcom/facebook/common/references/CloseableReference;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/u/a;",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
