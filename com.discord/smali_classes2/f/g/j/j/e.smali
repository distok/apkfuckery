.class public Lf/g/j/j/e;
.super Ljava/lang/Object;
.source "EncodedImage.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final d:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/io/FileInputStream;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lf/g/i/c;

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:Lf/g/j/d/a;

.field public n:Landroid/graphics/ColorSpace;


# direct methods
.method public constructor <init>(Lcom/facebook/common/internal/Supplier;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/io/FileInputStream;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/g/i/c;->b:Lf/g/i/c;

    iput-object v0, p0, Lf/g/j/j/e;->f:Lf/g/i/c;

    const/4 v0, -0x1

    iput v0, p0, Lf/g/j/j/e;->g:I

    const/4 v1, 0x0

    iput v1, p0, Lf/g/j/j/e;->h:I

    iput v0, p0, Lf/g/j/j/e;->i:I

    iput v0, p0, Lf/g/j/j/e;->j:I

    const/4 v1, 0x1

    iput v1, p0, Lf/g/j/j/e;->k:I

    iput v0, p0, Lf/g/j/j/e;->l:I

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    iput-object p1, p0, Lf/g/j/j/e;->e:Lcom/facebook/common/internal/Supplier;

    iput p2, p0, Lf/g/j/j/e;->l:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/references/CloseableReference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/g/i/c;->b:Lf/g/i/c;

    iput-object v0, p0, Lf/g/j/j/e;->f:Lf/g/i/c;

    const/4 v0, -0x1

    iput v0, p0, Lf/g/j/j/e;->g:I

    const/4 v1, 0x0

    iput v1, p0, Lf/g/j/j/e;->h:I

    iput v0, p0, Lf/g/j/j/e;->i:I

    iput v0, p0, Lf/g/j/j/e;->j:I

    const/4 v1, 0x1

    iput v1, p0, Lf/g/j/j/e;->k:I

    iput v0, p0, Lf/g/j/j/e;->l:I

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->b()Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/j/j/e;->e:Lcom/facebook/common/internal/Supplier;

    return-void
.end method

.method public static a(Lf/g/j/j/e;)Lf/g/j/j/e;
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_3

    iget-object v1, p0, Lf/g/j/j/e;->e:Lcom/facebook/common/internal/Supplier;

    if-eqz v1, :cond_0

    new-instance v0, Lf/g/j/j/e;

    iget v2, p0, Lf/g/j/j/e;->l:I

    invoke-direct {v0, v1, v2}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/internal/Supplier;I)V

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    :try_start_0
    new-instance v0, Lf/g/j/j/e;

    invoke-direct {v0, v1}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0, p0}, Lf/g/j/j/e;->b(Lf/g/j/j/e;)V

    goto :goto_2

    :catchall_0
    move-exception p0

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p0

    :cond_3
    :goto_2
    return-object v0
.end method

.method public static g(Lf/g/j/j/e;)Z
    .locals 1

    iget v0, p0, Lf/g/j/j/e;->g:I

    if-ltz v0, :cond_0

    iget v0, p0, Lf/g/j/j/e;->i:I

    if-ltz v0, :cond_0

    iget p0, p0, Lf/g/j/j/e;->j:I

    if-ltz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static m(Lf/g/j/j/e;)Z
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lf/g/j/j/e;->i()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public b(Lf/g/j/j/e;)V
    .locals 1

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object v0, p1, Lf/g/j/j/e;->f:Lf/g/i/c;

    iput-object v0, p0, Lf/g/j/j/e;->f:Lf/g/i/c;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->i:I

    iput v0, p0, Lf/g/j/j/e;->i:I

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->j:I

    iput v0, p0, Lf/g/j/j/e;->j:I

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->g:I

    iput v0, p0, Lf/g/j/j/e;->g:I

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v0, p1, Lf/g/j/j/e;->h:I

    iput v0, p0, Lf/g/j/j/e;->h:I

    iget v0, p1, Lf/g/j/j/e;->k:I

    iput v0, p0, Lf/g/j/j/e;->k:I

    invoke-virtual {p1}, Lf/g/j/j/e;->f()I

    move-result v0

    iput v0, p0, Lf/g/j/j/e;->l:I

    iget-object v0, p1, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    iput-object v0, p0, Lf/g/j/j/e;->m:Lf/g/j/d/a;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget-object p1, p1, Lf/g/j/j/e;->n:Landroid/graphics/ColorSpace;

    iput-object p1, p0, Lf/g/j/j/e;->n:Landroid/graphics/ColorSpace;

    return-void
.end method

.method public c()Lcom/facebook/common/references/CloseableReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lcom/facebook/common/memory/PooledByteBuffer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    sget-object v1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void
.end method

.method public d(I)Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Lf/g/j/j/e;->c()Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lf/g/j/j/e;->f()I

    move-result v2

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    new-array v2, p1, [B

    :try_start_0
    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/memory/PooledByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    return-object v1

    :cond_1
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v3, v1, v2, v1, p1}, Lcom/facebook/common/memory/PooledByteBuffer;->k(I[BII)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    new-instance v0, Ljava/lang/StringBuilder;

    mul-int/lit8 v3, p1, 0x2

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p1, :cond_2

    aget-byte v4, v2, v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v5, v1

    const-string v4, "%02X"

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1
.end method

.method public e()Ljava/io/InputStream;
    .locals 3

    iget-object v0, p0, Lf/g/j/j/e;->e:Lcom/facebook/common/internal/Supplier;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    return-object v0

    :cond_0
    iget-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    new-instance v1, Lf/g/d/g/h;

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/common/memory/PooledByteBuffer;

    invoke-direct {v1, v2}, Lf/g/d/g/h;-><init>(Lcom/facebook/common/memory/PooledByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/memory/PooledByteBuffer;

    invoke-interface {v0}, Lcom/facebook/common/memory/PooledByteBuffer;->size()I

    move-result v0

    return v0

    :cond_0
    iget v0, p0, Lf/g/j/j/e;->l:I

    return v0
.end method

.method public declared-synchronized i()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/j/e;->d:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/j/j/e;->e:Lcom/facebook/common/internal/Supplier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public n()V
    .locals 12

    invoke-virtual {p0}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lf/g/i/d;->b(Ljava/io/InputStream;)Lf/g/i/c;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/j/e;->f:Lf/g/i/c;

    invoke-static {v0}, Lf/g/i/b;->a(Lf/g/i/c;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_1

    sget-object v1, Lf/g/i/b;->j:Lf/g/i/c;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const/4 v4, 0x4

    const/4 v5, 0x0

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v1

    new-array v6, v4, [B

    :try_start_0
    invoke-virtual {v1, v6}, Ljava/io/InputStream;->read([B)I

    const-string v7, "RIFF"

    invoke-static {v6, v7}, Lf/g/j/k/a;->x([BLjava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    goto/16 :goto_4

    :cond_2
    invoke-static {v1}, Lf/g/j/k/a;->d0(Ljava/io/InputStream;)I

    invoke-virtual {v1, v6}, Ljava/io/InputStream;->read([B)I

    const-string v7, "WEBP"

    invoke-static {v6, v7}, Lf/g/j/k/a;->x([BLjava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    goto :goto_4

    :cond_3
    invoke-virtual {v1, v6}, Ljava/io/InputStream;->read([B)I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v4, :cond_4

    aget-byte v9, v6, v8

    int-to-char v9, v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "VP8 "

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-static {v1}, Lf/g/j/k/a;->h0(Ljava/io/InputStream;)Landroid/util/Pair;

    move-result-object v5

    goto :goto_4

    :cond_5
    const-string v7, "VP8L"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-static {v1}, Lf/g/j/k/a;->i0(Ljava/io/InputStream;)Landroid/util/Pair;

    move-result-object v5

    goto :goto_4

    :cond_6
    const-string v7, "VP8X"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-wide/16 v6, 0x8

    invoke-virtual {v1, v6, v7}, Ljava/io/InputStream;->skip(J)J

    new-instance v6, Landroid/util/Pair;

    invoke-static {v1}, Lf/g/j/k/a;->B0(Ljava/io/InputStream;)I

    move-result v7

    add-int/2addr v7, v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v1}, Lf/g/j/k/a;->B0(Ljava/io/InputStream;)I

    move-result v8

    add-int/2addr v8, v2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :goto_3
    move-object v5, v6

    goto :goto_5

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v6

    :try_start_2
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_8

    :cond_7
    :goto_4
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_5

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :cond_8
    :goto_5
    if-eqz v5, :cond_c

    iget-object v1, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lf/g/j/j/e;->i:I

    iget-object v1, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lf/g/j/j/e;->j:I

    goto :goto_8

    :goto_6
    if-eqz v1, :cond_9

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_7

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :cond_9
    :goto_7
    throw v0

    :cond_a
    :try_start_5
    invoke-virtual {p0}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lf/g/k/a;->a(Ljava/io/InputStream;)Lf/g/k/b;

    move-result-object v1

    iget-object v6, v1, Lf/g/k/b;->b:Landroid/graphics/ColorSpace;

    iput-object v6, p0, Lf/g/j/j/e;->n:Landroid/graphics/ColorSpace;

    iget-object v6, v1, Lf/g/k/b;->a:Landroid/util/Pair;

    if-eqz v6, :cond_b

    iget-object v7, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p0, Lf/g/j/j/e;->i:I

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, p0, Lf/g/j/j/e;->j:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_b
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    iget-object v5, v1, Lf/g/k/b;->a:Landroid/util/Pair;

    :cond_c
    :goto_8
    sget-object v1, Lf/g/i/b;->a:Lf/g/i/c;

    const/4 v6, -0x1

    if-ne v0, v1, :cond_22

    iget v1, p0, Lf/g/j/j/e;->g:I

    if-ne v1, v6, :cond_22

    if-eqz v5, :cond_25

    invoke-virtual {p0}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v0

    :try_start_7
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    :goto_9
    invoke-static {v0, v2, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v1

    const/4 v5, 0x2

    const/16 v6, 0xff

    if-ne v1, v6, :cond_12

    const/16 v1, 0xff

    :goto_a
    if-ne v1, v6, :cond_e

    invoke-static {v0, v2, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v1

    goto :goto_a

    :cond_e
    const/16 v6, 0xe1

    if-ne v1, v6, :cond_f

    const/4 v1, 0x1

    goto :goto_c

    :cond_f
    const/16 v6, 0xd8

    if-eq v1, v6, :cond_d

    if-ne v1, v2, :cond_10

    goto :goto_9

    :cond_10
    const/16 v6, 0xd9

    if-eq v1, v6, :cond_12

    const/16 v6, 0xda

    if-ne v1, v6, :cond_11

    goto :goto_b

    :cond_11
    invoke-static {v0, v5, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v1

    sub-int/2addr v1, v5

    int-to-long v5, v1

    invoke-virtual {v0, v5, v6}, Ljava/io/InputStream;->skip(J)J

    goto :goto_9

    :cond_12
    :goto_b
    const/4 v1, 0x0

    :goto_c
    if-eqz v1, :cond_13

    invoke-static {v0, v5, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v1

    sub-int/2addr v1, v5

    const/4 v6, 0x6

    if-le v1, v6, :cond_13

    invoke-static {v0, v4, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v6

    add-int/lit8 v1, v1, -0x4

    invoke-static {v0, v5, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v7

    add-int/lit8 v1, v1, -0x2

    const v8, 0x45786966

    if-ne v6, v8, :cond_13

    if-nez v7, :cond_13

    goto :goto_d

    :cond_13
    const/4 v1, 0x0

    :goto_d
    if-nez v1, :cond_14

    goto/16 :goto_14

    :cond_14
    const-class v6, Lf/g/k/c;

    const/16 v7, 0x8

    if-gt v1, v7, :cond_15

    goto :goto_e

    :cond_15
    invoke-static {v0, v4, v3}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v8

    add-int/lit8 v1, v1, -0x4

    const v9, 0x49492a00    # 823968.0f

    if-eq v8, v9, :cond_16

    const v10, 0x4d4d002a    # 2.14958752E8f

    if-eq v8, v10, :cond_16

    const-string v1, "Invalid TIFF header"

    invoke-static {v6, v1}, Lf/g/d/e/a;->a(Ljava/lang/Class;Ljava/lang/String;)V

    :goto_e
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    goto :goto_10

    :cond_16
    if-ne v8, v9, :cond_17

    const/4 v8, 0x1

    goto :goto_f

    :cond_17
    const/4 v8, 0x0

    :goto_f
    invoke-static {v0, v4, v8}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v9

    add-int/lit8 v1, v1, -0x4

    if-lt v9, v7, :cond_18

    add-int/lit8 v10, v9, -0x8

    if-le v10, v1, :cond_19

    :cond_18
    const-string v1, "Invalid offset"

    invoke-static {v6, v1}, Lf/g/d/e/a;->a(Ljava/lang/Class;Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_19
    :goto_10
    sub-int/2addr v9, v7

    if-eqz v1, :cond_21

    if-le v9, v1, :cond_1a

    goto :goto_14

    :cond_1a
    int-to-long v6, v9

    invoke-virtual {v0, v6, v7}, Ljava/io/InputStream;->skip(J)J

    sub-int/2addr v1, v9

    const/16 v6, 0x112

    const/16 v7, 0xe

    if-ge v1, v7, :cond_1b

    goto :goto_12

    :cond_1b
    invoke-static {v0, v5, v8}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v7

    add-int/lit8 v1, v1, -0x2

    :goto_11
    add-int/lit8 v9, v7, -0x1

    if-lez v7, :cond_1d

    const/16 v7, 0xc

    if-lt v1, v7, :cond_1d

    invoke-static {v0, v5, v8}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v7

    add-int/lit8 v1, v1, -0x2

    if-ne v7, v6, :cond_1c

    goto :goto_13

    :cond_1c
    const-wide/16 v10, 0xa

    invoke-virtual {v0, v10, v11}, Ljava/io/InputStream;->skip(J)J

    add-int/lit8 v1, v1, -0xa

    move v7, v9

    goto :goto_11

    :cond_1d
    :goto_12
    const/4 v1, 0x0

    :goto_13
    const/16 v6, 0xa

    if-ge v1, v6, :cond_1e

    goto :goto_14

    :cond_1e
    invoke-static {v0, v5, v8}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v1

    const/4 v6, 0x3

    if-eq v1, v6, :cond_1f

    goto :goto_14

    :cond_1f
    invoke-static {v0, v4, v8}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v1

    if-eq v1, v2, :cond_20

    goto :goto_14

    :cond_20
    invoke-static {v0, v5, v8}, Lf/g/j/k/a;->H0(Ljava/io/InputStream;IZ)I

    move-result v3
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    :cond_21
    :goto_14
    iput v3, p0, Lf/g/j/j/e;->h:I

    invoke-static {v3}, Lf/g/j/k/a;->b0(I)I

    move-result v0

    iput v0, p0, Lf/g/j/j/e;->g:I

    goto :goto_16

    :cond_22
    sget-object v1, Lf/g/i/b;->k:Lf/g/i/c;

    if-ne v0, v1, :cond_24

    iget v0, p0, Lf/g/j/j/e;->g:I

    if-ne v0, v6, :cond_24

    invoke-virtual {p0}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v1, v4, :cond_23

    :try_start_8
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, v0}, Landroid/media/ExifInterface;-><init>(Ljava/io/InputStream;)V

    const-string v0, "Orientation"

    invoke-virtual {v1, v0, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_15

    :catch_6
    sget v0, Lf/g/d/e/a;->a:I

    goto :goto_15

    :cond_23
    sget v0, Lf/g/d/e/a;->a:I

    :goto_15
    iput v3, p0, Lf/g/j/j/e;->h:I

    invoke-static {v3}, Lf/g/j/k/a;->b0(I)I

    move-result v0

    iput v0, p0, Lf/g/j/j/e;->g:I

    goto :goto_16

    :cond_24
    iget v0, p0, Lf/g/j/j/e;->g:I

    if-ne v0, v6, :cond_25

    iput v3, p0, Lf/g/j/j/e;->g:I

    :cond_25
    :goto_16
    return-void

    :catchall_1
    move-exception v0

    if-eqz v5, :cond_26

    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    :cond_26
    throw v0
.end method

.method public final o()V
    .locals 1

    iget v0, p0, Lf/g/j/j/e;->i:I

    if-ltz v0, :cond_0

    iget v0, p0, Lf/g/j/j/e;->j:I

    if-gez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lf/g/j/j/e;->n()V

    :cond_1
    return-void
.end method
