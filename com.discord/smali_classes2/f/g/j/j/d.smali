.class public Lf/g/j/j/d;
.super Lf/g/j/j/b;
.source "CloseableStaticBitmap.java"

# interfaces
.implements Lf/g/d/h/c;


# instance fields
.field public f:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public volatile g:Landroid/graphics/Bitmap;

.field public final h:Lf/g/j/j/i;

.field public final i:I

.field public final j:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Lf/g/d/h/f;Lf/g/j/j/i;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lf/g/d/h/f<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lf/g/j/j/i;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/g/j/j/b;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    iget-object p1, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, p2}, Lcom/facebook/common/references/CloseableReference;->B(Ljava/lang/Object;Lf/g/d/h/f;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    iput-object p1, p0, Lf/g/j/j/d;->f:Lcom/facebook/common/references/CloseableReference;

    iput-object p3, p0, Lf/g/j/j/d;->h:Lf/g/j/j/i;

    iput p4, p0, Lf/g/j/j/d;->i:I

    const/4 p1, 0x0

    iput p1, p0, Lf/g/j/j/d;->j:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/references/CloseableReference;Lf/g/j/j/i;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lf/g/j/j/i;",
            "II)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/g/j/j/b;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->e()Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lf/g/j/j/d;->f:Lcom/facebook/common/references/CloseableReference;

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Bitmap;

    iput-object p1, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lf/g/j/j/d;->h:Lf/g/j/j/i;

    iput p3, p0, Lf/g/j/j/d;->i:I

    iput p4, p0, Lf/g/j/j/d;->j:I

    return-void
.end method


# virtual methods
.method public b()Lf/g/j/j/i;
    .locals 1

    iget-object v0, p0, Lf/g/j/j/d;->h:Lf/g/j/j/i;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lf/g/k/a;->d(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/j/d;->f:Lcom/facebook/common/references/CloseableReference;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/g/j/j/d;->f:Lcom/facebook/common/references/CloseableReference;

    iput-object v1, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getHeight()I
    .locals 3

    iget v0, p0, Lf/g/j/j/d;->i:I

    rem-int/lit16 v0, v0, 0xb4

    const/4 v1, 0x0

    if-nez v0, :cond_2

    iget v0, p0, Lf/g/j/j/d;->j:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-ne v0, v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    :goto_0
    return v1

    :cond_2
    :goto_1
    iget-object v0, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    :goto_2
    return v1
.end method

.method public getWidth()I
    .locals 3

    iget v0, p0, Lf/g/j/j/d;->i:I

    rem-int/lit16 v0, v0, 0xb4

    const/4 v1, 0x0

    if-nez v0, :cond_2

    iget v0, p0, Lf/g/j/j/d;->j:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-ne v0, v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    :goto_0
    return v1

    :cond_2
    :goto_1
    iget-object v0, p0, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    :goto_2
    return v1
.end method

.method public declared-synchronized isClosed()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/j/d;->f:Lcom/facebook/common/references/CloseableReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
