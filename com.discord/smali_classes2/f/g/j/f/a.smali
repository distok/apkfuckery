.class public Lf/g/j/f/a;
.super Lf/g/j/q/b;
.source "AbstractProducerToDataSourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic b:Lf/g/j/f/b;


# direct methods
.method public constructor <init>(Lf/g/j/f/b;)V
    .locals 0

    iput-object p1, p0, Lf/g/j/f/a;->b:Lf/g/j/f/b;

    invoke-direct {p0}, Lf/g/j/q/b;-><init>()V

    return-void
.end method


# virtual methods
.method public g()V
    .locals 2

    iget-object v0, p0, Lf/g/j/f/a;->b:Lf/g/j/f/b;

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, Lf/g/e/c;->i()Z

    move-result v1

    invoke-static {v1}, Ls/a/b/b/a;->j(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public h(Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lf/g/j/f/a;->b:Lf/g/j/f/b;

    iget-object v1, v0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    invoke-virtual {v1}, Lf/g/j/q/d;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lf/g/e/c;->k(Ljava/lang/Throwable;Ljava/util/Map;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/g/j/f/b;->i:Lf/g/j/l/d;

    iget-object v0, v0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    invoke-interface {v1, v0, p1}, Lf/g/j/l/d;->h(Lf/g/j/q/v0;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public i(Ljava/lang/Object;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    iget-object v0, p0, Lf/g/j/f/a;->b:Lf/g/j/f/b;

    iget-object v1, v0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    check-cast v0, Lf/g/j/f/d;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    invoke-static {p2}, Lf/g/j/q/b;->e(I)Z

    move-result p2

    invoke-virtual {v1}, Lf/g/j/q/d;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lf/g/e/c;->m(Ljava/lang/Object;ZLjava/util/Map;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object p1, v0, Lf/g/j/f/b;->i:Lf/g/j/l/d;

    iget-object p2, v0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    invoke-interface {p1, p2}, Lf/g/j/l/d;->f(Lf/g/j/q/v0;)V

    :cond_0
    return-void
.end method

.method public j(F)V
    .locals 1

    iget-object v0, p0, Lf/g/j/f/a;->b:Lf/g/j/f/b;

    invoke-virtual {v0, p1}, Lf/g/e/c;->l(F)Z

    return-void
.end method
