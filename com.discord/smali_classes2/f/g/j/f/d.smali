.class public Lf/g/j/f/d;
.super Lf/g/j/f/b;
.source "CloseableProducerToDataSourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lf/g/j/f/b<",
        "Lcom/facebook/common/references/CloseableReference<",
        "TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lf/g/j/q/u0;Lf/g/j/q/b1;Lf/g/j/l/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "Lcom/facebook/common/references/CloseableReference<",
            "TT;>;>;",
            "Lf/g/j/q/b1;",
            "Lf/g/j/l/d;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lf/g/j/f/b;-><init>(Lf/g/j/q/u0;Lf/g/j/q/b1;Lf/g/j/l/d;)V

    return-void
.end method


# virtual methods
.method public g(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void
.end method

.method public getResult()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Lf/g/e/c;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/references/CloseableReference;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    return-object v0
.end method
