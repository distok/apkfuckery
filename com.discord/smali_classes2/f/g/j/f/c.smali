.class public abstract Lf/g/j/f/c;
.super Lf/g/e/d;
.source "BaseBitmapDataSubscriber.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/e/d<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/g/e/d;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract onNewResultImpl(Landroid/graphics/Bitmap;)V
.end method

.method public onNewResultImpl(Lcom/facebook/datasource/DataSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;)V"
        }
    .end annotation

    invoke-interface {p1}, Lcom/facebook/datasource/DataSource;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Lcom/facebook/datasource/DataSource;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lf/g/j/j/b;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/j/b;

    invoke-virtual {v0}, Lf/g/j/j/b;->f()Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    :try_start_0
    invoke-virtual {p0, v0}, Lf/g/j/f/c;->onNewResultImpl(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_3
    throw v0
.end method
