.class public abstract Lf/g/j/f/b;
.super Lf/g/e/c;
.source "AbstractProducerToDataSourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lf/g/e/c<",
        "TT;>;",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final h:Lf/g/j/q/b1;

.field public final i:Lf/g/j/l/d;


# direct methods
.method public constructor <init>(Lf/g/j/q/u0;Lf/g/j/q/b1;Lf/g/j/l/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/j/q/u0<",
            "TT;>;",
            "Lf/g/j/q/b1;",
            "Lf/g/j/l/d;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lf/g/e/c;-><init>()V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iput-object p2, p0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    iput-object p3, p0, Lf/g/j/f/b;->i:Lf/g/j/l/d;

    iget-object v0, p2, Lf/g/j/q/d;->g:Ljava/util/Map;

    iput-object v0, p0, Lf/g/e/c;->a:Ljava/util/Map;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-interface {p3, p2}, Lf/g/j/l/d;->b(Lf/g/j/q/v0;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance p3, Lf/g/j/f/a;

    invoke-direct {p3, p0}, Lf/g/j/f/a;-><init>(Lf/g/j/f/b;)V

    invoke-interface {p1, p3, p2}, Lf/g/j/q/u0;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method


# virtual methods
.method public close()Z
    .locals 2

    invoke-super {p0}, Lf/g/e/c;->close()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lf/g/e/c;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/j/f/b;->i:Lf/g/j/l/d;

    iget-object v1, p0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    invoke-interface {v0, v1}, Lf/g/j/l/d;->i(Lf/g/j/q/v0;)V

    iget-object v0, p0, Lf/g/j/f/b;->h:Lf/g/j/q/b1;

    invoke-virtual {v0}, Lf/g/j/q/d;->u()V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method
