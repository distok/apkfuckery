.class public Lf/g/j/b/b;
.super Ljava/lang/Object;
.source "SimpleBitmapReleaser.java"

# interfaces
.implements Lf/g/d/h/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/g/d/h/f<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Lf/g/j/b/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lf/g/j/b/b;
    .locals 1

    sget-object v0, Lf/g/j/b/b;->a:Lf/g/j/b/b;

    if-nez v0, :cond_0

    new-instance v0, Lf/g/j/b/b;

    invoke-direct {v0}, Lf/g/j/b/b;-><init>()V

    sput-object v0, Lf/g/j/b/b;->a:Lf/g/j/b/b;

    :cond_0
    sget-object v0, Lf/g/j/b/b;->a:Lf/g/j/b/b;

    return-object v0
.end method


# virtual methods
.method public release(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    return-void
.end method
