.class public Lf/g/j/b/a;
.super Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;
.source "ArtBitmapFactory.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field public final a:Lf/g/j/m/d;

.field public final b:Lf/g/j/e/b;


# direct methods
.method public constructor <init>(Lf/g/j/m/d;Lf/g/j/e/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;-><init>()V

    iput-object p1, p0, Lf/g/j/b/a;->a:Lf/g/j/m/d;

    iput-object p2, p0, Lf/g/j/b/a;->b:Lf/g/j/e/b;

    return-void
.end method


# virtual methods
.method public b(IILandroid/graphics/Bitmap$Config;)Lcom/facebook/common/references/CloseableReference;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/graphics/Bitmap$Config;",
            ")",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    invoke-static {p1, p2, p3}, Lf/g/k/a;->c(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    iget-object v1, p0, Lf/g/j/b/a;->a:Lf/g/j/m/d;

    invoke-interface {v1, v0}, Lf/g/d/g/e;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v1

    mul-int v2, p1, p2

    invoke-static {p3}, Lf/g/k/a;->b(Landroid/graphics/Bitmap$Config;)I

    move-result v3

    mul-int v3, v3, v2

    if-lt v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ls/a/b/b/a;->g(Z)V

    invoke-virtual {v0, p1, p2, p3}, Landroid/graphics/Bitmap;->reconfigure(IILandroid/graphics/Bitmap$Config;)V

    iget-object p1, p0, Lf/g/j/b/a;->b:Lf/g/j/e/b;

    iget-object p2, p0, Lf/g/j/b/a;->a:Lf/g/j/m/d;

    iget-object p1, p1, Lf/g/j/e/b;->a:Lcom/facebook/common/references/CloseableReference$c;

    invoke-static {v0, p2, p1}, Lcom/facebook/common/references/CloseableReference;->C(Ljava/lang/Object;Lf/g/d/h/f;Lcom/facebook/common/references/CloseableReference$c;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    return-object p1
.end method
