.class public Lf/g/j/a/c/c;
.super Ljava/lang/Object;
.source "AnimatedFrameCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/a/c/c$b;
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/cache/common/CacheKey;

.field public final b:Lf/g/j/c/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/g/j/c/m$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m$c<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet<",
            "Lcom/facebook/cache/common/CacheKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/cache/common/CacheKey;Lf/g/j/c/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/a/c/c;->a:Lcom/facebook/cache/common/CacheKey;

    iput-object p2, p0, Lf/g/j/a/c/c;->b:Lf/g/j/c/m;

    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lf/g/j/a/c/c;->d:Ljava/util/LinkedHashSet;

    new-instance p1, Lf/g/j/a/c/c$a;

    invoke-direct {p1, p0}, Lf/g/j/a/c/c$a;-><init>(Lf/g/j/a/c/c;)V

    iput-object p1, p0, Lf/g/j/a/c/c;->c:Lf/g/j/c/m$c;

    return-void
.end method


# virtual methods
.method public a(I)Z
    .locals 3

    iget-object v0, p0, Lf/g/j/a/c/c;->b:Lf/g/j/c/m;

    new-instance v1, Lf/g/j/a/c/c$b;

    iget-object v2, p0, Lf/g/j/a/c/c;->a:Lcom/facebook/cache/common/CacheKey;

    invoke-direct {v1, v2, p1}, Lf/g/j/a/c/c$b;-><init>(Lcom/facebook/cache/common/CacheKey;I)V

    monitor-enter v0

    :try_start_0
    iget-object p1, v0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p1, Lf/g/j/c/k;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit p1

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public b()Lcom/facebook/common/references/CloseableReference;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/j/a/c/c;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/cache/common/CacheKey;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    monitor-exit p0

    if-nez v1, :cond_2

    return-object v2

    :cond_2
    iget-object v0, p0, Lf/g/j/a/c/c;->b:Lf/g/j/c/m;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-enter v0

    :try_start_1
    iget-object v3, v0, Lf/g/j/c/m;->b:Lf/g/j/c/k;

    invoke-virtual {v3, v1}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/g/j/c/m$b;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_4

    iget-object v2, v0, Lf/g/j/c/m;->c:Lf/g/j/c/k;

    invoke-virtual {v2, v1}, Lf/g/j/c/k;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/j/c/m$b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget v2, v1, Lf/g/j/c/m$b;->c:I

    if-nez v2, :cond_3

    const/4 v4, 0x1

    :cond_3
    invoke-static {v4}, Ls/a/b/b/a;->j(Z)V

    iget-object v2, v1, Lf/g/j/c/m$b;->b:Lcom/facebook/common/references/CloseableReference;

    const/4 v4, 0x1

    :cond_4
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_5

    invoke-static {v3}, Lf/g/j/c/m;->j(Lf/g/j/c/m$b;)V

    :cond_5
    if-eqz v2, :cond_0

    return-object v2

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
