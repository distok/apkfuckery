.class public Lf/g/j/a/c/d;
.super Ljava/lang/Object;
.source "AnimatedImageCompositor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/j/a/c/d$b;,
        Lf/g/j/a/c/d$a;
    }
.end annotation


# instance fields
.field public final a:Lf/g/j/a/a/a;

.field public final b:Lf/g/j/a/c/d$a;

.field public final c:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lf/g/j/a/a/a;Lf/g/j/a/c/d$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    iput-object p2, p0, Lf/g/j/a/c/d;->b:Lf/g/j/a/c/d$a;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lf/g/j/a/c/d;->c:Landroid/graphics/Paint;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance p2, Landroid/graphics/PorterDuffXfermode;

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p2, v0}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Lf/g/j/a/a/b;)V
    .locals 7

    iget v0, p2, Lf/g/j/a/a/b;->a:I

    int-to-float v2, v0

    iget v1, p2, Lf/g/j/a/a/b;->b:I

    int-to-float v3, v1

    iget v4, p2, Lf/g/j/a/a/b;->c:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget p2, p2, Lf/g/j/a/a/b;->d:I

    add-int/2addr v1, p2

    int-to-float v5, v1

    iget-object v6, p0, Lf/g/j/a/c/d;->c:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final b(Lf/g/j/a/a/b;)Z
    .locals 2

    iget v0, p1, Lf/g/j/a/a/b;->a:I

    if-nez v0, :cond_0

    iget v0, p1, Lf/g/j/a/a/b;->b:I

    if-nez v0, :cond_0

    iget v0, p1, Lf/g/j/a/a/b;->c:I

    iget-object v1, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v1, Lf/g/j/a/c/a;

    iget-object v1, v1, Lf/g/j/a/c/a;->d:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget p1, p1, Lf/g/j/a/a/b;->d:I

    iget-object v0, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v0, Lf/g/j/a/c/a;

    iget-object v0, v0, Lf/g/j/a/c/a;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final c(I)Z
    .locals 4

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    move-object v2, v1

    check-cast v2, Lf/g/j/a/c/a;

    iget-object v2, v2, Lf/g/j/a/c/a;->f:[Lf/g/j/a/a/b;

    aget-object v2, v2, p1

    sub-int/2addr p1, v0

    check-cast v1, Lf/g/j/a/c/a;

    iget-object v1, v1, Lf/g/j/a/c/a;->f:[Lf/g/j/a/a/b;

    aget-object p1, v1, p1

    iget-object v1, v2, Lf/g/j/a/a/b;->e:Lf/g/j/a/a/b$a;

    sget-object v3, Lf/g/j/a/a/b$a;->e:Lf/g/j/a/a/b$a;

    if-ne v1, v3, :cond_1

    invoke-virtual {p0, v2}, Lf/g/j/a/c/d;->b(Lf/g/j/a/a/b;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    :cond_1
    iget-object v1, p1, Lf/g/j/a/a/b;->f:Lf/g/j/a/a/b$b;

    sget-object v2, Lf/g/j/a/a/b$b;->e:Lf/g/j/a/a/b$b;

    if-ne v1, v2, :cond_2

    invoke-virtual {p0, p1}, Lf/g/j/a/c/d;->b(Lf/g/j/a/a/b;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d(ILandroid/graphics/Bitmap;)V
    .locals 10

    sget-object v0, Lf/g/j/a/a/b$a;->e:Lf/g/j/a/a/b$a;

    sget-object v1, Lf/g/j/a/a/b$b;->f:Lf/g/j/a/a/b$b;

    sget-object v2, Lf/g/j/a/a/b$b;->e:Lf/g/j/a/a/b$b;

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0, p1}, Lf/g/j/a/c/d;->c(I)Z

    move-result v4

    if-nez v4, :cond_a

    add-int/lit8 v4, p1, -0x1

    :goto_0
    if-ltz v4, :cond_b

    sget-object v6, Lf/g/j/a/c/d$b;->d:Lf/g/j/a/c/d$b;

    iget-object v7, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v7, Lf/g/j/a/c/a;

    iget-object v7, v7, Lf/g/j/a/c/a;->f:[Lf/g/j/a/a/b;

    aget-object v7, v7, v4

    iget-object v8, v7, Lf/g/j/a/a/b;->f:Lf/g/j/a/a/b$b;

    sget-object v9, Lf/g/j/a/a/b$b;->d:Lf/g/j/a/a/b$b;

    if-ne v8, v9, :cond_0

    goto :goto_1

    :cond_0
    if-ne v8, v2, :cond_1

    invoke-virtual {p0, v7}, Lf/g/j/a/c/d;->b(Lf/g/j/a/a/b;)Z

    move-result v7

    if-eqz v7, :cond_3

    sget-object v6, Lf/g/j/a/c/d$b;->e:Lf/g/j/a/c/d$b;

    goto :goto_1

    :cond_1
    if-ne v8, v1, :cond_2

    sget-object v6, Lf/g/j/a/c/d$b;->f:Lf/g/j/a/c/d$b;

    goto :goto_1

    :cond_2
    sget-object v6, Lf/g/j/a/c/d$b;->g:Lf/g/j/a/c/d$b;

    :cond_3
    :goto_1
    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    if-eqz v6, :cond_5

    const/4 v7, 0x1

    if-eq v6, v7, :cond_4

    const/4 v7, 0x3

    if-eq v6, v7, :cond_8

    goto :goto_2

    :cond_4
    add-int/lit8 v5, v4, 0x1

    goto :goto_3

    :cond_5
    iget-object v6, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v6, Lf/g/j/a/c/a;

    iget-object v6, v6, Lf/g/j/a/c/a;->f:[Lf/g/j/a/a/b;

    aget-object v6, v6, v4

    iget-object v7, p0, Lf/g/j/a/c/d;->b:Lf/g/j/a/c/d$a;

    invoke-interface {v7, v4}, Lf/g/j/a/c/d$a;->b(I)Lcom/facebook/common/references/CloseableReference;

    move-result-object v7

    if-eqz v7, :cond_7

    :try_start_0
    invoke-virtual {v7}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v3, v5, v9, v9, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v5, v6, Lf/g/j/a/a/b;->f:Lf/g/j/a/a/b$b;

    if-ne v5, v2, :cond_6

    invoke-virtual {p0, v3, v6}, Lf/g/j/a/c/d;->a(Landroid/graphics/Canvas;Lf/g/j/a/a/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_6
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v7}, Lcom/facebook/common/references/CloseableReference;->close()V

    goto :goto_3

    :catchall_0
    move-exception p1

    invoke-virtual {v7}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p1

    :cond_7
    invoke-virtual {p0, v4}, Lf/g/j/a/c/d;->c(I)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_8
    move v5, v4

    goto :goto_3

    :cond_9
    :goto_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_a
    move v5, p1

    :cond_b
    :goto_3
    if-ge v5, p1, :cond_f

    iget-object v4, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v4, Lf/g/j/a/c/a;

    iget-object v4, v4, Lf/g/j/a/c/a;->f:[Lf/g/j/a/a/b;

    aget-object v4, v4, v5

    iget-object v6, v4, Lf/g/j/a/a/b;->f:Lf/g/j/a/a/b$b;

    if-ne v6, v1, :cond_c

    goto :goto_4

    :cond_c
    iget-object v7, v4, Lf/g/j/a/a/b;->e:Lf/g/j/a/a/b$a;

    if-ne v7, v0, :cond_d

    invoke-virtual {p0, v3, v4}, Lf/g/j/a/c/d;->a(Landroid/graphics/Canvas;Lf/g/j/a/a/b;)V

    :cond_d
    iget-object v7, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v7, Lf/g/j/a/c/a;

    invoke-virtual {v7, v5, v3}, Lf/g/j/a/c/a;->d(ILandroid/graphics/Canvas;)V

    iget-object v7, p0, Lf/g/j/a/c/d;->b:Lf/g/j/a/c/d$a;

    invoke-interface {v7, v5, p2}, Lf/g/j/a/c/d$a;->a(ILandroid/graphics/Bitmap;)V

    if-ne v6, v2, :cond_e

    invoke-virtual {p0, v3, v4}, Lf/g/j/a/c/d;->a(Landroid/graphics/Canvas;Lf/g/j/a/a/b;)V

    :cond_e
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_f
    iget-object v1, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v1, Lf/g/j/a/c/a;

    iget-object v1, v1, Lf/g/j/a/c/a;->f:[Lf/g/j/a/a/b;

    aget-object v1, v1, p1

    iget-object v2, v1, Lf/g/j/a/a/b;->e:Lf/g/j/a/a/b$a;

    if-ne v2, v0, :cond_10

    invoke-virtual {p0, v3, v1}, Lf/g/j/a/c/d;->a(Landroid/graphics/Canvas;Lf/g/j/a/a/b;)V

    :cond_10
    iget-object v0, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast v0, Lf/g/j/a/c/a;

    invoke-virtual {v0, p1, v3}, Lf/g/j/a/c/a;->d(ILandroid/graphics/Canvas;)V

    iget-object p1, p0, Lf/g/j/a/c/d;->a:Lf/g/j/a/a/a;

    check-cast p1, Lf/g/j/a/c/a;

    iget-object p1, p1, Lf/g/j/a/c/a;->b:Lf/g/j/a/a/e;

    if-nez p1, :cond_11

    goto :goto_5

    :cond_11
    iget-object p1, p1, Lf/g/j/a/a/e;->d:Lf/g/j/u/a;

    if-nez p1, :cond_12

    goto :goto_5

    :cond_12
    invoke-interface {p1, p2}, Lf/g/j/u/a;->a(Landroid/graphics/Bitmap;)V

    :goto_5
    return-void
.end method
