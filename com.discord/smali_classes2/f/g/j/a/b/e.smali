.class public Lf/g/j/a/b/e;
.super Ljava/lang/Object;
.source "AnimatedImageFactoryImpl.java"

# interfaces
.implements Lf/g/j/a/b/d;


# static fields
.field public static c:Lf/g/j/a/b/c;

.field public static d:Lf/g/j/a/b/c;


# instance fields
.field public final a:Lf/g/j/a/c/b;

.field public final b:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-string v0, "com.facebook.animated.gif.GifImage"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/a/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-object v0, v1

    :goto_0
    sput-object v0, Lf/g/j/a/b/e;->c:Lf/g/j/a/b/c;

    const-string v0, "com.facebook.animated.webp.WebPImage"

    :try_start_1
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/j/a/b/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v0

    :catchall_1
    sput-object v1, Lf/g/j/a/b/e;->d:Lf/g/j/a/b/c;

    return-void
.end method

.method public constructor <init>(Lf/g/j/a/c/b;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/a/b/e;->a:Lf/g/j/a/c/b;

    iput-object p2, p0, Lf/g/j/a/b/e;->b:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    return-void
.end method


# virtual methods
.method public final a(Lf/g/j/d/b;Lf/g/j/a/a/c;Landroid/graphics/Bitmap$Config;)Lf/g/j/j/c;
    .locals 1

    const/4 p3, 0x0

    :try_start_0
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    new-instance v0, Lf/g/j/a/a/f;

    invoke-direct {v0, p2}, Lf/g/j/a/a/f;-><init>(Lf/g/j/a/a/c;)V

    invoke-static {p3}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object p2

    iput-object p2, v0, Lf/g/j/a/a/f;->b:Lcom/facebook/common/references/CloseableReference;

    iput p1, v0, Lf/g/j/a/a/f;->d:I

    invoke-static {p3}, Lcom/facebook/common/references/CloseableReference;->g(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    iput-object p1, v0, Lf/g/j/a/a/f;->c:Ljava/util/List;

    iput-object p3, v0, Lf/g/j/a/a/f;->e:Lf/g/j/u/a;

    invoke-virtual {v0}, Lf/g/j/a/a/f;->a()Lf/g/j/a/a/e;

    move-result-object p1

    new-instance p2, Lf/g/j/j/a;

    invoke-direct {p2, p1}, Lf/g/j/j/a;-><init>(Lf/g/j/a/a/e;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p3}, Lcom/facebook/common/references/CloseableReference;->m(Ljava/lang/Iterable;)V

    return-object p2

    :catchall_0
    move-exception p1

    sget-object p2, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    invoke-static {p3}, Lcom/facebook/common/references/CloseableReference;->m(Ljava/lang/Iterable;)V

    throw p1
.end method
