.class public Lf/g/j/a/a/f;
.super Ljava/lang/Object;
.source "AnimatedImageResultBuilder.java"


# instance fields
.field public final a:Lf/g/j/a/a/c;

.field public b:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:I

.field public e:Lf/g/j/u/a;


# direct methods
.method public constructor <init>(Lf/g/j/a/a/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/j/a/a/f;->a:Lf/g/j/a/a/c;

    return-void
.end method


# virtual methods
.method public a()Lf/g/j/a/a/e;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lf/g/j/a/a/e;

    invoke-direct {v1, p0}, Lf/g/j/a/a/e;-><init>(Lf/g/j/a/a/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lf/g/j/a/a/f;->b:Lcom/facebook/common/references/CloseableReference;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    iput-object v0, p0, Lf/g/j/a/a/f;->b:Lcom/facebook/common/references/CloseableReference;

    iget-object v2, p0, Lf/g/j/a/a/f;->c:Ljava/util/List;

    invoke-static {v2}, Lcom/facebook/common/references/CloseableReference;->m(Ljava/lang/Iterable;)V

    iput-object v0, p0, Lf/g/j/a/a/f;->c:Ljava/util/List;

    return-object v1

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lf/g/j/a/a/f;->b:Lcom/facebook/common/references/CloseableReference;

    sget-object v3, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_1
    iput-object v0, p0, Lf/g/j/a/a/f;->b:Lcom/facebook/common/references/CloseableReference;

    iget-object v2, p0, Lf/g/j/a/a/f;->c:Ljava/util/List;

    invoke-static {v2}, Lcom/facebook/common/references/CloseableReference;->m(Ljava/lang/Iterable;)V

    iput-object v0, p0, Lf/g/j/a/a/f;->c:Ljava/util/List;

    throw v1
.end method
