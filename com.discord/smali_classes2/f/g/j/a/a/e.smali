.class public Lf/g/j/a/a/e;
.super Ljava/lang/Object;
.source "AnimatedImageResult.java"


# instance fields
.field public final a:Lf/g/j/a/a/c;

.field public b:Lcom/facebook/common/references/CloseableReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Lf/g/j/u/a;


# direct methods
.method public constructor <init>(Lf/g/j/a/a/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lf/g/j/a/a/f;->a:Lf/g/j/a/a/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p0, Lf/g/j/a/a/e;->a:Lf/g/j/a/a/c;

    iget-object v0, p1, Lf/g/j/a/a/f;->b:Lcom/facebook/common/references/CloseableReference;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->f(Lcom/facebook/common/references/CloseableReference;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/a/a/e;->b:Lcom/facebook/common/references/CloseableReference;

    iget-object v0, p1, Lf/g/j/a/a/f;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/facebook/common/references/CloseableReference;->g(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lf/g/j/a/a/e;->c:Ljava/util/List;

    iget-object p1, p1, Lf/g/j/a/a/f;->e:Lf/g/j/u/a;

    iput-object p1, p0, Lf/g/j/a/a/e;->d:Lf/g/j/u/a;

    return-void
.end method
