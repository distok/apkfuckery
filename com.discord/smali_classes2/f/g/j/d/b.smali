.class public Lf/g/j/d/b;
.super Ljava/lang/Object;
.source "ImageDecodeOptions.java"


# static fields
.field public static final d:Lf/g/j/d/b;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Landroid/graphics/Bitmap$Config;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/g/j/d/c;

    invoke-direct {v0}, Lf/g/j/d/c;-><init>()V

    new-instance v1, Lf/g/j/d/b;

    invoke-direct {v1, v0}, Lf/g/j/d/b;-><init>(Lf/g/j/d/c;)V

    sput-object v1, Lf/g/j/d/b;->d:Lf/g/j/d/b;

    return-void
.end method

.method public constructor <init>(Lf/g/j/d/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x64

    iput v0, p0, Lf/g/j/d/b;->a:I

    const v0, 0x7fffffff

    iput v0, p0, Lf/g/j/d/b;->b:I

    iget-object p1, p1, Lf/g/j/d/c;->a:Landroid/graphics/Bitmap$Config;

    iput-object p1, p0, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_5

    const-class v2, Lf/g/j/d/b;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lf/g/j/d/b;

    iget v2, p0, Lf/g/j/d/b;->a:I

    iget v3, p1, Lf/g/j/d/b;->a:I

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    iget v2, p0, Lf/g/j/d/b;->b:I

    iget v3, p1, Lf/g/j/d/b;->b:I

    if-eq v2, v3, :cond_3

    return v1

    :cond_3
    iget-object v2, p0, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    iget-object p1, p1, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    if-eq v2, p1, :cond_4

    return v1

    :cond_4
    return v0

    :cond_5
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lf/g/j/d/b;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lf/g/j/d/b;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    add-int/lit8 v1, v1, 0x0

    mul-int/lit8 v1, v1, 0x1f

    add-int/lit8 v1, v1, 0x0

    mul-int/lit8 v1, v1, 0x1f

    add-int/lit8 v1, v1, 0x0

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ImageDecodeOptions{"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Ls/a/b/b/a;->c0(Ljava/lang/Object;)Lf/g/d/d/i;

    move-result-object v1

    iget v2, p0, Lf/g/j/d/b;->a:I

    const-string v3, "minDecodeIntervalMs"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->a(Ljava/lang/String;I)Lf/g/d/d/i;

    iget v2, p0, Lf/g/j/d/b;->b:I

    const-string v3, "maxDimensionPx"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->a(Ljava/lang/String;I)Lf/g/d/d/i;

    const/4 v2, 0x0

    const-string v3, "decodePreviewFrame"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    const-string v3, "useLastFrameForPreview"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    const-string v3, "decodeAllFrames"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    const-string v3, "forceStaticImage"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-object v2, p0, Lf/g/j/d/b;->c:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$Config;->name()Ljava/lang/String;

    move-result-object v2

    const-string v3, "bitmapConfigName"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    const/4 v2, 0x0

    const-string v3, "customImageDecoder"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    const-string v3, "bitmapTransformation"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    const-string v3, "colorSpace"

    invoke-virtual {v1, v3, v2}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    invoke-virtual {v1}, Lf/g/d/d/i;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
