.class public Lf/g/e/a;
.super Ljava/lang/Object;
.source "AbstractDataSource.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Z

.field public final synthetic e:Lf/g/e/f;

.field public final synthetic f:Z

.field public final synthetic g:Lf/g/e/c;


# direct methods
.method public constructor <init>(Lf/g/e/c;ZLf/g/e/f;Z)V
    .locals 0

    iput-object p1, p0, Lf/g/e/a;->g:Lf/g/e/c;

    iput-boolean p2, p0, Lf/g/e/a;->d:Z

    iput-object p3, p0, Lf/g/e/a;->e:Lf/g/e/f;

    iput-boolean p4, p0, Lf/g/e/a;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-boolean v0, p0, Lf/g/e/a;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/e/a;->e:Lf/g/e/f;

    iget-object v1, p0, Lf/g/e/a;->g:Lf/g/e/c;

    invoke-interface {v0, v1}, Lf/g/e/f;->onFailure(Lcom/facebook/datasource/DataSource;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lf/g/e/a;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/g/e/a;->e:Lf/g/e/f;

    iget-object v1, p0, Lf/g/e/a;->g:Lf/g/e/c;

    invoke-interface {v0, v1}, Lf/g/e/f;->onCancellation(Lcom/facebook/datasource/DataSource;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/g/e/a;->e:Lf/g/e/f;

    iget-object v1, p0, Lf/g/e/a;->g:Lf/g/e/c;

    invoke-interface {v0, v1}, Lf/g/e/f;->onNewResult(Lcom/facebook/datasource/DataSource;)V

    :goto_0
    return-void
.end method
