.class public Lf/g/d/b/b$b;
.super Ljava/lang/Object;
.source "ConstrainedExecutorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/d/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic d:Lf/g/d/b/b;


# direct methods
.method public constructor <init>(Lf/g/d/b/b;Lf/g/d/b/b$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v0, v0, Lf/g/d/b/b;->g:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    sget-object v0, Lf/g/d/b/b;->k:Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lf/g/d/b/b;->k:Ljava/lang/Class;

    :try_start_1
    iget-object v0, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v0, v0, Lf/g/d/b/b;->d:Ljava/lang/String;

    sget v0, Lf/g/d/e/a;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v0, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v0, v0, Lf/g/d/b/b;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    iget-object v0, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v0, v0, Lf/g/d/b/b;->g:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    invoke-virtual {v0}, Lf/g/d/b/b;->a()V

    goto :goto_1

    :cond_1
    sget-object v0, Lf/g/d/b/b;->k:Ljava/lang/Class;

    sget-object v0, Lf/g/d/b/b;->k:Ljava/lang/Class;

    iget-object v0, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v0, v0, Lf/g/d/b/b;->d:Ljava/lang/String;

    sget v0, Lf/g/d/e/a;->a:I

    :goto_1
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v1, v1, Lf/g/d/b/b;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    iget-object v1, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v1, v1, Lf/g/d/b/b;->g:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    invoke-virtual {v1}, Lf/g/d/b/b;->a()V

    goto :goto_2

    :cond_2
    sget-object v1, Lf/g/d/b/b;->k:Ljava/lang/Class;

    sget-object v1, Lf/g/d/b/b;->k:Ljava/lang/Class;

    iget-object v1, p0, Lf/g/d/b/b$b;->d:Lf/g/d/b/b;

    iget-object v1, v1, Lf/g/d/b/b;->d:Ljava/lang/String;

    sget v1, Lf/g/d/e/a;->a:I

    :goto_2
    throw v0
.end method
