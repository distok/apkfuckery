.class public Lf/g/m/f$c;
.super Lf/g/m/m$f;
.source "ExtractFromZipSoSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/m/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/m/f$c$a;
    }
.end annotation


# instance fields
.field public d:[Lf/g/m/f$b;

.field public final e:Ljava/util/zip/ZipFile;

.field public final f:Lf/g/m/m;

.field public final synthetic g:Lf/g/m/f;


# direct methods
.method public constructor <init>(Lf/g/m/f;Lf/g/m/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lf/g/m/f$c;->g:Lf/g/m/f;

    invoke-direct {p0}, Lf/g/m/m$f;-><init>()V

    new-instance v0, Ljava/util/zip/ZipFile;

    iget-object p1, p1, Lf/g/m/f;->f:Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lf/g/m/f$c;->e:Ljava/util/zip/ZipFile;

    iput-object p2, p0, Lf/g/m/f$c;->f:Lf/g/m/m;

    return-void
.end method


# virtual methods
.method public final a()Lf/g/m/m$c;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lf/g/m/m$c;

    invoke-virtual {p0}, Lf/g/m/f$c;->c()[Lf/g/m/f$b;

    move-result-object v1

    invoke-direct {v0, v1}, Lf/g/m/m$c;-><init>([Lf/g/m/m$b;)V

    return-object v0
.end method

.method public final b()Lf/g/m/m$e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lf/g/m/f$c$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf/g/m/f$c$a;-><init>(Lf/g/m/f$c;Lf/g/m/f$a;)V

    return-object v0
.end method

.method public final c()[Lf/g/m/f$b;
    .locals 14

    iget-object v0, p0, Lf/g/m/f$c;->d:[Lf/g/m/f$b;

    if-nez v0, :cond_d

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p0, Lf/g/m/f$c;->g:Lf/g/m/f;

    iget-object v2, v2, Lf/g/m/f;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-static {}, Lf/g/j/k/a;->f0()[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lf/g/m/f$c;->e:Ljava/util/zip/ZipFile;

    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/zip/ZipEntry;

    invoke-virtual {v5}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v9, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    :goto_1
    array-length v9, v3

    if-ge v7, v9, :cond_2

    aget-object v9, v3, v7

    if-eqz v9, :cond_1

    aget-object v9, v3, v7

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, -0x1

    :goto_2
    if-ltz v7, :cond_0

    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lf/g/m/f$b;

    if-eqz v8, :cond_3

    iget v8, v8, Lf/g/m/f$b;->g:I

    if-ge v7, v8, :cond_0

    :cond_3
    new-instance v8, Lf/g/m/f$b;

    invoke-direct {v8, v6, v5, v7}, Lf/g/m/f$b;-><init>(Ljava/lang/String;Ljava/util/zip/ZipEntry;I)V

    invoke-virtual {v1, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lf/g/m/f$c;->f:Lf/g/m/m;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [Lf/g/m/f$b;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/g/m/f$b;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-ge v1, v3, :cond_a

    aget-object v3, v0, v1

    iget-object v4, v3, Lf/g/m/f$b;->f:Ljava/util/zip/ZipEntry;

    iget-object v3, v3, Lf/g/m/m$b;->d:Ljava/lang/String;

    move-object v5, p0

    check-cast v5, Lf/g/m/a$a;

    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v5, Lf/g/m/a$a;->j:Lf/g/m/a;

    iget-object v10, v10, Lf/g/m/m;->d:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    const/4 v11, 0x0

    if-eqz v10, :cond_5

    iget-object v4, v5, Lf/g/m/a$a;->j:Lf/g/m/a;

    iput-object v11, v4, Lf/g/m/m;->d:Ljava/lang/String;

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v3, v4, v7

    const-string v3, "allowing consideration of corrupted lib %s"

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_5
    iget v10, v5, Lf/g/m/a$a;->i:I

    and-int/2addr v10, v8

    if-nez v10, :cond_6

    const-string v3, "allowing consideration of "

    const-string v4, ": self-extraction preferred"

    invoke-static {v3, v9, v4}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_6
    new-instance v10, Ljava/io/File;

    iget-object v5, v5, Lf/g/m/a$a;->h:Ljava/io/File;

    invoke-direct {v10, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v5

    if-nez v5, :cond_7

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v9, v4, v7

    aput-object v3, v4, v8

    const-string v3, "allowing considering of %s: %s not in system lib dir"

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_7
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getSize()J

    move-result-wide v3

    cmp-long v5, v12, v3

    if-eqz v5, :cond_8

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v10, v5, v7

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v6

    const-string v3, "allowing consideration of %s: sysdir file length is %s, but the file is %s bytes long in the APK"

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_4
    const/4 v4, 0x1

    goto :goto_5

    :cond_8
    const-string v3, "not allowing consideration of "

    const-string v4, ": deferring to libdir"

    invoke-static {v3, v9, v4}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    :goto_5
    const-string v5, "ApkSoSource"

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_9

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_9
    aput-object v11, v0, v1

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_a
    new-array v1, v2, [Lf/g/m/f$b;

    const/4 v2, 0x0

    :goto_7
    array-length v3, v0

    if-ge v7, v3, :cond_c

    aget-object v3, v0, v7

    if-eqz v3, :cond_b

    add-int/lit8 v4, v2, 0x1

    aput-object v3, v1, v2

    move v2, v4

    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_c
    iput-object v1, p0, Lf/g/m/f$c;->d:[Lf/g/m/f$b;

    :cond_d
    iget-object v0, p0, Lf/g/m/f$c;->d:[Lf/g/m/f$b;

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/g/m/f$c;->e:Ljava/util/zip/ZipFile;

    invoke-virtual {v0}, Ljava/util/zip/ZipFile;->close()V

    return-void
.end method
