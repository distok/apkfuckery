.class public final Lf/g/m/f$c$a;
.super Lf/g/m/m$e;
.source "ExtractFromZipSoSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/m/f$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public d:I

.field public final synthetic e:Lf/g/m/f$c;


# direct methods
.method public constructor <init>(Lf/g/m/f$c;Lf/g/m/f$a;)V
    .locals 0

    iput-object p1, p0, Lf/g/m/f$c$a;->e:Lf/g/m/f$c;

    invoke-direct {p0}, Lf/g/m/m$e;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    iget-object v0, p0, Lf/g/m/f$c$a;->e:Lf/g/m/f$c;

    invoke-virtual {v0}, Lf/g/m/f$c;->c()[Lf/g/m/f$b;

    iget v0, p0, Lf/g/m/f$c$a;->d:I

    iget-object v1, p0, Lf/g/m/f$c$a;->e:Lf/g/m/f$c;

    iget-object v1, v1, Lf/g/m/f$c;->d:[Lf/g/m/f$b;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b()Lf/g/m/m$d;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf/g/m/f$c$a;->e:Lf/g/m/f$c;

    invoke-virtual {v0}, Lf/g/m/f$c;->c()[Lf/g/m/f$b;

    iget-object v0, p0, Lf/g/m/f$c$a;->e:Lf/g/m/f$c;

    iget-object v1, v0, Lf/g/m/f$c;->d:[Lf/g/m/f$b;

    iget v2, p0, Lf/g/m/f$c$a;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lf/g/m/f$c$a;->d:I

    aget-object v1, v1, v2

    iget-object v0, v0, Lf/g/m/f$c;->e:Ljava/util/zip/ZipFile;

    iget-object v2, v1, Lf/g/m/f$b;->f:Ljava/util/zip/ZipEntry;

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    :try_start_0
    new-instance v2, Lf/g/m/m$d;

    invoke-direct {v2, v1, v0}, Lf/g/m/m$d;-><init>(Lf/g/m/m$b;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v2

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    throw v1
.end method
