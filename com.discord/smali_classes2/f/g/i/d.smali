.class public Lf/g/i/d;
.super Ljava/lang/Object;
.source "ImageFormatChecker.java"


# static fields
.field public static d:Lf/g/i/d;


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/g/i/c$a;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lf/g/i/c$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/g/i/a;

    invoke-direct {v0}, Lf/g/i/a;-><init>()V

    iput-object v0, p0, Lf/g/i/d;->c:Lf/g/i/c$a;

    invoke-virtual {p0}, Lf/g/i/d;->d()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lf/g/i/c;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lf/g/i/d;->c()Lf/g/i/d;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/g/i/c;->b:Lf/g/i/c;

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget v2, v0, Lf/g/i/d;->a:I

    new-array v3, v2, [B

    const/4 v4, 0x0

    if-lt v2, v2, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-static {v5}, Ls/a/b/b/a;->g(Z)V

    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v5

    if-eqz v5, :cond_1

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->mark(I)V

    invoke-static {p0, v3, v4, v2}, Ls/a/b/b/a;->V(Ljava/io/InputStream;[BII)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    throw v0

    :cond_1
    invoke-static {p0, v3, v4, v2}, Ls/a/b/b/a;->V(Ljava/io/InputStream;[BII)I

    move-result v2

    :goto_1
    iget-object p0, v0, Lf/g/i/d;->c:Lf/g/i/c$a;

    invoke-interface {p0, v3, v2}, Lf/g/i/c$a;->b([BI)Lf/g/i/c;

    move-result-object p0

    if-eqz p0, :cond_2

    if-eq p0, v1, :cond_2

    move-object v1, p0

    goto :goto_2

    :cond_2
    iget-object p0, v0, Lf/g/i/d;->b:Ljava/util/List;

    if-eqz p0, :cond_4

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/i/c$a;

    invoke-interface {v0, v3, v2}, Lf/g/i/c$a;->b([BI)Lf/g/i/c;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eq v0, v1, :cond_3

    move-object v1, v0

    :cond_4
    :goto_2
    return-object v1
.end method

.method public static b(Ljava/io/InputStream;)Lf/g/i/c;
    .locals 1

    :try_start_0
    invoke-static {p0}, Lf/g/i/d;->a(Ljava/io/InputStream;)Lf/g/i/c;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lf/g/d/d/m;->a(Ljava/lang/Throwable;)V

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static declared-synchronized c()Lf/g/i/d;
    .locals 2

    const-class v0, Lf/g/i/d;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf/g/i/d;->d:Lf/g/i/d;

    if-nez v1, :cond_0

    new-instance v1, Lf/g/i/d;

    invoke-direct {v1}, Lf/g/i/d;-><init>()V

    sput-object v1, Lf/g/i/d;->d:Lf/g/i/d;

    :cond_0
    sget-object v1, Lf/g/i/d;->d:Lf/g/i/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public final d()V
    .locals 3

    iget-object v0, p0, Lf/g/i/d;->c:Lf/g/i/c$a;

    invoke-interface {v0}, Lf/g/i/c$a;->a()I

    move-result v0

    iput v0, p0, Lf/g/i/d;->a:I

    iget-object v0, p0, Lf/g/i/d;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/i/c$a;

    iget v2, p0, Lf/g/i/d;->a:I

    invoke-interface {v1}, Lf/g/i/c$a;->a()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lf/g/i/d;->a:I

    goto :goto_0

    :cond_0
    return-void
.end method
