.class public final Lf/g/i/b;
.super Ljava/lang/Object;
.source "DefaultImageFormats.java"


# static fields
.field public static final a:Lf/g/i/c;

.field public static final b:Lf/g/i/c;

.field public static final c:Lf/g/i/c;

.field public static final d:Lf/g/i/c;

.field public static final e:Lf/g/i/c;

.field public static final f:Lf/g/i/c;

.field public static final g:Lf/g/i/c;

.field public static final h:Lf/g/i/c;

.field public static final i:Lf/g/i/c;

.field public static final j:Lf/g/i/c;

.field public static final k:Lf/g/i/c;

.field public static final l:Lf/g/i/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lf/g/i/c;

    const-string v1, "JPEG"

    const-string v2, "jpeg"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->a:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "PNG"

    const-string v2, "png"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->b:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "GIF"

    const-string v2, "gif"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->c:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "BMP"

    const-string v2, "bmp"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->d:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "ICO"

    const-string v2, "ico"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->e:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "WEBP_SIMPLE"

    const-string v2, "webp"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->f:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "WEBP_LOSSLESS"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->g:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "WEBP_EXTENDED"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->h:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "WEBP_EXTENDED_WITH_ALPHA"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->i:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "WEBP_ANIMATED"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->j:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "HEIF"

    const-string v2, "heif"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->k:Lf/g/i/c;

    new-instance v0, Lf/g/i/c;

    const-string v1, "DNG"

    const-string v2, "dng"

    invoke-direct {v0, v1, v2}, Lf/g/i/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lf/g/i/b;->l:Lf/g/i/c;

    return-void
.end method

.method public static a(Lf/g/i/c;)Z
    .locals 1

    sget-object v0, Lf/g/i/b;->f:Lf/g/i/c;

    if-eq p0, v0, :cond_1

    sget-object v0, Lf/g/i/b;->g:Lf/g/i/c;

    if-eq p0, v0, :cond_1

    sget-object v0, Lf/g/i/b;->h:Lf/g/i/c;

    if-eq p0, v0, :cond_1

    sget-object v0, Lf/g/i/b;->i:Lf/g/i/c;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
