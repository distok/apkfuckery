.class public Lf/g/i/a;
.super Ljava/lang/Object;
.source "DefaultImageFormatChecker.java"

# interfaces
.implements Lf/g/i/c$a;


# static fields
.field public static final b:[B

.field public static final c:I

.field public static final d:[B

.field public static final e:I

.field public static final f:[B

.field public static final g:[B

.field public static final h:[B

.field public static final i:I

.field public static final j:[B

.field public static final k:I

.field public static final l:[B

.field public static final m:[[B

.field public static final n:[B

.field public static final o:[B

.field public static final p:I


# instance fields
.field public final a:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lf/g/i/a;->b:[B

    array-length v1, v1

    sput v1, Lf/g/i/a;->c:I

    const/16 v1, 0x8

    new-array v1, v1, [B

    fill-array-data v1, :array_1

    sput-object v1, Lf/g/i/a;->d:[B

    array-length v1, v1

    sput v1, Lf/g/i/a;->e:I

    const-string v1, "GIF87a"

    invoke-static {v1}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v1

    sput-object v1, Lf/g/i/a;->f:[B

    const-string v1, "GIF89a"

    invoke-static {v1}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v1

    sput-object v1, Lf/g/i/a;->g:[B

    const-string v1, "BM"

    invoke-static {v1}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v1

    sput-object v1, Lf/g/i/a;->h:[B

    array-length v1, v1

    sput v1, Lf/g/i/a;->i:I

    const/4 v1, 0x4

    new-array v2, v1, [B

    fill-array-data v2, :array_2

    sput-object v2, Lf/g/i/a;->j:[B

    array-length v2, v2

    sput v2, Lf/g/i/a;->k:I

    const-string v2, "ftyp"

    invoke-static {v2}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v2

    sput-object v2, Lf/g/i/a;->l:[B

    const/4 v2, 0x6

    new-array v2, v2, [[B

    const-string v3, "heic"

    invoke-static {v3}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "heix"

    invoke-static {v3}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "hevc"

    invoke-static {v3}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "hevx"

    invoke-static {v3}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v3

    aput-object v3, v2, v0

    const-string v0, "mif1"

    invoke-static {v0}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    aput-object v0, v2, v1

    const-string v0, "msf1"

    invoke-static {v0}, Ls/a/b/b/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v3, 0x5

    aput-object v0, v2, v3

    sput-object v2, Lf/g/i/a;->m:[[B

    new-array v0, v1, [B

    fill-array-data v0, :array_3

    sput-object v0, Lf/g/i/a;->n:[B

    new-array v1, v1, [B

    fill-array-data v1, :array_4

    sput-object v1, Lf/g/i/a;->o:[B

    array-length v0, v0

    sput v0, Lf/g/i/a;->p:I

    return-void

    :array_0
    .array-data 1
        -0x1t
        -0x28t
        -0x1t
    .end array-data

    :array_1
    .array-data 1
        -0x77t
        0x50t
        0x4et
        0x47t
        0xdt
        0xat
        0x1at
        0xat
    .end array-data

    :array_2
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x49t
        0x49t
        0x2at
        0x0t
    .end array-data

    :array_4
    .array-data 1
        0x4dt
        0x4dt
        0x0t
        0x2at
    .end array-data
.end method

.method public constructor <init>()V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/16 v2, 0x15

    const/4 v3, 0x0

    aput v2, v1, v3

    const/16 v2, 0x14

    const/4 v4, 0x1

    aput v2, v1, v4

    sget v2, Lf/g/i/a;->c:I

    const/4 v5, 0x2

    aput v2, v1, v5

    sget v2, Lf/g/i/a;->e:I

    const/4 v5, 0x3

    aput v2, v1, v5

    const/4 v2, 0x4

    const/4 v5, 0x6

    aput v5, v1, v2

    sget v2, Lf/g/i/a;->i:I

    const/4 v6, 0x5

    aput v2, v1, v6

    sget v2, Lf/g/i/a;->k:I

    aput v2, v1, v5

    const/4 v2, 0x7

    const/16 v5, 0xc

    aput v5, v1, v2

    invoke-static {v4}, Ls/a/b/b/a;->g(Z)V

    aget v2, v1, v3

    :goto_0
    if-ge v4, v0, :cond_1

    aget v3, v1, v4

    if-le v3, v2, :cond_0

    aget v2, v1, v4

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iput v2, p0, Lf/g/i/a;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lf/g/i/a;->a:I

    return v0
.end method

.method public final b([BI)Lf/g/i/c;
    .locals 8

    sget-object v0, Lf/g/i/c;->b:Lf/g/i/c;

    const/4 v1, 0x0

    invoke-static {p1, v1, p2}, Lf/g/d/m/c;->b([BII)Z

    move-result v2

    const/4 v3, 0x1

    const/16 v4, 0xc

    if-eqz v2, :cond_a

    invoke-static {p1, v1, p2}, Lf/g/d/m/c;->b([BII)Z

    move-result v2

    invoke-static {v2}, Ls/a/b/b/a;->g(Z)V

    sget-object v2, Lf/g/d/m/c;->e:[B

    invoke-static {p1, v4, v2}, Lf/g/d/m/c;->d([BI[B)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lf/g/i/b;->f:Lf/g/i/c;

    goto :goto_4

    :cond_0
    sget-object v2, Lf/g/d/m/c;->f:[B

    invoke-static {p1, v4, v2}, Lf/g/d/m/c;->d([BI[B)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lf/g/i/b;->g:Lf/g/i/c;

    goto :goto_4

    :cond_1
    const/16 v2, 0x15

    if-lt p2, v2, :cond_2

    sget-object p2, Lf/g/d/m/c;->g:[B

    invoke-static {p1, v4, p2}, Lf/g/d/m/c;->d([BI[B)Z

    move-result p2

    if-eqz p2, :cond_2

    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_9

    sget-object p2, Lf/g/d/m/c;->g:[B

    invoke-static {p1, v4, p2}, Lf/g/d/m/c;->d([BI[B)Z

    move-result v0

    const/16 v2, 0x14

    aget-byte v5, p1, v2

    const/4 v6, 0x2

    and-int/2addr v5, v6

    if-ne v5, v6, :cond_3

    const/4 v5, 0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_1
    if-eqz v0, :cond_4

    if-eqz v5, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    sget-object v0, Lf/g/i/b;->j:Lf/g/i/c;

    goto :goto_4

    :cond_5
    invoke-static {p1, v4, p2}, Lf/g/d/m/c;->d([BI[B)Z

    move-result p2

    aget-byte p1, p1, v2

    const/16 v0, 0x10

    and-int/2addr p1, v0

    if-ne p1, v0, :cond_6

    const/4 p1, 0x1

    goto :goto_3

    :cond_6
    const/4 p1, 0x0

    :goto_3
    if-eqz p2, :cond_7

    if-eqz p1, :cond_7

    const/4 v1, 0x1

    :cond_7
    if-eqz v1, :cond_8

    sget-object v0, Lf/g/i/b;->i:Lf/g/i/c;

    goto :goto_4

    :cond_8
    sget-object v0, Lf/g/i/b;->h:Lf/g/i/c;

    :cond_9
    :goto_4
    return-object v0

    :cond_a
    sget-object v2, Lf/g/i/a;->b:[B

    array-length v5, v2

    if-lt p2, v5, :cond_b

    invoke-static {p1, v2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    goto :goto_5

    :cond_b
    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_c

    sget-object p1, Lf/g/i/b;->a:Lf/g/i/c;

    return-object p1

    :cond_c
    sget-object v2, Lf/g/i/a;->d:[B

    array-length v5, v2

    if-lt p2, v5, :cond_d

    invoke-static {p1, v2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    if-eqz v2, :cond_d

    const/4 v2, 0x1

    goto :goto_6

    :cond_d
    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_e

    sget-object p1, Lf/g/i/b;->b:Lf/g/i/c;

    return-object p1

    :cond_e
    const/4 v2, 0x6

    if-ge p2, v2, :cond_f

    goto :goto_7

    :cond_f
    sget-object v2, Lf/g/i/a;->f:[B

    invoke-static {p1, v2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    if-nez v2, :cond_11

    sget-object v2, Lf/g/i/a;->g:[B

    invoke-static {p1, v2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    if-eqz v2, :cond_10

    goto :goto_8

    :cond_10
    :goto_7
    const/4 v2, 0x0

    goto :goto_9

    :cond_11
    :goto_8
    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_12

    sget-object p1, Lf/g/i/b;->c:Lf/g/i/c;

    return-object p1

    :cond_12
    sget-object v2, Lf/g/i/a;->h:[B

    array-length v5, v2

    if-ge p2, v5, :cond_13

    const/4 v2, 0x0

    goto :goto_a

    :cond_13
    invoke-static {p1, v2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    :goto_a
    if-eqz v2, :cond_14

    sget-object p1, Lf/g/i/b;->d:Lf/g/i/c;

    return-object p1

    :cond_14
    sget-object v2, Lf/g/i/a;->j:[B

    array-length v5, v2

    if-ge p2, v5, :cond_15

    const/4 v2, 0x0

    goto :goto_b

    :cond_15
    invoke-static {p1, v2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    :goto_b
    if-eqz v2, :cond_16

    sget-object p1, Lf/g/i/b;->e:Lf/g/i/c;

    return-object p1

    :cond_16
    if-ge p2, v4, :cond_17

    goto :goto_d

    :cond_17
    const/4 v2, 0x3

    aget-byte v2, p1, v2

    const/16 v4, 0x8

    if-ge v2, v4, :cond_18

    goto :goto_d

    :cond_18
    sget-object v2, Lf/g/i/a;->l:[B

    const/4 v5, 0x4

    invoke-static {p1, v2, v5}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v2

    if-nez v2, :cond_19

    goto :goto_d

    :cond_19
    sget-object v2, Lf/g/i/a;->m:[[B

    array-length v5, v2

    const/4 v6, 0x0

    :goto_c
    if-ge v6, v5, :cond_1b

    aget-object v7, v2, v6

    invoke-static {p1, v7, v4}, Ls/a/b/b/a;->D([B[BI)Z

    move-result v7

    if-eqz v7, :cond_1a

    const/4 v2, 0x1

    goto :goto_e

    :cond_1a
    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    :cond_1b
    :goto_d
    const/4 v2, 0x0

    :goto_e
    if-eqz v2, :cond_1c

    sget-object p1, Lf/g/i/b;->k:Lf/g/i/c;

    return-object p1

    :cond_1c
    sget v2, Lf/g/i/a;->p:I

    if-lt p2, v2, :cond_1e

    sget-object p2, Lf/g/i/a;->n:[B

    invoke-static {p1, p2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result p2

    if-nez p2, :cond_1d

    sget-object p2, Lf/g/i/a;->o:[B

    invoke-static {p1, p2, v1}, Ls/a/b/b/a;->D([B[BI)Z

    move-result p1

    if-eqz p1, :cond_1e

    :cond_1d
    const/4 v1, 0x1

    :cond_1e
    if-eqz v1, :cond_1f

    sget-object p1, Lf/g/i/b;->l:Lf/g/i/c;

    return-object p1

    :cond_1f
    return-object v0
.end method
