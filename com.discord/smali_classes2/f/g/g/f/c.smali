.class public Lf/g/g/f/c;
.super Ljava/lang/Object;
.source "RoundingParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/g/f/c$a;
    }
.end annotation


# instance fields
.field public a:Lf/g/g/f/c$a;

.field public b:Z

.field public c:[F

.field public d:I

.field public e:F

.field public f:I

.field public g:F

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/g/g/f/c$a;->e:Lf/g/g/f/c$a;

    iput-object v0, p0, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/g/f/c;->b:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lf/g/g/f/c;->c:[F

    iput v0, p0, Lf/g/g/f/c;->d:I

    const/4 v1, 0x0

    iput v1, p0, Lf/g/g/f/c;->e:F

    iput v0, p0, Lf/g/g/f/c;->f:I

    iput v1, p0, Lf/g/g/f/c;->g:F

    iput-boolean v0, p0, Lf/g/g/f/c;->h:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_9

    const-class v1, Lf/g/g/f/c;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lf/g/g/f/c;

    iget-boolean v1, p0, Lf/g/g/f/c;->b:Z

    iget-boolean v2, p1, Lf/g/g/f/c;->b:Z

    if-eq v1, v2, :cond_2

    return v0

    :cond_2
    iget v1, p0, Lf/g/g/f/c;->d:I

    iget v2, p1, Lf/g/g/f/c;->d:I

    if-eq v1, v2, :cond_3

    return v0

    :cond_3
    iget v1, p1, Lf/g/g/f/c;->e:F

    iget v2, p0, Lf/g/g/f/c;->e:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_4

    return v0

    :cond_4
    iget v1, p0, Lf/g/g/f/c;->f:I

    iget v2, p1, Lf/g/g/f/c;->f:I

    if-eq v1, v2, :cond_5

    return v0

    :cond_5
    iget v1, p1, Lf/g/g/f/c;->g:F

    iget v2, p0, Lf/g/g/f/c;->g:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_6

    return v0

    :cond_6
    iget-object v1, p0, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    iget-object v2, p1, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    if-eq v1, v2, :cond_7

    return v0

    :cond_7
    iget-boolean v1, p0, Lf/g/g/f/c;->h:Z

    iget-boolean v2, p1, Lf/g/g/f/c;->h:Z

    if-eq v1, v2, :cond_8

    return v0

    :cond_8
    iget-object v0, p0, Lf/g/g/f/c;->c:[F

    iget-object p1, p1, Lf/g/g/f/c;->c:[F

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result p1

    return p1

    :cond_9
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 5

    iget-object v0, p0, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Enum;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lf/g/g/f/c;->b:Z

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lf/g/g/f/c;->c:[F

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([F)I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lf/g/g/f/c;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lf/g/g/f/c;->e:F

    const/4 v3, 0x0

    cmpl-float v4, v2, v3

    if-eqz v4, :cond_2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lf/g/g/f/c;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lf/g/g/f/c;->g:F

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_3

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lf/g/g/f/c;->h:Z

    add-int/2addr v0, v1

    return v0
.end method
