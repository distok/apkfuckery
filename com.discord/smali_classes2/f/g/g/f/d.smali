.class public Lf/g/g/f/d;
.super Ljava/lang/Object;
.source "WrappingUtils.java"


# static fields
.field public static final a:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2

    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v0, Lf/g/g/e/k;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object p0

    invoke-direct {v0, p2, v1, p0}, Lf/g/g/e/k;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    invoke-static {v0, p1}, Lf/g/g/f/d;->b(Lf/g/g/e/j;Lf/g/g/f/c;)V

    return-object v0

    :cond_0
    instance-of p2, p0, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz p2, :cond_1

    check-cast p0, Landroid/graphics/drawable/NinePatchDrawable;

    new-instance p2, Lf/g/g/e/o;

    invoke-direct {p2, p0}, Lf/g/g/e/o;-><init>(Landroid/graphics/drawable/NinePatchDrawable;)V

    invoke-static {p2, p1}, Lf/g/g/f/d;->b(Lf/g/g/e/j;Lf/g/g/f/c;)V

    return-object p2

    :cond_1
    instance-of p2, p0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz p2, :cond_2

    check-cast p0, Landroid/graphics/drawable/ColorDrawable;

    new-instance p2, Lf/g/g/e/l;

    invoke-virtual {p0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result p0

    invoke-direct {p2, p0}, Lf/g/g/e/l;-><init>(I)V

    invoke-static {p2, p1}, Lf/g/g/f/d;->b(Lf/g/g/e/j;Lf/g/g/f/c;)V

    return-object p2

    :cond_2
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object p0, p1, p2

    const-string p2, "WrappingUtils"

    const-string v0, "Don\'t know how to round that drawable: %s"

    invoke-static {p2, v0, p1}, Lf/g/d/e/a;->o(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p0
.end method

.method public static b(Lf/g/g/e/j;Lf/g/g/f/c;)V
    .locals 2

    iget-boolean v0, p1, Lf/g/g/f/c;->b:Z

    invoke-interface {p0, v0}, Lf/g/g/e/j;->c(Z)V

    iget-object v0, p1, Lf/g/g/f/c;->c:[F

    invoke-interface {p0, v0}, Lf/g/g/e/j;->m([F)V

    iget v0, p1, Lf/g/g/f/c;->f:I

    iget v1, p1, Lf/g/g/f/c;->e:F

    invoke-interface {p0, v0, v1}, Lf/g/g/e/j;->a(IF)V

    iget v0, p1, Lf/g/g/f/c;->g:F

    invoke-interface {p0, v0}, Lf/g/g/e/j;->i(F)V

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lf/g/g/e/j;->f(Z)V

    iget-boolean p1, p1, Lf/g/g/f/c;->h:Z

    invoke-interface {p0, p1}, Lf/g/g/e/j;->e(Z)V

    return-void
.end method

.method public static c(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 3

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    iget-object v0, p1, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    sget-object v1, Lf/g/g/f/c$a;->e:Lf/g/g/f/c$a;

    if-eq v0, v1, :cond_0

    goto :goto_2

    :cond_0
    instance-of v0, p0, Lf/g/g/e/g;

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lf/g/g/e/g;

    :goto_0
    invoke-interface {v0}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eq v1, v0, :cond_2

    instance-of v2, v1, Lf/g/g/e/d;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v1

    check-cast v0, Lf/g/g/e/d;

    goto :goto_0

    :cond_2
    :goto_1
    sget-object v1, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {v0, v1}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lf/g/g/f/d;->a(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-interface {v0, p1}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p0

    :cond_3
    :try_start_1
    invoke-static {p0, p1, p2}, Lf/g/g/f/d;->a(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p0

    :cond_4
    :goto_2
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p0

    :catchall_0
    move-exception p0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p0
.end method

.method public static d(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;)Landroid/graphics/drawable/Drawable;
    .locals 2

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p1, Lf/g/g/f/c;->a:Lf/g/g/f/c$a;

    sget-object v1, Lf/g/g/f/c$a;->d:Lf/g/g/f/c$a;

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lf/g/g/e/m;

    invoke-direct {v0, p0}, Lf/g/g/e/m;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-static {v0, p1}, Lf/g/g/f/d;->b(Lf/g/g/e/j;Lf/g/g/f/c;)V

    iget p0, p1, Lf/g/g/f/c;->d:I

    iput p0, v0, Lf/g/g/e/m;->r:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :cond_1
    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p0

    :catchall_0
    move-exception p0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p0
.end method

.method public static e(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    if-eqz p0, :cond_4

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Lf/g/g/e/p;

    invoke-direct {v0, p0, p1}, Lf/g/g/e/p;-><init>(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V

    if-eqz p2, :cond_3

    iget-object p0, v0, Lf/g/g/e/p;->j:Landroid/graphics/PointF;

    invoke-static {p0, p2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    iget-object p0, v0, Lf/g/g/e/p;->j:Landroid/graphics/PointF;

    if-nez p0, :cond_2

    new-instance p0, Landroid/graphics/PointF;

    invoke-direct {p0}, Landroid/graphics/PointF;-><init>()V

    iput-object p0, v0, Lf/g/g/e/p;->j:Landroid/graphics/PointF;

    :cond_2
    iget-object p0, v0, Lf/g/g/e/p;->j:Landroid/graphics/PointF;

    invoke-virtual {p0, p2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    invoke-virtual {v0}, Lf/g/g/e/p;->p()V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_3
    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :cond_4
    :goto_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p0
.end method
