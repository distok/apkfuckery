.class public Lf/g/g/f/a;
.super Ljava/lang/Object;
.source "GenericDraweeHierarchyBuilder.java"


# static fields
.field public static final q:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final r:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:I

.field public c:F

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public o:Landroid/graphics/drawable/Drawable;

.field public p:Lf/g/g/f/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/t;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lf/g/g/f/a;->q:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/s;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lf/g/g/f/a;->r:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/g/f/a;->a:Landroid/content/res/Resources;

    const/16 p1, 0x12c

    iput p1, p0, Lf/g/g/f/a;->b:I

    const/4 p1, 0x0

    iput p1, p0, Lf/g/g/f/a;->c:F

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/g/f/a;->d:Landroid/graphics/drawable/Drawable;

    sget-object v0, Lf/g/g/f/a;->q:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object v0, p0, Lf/g/g/f/a;->e:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object p1, p0, Lf/g/g/f/a;->f:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lf/g/g/f/a;->g:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object p1, p0, Lf/g/g/f/a;->h:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lf/g/g/f/a;->i:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object p1, p0, Lf/g/g/f/a;->j:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lf/g/g/f/a;->k:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/f/a;->r:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object v0, p0, Lf/g/g/f/a;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object p1, p0, Lf/g/g/f/a;->m:Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lf/g/g/f/a;->n:Ljava/util/List;

    iput-object p1, p0, Lf/g/g/f/a;->o:Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lf/g/g/f/a;->p:Lf/g/g/f/c;

    return-void
.end method


# virtual methods
.method public a()Lcom/facebook/drawee/generic/GenericDraweeHierarchy;
    .locals 2

    iget-object v0, p0, Lf/g/g/f/a;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    invoke-direct {v0, p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;-><init>(Lf/g/g/f/a;)V

    return-object v0
.end method

.method public b(Landroid/graphics/drawable/Drawable;)Lf/g/g/f/a;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/g/f/a;->n:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lf/g/g/f/a;->n:Ljava/util/List;

    :goto_0
    return-object p0
.end method
