.class public Lf/g/g/f/b;
.super Lf/g/g/e/g;
.source "RootDrawable.java"

# interfaces
.implements Lf/g/g/e/f0;


# instance fields
.field public h:Landroid/graphics/drawable/Drawable;

.field public i:Lf/g/g/e/g0;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/g/g/e/g;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lf/g/g/f/b;->h:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lf/g/g/f/b;->i:Lf/g/g/e/g0;

    if-eqz v0, :cond_2

    check-cast v0, Lcom/facebook/drawee/view/DraweeHolder;

    iget-boolean v1, v0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const-class v1, Lf/g/g/b/c;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, v0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeHolder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    const-string v3, "%x: Draw requested for a non-attached controller %x. %s"

    invoke-static {v1, v3, v2}, Lf/g/d/e/a;->m(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v4, v0, Lcom/facebook/drawee/view/DraweeHolder;->b:Z

    iput-boolean v4, v0, Lcom/facebook/drawee/view/DraweeHolder;->c:Z

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeHolder;->b()V

    :cond_2
    :goto_0
    iget-object v0, p0, Lf/g/g/e/g;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    iget-object v0, p0, Lf/g/g/f/b;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lf/g/g/f/b;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public k(Lf/g/g/e/g0;)V
    .locals 0

    iput-object p1, p0, Lf/g/g/f/b;->i:Lf/g/g/e/g0;

    return-void
.end method

.method public setVisible(ZZ)Z
    .locals 1

    iget-object v0, p0, Lf/g/g/f/b;->i:Lf/g/g/e/g0;

    if-eqz v0, :cond_0

    check-cast v0, Lcom/facebook/drawee/view/DraweeHolder;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeHolder;->f(Z)V

    :cond_0
    invoke-super {p0, p1, p2}, Lf/g/g/e/g;->setVisible(ZZ)Z

    move-result p1

    return p1
.end method
