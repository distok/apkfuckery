.class public Lf/g/g/a/a/d;
.super Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;
.source "PipelineDraweeControllerBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder<",
        "Lf/g/g/a/a/d;",
        "Lcom/facebook/imagepipeline/request/ImageRequest;",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final o:Lf/g/j/e/i;

.field public final p:Lf/g/g/a/a/f;

.field public q:Lf/g/g/a/a/h/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/g/g/a/a/f;Lf/g/j/e/i;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lf/g/g/a/a/f;",
            "Lf/g/j/e/i;",
            "Ljava/util/Set<",
            "Lcom/facebook/drawee/controller/ControllerListener;",
            ">;",
            "Ljava/util/Set<",
            "Lf/g/h/b/a/b;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p4, p5}, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Set;)V

    iput-object p3, p0, Lf/g/g/a/a/d;->o:Lf/g/j/e/i;

    iput-object p2, p0, Lf/g/g/a/a/d;->p:Lf/g/g/a/a/f;

    return-void
.end method


# virtual methods
.method public b(Lcom/facebook/drawee/interfaces/DraweeController;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder$b;)Lcom/facebook/datasource/DataSource;
    .locals 6

    move-object v1, p3

    check-cast v1, Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v0, p0, Lf/g/g/a/a/d;->o:Lf/g/j/e/i;

    invoke-virtual {p5}, Ljava/lang/Enum;->ordinal()I

    move-result p3

    if-eqz p3, :cond_2

    const/4 v2, 0x1

    if-eq p3, v2, :cond_1

    const/4 v2, 0x2

    if-ne p3, v2, :cond_0

    sget-object p3, Lcom/facebook/imagepipeline/request/ImageRequest$c;->g:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Cache level"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, "is not supported. "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    sget-object p3, Lcom/facebook/imagepipeline/request/ImageRequest$c;->e:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    goto :goto_0

    :cond_2
    sget-object p3, Lcom/facebook/imagepipeline/request/ImageRequest$c;->d:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    :goto_0
    move-object v3, p3

    instance-of p3, p1, Lf/g/g/a/a/c;

    const/4 p5, 0x0

    if-eqz p3, :cond_6

    check-cast p1, Lf/g/g/a/a/c;

    monitor-enter p1

    :try_start_0
    iget-object p3, p1, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    if-eqz p3, :cond_3

    new-instance p5, Lf/g/g/a/a/h/c;

    iget-object v2, p1, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-direct {p5, v2, p3}, Lf/g/g/a/a/h/c;-><init>(Ljava/lang/String;Lf/g/g/a/a/h/b;)V

    :cond_3
    iget-object p3, p1, Lf/g/g/a/a/c;->F:Ljava/util/Set;

    if-eqz p3, :cond_5

    new-instance v2, Lf/g/j/l/c;

    invoke-direct {v2, p3}, Lf/g/j/l/c;-><init>(Ljava/util/Set;)V

    if-eqz p5, :cond_4

    iget-object p3, v2, Lf/g/j/l/c;->a:Ljava/util/List;

    invoke-interface {p3, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    monitor-exit p1

    move-object v4, v2

    goto :goto_2

    :cond_5
    monitor-exit p1

    goto :goto_1

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_6
    :goto_1
    move-object v4, p5

    :goto_2
    move-object v2, p4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lf/g/j/e/i;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Lcom/facebook/imagepipeline/request/ImageRequest$c;Lf/g/j/l/e;Ljava/lang/String;)Lcom/facebook/datasource/DataSource;

    move-result-object p1

    return-object p1
.end method

.method public d()Lcom/facebook/drawee/controller/AbstractDraweeController;
    .locals 13

    invoke-static {}, Lf/g/j/s/b;->b()Z

    :try_start_0
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->k:Lcom/facebook/drawee/interfaces/DraweeController;

    sget-object v1, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->n:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    instance-of v1, v0, Lf/g/g/a/a/c;

    if-eqz v1, :cond_0

    check-cast v0, Lf/g/g/a/a/c;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/g/a/a/d;->p:Lf/g/g/a/a/f;

    iget-object v7, v0, Lf/g/g/a/a/f;->a:Landroid/content/res/Resources;

    iget-object v8, v0, Lf/g/g/a/a/f;->b:Lf/g/g/b/a;

    iget-object v9, v0, Lf/g/g/a/a/f;->c:Lf/g/j/i/a;

    iget-object v10, v0, Lf/g/g/a/a/f;->d:Ljava/util/concurrent/Executor;

    iget-object v11, v0, Lf/g/g/a/a/f;->e:Lf/g/j/c/t;

    iget-object v12, v0, Lf/g/g/a/a/f;->f:Lf/g/d/d/e;

    new-instance v1, Lf/g/g/a/a/c;

    move-object v6, v1

    invoke-direct/range {v6 .. v12}, Lf/g/g/a/a/c;-><init>(Landroid/content/res/Resources;Lf/g/g/b/a;Lf/g/j/i/a;Ljava/util/concurrent/Executor;Lf/g/j/c/t;Lf/g/d/d/e;)V

    iget-object v0, v0, Lf/g/g/a/a/f;->g:Lcom/facebook/common/internal/Supplier;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v1, Lf/g/g/a/a/c;->C:Z

    :cond_1
    move-object v0, v1

    :goto_0
    invoke-virtual {p0, v0, v5}, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e(Lcom/facebook/drawee/interfaces/DraweeController;Ljava/lang/String;)Lcom/facebook/common/internal/Supplier;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v2, p0, Lf/g/g/a/a/d;->o:Lf/g/j/e/i;

    iget-object v2, v2, Lf/g/j/e/i;->g:Lf/g/j/c/j;

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    iget-object v3, v1, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->d:Ljava/lang/Object;

    check-cast v2, Lf/g/j/c/n;

    invoke-virtual {v2, v1, v3}, Lf/g/j/c/n;->c(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->d:Ljava/lang/Object;

    check-cast v2, Lf/g/j/c/n;

    invoke-virtual {v2, v1, v3}, Lf/g/j/c/n;->a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;)Lcom/facebook/cache/common/CacheKey;

    move-result-object v1

    :goto_1
    move-object v6, v1

    goto :goto_2

    :cond_3
    move-object v6, v3

    :goto_2
    iget-object v7, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->d:Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, v0

    invoke-virtual/range {v3 .. v9}, Lf/g/g/a/a/c;->H(Lcom/facebook/common/internal/Supplier;Ljava/lang/String;Lcom/facebook/cache/common/CacheKey;Ljava/lang/Object;Lf/g/d/d/e;Lf/g/g/a/a/h/b;)V

    iget-object v1, p0, Lf/g/g/a/a/d;->q:Lf/g/g/a/a/h/e;

    sget-object v2, Lf/g/d/d/l;->a:Lcom/facebook/common/internal/Supplier;

    invoke-virtual {v0, v1, p0, v2}, Lf/g/g/a/a/c;->I(Lf/g/g/a/a/h/e;Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;Lcom/facebook/common/internal/Supplier;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw v0
.end method

.method public f(Landroid/net/Uri;)Lf/g/g/a/a/d;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    return-object p0

    :cond_0
    invoke-static {p1}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object p1

    sget-object v0, Lf/g/j/d/f;->d:Lf/g/j/d/f;

    iput-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->d:Lf/g/j/d/f;

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    return-object p0
.end method

.method public g(Ljava/lang/String;)Lf/g/g/a/a/d;
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b(Landroid/net/Uri;)Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a()Lcom/facebook/imagepipeline/request/ImageRequest;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    return-object p0

    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/g/g/a/a/d;->f(Landroid/net/Uri;)Lf/g/g/a/a/d;

    move-result-object p1

    return-object p1
.end method
