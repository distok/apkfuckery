.class public Lf/g/g/a/a/e;
.super Ljava/lang/Object;
.source "PipelineDraweeControllerBuilderSupplier.java"

# interfaces
.implements Lcom/facebook/common/internal/Supplier;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/internal/Supplier<",
        "Lf/g/g/a/a/d;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/g/j/e/i;

.field public final c:Lf/g/g/a/a/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    sget-object v0, Lf/g/j/e/m;->t:Lf/g/j/e/m;

    const-string v1, "ImagePipelineFactory was not initialized!"

    invoke-static {v0, v1}, Ls/a/b/b/a;->i(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/g/a/a/e;->a:Landroid/content/Context;

    iget-object v1, v0, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lf/g/j/e/m;->a()Lf/g/j/e/i;

    move-result-object v1

    iput-object v1, v0, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    :cond_0
    iget-object v1, v0, Lf/g/j/e/m;->k:Lf/g/j/e/i;

    iput-object v1, p0, Lf/g/g/a/a/e;->b:Lf/g/j/e/i;

    new-instance v2, Lf/g/g/a/a/f;

    invoke-direct {v2}, Lf/g/g/a/a/f;-><init>()V

    iput-object v2, p0, Lf/g/g/a/a/e;->c:Lf/g/g/a/a/f;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-class v4, Lf/g/g/b/a;

    monitor-enter v4

    :try_start_0
    sget-object v5, Lf/g/g/b/a;->a:Lf/g/g/b/a;

    if-nez v5, :cond_1

    new-instance v5, Lf/g/g/b/b;

    invoke-direct {v5}, Lf/g/g/b/b;-><init>()V

    sput-object v5, Lf/g/g/b/a;->a:Lf/g/g/b/a;

    :cond_1
    sget-object v5, Lf/g/g/b/a;->a:Lf/g/g/b/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v4

    invoke-virtual {v0}, Lf/g/j/e/m;->b()Lf/g/j/a/b/a;

    move-result-object v0

    const/4 v4, 0x0

    if-nez v0, :cond_2

    move-object p1, v4

    goto :goto_0

    :cond_2
    invoke-interface {v0, p1}, Lf/g/j/a/b/a;->a(Landroid/content/Context;)Lf/g/j/i/a;

    move-result-object p1

    :goto_0
    sget-object v0, Lf/g/d/b/f;->e:Lf/g/d/b/f;

    if-nez v0, :cond_3

    new-instance v0, Lf/g/d/b/f;

    invoke-direct {v0}, Lf/g/d/b/f;-><init>()V

    sput-object v0, Lf/g/d/b/f;->e:Lf/g/d/b/f;

    :cond_3
    sget-object v0, Lf/g/d/b/f;->e:Lf/g/d/b/f;

    iget-object v1, v1, Lf/g/j/e/i;->e:Lf/g/j/c/t;

    iput-object v3, v2, Lf/g/g/a/a/f;->a:Landroid/content/res/Resources;

    iput-object v5, v2, Lf/g/g/a/a/f;->b:Lf/g/g/b/a;

    iput-object p1, v2, Lf/g/g/a/a/f;->c:Lf/g/j/i/a;

    iput-object v0, v2, Lf/g/g/a/a/f;->d:Ljava/util/concurrent/Executor;

    iput-object v1, v2, Lf/g/g/a/a/f;->e:Lf/g/j/c/t;

    iput-object v4, v2, Lf/g/g/a/a/f;->f:Lf/g/d/d/e;

    iput-object v4, v2, Lf/g/g/a/a/f;->g:Lcom/facebook/common/internal/Supplier;

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v4

    throw p1
.end method


# virtual methods
.method public a()Lf/g/g/a/a/d;
    .locals 7

    new-instance v6, Lf/g/g/a/a/d;

    iget-object v1, p0, Lf/g/g/a/a/e;->a:Landroid/content/Context;

    iget-object v2, p0, Lf/g/g/a/a/e;->c:Lf/g/g/a/a/f;

    iget-object v3, p0, Lf/g/g/a/a/e;->b:Lf/g/j/e/i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/g/g/a/a/d;-><init>(Landroid/content/Context;Lf/g/g/a/a/f;Lf/g/j/e/i;Ljava/util/Set;Ljava/util/Set;)V

    const/4 v0, 0x0

    iput-object v0, v6, Lf/g/g/a/a/d;->q:Lf/g/g/a/a/h/e;

    return-object v6
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lf/g/g/a/a/e;->a()Lf/g/g/a/a/d;

    move-result-object v0

    return-object v0
.end method
