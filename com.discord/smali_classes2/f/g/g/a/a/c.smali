.class public Lf/g/g/a/a/c;
.super Lcom/facebook/drawee/controller/AbstractDraweeController;
.source "PipelineDraweeController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/drawee/controller/AbstractDraweeController<",
        "Lcom/facebook/common/references/CloseableReference<",
        "Lf/g/j/j/c;",
        ">;",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public A:Lcom/facebook/cache/common/CacheKey;

.field public B:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public C:Z

.field public D:Lf/g/d/d/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/d/d/e<",
            "Lf/g/j/i/a;",
            ">;"
        }
    .end annotation
.end field

.field public E:Lf/g/g/a/a/h/f;

.field public F:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/g/j/l/e;",
            ">;"
        }
    .end annotation
.end field

.field public G:Lf/g/g/a/a/h/b;

.field public H:Lf/g/g/a/a/g/b;

.field public I:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public J:[Lcom/facebook/imagepipeline/request/ImageRequest;

.field public K:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public final x:Lf/g/j/i/a;

.field public final y:Lf/g/d/d/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/d/d/e<",
            "Lf/g/j/i/a;",
            ">;"
        }
    .end annotation
.end field

.field public final z:Lf/g/j/c/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lf/g/g/b/a;Lf/g/j/i/a;Ljava/util/concurrent/Executor;Lf/g/j/c/t;Lf/g/d/d/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lf/g/g/b/a;",
            "Lf/g/j/i/a;",
            "Ljava/util/concurrent/Executor;",
            "Lf/g/j/c/t<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;",
            "Lf/g/d/d/e<",
            "Lf/g/j/i/a;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p2, p4, v0, v0}, Lcom/facebook/drawee/controller/AbstractDraweeController;-><init>(Lf/g/g/b/a;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Object;)V

    new-instance p2, Lf/g/g/a/a/a;

    invoke-direct {p2, p1, p3}, Lf/g/g/a/a/a;-><init>(Landroid/content/res/Resources;Lf/g/j/i/a;)V

    iput-object p2, p0, Lf/g/g/a/a/c;->x:Lf/g/j/i/a;

    iput-object p6, p0, Lf/g/g/a/a/c;->y:Lf/g/d/d/e;

    iput-object p5, p0, Lf/g/g/a/a/c;->z:Lf/g/j/c/t;

    return-void
.end method


# virtual methods
.method public declared-synchronized F(Lf/g/g/a/a/h/b;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    instance-of v1, v0, Lf/g/g/a/a/h/a;

    if-eqz v1, :cond_0

    check-cast v0, Lf/g/g/a/a/h/a;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, v0, Lf/g/g/a/a/h/a;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_0
    if-eqz v0, :cond_1

    new-instance v1, Lf/g/g/a/a/h/a;

    const/4 v2, 0x2

    new-array v2, v2, [Lf/g/g/a/a/h/b;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    invoke-direct {v1, v2}, Lf/g/g/a/a/h/a;-><init>([Lf/g/g/a/a/h/b;)V

    iput-object v1, p0, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized G(Lf/g/j/l/e;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/g/a/a/c;->F:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lf/g/g/a/a/c;->F:Ljava/util/Set;

    :cond_0
    iget-object v0, p0, Lf/g/g/a/a/c;->F:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public H(Lcom/facebook/common/internal/Supplier;Ljava/lang/String;Lcom/facebook/cache/common/CacheKey;Ljava/lang/Object;Lf/g/d/d/e;Lf/g/g/a/a/h/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/internal/Supplier<",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;>;",
            "Ljava/lang/String;",
            "Lcom/facebook/cache/common/CacheKey;",
            "Ljava/lang/Object;",
            "Lf/g/d/d/e<",
            "Lf/g/j/i/a;",
            ">;",
            "Lf/g/g/a/a/h/b;",
            ")V"
        }
    .end annotation

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-virtual {p0, p2, p4}, Lcom/facebook/drawee/controller/AbstractDraweeController;->n(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->s:Z

    iput-object p1, p0, Lf/g/g/a/a/c;->B:Lcom/facebook/common/internal/Supplier;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/g/g/a/a/c;->K(Lf/g/j/j/c;)V

    iput-object p3, p0, Lf/g/g/a/a/c;->A:Lcom/facebook/cache/common/CacheKey;

    iput-object p1, p0, Lf/g/g/a/a/c;->D:Lf/g/d/d/e;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, p1}, Lf/g/g/a/a/c;->K(Lf/g/j/j/c;)V

    invoke-virtual {p0, p1}, Lf/g/g/a/a/c;->F(Lf/g/g/a/a/h/b;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public declared-synchronized I(Lf/g/g/a/a/h/e;Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;Lcom/facebook/common/internal/Supplier;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/g/a/a/h/e;",
            "Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder<",
            "Lf/g/g/a/a/d;",
            "Lcom/facebook/imagepipeline/request/ImageRequest;",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;",
            "Lcom/facebook/imagepipeline/image/ImageInfo;",
            ">;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/g/a/a/c;->E:Lf/g/g/a/a/h/f;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/g/g/a/a/h/f;->c()V

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lf/g/g/a/a/c;->E:Lf/g/g/a/a/h/f;

    if-nez v1, :cond_1

    new-instance v1, Lf/g/g/a/a/h/f;

    invoke-static {}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->get()Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    invoke-direct {v1, v2, p0, p3}, Lf/g/g/a/a/h/f;-><init>(Lf/g/d/k/b;Lf/g/g/a/a/c;Lcom/facebook/common/internal/Supplier;)V

    iput-object v1, p0, Lf/g/g/a/a/c;->E:Lf/g/g/a/a/h/f;

    :cond_1
    iget-object p3, p0, Lf/g/g/a/a/c;->E:Lf/g/g/a/a/h/f;

    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p3, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p3, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    :cond_2
    iget-object p3, p3, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/g/g/a/a/c;->E:Lf/g/g/a/a/h/f;

    const/4 p3, 0x1

    invoke-virtual {p1, p3}, Lf/g/g/a/a/h/f;->d(Z)V

    iget-object p1, p0, Lf/g/g/a/a/c;->E:Lf/g/g/a/a/h/f;

    iget-object p1, p1, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    iget-object p3, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    check-cast p3, Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v1, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->f:[Ljava/lang/Object;

    check-cast v1, [Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p3, p1, Lf/g/g/a/a/h/h;->f:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object v0, p1, Lf/g/g/a/a/h/h;->g:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object v1, p1, Lf/g/g/a/a/h/h;->h:[Lcom/facebook/imagepipeline/request/ImageRequest;

    :cond_3
    iget-object p1, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->e:Ljava/lang/Object;

    check-cast p1, Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p1, p0, Lf/g/g/a/a/c;->I:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object p1, p2, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->f:[Ljava/lang/Object;

    check-cast p1, [Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p1, p0, Lf/g/g/a/a/c;->J:[Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object v0, p0, Lf/g/g/a/a/c;->K:Lcom/facebook/imagepipeline/request/ImageRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final J(Lf/g/d/d/e;Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/d/e<",
            "Lf/g/j/i/a;",
            ">;",
            "Lf/g/j/j/c;",
            ")",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/j/i/a;

    invoke-interface {v1, p2}, Lf/g/j/i/a;->a(Lf/g/j/j/c;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, p2}, Lf/g/j/i/a;->b(Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    return-object v1

    :cond_2
    return-object v0
.end method

.method public final K(Lf/g/j/j/c;)V
    .locals 5

    iget-boolean v0, p0, Lf/g/g/a/a/c;->C:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->i:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    new-instance v0, Lf/g/g/d/a;

    invoke-direct {v0}, Lf/g/g/d/a;-><init>()V

    new-instance v1, Lf/g/g/d/b/a;

    invoke-direct {v1, v0}, Lf/g/g/d/b/a;-><init>(Lf/g/g/d/b/b;)V

    new-instance v2, Lf/g/g/a/a/g/b;

    invoke-direct {v2}, Lf/g/g/a/a/g/b;-><init>()V

    iput-object v2, p0, Lf/g/g/a/a/c;->H:Lf/g/g/a/a/g/b;

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->f(Lcom/facebook/drawee/controller/ControllerListener;)V

    iput-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->i:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lf/g/g/h/a;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget-object v0, p0, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    if-nez v0, :cond_2

    iget-object v0, p0, Lf/g/g/a/a/c;->H:Lf/g/g/a/a/g/b;

    invoke-virtual {p0, v0}, Lf/g/g/a/a/c;->F(Lf/g/g/a/a/h/b;)V

    :cond_2
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->i:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lf/g/g/d/a;

    if-eqz v1, :cond_6

    check-cast v0, Lf/g/g/d/a;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const-string v1, "none"

    :goto_0
    iput-object v1, v0, Lf/g/g/d/a;->d:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcom/facebook/drawee/interfaces/DraweeHierarchy;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Ls/a/b/b/a;->u(Landroid/graphics/drawable/Drawable;)Lf/g/g/e/p;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v2, v1, Lf/g/g/e/p;->h:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    :cond_4
    iput-object v2, v0, Lf/g/g/d/a;->h:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iget-object v1, p0, Lf/g/g/a/a/c;->H:Lf/g/g/a/a/g/b;

    iget v1, v1, Lf/g/g/a/a/g/b;->a:I

    packed-switch v1, :pswitch_data_0

    const-string v2, "unknown"

    goto :goto_1

    :pswitch_0
    const-string v2, "local"

    goto :goto_1

    :pswitch_1
    const-string v2, "memory_bitmap_shortcut"

    goto :goto_1

    :pswitch_2
    const-string v2, "memory_bitmap"

    goto :goto_1

    :pswitch_3
    const-string v2, "memory_encoded"

    goto :goto_1

    :pswitch_4
    const-string v2, "disk"

    goto :goto_1

    :pswitch_5
    const-string v2, "network"

    :goto_1
    sget-object v3, Lf/g/g/a/a/g/a;->a:Landroid/util/SparseIntArray;

    const/4 v4, -0x1

    invoke-virtual {v3, v1, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    iput-object v2, v0, Lf/g/g/d/a;->w:Ljava/lang/String;

    iput v1, v0, Lf/g/g/d/a;->x:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    if-eqz p1, :cond_5

    invoke-interface {p1}, Lcom/facebook/imagepipeline/image/ImageInfo;->getWidth()I

    move-result v1

    invoke-interface {p1}, Lcom/facebook/imagepipeline/image/ImageInfo;->getHeight()I

    move-result v2

    iput v1, v0, Lf/g/g/d/a;->e:I

    iput v2, v0, Lf/g/g/d/a;->f:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    invoke-virtual {p1}, Lf/g/j/j/c;->c()I

    move-result p1

    iput p1, v0, Lf/g/g/d/a;->g:I

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lf/g/g/d/a;->c()V

    :cond_6
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized L(Lf/g/j/l/e;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf/g/g/a/a/c;->F:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public e(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->e(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/g/g/a/a/c;->K(Lf/g/j/j/c;)V

    return-void
.end method

.method public g(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 3

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf/g/j/j/c;

    invoke-virtual {p0, p1}, Lf/g/g/a/a/c;->K(Lf/g/j/j/c;)V

    iget-object v0, p0, Lf/g/g/a/a/c;->D:Lf/g/d/d/e;

    invoke-virtual {p0, v0, p1}, Lf/g/g/a/a/c;->J(Lf/g/d/d/e;Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    goto :goto_1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lf/g/g/a/a/c;->y:Lf/g/d/d/e;

    invoke-virtual {p0, v0, p1}, Lf/g/g/a/a/c;->J(Lf/g/d/d/e;Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/g/g/a/a/c;->x:Lf/g/j/i/a;

    invoke-interface {v0, p1}, Lf/g/j/i/a;->b(Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_0

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized image class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public h()Ljava/lang/Object;
    .locals 3

    invoke-static {}, Lf/g/j/s/b;->b()Z

    :try_start_0
    iget-object v0, p0, Lf/g/g/a/a/c;->z:Lf/g/j/c/t;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lf/g/g/a/a/c;->A:Lcom/facebook/cache/common/CacheKey;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0, v2}, Lf/g/j/c/t;->get(Ljava/lang/Object;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/g/j/j/c;

    invoke-virtual {v2}, Lf/g/j/j/c;->b()Lf/g/j/j/i;

    move-result-object v2

    check-cast v2, Lf/g/j/j/h;

    iget-boolean v2, v2, Lf/g/j/j/h;->c:Z

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/common/references/CloseableReference;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    move-object v1, v0

    goto :goto_1

    :cond_2
    :goto_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    :goto_1
    return-object v1

    :catchall_0
    move-exception v0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw v0
.end method

.method public j()Lcom/facebook/datasource/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/datasource/DataSource<",
            "Lcom/facebook/common/references/CloseableReference<",
            "Lf/g/j/j/c;",
            ">;>;"
        }
    .end annotation

    invoke-static {}, Lf/g/j/s/b;->b()Z

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    :cond_0
    iget-object v0, p0, Lf/g/g/a/a/c;->B:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v0}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/datasource/DataSource;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0
.end method

.method public k(Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p1, Lcom/facebook/common/references/CloseableReference;->e:Lcom/facebook/common/references/SharedReference;

    invoke-virtual {p1}, Lcom/facebook/common/references/SharedReference;->c()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    :cond_0
    return v0
.end method

.method public l(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    invoke-static {p1}, Lcom/facebook/common/references/CloseableReference;->q(Lcom/facebook/common/references/CloseableReference;)Z

    move-result v0

    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/imagepipeline/image/ImageInfo;

    return-object p1
.end method

.method public m()Landroid/net/Uri;
    .locals 5

    iget-object v0, p0, Lf/g/g/a/a/c;->I:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v1, p0, Lf/g/g/a/a/c;->K:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v2, p0, Lf/g/g/a/a/c;->J:[Lcom/facebook/imagepipeline/request/ImageRequest;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    :goto_0
    move-object v3, v0

    goto :goto_2

    :cond_0
    if-eqz v2, :cond_2

    array-length v0, v2

    if-lez v0, :cond_2

    const/4 v0, 0x0

    aget-object v4, v2, v0

    if-eqz v4, :cond_2

    aget-object v0, v2, v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    goto :goto_1

    :cond_1
    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    iget-object v3, v1, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    :cond_3
    :goto_2
    return-object v3
.end method

.method public t(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0

    check-cast p1, Lcom/facebook/imagepipeline/image/ImageInfo;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lf/g/j/j/g;->a()Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Ls/a/b/b/a;->c0(Ljava/lang/Object;)Lf/g/d/d/i;

    move-result-object v0

    invoke-super {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "super"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lf/g/g/a/a/c;->B:Lcom/facebook/common/internal/Supplier;

    const-string v2, "dataSourceSupplier"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    invoke-virtual {v0}, Lf/g/d/d/i;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/facebook/common/references/CloseableReference;

    monitor-enter p0

    :try_start_0
    iget-object p2, p0, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    if-eqz p2, :cond_0

    const/4 v0, 0x6

    const/4 v1, 0x1

    const-string v2, "PipelineDraweeController"

    invoke-interface {p2, p1, v0, v1, v2}, Lf/g/g/a/a/h/b;->a(Ljava/lang/String;IZLjava/lang/String;)V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public x(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    instance-of v0, p1, Lf/g/f/a/a;

    if-eqz v0, :cond_0

    check-cast p1, Lf/g/f/a/a;

    invoke-interface {p1}, Lf/g/f/a/a;->a()V

    :cond_0
    return-void
.end method

.method public z(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/facebook/common/references/CloseableReference;

    sget-object v0, Lcom/facebook/common/references/CloseableReference;->h:Ljava/lang/Class;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_0
    return-void
.end method
