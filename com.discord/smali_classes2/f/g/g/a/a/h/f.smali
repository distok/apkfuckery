.class public Lf/g/g/a/a/h/f;
.super Ljava/lang/Object;
.source "ImagePerfMonitor.java"

# interfaces
.implements Lf/g/g/a/a/h/g;


# instance fields
.field public final a:Lf/g/g/a/a/c;

.field public final b:Lf/g/d/k/b;

.field public final c:Lf/g/g/a/a/h/h;

.field public final d:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lf/g/g/a/a/h/c;

.field public f:Lf/g/g/a/a/h/b;

.field public g:Lf/g/g/a/a/h/i/c;

.field public h:Lf/g/g/a/a/h/i/a;

.field public i:Lf/g/j/l/c;

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/g/g/a/a/h/e;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z


# direct methods
.method public constructor <init>(Lf/g/d/k/b;Lf/g/g/a/a/c;Lcom/facebook/common/internal/Supplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/k/b;",
            "Lf/g/g/a/a/c;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->b:Lf/g/d/k/b;

    iput-object p2, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    new-instance p1, Lf/g/g/a/a/h/h;

    invoke-direct {p1}, Lf/g/g/a/a/h/h;-><init>()V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    iput-object p3, p0, Lf/g/g/a/a/h/f;->d:Lcom/facebook/common/internal/Supplier;

    return-void
.end method


# virtual methods
.method public a(Lf/g/g/a/a/h/h;I)V
    .locals 2

    iget-boolean v0, p0, Lf/g/g/a/a/h/f;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lf/g/g/a/a/h/h;->b()Lf/g/g/a/a/h/d;

    move-result-object p1

    iget-object v0, p0, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/g/a/a/h/e;

    invoke-interface {v1, p1, p2}, Lf/g/g/a/a/h/e;->b(Lf/g/g/a/a/h/d;I)V

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public b(Lf/g/g/a/a/h/h;I)V
    .locals 3

    iput p2, p1, Lf/g/g/a/a/h/h;->v:I

    iget-boolean v0, p0, Lf/g/g/a/a/h/f;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    iget-object v0, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeHierarchy;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeHierarchy;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v1, Lf/g/g/a/a/h/h;->s:I

    iget-object v1, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, v1, Lf/g/g/a/a/h/h;->t:I

    :cond_1
    invoke-virtual {p1}, Lf/g/g/a/a/h/h;->b()Lf/g/g/a/a/h/d;

    move-result-object p1

    iget-object v0, p0, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/g/g/a/a/h/e;

    invoke-interface {v1, p1, p2}, Lf/g/g/a/a/h/e;->a(Lf/g/g/a/a/h/d;I)V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method public c()V
    .locals 4

    iget-object v0, p0, Lf/g/g/a/a/h/f;->j:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lf/g/g/a/a/h/f;->d(Z)V

    iget-object v1, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    const/4 v2, 0x0

    iput-object v2, v1, Lf/g/g/a/a/h/h;->b:Ljava/lang/String;

    iput-object v2, v1, Lf/g/g/a/a/h/h;->c:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object v2, v1, Lf/g/g/a/a/h/h;->d:Ljava/lang/Object;

    iput-object v2, v1, Lf/g/g/a/a/h/h;->e:Lcom/facebook/imagepipeline/image/ImageInfo;

    iput-object v2, v1, Lf/g/g/a/a/h/h;->f:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object v2, v1, Lf/g/g/a/a/h/h;->g:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object v2, v1, Lf/g/g/a/a/h/h;->h:[Lcom/facebook/imagepipeline/request/ImageRequest;

    const/4 v3, 0x1

    iput v3, v1, Lf/g/g/a/a/h/h;->p:I

    iput-object v2, v1, Lf/g/g/a/a/h/h;->q:Ljava/lang/String;

    iput-boolean v0, v1, Lf/g/g/a/a/h/h;->r:Z

    const/4 v0, -0x1

    iput v0, v1, Lf/g/g/a/a/h/h;->s:I

    iput v0, v1, Lf/g/g/a/a/h/h;->t:I

    iput-object v2, v1, Lf/g/g/a/a/h/h;->u:Ljava/lang/Throwable;

    iput v0, v1, Lf/g/g/a/a/h/h;->v:I

    iput v0, v1, Lf/g/g/a/a/h/h;->w:I

    iput-object v2, v1, Lf/g/g/a/a/h/h;->A:Lf/g/h/b/a/b$a;

    invoke-virtual {v1}, Lf/g/g/a/a/h/h;->a()V

    return-void
.end method

.method public d(Z)V
    .locals 4

    iput-boolean p1, p0, Lf/g/g/a/a/h/f;->k:Z

    if-eqz p1, :cond_7

    iget-object p1, p0, Lf/g/g/a/a/h/f;->h:Lf/g/g/a/a/h/i/a;

    if-nez p1, :cond_0

    new-instance p1, Lf/g/g/a/a/h/i/a;

    iget-object v0, p0, Lf/g/g/a/a/h/f;->b:Lf/g/d/k/b;

    iget-object v1, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    iget-object v2, p0, Lf/g/g/a/a/h/f;->d:Lcom/facebook/common/internal/Supplier;

    invoke-direct {p1, v0, v1, p0, v2}, Lf/g/g/a/a/h/i/a;-><init>(Lf/g/d/k/b;Lf/g/g/a/a/h/h;Lf/g/g/a/a/h/g;Lcom/facebook/common/internal/Supplier;)V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->h:Lf/g/g/a/a/h/i/a;

    :cond_0
    iget-object p1, p0, Lf/g/g/a/a/h/f;->g:Lf/g/g/a/a/h/i/c;

    if-nez p1, :cond_1

    new-instance p1, Lf/g/g/a/a/h/i/c;

    iget-object v0, p0, Lf/g/g/a/a/h/f;->b:Lf/g/d/k/b;

    iget-object v1, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    invoke-direct {p1, v0, v1}, Lf/g/g/a/a/h/i/c;-><init>(Lf/g/d/k/b;Lf/g/g/a/a/h/h;)V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->g:Lf/g/g/a/a/h/i/c;

    :cond_1
    iget-object p1, p0, Lf/g/g/a/a/h/f;->f:Lf/g/g/a/a/h/b;

    if-nez p1, :cond_2

    new-instance p1, Lf/g/g/a/a/h/i/b;

    iget-object v0, p0, Lf/g/g/a/a/h/f;->c:Lf/g/g/a/a/h/h;

    invoke-direct {p1, v0, p0}, Lf/g/g/a/a/h/i/b;-><init>(Lf/g/g/a/a/h/h;Lf/g/g/a/a/h/f;)V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->f:Lf/g/g/a/a/h/b;

    :cond_2
    iget-object p1, p0, Lf/g/g/a/a/h/f;->e:Lf/g/g/a/a/h/c;

    if-nez p1, :cond_3

    new-instance p1, Lf/g/g/a/a/h/c;

    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    iget-object v0, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-object v1, p0, Lf/g/g/a/a/h/f;->f:Lf/g/g/a/a/h/b;

    invoke-direct {p1, v0, v1}, Lf/g/g/a/a/h/c;-><init>(Ljava/lang/String;Lf/g/g/a/a/h/b;)V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->e:Lf/g/g/a/a/h/c;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    iget-object v0, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iput-object v0, p1, Lf/g/g/a/a/h/c;->a:Ljava/lang/String;

    :goto_0
    iget-object p1, p0, Lf/g/g/a/a/h/f;->i:Lf/g/j/l/c;

    if-nez p1, :cond_4

    new-instance p1, Lf/g/j/l/c;

    const/4 v0, 0x2

    new-array v0, v0, [Lf/g/j/l/e;

    const/4 v1, 0x0

    iget-object v2, p0, Lf/g/g/a/a/h/f;->g:Lf/g/g/a/a/h/i/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lf/g/g/a/a/h/f;->e:Lf/g/g/a/a/h/c;

    aput-object v2, v0, v1

    invoke-direct {p1, v0}, Lf/g/j/l/c;-><init>([Lf/g/j/l/e;)V

    iput-object p1, p0, Lf/g/g/a/a/h/f;->i:Lf/g/j/l/c;

    :cond_4
    iget-object p1, p0, Lf/g/g/a/a/h/f;->f:Lf/g/g/a/a/h/b;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    invoke-virtual {v0, p1}, Lf/g/g/a/a/c;->F(Lf/g/g/a/a/h/b;)V

    :cond_5
    iget-object p1, p0, Lf/g/g/a/a/h/f;->h:Lf/g/g/a/a/h/i/a;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    iget-object v0, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    monitor-enter v0

    :try_start_0
    iget-object v1, v0, Lf/g/h/b/a/c;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_6
    :goto_1
    iget-object p1, p0, Lf/g/g/a/a/h/f;->i:Lf/g/j/l/c;

    if-eqz p1, :cond_d

    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    invoke-virtual {v0, p1}, Lf/g/g/a/a/c;->G(Lf/g/j/l/e;)V

    goto :goto_4

    :cond_7
    iget-object p1, p0, Lf/g/g/a/a/h/f;->f:Lf/g/g/a/a/h/b;

    const/4 v0, 0x0

    if-eqz p1, :cond_a

    iget-object v1, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    monitor-enter v1

    :try_start_1
    iget-object v2, v1, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;

    instance-of v3, v2, Lf/g/g/a/a/h/a;

    if-eqz v3, :cond_8

    check-cast v2, Lf/g/g/a/a/h/a;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v3, v2, Lf/g/g/a/a/h/a;->a:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    monitor-exit v1

    goto :goto_2

    :catchall_1
    move-exception p1

    :try_start_4
    monitor-exit v2

    throw p1

    :cond_8
    if-ne v2, p1, :cond_9

    iput-object v0, v1, Lf/g/g/a/a/c;->G:Lf/g/g/a/a/h/b;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :cond_9
    monitor-exit v1

    goto :goto_2

    :catchall_2
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_a
    :goto_2
    iget-object p1, p0, Lf/g/g/a/a/h/f;->h:Lf/g/g/a/a/h/i/a;

    if-eqz p1, :cond_c

    iget-object v1, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    iget-object v1, v1, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    monitor-enter v1

    :try_start_5
    iget-object v2, v1, Lf/g/h/b/a/c;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    const/4 v2, -0x1

    if-eq p1, v2, :cond_b

    iget-object v2, v1, Lf/g/h/b/a/c;->a:Ljava/util/List;

    invoke-interface {v2, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_b
    monitor-exit v1

    goto :goto_3

    :catchall_3
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_c
    :goto_3
    iget-object p1, p0, Lf/g/g/a/a/h/f;->i:Lf/g/j/l/c;

    if-eqz p1, :cond_d

    iget-object v0, p0, Lf/g/g/a/a/h/f;->a:Lf/g/g/a/a/c;

    invoke-virtual {v0, p1}, Lf/g/g/a/a/c;->L(Lf/g/j/l/e;)V

    :cond_d
    :goto_4
    return-void
.end method
