.class public Lf/g/g/a/a/h/i/a;
.super Lf/g/h/b/a/a;
.source "ImagePerfControllerListener2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/g/a/a/h/i/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/h/b/a/a<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;",
        "Ljava/lang/Object<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/d/k/b;

.field public final b:Lf/g/g/a/a/h/h;

.field public final c:Lf/g/g/a/a/h/g;

.field public final d:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lf/g/d/k/b;Lf/g/g/a/a/h/h;Lf/g/g/a/a/h/g;Lcom/facebook/common/internal/Supplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/g/d/k/b;",
            "Lf/g/g/a/a/h/h;",
            "Lf/g/g/a/a/h/g;",
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/g/h/b/a/a;-><init>()V

    iput-object p1, p0, Lf/g/g/a/a/h/i/a;->a:Lf/g/d/k/b;

    iput-object p2, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-object p3, p0, Lf/g/g/a/a/h/i/a;->c:Lf/g/g/a/a/h/g;

    iput-object p4, p0, Lf/g/g/a/a/h/i/a;->d:Lcom/facebook/common/internal/Supplier;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/Object;Lf/g/h/b/a/b$a;)V
    .locals 3

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->a:Lf/g/d/k/b;

    invoke-interface {v0}, Lf/g/d/k/b;->now()J

    move-result-wide v0

    iget-object v2, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    invoke-virtual {v2}, Lf/g/g/a/a/h/h;->a()V

    iget-object v2, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-wide v0, v2, Lf/g/g/a/a/h/h;->i:J

    iput-object p1, v2, Lf/g/g/a/a/h/h;->a:Ljava/lang/String;

    iput-object p2, v2, Lf/g/g/a/a/h/h;->d:Ljava/lang/Object;

    iput-object p3, v2, Lf/g/g/a/a/h/h;->A:Lf/g/h/b/a/b$a;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/g/g/a/a/h/i/a;->f(I)V

    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    const/4 p2, 0x1

    iput p2, p1, Lf/g/g/a/a/h/h;->w:I

    iput-wide v0, p1, Lf/g/g/a/a/h/h;->x:J

    invoke-virtual {p0, p2}, Lf/g/g/a/a/h/i/a;->g(I)V

    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/Throwable;Lf/g/h/b/a/b$a;)V
    .locals 3

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->a:Lf/g/d/k/b;

    invoke-interface {v0}, Lf/g/d/k/b;->now()J

    move-result-wide v0

    iget-object v2, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-object p3, v2, Lf/g/g/a/a/h/h;->A:Lf/g/h/b/a/b$a;

    iput-wide v0, v2, Lf/g/g/a/a/h/h;->l:J

    iput-object p1, v2, Lf/g/g/a/a/h/h;->a:Ljava/lang/String;

    iput-object p2, v2, Lf/g/g/a/a/h/h;->u:Ljava/lang/Throwable;

    const/4 p1, 0x5

    invoke-virtual {p0, p1}, Lf/g/g/a/a/h/i/a;->f(I)V

    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    const/4 p2, 0x2

    iput p2, p1, Lf/g/g/a/a/h/h;->w:I

    iput-wide v0, p1, Lf/g/g/a/a/h/h;->y:J

    invoke-virtual {p0, p2}, Lf/g/g/a/a/h/i/a;->g(I)V

    return-void
.end method

.method public c(Ljava/lang/String;Lf/g/h/b/a/b$a;)V
    .locals 4

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->a:Lf/g/d/k/b;

    invoke-interface {v0}, Lf/g/d/k/b;->now()J

    move-result-wide v0

    iget-object v2, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-object p2, v2, Lf/g/g/a/a/h/h;->A:Lf/g/h/b/a/b$a;

    iget p2, v2, Lf/g/g/a/a/h/h;->v:I

    const/4 v3, 0x3

    if-eq p2, v3, :cond_0

    const/4 v3, 0x5

    if-eq p2, v3, :cond_0

    const/4 v3, 0x6

    if-eq p2, v3, :cond_0

    iput-wide v0, v2, Lf/g/g/a/a/h/h;->m:J

    iput-object p1, v2, Lf/g/g/a/a/h/h;->a:Ljava/lang/String;

    const/4 p1, 0x4

    invoke-virtual {p0, p1}, Lf/g/g/a/a/h/i/a;->f(I)V

    :cond_0
    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    const/4 p2, 0x2

    iput p2, p1, Lf/g/g/a/a/h/h;->w:I

    iput-wide v0, p1, Lf/g/g/a/a/h/h;->y:J

    invoke-virtual {p0, p2}, Lf/g/g/a/a/h/i/a;->g(I)V

    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/Object;Lf/g/h/b/a/b$a;)V
    .locals 3

    check-cast p2, Lcom/facebook/imagepipeline/image/ImageInfo;

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->a:Lf/g/d/k/b;

    invoke-interface {v0}, Lf/g/d/k/b;->now()J

    move-result-wide v0

    iget-object v2, p3, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    iget-object v2, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-object p3, v2, Lf/g/g/a/a/h/h;->A:Lf/g/h/b/a/b$a;

    iput-wide v0, v2, Lf/g/g/a/a/h/h;->k:J

    iput-wide v0, v2, Lf/g/g/a/a/h/h;->o:J

    iput-object p1, v2, Lf/g/g/a/a/h/h;->a:Ljava/lang/String;

    iput-object p2, v2, Lf/g/g/a/a/h/h;->e:Lcom/facebook/imagepipeline/image/ImageInfo;

    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Lf/g/g/a/a/h/i/a;->f(I)V

    return-void
.end method

.method public final e()Z
    .locals 4

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->d:Lcom/facebook/common/internal/Supplier;

    invoke-interface {v0}, Lcom/facebook/common/internal/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;

    if-nez v1, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit p0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "ImagePerfControllerListener2Thread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lf/g/g/a/a/h/i/a$a;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v3, p0, Lf/g/g/a/a/h/i/a;->c:Lf/g/g/a/a/h/g;

    invoke-direct {v2, v1, v3}, Lf/g/g/a/a/h/i/a$a;-><init>(Landroid/os/Looper;Lf/g/g/a/a/h/g;)V

    iput-object v2, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :goto_0
    return v0
.end method

.method public final f(I)V
    .locals 2

    invoke-virtual {p0}, Lf/g/g/a/a/h/i/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->c:Lf/g/g/a/a/h/g;

    iget-object v1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    check-cast v0, Lf/g/g/a/a/h/f;

    invoke-virtual {v0, v1, p1}, Lf/g/g/a/a/h/f;->b(Lf/g/g/a/a/h/h;I)V

    :goto_0
    return-void
.end method

.method public final g(I)V
    .locals 2

    invoke-virtual {p0}, Lf/g/g/a/a/h/i/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lf/g/g/a/a/h/i/a;->e:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/g/a/a/h/i/a;->c:Lf/g/g/a/a/h/g;

    iget-object v1, p0, Lf/g/g/a/a/h/i/a;->b:Lf/g/g/a/a/h/h;

    check-cast v0, Lf/g/g/a/a/h/f;

    invoke-virtual {v0, v1, p1}, Lf/g/g/a/a/h/f;->a(Lf/g/g/a/a/h/h;I)V

    :goto_0
    return-void
.end method
