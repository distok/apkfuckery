.class public Lf/g/g/a/a/h/i/a$a;
.super Landroid/os/Handler;
.source "ImagePerfControllerListener2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/g/a/a/h/i/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final a:Lf/g/g/a/a/h/g;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lf/g/g/a/a/h/g;)V
    .locals 0
    .param p1    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/g/g/a/a/h/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lf/g/g/a/a/h/i/a$a;->a:Lf/g/g/a/a/h/g;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/g/a/a/h/i/a$a;->a:Lf/g/g/a/a/h/g;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lf/g/g/a/a/h/h;

    iget p1, p1, Landroid/os/Message;->arg1:I

    check-cast v0, Lf/g/g/a/a/h/f;

    invoke-virtual {v0, v1, p1}, Lf/g/g/a/a/h/f;->a(Lf/g/g/a/a/h/h;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/g/g/a/a/h/i/a$a;->a:Lf/g/g/a/a/h/g;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lf/g/g/a/a/h/h;

    iget p1, p1, Landroid/os/Message;->arg1:I

    check-cast v0, Lf/g/g/a/a/h/f;

    invoke-virtual {v0, v1, p1}, Lf/g/g/a/a/h/f;->b(Lf/g/g/a/a/h/h;I)V

    :goto_0
    return-void
.end method
