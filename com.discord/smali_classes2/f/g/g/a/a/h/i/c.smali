.class public Lf/g/g/a/a/h/i/c;
.super Lf/g/j/l/a;
.source "ImagePerfRequestListener.java"


# instance fields
.field public final a:Lf/g/d/k/b;

.field public final b:Lf/g/g/a/a/h/h;


# direct methods
.method public constructor <init>(Lf/g/d/k/b;Lf/g/g/a/a/h/h;)V
    .locals 0

    invoke-direct {p0}, Lf/g/j/l/a;-><init>()V

    iput-object p1, p0, Lf/g/g/a/a/h/i/c;->a:Lf/g/d/k/b;

    iput-object p2, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iget-object v1, p0, Lf/g/g/a/a/h/i/c;->a:Lf/g/d/k/b;

    invoke-interface {v1}, Lf/g/d/k/b;->now()J

    move-result-wide v1

    iput-wide v1, v0, Lf/g/g/a/a/h/h;->n:J

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iput-object p1, v0, Lf/g/g/a/a/h/h;->c:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p2, v0, Lf/g/g/a/a/h/h;->d:Ljava/lang/Object;

    iput-object p3, v0, Lf/g/g/a/a/h/h;->b:Ljava/lang/String;

    iput-boolean p4, v0, Lf/g/g/a/a/h/h;->r:Z

    return-void
.end method

.method public c(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/String;Z)V
    .locals 3

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iget-object v1, p0, Lf/g/g/a/a/h/i/c;->a:Lf/g/d/k/b;

    invoke-interface {v1}, Lf/g/d/k/b;->now()J

    move-result-wide v1

    iput-wide v1, v0, Lf/g/g/a/a/h/h;->o:J

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iput-object p1, v0, Lf/g/g/a/a/h/h;->c:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p2, v0, Lf/g/g/a/a/h/h;->b:Ljava/lang/String;

    iput-boolean p3, v0, Lf/g/g/a/a/h/h;->r:Z

    return-void
.end method

.method public g(Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 2

    iget-object p3, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->a:Lf/g/d/k/b;

    invoke-interface {v0}, Lf/g/d/k/b;->now()J

    move-result-wide v0

    iput-wide v0, p3, Lf/g/g/a/a/h/h;->o:J

    iget-object p3, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iput-object p1, p3, Lf/g/g/a/a/h/h;->c:Lcom/facebook/imagepipeline/request/ImageRequest;

    iput-object p2, p3, Lf/g/g/a/a/h/h;->b:Ljava/lang/String;

    iput-boolean p4, p3, Lf/g/g/a/a/h/h;->r:Z

    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iget-object v1, p0, Lf/g/g/a/a/h/i/c;->a:Lf/g/d/k/b;

    invoke-interface {v1}, Lf/g/d/k/b;->now()J

    move-result-wide v1

    iput-wide v1, v0, Lf/g/g/a/a/h/h;->o:J

    iget-object v0, p0, Lf/g/g/a/a/h/i/c;->b:Lf/g/g/a/a/h/h;

    iput-object p1, v0, Lf/g/g/a/a/h/h;->b:Ljava/lang/String;

    return-void
.end method
