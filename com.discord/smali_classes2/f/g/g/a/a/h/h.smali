.class public Lf/g/g/a/a/h/h;
.super Ljava/lang/Object;
.source "ImagePerfState.java"


# instance fields
.field public A:Lf/g/h/b/a/b$a;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public d:Ljava/lang/Object;

.field public e:Lcom/facebook/imagepipeline/image/ImageInfo;

.field public f:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public g:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public h:[Lcom/facebook/imagepipeline/request/ImageRequest;

.field public i:J

.field public j:J

.field public k:J

.field public l:J

.field public m:J

.field public n:J

.field public o:J

.field public p:I

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:I

.field public t:I

.field public u:Ljava/lang/Throwable;

.field public v:I

.field public w:I

.field public x:J

.field public y:J

.field public z:J


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->i:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->j:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->k:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->l:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->m:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->n:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->o:J

    const/4 v2, 0x1

    iput v2, p0, Lf/g/g/a/a/h/h;->p:I

    const/4 v2, -0x1

    iput v2, p0, Lf/g/g/a/a/h/h;->s:I

    iput v2, p0, Lf/g/g/a/a/h/h;->t:I

    iput v2, p0, Lf/g/g/a/a/h/h;->v:I

    iput v2, p0, Lf/g/g/a/a/h/h;->w:I

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->x:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->y:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->z:J

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->n:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->o:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->i:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->k:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->l:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->m:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->x:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->y:J

    iput-wide v0, p0, Lf/g/g/a/a/h/h;->z:J

    return-void
.end method

.method public b()Lf/g/g/a/a/h/d;
    .locals 42

    move-object/from16 v0, p0

    new-instance v39, Lf/g/g/a/a/h/d;

    move-object/from16 v1, v39

    iget-object v2, v0, Lf/g/g/a/a/h/h;->a:Ljava/lang/String;

    iget-object v3, v0, Lf/g/g/a/a/h/h;->b:Ljava/lang/String;

    iget-object v4, v0, Lf/g/g/a/a/h/h;->c:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v5, v0, Lf/g/g/a/a/h/h;->d:Ljava/lang/Object;

    iget-object v6, v0, Lf/g/g/a/a/h/h;->e:Lcom/facebook/imagepipeline/image/ImageInfo;

    iget-object v7, v0, Lf/g/g/a/a/h/h;->f:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v8, v0, Lf/g/g/a/a/h/h;->g:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v9, v0, Lf/g/g/a/a/h/h;->h:[Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-wide v10, v0, Lf/g/g/a/a/h/h;->i:J

    iget-wide v12, v0, Lf/g/g/a/a/h/h;->j:J

    iget-wide v14, v0, Lf/g/g/a/a/h/h;->k:J

    move-object/from16 v40, v1

    move-object/from16 v41, v2

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->l:J

    move-wide/from16 v16, v1

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->m:J

    move-wide/from16 v18, v1

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->n:J

    move-wide/from16 v20, v1

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->o:J

    move-wide/from16 v22, v1

    iget v1, v0, Lf/g/g/a/a/h/h;->p:I

    move/from16 v24, v1

    iget-object v1, v0, Lf/g/g/a/a/h/h;->q:Ljava/lang/String;

    move-object/from16 v25, v1

    iget-boolean v1, v0, Lf/g/g/a/a/h/h;->r:Z

    move/from16 v26, v1

    iget v1, v0, Lf/g/g/a/a/h/h;->s:I

    move/from16 v27, v1

    iget v1, v0, Lf/g/g/a/a/h/h;->t:I

    move/from16 v28, v1

    iget-object v1, v0, Lf/g/g/a/a/h/h;->u:Ljava/lang/Throwable;

    move-object/from16 v29, v1

    iget v1, v0, Lf/g/g/a/a/h/h;->w:I

    move/from16 v30, v1

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->x:J

    move-wide/from16 v31, v1

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->y:J

    move-wide/from16 v33, v1

    iget-wide v1, v0, Lf/g/g/a/a/h/h;->z:J

    move-wide/from16 v36, v1

    iget-object v1, v0, Lf/g/g/a/a/h/h;->A:Lf/g/h/b/a/b$a;

    move-object/from16 v38, v1

    const/16 v35, 0x0

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct/range {v1 .. v38}, Lf/g/g/a/a/h/d;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/imagepipeline/request/ImageRequest;Ljava/lang/Object;Lcom/facebook/imagepipeline/image/ImageInfo;Lcom/facebook/imagepipeline/request/ImageRequest;Lcom/facebook/imagepipeline/request/ImageRequest;[Lcom/facebook/imagepipeline/request/ImageRequest;JJJJJJJILjava/lang/String;ZIILjava/lang/Throwable;IJJLjava/lang/String;JLf/g/h/b/a/b$a;)V

    return-object v39
.end method
