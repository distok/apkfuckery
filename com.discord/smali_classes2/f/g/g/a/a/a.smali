.class public Lf/g/g/a/a/a;
.super Ljava/lang/Object;
.source "DefaultDrawableFactory.java"

# interfaces
.implements Lf/g/j/i/a;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:Lf/g/j/i/a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lf/g/j/i/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/g/g/a/a/a;->a:Landroid/content/res/Resources;

    iput-object p2, p0, Lf/g/g/a/a/a;->b:Lf/g/j/i/a;

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/c;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public b(Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;
    .locals 5

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    instance-of v0, p1, Lf/g/j/j/d;

    if-eqz v0, :cond_3

    check-cast p1, Lf/g/j/j/d;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lf/g/g/a/a/a;->a:Landroid/content/res/Resources;

    iget-object v2, p1, Lf/g/j/j/d;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget v1, p1, Lf/g/j/j/d;->i:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    iget v1, p1, Lf/g/j/j/d;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v2, :cond_1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-nez v2, :cond_2

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v0

    :cond_2
    :try_start_1
    new-instance v1, Lf/g/g/e/i;

    iget v2, p1, Lf/g/j/j/d;->i:I

    iget p1, p1, Lf/g/j/j/d;->j:I

    invoke-direct {v1, v0, v2, p1}, Lf/g/g/e/i;-><init>(Landroid/graphics/drawable/Drawable;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object v1

    :cond_3
    :try_start_2
    iget-object v0, p0, Lf/g/g/a/a/a;->b:Lf/g/j/i/a;

    if-eqz v0, :cond_4

    invoke-interface {v0, p1}, Lf/g/j/i/a;->a(Lf/g/j/j/c;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/g/g/a/a/a;->b:Lf/g/j/i/a;

    invoke-interface {v0, p1}, Lf/g/j/i/a;->b(Lf/g/j/j/c;)Landroid/graphics/drawable/Drawable;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p1

    :cond_4
    const/4 p1, 0x0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method
