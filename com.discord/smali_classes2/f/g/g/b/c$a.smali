.class public final enum Lf/g/g/b/c$a;
.super Ljava/lang/Enum;
.source "DraweeEventTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/g/b/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/g/g/b/c$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lf/g/g/b/c$a;

.field public static final synthetic B:[Lf/g/g/b/c$a;

.field public static final enum d:Lf/g/g/b/c$a;

.field public static final enum e:Lf/g/g/b/c$a;

.field public static final enum f:Lf/g/g/b/c$a;

.field public static final enum g:Lf/g/g/b/c$a;

.field public static final enum h:Lf/g/g/b/c$a;

.field public static final enum i:Lf/g/g/b/c$a;

.field public static final enum j:Lf/g/g/b/c$a;

.field public static final enum k:Lf/g/g/b/c$a;

.field public static final enum l:Lf/g/g/b/c$a;

.field public static final enum m:Lf/g/g/b/c$a;

.field public static final enum n:Lf/g/g/b/c$a;

.field public static final enum o:Lf/g/g/b/c$a;

.field public static final enum p:Lf/g/g/b/c$a;

.field public static final enum q:Lf/g/g/b/c$a;

.field public static final enum r:Lf/g/g/b/c$a;

.field public static final enum s:Lf/g/g/b/c$a;

.field public static final enum t:Lf/g/g/b/c$a;

.field public static final enum u:Lf/g/g/b/c$a;

.field public static final enum v:Lf/g/g/b/c$a;

.field public static final enum w:Lf/g/g/b/c$a;

.field public static final enum x:Lf/g/g/b/c$a;

.field public static final enum y:Lf/g/g/b/c$a;

.field public static final enum z:Lf/g/g/b/c$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 26

    new-instance v0, Lf/g/g/b/c$a;

    const-string v1, "ON_SET_HIERARCHY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/g/g/b/c$a;->d:Lf/g/g/b/c$a;

    new-instance v1, Lf/g/g/b/c$a;

    const-string v3, "ON_CLEAR_HIERARCHY"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/g/g/b/c$a;->e:Lf/g/g/b/c$a;

    new-instance v3, Lf/g/g/b/c$a;

    const-string v5, "ON_SET_CONTROLLER"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/g/g/b/c$a;->f:Lf/g/g/b/c$a;

    new-instance v5, Lf/g/g/b/c$a;

    const-string v7, "ON_CLEAR_OLD_CONTROLLER"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/g/g/b/c$a;->g:Lf/g/g/b/c$a;

    new-instance v7, Lf/g/g/b/c$a;

    const-string v9, "ON_CLEAR_CONTROLLER"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lf/g/g/b/c$a;->h:Lf/g/g/b/c$a;

    new-instance v9, Lf/g/g/b/c$a;

    const-string v11, "ON_INIT_CONTROLLER"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lf/g/g/b/c$a;->i:Lf/g/g/b/c$a;

    new-instance v11, Lf/g/g/b/c$a;

    const-string v13, "ON_ATTACH_CONTROLLER"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lf/g/g/b/c$a;->j:Lf/g/g/b/c$a;

    new-instance v13, Lf/g/g/b/c$a;

    const-string v15, "ON_DETACH_CONTROLLER"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lf/g/g/b/c$a;->k:Lf/g/g/b/c$a;

    new-instance v15, Lf/g/g/b/c$a;

    const-string v14, "ON_RELEASE_CONTROLLER"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lf/g/g/b/c$a;->l:Lf/g/g/b/c$a;

    new-instance v14, Lf/g/g/b/c$a;

    const-string v12, "ON_DATASOURCE_SUBMIT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lf/g/g/b/c$a;->m:Lf/g/g/b/c$a;

    new-instance v12, Lf/g/g/b/c$a;

    const-string v10, "ON_DATASOURCE_RESULT"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lf/g/g/b/c$a;->n:Lf/g/g/b/c$a;

    new-instance v10, Lf/g/g/b/c$a;

    const-string v8, "ON_DATASOURCE_RESULT_INT"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lf/g/g/b/c$a;->o:Lf/g/g/b/c$a;

    new-instance v8, Lf/g/g/b/c$a;

    const-string v6, "ON_DATASOURCE_FAILURE"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lf/g/g/b/c$a;->p:Lf/g/g/b/c$a;

    new-instance v6, Lf/g/g/b/c$a;

    const-string v4, "ON_DATASOURCE_FAILURE_INT"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lf/g/g/b/c$a;->q:Lf/g/g/b/c$a;

    new-instance v4, Lf/g/g/b/c$a;

    const-string v2, "ON_HOLDER_ATTACH"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lf/g/g/b/c$a;->r:Lf/g/g/b/c$a;

    new-instance v2, Lf/g/g/b/c$a;

    const-string v6, "ON_HOLDER_DETACH"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lf/g/g/b/c$a;->s:Lf/g/g/b/c$a;

    new-instance v6, Lf/g/g/b/c$a;

    const-string v4, "ON_DRAWABLE_SHOW"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lf/g/g/b/c$a;->t:Lf/g/g/b/c$a;

    new-instance v4, Lf/g/g/b/c$a;

    const-string v2, "ON_DRAWABLE_HIDE"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lf/g/g/b/c$a;->u:Lf/g/g/b/c$a;

    new-instance v2, Lf/g/g/b/c$a;

    const-string v6, "ON_ACTIVITY_START"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lf/g/g/b/c$a;->v:Lf/g/g/b/c$a;

    new-instance v6, Lf/g/g/b/c$a;

    const-string v4, "ON_ACTIVITY_STOP"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lf/g/g/b/c$a;->w:Lf/g/g/b/c$a;

    new-instance v4, Lf/g/g/b/c$a;

    const-string v2, "ON_RUN_CLEAR_CONTROLLER"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lf/g/g/b/c$a;->x:Lf/g/g/b/c$a;

    new-instance v2, Lf/g/g/b/c$a;

    const-string v6, "ON_SCHEDULE_CLEAR_CONTROLLER"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lf/g/g/b/c$a;->y:Lf/g/g/b/c$a;

    new-instance v6, Lf/g/g/b/c$a;

    const-string v4, "ON_SAME_CONTROLLER_SKIPPED"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lf/g/g/b/c$a;->z:Lf/g/g/b/c$a;

    new-instance v2, Lf/g/g/b/c$a;

    const-string v4, "ON_SUBMIT_CACHE_HIT"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lf/g/g/b/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lf/g/g/b/c$a;->A:Lf/g/g/b/c$a;

    const/16 v4, 0x18

    new-array v4, v4, [Lf/g/g/b/c$a;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    const/16 v0, 0xd

    aput-object v16, v4, v0

    const/16 v0, 0xe

    aput-object v17, v4, v0

    const/16 v0, 0xf

    aput-object v18, v4, v0

    const/16 v0, 0x10

    aput-object v19, v4, v0

    const/16 v0, 0x11

    aput-object v20, v4, v0

    const/16 v0, 0x12

    aput-object v21, v4, v0

    const/16 v0, 0x13

    aput-object v22, v4, v0

    const/16 v0, 0x14

    aput-object v23, v4, v0

    const/16 v0, 0x15

    aput-object v24, v4, v0

    const/16 v0, 0x16

    aput-object v25, v4, v0

    const/16 v0, 0x17

    aput-object v2, v4, v0

    sput-object v4, Lf/g/g/b/c$a;->B:[Lf/g/g/b/c$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/g/g/b/c$a;
    .locals 1

    const-class v0, Lf/g/g/b/c$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/g/g/b/c$a;

    return-object p0
.end method

.method public static values()[Lf/g/g/b/c$a;
    .locals 1

    sget-object v0, Lf/g/g/b/c$a;->B:[Lf/g/g/b/c$a;

    invoke-virtual {v0}, [Lf/g/g/b/c$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/g/g/b/c$a;

    return-object v0
.end method
