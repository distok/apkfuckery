.class public Lf/g/g/b/b$a;
.super Ljava/lang/Object;
.source "DeferredReleaserConcurrentImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/g/g/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/g/g/b/b;


# direct methods
.method public constructor <init>(Lf/g/g/b/b;)V
    .locals 0

    iput-object p1, p0, Lf/g/g/b/b$a;->d:Lf/g/g/b/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lf/g/g/b/b$a;->d:Lf/g/g/b/b;

    iget-object v0, v0, Lf/g/g/b/b;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/g/g/b/b$a;->d:Lf/g/g/b/b;

    iget-object v2, v1, Lf/g/g/b/b;->e:Ljava/util/ArrayList;

    iget-object v3, v1, Lf/g/g/b/b;->d:Ljava/util/ArrayList;

    iput-object v3, v1, Lf/g/g/b/b;->e:Ljava/util/ArrayList;

    iput-object v2, v1, Lf/g/g/b/b;->d:Ljava/util/ArrayList;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lf/g/g/b/b$a;->d:Lf/g/g/b/b;

    iget-object v2, v2, Lf/g/g/b/b;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/g/g/b/a$a;

    invoke-interface {v2}, Lf/g/g/b/a$a;->release()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/g/g/b/b$a;->d:Lf/g/g/b/b;

    iget-object v0, v0, Lf/g/g/b/b;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
