.class public Lf/g/g/d/b/a;
.super Lf/g/g/c/c;
.source "ImageLoadingTimeControllerListener.java"


# instance fields
.field public a:J

.field public b:J

.field public c:Lf/g/g/d/b/b;


# direct methods
.method public constructor <init>(Lf/g/g/d/b/b;)V
    .locals 2

    invoke-direct {p0}, Lf/g/g/c/c;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/g/g/d/b/a;->a:J

    iput-wide v0, p0, Lf/g/g/d/b/a;->b:J

    iput-object p1, p0, Lf/g/g/d/b/a;->c:Lf/g/g/d/b/b;

    return-void
.end method


# virtual methods
.method public onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lf/g/g/d/b/a;->b:J

    iget-object p3, p0, Lf/g/g/d/b/a;->c:Lf/g/g/d/b/b;

    if-eqz p3, :cond_0

    iget-wide v0, p0, Lf/g/g/d/b/a;->a:J

    sub-long/2addr p1, v0

    check-cast p3, Lf/g/g/d/a;

    iput-wide p1, p3, Lf/g/g/d/a;->v:J

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public onSubmit(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lf/g/g/d/b/a;->a:J

    return-void
.end method
