.class public Lf/g/g/e/f;
.super Lf/g/g/e/b;
.source "FadeDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/g/g/e/f$a;
    }
.end annotation


# instance fields
.field public final l:[Landroid/graphics/drawable/Drawable;

.field public m:I

.field public n:I

.field public o:J

.field public p:[I

.field public q:[I

.field public r:I

.field public s:[Z

.field public t:I

.field public u:Lf/g/g/e/f$a;

.field public v:Z


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;)V
    .locals 4

    invoke-direct {p0, p1}, Lf/g/g/e/b;-><init>([Landroid/graphics/drawable/Drawable;)V

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "At least one layer required!"

    invoke-static {v0, v3}, Ls/a/b/b/a;->k(ZLjava/lang/Object;)V

    iput-object p1, p0, Lf/g/g/e/f;->l:[Landroid/graphics/drawable/Drawable;

    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, Lf/g/g/e/f;->p:[I

    array-length v3, p1

    new-array v3, v3, [I

    iput-object v3, p0, Lf/g/g/e/f;->q:[I

    const/16 v3, 0xff

    iput v3, p0, Lf/g/g/e/f;->r:I

    array-length p1, p1

    new-array p1, p1, [Z

    iput-object p1, p0, Lf/g/g/e/f;->s:[Z

    iput v1, p0, Lf/g/g/e/f;->t:I

    const/4 p1, 0x2

    iput p1, p0, Lf/g/g/e/f;->m:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object p1, p0, Lf/g/g/e/f;->p:[I

    aput v3, p1, v1

    iget-object p1, p0, Lf/g/g/e/f;->q:[I

    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object p1, p0, Lf/g/g/e/f;->q:[I

    aput v3, p1, v1

    iget-object p1, p0, Lf/g/g/e/f;->s:[Z

    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([ZZ)V

    iget-object p1, p0, Lf/g/g/e/f;->s:[Z

    aput-boolean v2, p1, v1

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    iget v0, p0, Lf/g/g/e/f;->m:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-eqz v0, :cond_4

    if-eq v0, v2, :cond_1

    if-eq v0, v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lf/g/g/e/f;->i()V

    :goto_0
    const/4 v0, 0x1

    goto :goto_5

    :cond_1
    iget v0, p0, Lf/g/g/e/f;->n:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Ls/a/b/b/a;->j(Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lf/g/g/e/f;->o:J

    sub-long/2addr v4, v6

    long-to-float v0, v4

    iget v4, p0, Lf/g/g/e/f;->n:I

    int-to-float v4, v4

    div-float/2addr v0, v4

    invoke-virtual {p0, v0}, Lf/g/g/e/f;->j(F)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v3, 0x1

    :goto_2
    iput v3, p0, Lf/g/g/e/f;->m:I

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lf/g/g/e/f;->i()V

    goto :goto_5

    :cond_4
    iget-object v0, p0, Lf/g/g/e/f;->q:[I

    iget-object v4, p0, Lf/g/g/e/f;->p:[I

    iget-object v5, p0, Lf/g/g/e/f;->l:[Landroid/graphics/drawable/Drawable;

    array-length v5, v5

    invoke-static {v0, v1, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lf/g/g/e/f;->o:J

    iget v0, p0, Lf/g/g/e/f;->n:I

    if-nez v0, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lf/g/g/e/f;->j(F)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_4

    :cond_6
    const/4 v3, 0x1

    :goto_4
    iput v3, p0, Lf/g/g/e/f;->m:I

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lf/g/g/e/f;->i()V

    :cond_7
    :goto_5
    iget-object v3, p0, Lf/g/g/e/f;->l:[Landroid/graphics/drawable/Drawable;

    array-length v4, v3

    if-ge v1, v4, :cond_9

    aget-object v3, v3, v1

    iget-object v4, p0, Lf/g/g/e/f;->q:[I

    aget v4, v4, v1

    iget v5, p0, Lf/g/g/e/f;->r:I

    mul-int v4, v4, v5

    div-int/lit16 v4, v4, 0xff

    if-eqz v3, :cond_8

    if-lez v4, :cond_8

    iget v5, p0, Lf/g/g/e/f;->t:I

    add-int/2addr v5, v2

    iput v5, p0, Lf/g/g/e/f;->t:I

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget v4, p0, Lf/g/g/e/f;->t:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lf/g/g/e/f;->t:I

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    if-nez v0, :cond_a

    invoke-virtual {p0}, Lf/g/g/e/f;->invalidateSelf()V

    :cond_a
    return-void
.end method

.method public e()V
    .locals 1

    iget v0, p0, Lf/g/g/e/f;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/g/g/e/f;->t:I

    return-void
.end method

.method public f()V
    .locals 1

    iget v0, p0, Lf/g/g/e/f;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lf/g/g/e/f;->t:I

    invoke-virtual {p0}, Lf/g/g/e/f;->invalidateSelf()V

    return-void
.end method

.method public g()V
    .locals 4

    const/4 v0, 0x2

    iput v0, p0, Lf/g/g/e/f;->m:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lf/g/g/e/f;->l:[Landroid/graphics/drawable/Drawable;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lf/g/g/e/f;->q:[I

    iget-object v3, p0, Lf/g/g/e/f;->s:[Z

    aget-boolean v3, v3, v1

    if-eqz v3, :cond_0

    const/16 v3, 0xff

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/g/g/e/f;->invalidateSelf()V

    return-void
.end method

.method public getAlpha()I
    .locals 1

    iget v0, p0, Lf/g/g/e/f;->r:I

    return v0
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lf/g/g/e/f;->u:Lf/g/g/e/f$a;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lf/g/g/e/f;->v:Z

    if-eqz v1, :cond_0

    check-cast v0, Lf/g/g/c/a;

    iget-object v0, v0, Lf/g/g/c/a;->a:Lcom/facebook/drawee/controller/AbstractDraweeController;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g/g/e/f;->v:Z

    :cond_0
    return-void
.end method

.method public invalidateSelf()V
    .locals 1

    iget v0, p0, Lf/g/g/e/f;->t:I

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public final j(F)Z
    .locals 9

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    :goto_0
    iget-object v4, p0, Lf/g/g/e/f;->l:[Landroid/graphics/drawable/Drawable;

    array-length v4, v4

    if-ge v2, v4, :cond_5

    iget-object v4, p0, Lf/g/g/e/f;->s:[Z

    aget-boolean v5, v4, v2

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, -0x1

    :goto_1
    iget-object v6, p0, Lf/g/g/e/f;->q:[I

    iget-object v7, p0, Lf/g/g/e/f;->p:[I

    aget v7, v7, v2

    int-to-float v7, v7

    const/16 v8, 0xff

    mul-int/lit16 v5, v5, 0xff

    int-to-float v5, v5

    mul-float v5, v5, p1

    add-float/2addr v5, v7

    float-to-int v5, v5

    aput v5, v6, v2

    aget v5, v6, v2

    if-gez v5, :cond_1

    aput v1, v6, v2

    :cond_1
    aget v5, v6, v2

    if-le v5, v8, :cond_2

    aput v8, v6, v2

    :cond_2
    aget-boolean v5, v4, v2

    if-eqz v5, :cond_3

    aget v5, v6, v2

    if-ge v5, v8, :cond_3

    const/4 v3, 0x0

    :cond_3
    aget-boolean v4, v4, v2

    if-nez v4, :cond_4

    aget v4, v6, v2

    if-lez v4, :cond_4

    const/4 v3, 0x0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return v3
.end method

.method public setAlpha(I)V
    .locals 1

    iget v0, p0, Lf/g/g/e/f;->r:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lf/g/g/e/f;->r:I

    invoke-virtual {p0}, Lf/g/g/e/f;->invalidateSelf()V

    :cond_0
    return-void
.end method
