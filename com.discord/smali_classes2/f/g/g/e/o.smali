.class public Lf/g/g/e/o;
.super Lf/g/g/e/n;
.source "RoundedNinePatchDrawable.java"


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/NinePatchDrawable;)V
    .locals 0

    invoke-direct {p0, p1}, Lf/g/g/e/n;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-virtual {p0}, Lf/g/g/e/n;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lf/g/g/e/n;->draw(Landroid/graphics/Canvas;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_0
    invoke-virtual {p0}, Lf/g/g/e/n;->h()V

    invoke-virtual {p0}, Lf/g/g/e/n;->g()V

    iget-object v0, p0, Lf/g/g/e/n;->h:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    invoke-super {p0, p1}, Lf/g/g/e/n;->draw(Landroid/graphics/Canvas;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method
