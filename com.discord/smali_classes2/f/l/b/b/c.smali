.class public Lf/l/b/b/c;
.super Lf/l/b/b/a;
.source "GlRect.kt"


# static fields
.field public static final d:[F


# instance fields
.field public c:Ljava/nio/FloatBuffer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lf/l/b/b/c;->d:[F

    return-void

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lf/l/b/b/a;-><init>()V

    sget-object v0, Lf/l/b/b/c;->d:[F

    array-length v1, v0

    invoke-static {v1}, Lf/h/a/f/f/n/g;->r(I)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    iput-object v1, p0, Lf/l/b/b/c;->c:Ljava/nio/FloatBuffer;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const-string v0, "glDrawArrays start"

    invoke-static {v0}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    sget v0, Lf/l/b/c/a;->a:F

    iget-object v0, p0, Lf/l/b/b/c;->c:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->limit()I

    move-result v0

    iget v1, p0, Lf/l/b/b/a;->b:I

    div-int/2addr v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    const-string v0, "glDrawArrays end"

    invoke-static {v0}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()Ljava/nio/FloatBuffer;
    .locals 1

    iget-object v0, p0, Lf/l/b/b/c;->c:Ljava/nio/FloatBuffer;

    return-object v0
.end method
