.class public final Lf/l/b/e/a;
.super Lx/m/c/k;
.source "GlTexture.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $internalFormat:Ljava/lang/Integer;

.field public final synthetic this$0:Lf/l/b/e/b;


# direct methods
.method public constructor <init>(Lf/l/b/e/b;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iput-object p2, p0, Lf/l/b/e/a;->$internalFormat:Ljava/lang/Integer;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke()Ljava/lang/Object;
    .locals 12

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget-object v1, v0, Lf/l/b/e/b;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/l/b/e/b;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/l/b/e/b;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/l/b/e/a;->$internalFormat:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lf/l/b/e/b;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget v3, v0, Lf/l/b/e/b;->c:I

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget-object v0, v0, Lf/l/b/e/b;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget-object v0, v0, Lf/l/b/e/b;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget-object v0, v0, Lf/l/b/e/b;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget-object v0, v0, Lf/l/b/e/b;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    :cond_0
    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget v0, v0, Lf/l/b/e/b;->c:I

    sget v1, Lf/l/b/c/a;->a:F

    const/16 v1, 0x2801

    sget v2, Lf/l/b/c/a;->a:F

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget v0, v0, Lf/l/b/e/b;->c:I

    const/16 v1, 0x2800

    sget v2, Lf/l/b/c/a;->b:F

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget v0, v0, Lf/l/b/e/b;->c:I

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    iget-object v0, p0, Lf/l/b/e/a;->this$0:Lf/l/b/e/b;

    iget v0, v0, Lf/l/b/e/b;->c:I

    const/16 v1, 0x2803

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    const-string v0, "glTexParameter"

    invoke-static {v0}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method
