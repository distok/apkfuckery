.class public final Lf/l/b/e/b;
.super Ljava/lang/Object;
.source "GlTexture.kt"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/Integer;

.field public final e:Ljava/lang/Integer;

.field public final f:Ljava/lang/Integer;

.field public final g:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(IILjava/lang/Integer;I)V
    .locals 3

    and-int/lit8 p3, p4, 0x1

    if-eqz p3, :cond_0

    sget p1, Lf/l/b/c/a;->a:F

    const p1, 0x84c0

    :cond_0
    and-int/lit8 p3, p4, 0x2

    if-eqz p3, :cond_1

    sget p2, Lf/l/b/c/a;->a:F

    const p2, 0x8d65

    :cond_1
    and-int/lit8 p3, p4, 0x4

    const/4 p4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/l/b/e/b;->b:I

    iput p2, p0, Lf/l/b/e/b;->c:I

    iput-object p4, p0, Lf/l/b/e/b;->d:Ljava/lang/Integer;

    iput-object p4, p0, Lf/l/b/e/b;->e:Ljava/lang/Integer;

    iput-object p4, p0, Lf/l/b/e/b;->f:Ljava/lang/Integer;

    iput-object p4, p0, Lf/l/b/e/b;->g:Ljava/lang/Integer;

    const/4 p1, 0x1

    new-array p2, p1, [I

    const-string p3, "storage"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-array p3, p1, [I

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_2

    aget v2, p2, v1

    aput v2, p3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p1, p3, v0}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p1, :cond_3

    aget v2, p3, v1

    aput v2, p2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string p1, "glGenTextures"

    invoke-static {p1}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    aget p1, p2, v0

    iput p1, p0, Lf/l/b/e/b;->a:I

    new-instance p1, Lf/l/b/e/a;

    invoke-direct {p1, p0, p4}, Lf/l/b/e/a;-><init>(Lf/l/b/e/b;Ljava/lang/Integer;)V

    const-string p2, "$this$use"

    invoke-static {p0, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "block"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/l/b/e/b;->a()V

    invoke-virtual {p1}, Lf/l/b/e/a;->invoke()Ljava/lang/Object;

    invoke-virtual {p0}, Lf/l/b/e/b;->b()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget v0, p0, Lf/l/b/e/b;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    iget v0, p0, Lf/l/b/e/b;->c:I

    iget v1, p0, Lf/l/b/e/b;->a:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    const-string v0, "bind"

    invoke-static {v0}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 2

    iget v0, p0, Lf/l/b/e/b;->c:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    sget v0, Lf/l/b/c/a;->a:F

    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    const-string v0, "unbind"

    invoke-static {v0}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    return-void
.end method
