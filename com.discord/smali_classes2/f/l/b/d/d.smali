.class public Lf/l/b/d/d;
.super Lf/l/b/d/a;
.source "GlTextureProgram.kt"


# instance fields
.field public e:[F

.field public final f:Lf/l/b/d/b;

.field public g:Ljava/nio/FloatBuffer;

.field public final h:Lf/l/b/d/b;

.field public final i:Lf/l/b/d/b;

.field public final j:Lf/l/b/d/b;

.field public final k:Landroid/graphics/RectF;

.field public l:I

.field public m:Lf/l/b/b/a;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const-string v0, "vertexPositionName"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "vertexMvpMatrixName"

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lf/l/b/d/b$a;->d:Lf/l/b/d/b$a;

    sget-object v3, Lf/l/b/d/b$a;->e:Lf/l/b/d/b$a;

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v1, v0, [Lf/l/b/d/c;

    invoke-direct {p0, p1, v0, v1}, Lf/l/b/d/a;-><init>(IZ[Lf/l/b/d/c;)V

    sget-object p1, Lf/l/b/a/a;->a:[F

    invoke-static {p1}, Lf/h/a/f/f/n/g;->N([F)[F

    move-result-object p1

    iput-object p1, p0, Lf/l/b/d/d;->e:[F

    const-string p1, "name"

    invoke-static {p5, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lf/l/b/d/a;->b:I

    invoke-static {p5, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lf/l/b/d/b;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v3, p5, v4}, Lf/l/b/d/b;-><init>(ILf/l/b/d/b$a;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v1, p0, Lf/l/b/d/d;->f:Lf/l/b/d/b;

    const/16 p5, 0x8

    invoke-static {p5}, Lf/h/a/f/f/n/g;->r(I)Ljava/nio/FloatBuffer;

    move-result-object p5

    iput-object p5, p0, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    invoke-static {p4, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget p5, p0, Lf/l/b/d/a;->b:I

    invoke-static {p4, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/l/b/d/b;

    invoke-direct {v0, p5, v2, p4, v4}, Lf/l/b/d/b;-><init>(ILf/l/b/d/b$a;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lf/l/b/d/d;->h:Lf/l/b/d/b;

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget p4, p0, Lf/l/b/d/a;->b:I

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p5, Lf/l/b/d/b;

    invoke-direct {p5, p4, v2, p2, v4}, Lf/l/b/d/b;-><init>(ILf/l/b/d/b$a;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p5, p0, Lf/l/b/d/d;->i:Lf/l/b/d/b;

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget p2, p0, Lf/l/b/d/a;->b:I

    invoke-static {p3, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lf/l/b/d/b;

    invoke-direct {p1, p2, v3, p3, v4}, Lf/l/b/d/b;-><init>(ILf/l/b/d/b$a;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lf/l/b/d/d;->j:Lf/l/b/d/b;

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lf/l/b/d/d;->k:Landroid/graphics/RectF;

    const/4 p1, -0x1

    iput p1, p0, Lf/l/b/d/d;->l:I

    return-void
.end method
