.class public Lf/l/a/s/b;
.super Ljava/lang/Object;
.source "MeteringRegions.java"


# instance fields
.field public final a:Ljava/util/List;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/s/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/l/a/s/a;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/l/a/s/b;->a:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/graphics/PointF;FF)Landroid/graphics/RectF;
    .locals 4
    .param p0    # Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Landroid/graphics/PointF;->x:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr p1, v2

    sub-float v3, v1, p1

    iget p0, p0, Landroid/graphics/PointF;->y:F

    div-float/2addr p2, v2

    sub-float v2, p0, p2

    add-float/2addr v1, p1

    add-float/2addr p0, p2

    invoke-direct {v0, v3, v2, v1, p0}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method


# virtual methods
.method public b(ILf/l/a/s/c;)Ljava/util/List;
    .locals 4
    .param p2    # Lf/l/a/s/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lf/l/a/s/c<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf/l/a/s/b;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lf/l/a/s/b;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/l/a/s/a;

    iget-object v3, v2, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v2, v2, Lf/l/a/s/a;->e:I

    invoke-interface {p2, v3, v2}, Lf/l/a/s/c;->a(Landroid/graphics/RectF;I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/4 p2, 0x0

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public c(Lf/l/a/s/c;)Lf/l/a/s/b;
    .locals 7
    .param p1    # Lf/l/a/s/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf/l/a/s/b;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/l/a/s/a;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Landroid/graphics/RectF;

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const v5, -0x800001

    invoke-direct {v3, v4, v4, v5, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    iget-object v5, v2, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v6, v5}, Landroid/graphics/PointF;->set(FF)V

    invoke-interface {p1, v4}, Lf/l/a/s/c;->b(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lf/l/a/s/a;->f(Landroid/graphics/RectF;Landroid/graphics/PointF;)V

    iget-object v5, v2, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v6, v5}, Landroid/graphics/PointF;->set(FF)V

    invoke-interface {p1, v4}, Lf/l/a/s/c;->b(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lf/l/a/s/a;->f(Landroid/graphics/RectF;Landroid/graphics/PointF;)V

    iget-object v5, v2, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->right:F

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v5}, Landroid/graphics/PointF;->set(FF)V

    invoke-interface {p1, v4}, Lf/l/a/s/c;->b(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lf/l/a/s/a;->f(Landroid/graphics/RectF;Landroid/graphics/PointF;)V

    iget-object v5, v2, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v6, v5}, Landroid/graphics/PointF;->set(FF)V

    invoke-interface {p1, v4}, Lf/l/a/s/c;->b(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lf/l/a/s/a;->f(Landroid/graphics/RectF;Landroid/graphics/PointF;)V

    new-instance v4, Lf/l/a/s/a;

    iget v2, v2, Lf/l/a/s/a;->e:I

    invoke-direct {v4, v3, v2}, Lf/l/a/s/a;-><init>(Landroid/graphics/RectF;I)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lf/l/a/s/b;

    invoke-direct {p1, v0}, Lf/l/a/s/b;-><init>(Ljava/util/List;)V

    return-object p1
.end method
