.class public final enum Lf/l/a/r/b;
.super Ljava/lang/Enum;
.source "AutoFocusTrigger.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/r/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/r/b;

.field public static final enum e:Lf/l/a/r/b;

.field public static final synthetic f:[Lf/l/a/r/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/l/a/r/b;

    const-string v1, "GESTURE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/l/a/r/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/l/a/r/b;->d:Lf/l/a/r/b;

    new-instance v1, Lf/l/a/r/b;

    const-string v3, "METHOD"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/l/a/r/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/l/a/r/b;->e:Lf/l/a/r/b;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/l/a/r/b;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/l/a/r/b;->f:[Lf/l/a/r/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/r/b;
    .locals 1

    const-class v0, Lf/l/a/r/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/r/b;

    return-object p0
.end method

.method public static values()[Lf/l/a/r/b;
    .locals 1

    sget-object v0, Lf/l/a/r/b;->f:[Lf/l/a/r/b;

    invoke-virtual {v0}, [Lf/l/a/r/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/r/b;

    return-object v0
.end method
