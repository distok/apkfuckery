.class public final Lf/l/a/q/a$a;
.super Ljava/lang/Object;
.source "CamcorderProfiles.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/q/a;->a(ILf/l/a/w/b;)Landroid/media/CamcorderProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lf/l/a/w/b;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lf/l/a/q/a$a;->d:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    check-cast p1, Lf/l/a/w/b;

    check-cast p2, Lf/l/a/w/b;

    iget v0, p1, Lf/l/a/w/b;->d:I

    iget p1, p1, Lf/l/a/w/b;->e:I

    mul-int v0, v0, p1

    int-to-long v0, v0

    iget-wide v2, p0, Lf/l/a/q/a$a;->d:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget p1, p2, Lf/l/a/w/b;->d:I

    iget p2, p2, Lf/l/a/w/b;->e:I

    mul-int p1, p1, p2

    int-to-long p1, p1

    iget-wide v2, p0, Lf/l/a/q/a$a;->d:J

    sub-long/2addr p1, v2

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide p1

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    if-nez v2, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    return p1
.end method
