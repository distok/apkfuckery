.class public Lf/l/a/q/d;
.super Ljava/lang/Object;
.source "OrientationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/q/d$c;
    }
.end annotation


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Landroid/content/Context;

.field public final c:Lf/l/a/q/d$c;

.field public final d:Landroid/view/OrientationEventListener;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public e:I

.field public final f:Landroid/hardware/display/DisplayManager$DisplayListener;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public g:I

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lf/l/a/q/d$c;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/q/d$c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lf/l/a/q/d;->a:Landroid/os/Handler;

    const/4 v0, -0x1

    iput v0, p0, Lf/l/a/q/d;->e:I

    iput v0, p0, Lf/l/a/q/d;->g:I

    iput-object p1, p0, Lf/l/a/q/d;->b:Landroid/content/Context;

    iput-object p2, p0, Lf/l/a/q/d;->c:Lf/l/a/q/d$c;

    new-instance p2, Lf/l/a/q/d$a;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x3

    invoke-direct {p2, p0, p1, v0}, Lf/l/a/q/d$a;-><init>(Lf/l/a/q/d;Landroid/content/Context;I)V

    iput-object p2, p0, Lf/l/a/q/d;->d:Landroid/view/OrientationEventListener;

    new-instance p1, Lf/l/a/q/d$b;

    invoke-direct {p1, p0}, Lf/l/a/q/d$b;-><init>(Lf/l/a/q/d;)V

    iput-object p1, p0, Lf/l/a/q/d;->f:Landroid/hardware/display/DisplayManager$DisplayListener;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lf/l/a/q/d;->b:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/16 v0, 0x10e

    return v0

    :cond_1
    const/16 v0, 0xb4

    return v0

    :cond_2
    const/16 v0, 0x5a

    return v0
.end method
