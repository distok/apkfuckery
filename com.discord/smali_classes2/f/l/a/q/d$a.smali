.class public Lf/l/a/q/d$a;
.super Landroid/view/OrientationEventListener;
.source "OrientationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/q/d;-><init>(Landroid/content/Context;Lf/l/a/q/d$c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/q/d;


# direct methods
.method public constructor <init>(Lf/l/a/q/d;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lf/l/a/q/d$a;->a:Lf/l/a/q/d;

    invoke-direct {p0, p2, p3}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    iget-object p1, p0, Lf/l/a/q/d$a;->a:Lf/l/a/q/d;

    iget p1, p1, Lf/l/a/q/d;->e:I

    if-eq p1, v1, :cond_4

    goto :goto_1

    :cond_0
    const/16 v1, 0x13b

    if-ge p1, v1, :cond_4

    const/16 v2, 0x2d

    if-ge p1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/16 v3, 0x87

    if-lt p1, v2, :cond_2

    if-ge p1, v3, :cond_2

    const/16 p1, 0x5a

    goto :goto_1

    :cond_2
    const/16 v2, 0xe1

    if-lt p1, v3, :cond_3

    if-ge p1, v2, :cond_3

    const/16 p1, 0xb4

    goto :goto_1

    :cond_3
    if-lt p1, v2, :cond_4

    if-ge p1, v1, :cond_4

    const/16 p1, 0x10e

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p1, 0x0

    :goto_1
    iget-object v1, p0, Lf/l/a/q/d$a;->a:Lf/l/a/q/d;

    iget v2, v1, Lf/l/a/q/d;->e:I

    if-eq p1, v2, :cond_6

    iput p1, v1, Lf/l/a/q/d;->e:I

    iget-object v1, v1, Lf/l/a/q/d;->c:Lf/l/a/q/d$c;

    check-cast v1, Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v2, v1, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "onDeviceOrientationChanged"

    aput-object v4, v3, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v2, v4, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, v1, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v2, v0, Lcom/otaliastudios/cameraview/CameraView;->p:Lf/l/a/q/d;

    iget v2, v2, Lf/l/a/q/d;->g:I

    iget-boolean v3, v0, Lcom/otaliastudios/cameraview/CameraView;->e:Z

    if-nez v3, :cond_5

    rsub-int v3, v2, 0x168

    rem-int/lit16 v3, v3, 0x168

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->e()Lf/l/a/m/t/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lf/l/a/m/t/a;->e(I)V

    iput v3, v0, Lf/l/a/m/t/a;->d:I

    invoke-virtual {v0}, Lf/l/a/m/t/a;->d()V

    goto :goto_2

    :cond_5
    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->e()Lf/l/a/m/t/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf/l/a/m/t/a;->e(I)V

    iput p1, v0, Lf/l/a/m/t/a;->d:I

    invoke-virtual {v0}, Lf/l/a/m/t/a;->d()V

    :goto_2
    add-int/2addr p1, v2

    rem-int/lit16 p1, p1, 0x168

    iget-object v0, v1, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->l:Landroid/os/Handler;

    new-instance v2, Lf/l/a/i;

    invoke-direct {v2, v1, p1}, Lf/l/a/i;-><init>(Lcom/otaliastudios/cameraview/CameraView$b;I)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_6
    return-void
.end method
