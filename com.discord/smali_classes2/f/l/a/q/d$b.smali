.class public Lf/l/a/q/d$b;
.super Ljava/lang/Object;
.source "OrientationHelper.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/q/d;-><init>(Landroid/content/Context;Lf/l/a/q/d$c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/q/d;


# direct methods
.method public constructor <init>(Lf/l/a/q/d;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/q/d$b;->a:Lf/l/a/q/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0

    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 9

    iget-object p1, p0, Lf/l/a/q/d$b;->a:Lf/l/a/q/d;

    iget v0, p1, Lf/l/a/q/d;->g:I

    invoke-virtual {p1}, Lf/l/a/q/d;->a()I

    move-result p1

    if-eq p1, v0, :cond_1

    iget-object v1, p0, Lf/l/a/q/d$b;->a:Lf/l/a/q/d;

    iput p1, v1, Lf/l/a/q/d;->g:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0xb4

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lf/l/a/q/d$b;->a:Lf/l/a/q/d;

    iget-object v1, v1, Lf/l/a/q/d;->c:Lf/l/a/q/d$c;

    check-cast v1, Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v4, v1, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "onDisplayOffsetChanged"

    aput-object v6, v5, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v5, v3

    const/4 p1, 0x2

    const-string v7, "recreate:"

    aput-object v7, v5, p1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    const/4 v8, 0x3

    aput-object v7, v5, v8

    invoke-virtual {v4, v3, v5}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v4, v1, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    invoke-virtual {v4}, Lcom/otaliastudios/cameraview/CameraView;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    new-array v4, p1, [Ljava/lang/Object;

    aput-object v6, v4, v2

    const-string v2, "restarting the camera."

    aput-object v2, v4, v3

    invoke-virtual {v0, p1, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, v1, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    invoke-virtual {p1}, Lcom/otaliastudios/cameraview/CameraView;->close()V

    iget-object p1, v1, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    invoke-virtual {p1}, Lcom/otaliastudios/cameraview/CameraView;->open()V

    :cond_1
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0

    return-void
.end method
