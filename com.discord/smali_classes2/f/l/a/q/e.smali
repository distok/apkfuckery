.class public Lf/l/a/q/e;
.super Ljava/lang/Object;
.source "WorkerHandler.java"


# static fields
.field public static final e:Lf/l/a/b;

.field public static final f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/WeakReference<",
            "Lf/l/a/q/e;",
            ">;>;"
        }
    .end annotation
.end field

.field public static g:Lf/l/a/q/e;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/os/HandlerThread;

.field public c:Landroid/os/Handler;

.field public d:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/q/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/q/e;->e:Lf/l/a/b;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    sput-object v0, Lf/l/a/q/e;->f:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/l/a/q/e;->a:Ljava/lang/String;

    new-instance v0, Lf/l/a/q/e$a;

    invoke-direct {v0, p0, p1}, Lf/l/a/q/e$a;-><init>(Lf/l/a/q/e;Ljava/lang/String;)V

    iput-object v0, p0, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Landroid/os/HandlerThread;->setDaemon(Z)V

    iget-object v0, p0, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lf/l/a/q/e;->c:Landroid/os/Handler;

    new-instance v0, Lf/l/a/q/e$b;

    invoke-direct {v0, p0}, Lf/l/a/q/e$b;-><init>(Lf/l/a/q/e;)V

    iput-object v0, p0, Lf/l/a/q/e;->d:Ljava/util/concurrent/Executor;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, p1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    new-instance p1, Lf/l/a/q/e$c;

    invoke-direct {p1, p0, v0}, Lf/l/a/q/e$c;-><init>(Lf/l/a/q/e;Ljava/util/concurrent/CountDownLatch;)V

    iget-object v1, p0, Lf/l/a/q/e;->c:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static a(Ljava/lang/String;)Lf/l/a/q/e;
    .locals 9
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    sget-object v0, Lf/l/a/q/e;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "get:"

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x1

    const/4 v6, 0x2

    if-eqz v1, :cond_3

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/l/a/q/e;

    if-eqz v1, :cond_2

    iget-object v7, v1, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, v1, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->isInterrupted()Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v0, Lf/l/a/q/e;->e:Lf/l/a/b;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    const-string v2, "Reusing cached worker handler."

    aput-object v2, v4, v5

    aput-object p0, v4, v6

    invoke-virtual {v0, v6, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-object v1

    :cond_0
    iget-object v7, v1, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7}, Landroid/os/HandlerThread;->interrupt()V

    invoke-virtual {v7}, Landroid/os/HandlerThread;->quit()Z

    :cond_1
    iget-object v1, v1, Lf/l/a/q/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/l/a/q/e;->e:Lf/l/a/b;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v3

    const-string v8, "Thread reference found, but not alive or interrupted."

    aput-object v8, v7, v5

    const-string v8, "Removing."

    aput-object v8, v7, v6

    aput-object p0, v7, v4

    invoke-virtual {v1, v6, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object v1, Lf/l/a/q/e;->e:Lf/l/a/b;

    new-array v7, v4, [Ljava/lang/Object;

    aput-object v2, v7, v3

    const-string v8, "Thread reference died. Removing."

    aput-object v8, v7, v5

    aput-object p0, v7, v6

    invoke-virtual {v1, v6, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_0
    sget-object v1, Lf/l/a/q/e;->e:Lf/l/a/b;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    const-string v2, "Creating new handler."

    aput-object v2, v4, v5

    aput-object p0, v4, v6

    invoke-virtual {v1, v5, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v1, Lf/l/a/q/e;

    invoke-direct {v1, p0}, Lf/l/a/q/e;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method
