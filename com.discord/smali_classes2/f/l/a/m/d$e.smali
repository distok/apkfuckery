.class public Lf/l/a/m/d$e;
.super Ljava/lang/Object;
.source "Camera2Engine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/d;->G0(F[Landroid/graphics/PointF;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:F

.field public final synthetic e:Z

.field public final synthetic f:F

.field public final synthetic g:[Landroid/graphics/PointF;

.field public final synthetic h:Lf/l/a/m/d;


# direct methods
.method public constructor <init>(Lf/l/a/m/d;FZF[Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/d$e;->h:Lf/l/a/m/d;

    iput p2, p0, Lf/l/a/m/d$e;->d:F

    iput-boolean p3, p0, Lf/l/a/m/d$e;->e:Z

    iput p4, p0, Lf/l/a/m/d$e;->f:F

    iput-object p5, p0, Lf/l/a/m/d$e;->g:[Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lf/l/a/m/d$e;->h:Lf/l/a/m/d;

    iget-object v1, v0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget v2, p0, Lf/l/a/m/d$e;->d:F

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/d;->j1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/d$e;->h:Lf/l/a/m/d;

    invoke-virtual {v0}, Lf/l/a/m/d;->g1()V

    iget-boolean v0, p0, Lf/l/a/m/d$e;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/d$e;->h:Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    iget v1, p0, Lf/l/a/m/d$e;->f:F

    iget-object v2, p0, Lf/l/a/m/d$e;->g:[Landroid/graphics/PointF;

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v0, v1, v2}, Lcom/otaliastudios/cameraview/CameraView$b;->f(F[Landroid/graphics/PointF;)V

    :cond_0
    return-void
.end method
