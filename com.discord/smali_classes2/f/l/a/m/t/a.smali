.class public Lf/l/a/m/t/a;
.super Ljava/lang/Object;
.source "Angles.java"


# static fields
.field public static final e:Lf/l/a/b;


# instance fields
.field public a:Lf/l/a/l/e;

.field public b:I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public c:I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public d:I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/m/t/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/m/t/a;->e:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lf/l/a/m/t/a;->b:I

    iput v0, p0, Lf/l/a/m/t/a;->c:I

    iput v0, p0, Lf/l/a/m/t/a;->d:I

    return-void
.end method


# virtual methods
.method public final a(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)I
    .locals 2
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    sget-object v0, Lf/l/a/m/t/c;->d:Lf/l/a/m/t/c;

    if-ne p2, v0, :cond_1

    invoke-virtual {p0, p2, p1}, Lf/l/a/m/t/a;->a(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)I

    move-result p1

    rsub-int p1, p1, 0x168

    add-int/lit16 p1, p1, 0x168

    rem-int/lit16 p1, p1, 0x168

    return p1

    :cond_1
    if-ne p1, v0, :cond_5

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    iget p1, p0, Lf/l/a/m/t/a;->d:I

    add-int/lit16 p1, p1, 0x168

    rem-int/lit16 p1, p1, 0x168

    return p1

    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown reference: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    iget p1, p0, Lf/l/a/m/t/a;->c:I

    rsub-int p1, p1, 0x168

    add-int/lit16 p1, p1, 0x168

    rem-int/lit16 p1, p1, 0x168

    return p1

    :cond_4
    iget p1, p0, Lf/l/a/m/t/a;->b:I

    rsub-int p1, p1, 0x168

    add-int/lit16 p1, p1, 0x168

    rem-int/lit16 p1, p1, 0x168

    return p1

    :cond_5
    invoke-virtual {p0, v0, p2}, Lf/l/a/m/t/a;->a(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)I

    move-result p2

    invoke-virtual {p0, v0, p1}, Lf/l/a/m/t/a;->a(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)I

    move-result p1

    sub-int/2addr p2, p1

    add-int/lit16 p2, p2, 0x168

    rem-int/lit16 p2, p2, 0x168

    return p2
.end method

.method public b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z
    .locals 1
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/m/t/b;->d:Lf/l/a/m/t/b;

    invoke-virtual {p0, p1, p2, v0}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    move-result p1

    rem-int/lit16 p1, p1, 0xb4

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I
    .locals 0
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/l/a/m/t/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0, p1, p2}, Lf/l/a/m/t/a;->a(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)I

    move-result p1

    sget-object p2, Lf/l/a/m/t/b;->e:Lf/l/a/m/t/b;

    if-ne p3, p2, :cond_0

    iget-object p2, p0, Lf/l/a/m/t/a;->a:Lf/l/a/l/e;

    sget-object p3, Lf/l/a/l/e;->e:Lf/l/a/l/e;

    if-ne p2, p3, :cond_0

    rsub-int p1, p1, 0x168

    add-int/lit16 p1, p1, 0x168

    rem-int/lit16 p1, p1, 0x168

    :cond_0
    return p1
.end method

.method public final d()V
    .locals 5

    sget-object v0, Lf/l/a/m/t/a;->e:Lf/l/a/b;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Angles changed:"

    aput-object v3, v1, v2

    const-string v2, "sensorOffset:"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget v2, p0, Lf/l/a/m/t/a;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    const-string v4, "displayOffset:"

    aput-object v4, v1, v2

    iget v2, p0, Lf/l/a/m/t/a;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x4

    aput-object v2, v1, v4

    const/4 v2, 0x5

    const-string v4, "deviceOrientation:"

    aput-object v4, v1, v2

    iget v2, p0, Lf/l/a/m/t/a;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x6

    aput-object v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-void
.end method

.method public final e(I)V
    .locals 2

    if-eqz p1, :cond_1

    const/16 v0, 0x5a

    if-eq p1, v0, :cond_1

    const/16 v0, 0xb4

    if-eq p1, v0, :cond_1

    const/16 v0, 0x10e

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This value is not sanitized: "

    invoke-static {v1, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public f(Lf/l/a/l/e;I)V
    .locals 1
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0, p2}, Lf/l/a/m/t/a;->e(I)V

    iput-object p1, p0, Lf/l/a/m/t/a;->a:Lf/l/a/l/e;

    iput p2, p0, Lf/l/a/m/t/a;->b:I

    sget-object v0, Lf/l/a/l/e;->e:Lf/l/a/l/e;

    if-ne p1, v0, :cond_0

    rsub-int p1, p2, 0x168

    add-int/lit16 p1, p1, 0x168

    rem-int/lit16 p1, p1, 0x168

    iput p1, p0, Lf/l/a/m/t/a;->b:I

    :cond_0
    invoke-virtual {p0}, Lf/l/a/m/t/a;->d()V

    return-void
.end method
