.class public final enum Lf/l/a/m/t/c;
.super Ljava/lang/Enum;
.source "Reference.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/m/t/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/m/t/c;

.field public static final enum e:Lf/l/a/m/t/c;

.field public static final enum f:Lf/l/a/m/t/c;

.field public static final enum g:Lf/l/a/m/t/c;

.field public static final synthetic h:[Lf/l/a/m/t/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/l/a/m/t/c;

    const-string v1, "BASE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lf/l/a/m/t/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lf/l/a/m/t/c;->d:Lf/l/a/m/t/c;

    new-instance v1, Lf/l/a/m/t/c;

    const-string v3, "SENSOR"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lf/l/a/m/t/c;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    new-instance v3, Lf/l/a/m/t/c;

    const-string v5, "VIEW"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lf/l/a/m/t/c;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    new-instance v5, Lf/l/a/m/t/c;

    const-string v7, "OUTPUT"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lf/l/a/m/t/c;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/l/a/m/t/c;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/l/a/m/t/c;->h:[Lf/l/a/m/t/c;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/m/t/c;
    .locals 1

    const-class v0, Lf/l/a/m/t/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/m/t/c;

    return-object p0
.end method

.method public static values()[Lf/l/a/m/t/c;
    .locals 1

    sget-object v0, Lf/l/a/m/t/c;->h:[Lf/l/a/m/t/c;

    invoke-virtual {v0}, [Lf/l/a/m/t/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/m/t/c;

    return-object v0
.end method
