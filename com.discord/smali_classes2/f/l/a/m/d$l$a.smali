.class public Lf/l/a/m/d$l$a;
.super Lf/l/a/m/p/f;
.source "Camera2Engine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/d$l;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/m/r/g;

.field public final synthetic b:Lf/l/a/m/d$l;


# direct methods
.method public constructor <init>(Lf/l/a/m/d$l;Lf/l/a/m/r/g;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/d$l$a;->b:Lf/l/a/m/d$l;

    iput-object p2, p0, Lf/l/a/m/d$l$a;->a:Lf/l/a/m/r/g;

    invoke-direct {p0}, Lf/l/a/m/p/f;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lf/l/a/m/p/a;)V
    .locals 7
    .param p1    # Lf/l/a/m/p/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lf/l/a/m/d$l$a;->b:Lf/l/a/m/d$l;

    iget-object v0, p1, Lf/l/a/m/d$l;->g:Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    iget-object p1, p1, Lf/l/a/m/d$l;->d:Lf/l/a/p/a;

    iget-object v1, p0, Lf/l/a/m/d$l$a;->a:Lf/l/a/m/r/g;

    iget-object v1, v1, Lf/l/a/m/r/g;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "isSuccessful:"

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/l/a/m/r/a;

    iget-boolean v2, v2, Lf/l/a/m/r/a;->f:Z

    if-nez v2, :cond_0

    sget-object v1, Lf/l/a/m/r/g;->j:Lf/l/a/b;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v3, v2, v5

    const-string v3, "returning false."

    aput-object v3, v2, v6

    invoke-virtual {v1, v6, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget-object v1, Lf/l/a/m/r/g;->j:Lf/l/a/b;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v3, v2, v5

    const-string v3, "returning true."

    aput-object v3, v2, v6

    invoke-virtual {v1, v6, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v5, 0x1

    :goto_0
    iget-object v1, p0, Lf/l/a/m/d$l$a;->b:Lf/l/a/m/d$l;

    iget-object v1, v1, Lf/l/a/m/d$l;->e:Landroid/graphics/PointF;

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v0, p1, v5, v1}, Lcom/otaliastudios/cameraview/CameraView$b;->d(Lf/l/a/p/a;ZLandroid/graphics/PointF;)V

    iget-object p1, p0, Lf/l/a/m/d$l$a;->b:Lf/l/a/m/d$l;

    iget-object p1, p1, Lf/l/a/m/d$l;->g:Lf/l/a/m/d;

    iget-object p1, p1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v0, "reset metering"

    invoke-virtual {p1, v0}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object p1, p0, Lf/l/a/m/d$l$a;->b:Lf/l/a/m/d$l;

    iget-object p1, p1, Lf/l/a/m/d$l;->g:Lf/l/a/m/d;

    invoke-virtual {p1}, Lf/l/a/m/h;->X0()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lf/l/a/m/d$l$a;->b:Lf/l/a/m/d$l;

    iget-object p1, p1, Lf/l/a/m/d$l;->g:Lf/l/a/m/d;

    iget-object v1, p1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v2, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    iget-wide v3, p1, Lf/l/a/m/h;->Q:J

    new-instance p1, Lf/l/a/m/d$l$a$a;

    invoke-direct {p1, p0}, Lf/l/a/m/d$l$a$a;-><init>(Lf/l/a/m/d$l$a;)V

    new-instance v5, Lf/l/a/m/v/e;

    invoke-direct {v5, v1, v2, p1}, Lf/l/a/m/v/e;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v0, v3, v4, v5}, Lf/l/a/m/v/a;->f(Ljava/lang/String;JLjava/lang/Runnable;)V

    :cond_2
    return-void
.end method
