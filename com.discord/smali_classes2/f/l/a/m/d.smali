.class public Lf/l/a/m/d;
.super Lf/l/a/m/h;
.source "Camera2Engine.java"

# interfaces
.implements Landroid/media/ImageReader$OnImageAvailableListener;
.implements Lf/l/a/m/p/c;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# instance fields
.field public final W:Landroid/hardware/camera2/CameraManager;

.field public X:Ljava/lang/String;

.field public Y:Landroid/hardware/camera2/CameraDevice;

.field public Z:Landroid/hardware/camera2/CameraCharacteristics;

.field public a0:Landroid/hardware/camera2/CameraCaptureSession;

.field public b0:Landroid/hardware/camera2/CaptureRequest$Builder;

.field public c0:Landroid/hardware/camera2/TotalCaptureResult;

.field public final d0:Lf/l/a/m/q/b;

.field public e0:Landroid/media/ImageReader;

.field public f0:Landroid/view/Surface;

.field public g0:Landroid/view/Surface;

.field public h0:Landroid/media/ImageReader;

.field public final i0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/m/p/a;",
            ">;"
        }
    .end annotation
.end field

.field public j0:Lf/l/a/m/r/g;

.field public final k0:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;


# direct methods
.method public constructor <init>(Lf/l/a/m/j$g;)V
    .locals 1

    invoke-direct {p0, p1}, Lf/l/a/m/h;-><init>(Lf/l/a/m/j$g;)V

    sget-object p1, Lf/l/a/m/q/b;->a:Lf/l/a/m/q/b;

    if-nez p1, :cond_0

    new-instance p1, Lf/l/a/m/q/b;

    invoke-direct {p1}, Lf/l/a/m/q/b;-><init>()V

    sput-object p1, Lf/l/a/m/q/b;->a:Lf/l/a/m/q/b;

    :cond_0
    sget-object p1, Lf/l/a/m/q/b;->a:Lf/l/a/m/q/b;

    iput-object p1, p0, Lf/l/a/m/d;->d0:Lf/l/a/m/q/b;

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object p1, p0, Lf/l/a/m/d;->i0:Ljava/util/List;

    new-instance p1, Lf/l/a/m/d$h;

    invoke-direct {p1, p0}, Lf/l/a/m/d$h;-><init>(Lf/l/a/m/d;)V

    iput-object p1, p0, Lf/l/a/m/d;->k0:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    iget-object p1, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast p1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {p1}, Lcom/otaliastudios/cameraview/CameraView$b;->g()Landroid/content/Context;

    move-result-object p1

    const-string v0, "camera"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/hardware/camera2/CameraManager;

    iput-object p1, p0, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    new-instance p1, Lf/l/a/m/p/g;

    invoke-direct {p1}, Lf/l/a/m/p/g;-><init>()V

    invoke-virtual {p1, p0}, Lf/l/a/m/p/e;->e(Lf/l/a/m/p/c;)V

    return-void
.end method

.method public static Y0(Lf/l/a/m/d;)V
    .locals 3

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    new-array v0, v0, [Lf/l/a/m/p/e;

    new-instance v1, Lf/l/a/m/g;

    invoke-direct {v1, p0}, Lf/l/a/m/g;-><init>(Lf/l/a/m/d;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lf/l/a/m/r/h;

    invoke-direct {v1}, Lf/l/a/m/r/h;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lf/l/a/m/p/h;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lf/l/a/m/p/h;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, p0}, Lf/l/a/m/p/e;->e(Lf/l/a/m/p/c;)V

    return-void
.end method


# virtual methods
.method public F0(Lf/l/a/l/m;)V
    .locals 4
    .param p1    # Lf/l/a/l/m;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    iput-object p1, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "white balance ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/d$c;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/d$c;-><init>(Lf/l/a/m/d;Lf/l/a/l/m;)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public G0(F[Landroid/graphics/PointF;Z)V
    .locals 10
    .param p2    # [Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget v2, p0, Lf/l/a/m/h;->x:F

    iput p1, p0, Lf/l/a/m/h;->x:F

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string/jumbo v6, "zoom"

    invoke-virtual {v0, v6}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object v7, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v8, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v9, Lf/l/a/m/d$e;

    move-object v0, v9

    move-object v1, p0

    move v3, p3

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lf/l/a/m/d$e;-><init>(Lf/l/a/m/d;FZF[Landroid/graphics/PointF;)V

    new-instance p1, Lf/l/a/m/v/c$c;

    invoke-direct {p1, v7, v8, v9}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance p2, Lf/l/a/m/v/a$a;

    invoke-direct {p2, v7, p1}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    invoke-virtual {v7, v6, p1, p2}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public I0(Lf/l/a/p/a;Lf/l/a/s/b;Landroid/graphics/PointF;)V
    .locals 4
    .param p1    # Lf/l/a/p/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lf/l/a/s/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "autofocus ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/d$l;

    invoke-direct {v3, p0, p1, p3, p2}, Lf/l/a/m/d$l;-><init>(Lf/l/a/m/d;Lf/l/a/p/a;Landroid/graphics/PointF;Lf/l/a/s/b;)V

    invoke-virtual {v0, v1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public P()Lcom/google/android/gms/tasks/Task;
    .locals 16
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "onStartBind:"

    aput-object v5, v3, v4

    const/4 v5, 0x1

    const-string v6, "Started"

    aput-object v6, v3, v5

    invoke-virtual {v0, v5, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iget-object v3, v1, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    invoke-virtual {v1, v3}, Lf/l/a/m/h;->P0(Lf/l/a/l/i;)Lf/l/a/w/b;

    move-result-object v3

    iput-object v3, v1, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    invoke-virtual/range {p0 .. p0}, Lf/l/a/m/h;->Q0()Lf/l/a/w/b;

    move-result-object v3

    iput-object v3, v1, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, v1, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v6}, Lf/l/a/v/a;->h()Ljava/lang/Class;

    move-result-object v6

    iget-object v7, v1, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v7}, Lf/l/a/v/a;->g()Ljava/lang/Object;

    move-result-object v7

    const-class v8, Landroid/view/SurfaceHolder;

    if-ne v6, v8, :cond_0

    :try_start_0
    new-instance v6, Lf/l/a/m/d$n;

    invoke-direct {v6, v1, v7}, Lf/l/a/m/d$n;-><init>(Lf/l/a/m/d;Ljava/lang/Object;)V

    sget-object v8, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-static {v8, v6}, Lf/h/a/f/f/n/g;->f(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v6

    invoke-static {v6}, Lf/h/a/f/f/n/g;->c(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    check-cast v7, Landroid/view/SurfaceHolder;

    invoke-interface {v7}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v6

    iput-object v6, v1, Lf/l/a/m/d;->g0:Landroid/view/Surface;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_0
    new-instance v2, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v2, v0, v5}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v2

    :cond_0
    const-class v8, Landroid/graphics/SurfaceTexture;

    if-ne v6, v8, :cond_e

    check-cast v7, Landroid/graphics/SurfaceTexture;

    iget-object v6, v1, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    iget v8, v6, Lf/l/a/w/b;->d:I

    iget v6, v6, Lf/l/a/w/b;->e:I

    invoke-virtual {v7, v8, v6}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    new-instance v6, Landroid/view/Surface;

    invoke-direct {v6, v7}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v6, v1, Lf/l/a/m/d;->g0:Landroid/view/Surface;

    :goto_1
    iget-object v6, v1, Lf/l/a/m/d;->g0:Landroid/view/Surface;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, v1, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v7, Lf/l/a/l/i;->d:Lf/l/a/l/i;

    if-ne v6, v7, :cond_3

    iget-object v6, v1, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    if-eqz v6, :cond_2

    if-ne v6, v5, :cond_1

    const/16 v6, 0x20

    goto :goto_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unknown format:"

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/16 v6, 0x100

    :goto_2
    iget-object v7, v1, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    iget v8, v7, Lf/l/a/w/b;->d:I

    iget v7, v7, Lf/l/a/w/b;->e:I

    invoke-static {v8, v7, v6, v2}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v6

    iput-object v6, v1, Lf/l/a/m/d;->h0:Landroid/media/ImageReader;

    invoke-virtual {v6}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-boolean v6, v1, Lf/l/a/m/h;->p:Z

    if-eqz v6, :cond_d

    invoke-virtual/range {p0 .. p0}, Lf/l/a/m/d;->n1()Ljava/util/List;

    move-result-object v6

    iget-object v7, v1, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v8, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v9, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v7, v8, v9}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lf/l/a/w/b;

    if-eqz v7, :cond_4

    invoke-virtual {v9}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v9

    :cond_4
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    iget-object v6, v1, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    iget v9, v6, Lf/l/a/w/b;->d:I

    iget v6, v6, Lf/l/a/w/b;->e:I

    invoke-static {v9, v6}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v6

    if-eqz v7, :cond_6

    iget v9, v6, Lf/l/a/w/a;->e:I

    iget v6, v6, Lf/l/a/w/a;->d:I

    invoke-static {v9, v6}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v6

    :cond_6
    iget v9, v1, Lf/l/a/m/h;->T:I

    iget v10, v1, Lf/l/a/m/h;->U:I

    const v11, 0x7fffffff

    if-lez v9, :cond_7

    if-ne v9, v11, :cond_8

    :cond_7
    const/16 v9, 0x280

    :cond_8
    if-lez v10, :cond_9

    if-ne v10, v11, :cond_a

    :cond_9
    const/16 v10, 0x280

    :cond_a
    new-instance v11, Lf/l/a/w/b;

    invoke-direct {v11, v9, v10}, Lf/l/a/w/b;-><init>(II)V

    sget-object v9, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v10, 0x5

    new-array v12, v10, [Ljava/lang/Object;

    const-string v13, "computeFrameProcessingSize:"

    aput-object v13, v12, v4

    const-string v14, "targetRatio:"

    aput-object v14, v12, v5

    aput-object v6, v12, v2

    const/4 v14, 0x3

    const-string v15, "targetMaxSize:"

    aput-object v15, v12, v14

    const/4 v15, 0x4

    aput-object v11, v12, v15

    invoke-virtual {v9, v5, v12}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v6}, Lf/l/a/w/a;->i()F

    move-result v6

    new-instance v15, Lf/l/a/w/h;

    invoke-direct {v15, v6, v12}, Lf/l/a/w/h;-><init>(FF)V

    invoke-static {v15}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v6

    new-array v12, v14, [Lf/l/a/w/c;

    iget v15, v11, Lf/l/a/w/b;->e:I

    invoke-static {v15}, Lf/h/a/f/f/n/g;->O(I)Lf/l/a/w/c;

    move-result-object v15

    aput-object v15, v12, v4

    iget v11, v11, Lf/l/a/w/b;->d:I

    invoke-static {v11}, Lf/h/a/f/f/n/g;->P(I)Lf/l/a/w/c;

    move-result-object v11

    aput-object v11, v12, v5

    new-instance v11, Lf/l/a/w/i;

    invoke-direct {v11}, Lf/l/a/w/i;-><init>()V

    aput-object v11, v12, v2

    invoke-static {v12}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v11

    new-array v12, v14, [Lf/l/a/w/c;

    new-array v15, v2, [Lf/l/a/w/c;

    aput-object v6, v15, v4

    aput-object v11, v15, v5

    invoke-static {v15}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v6

    aput-object v6, v12, v4

    aput-object v11, v12, v5

    new-instance v6, Lf/l/a/w/j;

    invoke-direct {v6}, Lf/l/a/w/j;-><init>()V

    aput-object v6, v12, v2

    invoke-static {v12}, Lf/h/a/f/f/n/g;->V([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v6

    check-cast v6, Lf/l/a/w/p;

    invoke-virtual {v6, v8}, Lf/l/a/w/p;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/l/a/w/b;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    if-eqz v7, :cond_b

    invoke-virtual {v6}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v6

    :cond_b
    new-array v8, v10, [Ljava/lang/Object;

    aput-object v13, v8, v4

    const-string v4, "result:"

    aput-object v4, v8, v5

    aput-object v6, v8, v2

    const-string v2, "flip:"

    aput-object v2, v8, v14

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v4, 0x4

    aput-object v2, v8, v4

    invoke-virtual {v9, v5, v8}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iput-object v6, v1, Lf/l/a/m/h;->n:Lf/l/a/w/b;

    iget v2, v6, Lf/l/a/w/b;->d:I

    iget v4, v6, Lf/l/a/w/b;->e:I

    iget v6, v1, Lf/l/a/m/h;->o:I

    iget v7, v1, Lf/l/a/m/h;->V:I

    add-int/2addr v7, v5

    invoke-static {v2, v4, v6, v7}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v2

    iput-object v2, v1, Lf/l/a/m/d;->e0:Landroid/media/ImageReader;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    iget-object v2, v1, Lf/l/a/m/d;->e0:Landroid/media/ImageReader;

    invoke-virtual {v2}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v2

    iput-object v2, v1, Lf/l/a/m/d;->f0:Landroid/view/Surface;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_c
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "SizeSelectors must not return Sizes other than those in the input list."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    const/4 v4, 0x0

    iput-object v4, v1, Lf/l/a/m/d;->e0:Landroid/media/ImageReader;

    iput-object v4, v1, Lf/l/a/m/h;->n:Lf/l/a/w/b;

    iput-object v4, v1, Lf/l/a/m/d;->f0:Landroid/view/Surface;

    :goto_4
    :try_start_1
    iget-object v2, v1, Lf/l/a/m/d;->Y:Landroid/hardware/camera2/CameraDevice;

    new-instance v5, Lf/l/a/m/d$o;

    invoke-direct {v5, v1, v0}, Lf/l/a/m/d$o;-><init>(Lf/l/a/m/d;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {v2, v3, v5, v4}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_2

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object v0

    :catch_2
    move-exception v0

    invoke-virtual {v1, v0}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object v0

    throw v0

    :cond_e
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unknown CameraPreview output class."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public Q()Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/l/a/c;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    :try_start_0
    iget-object v1, p0, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    iget-object v2, p0, Lf/l/a/m/d;->X:Ljava/lang/String;

    new-instance v3, Lf/l/a/m/d$m;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/d$m;-><init>(Lf/l/a/m/d;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object v0

    throw v0
.end method

.method public R()Lcom/google/android/gms/tasks/Task;
    .locals 10
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStartPreview:"

    aput-object v4, v2, v3

    const-string v5, "Dispatching onCameraPreviewStreamSizeChanged."

    const/4 v6, 0x1

    aput-object v5, v2, v6

    invoke-virtual {v0, v6, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast v2, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v2}, Lcom/otaliastudios/cameraview/CameraView$b;->h()V

    sget-object v2, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {p0, v2}, Lf/l/a/m/h;->C(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v7, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    iget v8, v5, Lf/l/a/w/b;->d:I

    iget v5, v5, Lf/l/a/w/b;->e:I

    invoke-virtual {v7, v8, v5}, Lf/l/a/v/a;->q(II)V

    iget-object v5, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    iget-object v7, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v8, Lf/l/a/m/t/c;->d:Lf/l/a/m/t/c;

    sget-object v9, Lf/l/a/m/t/b;->d:Lf/l/a/m/t/b;

    invoke-virtual {v7, v8, v2, v9}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    move-result v2

    invoke-virtual {v5, v2}, Lf/l/a/v/a;->p(I)V

    iget-boolean v2, p0, Lf/l/a/m/h;->p:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lf/l/a/m/h;->R0()Lf/l/a/o/c;

    move-result-object v2

    iget v5, p0, Lf/l/a/m/h;->o:I

    iget-object v7, p0, Lf/l/a/m/h;->n:Lf/l/a/w/b;

    iget-object v8, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    invoke-virtual {v2, v5, v7, v8}, Lf/l/a/o/c;->e(ILf/l/a/w/b;Lf/l/a/m/t/a;)V

    :cond_0
    new-array v2, v1, [Ljava/lang/Object;

    aput-object v4, v2, v3

    const-string v5, "Starting preview."

    aput-object v5, v2, v6

    invoke-virtual {v0, v6, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-array v2, v3, [Landroid/view/Surface;

    invoke-virtual {p0, v2}, Lf/l/a/m/d;->Z0([Landroid/view/Surface;)V

    invoke-virtual {p0, v3, v1}, Lf/l/a/m/d;->h1(ZI)V

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v2, "Started preview."

    aput-object v2, v1, v6

    invoke-virtual {v0, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v1, Lf/l/a/m/d$p;

    invoke-direct {v1, p0, v0}, Lf/l/a/m/d$p;-><init>(Lf/l/a/m/d;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {v1, p0}, Lf/l/a/m/p/e;->e(Lf/l/a/m/p/c;)V

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "previewStreamSize should not be null at this point."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public S()Lcom/google/android/gms/tasks/Task;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStopBind:"

    aput-object v4, v2, v3

    const-string v5, "About to clean up."

    const/4 v6, 0x1

    aput-object v5, v2, v6

    invoke-virtual {v0, v6, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lf/l/a/m/d;->f0:Landroid/view/Surface;

    iput-object v2, p0, Lf/l/a/m/d;->g0:Landroid/view/Surface;

    iput-object v2, p0, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    iput-object v2, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    iput-object v2, p0, Lf/l/a/m/h;->n:Lf/l/a/w/b;

    iget-object v5, p0, Lf/l/a/m/d;->e0:Landroid/media/ImageReader;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/media/ImageReader;->close()V

    iput-object v2, p0, Lf/l/a/m/d;->e0:Landroid/media/ImageReader;

    :cond_0
    iget-object v5, p0, Lf/l/a/m/d;->h0:Landroid/media/ImageReader;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/media/ImageReader;->close()V

    iput-object v2, p0, Lf/l/a/m/d;->h0:Landroid/media/ImageReader;

    :cond_1
    iget-object v5, p0, Lf/l/a/m/d;->a0:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v5}, Landroid/hardware/camera2/CameraCaptureSession;->close()V

    iput-object v2, p0, Lf/l/a/m/d;->a0:Landroid/hardware/camera2/CameraCaptureSession;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v3, "Returning."

    aput-object v3, v1, v6

    invoke-virtual {v0, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public S0()Ljava/util/List;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, Lf/l/a/m/d;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_STREAM_CONFIGURATION_MAP:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/params/StreamConfigurationMap;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v1}, Lf/l/a/v/a;->h()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(Ljava/lang/Class;)[Landroid/util/Size;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    new-instance v5, Lf/l/a/w/b;

    invoke-virtual {v4}, Landroid/util/Size;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/util/Size;->getHeight()I

    move-result v4

    invoke-direct {v5, v6, v4}, Lf/l/a/w/b;-><init>(II)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "StreamConfigurationMap is null. Should not happen."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object v0

    throw v0
.end method

.method public T()Lcom/google/android/gms/tasks/Task;
    .locals 9
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "Clean up."

    const-string v1, "onStopEngine:"

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x1

    :try_start_0
    sget-object v6, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v1, v7, v3

    aput-object v0, v7, v5

    const-string v8, "Releasing camera."

    aput-object v8, v7, v4

    invoke-virtual {v6, v5, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v7, p0, Lf/l/a/m/d;->Y:Landroid/hardware/camera2/CameraDevice;

    invoke-virtual {v7}, Landroid/hardware/camera2/CameraDevice;->close()V

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v1, v7, v3

    aput-object v0, v7, v5

    const-string v8, "Released camera."

    aput-object v8, v7, v4

    invoke-virtual {v6, v5, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    sget-object v7, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v1, v8, v3

    aput-object v0, v8, v5

    const-string v0, "Exception while releasing camera."

    aput-object v0, v8, v4

    aput-object v6, v8, v2

    invoke-virtual {v7, v4, v8}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/m/d;->Y:Landroid/hardware/camera2/CameraDevice;

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v1, v6, v3

    const-string v7, "Aborting actions."

    aput-object v7, v6, v5

    invoke-virtual {v2, v5, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lf/l/a/m/d;->i0:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf/l/a/m/p/a;

    invoke-interface {v6, p0}, Lf/l/a/m/p/a;->a(Lf/l/a/m/p/c;)V

    goto :goto_1

    :cond_0
    iput-object v0, p0, Lf/l/a/m/d;->Z:Landroid/hardware/camera2/CameraCharacteristics;

    iput-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iput-object v0, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v1, v6, v3

    const-string v1, "Returning."

    aput-object v1, v6, v5

    invoke-virtual {v2, v4, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public U()Lcom/google/android/gms/tasks/Task;
    .locals 8
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStopPreview:"

    aput-object v4, v2, v3

    const/4 v5, 0x1

    const-string v6, "Started."

    aput-object v6, v2, v5

    invoke-virtual {v0, v5, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    iget-boolean v6, p0, Lf/l/a/m/h;->p:Z

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lf/l/a/m/h;->R0()Lf/l/a/o/c;

    move-result-object v6

    invoke-virtual {v6}, Lf/l/a/o/c;->d()V

    :cond_0
    iget-object v6, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v7, p0, Lf/l/a/m/d;->g0:Landroid/view/Surface;

    invoke-virtual {v6, v7}, Landroid/hardware/camera2/CaptureRequest$Builder;->removeTarget(Landroid/view/Surface;)V

    iget-object v6, p0, Lf/l/a/m/d;->f0:Landroid/view/Surface;

    if-eqz v6, :cond_1

    iget-object v7, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v7, v6}, Landroid/hardware/camera2/CaptureRequest$Builder;->removeTarget(Landroid/view/Surface;)V

    :cond_1
    iput-object v2, p0, Lf/l/a/m/d;->c0:Landroid/hardware/camera2/TotalCaptureResult;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v3, "Returning."

    aput-object v3, v1, v5

    invoke-virtual {v0, v5, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public U0(I)Lf/l/a/o/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Lf/l/a/o/e;

    invoke-direct {v0, p1}, Lf/l/a/o/e;-><init>(I)V

    return-object v0
.end method

.method public V0()V
    .locals 4

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "onPreviewStreamSizeChanged:"

    aput-object v3, v1, v2

    const-string v2, "Calling restartBind()."

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/l/a/m/j;->Y()Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public W0(Lf/l/a/k$a;Z)V
    .locals 5
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "onTakePicture:"

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz p2, :cond_0

    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v1

    const-string v0, "doMetering is true. Delaying."

    aput-object v0, v2, v3

    invoke-virtual {p2, v3, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const-wide/16 v0, 0x9c4

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Lf/l/a/m/d;->l1(Lf/l/a/s/b;)Lf/l/a/m/r/g;

    move-result-object p2

    new-instance v2, Lf/l/a/m/p/i;

    invoke-direct {v2, v0, v1, p2}, Lf/l/a/m/p/i;-><init>(JLf/l/a/m/p/e;)V

    new-instance p2, Lf/l/a/m/d$q;

    invoke-direct {p2, p0, p1}, Lf/l/a/m/d$q;-><init>(Lf/l/a/m/d;Lf/l/a/k$a;)V

    invoke-virtual {v2, p2}, Lf/l/a/m/p/e;->f(Lf/l/a/m/p/b;)V

    invoke-virtual {v2, p0}, Lf/l/a/m/p/e;->e(Lf/l/a/m/p/c;)V

    goto :goto_0

    :cond_0
    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v1

    const-string v0, "doMetering is false. Performing."

    aput-object v0, v4, v3

    invoke-virtual {p2, v3, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p2, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v0, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v1, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    sget-object v3, Lf/l/a/m/t/b;->e:Lf/l/a/m/t/b;

    invoke-virtual {p2, v0, v1, v3}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    move-result p2

    iput p2, p1, Lf/l/a/k$a;->b:I

    invoke-virtual {p0, v1}, Lf/l/a/m/h;->w(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object p2

    iput-object p2, p1, Lf/l/a/k$a;->c:Lf/l/a/w/b;

    :try_start_0
    iget-object p2, p0, Lf/l/a/m/d;->Y:Landroid/hardware/camera2/CameraDevice;

    invoke-virtual {p2, v2}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object p2

    iget-object v0, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {p0, p2, v0}, Lf/l/a/m/d;->a1(Landroid/hardware/camera2/CaptureRequest$Builder;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    new-instance v0, Lf/l/a/u/b;

    iget-object v1, p0, Lf/l/a/m/d;->h0:Landroid/media/ImageReader;

    invoke-direct {v0, p1, p0, p2, v1}, Lf/l/a/u/b;-><init>(Lf/l/a/k$a;Lf/l/a/m/d;Landroid/hardware/camera2/CaptureRequest$Builder;Landroid/media/ImageReader;)V

    iput-object v0, p0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    invoke-virtual {v0}, Lf/l/a/u/b;->c()V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object p1

    throw p1
.end method

.method public final varargs Z0([Landroid/view/Surface;)V
    .locals 4
    .param p1    # [Landroid/view/Surface;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v1, p0, Lf/l/a/m/d;->g0:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    iget-object v0, p0, Lf/l/a/m/d;->f0:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    :cond_0
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p1, v1

    if-eqz v2, :cond_1

    iget-object v3, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v3, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Should not add a null surface."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-void
.end method

.method public a(Lf/l/a/k$a;Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    instance-of v0, v0, Lf/l/a/u/b;

    invoke-super {p0, p1, p2}, Lf/l/a/m/h;->a(Lf/l/a/k$a;Ljava/lang/Exception;)V

    const/4 p1, 0x1

    if-eqz v0, :cond_0

    iget-boolean p2, p0, Lf/l/a/m/h;->A:Z

    if-nez p2, :cond_1

    :cond_0
    if-nez v0, :cond_2

    iget-boolean p2, p0, Lf/l/a/m/h;->B:Z

    if-eqz p2, :cond_2

    :cond_1
    const/4 p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    iget-object p2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v0, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    new-instance v1, Lf/l/a/m/d$r;

    invoke-direct {v1, p0}, Lf/l/a/m/d$r;-><init>(Lf/l/a/m/d;)V

    new-instance v2, Lf/l/a/m/v/c$c;

    invoke-direct {v2, p2, v0, v1}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v0, Lf/l/a/m/v/a$a;

    invoke-direct {v0, p2, v2}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const-string v1, "reset metering after picture"

    invoke-virtual {p2, v1, p1, v0}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    :cond_3
    return-void
.end method

.method public final a1(Landroid/hardware/camera2/CaptureRequest$Builder;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 5
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "applyAllParameters:"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "called for tag"

    aput-object v3, v1, v2

    invoke-virtual {p1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/camera2/CaptureRequest;->getTag()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v1, v4

    invoke-virtual {v0, v2, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lf/l/a/m/d;->b1(Landroid/hardware/camera2/CaptureRequest$Builder;)V

    sget-object v0, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->d1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/f;)Z

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    if-eqz v0, :cond_0

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->JPEG_GPS_LOCATION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p1, v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lf/l/a/l/m;->d:Lf/l/a/l/m;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->i1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/m;)Z

    sget-object v0, Lf/l/a/l/h;->d:Lf/l/a/l/h;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->e1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/h;)Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->j1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->c1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->f1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z

    if-eqz p2, :cond_1

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_REGIONS:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_REGIONS:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AWB_REGIONS:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public b1(Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 6
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AF_AVAILABLE_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-array v3, v1, [I

    invoke-virtual {p0, v0, v3}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget v5, v0, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v1, Lf/l/a/l/i;->e:Lf/l/a/l/i;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return-void

    :cond_1
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return-void

    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return-void

    :cond_3
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p1, v0, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->LENS_FOCUS_DISTANCE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public final c(Lf/l/a/l/e;)Z
    .locals 10
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/d;->d0:Lf/l/a/m/q/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/l/a/m/q/b;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v1}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_1

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "collectCameraInfo"

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "Facing:"

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const/4 v4, 0x2

    aput-object p1, v3, v4

    const/4 v4, 0x3

    const-string v7, "Internal:"

    aput-object v7, v3, v4

    const/4 v4, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x5

    const-string v7, "Cameras:"

    aput-object v7, v3, v4

    const/4 v4, 0x6

    array-length v7, v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-virtual {v2, v6, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    :try_start_1
    iget-object v7, p0, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    invoke-virtual {v7, v4}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v7

    sget-object v8, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    const/16 v9, -0x63

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p0, v7, v8, v9}, Lf/l/a/m/d;->p1(Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v0, v8, :cond_0

    iput-object v4, p0, Lf/l/a/m/d;->X:Ljava/lang/String;

    sget-object v4, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_ORIENTATION:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p0, v7, v4, v8}, Lf/l/a/m/d;->p1(Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v7, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    invoke-virtual {v7, p1, v4}, Lf/l/a/m/t/a;->f(Lf/l/a/l/e;I)V
    :try_end_1
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_1 .. :try_end_1} :catch_0

    return v6

    :catch_0
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v5

    :catch_1
    move-exception p1

    invoke-virtual {p0, p1}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object p1

    throw p1
.end method

.method public c1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z
    .locals 2
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-boolean v0, v0, Lf/l/a/c;->l:Z

    if-eqz v0, :cond_0

    sget-object p2, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AE_COMPENSATION_STEP:Landroid/hardware/camera2/CameraCharacteristics$Key;

    new-instance v0, Landroid/util/Rational;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v1}, Landroid/util/Rational;-><init>(II)V

    invoke-virtual {p0, p2, v0}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/util/Rational;

    iget v0, p0, Lf/l/a/m/h;->y:F

    invoke-virtual {p2}, Landroid/util/Rational;->floatValue()F

    move-result p2

    mul-float p2, p2, v0

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p2

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_EXPOSURE_COMPENSATION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return v1

    :cond_0
    iput p2, p0, Lf/l/a/m/h;->y:F

    const/4 p1, 0x0

    return p1
.end method

.method public d0(F[F[Landroid/graphics/PointF;Z)V
    .locals 11
    .param p2    # [F
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget v2, p0, Lf/l/a/m/h;->y:F

    iput p1, p0, Lf/l/a/m/h;->y:F

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v7, "exposure correction"

    invoke-virtual {v0, v7}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object v8, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v9, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v10, Lf/l/a/m/d$f;

    move-object v0, v10

    move-object v1, p0

    move v3, p4

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lf/l/a/m/d$f;-><init>(Lf/l/a/m/d;FZF[F[Landroid/graphics/PointF;)V

    new-instance p1, Lf/l/a/m/v/c$c;

    invoke-direct {p1, v8, v9, v10}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance p2, Lf/l/a/m/v/a$a;

    invoke-direct {p2, v8, p1}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    invoke-virtual {v8, v7, p1, p2}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public d1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/f;)Z
    .locals 9
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/l/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v1, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    invoke-virtual {v0, v1}, Lf/l/a/c;->c(Lf/l/a/l/c;)Z

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v0, :cond_6

    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AE_AVAILABLE_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    new-array v3, v1, [I

    invoke-virtual {p0, v0, v3}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_0

    aget v6, v0, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/l/a/m/d;->d0:Lf/l/a/m/q/b;

    iget-object v4, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eqz v4, :cond_4

    const/4 v7, 0x3

    if-eq v4, v6, :cond_3

    if-eq v4, v5, :cond_2

    if-eq v4, v7, :cond_1

    goto :goto_1

    :cond_1
    new-instance v4, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v4, v2, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v4, Landroid/util/Pair;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v4, v7, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Landroid/util/Pair;

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v4, v7, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v4, Landroid/util/Pair;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v4, v7, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    new-instance v4, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v4, v7, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v2, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v0, v5, [Ljava/lang/Object;

    const-string v3, "applyFlash: setting CONTROL_AE_MODE to"

    aput-object v3, v0, v1

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v3, v0, v6

    invoke-virtual {p2, v6, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/Object;

    const-string v3, "applyFlash: setting FLASH_MODE to"

    aput-object v3, v0, v1

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v1, v0, v6

    invoke-virtual {p2, v6, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    sget-object p2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {p1, p2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    sget-object p2, Landroid/hardware/camera2/CaptureRequest;->FLASH_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p1, p2, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return v6

    :cond_6
    iput-object p2, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    return v1
.end method

.method public e1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/h;)Z
    .locals 2
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/l/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v1, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    invoke-virtual {v0, v1}, Lf/l/a/c;->c(Lf/l/a/l/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lf/l/a/m/d;->d0:Lf/l/a/m/q/b;

    iget-object v0, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/l/a/m/q/b;->d:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_SCENE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput-object p2, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    const/4 p1, 0x0

    return p1
.end method

.method public f0(Lf/l/a/l/f;)V
    .locals 5
    .param p1    # Lf/l/a/l/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    iput-object p1, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "flash ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v4, Lf/l/a/m/d$a;

    invoke-direct {v4, p0, v0, p1}, Lf/l/a/m/d$a;-><init>(Lf/l/a/m/d;Lf/l/a/l/f;Lf/l/a/l/f;)V

    invoke-virtual {v1, v2, v3, v4}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public f1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z
    .locals 7
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x0

    new-array v1, v0, [Landroid/util/Range;

    sget-object v2, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p0, v2, v1}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/util/Range;

    iget-boolean v2, p0, Lf/l/a/m/h;->D:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    iget v2, p0, Lf/l/a/m/h;->C:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    new-instance v2, Lf/l/a/m/e;

    invoke-direct {v2, p0}, Lf/l/a/m/e;-><init>(Lf/l/a/m/d;)V

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_0
    new-instance v2, Lf/l/a/m/f;

    invoke-direct {v2, p0}, Lf/l/a/m/f;-><init>(Lf/l/a/m/d;)V

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    :goto_0
    iget v2, p0, Lf/l/a/m/h;->C:F

    const/4 v4, 0x1

    cmpl-float v3, v2, v3

    if-nez v3, :cond_3

    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_5

    aget-object v5, v1, v3

    const/16 v6, 0x1e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/util/Range;->contains(Ljava/lang/Comparable;)Z

    move-result v6

    if-nez v6, :cond_2

    const/16 v6, 0x18

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/util/Range;->contains(Ljava/lang/Comparable;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    sget-object p2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_TARGET_FPS_RANGE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p1, p2, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return v4

    :cond_3
    iget-object v3, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget v3, v3, Lf/l/a/c;->q:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iput v2, p0, Lf/l/a/m/h;->C:F

    iget-object v3, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget v3, v3, Lf/l/a/c;->p:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lf/l/a/m/h;->C:F

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_5

    aget-object v5, v1, v3

    iget v6, p0, Lf/l/a/m/h;->C:F

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/util/Range;->contains(Ljava/lang/Comparable;)Z

    move-result v6

    if-eqz v6, :cond_4

    sget-object p2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_TARGET_FPS_RANGE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p1, p2, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    return v4

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    iput p2, p0, Lf/l/a/m/h;->C:F

    return v0
.end method

.method public g0(I)V
    .locals 4

    iget v0, p0, Lf/l/a/m/h;->o:I

    if-nez v0, :cond_0

    const/16 v0, 0x23

    iput v0, p0, Lf/l/a/m/h;->o:I

    :cond_0
    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v1, "frame processing format ("

    const-string v2, ")"

    invoke-static {v1, p1, v2}, Lf/e/c/a/a;->k(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lf/l/a/m/d$k;

    invoke-direct {v3, p0, p1}, Lf/l/a/m/d$k;-><init>(Lf/l/a/m/d;I)V

    invoke-virtual {v0, v1, v2, v3}, Lf/l/a/m/v/a;->d(Ljava/lang/String;ZLjava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public g1()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lf/l/a/m/d;->h1(ZI)V

    return-void
.end method

.method public final h1(ZI)V
    .locals 4

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v0, v0, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v1, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lf/l/a/m/j;->O()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-nez p1, :cond_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lf/l/a/m/d;->a0:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v1, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object v2, p0, Lf/l/a/m/d;->k0:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "applyRepeatingRequestBuilder: session is invalid!"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 p2, 0x2

    const-string v2, "checkStarted:"

    aput-object v2, v1, p2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v1, p2

    const/4 p1, 0x4

    const-string v2, "currentThread:"

    aput-object v2, v1, p1

    const/4 p1, 0x5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, p1

    const/4 p1, 0x6

    const-string v2, "state:"

    aput-object v2, v1, p1

    const/4 p1, 0x7

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v3, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    aput-object v3, v1, p1

    const/16 p1, 0x8

    const-string v3, "targetState:"

    aput-object v3, v1, p1

    const/16 p1, 0x9

    iget-object v2, v2, Lf/l/a/m/v/c;->g:Lf/l/a/m/v/b;

    aput-object v2, v1, p1

    invoke-virtual {v0, p2, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance p1, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {p1, p2}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    throw p1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, p1, p2}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v0

    :cond_2
    :goto_0
    return-void
.end method

.method public i1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/m;)Z
    .locals 2
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/l/m;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v1, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    invoke-virtual {v0, v1}, Lf/l/a/c;->c(Lf/l/a/l/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lf/l/a/m/d;->d0:Lf/l/a/m/q/b;

    iget-object v0, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/l/a/m/q/b;->c:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AWB_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput-object p2, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    const/4 p1, 0x0

    return p1
.end method

.method public j1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z
    .locals 6
    .param p1    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-boolean v0, v0, Lf/l/a/c;->k:Z

    if-eqz v0, :cond_0

    sget-object p2, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:Landroid/hardware/camera2/CameraCharacteristics$Key;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    iget v1, p0, Lf/l/a/m/h;->x:F

    sub-float v2, p2, v0

    mul-float v1, v1, v2

    add-float/2addr v1, v0

    sget-object v3, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_INFO_ACTIVE_ARRAY_SIZE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v3, v4}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, p2

    float-to-int v4, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, p2

    float-to-int p2, v5

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, p2

    int-to-float p2, v5

    sub-float/2addr v1, v0

    mul-float p2, p2, v1

    div-float/2addr p2, v2

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr p2, v0

    float-to-int p2, p2

    int-to-float v4, v4

    mul-float v4, v4, v1

    div-float/2addr v4, v2

    div-float/2addr v4, v0

    float-to-int v0, v4

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v2, p2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-direct {v1, p2, v0, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object p2, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {p1, p2, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput p2, p0, Lf/l/a/m/h;->x:F

    const/4 p1, 0x0

    return p1
.end method

.method public k0(Z)V
    .locals 4

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "has frame processors ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lf/l/a/m/d$j;

    invoke-direct {v3, p0, p1}, Lf/l/a/m/d$j;-><init>(Lf/l/a/m/d;Z)V

    invoke-virtual {v0, v1, v2, v3}, Lf/l/a/m/v/a;->d(Ljava/lang/String;ZLjava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;
    .locals 4
    .param p1    # Landroid/hardware/camera2/CameraAccessException;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p1}, Landroid/hardware/camera2/CameraAccessException;->getReason()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :cond_1
    :goto_0
    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, p1, v1}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    return-object v0
.end method

.method public l0(Lf/l/a/l/h;)V
    .locals 4
    .param p1    # Lf/l/a/l/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    iput-object p1, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hdr ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/d$d;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/d$d;-><init>(Lf/l/a/m/d;Lf/l/a/l/h;)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final l1(Lf/l/a/s/b;)Lf/l/a/m/r/g;
    .locals 7
    .param p1    # Lf/l/a/s/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/d;->j0:Lf/l/a/m/r/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lf/l/a/m/p/e;->a(Lf/l/a/m/p/c;)V

    :cond_0
    iget-object v0, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AF_AVAILABLE_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    const/4 v2, 0x0

    new-array v3, v2, [I

    invoke-virtual {p0, v1, v3}, Lf/l/a/m/d;->o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_1

    aget v6, v1, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v5, Lf/l/a/l/i;->e:Lf/l/a/l/i;

    if-ne v4, v5, :cond_3

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    :cond_4
    :goto_1
    new-instance v0, Lf/l/a/m/r/g;

    if-nez p1, :cond_5

    const/4 v2, 0x1

    :cond_5
    invoke-direct {v0, p0, p1, v2}, Lf/l/a/m/r/g;-><init>(Lf/l/a/m/j;Lf/l/a/s/b;Z)V

    iput-object v0, p0, Lf/l/a/m/d;->j0:Lf/l/a/m/r/g;

    return-object v0
.end method

.method public m0(Landroid/location/Location;)V
    .locals 3
    .param p1    # Landroid/location/Location;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    iput-object p1, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v2, Lf/l/a/m/d$b;

    invoke-direct {v2, p0, v0}, Lf/l/a/m/d$b;-><init>(Lf/l/a/m/d;Landroid/location/Location;)V

    new-instance v0, Lf/l/a/m/v/c$c;

    invoke-direct {v0, p1, v1, v2}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v1, Lf/l/a/m/v/a$a;

    invoke-direct {v1, p1, v0}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    const-string v2, "location"

    invoke-virtual {p1, v2, v0, v1}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final m1(I)Landroid/hardware/camera2/CaptureRequest$Builder;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/hardware/camera2/CameraAccessException;
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v1, p0, Lf/l/a/m/d;->Y:Landroid/hardware/camera2/CameraDevice;

    invoke-virtual {v1, p1}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v1

    iput-object v1, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/hardware/camera2/CaptureRequest$Builder;->setTag(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/d;->a1(Landroid/hardware/camera2/CaptureRequest$Builder;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    iget-object p1, p0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    return-object p1
.end method

.method public n1()Ljava/util/List;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, Lf/l/a/m/d;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_STREAM_CONFIGURATION_MAP:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/params/StreamConfigurationMap;

    if-eqz v0, :cond_2

    iget v1, p0, Lf/l/a/m/h;->o:I

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(I)[Landroid/util/Size;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    array-length v2, v0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    new-instance v5, Lf/l/a/w/b;

    invoke-virtual {v4}, Landroid/util/Size;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/util/Size;->getHeight()I

    move-result v4

    invoke-direct {v5, v6, v4}, Lf/l/a/w/b;-><init>(II)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "StreamConfigurationMap is null. Should not happen."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object v0

    throw v0
.end method

.method public o1(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/hardware/camera2/CameraCharacteristics$Key;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/hardware/camera2/CameraCharacteristics$Key<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/d;->Z:Landroid/hardware/camera2/CameraCharacteristics;

    invoke-virtual {v0, p1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    return-object p2
.end method

.method public onImageAvailable(Landroid/media/ImageReader;)V
    .locals 9

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onImageAvailable:"

    aput-object v4, v2, v3

    const-string v5, "trying to acquire Image."

    const/4 v6, 0x1

    aput-object v5, v2, v6

    invoke-virtual {v0, v3, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/media/ImageReader;->acquireLatestImage()Landroid/media/Image;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    if-nez v0, :cond_0

    sget-object p1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object v4, v0, v3

    const-string v2, "failed to acquire Image!"

    aput-object v2, v0, v6

    invoke-virtual {p1, v1, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object p1, p1, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    if-ne p1, v2, :cond_2

    invoke-virtual {p0}, Lf/l/a/m/j;->O()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lf/l/a/m/h;->R0()Lf/l/a/o/c;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {p1, v0, v7, v8}, Lf/l/a/o/c;->a(Ljava/lang/Object;J)Lf/l/a/o/b;

    move-result-object p1

    if-eqz p1, :cond_1

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v2, "Image acquired, dispatching."

    aput-object v2, v1, v6

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v0, p1}, Lcom/otaliastudios/cameraview/CameraView$b;->b(Lf/l/a/o/b;)V

    goto :goto_1

    :cond_1
    sget-object p1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v0, v1, [Ljava/lang/Object;

    aput-object v4, v0, v3

    const-string v1, "Image acquired, but no free frames. DROPPING."

    aput-object v1, v0, v6

    invoke-virtual {p1, v6, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_1

    :cond_2
    sget-object p1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v2, "Image acquired in wrong state. Closing it now."

    aput-object v2, v1, v6

    invoke-virtual {p1, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v0}, Landroid/media/Image;->close()V

    :goto_1
    return-void
.end method

.method public p0(Lf/l/a/l/j;)V
    .locals 3
    .param p1    # Lf/l/a/l/j;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "picture format ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v2, Lf/l/a/m/d$i;

    invoke-direct {v2, p0}, Lf/l/a/m/d$i;-><init>(Lf/l/a/m/d;)V

    invoke-virtual {v0, p1, v1, v2}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final p1(Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1    # Landroid/hardware/camera2/CameraCharacteristics;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CameraCharacteristics$Key;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/hardware/camera2/CameraCharacteristics;",
            "Landroid/hardware/camera2/CameraCharacteristics$Key<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    invoke-virtual {p1, p2}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p3, p1

    :goto_0
    return-object p3
.end method

.method public t0(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/l/a/m/h;->z:Z

    const/4 p1, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public v0(F)V
    .locals 4

    iget v0, p0, Lf/l/a/m/h;->C:F

    iput p1, p0, Lf/l/a/m/h;->C:F

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preview fps ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/d$g;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/d$g;-><init>(Lf/l/a/m/d;F)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method
