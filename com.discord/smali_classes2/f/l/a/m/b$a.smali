.class public Lf/l/a/m/b$a;
.super Ljava/lang/Object;
.source "Camera1Engine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/b;->I0(Lf/l/a/p/a;Lf/l/a/s/b;Landroid/graphics/PointF;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/s/b;

.field public final synthetic e:Lf/l/a/p/a;

.field public final synthetic f:Landroid/graphics/PointF;

.field public final synthetic g:Lf/l/a/m/b;


# direct methods
.method public constructor <init>(Lf/l/a/m/b;Lf/l/a/s/b;Lf/l/a/p/a;Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iput-object p2, p0, Lf/l/a/m/b$a;->d:Lf/l/a/s/b;

    iput-object p3, p0, Lf/l/a/m/b$a;->e:Lf/l/a/p/a;

    iput-object p4, p0, Lf/l/a/m/b$a;->f:Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-boolean v0, v0, Lf/l/a/c;->o:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lf/l/a/m/s/a;

    iget-object v1, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v2, v1, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    iget-object v1, v1, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v1}, Lf/l/a/v/a;->j()Lf/l/a/w/b;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lf/l/a/m/s/a;-><init>(Lf/l/a/m/t/a;Lf/l/a/w/b;)V

    iget-object v1, p0, Lf/l/a/m/b$a;->d:Lf/l/a/s/b;

    invoke-virtual {v1, v0}, Lf/l/a/s/b;->c(Lf/l/a/s/c;)Lf/l/a/s/b;

    move-result-object v1

    iget-object v2, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v2, v2, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v3

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v4

    if-lez v3, :cond_1

    invoke-virtual {v1, v3, v0}, Lf/l/a/s/b;->b(ILf/l/a/s/c;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    :cond_1
    if-lez v4, :cond_2

    invoke-virtual {v1, v4, v0}, Lf/l/a/s/b;->b(ILf/l/a/s/c;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    :cond_2
    const-string v0, "auto"

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    iget-object v0, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object v0, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    iget-object v1, p0, Lf/l/a/m/b$a;->e:Lf/l/a/p/a;

    iget-object v2, p0, Lf/l/a/m/b$a;->f:Landroid/graphics/PointF;

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v0, v1, v2}, Lcom/otaliastudios/cameraview/CameraView$b;->e(Lf/l/a/p/a;Landroid/graphics/PointF;)V

    iget-object v0, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v1, "focus end"

    invoke-virtual {v0, v1}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-wide/16 v2, 0x9c4

    new-instance v4, Lf/l/a/m/b$a$a;

    invoke-direct {v4, p0}, Lf/l/a/m/b$a$a;-><init>(Lf/l/a/m/b$a;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lf/l/a/m/v/a;->f(Ljava/lang/String;JLjava/lang/Runnable;)V

    :try_start_0
    iget-object v0, p0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    new-instance v1, Lf/l/a/m/b$a$b;

    invoke-direct {v1, p0}, Lf/l/a/m/b$a$b;-><init>(Lf/l/a/m/b$a;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "startAutoFocus:"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "Error calling autoFocus"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :goto_0
    return-void
.end method
