.class public Lf/l/a/m/v/a$b$a;
.super Ljava/lang/Object;
.source "CameraOrchestrator.java"

# interfaces
.implements Lf/h/a/f/p/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/v/a$b;->onComplete(Lcom/google/android/gms/tasks/Task;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/m/v/a$b;


# direct methods
.method public constructor <init>(Lf/l/a/m/v/a$b;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->k()Ljava/lang/Exception;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    sget-object p1, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-object v5, v5, Lf/l/a/m/v/a$b;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, "- Finished with ERROR."

    aput-object v5, v4, v3

    aput-object v0, v4, v1

    invoke-virtual {p1, v1, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-boolean v1, p1, Lf/l/a/m/v/a$b;->d:Z

    if-eqz v1, :cond_0

    iget-object p1, p1, Lf/l/a/m/v/a$b;->f:Lf/l/a/m/v/a;

    iget-object p1, p1, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    check-cast p1, Lf/l/a/m/j$c;

    iget-object p1, p1, Lf/l/a/m/j$c;->a:Lf/l/a/m/j;

    invoke-static {p1, v0, v2}, Lf/l/a/m/j;->b(Lf/l/a/m/j;Ljava/lang/Throwable;Z)V

    :cond_0
    iget-object p1, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-object p1, p1, Lf/l/a/m/v/a$b;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-object v1, v1, Lf/l/a/m/v/a$b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "- Finished because ABORTED."

    aput-object v1, v0, v3

    invoke-virtual {p1, v3, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-object p1, p1, Lf/l/a/m/v/a$b;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    goto :goto_0

    :cond_2
    sget-object v0, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-object v4, v4, Lf/l/a/m/v/a$b;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    const-string v2, "- Finished."

    aput-object v2, v1, v3

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/v/a$b$a;->a:Lf/l/a/m/v/a$b;

    iget-object v0, v0, Lf/l/a/m/v/a$b;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1}, Lcom/google/android/gms/tasks/Task;->l()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method
