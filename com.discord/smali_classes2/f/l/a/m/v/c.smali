.class public Lf/l/a/m/v/c;
.super Lf/l/a/m/v/a;
.source "CameraStateOrchestrator.java"


# instance fields
.field public f:Lf/l/a/m/v/b;

.field public g:Lf/l/a/m/v/b;

.field public h:I


# direct methods
.method public constructor <init>(Lf/l/a/m/v/a$e;)V
    .locals 0
    .param p1    # Lf/l/a/m/v/a$e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lf/l/a/m/v/a;-><init>(Lf/l/a/m/v/a$e;)V

    sget-object p1, Lf/l/a/m/v/b;->d:Lf/l/a/m/v/b;

    iput-object p1, p0, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    iput-object p1, p0, Lf/l/a/m/v/c;->g:Lf/l/a/m/v/b;

    const/4 p1, 0x0

    iput p1, p0, Lf/l/a/m/v/c;->h:I

    return-void
.end method


# virtual methods
.method public g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;
    .locals 10
    .param p1    # Lf/l/a/m/v/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/v/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Callable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lf/l/a/m/v/b;",
            "Lf/l/a/m/v/b;",
            "Z",
            "Ljava/util/concurrent/Callable<",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;"
        }
    .end annotation

    iget v0, p0, Lf/l/a/m/v/c;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/l/a/m/v/c;->h:I

    iput-object p2, p0, Lf/l/a/m/v/c;->g:Lf/l/a/m/v/b;

    invoke-virtual {p2, p1}, Lf/l/a/m/v/b;->f(Lf/l/a/m/v/b;)Z

    move-result v1

    xor-int/lit8 v8, v1, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " << "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " >> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v9, Lf/l/a/m/v/c$b;

    move-object v2, v9

    move-object v3, p0

    move-object v4, p1

    move-object v5, v1

    move-object v6, p2

    move-object v7, p4

    invoke-direct/range {v2 .. v8}, Lf/l/a/m/v/c$b;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/String;Lf/l/a/m/v/b;Ljava/util/concurrent/Callable;Z)V

    invoke-virtual {p0, v1, p3, v9}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance p2, Lf/l/a/m/v/c$a;

    invoke-direct {p2, p0, v0}, Lf/l/a/m/v/c$a;-><init>(Lf/l/a/m/v/c;I)V

    check-cast p1, Lf/h/a/f/p/b0;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p3, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, p3, p2}, Lf/h/a/f/p/b0;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    return-object p1
.end method

.method public h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/v/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lf/l/a/m/v/b;",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/l/a/m/v/c$c;

    invoke-direct {v0, p0, p2, p3}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance p2, Lf/l/a/m/v/a$a;

    invoke-direct {p2, p0, v0}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p3, 0x1

    invoke-virtual {p0, p1, p3, p2}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method
