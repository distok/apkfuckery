.class public Lf/l/a/m/v/a$b;
.super Ljava/lang/Object;
.source "CameraOrchestrator.java"

# interfaces
.implements Lf/h/a/f/p/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/util/concurrent/Callable;

.field public final synthetic c:Lf/l/a/q/e;

.field public final synthetic d:Z

.field public final synthetic e:Lcom/google/android/gms/tasks/TaskCompletionSource;

.field public final synthetic f:Lf/l/a/m/v/a;


# direct methods
.method public constructor <init>(Lf/l/a/m/v/a;Ljava/lang/String;Ljava/util/concurrent/Callable;Lf/l/a/q/e;ZLcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/v/a$b;->f:Lf/l/a/m/v/a;

    iput-object p2, p0, Lf/l/a/m/v/a$b;->a:Ljava/lang/String;

    iput-object p3, p0, Lf/l/a/m/v/a$b;->b:Ljava/util/concurrent/Callable;

    iput-object p4, p0, Lf/l/a/m/v/a$b;->c:Lf/l/a/q/e;

    iput-boolean p5, p0, Lf/l/a/m/v/a$b;->d:Z

    iput-object p6, p0, Lf/l/a/m/v/a$b;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/google/android/gms/tasks/Task;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lf/l/a/m/v/a$b;->f:Lf/l/a/m/v/a;

    iget-object p1, p1, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lf/l/a/m/v/a$b;->f:Lf/l/a/m/v/a;

    iget-object v0, v0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    iget-object v0, p0, Lf/l/a/m/v/a$b;->f:Lf/l/a/m/v/a;

    invoke-virtual {v0}, Lf/l/a/m/v/a;->b()V

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_1
    sget-object v2, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    new-array v3, p1, [Ljava/lang/Object;

    iget-object v4, p0, Lf/l/a/m/v/a$b;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const-string v4, "- Executing."

    aput-object v4, v3, v1

    invoke-virtual {v2, v1, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lf/l/a/m/v/a$b;->b:Ljava/util/concurrent/Callable;

    invoke-interface {v2}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/tasks/Task;

    iget-object v3, p0, Lf/l/a/m/v/a$b;->c:Lf/l/a/q/e;

    new-instance v4, Lf/l/a/m/v/a$b$a;

    invoke-direct {v4, p0}, Lf/l/a/m/v/a$b$a;-><init>(Lf/l/a/m/v/a$b;)V

    invoke-static {v2, v3, v4}, Lf/l/a/m/v/a;->a(Lcom/google/android/gms/tasks/Task;Lf/l/a/q/e;Lf/h/a/f/p/c;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    sget-object v3, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lf/l/a/m/v/a$b;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const-string v5, "- Finished."

    aput-object v5, v4, v1

    aput-object v2, v4, p1

    invoke-virtual {v3, v1, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-boolean p1, p0, Lf/l/a/m/v/a$b;->d:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/l/a/m/v/a$b;->f:Lf/l/a/m/v/a;

    iget-object p1, p1, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    check-cast p1, Lf/l/a/m/j$c;

    iget-object p1, p1, Lf/l/a/m/j$c;->a:Lf/l/a/m/j;

    invoke-static {p1, v2, v0}, Lf/l/a/m/j;->b(Lf/l/a/m/j;Ljava/lang/Throwable;Z)V

    :cond_0
    iget-object p1, p0, Lf/l/a/m/v/a$b;->e:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
