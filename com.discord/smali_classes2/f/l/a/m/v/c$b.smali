.class public Lf/l/a/m/v/c$b;
.super Ljava/lang/Object;
.source "CameraStateOrchestrator.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/google/android/gms/tasks/Task<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/m/v/b;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lf/l/a/m/v/b;

.field public final synthetic g:Ljava/util/concurrent/Callable;

.field public final synthetic h:Z

.field public final synthetic i:Lf/l/a/m/v/c;


# direct methods
.method public constructor <init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/String;Lf/l/a/m/v/b;Ljava/util/concurrent/Callable;Z)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/v/c$b;->i:Lf/l/a/m/v/c;

    iput-object p2, p0, Lf/l/a/m/v/c$b;->d:Lf/l/a/m/v/b;

    iput-object p3, p0, Lf/l/a/m/v/c$b;->e:Ljava/lang/String;

    iput-object p4, p0, Lf/l/a/m/v/c$b;->f:Lf/l/a/m/v/b;

    iput-object p5, p0, Lf/l/a/m/v/c$b;->g:Ljava/util/concurrent/Callable;

    iput-boolean p6, p0, Lf/l/a/m/v/c$b;->h:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/v/c$b;->i:Lf/l/a/m/v/c;

    iget-object v1, v0, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    iget-object v2, p0, Lf/l/a/m/v/c$b;->d:Lf/l/a/m/v/b;

    if-eq v1, v2, :cond_0

    sget-object v0, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lf/l/a/m/v/c$b;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "- State mismatch, aborting. current:"

    aput-object v3, v1, v2

    iget-object v2, p0, Lf/l/a/m/v/c$b;->i:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    const-string v4, "from:"

    aput-object v4, v1, v2

    const/4 v2, 0x4

    iget-object v4, p0, Lf/l/a/m/v/c$b;->d:Lf/l/a/m/v/b;

    aput-object v4, v1, v2

    const/4 v2, 0x5

    const-string v4, "to:"

    aput-object v4, v1, v2

    const/4 v2, 0x6

    iget-object v4, p0, Lf/l/a/m/v/c$b;->f:Lf/l/a/m/v/b;

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lf/h/a/f/p/b0;

    invoke-direct {v0}, Lf/h/a/f/p/b0;-><init>()V

    invoke-virtual {v0}, Lf/h/a/f/p/b0;->u()Z

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    check-cast v0, Lf/l/a/m/j$c;

    iget-object v0, v0, Lf/l/a/m/j$c;->a:Lf/l/a/m/j;

    iget-object v0, v0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v0, v0, Lf/l/a/q/e;->d:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lf/l/a/m/v/c$b;->g:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/tasks/Task;

    new-instance v2, Lf/l/a/m/v/d;

    invoke-direct {v2, p0}, Lf/l/a/m/v/d;-><init>(Lf/l/a/m/v/c$b;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/tasks/Task;->j(Ljava/util/concurrent/Executor;Lf/h/a/f/p/a;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    :goto_0
    return-object v0
.end method
