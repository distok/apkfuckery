.class public Lf/l/a/m/v/a;
.super Ljava/lang/Object;
.source "CameraOrchestrator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/m/v/a$f;,
        Lf/l/a/m/v/a$e;
    }
.end annotation


# static fields
.field public static final e:Lf/l/a/b;


# instance fields
.field public final a:Lf/l/a/m/v/a$e;

.field public final b:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lf/l/a/m/v/a$f;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/m/v/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Lf/l/a/m/v/a$e;)V
    .locals 1
    .param p1    # Lf/l/a/m/v/a$e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/l/a/m/v/a;->d:Ljava/util/Map;

    iput-object p1, p0, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    invoke-virtual {p0}, Lf/l/a/m/v/a;->b()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/tasks/Task;Lf/l/a/q/e;Lf/h/a/f/p/c;)V
    .locals 1
    .param p0    # Lcom/google/android/gms/tasks/Task;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lf/l/a/q/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/h/a/f/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;",
            "Lf/l/a/q/e;",
            "Lf/h/a/f/p/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/tasks/Task;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lf/l/a/m/v/a$d;

    invoke-direct {v0, p2, p0}, Lf/l/a/m/v/a$d;-><init>(Lf/h/a/f/p/c;Lcom/google/android/gms/tasks/Task;)V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p0

    iget-object p2, p1, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    if-ne p0, p2, :cond_0

    invoke-virtual {v0}, Lf/l/a/m/v/a$d;->run()V

    goto :goto_0

    :cond_0
    iget-object p0, p1, Lf/l/a/q/e;->c:Landroid/os/Handler;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p1, Lf/l/a/q/e;->d:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/tasks/Task;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    :goto_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 6

    iget-object v0, p0, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    new-instance v2, Lf/l/a/m/v/a$f;

    const-string v3, "BASE"

    const/4 v4, 0x0

    invoke-static {v4}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v5

    invoke-direct {v2, v3, v5, v4}, Lf/l/a/m/v/a$f;-><init>(Ljava/lang/String;Lcom/google/android/gms/tasks/Task;Lf/l/a/m/v/a$a;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public c(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lf/l/a/m/v/a;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    check-cast v1, Lf/l/a/m/j$c;

    iget-object v1, v1, Lf/l/a/m/j$c;->a:Lf/l/a/m/j;

    iget-object v1, v1, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v2, p0, Lf/l/a/m/v/a;->d:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    iget-object v1, v1, Lf/l/a/q/e;->c:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lf/l/a/m/v/a;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v1, Lf/l/a/m/v/a$f;

    const/4 v2, 0x0

    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v3

    invoke-direct {v1, p1, v3, v2}, Lf/l/a/m/v/a$f;-><init>(Ljava/lang/String;Lcom/google/android/gms/tasks/Task;Lf/l/a/m/v/a$a;)V

    :goto_0
    iget-object p1, p0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {p1, v1}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/l/a/m/v/a;->b()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public d(Ljava/lang/String;ZLjava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Runnable;",
            ")",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Lf/l/a/m/v/a$a;

    invoke-direct {v0, p0, p3}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    invoke-virtual {p0, p1, p2, v0}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;
    .locals 12
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Callable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/concurrent/Callable<",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;>;)",
            "Lcom/google/android/gms/tasks/Task<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/v/a;->e:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "- Scheduling."

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    iget-object v1, p0, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    check-cast v1, Lf/l/a/m/j$c;

    iget-object v1, v1, Lf/l/a/m/j$c;->a:Lf/l/a/m/j;

    iget-object v1, v1, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v2, p0, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/l/a/m/v/a$f;

    iget-object v3, v3, Lf/l/a/m/v/a$f;->b:Lcom/google/android/gms/tasks/Task;

    new-instance v11, Lf/l/a/m/v/a$b;

    move-object v4, v11

    move-object v5, p0

    move-object v6, p1

    move-object v7, p3

    move-object v8, v1

    move v9, p2

    move-object v10, v0

    invoke-direct/range {v4 .. v10}, Lf/l/a/m/v/a$b;-><init>(Lf/l/a/m/v/a;Ljava/lang/String;Ljava/util/concurrent/Callable;Lf/l/a/q/e;ZLcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-static {v3, v1, v11}, Lf/l/a/m/v/a;->a(Lcom/google/android/gms/tasks/Task;Lf/l/a/q/e;Lf/h/a/f/p/c;)V

    iget-object p2, p0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    new-instance p3, Lf/l/a/m/v/a$f;

    iget-object v1, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    const/4 v3, 0x0

    invoke-direct {p3, p1, v1, v3}, Lf/l/a/m/v/a$f;-><init>(Ljava/lang/String;Lcom/google/android/gms/tasks/Task;Lf/l/a/m/v/a$a;)V

    invoke-virtual {p2, p3}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-object p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public f(Ljava/lang/String;JLjava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Lf/l/a/m/v/a$c;

    invoke-direct {v0, p0, p1, p4}, Lf/l/a/m/v/a$c;-><init>(Lf/l/a/m/v/a;Ljava/lang/String;Ljava/lang/Runnable;)V

    iget-object p4, p0, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter p4

    :try_start_0
    iget-object v1, p0, Lf/l/a/m/v/a;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lf/l/a/m/v/a;->a:Lf/l/a/m/v/a$e;

    check-cast p1, Lf/l/a/m/j$c;

    iget-object p1, p1, Lf/l/a/m/j$c;->a:Lf/l/a/m/j;

    iget-object p1, p1, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object p1, p1, Lf/l/a/q/e;->c:Landroid/os/Handler;

    invoke-virtual {p1, v0, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    monitor-exit p4

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
