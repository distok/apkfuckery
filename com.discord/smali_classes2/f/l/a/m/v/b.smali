.class public final enum Lf/l/a/m/v/b;
.super Ljava/lang/Enum;
.source "CameraState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/m/v/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/m/v/b;

.field public static final enum e:Lf/l/a/m/v/b;

.field public static final enum f:Lf/l/a/m/v/b;

.field public static final enum g:Lf/l/a/m/v/b;

.field public static final synthetic h:[Lf/l/a/m/v/b;


# instance fields
.field private mState:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/l/a/m/v/b;

    const-string v1, "OFF"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/l/a/m/v/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/l/a/m/v/b;->d:Lf/l/a/m/v/b;

    new-instance v1, Lf/l/a/m/v/b;

    const-string v3, "ENGINE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/l/a/m/v/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/v/b;

    const-string v5, "BIND"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lf/l/a/m/v/b;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    new-instance v5, Lf/l/a/m/v/b;

    const-string v7, "PREVIEW"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lf/l/a/m/v/b;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/l/a/m/v/b;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/l/a/m/v/b;->h:[Lf/l/a/m/v/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/l/a/m/v/b;->mState:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/m/v/b;
    .locals 1

    const-class v0, Lf/l/a/m/v/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/m/v/b;

    return-object p0
.end method

.method public static values()[Lf/l/a/m/v/b;
    .locals 1

    sget-object v0, Lf/l/a/m/v/b;->h:[Lf/l/a/m/v/b;

    invoke-virtual {v0}, [Lf/l/a/m/v/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/m/v/b;

    return-object v0
.end method


# virtual methods
.method public f(Lf/l/a/m/v/b;)Z
    .locals 1
    .param p1    # Lf/l/a/m/v/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p0, Lf/l/a/m/v/b;->mState:I

    iget p1, p1, Lf/l/a/m/v/b;->mState:I

    if-lt v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
