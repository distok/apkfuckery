.class public Lf/l/a/m/g;
.super Lf/l/a/m/p/e;
.source "Camera2Engine.java"


# instance fields
.field public final synthetic e:Lf/l/a/m/d;


# direct methods
.method public constructor <init>(Lf/l/a/m/d;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/g;->e:Lf/l/a/m/d;

    invoke-direct {p0}, Lf/l/a/m/p/e;-><init>()V

    return-void
.end method


# virtual methods
.method public j(Lf/l/a/m/p/c;)V
    .locals 3
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    iget-object v0, p0, Lf/l/a/m/g;->e:Lf/l/a/m/d;

    move-object v1, p1

    check-cast v1, Lf/l/a/m/d;

    iget-object v1, v1, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v0, v1}, Lf/l/a/m/d;->b1(Landroid/hardware/camera2/CaptureRequest$Builder;)V

    check-cast p1, Lf/l/a/m/d;

    iget-object v0, p1, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_LOCK:Landroid/hardware/camera2/CaptureRequest$Key;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    iget-object v0, p1, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AWB_LOCK:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lf/l/a/m/d;->g1()V

    const p1, 0x7fffffff

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->l(I)V

    return-void
.end method
