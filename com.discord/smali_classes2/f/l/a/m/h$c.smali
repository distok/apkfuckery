.class public Lf/l/a/m/h$c;
.super Ljava/lang/Object;
.source "CameraBaseEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/h;->O0(Lf/l/a/k$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/k$a;

.field public final synthetic e:Z

.field public final synthetic f:Lf/l/a/m/h;


# direct methods
.method public constructor <init>(Lf/l/a/m/h;Lf/l/a/k$a;Z)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/h$c;->f:Lf/l/a/m/h;

    iput-object p2, p0, Lf/l/a/m/h$c;->d:Lf/l/a/k$a;

    iput-boolean p3, p0, Lf/l/a/m/h$c;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "takePicture:"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "running. isTakingPicture:"

    aput-object v4, v1, v3

    iget-object v4, p0, Lf/l/a/m/h$c;->f:Lf/l/a/m/h;

    iget-object v4, v4, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v1, v5

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/h$c;->f:Lf/l/a/m/h;

    iget-object v1, v0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_2

    return-void

    :cond_2
    iget-object v1, v0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v2, Lf/l/a/l/i;->e:Lf/l/a/l/i;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lf/l/a/m/h$c;->d:Lf/l/a/k$a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lf/l/a/m/h;->w:Landroid/location/Location;

    iput-object v2, v1, Lf/l/a/k$a;->a:Landroid/location/Location;

    iget-object v2, v0, Lf/l/a/m/h;->J:Lf/l/a/l/e;

    iput-object v2, v1, Lf/l/a/k$a;->d:Lf/l/a/l/e;

    iget-object v2, v0, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    iput-object v2, v1, Lf/l/a/k$a;->f:Lf/l/a/l/j;

    iget-boolean v2, p0, Lf/l/a/m/h$c;->e:Z

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/h;->W0(Lf/l/a/k$a;Z)V

    return-void

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t take hq pictures while in VIDEO mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
