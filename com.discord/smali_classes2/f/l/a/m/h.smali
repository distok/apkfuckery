.class public abstract Lf/l/a/m/h;
.super Lf/l/a/m/j;
.source "CameraBaseEngine.java"


# instance fields
.field public A:Z

.field public B:Z

.field public C:F

.field public D:Z

.field public E:Lf/l/a/o/c;

.field public final F:Lf/l/a/m/t/a;

.field public G:Lf/l/a/w/c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public H:Lf/l/a/w/c;

.field public I:Lf/l/a/w/c;

.field public J:Lf/l/a/l/e;

.field public K:Lf/l/a/l/i;

.field public L:Lf/l/a/l/a;

.field public M:J

.field public N:I

.field public O:I

.field public P:I

.field public Q:J

.field public R:I

.field public S:I

.field public T:I

.field public U:I

.field public V:I

.field public i:Lf/l/a/v/a;

.field public j:Lf/l/a/c;

.field public k:Lf/l/a/u/d;

.field public l:Lf/l/a/w/b;

.field public m:Lf/l/a/w/b;

.field public n:Lf/l/a/w/b;

.field public o:I

.field public p:Z

.field public q:Lf/l/a/l/f;

.field public r:Lf/l/a/l/m;

.field public s:Lf/l/a/l/l;

.field public t:Lf/l/a/l/b;

.field public u:Lf/l/a/l/h;

.field public v:Lf/l/a/l/j;

.field public w:Landroid/location/Location;

.field public x:F

.field public y:F

.field public z:Z


# direct methods
.method public constructor <init>(Lf/l/a/m/j$g;)V
    .locals 0
    .param p1    # Lf/l/a/m/j$g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lf/l/a/m/j;-><init>(Lf/l/a/m/j$g;)V

    new-instance p1, Lf/l/a/m/t/a;

    invoke-direct {p1}, Lf/l/a/m/t/a;-><init>()V

    iput-object p1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    const/4 p1, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method


# virtual methods
.method public final A()F
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->C:F

    return v0
.end method

.method public final A0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->O:I

    return-void
.end method

.method public final B()Z
    .locals 1

    iget-boolean v0, p0, Lf/l/a/m/h;->D:Z

    return v0
.end method

.method public final B0(Lf/l/a/l/l;)V
    .locals 0
    .param p1    # Lf/l/a/l/l;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/h;->s:Lf/l/a/l/l;

    return-void
.end method

.method public final C(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .locals 3
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v2, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    invoke-virtual {v1, v2, p1}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public final C0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->N:I

    return-void
.end method

.method public final D()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->S:I

    return v0
.end method

.method public final D0(J)V
    .locals 0

    iput-wide p1, p0, Lf/l/a/m/h;->M:J

    return-void
.end method

.method public final E()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->R:I

    return v0
.end method

.method public final E0(Lf/l/a/w/c;)V
    .locals 0
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/h;->I:Lf/l/a/w/c;

    return-void
.end method

.method public final F(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .locals 4
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p0, p1}, Lf/l/a/m/h;->C(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v2, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v1, p1, v2}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget v1, p0, Lf/l/a/m/h;->S:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lf/l/a/m/h;->R:I

    :goto_0
    if-eqz p1, :cond_2

    iget p1, p0, Lf/l/a/m/h;->R:I

    goto :goto_1

    :cond_2
    iget p1, p0, Lf/l/a/m/h;->S:I

    :goto_1
    const v2, 0x7fffffff

    if-gtz v1, :cond_3

    const v1, 0x7fffffff

    :cond_3
    if-gtz p1, :cond_4

    const p1, 0x7fffffff

    :cond_4
    sget-object v2, Lf/l/a/w/a;->f:Ljava/util/HashMap;

    iget v2, v0, Lf/l/a/w/b;->d:I

    iget v3, v0, Lf/l/a/w/b;->e:I

    invoke-static {v2, v3}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v2

    invoke-virtual {v2}, Lf/l/a/w/a;->i()F

    move-result v2

    invoke-static {v1, p1}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v3

    invoke-virtual {v3}, Lf/l/a/w/a;->i()F

    move-result v3

    cmpl-float v3, v3, v2

    if-ltz v3, :cond_5

    iget v0, v0, Lf/l/a/w/b;->e:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    int-to-float v0, p1

    mul-float v0, v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    new-instance v1, Lf/l/a/w/b;

    invoke-direct {v1, v0, p1}, Lf/l/a/w/b;-><init>(II)V

    return-object v1

    :cond_5
    iget p1, v0, Lf/l/a/w/b;->d:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    int-to-float v0, p1

    div-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    new-instance v1, Lf/l/a/w/b;

    invoke-direct {v1, p1, v0}, Lf/l/a/w/b;-><init>(II)V

    return-object v1
.end method

.method public final G()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->O:I

    return v0
.end method

.method public final H()Lf/l/a/l/l;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->s:Lf/l/a/l/l;

    return-object v0
.end method

.method public final I()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->N:I

    return v0
.end method

.method public final J()J
    .locals 2

    iget-wide v0, p0, Lf/l/a/m/h;->M:J

    return-wide v0
.end method

.method public final K(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .locals 3
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v2, Lf/l/a/l/i;->d:Lf/l/a/l/i;

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v2, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    invoke-virtual {v1, v2, p1}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v0

    :cond_1
    return-object v0

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final L()Lf/l/a/w/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->I:Lf/l/a/w/c;

    return-object v0
.end method

.method public final M()Lf/l/a/l/m;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    return-object v0
.end method

.method public final N()F
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->x:F

    return v0
.end method

.method public O0(Lf/l/a/k$a;)V
    .locals 4
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-boolean v0, p0, Lf/l/a/m/h;->A:Z

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v2, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/h$c;

    invoke-direct {v3, p0, p1, v0}, Lf/l/a/m/h$c;-><init>(Lf/l/a/m/h;Lf/l/a/k$a;Z)V

    new-instance p1, Lf/l/a/m/v/c$c;

    invoke-direct {p1, v1, v2, v3}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v0, Lf/l/a/m/v/a$a;

    invoke-direct {v0, v1, p1}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    const-string v2, "take picture"

    invoke-virtual {v1, v2, p1, v0}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final P0(Lf/l/a/l/i;)Lf/l/a/w/b;
    .locals 8
    .param p1    # Lf/l/a/l/i;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v1, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v2, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result v0

    sget-object v1, Lf/l/a/l/i;->d:Lf/l/a/l/i;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lf/l/a/m/h;->H:Lf/l/a/w/c;

    iget-object v2, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v2, v2, Lf/l/a/c;->e:Ljava/util/Set;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->I:Lf/l/a/w/c;

    iget-object v2, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v2, v2, Lf/l/a/c;->f:Ljava/util/Set;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    :goto_0
    const/4 v3, 0x2

    new-array v4, v3, [Lf/l/a/w/c;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    new-instance v1, Lf/l/a/w/i;

    invoke-direct {v1}, Lf/l/a/w/i;-><init>()V

    const/4 v6, 0x1

    aput-object v1, v4, v6

    invoke-static {v4}, Lf/h/a/f/f/n/g;->V([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    check-cast v1, Lf/l/a/w/p;

    invoke-virtual {v1, v4}, Lf/l/a/w/p;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/l/a/w/b;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const-string v7, "computeCaptureSize:"

    aput-object v7, v4, v5

    const-string v5, "result:"

    aput-object v5, v4, v6

    aput-object v1, v4, v3

    const/4 v3, 0x3

    const-string v5, "flip:"

    aput-object v5, v4, v3

    const/4 v3, 0x4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x5

    const-string v5, "mode:"

    aput-object v5, v4, v3

    const/4 v3, 0x6

    aput-object p1, v4, v3

    invoke-virtual {v2, v6, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v1

    :cond_1
    return-object v1

    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "SizeSelectors must not return Sizes other than those in the input list."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final Q0()Lf/l/a/w/b;
    .locals 15
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    sget-object v0, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {p0}, Lf/l/a/m/h;->S0()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v3, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    invoke-virtual {v2, v3, v0}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/l/a/w/b;

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v4

    :cond_0
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lf/l/a/m/h;->T0(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v1, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    iget v4, v1, Lf/l/a/w/b;->d:I

    iget v1, v1, Lf/l/a/w/b;->e:I

    invoke-static {v4, v1}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v1

    if-eqz v2, :cond_2

    iget v4, v1, Lf/l/a/w/a;->e:I

    iget v1, v1, Lf/l/a/w/a;->d:I

    invoke-static {v4, v1}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v1

    :cond_2
    sget-object v4, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v5, 0x5

    new-array v6, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "computePreviewStreamSize:"

    aput-object v8, v6, v7

    const/4 v9, 0x1

    const-string v10, "targetRatio:"

    aput-object v10, v6, v9

    const/4 v10, 0x2

    aput-object v1, v6, v10

    const/4 v11, 0x3

    const-string v12, "targetMinSize:"

    aput-object v12, v6, v11

    const/4 v12, 0x4

    aput-object v0, v6, v12

    invoke-virtual {v4, v9, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-array v6, v10, [Lf/l/a/w/c;

    const/4 v13, 0x0

    invoke-virtual {v1}, Lf/l/a/w/a;->i()F

    move-result v1

    new-instance v14, Lf/l/a/w/h;

    invoke-direct {v14, v1, v13}, Lf/l/a/w/h;-><init>(FF)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v1

    aput-object v1, v6, v7

    new-instance v1, Lf/l/a/w/i;

    invoke-direct {v1}, Lf/l/a/w/i;-><init>()V

    aput-object v1, v6, v9

    invoke-static {v6}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v1

    new-array v6, v11, [Lf/l/a/w/c;

    iget v13, v0, Lf/l/a/w/b;->e:I

    invoke-static {v13}, Lf/h/a/f/f/n/g;->Q(I)Lf/l/a/w/c;

    move-result-object v13

    aput-object v13, v6, v7

    iget v0, v0, Lf/l/a/w/b;->d:I

    invoke-static {v0}, Lf/h/a/f/f/n/g;->R(I)Lf/l/a/w/c;

    move-result-object v0

    aput-object v0, v6, v9

    new-instance v0, Lf/l/a/w/j;

    invoke-direct {v0}, Lf/l/a/w/j;-><init>()V

    aput-object v0, v6, v10

    invoke-static {v6}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v0

    new-array v6, v12, [Lf/l/a/w/c;

    new-array v13, v10, [Lf/l/a/w/c;

    aput-object v1, v13, v7

    aput-object v0, v13, v9

    invoke-static {v13}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v13

    aput-object v13, v6, v7

    aput-object v0, v6, v9

    aput-object v1, v6, v10

    new-instance v0, Lf/l/a/w/i;

    invoke-direct {v0}, Lf/l/a/w/i;-><init>()V

    aput-object v0, v6, v11

    invoke-static {v6}, Lf/h/a/f/f/n/g;->V([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v0

    iget-object v1, p0, Lf/l/a/m/h;->G:Lf/l/a/w/c;

    if-eqz v1, :cond_3

    new-array v6, v10, [Lf/l/a/w/c;

    aput-object v1, v6, v7

    aput-object v0, v6, v9

    invoke-static {v6}, Lf/h/a/f/f/n/g;->V([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v0

    :cond_3
    check-cast v0, Lf/l/a/w/p;

    invoke-virtual {v0, v3}, Lf/l/a/w/p;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/w/b;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v0

    :cond_4
    new-array v1, v5, [Ljava/lang/Object;

    aput-object v8, v1, v7

    const-string v3, "result:"

    aput-object v3, v1, v9

    aput-object v0, v1, v10

    const-string v3, "flip:"

    aput-object v3, v1, v11

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v12

    invoke-virtual {v4, v9, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-object v0

    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SizeSelectors must not return Sizes other than those in the input list."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "targetMinSize should not be null here."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public R0()Lf/l/a/o/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->E:Lf/l/a/o/c;

    if-nez v0, :cond_0

    iget v0, p0, Lf/l/a/m/h;->V:I

    invoke-virtual {p0, v0}, Lf/l/a/m/h;->U0(I)Lf/l/a/o/c;

    move-result-object v0

    iput-object v0, p0, Lf/l/a/m/h;->E:Lf/l/a/o/c;

    :cond_0
    iget-object v0, p0, Lf/l/a/m/h;->E:Lf/l/a/o/c;

    return-object v0
.end method

.method public abstract S0()Ljava/util/List;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;"
        }
    .end annotation
.end method

.method public final T0(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .locals 3
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v2, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v1, v2, p1}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lf/l/a/v/a;->j()Lf/l/a/w/b;

    move-result-object p1

    invoke-virtual {p1}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lf/l/a/v/a;->j()Lf/l/a/w/b;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public abstract U0(I)Lf/l/a/o/c;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract V0()V
.end method

.method public abstract W0(Lf/l/a/k$a;Z)V
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public final X0()Z
    .locals 5

    iget-wide v0, p0, Lf/l/a/m/h;->Q:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final Z(Lf/l/a/l/a;)V
    .locals 1
    .param p1    # Lf/l/a/l/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->L:Lf/l/a/l/a;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lf/l/a/m/h;->L:Lf/l/a/l/a;

    :cond_0
    return-void
.end method

.method public a(Lf/l/a/k$a;Ljava/lang/Exception;)V
    .locals 6
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Exception;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object p2, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast p2, Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v3, p2, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v4, "dispatchOnPictureTaken"

    aput-object v4, v0, v2

    aput-object p1, v0, v1

    invoke-virtual {v3, v1, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p2, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->l:Landroid/os/Handler;

    new-instance v1, Lf/l/a/h;

    invoke-direct {v1, p2, p1}, Lf/l/a/h;-><init>(Lcom/otaliastudios/cameraview/CameraView$b;Lf/l/a/k$a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    sget-object p1, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "onPictureResult"

    aput-object v5, v4, v2

    const-string v2, "result is null: something went wrong."

    aput-object v2, v4, v1

    aput-object p2, v4, v0

    invoke-virtual {p1, v3, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    const/4 v1, 0x4

    invoke-direct {v0, p2, v1}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    check-cast p1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {p1, v0}, Lcom/otaliastudios/cameraview/CameraView$b;->a(Lcom/otaliastudios/cameraview/CameraException;)V

    :goto_0
    return-void
.end method

.method public final a0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->P:I

    return-void
.end method

.method public final b0(Lf/l/a/l/b;)V
    .locals 0
    .param p1    # Lf/l/a/l/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/h;->t:Lf/l/a/l/b;

    return-void
.end method

.method public final c0(J)V
    .locals 0

    iput-wide p1, p0, Lf/l/a/m/h;->Q:J

    return-void
.end method

.method public final e()Lf/l/a/m/t/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    return-object v0
.end method

.method public final e0(Lf/l/a/l/e;)V
    .locals 4
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->J:Lf/l/a/l/e;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lf/l/a/m/h;->J:Lf/l/a/l/e;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/h$a;

    invoke-direct {v3, p0, p1, v0}, Lf/l/a/m/h$a;-><init>(Lf/l/a/m/h;Lf/l/a/l/e;Lf/l/a/l/e;)V

    new-instance p1, Lf/l/a/m/v/c$c;

    invoke-direct {p1, v1, v2, v3}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v0, Lf/l/a/m/v/a$a;

    invoke-direct {v0, v1, p1}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    const-string v2, "facing"

    invoke-virtual {v1, v2, p1, v0}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final f()Lf/l/a/l/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->L:Lf/l/a/l/a;

    return-object v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->P:I

    return v0
.end method

.method public final h()Lf/l/a/l/b;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->t:Lf/l/a/l/b;

    return-object v0
.end method

.method public final h0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->U:I

    return-void
.end method

.method public final i()J
    .locals 2

    iget-wide v0, p0, Lf/l/a/m/h;->Q:J

    return-wide v0
.end method

.method public final i0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->T:I

    return-void
.end method

.method public final j()Lf/l/a/c;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    return-object v0
.end method

.method public final j0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->V:I

    return-void
.end method

.method public final k()F
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->y:F

    return v0
.end method

.method public final l()Lf/l/a/l/e;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->J:Lf/l/a/l/e;

    return-object v0
.end method

.method public final m()Lf/l/a/l/f;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    return-object v0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->o:I

    return v0
.end method

.method public final n0(Lf/l/a/l/i;)V
    .locals 3
    .param p1    # Lf/l/a/l/i;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v0, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v1, Lf/l/a/m/h$b;

    invoke-direct {v1, p0}, Lf/l/a/m/h$b;-><init>(Lf/l/a/m/h;)V

    new-instance v2, Lf/l/a/m/v/c$c;

    invoke-direct {v2, p1, v0, v1}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v0, Lf/l/a/m/v/a$a;

    invoke-direct {v0, p1, v2}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 v1, 0x1

    const-string v2, "mode"

    invoke-virtual {p1, v2, v1, v0}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final o()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->U:I

    return v0
.end method

.method public final o0(Lf/l/a/t/a;)V
    .locals 0
    .param p1    # Lf/l/a/t/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public final p()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->T:I

    return v0
.end method

.method public final q()I
    .locals 1

    iget v0, p0, Lf/l/a/m/h;->V:I

    return v0
.end method

.method public final q0(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/l/a/m/h;->A:Z

    return-void
.end method

.method public final r()Lf/l/a/l/h;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    return-object v0
.end method

.method public final r0(Lf/l/a/w/c;)V
    .locals 0
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/h;->H:Lf/l/a/w/c;

    return-void
.end method

.method public final s()Landroid/location/Location;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    return-object v0
.end method

.method public final s0(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/l/a/m/h;->B:Z

    return-void
.end method

.method public final t()Lf/l/a/l/i;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    return-object v0
.end method

.method public final u()Lf/l/a/l/j;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    return-object v0
.end method

.method public final u0(Lf/l/a/v/a;)V
    .locals 2
    .param p1    # Lf/l/a/v/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/l/a/v/a;->r(Lf/l/a/v/a$c;)V

    :cond_0
    iput-object p1, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {p1, p0}, Lf/l/a/v/a;->r(Lf/l/a/v/a$c;)V

    return-void
.end method

.method public final v()Z
    .locals 1

    iget-boolean v0, p0, Lf/l/a/m/h;->A:Z

    return v0
.end method

.method public final w(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .locals 3
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v2, Lf/l/a/l/i;->e:Lf/l/a/l/i;

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v2, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    invoke-virtual {v1, v2, p1}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v0

    :cond_1
    return-object v0

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final w0(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/l/a/m/h;->D:Z

    return-void
.end method

.method public final x()Lf/l/a/w/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->H:Lf/l/a/w/c;

    return-object v0
.end method

.method public final x0(Lf/l/a/w/c;)V
    .locals 0
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/h;->G:Lf/l/a/w/c;

    return-void
.end method

.method public final y()Z
    .locals 1

    iget-boolean v0, p0, Lf/l/a/m/h;->B:Z

    return v0
.end method

.method public final y0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->S:I

    return-void
.end method

.method public final z()Lf/l/a/v/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    return-object v0
.end method

.method public final z0(I)V
    .locals 0

    iput p1, p0, Lf/l/a/m/h;->R:I

    return-void
.end method
