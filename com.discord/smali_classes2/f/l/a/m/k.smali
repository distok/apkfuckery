.class public Lf/l/a/m/k;
.super Ljava/lang/Object;
.source "CameraEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Ljava/lang/Throwable;

.field public final synthetic e:Lf/l/a/m/j;


# direct methods
.method public constructor <init>(Lf/l/a/m/j;Ljava/lang/Throwable;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/k;->e:Lf/l/a/m/j;

    iput-object p2, p0, Lf/l/a/m/k;->d:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v0, p0, Lf/l/a/m/k;->d:Ljava/lang/Throwable;

    instance-of v1, v0, Lcom/otaliastudios/cameraview/CameraException;

    const/4 v2, 0x3

    const-string v3, "EXCEPTION:"

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v1, :cond_1

    check-cast v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-virtual {v0}, Lcom/otaliastudios/cameraview/CameraException;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v7, v4, [Ljava/lang/Object;

    aput-object v3, v7, v6

    const-string v8, "Got CameraException. Since it is unrecoverable, executing destroy(false)."

    aput-object v8, v7, v5

    invoke-virtual {v1, v2, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lf/l/a/m/k;->e:Lf/l/a/m/j;

    invoke-virtual {v1, v6, v6}, Lf/l/a/m/j;->d(ZI)V

    :cond_0
    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v6

    const-string v3, "Got CameraException. Dispatching to callback."

    aput-object v3, v4, v5

    invoke-virtual {v1, v2, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lf/l/a/m/k;->e:Lf/l/a/m/j;

    iget-object v1, v1, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast v1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v1, v0}, Lcom/otaliastudios/cameraview/CameraView$b;->a(Lcom/otaliastudios/cameraview/CameraException;)V

    return-void

    :cond_1
    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v3, v1, v6

    const-string v7, "Unexpected error! Executing destroy(true)."

    aput-object v7, v1, v5

    invoke-virtual {v0, v2, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lf/l/a/m/k;->e:Lf/l/a/m/j;

    invoke-virtual {v1, v5, v6}, Lf/l/a/m/j;->d(ZI)V

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v3, v1, v6

    const-string v3, "Unexpected error! Throwing."

    aput-object v3, v1, v5

    invoke-virtual {v0, v2, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/k;->d:Ljava/lang/Throwable;

    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Lf/l/a/m/k;->d:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
