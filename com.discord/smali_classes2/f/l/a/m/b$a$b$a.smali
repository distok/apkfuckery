.class public Lf/l/a/m/b$a$b$a;
.super Ljava/lang/Object;
.source "Camera1Engine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/b$a$b;->onAutoFocus(ZLandroid/hardware/Camera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/m/b$a$b;


# direct methods
.method public constructor <init>(Lf/l/a/m/b$a$b;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/b$a$b$a;->d:Lf/l/a/m/b$a$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lf/l/a/m/b$a$b$a;->d:Lf/l/a/m/b$a$b;

    iget-object v0, v0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object v0, v0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    iget-object v0, p0, Lf/l/a/m/b$a$b$a;->d:Lf/l/a/m/b$a$b;

    iget-object v0, v0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object v0, v0, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v1

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v2

    const/4 v3, 0x0

    if-lez v1, :cond_0

    invoke-virtual {v0, v3}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    :cond_0
    if-lez v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/hardware/Camera$Parameters;->setMeteringAreas(Ljava/util/List;)V

    :cond_1
    iget-object v1, p0, Lf/l/a/m/b$a$b$a;->d:Lf/l/a/m/b$a$b;

    iget-object v1, v1, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object v1, v1, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    invoke-virtual {v1, v0}, Lf/l/a/m/b;->Z0(Landroid/hardware/Camera$Parameters;)V

    iget-object v1, p0, Lf/l/a/m/b$a$b$a;->d:Lf/l/a/m/b$a$b;

    iget-object v1, v1, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object v1, v1, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v1, v1, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method
