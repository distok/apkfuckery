.class public abstract Lf/l/a/m/j;
.super Ljava/lang/Object;
.source "CameraEngine.java"

# interfaces
.implements Lf/l/a/v/a$c;
.implements Lf/l/a/u/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/m/j$i;,
        Lf/l/a/m/j$h;,
        Lf/l/a/m/j$g;
    }
.end annotation


# static fields
.field public static final h:Lf/l/a/b;


# instance fields
.field public d:Lf/l/a/q/e;

.field public e:Landroid/os/Handler;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public final f:Lf/l/a/m/j$g;

.field public final g:Lf/l/a/m/v/c;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/m/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Lf/l/a/m/j$g;)V
    .locals 2
    .param p1    # Lf/l/a/m/j$g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf/l/a/m/v/c;

    new-instance v1, Lf/l/a/m/j$c;

    invoke-direct {v1, p0}, Lf/l/a/m/j$c;-><init>(Lf/l/a/m/j;)V

    invoke-direct {v0, v1}, Lf/l/a/m/v/c;-><init>(Lf/l/a/m/v/a$e;)V

    iput-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iput-object p1, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lf/l/a/m/j;->e:Landroid/os/Handler;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/l/a/m/j;->W(Z)V

    return-void
.end method

.method public static b(Lf/l/a/m/j;Ljava/lang/Throwable;Z)V
    .locals 7

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x3

    const/4 v1, 0x1

    const-string v2, "EXCEPTION:"

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v2, v5, v4

    const-string v6, "Handler thread is gone. Replacing."

    aput-object v6, v5, v1

    invoke-virtual {p2, v0, v5}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, v4}, Lf/l/a/m/j;->W(Z)V

    :cond_0
    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    const-string v2, "Scheduling on the crash handler..."

    aput-object v2, v3, v1

    invoke-virtual {p2, v0, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p2, p0, Lf/l/a/m/j;->e:Landroid/os/Handler;

    new-instance v0, Lf/l/a/m/k;

    invoke-direct {v0, p0, p1}, Lf/l/a/m/k;-><init>(Lf/l/a/m/j;Ljava/lang/Throwable;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public abstract A()F
.end method

.method public abstract A0(I)V
.end method

.method public abstract B()Z
.end method

.method public abstract B0(Lf/l/a/l/l;)V
    .param p1    # Lf/l/a/l/l;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract C(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract C0(I)V
.end method

.method public abstract D()I
.end method

.method public abstract D0(J)V
.end method

.method public abstract E()I
.end method

.method public abstract E0(Lf/l/a/w/c;)V
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract F(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract F0(Lf/l/a/l/m;)V
    .param p1    # Lf/l/a/l/m;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract G()I
.end method

.method public abstract G0(F[Landroid/graphics/PointF;Z)V
    .param p2    # [Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract H()Lf/l/a/l/l;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public H0()Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "START:"

    aput-object v3, v1, v2

    const-string v2, "scheduled. State:"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->d:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v4, Lf/l/a/m/m;

    invoke-direct {v4, p0}, Lf/l/a/m/m;-><init>(Lf/l/a/m/j;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lf/l/a/m/l;

    invoke-direct {v1, p0}, Lf/l/a/m/l;-><init>(Lf/l/a/m/j;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->q(Lf/h/a/f/p/f;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    invoke-virtual {p0}, Lf/l/a/m/j;->J0()Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0}, Lf/l/a/m/j;->K0()Lcom/google/android/gms/tasks/Task;

    return-object v0
.end method

.method public abstract I()I
.end method

.method public abstract I0(Lf/l/a/p/a;Lf/l/a/s/b;Landroid/graphics/PointF;)V
    .param p1    # Lf/l/a/p/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lf/l/a/s/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract J()J
.end method

.method public final J0()Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/j$e;

    invoke-direct {v3, p0}, Lf/l/a/m/j$e;-><init>(Lf/l/a/m/j;)V

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v4, v3}, Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public abstract K(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public final K0()Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/j$a;

    invoke-direct {v3, p0}, Lf/l/a/m/j$a;-><init>(Lf/l/a/m/j;)V

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v4, v3}, Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public abstract L()Lf/l/a/w/c;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public L0(Z)Lcom/google/android/gms/tasks/Task;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "STOP:"

    aput-object v3, v1, v2

    const-string v2, "scheduled. State:"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, p1}, Lf/l/a/m/j;->N0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0, p1}, Lf/l/a/m/j;->M0(Z)Lcom/google/android/gms/tasks/Task;

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->d:Lf/l/a/m/v/b;

    xor-int/2addr p1, v3

    new-instance v3, Lf/l/a/m/o;

    invoke-direct {v3, p0}, Lf/l/a/m/o;-><init>(Lf/l/a/m/j;)V

    invoke-virtual {v0, v1, v2, p1, v3}, Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    new-instance v0, Lf/l/a/m/n;

    invoke-direct {v0, p0}, Lf/l/a/m/n;-><init>(Lf/l/a/m/j;)V

    check-cast p1, Lf/h/a/f/p/b0;

    sget-object v1, Lf/h/a/f/p/g;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1, v1, v0}, Lf/h/a/f/p/b0;->g(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)Lcom/google/android/gms/tasks/Task;

    return-object p1
.end method

.method public abstract M()Lf/l/a/l/m;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public final M0(Z)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    xor-int/lit8 p1, p1, 0x1

    new-instance v3, Lf/l/a/m/j$f;

    invoke-direct {v3, p0}, Lf/l/a/m/j$f;-><init>(Lf/l/a/m/j;)V

    invoke-virtual {v0, v1, v2, p1, v3}, Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public abstract N()F
.end method

.method public final N0(Z)Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    xor-int/lit8 p1, p1, 0x1

    new-instance v3, Lf/l/a/m/j$b;

    invoke-direct {v3, p0}, Lf/l/a/m/j$b;-><init>(Lf/l/a/m/j;)V

    invoke-virtual {v0, v1, v2, p1, v3}, Lf/l/a/m/v/c;->g(Lf/l/a/m/v/b;Lf/l/a/m/v/b;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final O()Z
    .locals 5

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v1, v0, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/l/a/m/v/a$f;

    iget-object v3, v2, Lf/l/a/m/v/a$f;->a:Ljava/lang/String;

    const-string v4, " >> "

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v2, Lf/l/a/m/v/a$f;->a:Ljava/lang/String;

    const-string v4, " << "

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    iget-object v2, v2, Lf/l/a/m/v/a$f;->b:Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v2}, Lcom/google/android/gms/tasks/Task;->o()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract O0(Lf/l/a/k$a;)V
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract P()Lcom/google/android/gms/tasks/Task;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract Q()Lcom/google/android/gms/tasks/Task;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/l/a/c;",
            ">;"
        }
    .end annotation
.end method

.method public abstract R()Lcom/google/android/gms/tasks/Task;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract S()Lcom/google/android/gms/tasks/Task;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract T()Lcom/google/android/gms/tasks/Task;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public abstract U()Lcom/google/android/gms/tasks/Task;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method public final V()V
    .locals 5

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "onSurfaceAvailable:"

    aput-object v3, v1, v2

    const-string v2, "Size is"

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {p0}, Lf/l/a/m/j;->z()Lf/l/a/v/a;

    move-result-object v2

    invoke-virtual {v2}, Lf/l/a/v/a;->j()Lf/l/a/w/b;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/l/a/m/j;->J0()Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0}, Lf/l/a/m/j;->K0()Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final W(Z)V
    .locals 4

    iget-object v0, p0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/os/HandlerThread;->interrupt()V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    :cond_0
    sget-object v1, Lf/l/a/q/e;->f:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v0, v0, Lf/l/a/q/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v0, "CameraViewEngine"

    invoke-static {v0}, Lf/l/a/q/e;->a(Ljava/lang/String;)Lf/l/a/q/e;

    move-result-object v0

    iput-object v0, p0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v0, v0, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    new-instance v1, Lf/l/a/m/j$h;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lf/l/a/m/j$h;-><init>(Lf/l/a/m/j;Lf/l/a/m/j$c;)V

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    if-eqz p1, :cond_4

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v0, p1, Lf/l/a/m/v/a;->c:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p1, Lf/l/a/m/v/a;->d:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p1, Lf/l/a/m/v/a;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/l/a/m/v/a$f;

    iget-object v3, v3, Lf/l/a/m/v/a$f;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_4
    :goto_2
    return-void
.end method

.method public X()V
    .locals 6

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "RESTART:"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "scheduled. State:"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    invoke-virtual {v0, v4, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, v3}, Lf/l/a/m/j;->L0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0}, Lf/l/a/m/j;->H0()Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public Y()Lcom/google/android/gms/tasks/Task;
    .locals 6
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "RESTART BIND:"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "scheduled. State:"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    invoke-virtual {v0, v4, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, v3}, Lf/l/a/m/j;->N0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0, v3}, Lf/l/a/m/j;->M0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0}, Lf/l/a/m/j;->J0()Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0}, Lf/l/a/m/j;->K0()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public abstract Z(Lf/l/a/l/a;)V
    .param p1    # Lf/l/a/l/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract a0(I)V
.end method

.method public abstract b0(Lf/l/a/l/b;)V
    .param p1    # Lf/l/a/l/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract c(Lf/l/a/l/e;)Z
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract c0(J)V
.end method

.method public final d(ZI)V
    .locals 11

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "DESTROY:"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "state:"

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-string v2, "thread:"

    const/4 v6, 0x3

    aput-object v2, v1, v6

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    const-string v2, "depth:"

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const/4 v2, 0x6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v1, v2

    const/4 v2, 0x7

    const-string v9, "unrecoverably:"

    aput-object v9, v1, v2

    const/16 v2, 0x8

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v1, v2

    invoke-virtual {v0, v4, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v1, p0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v1, v1, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    new-instance v2, Lf/l/a/m/j$i;

    const/4 v9, 0x0

    invoke-direct {v2, v9}, Lf/l/a/m/j$i;-><init>(Lf/l/a/m/j$c;)V

    invoke-virtual {v1, v2}, Landroid/os/HandlerThread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_0
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    invoke-virtual {p0, v4}, Lf/l/a/m/j;->L0(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v2

    iget-object v9, p0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v9, v9, Lf/l/a/q/e;->d:Ljava/util/concurrent/Executor;

    new-instance v10, Lf/l/a/m/j$d;

    invoke-direct {v10, p0, v1}, Lf/l/a/m/j$d;-><init>(Lf/l/a/m/j;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v2, v9, v10}, Lcom/google/android/gms/tasks/Task;->c(Ljava/util/concurrent/Executor;Lf/h/a/f/p/c;)Lcom/google/android/gms/tasks/Task;

    const-wide/16 v9, 0x6

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v9, v10, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_2

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "DESTROY: Could not destroy synchronously after 6 seconds."

    aput-object v2, v1, v3

    const-string v2, "Current thread:"

    aput-object v2, v1, v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "Handler thread:"

    aput-object v2, v1, v6

    iget-object v2, p0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v2, v2, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    aput-object v2, v1, v7

    invoke-virtual {v0, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    add-int/2addr p2, v4

    if-ge p2, v5, :cond_1

    invoke-virtual {p0, v4}, Lf/l/a/m/j;->W(Z)V

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "DESTROY: Trying again on thread:"

    aput-object v2, v1, v3

    iget-object v2, p0, Lf/l/a/m/j;->d:Lf/l/a/q/e;

    iget-object v2, v2, Lf/l/a/q/e;->b:Landroid/os/HandlerThread;

    aput-object v2, v1, v4

    invoke-virtual {v0, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lf/l/a/m/j;->d(ZI)V

    goto :goto_0

    :cond_1
    new-array p1, v4, [Ljava/lang/Object;

    const-string p2, "DESTROY: Giving up because DESTROY_RETRIES was reached."

    aput-object p2, p1, v3

    invoke-virtual {v0, v5, p1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    :goto_0
    return-void
.end method

.method public abstract d0(F[F[Landroid/graphics/PointF;Z)V
    .param p2    # [F
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract e()Lf/l/a/m/t/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract e0(Lf/l/a/l/e;)V
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract f()Lf/l/a/l/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract f0(Lf/l/a/l/f;)V
    .param p1    # Lf/l/a/l/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract g()I
.end method

.method public abstract g0(I)V
.end method

.method public abstract h()Lf/l/a/l/b;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract h0(I)V
.end method

.method public abstract i()J
.end method

.method public abstract i0(I)V
.end method

.method public abstract j()Lf/l/a/c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract j0(I)V
.end method

.method public abstract k()F
.end method

.method public abstract k0(Z)V
.end method

.method public abstract l()Lf/l/a/l/e;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract l0(Lf/l/a/l/h;)V
    .param p1    # Lf/l/a/l/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract m()Lf/l/a/l/f;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract m0(Landroid/location/Location;)V
    .param p1    # Landroid/location/Location;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract n()I
.end method

.method public abstract n0(Lf/l/a/l/i;)V
    .param p1    # Lf/l/a/l/i;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract o()I
.end method

.method public abstract o0(Lf/l/a/t/a;)V
    .param p1    # Lf/l/a/t/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract p()I
.end method

.method public abstract p0(Lf/l/a/l/j;)V
    .param p1    # Lf/l/a/l/j;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract q()I
.end method

.method public abstract q0(Z)V
.end method

.method public abstract r()Lf/l/a/l/h;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract r0(Lf/l/a/w/c;)V
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract s()Landroid/location/Location;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract s0(Z)V
.end method

.method public abstract t()Lf/l/a/l/i;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract t0(Z)V
.end method

.method public abstract u()Lf/l/a/l/j;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract u0(Lf/l/a/v/a;)V
    .param p1    # Lf/l/a/v/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract v()Z
.end method

.method public abstract v0(F)V
.end method

.method public abstract w(Lf/l/a/m/t/c;)Lf/l/a/w/b;
    .param p1    # Lf/l/a/m/t/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract w0(Z)V
.end method

.method public abstract x()Lf/l/a/w/c;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract x0(Lf/l/a/w/c;)V
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract y()Z
.end method

.method public abstract y0(I)V
.end method

.method public abstract z()Lf/l/a/v/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end method

.method public abstract z0(I)V
.end method
