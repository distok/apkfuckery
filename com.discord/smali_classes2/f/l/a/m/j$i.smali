.class public Lf/l/a/m/j$i;
.super Ljava/lang/Object;
.source "CameraEngine.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/l/a/m/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "i"
.end annotation


# direct methods
.method public constructor <init>(Lf/l/a/m/j$c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Ljava/lang/Thread;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "EXCEPTION:"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "In the NoOpExceptionHandler, probably while destroying."

    aput-object v3, v1, v2

    const-string v2, "Thread:"

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    aput-object p1, v1, v2

    const/4 p1, 0x4

    const-string v2, "Error:"

    aput-object v2, v1, p1

    const/4 p1, 0x5

    aput-object p2, v1, p1

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-void
.end method
