.class public abstract Lf/l/a/m/r/a;
.super Lf/l/a/m/p/e;
.source "BaseMeter.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# static fields
.field public static final h:Lf/l/a/b;


# instance fields
.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/hardware/camera2/params/MeteringRectangle;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/m/r/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/m/r/a;->h:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/camera2/params/MeteringRectangle;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/l/a/m/p/e;-><init>()V

    iput-object p1, p0, Lf/l/a/m/r/a;->e:Ljava/util/List;

    iput-boolean p2, p0, Lf/l/a/m/r/a;->g:Z

    return-void
.end method


# virtual methods
.method public final j(Lf/l/a/m/p/c;)V
    .locals 6
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    iget-boolean v0, p0, Lf/l/a/m/r/a;->g:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/m/r/a;->n(Lf/l/a/m/p/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lf/l/a/m/r/a;->m(Lf/l/a/m/p/c;)Z

    move-result v3

    const-string v4, "onStart:"

    const/4 v5, 0x2

    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    sget-object v0, Lf/l/a/m/r/a;->h:Lf/l/a/b;

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v4, v3, v1

    const-string v1, "supported and not skipped. Dispatching onStarted."

    aput-object v1, v3, v2

    invoke-virtual {v0, v2, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/r/a;->e:Ljava/util/List;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/r/a;->o(Lf/l/a/m/p/c;Ljava/util/List;)V

    goto :goto_1

    :cond_1
    sget-object p1, Lf/l/a/m/r/a;->h:Lf/l/a/b;

    new-array v0, v5, [Ljava/lang/Object;

    aput-object v4, v0, v1

    const-string v1, "not supported or skipped. Dispatching COMPLETED state."

    aput-object v1, v0, v2

    invoke-virtual {p1, v2, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iput-boolean v2, p0, Lf/l/a/m/r/a;->f:Z

    const p1, 0x7fffffff

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->l(I)V

    :goto_1
    return-void
.end method

.method public abstract m(Lf/l/a/m/p/c;)Z
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract n(Lf/l/a/m/p/c;)Z
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract o(Lf/l/a/m/p/c;Ljava/util/List;)V
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/l/a/m/p/c;",
            "Ljava/util/List<",
            "Landroid/hardware/camera2/params/MeteringRectangle;",
            ">;)V"
        }
    .end annotation
.end method
