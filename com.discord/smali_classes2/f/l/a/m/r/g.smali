.class public Lf/l/a/m/r/g;
.super Lf/l/a/m/p/d;
.source "MeterAction.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# static fields
.field public static final j:Lf/l/a/b;


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/m/r/a;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lf/l/a/m/p/e;

.field public final g:Lf/l/a/s/b;

.field public final h:Lf/l/a/m/j;

.field public final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/m/r/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/m/r/g;->j:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Lf/l/a/m/j;Lf/l/a/s/b;Z)V
    .locals 0
    .param p1    # Lf/l/a/m/j;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/s/b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lf/l/a/m/p/d;-><init>()V

    iput-object p2, p0, Lf/l/a/m/r/g;->g:Lf/l/a/s/b;

    iput-object p1, p0, Lf/l/a/m/r/g;->h:Lf/l/a/m/j;

    iput-boolean p3, p0, Lf/l/a/m/r/g;->i:Z

    return-void
.end method


# virtual methods
.method public j(Lf/l/a/m/p/c;)V
    .locals 7
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/m/r/g;->j:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStart:"

    aput-object v4, v2, v3

    const/4 v5, 0x1

    const-string v6, "initializing."

    aput-object v6, v2, v5

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, p1}, Lf/l/a/m/r/g;->n(Lf/l/a/m/p/c;)V

    new-array v2, v1, [Ljava/lang/Object;

    aput-object v4, v2, v3

    const-string v3, "initialized."

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-super {p0, p1}, Lf/l/a/m/p/d;->j(Lf/l/a/m/p/c;)V

    return-void
.end method

.method public m()Lf/l/a/m/p/e;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/r/g;->f:Lf/l/a/m/p/e;

    return-object v0
.end method

.method public final n(Lf/l/a/m/p/c;)V
    .locals 9
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lf/l/a/m/r/g;->g:Lf/l/a/s/b;

    if-eqz v1, :cond_0

    new-instance v0, Lf/l/a/m/s/b;

    iget-object v1, p0, Lf/l/a/m/r/g;->h:Lf/l/a/m/j;

    invoke-virtual {v1}, Lf/l/a/m/j;->e()Lf/l/a/m/t/a;

    move-result-object v3

    iget-object v1, p0, Lf/l/a/m/r/g;->h:Lf/l/a/m/j;

    invoke-virtual {v1}, Lf/l/a/m/j;->z()Lf/l/a/v/a;

    move-result-object v1

    invoke-virtual {v1}, Lf/l/a/v/a;->j()Lf/l/a/w/b;

    move-result-object v4

    iget-object v1, p0, Lf/l/a/m/r/g;->h:Lf/l/a/m/j;

    sget-object v2, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v1, v2}, Lf/l/a/m/j;->C(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v5

    iget-object v1, p0, Lf/l/a/m/r/g;->h:Lf/l/a/m/j;

    invoke-virtual {v1}, Lf/l/a/m/j;->z()Lf/l/a/v/a;

    move-result-object v1

    iget-boolean v6, v1, Lf/l/a/v/a;->c:Z

    check-cast p1, Lf/l/a/m/d;

    iget-object v7, p1, Lf/l/a/m/d;->Z:Landroid/hardware/camera2/CameraCharacteristics;

    iget-object v8, p1, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lf/l/a/m/s/b;-><init>(Lf/l/a/m/t/a;Lf/l/a/w/b;Lf/l/a/w/b;ZLandroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    iget-object p1, p0, Lf/l/a/m/r/g;->g:Lf/l/a/s/b;

    invoke-virtual {p1, v0}, Lf/l/a/s/b;->c(Lf/l/a/s/c;)Lf/l/a/s/b;

    move-result-object p1

    const v1, 0x7fffffff

    invoke-virtual {p1, v1, v0}, Lf/l/a/s/b;->b(ILf/l/a/s/c;)Ljava/util/List;

    move-result-object v0

    :cond_0
    new-instance p1, Lf/l/a/m/r/c;

    iget-boolean v1, p0, Lf/l/a/m/r/g;->i:Z

    invoke-direct {p1, v0, v1}, Lf/l/a/m/r/c;-><init>(Ljava/util/List;Z)V

    new-instance v1, Lf/l/a/m/r/e;

    iget-boolean v2, p0, Lf/l/a/m/r/g;->i:Z

    invoke-direct {v1, v0, v2}, Lf/l/a/m/r/e;-><init>(Ljava/util/List;Z)V

    new-instance v2, Lf/l/a/m/r/i;

    iget-boolean v3, p0, Lf/l/a/m/r/g;->i:Z

    invoke-direct {v2, v0, v3}, Lf/l/a/m/r/i;-><init>(Ljava/util/List;Z)V

    const/4 v0, 0x3

    new-array v3, v0, [Lf/l/a/m/r/a;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v5, 0x1

    aput-object v1, v3, v5

    const/4 v6, 0x2

    aput-object v2, v3, v6

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lf/l/a/m/r/g;->e:Ljava/util/List;

    new-array v0, v0, [Lf/l/a/m/p/e;

    aput-object p1, v0, v4

    aput-object v1, v0, v5

    aput-object v2, v0, v6

    new-instance p1, Lf/l/a/m/p/j;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, v0}, Lf/l/a/m/p/j;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lf/l/a/m/r/g;->f:Lf/l/a/m/p/e;

    return-void
.end method
