.class public abstract Lf/l/a/m/r/b;
.super Lf/l/a/m/p/e;
.source "BaseReset.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# instance fields
.field public e:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lf/l/a/m/p/e;-><init>()V

    iput-boolean p1, p0, Lf/l/a/m/r/b;->e:Z

    return-void
.end method


# virtual methods
.method public final j(Lf/l/a/m/p/c;)V
    .locals 3
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lf/l/a/m/r/b;->e:Z

    if-eqz v1, :cond_0

    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_INFO_ACTIVE_ARRAY_SIZE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0, v1}, Lf/l/a/m/p/e;->k(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    new-instance v1, Landroid/hardware/camera2/params/MeteringRectangle;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Landroid/hardware/camera2/params/MeteringRectangle;-><init>(Landroid/graphics/Rect;I)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0, p1, v0}, Lf/l/a/m/r/b;->m(Lf/l/a/m/p/c;Landroid/hardware/camera2/params/MeteringRectangle;)V

    return-void
.end method

.method public abstract m(Lf/l/a/m/p/c;Landroid/hardware/camera2/params/MeteringRectangle;)V
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/params/MeteringRectangle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method
