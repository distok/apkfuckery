.class public Lf/l/a/m/b;
.super Lf/l/a/m/h;
.source "Camera1Engine.java"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/hardware/Camera$ErrorCallback;
.implements Lf/l/a/o/a$a;


# instance fields
.field public final W:Lf/l/a/m/q/a;

.field public X:Landroid/hardware/Camera;

.field public Y:I
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/l/a/m/j$g;)V
    .locals 0
    .param p1    # Lf/l/a/m/j$g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lf/l/a/m/h;-><init>(Lf/l/a/m/j$g;)V

    invoke-static {}, Lf/l/a/m/q/a;->a()Lf/l/a/m/q/a;

    move-result-object p1

    iput-object p1, p0, Lf/l/a/m/b;->W:Lf/l/a/m/q/a;

    return-void
.end method


# virtual methods
.method public F0(Lf/l/a/l/m;)V
    .locals 4
    .param p1    # Lf/l/a/l/m;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    iput-object p1, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "white balance ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/b$d;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/b$d;-><init>(Lf/l/a/m/b;Lf/l/a/l/m;)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public G0(F[Landroid/graphics/PointF;Z)V
    .locals 4
    .param p2    # [Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget v0, p0, Lf/l/a/m/h;->x:F

    iput p1, p0, Lf/l/a/m/h;->x:F

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string/jumbo v1, "zoom"

    invoke-virtual {p1, v1}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/b$f;

    invoke-direct {v3, p0, v0, p3, p2}, Lf/l/a/m/b$f;-><init>(Lf/l/a/m/b;FZ[Landroid/graphics/PointF;)V

    new-instance p2, Lf/l/a/m/v/c$c;

    invoke-direct {p2, p1, v2, v3}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance p3, Lf/l/a/m/v/a$a;

    invoke-direct {p3, p1, p2}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p2, 0x1

    invoke-virtual {p1, v1, p2, p3}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public I0(Lf/l/a/p/a;Lf/l/a/s/b;Landroid/graphics/PointF;)V
    .locals 3
    .param p1    # Lf/l/a/p/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lf/l/a/s/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    new-instance v2, Lf/l/a/m/b$a;

    invoke-direct {v2, p0, p2, p1, p3}, Lf/l/a/m/b$a;-><init>(Lf/l/a/m/b;Lf/l/a/s/b;Lf/l/a/p/a;Landroid/graphics/PointF;)V

    new-instance p1, Lf/l/a/m/v/c$c;

    invoke-direct {p1, v0, v1, v2}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance p2, Lf/l/a/m/v/a$a;

    invoke-direct {p2, v0, p1}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const-string p1, "auto focus"

    const/4 p3, 0x1

    invoke-virtual {v0, p1, p3, p2}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public P()Lcom/google/android/gms/tasks/Task;
    .locals 8
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStartBind:"

    aput-object v4, v2, v3

    const/4 v5, 0x1

    const-string v6, "Started"

    aput-object v6, v2, v5

    invoke-virtual {v0, v5, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v0}, Lf/l/a/v/a;->h()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Landroid/view/SurfaceHolder;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    iget-object v2, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v2}, Lf/l/a/v/a;->g()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v0}, Lf/l/a/v/a;->h()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Landroid/graphics/SurfaceTexture;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    iget-object v2, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v2}, Lf/l/a/v/a;->g()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    invoke-virtual {p0, v0}, Lf/l/a/m/h;->P0(Lf/l/a/l/i;)Lf/l/a/w/b;

    move-result-object v0

    iput-object v0, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    invoke-virtual {p0}, Lf/l/a/m/h;->Q0()Lf/l/a/w/b;

    move-result-object v0

    iput-object v0, p0, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    const/4 v0, 0x0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unknown CameraPreview output class."

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v6, 0x3

    new-array v7, v6, [Ljava/lang/Object;

    aput-object v4, v7, v3

    const-string v3, "Failed to bind."

    aput-object v3, v7, v5

    aput-object v0, v7, v1

    invoke-virtual {v2, v6, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v2, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v2, v0, v1}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v2
.end method

.method public Q()Lcom/google/android/gms/tasks/Task;
    .locals 12
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/l/a/c;",
            ">;"
        }
    .end annotation

    const-string v0, "onStartEngine:"

    const/4 v1, 0x3

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    :try_start_0
    iget v5, p0, Lf/l/a/m/b;->Y:I

    invoke-static {v5}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v5

    iput-object v5, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v5, :cond_0

    invoke-virtual {v5, p0}, Landroid/hardware/Camera;->setErrorCallback(Landroid/hardware/Camera$ErrorCallback;)V

    sget-object v5, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v0, v6, v2

    const-string v7, "Applying default parameters."

    aput-object v7, v6, v4

    invoke-virtual {v5, v4, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :try_start_1
    iget-object v6, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6

    new-instance v7, Lf/l/a/m/u/a;

    iget v8, p0, Lf/l/a/m/b;->Y:I

    iget-object v9, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v10, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v11, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v9, v10, v11}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result v9

    invoke-direct {v7, v6, v8, v9}, Lf/l/a/m/u/a;-><init>(Landroid/hardware/Camera$Parameters;IZ)V

    iput-object v7, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    invoke-virtual {p0, v6}, Lf/l/a/m/b;->Y0(Landroid/hardware/Camera$Parameters;)V

    iget-object v7, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v7, v6}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    iget-object v6, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    iget-object v7, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v8, Lf/l/a/m/t/b;->d:Lf/l/a/m/t/b;

    invoke-virtual {v7, v10, v11, v8}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/hardware/Camera;->setDisplayOrientation(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    new-array v1, v3, [Ljava/lang/Object;

    aput-object v0, v1, v2

    const-string v0, "Ended"

    aput-object v0, v1, v4

    invoke-virtual {v5, v4, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :catch_0
    sget-object v5, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "Failed to connect. Can\'t set display orientation, maybe preview already exists?"

    aput-object v0, v3, v4

    invoke-virtual {v5, v1, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v4}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    throw v0

    :catch_1
    move-exception v5

    sget-object v6, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "Failed to connect. Problem with camera params"

    aput-object v0, v3, v4

    invoke-virtual {v6, v1, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v5, v4}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v0

    :cond_0
    sget-object v5, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "Failed to connect. Camera is null, maybe in use by another app or already released?"

    aput-object v0, v3, v4

    invoke-virtual {v5, v1, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v4}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    throw v0

    :catch_2
    move-exception v5

    sget-object v6, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "Failed to connect. Maybe in use by another app?"

    aput-object v0, v3, v4

    invoke-virtual {v6, v1, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v5, v4}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v0
.end method

.method public R()Lcom/google/android/gms/tasks/Task;
    .locals 11
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const-string v0, "onStartPreview:"

    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "onStartPreview"

    aput-object v5, v3, v4

    const-string v6, "Dispatching onCameraPreviewStreamSizeChanged."

    const/4 v7, 0x1

    aput-object v6, v3, v7

    invoke-virtual {v1, v7, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v3, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast v3, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v3}, Lcom/otaliastudios/cameraview/CameraView$b;->h()V

    sget-object v3, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {p0, v3}, Lf/l/a/m/h;->C(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v6, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    iget v8, v3, Lf/l/a/w/b;->d:I

    iget v3, v3, Lf/l/a/w/b;->e:I

    invoke-virtual {v6, v8, v3}, Lf/l/a/v/a;->q(II)V

    iget-object v3, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v3, v4}, Lf/l/a/v/a;->p(I)V

    const/4 v3, 0x3

    :try_start_0
    iget-object v6, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v8, 0x11

    invoke-virtual {v6, v8}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    iget-object v9, p0, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    iget v10, v9, Lf/l/a/w/b;->d:I

    iget v9, v9, Lf/l/a/w/b;->e:I

    invoke-virtual {v6, v10, v9}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget-object v9, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v10, Lf/l/a/l/i;->d:Lf/l/a/l/i;

    if-ne v9, v10, :cond_0

    iget-object v9, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    iget v10, v9, Lf/l/a/w/b;->d:I

    iget v9, v9, Lf/l/a/w/b;->e:I

    invoke-virtual {v6, v10, v9}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v10}, Lf/l/a/m/h;->P0(Lf/l/a/l/i;)Lf/l/a/w/b;

    move-result-object v9

    iget v10, v9, Lf/l/a/w/b;->d:I

    iget v9, v9, Lf/l/a/w/b;->e:I

    invoke-virtual {v6, v10, v9}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :goto_0
    :try_start_1
    iget-object v9, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v9, v6}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0, p0}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    invoke-virtual {p0}, Lf/l/a/m/b;->i1()Lf/l/a/o/a;

    move-result-object v0

    iget-object v9, p0, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    iget-object v10, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    invoke-virtual {v0, v8, v9, v10}, Lf/l/a/o/a;->e(ILf/l/a/w/b;Lf/l/a/m/t/a;)V

    new-array v0, v2, [Ljava/lang/Object;

    aput-object v5, v0, v4

    const-string v8, "Starting preview with startPreview()."

    aput-object v8, v0, v7

    invoke-virtual {v1, v7, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :try_start_2
    iget-object v0, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    new-array v0, v2, [Ljava/lang/Object;

    aput-object v5, v0, v4

    const-string v2, "Started preview."

    aput-object v2, v0, v7

    invoke-virtual {v1, v7, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {v6}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v5, v6, v4

    const-string v4, "Failed to start preview."

    aput-object v4, v6, v7

    aput-object v0, v6, v2

    invoke-virtual {v1, v3, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v1, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v1, v0, v2}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    :catch_1
    move-exception v1

    sget-object v5, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v4

    const-string v0, "Failed to set params for camera. Maybe incorrect parameter put in params?"

    aput-object v0, v6, v7

    invoke-virtual {v5, v3, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v1, v2}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v0

    :catch_2
    move-exception v1

    sget-object v5, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v0, v6, v4

    const-string v0, "Failed to get params from camera. Maybe low level problem with camera or camera has already released?"

    aput-object v0, v6, v7

    invoke-virtual {v5, v3, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v1, v2}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "previewStreamSize should not be null at this point."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public S()Lcom/google/android/gms/tasks/Task;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    iput-object v0, p0, Lf/l/a/m/h;->l:Lf/l/a/w/b;

    :try_start_0
    iget-object v1, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v1}, Lf/l/a/v/a;->h()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroid/view/SurfaceHolder;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lf/l/a/m/h;->i:Lf/l/a/v/a;

    invoke-virtual {v1}, Lf/l/a/v/a;->h()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroid/graphics/SurfaceTexture;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unknown CameraPreview output class."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "onStopBind"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "Could not release surface"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :goto_0
    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public S0()Ljava/util/List;
    .locals 9
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;"
        }
    .end annotation

    const-string v0, "getPreviewStreamAvailableSizes:"

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    :try_start_0
    iget-object v4, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/Camera$Size;

    new-instance v7, Lf/l/a/w/b;

    iget v8, v6, Landroid/hardware/Camera$Size;->width:I

    iget v6, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v7, v8, v6}, Lf/l/a/w/b;-><init>(II)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v4, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v1

    aput-object v5, v3, v2

    invoke-virtual {v4, v2, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-object v5

    :catch_0
    move-exception v4

    sget-object v5, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v6, v3, [Ljava/lang/Object;

    aput-object v0, v6, v1

    const-string v0, "Failed to compute preview size. Camera params is empty"

    aput-object v0, v6, v2

    const/4 v0, 0x3

    invoke-virtual {v5, v0, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {v0, v4, v3}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw v0
.end method

.method public T()Lcom/google/android/gms/tasks/Task;
    .locals 11
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStopEngine:"

    aput-object v4, v2, v3

    const/4 v5, 0x1

    const-string v6, "About to clean up."

    aput-object v6, v2, v5

    invoke-virtual {v0, v5, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v6, "focus reset"

    invoke-virtual {v2, v6}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object v2, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v6, "focus end"

    invoke-virtual {v2, v6}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object v2, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    const-string v6, "Clean up."

    const/4 v7, 0x3

    const/4 v8, 0x0

    if-eqz v2, :cond_0

    :try_start_0
    new-array v2, v7, [Ljava/lang/Object;

    aput-object v4, v2, v3

    aput-object v6, v2, v5

    const-string v9, "Releasing camera."

    aput-object v9, v2, v1

    invoke-virtual {v0, v5, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v4, v2, v3

    aput-object v6, v2, v5

    const-string v9, "Released camera."

    aput-object v9, v2, v1

    invoke-virtual {v0, v5, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v4, v9, v3

    aput-object v6, v9, v5

    const-string v10, "Exception while releasing camera."

    aput-object v10, v9, v1

    aput-object v0, v9, v7

    invoke-virtual {v2, v1, v9}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :goto_0
    iput-object v8, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    iput-object v8, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    :cond_0
    iput-object v8, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iput-object v8, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v4, v2, v3

    aput-object v6, v2, v5

    const-string v3, "Returning."

    aput-object v3, v2, v1

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {v8}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public U()Lcom/google/android/gms/tasks/Task;
    .locals 9
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStopPreview:"

    aput-object v4, v2, v3

    const/4 v5, 0x1

    const-string v6, "Started."

    aput-object v6, v2, v5

    invoke-virtual {v0, v5, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    invoke-virtual {p0}, Lf/l/a/m/b;->i1()Lf/l/a/o/a;

    move-result-object v6

    invoke-virtual {v6}, Lf/l/a/o/a;->d()V

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v4, v6, v3

    const-string v7, "Releasing preview buffers."

    aput-object v7, v6, v5

    invoke-virtual {v0, v5, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v6, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v6, v2}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    :try_start_0
    new-array v6, v1, [Ljava/lang/Object;

    aput-object v4, v6, v3

    const-string v7, "Stopping preview."

    aput-object v7, v6, v5

    invoke-virtual {v0, v5, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v6, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v6}, Landroid/hardware/Camera;->stopPreview()V

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v4, v6, v3

    const-string v4, "Stopped preview."

    aput-object v4, v6, v5

    invoke-virtual {v0, v5, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v4, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v6, 0x3

    new-array v7, v6, [Ljava/lang/Object;

    const-string v8, "stopPreview"

    aput-object v8, v7, v3

    const-string v3, "Could not stop preview"

    aput-object v3, v7, v5

    aput-object v0, v7, v1

    invoke-virtual {v4, v6, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :goto_0
    invoke-static {v2}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public U0(I)Lf/l/a/o/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Lf/l/a/o/a;

    invoke-direct {v0, p1, p0}, Lf/l/a/o/a;-><init>(ILf/l/a/o/a$a;)V

    return-object v0
.end method

.method public V0()V
    .locals 6

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "RESTART PREVIEW:"

    aput-object v3, v1, v2

    const/4 v3, 0x1

    const-string v4, "scheduled. State:"

    aput-object v4, v1, v3

    iget-object v4, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v4, v4, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    const/4 v5, 0x2

    aput-object v4, v1, v5

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, v2}, Lf/l/a/m/j;->N0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {p0}, Lf/l/a/m/j;->K0()Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public W0(Lf/l/a/k$a;Z)V
    .locals 8
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "onTakePicture:"

    aput-object v3, v1, v2

    const-string v4, "executing."

    const/4 v5, 0x1

    aput-object v4, v1, v5

    invoke-virtual {p2, v5, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v4, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v6, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    sget-object v7, Lf/l/a/m/t/b;->e:Lf/l/a/m/t/b;

    invoke-virtual {v1, v4, v6, v7}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    move-result v1

    iput v1, p1, Lf/l/a/k$a;->b:I

    invoke-virtual {p0, v6}, Lf/l/a/m/h;->w(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v1

    iput-object v1, p1, Lf/l/a/k$a;->c:Lf/l/a/w/b;

    new-instance v1, Lf/l/a/u/a;

    iget-object v4, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-direct {v1, p1, p0, v4}, Lf/l/a/u/a;-><init>(Lf/l/a/k$a;Lf/l/a/m/b;Landroid/hardware/Camera;)V

    iput-object v1, p0, Lf/l/a/m/h;->k:Lf/l/a/u/d;

    invoke-virtual {v1}, Lf/l/a/u/d;->c()V

    new-array p1, v0, [Ljava/lang/Object;

    aput-object v3, p1, v2

    const-string v0, "executed."

    aput-object v0, p1, v5

    invoke-virtual {p2, v5, p1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-void
.end method

.method public final Y0(Landroid/hardware/Camera$Parameters;)V
    .locals 2
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v1, Lf/l/a/l/i;->e:Lf/l/a/l/i;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setRecordingHint(Z)V

    invoke-virtual {p0, p1}, Lf/l/a/m/b;->Z0(Landroid/hardware/Camera$Parameters;)V

    sget-object v0, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/b;->b1(Landroid/hardware/Camera$Parameters;Lf/l/a/l/f;)Z

    invoke-virtual {p0, p1}, Lf/l/a/m/b;->d1(Landroid/hardware/Camera$Parameters;)Z

    sget-object v0, Lf/l/a/l/m;->d:Lf/l/a/l/m;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/b;->g1(Landroid/hardware/Camera$Parameters;Lf/l/a/l/m;)Z

    sget-object v0, Lf/l/a/l/h;->d:Lf/l/a/l/h;

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/b;->c1(Landroid/hardware/Camera$Parameters;Lf/l/a/l/h;)Z

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/b;->h1(Landroid/hardware/Camera$Parameters;F)Z

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/b;->a1(Landroid/hardware/Camera$Parameters;F)Z

    iget-boolean v1, p0, Lf/l/a/m/h;->z:Z

    invoke-virtual {p0, v1}, Lf/l/a/m/b;->e1(Z)Z

    invoke-virtual {p0, p1, v0}, Lf/l/a/m/b;->f1(Landroid/hardware/Camera$Parameters;F)Z

    return-void
.end method

.method public final Z0(Landroid/hardware/Camera$Parameters;)V
    .locals 3
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lf/l/a/m/h;->K:Lf/l/a/l/i;

    sget-object v2, Lf/l/a/l/i;->e:Lf/l/a/l/i;

    if-ne v1, v2, :cond_0

    const-string v1, "continuous-video"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v1, "continuous-picture"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    return-void

    :cond_1
    const-string v1, "infinity"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v1, "fixed"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public final a1(Landroid/hardware/Camera$Parameters;F)Z
    .locals 3
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-boolean v1, v0, Lf/l/a/c;->l:Z

    if-eqz v1, :cond_2

    iget p2, v0, Lf/l/a/c;->n:F

    iget v0, v0, Lf/l/a/c;->m:F

    iget v1, p0, Lf/l/a/m/h;->y:F

    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    move p2, v0

    goto :goto_0

    :cond_0
    cmpl-float v0, v1, p2

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    move p2, v1

    :goto_0
    iput p2, p0, Lf/l/a/m/h;->y:F

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getExposureCompensationStep()F

    move-result v0

    div-float/2addr p2, v0

    float-to-int p2, p2

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->setExposureCompensation(I)V

    const/4 p1, 0x1

    return p1

    :cond_2
    iput p2, p0, Lf/l/a/m/h;->y:F

    const/4 p1, 0x0

    return p1
.end method

.method public final b1(Landroid/hardware/Camera$Parameters;Lf/l/a/l/f;)Z
    .locals 2
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/l/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v1, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    invoke-virtual {v0, v1}, Lf/l/a/c;->c(Lf/l/a/l/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lf/l/a/m/b;->W:Lf/l/a/m/q/a;

    iget-object v0, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/l/a/m/q/a;->b:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput-object p2, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    const/4 p1, 0x0

    return p1
.end method

.method public c(Lf/l/a/l/e;)Z
    .locals 7
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/b;->W:Lf/l/a/m/q/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/l/a/m/q/a;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "collectCameraInfo"

    aput-object v4, v2, v3

    const/4 v4, 0x1

    const-string v5, "Facing:"

    aput-object v5, v2, v4

    const/4 v5, 0x2

    aput-object p1, v2, v5

    const/4 v5, 0x3

    const-string v6, "Internal:"

    aput-object v6, v2, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x4

    aput-object v5, v2, v6

    const/4 v5, 0x5

    const-string v6, "Cameras:"

    aput-object v6, v2, v5

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x6

    aput-object v5, v2, v6

    invoke-virtual {v1, v4, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_1

    invoke-static {v5, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v6, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v6, v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    invoke-virtual {v0, p1, v1}, Lf/l/a/m/t/a;->f(Lf/l/a/l/e;I)V

    iput v5, p0, Lf/l/a/m/b;->Y:I

    return v4

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method public final c1(Landroid/hardware/Camera$Parameters;Lf/l/a/l/h;)Z
    .locals 2
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/l/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v1, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    invoke-virtual {v0, v1}, Lf/l/a/c;->c(Lf/l/a/l/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lf/l/a/m/b;->W:Lf/l/a/m/q/a;

    iget-object v0, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/l/a/m/q/a;->e:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->setSceneMode(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput-object p2, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    const/4 p1, 0x0

    return p1
.end method

.method public d0(F[F[Landroid/graphics/PointF;Z)V
    .locals 9
    .param p2    # [F
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # [Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget v2, p0, Lf/l/a/m/h;->y:F

    iput p1, p0, Lf/l/a/m/h;->y:F

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v6, "exposure correction"

    invoke-virtual {p1, v6}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v7, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v8, Lf/l/a/m/b$g;

    move-object v0, v8

    move-object v1, p0

    move v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lf/l/a/m/b$g;-><init>(Lf/l/a/m/b;FZ[F[Landroid/graphics/PointF;)V

    new-instance p2, Lf/l/a/m/v/c$c;

    invoke-direct {p2, p1, v7, v8}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance p3, Lf/l/a/m/v/a$a;

    invoke-direct {p3, p1, p2}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 p2, 0x1

    invoke-virtual {p1, v6, p2, p3}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final d1(Landroid/hardware/Camera$Parameters;)Z
    .locals 2
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/hardware/Camera$Parameters;->setGpsLatitude(D)V

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/hardware/Camera$Parameters;->setGpsLongitude(D)V

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/hardware/Camera$Parameters;->setGpsAltitude(D)V

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/hardware/Camera$Parameters;->setGpsTimestamp(J)V

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setGpsProcessingMethod(Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public final e1(Z)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    iget v1, p0, Lf/l/a/m/b;->Y:I

    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-boolean v0, v0, Landroid/hardware/Camera$CameraInfo;->canDisableShutterSound:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object p1, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    iget-boolean v0, p0, Lf/l/a/m/h;->z:Z

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->enableShutterSound(Z)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1

    :cond_0
    iget-boolean v0, p0, Lf/l/a/m/h;->z:Z

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    iput-boolean p1, p0, Lf/l/a/m/h;->z:Z

    return v1
.end method

.method public f0(Lf/l/a/l/f;)V
    .locals 4
    .param p1    # Lf/l/a/l/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    iput-object p1, p0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "flash ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/b$b;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/b$b;-><init>(Lf/l/a/m/b;Lf/l/a/l/f;)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public final f1(Landroid/hardware/Camera$Parameters;F)Z
    .locals 9
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v0

    iget-boolean v1, p0, Lf/l/a/m/h;->D:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget v1, p0, Lf/l/a/m/h;->C:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    new-instance v1, Lf/l/a/m/c;

    invoke-direct {v1, p0}, Lf/l/a/m/c;-><init>(Lf/l/a/m/b;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lf/l/a/m/a;

    invoke-direct {v1, p0}, Lf/l/a/m/a;-><init>(Lf/l/a/m/b;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :goto_0
    iget v1, p0, Lf/l/a/m/h;->C:F

    const/high16 v3, 0x447a0000    # 1000.0f

    const/4 v4, 0x0

    const/4 v5, 0x1

    cmpl-float v2, v1, v2

    if-nez v2, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    aget v2, v1, v4

    int-to-float v2, v2

    div-float/2addr v2, v3

    aget v6, v1, v5

    int-to-float v6, v6

    div-float/2addr v6, v3

    const/high16 v7, 0x41f00000    # 30.0f

    cmpg-float v8, v2, v7

    if-gtz v8, :cond_2

    cmpg-float v7, v7, v6

    if-lez v7, :cond_3

    :cond_2
    const/high16 v7, 0x41c00000    # 24.0f

    cmpg-float v2, v2, v7

    if-gtz v2, :cond_1

    cmpg-float v2, v7, v6

    if-gtz v2, :cond_1

    :cond_3
    aget p2, v1, v4

    aget v0, v1, v5

    invoke-virtual {p1, p2, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    return v5

    :cond_4
    iget-object v2, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget v2, v2, Lf/l/a/c;->q:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lf/l/a/m/h;->C:F

    iget-object v2, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget v2, v2, Lf/l/a/c;->p:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lf/l/a/m/h;->C:F

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    aget v2, v1, v4

    int-to-float v2, v2

    div-float/2addr v2, v3

    aget v6, v1, v5

    int-to-float v6, v6

    div-float/2addr v6, v3

    iget v7, p0, Lf/l/a/m/h;->C:F

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    int-to-float v7, v7

    cmpg-float v2, v2, v7

    if-gtz v2, :cond_5

    cmpg-float v2, v7, v6

    if-gtz v2, :cond_5

    aget p2, v1, v4

    aget v0, v1, v5

    invoke-virtual {p1, p2, v0}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    return v5

    :cond_6
    iput p2, p0, Lf/l/a/m/h;->C:F

    return v4
.end method

.method public g0(I)V
    .locals 0

    const/16 p1, 0x11

    iput p1, p0, Lf/l/a/m/h;->o:I

    return-void
.end method

.method public final g1(Landroid/hardware/Camera$Parameters;Lf/l/a/l/m;)Z
    .locals 2
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/l/m;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object v1, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    invoke-virtual {v0, v1}, Lf/l/a/c;->c(Lf/l/a/l/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lf/l/a/m/b;->W:Lf/l/a/m/q/a;

    iget-object v0, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/l/a/m/q/a;->c:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->setWhiteBalance(Ljava/lang/String;)V

    const-string p2, "auto-whitebalance-lock"

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->remove(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput-object p2, p0, Lf/l/a/m/h;->r:Lf/l/a/l/m;

    const/4 p1, 0x0

    return p1
.end method

.method public final h1(Landroid/hardware/Camera$Parameters;F)Z
    .locals 1
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-boolean v0, v0, Lf/l/a/c;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result p2

    int-to-float p2, p2

    iget v0, p0, Lf/l/a/m/h;->x:F

    mul-float v0, v0, p2

    float-to-int p2, v0

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    iget-object p2, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    iput p2, p0, Lf/l/a/m/h;->x:F

    const/4 p1, 0x0

    return p1
.end method

.method public i1()Lf/l/a/o/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Lf/l/a/m/h;->R0()Lf/l/a/o/c;

    move-result-object v0

    check-cast v0, Lf/l/a/o/a;

    return-object v0
.end method

.method public j1([B)V
    .locals 2
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v0, v0, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    invoke-virtual {v0, v1}, Lf/l/a/m/v/b;->f(Lf/l/a/m/v/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v0, v0, Lf/l/a/m/v/c;->g:Lf/l/a/m/v/b;

    invoke-virtual {v0, v1}, Lf/l/a/m/v/b;->f(Lf/l/a/m/v/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0, p1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    :cond_0
    return-void
.end method

.method public k0(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/l/a/m/h;->p:Z

    return-void
.end method

.method public l0(Lf/l/a/l/h;)V
    .locals 4
    .param p1    # Lf/l/a/l/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    iput-object p1, p0, Lf/l/a/m/h;->u:Lf/l/a/l/h;

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hdr ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/b$e;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/b$e;-><init>(Lf/l/a/m/b;Lf/l/a/l/h;)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public m0(Landroid/location/Location;)V
    .locals 3
    .param p1    # Landroid/location/Location;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    iput-object p1, p0, Lf/l/a/m/h;->w:Landroid/location/Location;

    iget-object p1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v2, Lf/l/a/m/b$c;

    invoke-direct {v2, p0, v0}, Lf/l/a/m/b$c;-><init>(Lf/l/a/m/b;Landroid/location/Location;)V

    new-instance v0, Lf/l/a/m/v/c$c;

    invoke-direct {v0, p1, v1, v2}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v1, Lf/l/a/m/v/a$a;

    invoke-direct {v1, p1, v0}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    const-string v2, "location"

    invoke-virtual {p1, v2, v0, v1}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public onError(ILandroid/hardware/Camera;)V
    .locals 5

    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "Internal Camera1 error."

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/4 v2, 0x3

    invoke-virtual {p2, v2, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    if-eq p1, v4, :cond_0

    if-eq p1, v0, :cond_0

    const/16 p2, 0x64

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x3

    :goto_0
    new-instance p1, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {p1, v1, v3}, Lcom/otaliastudios/cameraview/CameraException;-><init>(Ljava/lang/Throwable;I)V

    throw p1
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lf/l/a/m/b;->i1()Lf/l/a/o/a;

    move-result-object p2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, Lf/l/a/o/c;->a(Ljava/lang/Object;J)Lf/l/a/o/b;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p2, p0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast p2, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {p2, p1}, Lcom/otaliastudios/cameraview/CameraView$b;->b(Lf/l/a/o/b;)V

    :cond_1
    return-void
.end method

.method public p0(Lf/l/a/l/j;)V
    .locals 3
    .param p1    # Lf/l/a/l/j;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/l/j;->d:Lf/l/a/l/j;

    if-ne p1, v0, :cond_0

    iput-object p1, p0, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported picture format: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public t0(Z)V
    .locals 4

    iget-boolean v0, p0, Lf/l/a/m/h;->z:Z

    iput-boolean p1, p0, Lf/l/a/m/h;->z:Z

    iget-object v1, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play sounds ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/b$h;

    invoke-direct {v3, p0, v0}, Lf/l/a/m/b$h;-><init>(Lf/l/a/m/b;Z)V

    invoke-virtual {v1, p1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public v0(F)V
    .locals 4

    iput p1, p0, Lf/l/a/m/h;->C:F

    iget-object v0, p0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preview fps ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    new-instance v3, Lf/l/a/m/b$i;

    invoke-direct {v3, p0, p1}, Lf/l/a/m/b$i;-><init>(Lf/l/a/m/b;F)V

    invoke-virtual {v0, v1, v2, v3}, Lf/l/a/m/v/c;->h(Ljava/lang/String;Lf/l/a/m/v/b;Ljava/lang/Runnable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method
