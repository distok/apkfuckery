.class public Lf/l/a/m/d$m;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source "Camera2Engine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/d;->Q()Lcom/google/android/gms/tasks/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/android/gms/tasks/TaskCompletionSource;

.field public final synthetic b:Lf/l/a/m/d;


# direct methods
.method public constructor <init>(Lf/l/a/m/d;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iput-object p2, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 5
    .param p1    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance p1, Lcom/otaliastudios/cameraview/CameraException;

    const/4 v0, 0x3

    invoke-direct {p1, v0}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    iget-object v0, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {v0}, Lf/h/a/f/p/b0;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void

    :cond_0
    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "CameraDevice.StateCallback reported disconnection."

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    throw p1
.end method

.method public onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 5
    .param p1    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object p1, p1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-virtual {p1}, Lf/h/a/f/p/b0;->o()Z

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x3

    if-nez p1, :cond_1

    iget-object p1, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v4, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eq p2, v0, :cond_0

    if-eq p2, v2, :cond_0

    if-eq p2, v3, :cond_0

    const/4 v2, 0x4

    if-eq p2, v2, :cond_0

    const/4 v2, 0x5

    if-eq p2, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    new-instance p2, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {p2, v0}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    invoke-virtual {p1, p2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void

    :cond_1
    sget-object p1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v2, v2, [Ljava/lang/Object;

    const-string v4, "CameraDevice.StateCallback reported an error:"

    aput-object v4, v2, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v0

    invoke-virtual {p1, v3, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance p1, Lcom/otaliastudios/cameraview/CameraException;

    invoke-direct {p1, v3}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    throw p1
.end method

.method public onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 6
    .param p1    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iput-object p1, v0, Lf/l/a/m/d;->Y:Landroid/hardware/camera2/CameraDevice;

    :try_start_0
    sget-object p1, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "onStartEngine:"

    aput-object v2, v0, v1

    const-string v1, "Opened camera device."

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p1, v2, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iget-object v0, p1, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p1, Lf/l/a/m/d;->X:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    iput-object v0, p1, Lf/l/a/m/d;->Z:Landroid/hardware/camera2/CameraCharacteristics;

    iget-object p1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iget-object p1, p1, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    sget-object v0, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v1, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {p1, v0, v1}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result p1

    iget-object v0, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    if-ne v0, v2, :cond_0

    const/16 v0, 0x20

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown format:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iget-object v1, v1, Lf/l/a/m/h;->v:Lf/l/a/l/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/16 v0, 0x100

    :goto_0
    iget-object v1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    new-instance v3, Lf/l/a/m/u/b;

    iget-object v4, v1, Lf/l/a/m/d;->W:Landroid/hardware/camera2/CameraManager;

    iget-object v5, v1, Lf/l/a/m/d;->X:Ljava/lang/String;

    invoke-direct {v3, v4, v5, p1, v0}, Lf/l/a/m/u/b;-><init>(Landroid/hardware/camera2/CameraManager;Ljava/lang/String;ZI)V

    iput-object v3, v1, Lf/l/a/m/h;->j:Lf/l/a/c;

    iget-object p1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    invoke-virtual {p1, v2}, Lf/l/a/m/d;->m1(I)Landroid/hardware/camera2/CaptureRequest$Builder;
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object p1, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/h;->j:Lf/l/a/c;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/l/a/m/d$m;->a:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v1, p0, Lf/l/a/m/d$m;->b:Lf/l/a/m/d;

    invoke-virtual {v1, p1}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->a(Ljava/lang/Exception;)Z

    return-void
.end method
