.class public Lf/l/a/m/b$f;
.super Ljava/lang/Object;
.source "Camera1Engine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/b;->G0(F[Landroid/graphics/PointF;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:F

.field public final synthetic e:Z

.field public final synthetic f:[Landroid/graphics/PointF;

.field public final synthetic g:Lf/l/a/m/b;


# direct methods
.method public constructor <init>(Lf/l/a/m/b;FZ[Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/b$f;->g:Lf/l/a/m/b;

    iput p2, p0, Lf/l/a/m/b$f;->d:F

    iput-boolean p3, p0, Lf/l/a/m/b$f;->e:Z

    iput-object p4, p0, Lf/l/a/m/b$f;->f:[Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lf/l/a/m/b$f;->g:Lf/l/a/m/b;

    iget-object v0, v0, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iget-object v1, p0, Lf/l/a/m/b$f;->g:Lf/l/a/m/b;

    iget v2, p0, Lf/l/a/m/b$f;->d:F

    invoke-virtual {v1, v0, v2}, Lf/l/a/m/b;->h1(Landroid/hardware/Camera$Parameters;F)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lf/l/a/m/b$f;->g:Lf/l/a/m/b;

    iget-object v1, v1, Lf/l/a/m/b;->X:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-boolean v0, p0, Lf/l/a/m/b$f;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/b$f;->g:Lf/l/a/m/b;

    iget-object v1, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    iget v0, v0, Lf/l/a/m/h;->x:F

    iget-object v2, p0, Lf/l/a/m/b$f;->f:[Landroid/graphics/PointF;

    check-cast v1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v1, v0, v2}, Lcom/otaliastudios/cameraview/CameraView$b;->f(F[Landroid/graphics/PointF;)V

    :cond_0
    return-void
.end method
