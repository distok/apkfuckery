.class public Lf/l/a/m/d$a;
.super Ljava/lang/Object;
.source "Camera2Engine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/d;->f0(Lf/l/a/l/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/l/f;

.field public final synthetic e:Lf/l/a/l/f;

.field public final synthetic f:Lf/l/a/m/d;


# direct methods
.method public constructor <init>(Lf/l/a/m/d;Lf/l/a/l/f;Lf/l/a/l/f;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    iput-object p2, p0, Lf/l/a/m/d$a;->d:Lf/l/a/l/f;

    iput-object p3, p0, Lf/l/a/m/d$a;->e:Lf/l/a/l/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    iget-object v1, v0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v2, p0, Lf/l/a/m/d$a;->d:Lf/l/a/l/f;

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/d;->d1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/f;)Z

    move-result v0

    iget-object v1, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    iget-object v2, v1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v3, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    sget-object v0, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    iput-object v0, v1, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    iget-object v0, v1, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v2, p0, Lf/l/a/m/d$a;->d:Lf/l/a/l/f;

    invoke-virtual {v1, v0, v2}, Lf/l/a/m/d;->d1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/f;)Z

    :try_start_0
    iget-object v0, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    iget-object v1, v0, Lf/l/a/m/d;->a0:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v0, v0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    iget-object v1, p0, Lf/l/a/m/d$a;->e:Lf/l/a/l/f;

    iput-object v1, v0, Lf/l/a/m/h;->q:Lf/l/a/l/f;

    iget-object v1, v0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v2, p0, Lf/l/a/m/d$a;->d:Lf/l/a/l/f;

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/d;->d1(Landroid/hardware/camera2/CaptureRequest$Builder;Lf/l/a/l/f;)Z

    iget-object v0, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    invoke-virtual {v0}, Lf/l/a/m/d;->g1()V

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lf/l/a/m/d$a;->f:Lf/l/a/m/d;

    invoke-virtual {v1, v0}, Lf/l/a/m/d;->k1(Landroid/hardware/camera2/CameraAccessException;)Lcom/otaliastudios/cameraview/CameraException;

    move-result-object v0

    throw v0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lf/l/a/m/d;->g1()V

    :cond_2
    :goto_1
    return-void
.end method
