.class public Lf/l/a/m/i;
.super Ljava/lang/Object;
.source "CameraBaseEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lf/l/a/m/h;


# direct methods
.method public constructor <init>(Lf/l/a/m/h;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/i;->d:Lf/l/a/m/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lf/l/a/m/i;->d:Lf/l/a/m/h;

    invoke-virtual {v0}, Lf/l/a/m/h;->Q0()Lf/l/a/w/b;

    move-result-object v0

    iget-object v1, p0, Lf/l/a/m/i;->d:Lf/l/a/m/h;

    iget-object v1, v1, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    invoke-virtual {v0, v1}, Lf/l/a/w/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "onSurfaceChanged:"

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v1, :cond_0

    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v2, v1, v3

    const-string v2, "The computed preview size is identical. No op."

    aput-object v2, v1, v5

    invoke-virtual {v0, v5, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    const-string v2, "Computed a new preview size. Calling onPreviewStreamSizeChanged()."

    aput-object v2, v4, v5

    invoke-virtual {v1, v5, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lf/l/a/m/i;->d:Lf/l/a/m/h;

    iput-object v0, v1, Lf/l/a/m/h;->m:Lf/l/a/w/b;

    invoke-virtual {v1}, Lf/l/a/m/h;->V0()V

    :goto_0
    return-void
.end method
