.class public Lf/l/a/m/u/a;
.super Lf/l/a/c;
.source "Camera1Options.java"


# direct methods
.method public constructor <init>(Landroid/hardware/Camera$Parameters;IZ)V
    .locals 8
    .param p1    # Landroid/hardware/Camera$Parameters;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Lf/l/a/c;-><init>()V

    invoke-static {}, Lf/l/a/m/q/a;->a()Lf/l/a/m/q/a;

    move-result-object v0

    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    invoke-static {v4, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v5, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    sget-object v6, Lf/l/a/m/q/a;->d:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v6, v5}, Lf/l/a/m/q/a;->b(Ljava/util/Map;Ljava/lang/Object;)Lf/l/a/l/c;

    move-result-object v5

    check-cast v5, Lf/l/a/l/e;

    if-eqz v5, :cond_0

    iget-object v6, p0, Lf/l/a/c;->b:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedWhiteBalance()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v4, Lf/l/a/m/q/a;->c:Ljava/util/Map;

    invoke-virtual {v0, v4, v2}, Lf/l/a/m/q/a;->b(Ljava/util/Map;Ljava/lang/Object;)Lf/l/a/l/c;

    move-result-object v2

    check-cast v2, Lf/l/a/l/m;

    if-eqz v2, :cond_2

    iget-object v4, p0, Lf/l/a/c;->a:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lf/l/a/c;->c:Ljava/util/Set;

    sget-object v2, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v4, Lf/l/a/m/q/a;->b:Ljava/util/Map;

    invoke-virtual {v0, v4, v2}, Lf/l/a/m/q/a;->b(Ljava/util/Map;Ljava/lang/Object;)Lf/l/a/l/c;

    move-result-object v2

    check-cast v2, Lf/l/a/l/f;

    if-eqz v2, :cond_4

    iget-object v4, p0, Lf/l/a/c;->c:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lf/l/a/c;->d:Ljava/util/Set;

    sget-object v2, Lf/l/a/l/h;->d:Lf/l/a/l/h;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v4, Lf/l/a/m/q/a;->e:Ljava/util/Map;

    invoke-virtual {v0, v4, v2}, Lf/l/a/m/q/a;->b(Ljava/util/Map;Ljava/lang/Object;)Lf/l/a/l/c;

    move-result-object v2

    check-cast v2, Lf/l/a/l/h;

    if-eqz v2, :cond_6

    iget-object v4, p0, Lf/l/a/c;->d:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v0

    iput-boolean v0, p0, Lf/l/a/c;->k:Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lf/l/a/c;->o:Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getExposureCompensationStep()F

    move-result v0

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getMinExposureCompensation()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v0

    iput v1, p0, Lf/l/a/c;->m:F

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getMaxExposureCompensation()I

    move-result v1

    int-to-float v1, v1

    mul-float v1, v1, v0

    iput v1, p0, Lf/l/a/c;->n:F

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getMinExposureCompensation()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_9

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getMaxExposureCompensation()I

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto :goto_5

    :cond_9
    :goto_4
    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lf/l/a/c;->l:Z

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/Camera$Size;

    if-eqz p3, :cond_a

    iget v4, v2, Landroid/hardware/Camera$Size;->height:I

    goto :goto_7

    :cond_a
    iget v4, v2, Landroid/hardware/Camera$Size;->width:I

    :goto_7
    if-eqz p3, :cond_b

    iget v2, v2, Landroid/hardware/Camera$Size;->width:I

    goto :goto_8

    :cond_b
    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    :goto_8
    iget-object v5, p0, Lf/l/a/c;->e:Ljava/util/Set;

    new-instance v6, Lf/l/a/w/b;

    invoke-direct {v6, v4, v2}, Lf/l/a/w/b;-><init>(II)V

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lf/l/a/c;->g:Ljava/util/Set;

    invoke-static {v4, v2}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_c
    const v0, 0x7fffffff

    int-to-long v4, v0

    int-to-long v6, v0

    mul-long v4, v4, v6

    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lf/l/a/q/a;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lf/l/a/q/a$a;

    invoke-direct {v2, v4, v5}, Lf/l/a/q/a$a;-><init>(J)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_d
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_e

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/l/a/w/b;

    sget-object v4, Lf/l/a/q/a;->b:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {p2, v2}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static {p2, v2}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object p2

    goto :goto_9

    :cond_e
    invoke-static {p2, v3}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object p2

    :goto_9
    iget v0, p2, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget p2, p2, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedVideoSizes()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_12

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    iget v5, v4, Landroid/hardware/Camera$Size;->width:I

    if-gt v5, v0, :cond_f

    iget v4, v4, Landroid/hardware/Camera$Size;->height:I

    if-gt v4, p2, :cond_f

    if-eqz p3, :cond_10

    move v6, v4

    goto :goto_b

    :cond_10
    move v6, v5

    :goto_b
    if-eqz p3, :cond_11

    goto :goto_c

    :cond_11
    move v5, v4

    :goto_c
    iget-object v4, p0, Lf/l/a/c;->f:Ljava/util/Set;

    new-instance v7, Lf/l/a/w/b;

    invoke-direct {v7, v6, v5}, Lf/l/a/w/b;-><init>(II)V

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lf/l/a/c;->h:Ljava/util/Set;

    invoke-static {v6, v5}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_12
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    iget v5, v4, Landroid/hardware/Camera$Size;->width:I

    if-gt v5, v0, :cond_13

    iget v4, v4, Landroid/hardware/Camera$Size;->height:I

    if-gt v4, p2, :cond_13

    if-eqz p3, :cond_14

    move v6, v4

    goto :goto_e

    :cond_14
    move v6, v5

    :goto_e
    if-eqz p3, :cond_15

    goto :goto_f

    :cond_15
    move v5, v4

    :goto_f
    iget-object v4, p0, Lf/l/a/c;->f:Ljava/util/Set;

    new-instance v7, Lf/l/a/w/b;

    invoke-direct {v7, v6, v5}, Lf/l/a/w/b;-><init>(II)V

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lf/l/a/c;->h:Ljava/util/Set;

    invoke-static {v6, v5}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_d

    :cond_16
    const p2, 0x7f7fffff    # Float.MAX_VALUE

    iput p2, p0, Lf/l/a/c;->p:F

    const p2, -0x800001

    iput p2, p0, Lf/l/a/c;->q:F

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_10
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_17

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [I

    aget p3, p2, v3

    int-to-float p3, p3

    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr p3, v0

    aget p2, p2, v1

    int-to-float p2, p2

    div-float/2addr p2, v0

    iget v0, p0, Lf/l/a/c;->p:F

    invoke-static {v0, p3}, Ljava/lang/Math;->min(FF)F

    move-result p3

    iput p3, p0, Lf/l/a/c;->p:F

    iget p3, p0, Lf/l/a/c;->q:F

    invoke-static {p3, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iput p2, p0, Lf/l/a/c;->q:F

    goto :goto_10

    :cond_17
    iget-object p1, p0, Lf/l/a/c;->i:Ljava/util/Set;

    sget-object p2, Lf/l/a/l/j;->d:Lf/l/a/l/j;

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/l/a/c;->j:Ljava/util/Set;

    const/16 p2, 0x11

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method
