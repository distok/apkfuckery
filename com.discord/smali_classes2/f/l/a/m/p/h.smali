.class public Lf/l/a/m/p/h;
.super Lf/l/a/m/p/e;
.source "SequenceAction.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# instance fields
.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/m/p/e;",
            ">;"
        }
    .end annotation
.end field

.field public f:I


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/l/a/m/p/e;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf/l/a/m/p/e;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lf/l/a/m/p/h;->f:I

    iput-object p1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-virtual {p0}, Lf/l/a/m/p/h;->m()V

    return-void
.end method


# virtual methods
.method public b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p0, Lf/l/a/m/p/h;->f:I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    invoke-virtual {v0, p1, p2, p3}, Lf/l/a/m/p/e;->b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V

    :cond_0
    return-void
.end method

.method public c(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-boolean v0, p0, Lf/l/a/m/p/e;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/m/p/h;->j(Lf/l/a/m/p/c;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/l/a/m/p/e;->d:Z

    :cond_0
    iget v0, p0, Lf/l/a/m/p/h;->f:I

    if-ltz v0, :cond_1

    iget-object v1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    invoke-virtual {v0, p1, p2}, Lf/l/a/m/p/e;->c(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;)V

    :cond_1
    return-void
.end method

.method public d(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/CaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p0, Lf/l/a/m/p/h;->f:I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    invoke-virtual {v0, p1, p2, p3}, Lf/l/a/m/p/e;->d(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V

    :cond_0
    return-void
.end method

.method public h(Lf/l/a/m/p/c;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p0, Lf/l/a/m/p/h;->f:I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    invoke-virtual {v0, p1}, Lf/l/a/m/p/e;->h(Lf/l/a/m/p/c;)V

    :cond_0
    return-void
.end method

.method public j(Lf/l/a/m/p/c;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    iget v0, p0, Lf/l/a/m/p/h;->f:I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    invoke-virtual {v0, p1}, Lf/l/a/m/p/e;->j(Lf/l/a/m/p/c;)V

    :cond_0
    return-void
.end method

.method public final m()V
    .locals 5

    iget v0, p0, Lf/l/a/m/p/h;->f:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    if-ne v0, v4, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lf/l/a/m/p/e;->l(I)V

    goto :goto_1

    :cond_2
    iget v0, p0, Lf/l/a/m/p/h;->f:I

    add-int/2addr v0, v2

    iput v0, p0, Lf/l/a/m/p/h;->f:I

    iget-object v1, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    new-instance v1, Lf/l/a/m/p/h$a;

    invoke-direct {v1, p0}, Lf/l/a/m/p/h$a;-><init>(Lf/l/a/m/p/h;)V

    invoke-virtual {v0, v1}, Lf/l/a/m/p/e;->f(Lf/l/a/m/p/b;)V

    if-nez v3, :cond_3

    iget-object v0, p0, Lf/l/a/m/p/h;->e:Ljava/util/List;

    iget v1, p0, Lf/l/a/m/p/h;->f:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/e;

    iget-object v1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    invoke-virtual {v0, v1}, Lf/l/a/m/p/e;->j(Lf/l/a/m/p/c;)V

    :cond_3
    :goto_1
    return-void
.end method
