.class public abstract Lf/l/a/m/p/e;
.super Ljava/lang/Object;
.source "BaseAction.java"

# interfaces
.implements Lf/l/a/m/p/a;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/m/p/b;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Lf/l/a/m/p/c;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf/l/a/m/p/e;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Lf/l/a/m/p/c;)V
    .locals 1
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object v0, p1

    check-cast v0, Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/d;->i0:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lf/l/a/m/p/e;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->h(Lf/l/a/m/p/c;)V

    const p1, 0x7fffffff

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->l(I)V

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/l/a/m/p/e;->d:Z

    return-void
.end method

.method public b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 0
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public c(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;)V
    .locals 0
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iget-boolean p2, p0, Lf/l/a/m/p/e;->d:Z

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->j(Lf/l/a/m/p/c;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lf/l/a/m/p/e;->d:Z

    :cond_0
    return-void
.end method

.method public d(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
    .locals 0
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/CaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public final e(Lf/l/a/m/p/c;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    move-object v0, p1

    check-cast v0, Lf/l/a/m/d;

    iget-object v1, v0, Lf/l/a/m/d;->i0:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lf/l/a/m/d;->i0:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v0, p1

    check-cast v0, Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/d;->c0:Landroid/hardware/camera2/TotalCaptureResult;

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->j(Lf/l/a/m/p/c;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lf/l/a/m/p/e;->d:Z

    :goto_0
    return-void
.end method

.method public f(Lf/l/a/m/p/b;)V
    .locals 1
    .param p1    # Lf/l/a/m/p/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lf/l/a/m/p/e;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/p/e;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lf/l/a/m/p/e;->b:I

    invoke-interface {p1, p0, v0}, Lf/l/a/m/p/b;->a(Lf/l/a/m/p/a;I)V

    :cond_0
    return-void
.end method

.method public g()Z
    .locals 2

    iget v0, p0, Lf/l/a/m/p/e;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public h(Lf/l/a/m/p/c;)V
    .locals 0
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public i(Lf/l/a/m/p/c;)V
    .locals 0
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public j(Lf/l/a/m/p/c;)V
    .locals 0
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    return-void
.end method

.method public k(Landroid/hardware/camera2/CameraCharacteristics$Key;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/hardware/camera2/CameraCharacteristics$Key;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/hardware/camera2/CameraCharacteristics$Key<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    check-cast v0, Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/d;->Z:Landroid/hardware/camera2/CameraCharacteristics;

    invoke-virtual {v0, p1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    return-object p2
.end method

.method public final l(I)V
    .locals 2

    iget v0, p0, Lf/l/a/m/p/e;->b:I

    if-eq p1, v0, :cond_1

    iput p1, p0, Lf/l/a/m/p/e;->b:I

    iget-object p1, p0, Lf/l/a/m/p/e;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/m/p/b;

    iget v1, p0, Lf/l/a/m/p/e;->b:I

    invoke-interface {v0, p0, v1}, Lf/l/a/m/p/b;->a(Lf/l/a/m/p/a;I)V

    goto :goto_0

    :cond_0
    iget p1, p0, Lf/l/a/m/p/e;->b:I

    const v0, 0x7fffffff

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    check-cast p1, Lf/l/a/m/d;

    iget-object p1, p1, Lf/l/a/m/d;->i0:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->i(Lf/l/a/m/p/c;)V

    :cond_1
    return-void
.end method
