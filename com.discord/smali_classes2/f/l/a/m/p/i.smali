.class public Lf/l/a/m/p/i;
.super Lf/l/a/m/p/d;
.source "TimeoutAction.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# instance fields
.field public e:J

.field public f:J

.field public g:Lf/l/a/m/p/e;


# direct methods
.method public constructor <init>(JLf/l/a/m/p/e;)V
    .locals 0
    .param p3    # Lf/l/a/m/p/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Lf/l/a/m/p/d;-><init>()V

    iput-wide p1, p0, Lf/l/a/m/p/i;->f:J

    iput-object p3, p0, Lf/l/a/m/p/i;->g:Lf/l/a/m/p/e;

    return-void
.end method


# virtual methods
.method public b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 4
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2, p3}, Lf/l/a/m/p/d;->b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V

    invoke-virtual {p0}, Lf/l/a/m/p/e;->g()Z

    move-result p2

    if-nez p2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p2

    iget-wide v0, p0, Lf/l/a/m/p/i;->e:J

    iget-wide v2, p0, Lf/l/a/m/p/i;->f:J

    add-long/2addr v0, v2

    cmp-long v2, p2, v0

    if-lez v2, :cond_0

    iget-object p2, p0, Lf/l/a/m/p/i;->g:Lf/l/a/m/p/e;

    invoke-virtual {p2, p1}, Lf/l/a/m/p/e;->a(Lf/l/a/m/p/c;)V

    :cond_0
    return-void
.end method

.method public j(Lf/l/a/m/p/c;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lf/l/a/m/p/i;->e:J

    invoke-super {p0, p1}, Lf/l/a/m/p/d;->j(Lf/l/a/m/p/c;)V

    return-void
.end method

.method public m()Lf/l/a/m/p/e;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/m/p/i;->g:Lf/l/a/m/p/e;

    return-object v0
.end method
