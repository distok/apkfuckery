.class public abstract Lf/l/a/m/p/d;
.super Lf/l/a/m/p/e;
.source "ActionWrapper.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lf/l/a/m/p/e;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 1
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/l/a/m/p/d;->m()Lf/l/a/m/p/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lf/l/a/m/p/e;->b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V

    return-void
.end method

.method public c(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;)V
    .locals 1
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-boolean v0, p0, Lf/l/a/m/p/e;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/m/p/d;->j(Lf/l/a/m/p/c;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/l/a/m/p/e;->d:Z

    :cond_0
    invoke-virtual {p0}, Lf/l/a/m/p/d;->m()Lf/l/a/m/p/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lf/l/a/m/p/e;->c(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;)V

    return-void
.end method

.method public d(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
    .locals 1
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/CaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/l/a/m/p/d;->m()Lf/l/a/m/p/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lf/l/a/m/p/e;->d(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V

    return-void
.end method

.method public h(Lf/l/a/m/p/c;)V
    .locals 1
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/l/a/m/p/d;->m()Lf/l/a/m/p/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf/l/a/m/p/e;->h(Lf/l/a/m/p/c;)V

    return-void
.end method

.method public j(Lf/l/a/m/p/c;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    invoke-virtual {p0}, Lf/l/a/m/p/d;->m()Lf/l/a/m/p/e;

    move-result-object v0

    new-instance v1, Lf/l/a/m/p/d$a;

    invoke-direct {v1, p0}, Lf/l/a/m/p/d$a;-><init>(Lf/l/a/m/p/d;)V

    invoke-virtual {v0, v1}, Lf/l/a/m/p/e;->f(Lf/l/a/m/p/b;)V

    invoke-virtual {p0}, Lf/l/a/m/p/d;->m()Lf/l/a/m/p/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf/l/a/m/p/e;->j(Lf/l/a/m/p/c;)V

    return-void
.end method

.method public abstract m()Lf/l/a/m/p/e;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method
