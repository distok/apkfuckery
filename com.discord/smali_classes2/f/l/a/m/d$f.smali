.class public Lf/l/a/m/d$f;
.super Ljava/lang/Object;
.source "Camera2Engine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/d;->d0(F[F[Landroid/graphics/PointF;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:F

.field public final synthetic e:Z

.field public final synthetic f:F

.field public final synthetic g:[F

.field public final synthetic h:[Landroid/graphics/PointF;

.field public final synthetic i:Lf/l/a/m/d;


# direct methods
.method public constructor <init>(Lf/l/a/m/d;FZF[F[Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/d$f;->i:Lf/l/a/m/d;

    iput p2, p0, Lf/l/a/m/d$f;->d:F

    iput-boolean p3, p0, Lf/l/a/m/d$f;->e:Z

    iput p4, p0, Lf/l/a/m/d$f;->f:F

    iput-object p5, p0, Lf/l/a/m/d$f;->g:[F

    iput-object p6, p0, Lf/l/a/m/d$f;->h:[Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lf/l/a/m/d$f;->i:Lf/l/a/m/d;

    iget-object v1, v0, Lf/l/a/m/d;->b0:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget v2, p0, Lf/l/a/m/d$f;->d:F

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/d;->c1(Landroid/hardware/camera2/CaptureRequest$Builder;F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/d$f;->i:Lf/l/a/m/d;

    invoke-virtual {v0}, Lf/l/a/m/d;->g1()V

    iget-boolean v0, p0, Lf/l/a/m/d$f;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/d$f;->i:Lf/l/a/m/d;

    iget-object v0, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    iget v1, p0, Lf/l/a/m/d$f;->f:F

    iget-object v2, p0, Lf/l/a/m/d$f;->g:[F

    iget-object v3, p0, Lf/l/a/m/d$f;->h:[Landroid/graphics/PointF;

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v0, v1, v2, v3}, Lcom/otaliastudios/cameraview/CameraView$b;->c(F[F[Landroid/graphics/PointF;)V

    :cond_0
    return-void
.end method
