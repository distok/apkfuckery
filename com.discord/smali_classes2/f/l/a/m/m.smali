.class public Lf/l/a/m/m;
.super Ljava/lang/Object;
.source "CameraEngine.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/google/android/gms/tasks/Task<",
        "Lf/l/a/c;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/m/j;


# direct methods
.method public constructor <init>(Lf/l/a/m/j;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/m;->d:Lf/l/a/m/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/m/m;->d:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->l()Lf/l/a/l/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/l/a/m/j;->c(Lf/l/a/l/e;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/m/m;->d:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->Q()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "onStartEngine:"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "No camera available for facing"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lf/l/a/m/m;->d:Lf/l/a/m/j;

    invoke-virtual {v4}, Lf/l/a/m/j;->l()Lf/l/a/l/e;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Lcom/otaliastudios/cameraview/CameraException;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/otaliastudios/cameraview/CameraException;-><init>(I)V

    throw v0
.end method
