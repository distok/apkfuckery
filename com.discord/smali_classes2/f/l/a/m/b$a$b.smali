.class public Lf/l/a/m/b$a$b;
.super Ljava/lang/Object;
.source "Camera1Engine.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/m/b$a;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/m/b$a;


# direct methods
.method public constructor <init>(Lf/l/a/m/b$a;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 5

    iget-object p2, p0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object p2, p2, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object p2, p2, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v0, "focus end"

    invoke-virtual {p2, v0}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object p2, p0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object p2, p2, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object p2, p2, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    const-string v0, "focus reset"

    invoke-virtual {p2, v0}, Lf/l/a/m/v/a;->c(Ljava/lang/String;)V

    iget-object p2, p0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object v1, p2, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object v1, v1, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    iget-object v2, p2, Lf/l/a/m/b$a;->e:Lf/l/a/p/a;

    iget-object p2, p2, Lf/l/a/m/b$a;->f:Landroid/graphics/PointF;

    check-cast v1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {v1, v2, p1, p2}, Lcom/otaliastudios/cameraview/CameraView$b;->d(Lf/l/a/p/a;ZLandroid/graphics/PointF;)V

    iget-object p1, p0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object p1, p1, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    invoke-virtual {p1}, Lf/l/a/m/h;->X0()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lf/l/a/m/b$a$b;->a:Lf/l/a/m/b$a;

    iget-object p1, p1, Lf/l/a/m/b$a;->g:Lf/l/a/m/b;

    iget-object p2, p1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    iget-wide v2, p1, Lf/l/a/m/h;->Q:J

    new-instance p1, Lf/l/a/m/b$a$b$a;

    invoke-direct {p1, p0}, Lf/l/a/m/b$a$b$a;-><init>(Lf/l/a/m/b$a$b;)V

    new-instance v4, Lf/l/a/m/v/e;

    invoke-direct {v4, p2, v1, p1}, Lf/l/a/m/v/e;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    invoke-virtual {p2, v0, v2, v3, v4}, Lf/l/a/m/v/a;->f(Ljava/lang/String;JLjava/lang/Runnable;)V

    :cond_0
    return-void
.end method
