.class public Lf/l/a/m/l;
.super Ljava/lang/Object;
.source "CameraEngine.java"

# interfaces
.implements Lf/h/a/f/p/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/h/a/f/p/f<",
        "Lf/l/a/c;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/m/j;


# direct methods
.method public constructor <init>(Lf/l/a/m/j;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/m/l;->a:Lf/l/a/m/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    check-cast p1, Lf/l/a/c;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/l/a/m/l;->a:Lf/l/a/m/j;

    iget-object v0, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v1, v0, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "dispatchOnCameraOpened"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v1, v3, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, v0, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v1, v1, Lcom/otaliastudios/cameraview/CameraView;->l:Landroid/os/Handler;

    new-instance v2, Lf/l/a/f;

    invoke-direct {v2, v0, p1}, Lf/l/a/f;-><init>(Lcom/otaliastudios/cameraview/CameraView$b;Lf/l/a/c;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 p1, 0x0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Null options!"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
