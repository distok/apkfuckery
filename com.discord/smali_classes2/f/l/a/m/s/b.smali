.class public Lf/l/a/m/s/b;
.super Ljava/lang/Object;
.source "Camera2MeteringTransform.java"

# interfaces
.implements Lf/l/a/s/c;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf/l/a/s/c<",
        "Landroid/hardware/camera2/params/MeteringRectangle;",
        ">;"
    }
.end annotation


# static fields
.field public static final g:Lf/l/a/b;


# instance fields
.field public final a:Lf/l/a/m/t/a;

.field public final b:Lf/l/a/w/b;

.field public final c:Lf/l/a/w/b;

.field public final d:Z

.field public final e:Landroid/hardware/camera2/CameraCharacteristics;

.field public final f:Landroid/hardware/camera2/CaptureRequest$Builder;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/m/s/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/m/s/b;->g:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Lf/l/a/m/t/a;Lf/l/a/w/b;Lf/l/a/w/b;ZLandroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 0
    .param p1    # Lf/l/a/m/t/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/w/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/l/a/w/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Landroid/hardware/camera2/CameraCharacteristics;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/l/a/m/s/b;->a:Lf/l/a/m/t/a;

    iput-object p2, p0, Lf/l/a/m/s/b;->b:Lf/l/a/w/b;

    iput-object p3, p0, Lf/l/a/m/s/b;->c:Lf/l/a/w/b;

    iput-boolean p4, p0, Lf/l/a/m/s/b;->d:Z

    iput-object p5, p0, Lf/l/a/m/s/b;->e:Landroid/hardware/camera2/CameraCharacteristics;

    iput-object p6, p0, Lf/l/a/m/s/b;->f:Landroid/hardware/camera2/CaptureRequest$Builder;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/graphics/RectF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    new-instance p1, Landroid/hardware/camera2/params/MeteringRectangle;

    invoke-direct {p1, v0, p2}, Landroid/hardware/camera2/params/MeteringRectangle;-><init>(Landroid/graphics/Rect;I)V

    return-object p1
.end method

.method public b(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 12
    .param p1    # Landroid/graphics/PointF;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v1, p0, Lf/l/a/m/s/b;->b:Lf/l/a/w/b;

    iget-object v2, p0, Lf/l/a/m/s/b;->c:Lf/l/a/w/b;

    iget v3, v1, Lf/l/a/w/b;->d:I

    iget v4, v1, Lf/l/a/w/b;->e:I

    invoke-static {v2}, Lf/l/a/w/a;->g(Lf/l/a/w/b;)Lf/l/a/w/a;

    move-result-object v2

    iget v5, v1, Lf/l/a/w/b;->d:I

    iget v6, v1, Lf/l/a/w/b;->e:I

    invoke-static {v5, v6}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v5

    iget-boolean v6, p0, Lf/l/a/m/s/b;->d:Z

    const/high16 v7, 0x40000000    # 2.0f

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Lf/l/a/w/a;->i()F

    move-result v6

    invoke-virtual {v5}, Lf/l/a/w/a;->i()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v8

    if-lez v6, :cond_0

    invoke-virtual {v2}, Lf/l/a/w/a;->i()F

    move-result v2

    invoke-virtual {v5}, Lf/l/a/w/a;->i()F

    move-result v3

    div-float/2addr v2, v3

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v1, v1, Lf/l/a/w/b;->d:I

    int-to-float v1, v1

    sub-float v5, v2, v9

    mul-float v5, v5, v1

    div-float/2addr v5, v7

    add-float/2addr v5, v3

    iput v5, v0, Landroid/graphics/PointF;->x:F

    mul-float v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    goto :goto_0

    :cond_0
    invoke-virtual {v5}, Lf/l/a/w/a;->i()F

    move-result v4

    invoke-virtual {v2}, Lf/l/a/w/a;->i()F

    move-result v2

    div-float/2addr v4, v2

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v1, v1, Lf/l/a/w/b;->e:I

    int-to-float v1, v1

    sub-float v5, v4, v9

    mul-float v5, v5, v1

    div-float/2addr v5, v7

    add-float/2addr v5, v2

    iput v5, v0, Landroid/graphics/PointF;->y:F

    mul-float v1, v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    :cond_1
    :goto_0
    iget-object v1, p0, Lf/l/a/m/s/b;->c:Lf/l/a/w/b;

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v5, v1, Lf/l/a/w/b;->d:I

    int-to-float v5, v5

    int-to-float v3, v3

    div-float/2addr v5, v3

    mul-float v5, v5, v2

    iput v5, v0, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, v1, Lf/l/a/w/b;->e:I

    int-to-float v3, v3

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float v3, v3, v2

    iput v3, v0, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lf/l/a/m/s/b;->a:Lf/l/a/m/t/a;

    sget-object v3, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v4, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    sget-object v5, Lf/l/a/m/t/b;->d:Lf/l/a/m/t/b;

    invoke-virtual {v2, v3, v4, v5}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    move-result v2

    rem-int/lit16 v3, v2, 0xb4

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v8, v0, Landroid/graphics/PointF;->y:F

    if-nez v2, :cond_3

    iput v6, v0, Landroid/graphics/PointF;->x:F

    iput v8, v0, Landroid/graphics/PointF;->y:F

    goto :goto_2

    :cond_3
    const/16 v9, 0x5a

    if-ne v2, v9, :cond_4

    iput v8, v0, Landroid/graphics/PointF;->x:F

    iget v2, v1, Lf/l/a/w/b;->d:I

    int-to-float v2, v2

    sub-float/2addr v2, v6

    iput v2, v0, Landroid/graphics/PointF;->y:F

    goto :goto_2

    :cond_4
    const/16 v9, 0xb4

    if-ne v2, v9, :cond_5

    iget v2, v1, Lf/l/a/w/b;->d:I

    int-to-float v2, v2

    sub-float/2addr v2, v6

    iput v2, v0, Landroid/graphics/PointF;->x:F

    iget v2, v1, Lf/l/a/w/b;->e:I

    int-to-float v2, v2

    sub-float/2addr v2, v8

    iput v2, v0, Landroid/graphics/PointF;->y:F

    goto :goto_2

    :cond_5
    const/16 v9, 0x10e

    if-ne v2, v9, :cond_10

    iget v2, v1, Lf/l/a/w/b;->e:I

    int-to-float v2, v2

    sub-float/2addr v2, v8

    iput v2, v0, Landroid/graphics/PointF;->x:F

    iput v6, v0, Landroid/graphics/PointF;->y:F

    :goto_2
    if-eqz v3, :cond_6

    invoke-virtual {v1}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v1

    :cond_6
    iget-object v2, p0, Lf/l/a/m/s/b;->f:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    if-nez v2, :cond_7

    iget v3, v1, Lf/l/a/w/b;->d:I

    goto :goto_3

    :cond_7
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    :goto_3
    if-nez v2, :cond_8

    iget v2, v1, Lf/l/a/w/b;->e:I

    goto :goto_4

    :cond_8
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    :goto_4
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v8, v1, Lf/l/a/w/b;->d:I

    sub-int v8, v3, v8

    int-to-float v8, v8

    div-float/2addr v8, v7

    add-float/2addr v8, v6

    iput v8, v0, Landroid/graphics/PointF;->x:F

    iget v6, v0, Landroid/graphics/PointF;->y:F

    iget v1, v1, Lf/l/a/w/b;->e:I

    sub-int v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v1, v7

    add-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lf/l/a/m/s/b;->f:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v6, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {v1, v6}, Landroid/hardware/camera2/CaptureRequest$Builder;->get(Landroid/hardware/camera2/CaptureRequest$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    if-nez v1, :cond_9

    const/4 v8, 0x0

    goto :goto_5

    :cond_9
    iget v8, v1, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    :goto_5
    add-float/2addr v6, v8

    iput v6, v0, Landroid/graphics/PointF;->x:F

    iget v6, v0, Landroid/graphics/PointF;->y:F

    if-nez v1, :cond_a

    const/4 v1, 0x0

    goto :goto_6

    :cond_a
    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    :goto_6
    add-float/2addr v6, v1

    iput v6, v0, Landroid/graphics/PointF;->y:F

    iget-object v1, p0, Lf/l/a/m/s/b;->e:Landroid/hardware/camera2/CameraCharacteristics;

    sget-object v6, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_INFO_ACTIVE_ARRAY_SIZE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v1, v6}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    if-nez v1, :cond_b

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v5, v5, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    :cond_b
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v3, Lf/l/a/m/s/b;->g:Lf/l/a/b;

    const/4 v6, 0x4

    new-array v8, v6, [Ljava/lang/Object;

    const-string v9, "input:"

    aput-object v9, v8, v5

    aput-object p1, v8, v4

    const/4 v10, 0x2

    const-string v11, "output (before clipping):"

    aput-object v11, v8, v10

    const/4 v11, 0x3

    aput-object v0, v8, v11

    invoke-virtual {v3, v4, v8}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget v8, v0, Landroid/graphics/PointF;->x:F

    cmpg-float v8, v8, v7

    if-gez v8, :cond_c

    iput v7, v0, Landroid/graphics/PointF;->x:F

    :cond_c
    iget v8, v0, Landroid/graphics/PointF;->y:F

    cmpg-float v8, v8, v7

    if-gez v8, :cond_d

    iput v7, v0, Landroid/graphics/PointF;->y:F

    :cond_d
    iget v7, v0, Landroid/graphics/PointF;->x:F

    int-to-float v2, v2

    cmpl-float v7, v7, v2

    if-lez v7, :cond_e

    iput v2, v0, Landroid/graphics/PointF;->x:F

    :cond_e
    iget v2, v0, Landroid/graphics/PointF;->y:F

    int-to-float v1, v1

    cmpl-float v2, v2, v1

    if-lez v2, :cond_f

    iput v1, v0, Landroid/graphics/PointF;->y:F

    :cond_f
    new-array v1, v6, [Ljava/lang/Object;

    aput-object v9, v1, v5

    aput-object p1, v1, v4

    const-string p1, "output (after clipping):"

    aput-object p1, v1, v10

    aput-object v0, v1, v11

    invoke-virtual {v3, v4, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-object v0

    :cond_10
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected angle "

    invoke-static {v0, v2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
