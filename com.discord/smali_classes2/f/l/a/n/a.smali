.class public abstract Lf/l/a/n/a;
.super Ljava/lang/Object;
.source "BaseFilter.java"

# interfaces
.implements Lf/l/a/n/b;


# static fields
.field public static final c:Lf/l/a/b;


# instance fields
.field public a:Lf/l/b/d/d;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public b:Lf/l/b/b/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/n/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/n/a;->c:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    iput-object v0, p0, Lf/l/a/n/a;->b:Lf/l/b/b/b;

    return-void
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 8
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uniform mat4 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "uMVPMatrix"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ";\nuniform mat4 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "uTexMatrix"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ";\nattribute vec4 "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "aPosition"

    const-string v5, "aTextureCoord"

    const-string v6, ";\nvarying vec2 "

    invoke-static {v0, v4, v3, v5, v6}, Lf/e/c/a/a;->X(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "vTextureCoord"

    const-string v6, ";\nvoid main() {\n    gl_Position = "

    const-string v7, " * "

    invoke-static {v0, v3, v6, v1, v7}, Lf/e/c/a/a;->X(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, ";\n    "

    const-string v6, " = ("

    invoke-static {v0, v4, v1, v3, v6}, Lf/e/c/a/a;->X(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ").xy;\n}\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(J[F)V
    .locals 18
    .param p3    # [F
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    iget-object v2, v0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v2, :cond_0

    sget-object v1, Lf/l/a/n/a;->c:Lf/l/a/b;

    new-array v2, v5, [Ljava/lang/Object;

    const-string v5, "Filter.draw() called after destroying the filter. This can happen rarely because of threading."

    aput-object v5, v2, v4

    invoke-virtual {v1, v3, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_6

    :cond_0
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "<set-?>"

    invoke-static {v1, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v2, Lf/l/b/d/d;->e:[F

    iget-object v1, v0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    iget-object v2, v0, Lf/l/a/n/a;->b:Lf/l/b/b/b;

    iget-object v6, v2, Lf/l/b/b/b;->a:[F

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "drawable"

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "modelViewProjectionMatrix"

    invoke-static {v6, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v8, v2, Lf/l/b/b/a;

    if-eqz v8, :cond_d

    iget-object v8, v1, Lf/l/b/d/d;->j:Lf/l/b/d/b;

    iget v8, v8, Lf/l/b/d/b;->a:I

    invoke-static {v8, v5, v4, v6, v4}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    const-string v6, "glUniformMatrix4fv"

    invoke-static {v6}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    iget-object v8, v1, Lf/l/b/d/d;->f:Lf/l/b/d/b;

    if-eqz v8, :cond_1

    iget v8, v8, Lf/l/b/d/b;->a:I

    iget-object v9, v1, Lf/l/b/d/d;->e:[F

    invoke-static {v8, v5, v4, v9, v4}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {v6}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v6, v1, Lf/l/b/d/d;->i:Lf/l/b/d/b;

    iget v8, v6, Lf/l/b/d/b;->b:I

    invoke-static {v8}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    const-string v8, "glEnableVertexAttribArray"

    invoke-static {v8}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    iget v9, v6, Lf/l/b/d/b;->b:I

    const/4 v10, 0x2

    sget v6, Lf/l/b/c/a;->a:F

    const/16 v11, 0x1406

    const/4 v12, 0x0

    move-object v6, v2

    check-cast v6, Lf/l/b/b/a;

    iget v13, v6, Lf/l/b/b/a;->b:I

    mul-int/lit8 v13, v13, 0x4

    invoke-virtual {v2}, Lf/l/b/b/b;->b()Ljava/nio/FloatBuffer;

    move-result-object v14

    invoke-static/range {v9 .. v14}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    const-string v9, "glVertexAttribPointer"

    invoke-static {v9}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    iget-object v10, v1, Lf/l/b/d/d;->h:Lf/l/b/d/b;

    if-eqz v10, :cond_b

    iget-object v11, v1, Lf/l/b/d/d;->m:Lf/l/b/b/a;

    invoke-static {v2, v11}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/2addr v11, v5

    if-nez v11, :cond_2

    iget v11, v1, Lf/l/b/d/d;->l:I

    if-eqz v11, :cond_a

    :cond_2
    iput-object v6, v1, Lf/l/b/d/d;->m:Lf/l/b/b/a;

    iput v4, v1, Lf/l/b/d/d;->l:I

    iget-object v11, v1, Lf/l/b/d/d;->k:Landroid/graphics/RectF;

    const-string v12, "rect"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v12, -0x800001

    const v13, 0x7f7fffff    # Float.MAX_VALUE

    const v13, -0x800001

    const v14, 0x7f7fffff    # Float.MAX_VALUE

    const v15, 0x7f7fffff    # Float.MAX_VALUE

    const/16 v16, 0x0

    :goto_0
    invoke-virtual {v6}, Lf/l/b/b/b;->b()Ljava/nio/FloatBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/nio/FloatBuffer;->hasRemaining()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-virtual {v6}, Lf/l/b/b/b;->b()Ljava/nio/FloatBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/nio/FloatBuffer;->get()F

    move-result v4

    rem-int/lit8 v17, v16, 0x2

    if-nez v17, :cond_3

    invoke-static {v14, v4}, Ljava/lang/Math;->min(FF)F

    move-result v14

    invoke-static {v13, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    move v13, v4

    goto :goto_1

    :cond_3
    invoke-static {v12, v4}, Ljava/lang/Math;->max(FF)F

    move-result v12

    invoke-static {v15, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    move v15, v4

    :goto_1
    add-int/lit8 v16, v16, 0x1

    const/4 v4, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {v6}, Lf/l/b/b/b;->b()Ljava/nio/FloatBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v11, v14, v12, v13, v15}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {v2}, Lf/l/b/b/b;->b()Ljava/nio/FloatBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->limit()I

    move-result v4

    iget v11, v6, Lf/l/b/b/a;->b:I

    div-int/2addr v4, v11

    mul-int/lit8 v4, v4, 0x2

    iget-object v3, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    invoke-virtual {v3}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v3

    if-ge v3, v4, :cond_6

    iget-object v3, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    const-string v11, "$this$dispose"

    invoke-static {v3, v11}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v11, v3, Lf/l/b/f/a;

    if-eqz v11, :cond_5

    check-cast v3, Lf/l/b/f/a;

    invoke-interface {v3}, Lf/l/b/f/a;->dispose()V

    :cond_5
    invoke-static {v4}, Lf/h/a/f/f/n/g;->r(I)Ljava/nio/FloatBuffer;

    move-result-object v3

    iput-object v3, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    :cond_6
    iget-object v3, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    invoke-virtual {v3}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    iget-object v3, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    invoke-virtual {v3, v4}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_a

    rem-int/lit8 v11, v3, 0x2

    if-nez v11, :cond_7

    const/4 v11, 0x1

    goto :goto_3

    :cond_7
    const/4 v11, 0x0

    :goto_3
    invoke-virtual {v2}, Lf/l/b/b/b;->b()Ljava/nio/FloatBuffer;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v12

    iget-object v13, v1, Lf/l/b/d/d;->k:Landroid/graphics/RectF;

    if-eqz v11, :cond_8

    iget v14, v13, Landroid/graphics/RectF;->left:F

    goto :goto_4

    :cond_8
    iget v14, v13, Landroid/graphics/RectF;->bottom:F

    :goto_4
    if-eqz v11, :cond_9

    iget v11, v13, Landroid/graphics/RectF;->right:F

    goto :goto_5

    :cond_9
    iget v11, v13, Landroid/graphics/RectF;->top:F

    :goto_5
    div-int/lit8 v13, v3, 0x2

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sub-float/2addr v12, v14

    sub-float/2addr v11, v14

    div-float/2addr v12, v11

    const/high16 v11, 0x3f800000    # 1.0f

    mul-float v12, v12, v11

    const/4 v11, 0x0

    add-float/2addr v12, v11

    iget-object v11, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    invoke-virtual {v11, v12}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_a
    iget-object v2, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    iget v2, v10, Lf/l/b/d/b;->b:I

    invoke-static {v2}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    invoke-static {v8}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    iget v11, v10, Lf/l/b/d/b;->b:I

    const/4 v12, 0x2

    sget v2, Lf/l/b/c/a;->a:F

    const/16 v13, 0x1406

    const/4 v14, 0x0

    iget v2, v6, Lf/l/b/b/a;->b:I

    mul-int/lit8 v15, v2, 0x4

    iget-object v1, v1, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    move-object/from16 v16, v1

    invoke-static/range {v11 .. v16}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    invoke-static {v9}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    :cond_b
    iget-object v1, v0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    iget-object v2, v0, Lf/l/a/n/a;->b:Lf/l/b/b/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lf/l/b/b/b;->a()V

    iget-object v1, v0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    iget-object v2, v0, Lf/l/a/n/a;->b:Lf/l/b/b/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v1, Lf/l/b/d/d;->i:Lf/l/b/d/b;

    iget v2, v2, Lf/l/b/d/b;->b:I

    invoke-static {v2}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    iget-object v1, v1, Lf/l/b/d/d;->h:Lf/l/b/d/b;

    if-eqz v1, :cond_c

    iget v1, v1, Lf/l/b/d/b;->b:I

    invoke-static {v1}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :cond_c
    const-string v1, "onPostDraw end"

    invoke-static {v1}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    :goto_6
    return-void

    :cond_d
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "GlTextureProgram only supports 2D drawables."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public e()V
    .locals 5

    iget-object v0, p0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    iget-boolean v1, v0, Lf/l/b/d/a;->a:Z

    if-nez v1, :cond_2

    iget-boolean v1, v0, Lf/l/b/d/a;->c:Z

    if-eqz v1, :cond_0

    iget v1, v0, Lf/l/b/d/a;->b:I

    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    :cond_0
    iget-object v1, v0, Lf/l/b/d/a;->d:[Lf/l/b/d/c;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    iget v4, v4, Lf/l/b/d/c;->a:I

    invoke-static {v4}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/l/b/d/a;->a:Z

    :cond_2
    iget-object v0, v0, Lf/l/b/d/d;->g:Ljava/nio/FloatBuffer;

    const-string v1, "$this$dispose"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v1, v0, Lf/l/b/f/a;

    if-eqz v1, :cond_3

    check-cast v0, Lf/l/b/f/a;

    invoke-interface {v0}, Lf/l/b/f/a;->dispose()V

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    iput-object v0, p0, Lf/l/a/n/a;->b:Lf/l/b/b/b;

    return-void
.end method

.method public i(I)V
    .locals 7

    new-instance v6, Lf/l/b/d/d;

    const-string v2, "aPosition"

    const-string v3, "uMVPMatrix"

    const-string v4, "aTextureCoord"

    const-string v5, "uTexMatrix"

    move-object v0, v6

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lf/l/b/d/d;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lf/l/a/n/a;->a:Lf/l/b/d/d;

    new-instance p1, Lf/l/b/b/c;

    invoke-direct {p1}, Lf/l/b/b/c;-><init>()V

    iput-object p1, p0, Lf/l/a/n/a;->b:Lf/l/b/b/b;

    return-void
.end method

.method public j(II)V
    .locals 0

    return-void
.end method
