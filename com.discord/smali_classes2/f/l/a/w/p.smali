.class public Lf/l/a/w/p;
.super Ljava/lang/Object;
.source "SizeSelectors.java"

# interfaces
.implements Lf/l/a/w/c;


# instance fields
.field public a:[Lf/l/a/w/c;


# direct methods
.method public constructor <init>([Lf/l/a/w/c;Lf/l/a/w/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/l/a/w/p;->a:[Lf/l/a/w/c;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;)",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf/l/a/w/p;->a:[Lf/l/a/w/c;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v2, v0, v3

    invoke-interface {v2, p1}, Lf/l/a/w/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-nez v2, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    return-object v2
.end method
