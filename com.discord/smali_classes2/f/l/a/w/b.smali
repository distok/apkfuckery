.class public Lf/l/a/w/b;
.super Ljava/lang/Object;
.source "Size.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lf/l/a/w/b;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lf/l/a/w/b;->d:I

    iput p2, p0, Lf/l/a/w/b;->e:I

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Lf/l/a/w/b;

    iget v0, p0, Lf/l/a/w/b;->d:I

    iget v1, p0, Lf/l/a/w/b;->e:I

    mul-int v0, v0, v1

    iget v1, p1, Lf/l/a/w/b;->d:I

    iget p1, p1, Lf/l/a/w/b;->e:I

    mul-int v1, v1, p1

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    if-ne p0, p1, :cond_1

    return v1

    :cond_1
    instance-of v2, p1, Lf/l/a/w/b;

    if-eqz v2, :cond_2

    check-cast p1, Lf/l/a/w/b;

    iget v2, p0, Lf/l/a/w/b;->d:I

    iget v3, p1, Lf/l/a/w/b;->d:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lf/l/a/w/b;->e:I

    iget p1, p1, Lf/l/a/w/b;->e:I

    if-ne v2, p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public f()Lf/l/a/w/b;
    .locals 3

    new-instance v0, Lf/l/a/w/b;

    iget v1, p0, Lf/l/a/w/b;->e:I

    iget v2, p0, Lf/l/a/w/b;->d:I

    invoke-direct {v0, v1, v2}, Lf/l/a/w/b;-><init>(II)V

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lf/l/a/w/b;->e:I

    iget v1, p0, Lf/l/a/w/b;->d:I

    shl-int/lit8 v2, v1, 0x10

    ushr-int/lit8 v1, v1, 0x10

    or-int/2addr v1, v2

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lf/l/a/w/b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lf/l/a/w/b;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
