.class public Lf/l/a/w/o;
.super Ljava/lang/Object;
.source "SizeSelectors.java"

# interfaces
.implements Lf/l/a/w/c;


# instance fields
.field public a:Lf/l/a/w/n;


# direct methods
.method public constructor <init>(Lf/l/a/w/n;Lf/l/a/w/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/l/a/w/o;->a:Lf/l/a/w/n;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;)",
            "Ljava/util/List<",
            "Lf/l/a/w/b;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/l/a/w/b;

    iget-object v2, p0, Lf/l/a/w/o;->a:Lf/l/a/w/n;

    invoke-interface {v2, v1}, Lf/l/a/w/n;->a(Lf/l/a/w/b;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method
