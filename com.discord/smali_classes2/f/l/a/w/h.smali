.class public final Lf/l/a/w/h;
.super Ljava/lang/Object;
.source "SizeSelectors.java"

# interfaces
.implements Lf/l/a/w/n;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0

    iput p1, p0, Lf/l/a/w/h;->a:F

    iput p2, p0, Lf/l/a/w/h;->b:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/l/a/w/b;)Z
    .locals 3
    .param p1    # Lf/l/a/w/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p1, Lf/l/a/w/b;->d:I

    iget p1, p1, Lf/l/a/w/b;->e:I

    invoke-static {v0, p1}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object p1

    invoke-virtual {p1}, Lf/l/a/w/a;->i()F

    move-result p1

    iget v0, p0, Lf/l/a/w/h;->a:F

    iget v1, p0, Lf/l/a/w/h;->b:F

    sub-float v2, v0, v1

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_0

    add-float/2addr v0, v1

    cmpg-float p1, p1, v0

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
