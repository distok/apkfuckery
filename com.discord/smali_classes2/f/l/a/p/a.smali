.class public final enum Lf/l/a/p/a;
.super Ljava/lang/Enum;
.source "Gesture.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/p/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/p/a;

.field public static final enum e:Lf/l/a/p/a;

.field public static final enum f:Lf/l/a/p/a;

.field public static final enum g:Lf/l/a/p/a;

.field public static final enum h:Lf/l/a/p/a;

.field public static final synthetic i:[Lf/l/a/p/a;


# instance fields
.field private type:Lf/l/a/p/d;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    new-instance v0, Lf/l/a/p/a;

    sget-object v1, Lf/l/a/p/d;->e:Lf/l/a/p/d;

    const-string v2, "PINCH"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lf/l/a/p/a;-><init>(Ljava/lang/String;ILf/l/a/p/d;)V

    sput-object v0, Lf/l/a/p/a;->d:Lf/l/a/p/a;

    new-instance v2, Lf/l/a/p/a;

    sget-object v4, Lf/l/a/p/d;->d:Lf/l/a/p/d;

    const-string v5, "TAP"

    const/4 v6, 0x1

    invoke-direct {v2, v5, v6, v4}, Lf/l/a/p/a;-><init>(Ljava/lang/String;ILf/l/a/p/d;)V

    sput-object v2, Lf/l/a/p/a;->e:Lf/l/a/p/a;

    new-instance v5, Lf/l/a/p/a;

    const-string v7, "LONG_TAP"

    const/4 v8, 0x2

    invoke-direct {v5, v7, v8, v4}, Lf/l/a/p/a;-><init>(Ljava/lang/String;ILf/l/a/p/d;)V

    sput-object v5, Lf/l/a/p/a;->f:Lf/l/a/p/a;

    new-instance v4, Lf/l/a/p/a;

    const-string v7, "SCROLL_HORIZONTAL"

    const/4 v9, 0x3

    invoke-direct {v4, v7, v9, v1}, Lf/l/a/p/a;-><init>(Ljava/lang/String;ILf/l/a/p/d;)V

    sput-object v4, Lf/l/a/p/a;->g:Lf/l/a/p/a;

    new-instance v7, Lf/l/a/p/a;

    const-string v10, "SCROLL_VERTICAL"

    const/4 v11, 0x4

    invoke-direct {v7, v10, v11, v1}, Lf/l/a/p/a;-><init>(Ljava/lang/String;ILf/l/a/p/d;)V

    sput-object v7, Lf/l/a/p/a;->h:Lf/l/a/p/a;

    const/4 v1, 0x5

    new-array v1, v1, [Lf/l/a/p/a;

    aput-object v0, v1, v3

    aput-object v2, v1, v6

    aput-object v5, v1, v8

    aput-object v4, v1, v9

    aput-object v7, v1, v11

    sput-object v1, Lf/l/a/p/a;->i:[Lf/l/a/p/a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILf/l/a/p/d;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/l/a/p/d;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lf/l/a/p/a;->type:Lf/l/a/p/d;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/p/a;
    .locals 1

    const-class v0, Lf/l/a/p/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/p/a;

    return-object p0
.end method

.method public static values()[Lf/l/a/p/a;
    .locals 1

    sget-object v0, Lf/l/a/p/a;->i:[Lf/l/a/p/a;

    invoke-virtual {v0}, [Lf/l/a/p/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/p/a;

    return-object v0
.end method


# virtual methods
.method public f(Lf/l/a/p/b;)Z
    .locals 1
    .param p1    # Lf/l/a/p/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/p/b;->d:Lf/l/a/p/b;

    if-eq p1, v0, :cond_1

    invoke-virtual {p1}, Lf/l/a/p/b;->g()Lf/l/a/p/d;

    move-result-object p1

    iget-object v0, p0, Lf/l/a/p/a;->type:Lf/l/a/p/d;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
