.class public Lf/l/a/p/f;
.super Lf/l/a/p/c;
.source "ScrollGestureFinder.java"


# static fields
.field public static final g:Lf/l/a/b;


# instance fields
.field public d:Landroid/view/GestureDetector;

.field public e:Z

.field public f:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/p/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/p/f;->g:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Lf/l/a/p/c$a;)V
    .locals 3
    .param p1    # Lf/l/a/p/c$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lf/l/a/p/c;-><init>(Lf/l/a/p/c$a;I)V

    new-instance v0, Landroid/view/GestureDetector;

    check-cast p1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {p1}, Lcom/otaliastudios/cameraview/CameraView$b;->g()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lf/l/a/p/f$a;

    invoke-direct {v2, p0, p1}, Lf/l/a/p/f$a;-><init>(Lf/l/a/p/f;Lf/l/a/p/c$a;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lf/l/a/p/f;->d:Landroid/view/GestureDetector;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    return-void
.end method


# virtual methods
.method public b(FFF)F
    .locals 1

    iget v0, p0, Lf/l/a/p/f;->f:F

    sub-float/2addr p3, p2

    mul-float p3, p3, v0

    const/high16 p2, 0x40000000    # 2.0f

    mul-float p3, p3, p2

    add-float/2addr p3, p1

    return p3
.end method

.method public c(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lf/l/a/p/f;->e:Z

    :cond_0
    iget-object v0, p0, Lf/l/a/p/f;->d:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean p1, p0, Lf/l/a/p/f;->e:Z

    if-eqz p1, :cond_1

    sget-object p1, Lf/l/a/p/f;->g:Lf/l/a/b;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "Notifying a gesture of type"

    aput-object v2, v0, v1

    iget-object v1, p0, Lf/l/a/p/c;->b:Lf/l/a/p/a;

    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p1, v2, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :cond_1
    iget-boolean p1, p0, Lf/l/a/p/f;->e:Z

    return p1
.end method
