.class public final enum Lf/l/a/p/b;
.super Ljava/lang/Enum;
.source "GestureAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/p/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/p/b;

.field public static final enum e:Lf/l/a/p/b;

.field public static final enum f:Lf/l/a/p/b;

.field public static final enum g:Lf/l/a/p/b;

.field public static final enum h:Lf/l/a/p/b;

.field public static final enum i:Lf/l/a/p/b;

.field public static final enum j:Lf/l/a/p/b;

.field public static final synthetic k:[Lf/l/a/p/b;


# instance fields
.field private type:Lf/l/a/p/d;

.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Lf/l/a/p/b;

    sget-object v1, Lf/l/a/p/d;->d:Lf/l/a/p/d;

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v3, v1}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v0, Lf/l/a/p/b;->d:Lf/l/a/p/b;

    new-instance v2, Lf/l/a/p/b;

    const-string v4, "AUTO_FOCUS"

    const/4 v5, 0x1

    invoke-direct {v2, v4, v5, v5, v1}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v2, Lf/l/a/p/b;->e:Lf/l/a/p/b;

    new-instance v4, Lf/l/a/p/b;

    const-string v6, "TAKE_PICTURE"

    const/4 v7, 0x2

    invoke-direct {v4, v6, v7, v7, v1}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v4, Lf/l/a/p/b;->f:Lf/l/a/p/b;

    new-instance v1, Lf/l/a/p/b;

    sget-object v6, Lf/l/a/p/d;->e:Lf/l/a/p/d;

    const-string v8, "ZOOM"

    const/4 v9, 0x3

    invoke-direct {v1, v8, v9, v9, v6}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v1, Lf/l/a/p/b;->g:Lf/l/a/p/b;

    new-instance v8, Lf/l/a/p/b;

    const-string v10, "EXPOSURE_CORRECTION"

    const/4 v11, 0x4

    invoke-direct {v8, v10, v11, v11, v6}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v8, Lf/l/a/p/b;->h:Lf/l/a/p/b;

    new-instance v10, Lf/l/a/p/b;

    const-string v12, "FILTER_CONTROL_1"

    const/4 v13, 0x5

    invoke-direct {v10, v12, v13, v13, v6}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v10, Lf/l/a/p/b;->i:Lf/l/a/p/b;

    new-instance v12, Lf/l/a/p/b;

    const-string v14, "FILTER_CONTROL_2"

    const/4 v15, 0x6

    invoke-direct {v12, v14, v15, v15, v6}, Lf/l/a/p/b;-><init>(Ljava/lang/String;IILf/l/a/p/d;)V

    sput-object v12, Lf/l/a/p/b;->j:Lf/l/a/p/b;

    const/4 v6, 0x7

    new-array v6, v6, [Lf/l/a/p/b;

    aput-object v0, v6, v3

    aput-object v2, v6, v5

    aput-object v4, v6, v7

    aput-object v1, v6, v9

    aput-object v8, v6, v11

    aput-object v10, v6, v13

    aput-object v12, v6, v15

    sput-object v6, Lf/l/a/p/b;->k:[Lf/l/a/p/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILf/l/a/p/d;)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lf/l/a/p/d;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/l/a/p/b;->value:I

    iput-object p4, p0, Lf/l/a/p/b;->type:Lf/l/a/p/d;

    return-void
.end method

.method public static f(I)Lf/l/a/p/b;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-static {}, Lf/l/a/p/b;->values()[Lf/l/a/p/b;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    iget v3, v2, Lf/l/a/p/b;->value:I

    if-ne v3, p0, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/p/b;
    .locals 1

    const-class v0, Lf/l/a/p/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/p/b;

    return-object p0
.end method

.method public static values()[Lf/l/a/p/b;
    .locals 1

    sget-object v0, Lf/l/a/p/b;->k:[Lf/l/a/p/b;

    invoke-virtual {v0}, [Lf/l/a/p/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/p/b;

    return-object v0
.end method


# virtual methods
.method public g()Lf/l/a/p/d;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/p/b;->type:Lf/l/a/p/d;

    return-object v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lf/l/a/p/b;->value:I

    return v0
.end method
