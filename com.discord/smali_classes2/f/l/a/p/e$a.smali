.class public Lf/l/a/p/e$a;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "PinchGestureFinder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/p/e;-><init>(Lf/l/a/p/c$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/p/e;


# direct methods
.method public constructor <init>(Lf/l/a/p/e;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/p/e$a;->a:Lf/l/a/p/e;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 3

    iget-object v0, p0, Lf/l/a/p/e$a;->a:Lf/l/a/p/e;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/l/a/p/e;->e:Z

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result p1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr p1, v2

    const/high16 v2, 0x40000000    # 2.0f

    mul-float p1, p1, v2

    iput p1, v0, Lf/l/a/p/e;->f:F

    return v1
.end method
