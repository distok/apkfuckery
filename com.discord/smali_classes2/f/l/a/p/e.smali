.class public Lf/l/a/p/e;
.super Lf/l/a/p/c;
.source "PinchGestureFinder.java"


# instance fields
.field public d:Landroid/view/ScaleGestureDetector;

.field public e:Z

.field public f:F


# direct methods
.method public constructor <init>(Lf/l/a/p/c$a;)V
    .locals 2
    .param p1    # Lf/l/a/p/c$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lf/l/a/p/c;-><init>(Lf/l/a/p/c$a;I)V

    const/4 v0, 0x0

    iput v0, p0, Lf/l/a/p/e;->f:F

    sget-object v0, Lf/l/a/p/a;->d:Lf/l/a/p/a;

    iput-object v0, p0, Lf/l/a/p/c;->b:Lf/l/a/p/a;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    check-cast p1, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-virtual {p1}, Lcom/otaliastudios/cameraview/CameraView$b;->g()Landroid/content/Context;

    move-result-object p1

    new-instance v1, Lf/l/a/p/e$a;

    invoke-direct {v1, p0}, Lf/l/a/p/e$a;-><init>(Lf/l/a/p/e;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lf/l/a/p/e;->d:Landroid/view/ScaleGestureDetector;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->setQuickScaleEnabled(Z)V

    return-void
.end method


# virtual methods
.method public b(FFF)F
    .locals 1

    iget v0, p0, Lf/l/a/p/e;->f:F

    invoke-static {p3, p2, v0, p1}, Lf/e/c/a/a;->a(FFFF)F

    move-result p1

    return p1
.end method

.method public c(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lf/l/a/p/e;->e:Z

    :cond_0
    iget-object v0, p0, Lf/l/a/p/e;->d:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean v0, p0, Lf/l/a/p/e;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/l/a/p/c;->c:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lf/l/a/p/c;->c:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lf/l/a/p/c;->c:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lf/l/a/p/c;->c:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    iput p1, v0, Landroid/graphics/PointF;->y:F

    :cond_1
    return v1
.end method
