.class public Lf/l/a/u/a;
.super Lf/l/a/u/c;
.source "Full1PictureRecorder.java"


# instance fields
.field public final h:Landroid/hardware/Camera;

.field public final i:Lf/l/a/m/b;


# direct methods
.method public constructor <init>(Lf/l/a/k$a;Lf/l/a/m/b;Landroid/hardware/Camera;)V
    .locals 0
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/Camera;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lf/l/a/u/c;-><init>(Lf/l/a/k$a;Lf/l/a/u/d$a;)V

    iput-object p2, p0, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    iput-object p3, p0, Lf/l/a/u/a;->h:Landroid/hardware/Camera;

    invoke-virtual {p3}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object p1

    iget-object p2, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget p2, p2, Lf/l/a/k$a;->b:I

    invoke-virtual {p1, p2}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    invoke-virtual {p3, p1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method


# virtual methods
.method public b()V
    .locals 4

    sget-object v0, Lf/l/a/u/c;->g:Lf/l/a/b;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dispatching result. Thread:"

    aput-object v3, v1, v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-super {p0}, Lf/l/a/u/d;->b()V

    return-void
.end method

.method public c()V
    .locals 7

    sget-object v0, Lf/l/a/u/c;->g:Lf/l/a/b;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "take() called."

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lf/l/a/u/a;->h:Landroid/hardware/Camera;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v2, p0, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    invoke-virtual {v2}, Lf/l/a/m/b;->i1()Lf/l/a/o/a;

    move-result-object v2

    invoke-virtual {v2}, Lf/l/a/o/a;->d()V

    iget-object v2, p0, Lf/l/a/u/a;->h:Landroid/hardware/Camera;

    new-instance v5, Lf/l/a/u/a$a;

    invoke-direct {v5, p0}, Lf/l/a/u/a$a;-><init>(Lf/l/a/u/a;)V

    new-instance v6, Lf/l/a/u/a$b;

    invoke-direct {v6, p0}, Lf/l/a/u/a$b;-><init>(Lf/l/a/u/a;)V

    invoke-virtual {v2, v5, v3, v3, v6}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "take() returned."

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-void
.end method
