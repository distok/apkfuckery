.class public Lf/l/a/u/b;
.super Lf/l/a/u/c;
.source "Full2PictureRecorder.java"

# interfaces
.implements Landroid/media/ImageReader$OnImageAvailableListener;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# instance fields
.field public final h:Lf/l/a/m/p/c;

.field public final i:Lf/l/a/m/p/a;

.field public final j:Landroid/media/ImageReader;

.field public final k:Landroid/hardware/camera2/CaptureRequest$Builder;

.field public l:Landroid/hardware/camera2/DngCreator;


# direct methods
.method public constructor <init>(Lf/l/a/k$a;Lf/l/a/m/d;Landroid/hardware/camera2/CaptureRequest$Builder;Landroid/media/ImageReader;)V
    .locals 0
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/m/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/CaptureRequest$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroid/media/ImageReader;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lf/l/a/u/c;-><init>(Lf/l/a/k$a;Lf/l/a/u/d$a;)V

    iput-object p2, p0, Lf/l/a/u/b;->h:Lf/l/a/m/p/c;

    iput-object p3, p0, Lf/l/a/u/b;->k:Landroid/hardware/camera2/CaptureRequest$Builder;

    iput-object p4, p0, Lf/l/a/u/b;->j:Landroid/media/ImageReader;

    const-string p1, "FallbackCameraThread"

    invoke-static {p1}, Lf/l/a/q/e;->a(Ljava/lang/String;)Lf/l/a/q/e;

    move-result-object p1

    sput-object p1, Lf/l/a/q/e;->g:Lf/l/a/q/e;

    iget-object p1, p1, Lf/l/a/q/e;->c:Landroid/os/Handler;

    invoke-virtual {p4, p0, p1}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    new-instance p1, Lf/l/a/u/b$a;

    invoke-direct {p1, p0}, Lf/l/a/u/b$a;-><init>(Lf/l/a/u/b;)V

    iput-object p1, p0, Lf/l/a/u/b;->i:Lf/l/a/m/p/a;

    return-void
.end method


# virtual methods
.method public c()V
    .locals 2

    iget-object v0, p0, Lf/l/a/u/b;->i:Lf/l/a/m/p/a;

    iget-object v1, p0, Lf/l/a/u/b;->h:Lf/l/a/m/p/c;

    invoke-interface {v0, v1}, Lf/l/a/m/p/a;->e(Lf/l/a/m/p/c;)V

    return-void
.end method

.method public final d(Landroid/media/Image;)V
    .locals 3
    .param p1    # Landroid/media/Image;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object p1

    const/4 v0, 0x0

    aget-object p1, p1, v0

    invoke-virtual {p1}, Landroid/media/Image$Plane;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    iget-object p1, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iput-object v1, p1, Lf/l/a/k$a;->e:[B

    iput v0, p1, Lf/l/a/k$a;->b:I

    :try_start_0
    new-instance p1, Landroidx/exifinterface/media/ExifInterface;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object v2, v2, Lf/l/a/k$a;->e:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p1, v1}, Landroidx/exifinterface/media/ExifInterface;-><init>(Ljava/io/InputStream;)V

    const-string v1, "Orientation"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroidx/exifinterface/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result p1

    iget-object v1, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x10e

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xb4

    :goto_0
    iput v0, v1, Lf/l/a/k$a;->b:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final e(Landroid/media/Image;)V
    .locals 3
    .param p1    # Landroid/media/Image;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget-object v2, p0, Lf/l/a/u/b;->l:Landroid/hardware/camera2/DngCreator;

    invoke-virtual {v2, v1, p1}, Landroid/hardware/camera2/DngCreator;->writeImage(Ljava/io/OutputStream;Landroid/media/Image;)V

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    iget-object p1, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p1, Lf/l/a/k$a;->e:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Lf/l/a/u/b;->l:Landroid/hardware/camera2/DngCreator;

    invoke-virtual {v0}, Landroid/hardware/camera2/DngCreator;->close()V

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public onImageAvailable(Landroid/media/ImageReader;)V
    .locals 5

    sget-object v0, Lf/l/a/u/c;->g:Lf/l/a/b;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "onImageAvailable started."

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/media/ImageReader;->acquireNextImage()Landroid/media/Image;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object v3, v3, Lf/l/a/k$a;->f:Lf/l/a/l/j;

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    if-eqz v3, :cond_1

    if-ne v3, v1, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/u/b;->e(Landroid/media/Image;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown format: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object v3, v3, Lf/l/a/k$a;->f:Lf/l/a/l/j;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0, p1}, Lf/l/a/u/b;->d(Landroid/media/Image;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/media/Image;->close()V

    :cond_2
    new-array p1, v1, [Ljava/lang/Object;

    const-string v2, "onImageAvailable ended."

    aput-object v2, p1, v4

    invoke-virtual {v0, v1, p1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0}, Lf/l/a/u/d;->b()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object p1, v2

    :goto_1
    :try_start_2
    iput-object v2, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iput-object v0, p0, Lf/l/a/u/d;->f:Ljava/lang/Exception;

    invoke-virtual {p0}, Lf/l/a/u/d;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/media/Image;->close()V

    :cond_3
    return-void

    :catchall_1
    move-exception v0

    move-object v2, p1

    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/media/Image;->close()V

    :cond_4
    throw v0
.end method
