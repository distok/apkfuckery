.class public Lf/l/a/u/b$a;
.super Lf/l/a/m/p/e;
.source "Full2PictureRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/u/b;-><init>(Lf/l/a/k$a;Lf/l/a/m/d;Landroid/hardware/camera2/CaptureRequest$Builder;Landroid/media/ImageReader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic e:Lf/l/a/u/b;


# direct methods
.method public constructor <init>(Lf/l/a/u/b;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    invoke-direct {p0}, Lf/l/a/m/p/e;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 2
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p2, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object v0, p2, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object v0, v0, Lf/l/a/k$a;->f:Lf/l/a/l/j;

    sget-object v1, Lf/l/a/l/j;->e:Lf/l/a/l/j;

    if-ne v0, v1, :cond_4

    new-instance v0, Landroid/hardware/camera2/DngCreator;

    check-cast p1, Lf/l/a/m/d;

    iget-object p1, p1, Lf/l/a/m/d;->Z:Landroid/hardware/camera2/CameraCharacteristics;

    invoke-direct {v0, p1, p3}, Landroid/hardware/camera2/DngCreator;-><init>(Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureResult;)V

    iput-object v0, p2, Lf/l/a/u/b;->l:Landroid/hardware/camera2/DngCreator;

    iget-object p1, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object p2, p1, Lf/l/a/u/b;->l:Landroid/hardware/camera2/DngCreator;

    iget-object p1, p1, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget p1, p1, Lf/l/a/k$a;->b:I

    add-int/lit16 p3, p1, 0x168

    rem-int/lit16 p3, p3, 0x168

    if-eqz p3, :cond_3

    const/16 v0, 0x5a

    if-eq p3, v0, :cond_2

    const/16 v0, 0xb4

    if-eq p3, v0, :cond_1

    const/16 v0, 0x10e

    if-ne p3, v0, :cond_0

    const/16 p1, 0x8

    goto :goto_0

    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    const-string p3, "Invalid orientation: "

    invoke-static {p3, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_1
    const/4 p1, 0x3

    goto :goto_0

    :cond_2
    const/4 p1, 0x6

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    :goto_0
    invoke-virtual {p2, p1}, Landroid/hardware/camera2/DngCreator;->setOrientation(I)Landroid/hardware/camera2/DngCreator;

    iget-object p1, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object p2, p1, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object p2, p2, Lf/l/a/k$a;->a:Landroid/location/Location;

    if-eqz p2, :cond_4

    iget-object p1, p1, Lf/l/a/u/b;->l:Landroid/hardware/camera2/DngCreator;

    invoke-virtual {p1, p2}, Landroid/hardware/camera2/DngCreator;->setLocation(Landroid/location/Location;)Landroid/hardware/camera2/DngCreator;

    :cond_4
    return-void
.end method

.method public c(Lf/l/a/m/p/c;Landroid/hardware/camera2/CaptureRequest;)V
    .locals 3
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/hardware/camera2/CaptureRequest;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-boolean v0, p0, Lf/l/a/m/p/e;->d:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lf/l/a/u/b$a;->j(Lf/l/a/m/p/c;)V

    iput-boolean v1, p0, Lf/l/a/m/p/e;->d:Z

    :cond_0
    invoke-virtual {p2}, Landroid/hardware/camera2/CaptureRequest;->getTag()Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-ne p1, v0, :cond_1

    sget-object p1, Lf/l/a/u/c;->g:Lf/l/a/b;

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "onCaptureStarted:"

    aput-object v0, p2, v1

    const-string v0, "Dispatching picture shutter."

    const/4 v2, 0x1

    aput-object v0, p2, v2

    invoke-virtual {p1, v2, p2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    invoke-virtual {p1, v1}, Lf/l/a/u/d;->a(Z)V

    const p1, 0x7fffffff

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->l(I)V

    :cond_1
    return-void
.end method

.method public j(Lf/l/a/m/p/c;)V
    .locals 4
    .param p1    # Lf/l/a/m/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/m/p/e;->c:Lf/l/a/m/p/c;

    iget-object v0, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object v1, v0, Lf/l/a/u/b;->k:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v0, v0, Lf/l/a/u/b;->j:Landroid/media/ImageReader;

    invoke-virtual {v0}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    iget-object v0, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object v1, v0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object v2, v1, Lf/l/a/k$a;->f:Lf/l/a/l/j;

    sget-object v3, Lf/l/a/l/j;->d:Lf/l/a/l/j;

    if-ne v2, v3, :cond_0

    iget-object v0, v0, Lf/l/a/u/b;->k:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Landroid/hardware/camera2/CaptureRequest;->JPEG_ORIENTATION:Landroid/hardware/camera2/CaptureRequest$Key;

    iget v1, v1, Lf/l/a/k$a;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object v0, v0, Lf/l/a/u/b;->k:Landroid/hardware/camera2/CaptureRequest$Builder;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->setTag(Ljava/lang/Object;)V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iget-object v1, v1, Lf/l/a/u/b;->k:Landroid/hardware/camera2/CaptureRequest$Builder;

    check-cast p1, Lf/l/a/m/d;

    iget-object v2, p1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v2, v2, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v3, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Lf/l/a/m/j;->O()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p1, Lf/l/a/m/d;->a0:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v1

    iget-object p1, p1, Lf/l/a/m/d;->k0:Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;

    invoke-virtual {v2, v1, p1, v0}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object v1, p0, Lf/l/a/u/b$a;->e:Lf/l/a/u/b;

    iput-object v0, v1, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iput-object p1, v1, Lf/l/a/u/d;->f:Ljava/lang/Exception;

    invoke-virtual {v1}, Lf/l/a/u/d;->b()V

    const p1, 0x7fffffff

    invoke-virtual {p0, p1}, Lf/l/a/m/p/e;->l(I)V

    :cond_1
    :goto_0
    return-void
.end method
