.class public abstract Lf/l/a/u/d;
.super Ljava/lang/Object;
.source "PictureRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/u/d$a;
    }
.end annotation


# instance fields
.field public d:Lf/l/a/k$a;
    .annotation build Landroidx/annotation/VisibleForTesting;
        otherwise = 0x4
    .end annotation
.end field

.field public e:Lf/l/a/u/d$a;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public f:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Lf/l/a/k$a;Lf/l/a/u/d$a;)V
    .locals 0
    .param p1    # Lf/l/a/k$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/u/d$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iput-object p2, p0, Lf/l/a/u/d;->e:Lf/l/a/u/d$a;

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lf/l/a/u/d;->e:Lf/l/a/u/d$a;

    if-eqz v0, :cond_1

    check-cast v0, Lf/l/a/m/h;

    iget-object v0, v0, Lf/l/a/m/j;->f:Lf/l/a/m/j$g;

    xor-int/lit8 p1, p1, 0x1

    check-cast v0, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_1

    iget-object p1, v0, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-boolean v0, p1, Lcom/otaliastudios/cameraview/CameraView;->d:Z

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/otaliastudios/cameraview/CameraView;->s:Landroid/media/MediaActionSound;

    if-nez v0, :cond_0

    new-instance v0, Landroid/media/MediaActionSound;

    invoke-direct {v0}, Landroid/media/MediaActionSound;-><init>()V

    iput-object v0, p1, Lcom/otaliastudios/cameraview/CameraView;->s:Landroid/media/MediaActionSound;

    :cond_0
    iget-object p1, p1, Lcom/otaliastudios/cameraview/CameraView;->s:Landroid/media/MediaActionSound;

    invoke-virtual {p1, v1}, Landroid/media/MediaActionSound;->play(I)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lf/l/a/u/d;->e:Lf/l/a/u/d$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iget-object v2, p0, Lf/l/a/u/d;->f:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lf/l/a/u/d$a;->a(Lf/l/a/k$a;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/u/d;->e:Lf/l/a/u/d$a;

    iput-object v0, p0, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    :cond_0
    return-void
.end method

.method public abstract c()V
.end method
