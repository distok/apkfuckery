.class public Lf/l/a/u/a$b;
.super Ljava/lang/Object;
.source "Full1PictureRecorder.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/u/a;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf/l/a/u/a;


# direct methods
.method public constructor <init>(Lf/l/a/u/a;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 5

    sget-object v0, Lf/l/a/u/c;->g:Lf/l/a/b;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "take(): got picture callback."

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    :try_start_0
    new-instance v0, Landroidx/exifinterface/media/ExifInterface;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v2}, Landroidx/exifinterface/media/ExifInterface;-><init>(Ljava/io/InputStream;)V

    const-string v2, "Orientation"

    invoke-virtual {v0, v2, v1}, Landroidx/exifinterface/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x10e

    goto :goto_1

    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_1

    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_1

    :catch_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    iget-object v2, v2, Lf/l/a/u/d;->d:Lf/l/a/k$a;

    iput-object p1, v2, Lf/l/a/k$a;->e:[B

    iput v0, v2, Lf/l/a/k$a;->b:I

    sget-object p1, Lf/l/a/u/c;->g:Lf/l/a/b;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "take(): starting preview again. "

    aput-object v2, v0, v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1, v1, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    iget-object p1, p1, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    iget-object p1, p1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object p1, p1, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v0, Lf/l/a/m/v/b;->g:Lf/l/a/m/v/b;

    invoke-virtual {p1, v0}, Lf/l/a/m/v/b;->f(Lf/l/a/m/v/b;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    iget-object p1, p1, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object p1, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    iget-object p1, p1, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    sget-object v0, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    invoke-virtual {p1, v0}, Lf/l/a/m/h;->C(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    iget-object v0, v0, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    invoke-virtual {v0}, Lf/l/a/m/b;->i1()Lf/l/a/o/a;

    move-result-object v0

    iget-object v1, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    iget-object v1, v1, Lf/l/a/u/a;->i:Lf/l/a/m/b;

    iget v2, v1, Lf/l/a/m/h;->o:I

    iget-object v1, v1, Lf/l/a/m/h;->F:Lf/l/a/m/t/a;

    invoke-virtual {v0, v2, p1, v1}, Lf/l/a/o/a;->e(ILf/l/a/w/b;Lf/l/a/m/t/a;)V

    invoke-virtual {p2}, Landroid/hardware/Camera;->startPreview()V

    goto :goto_2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Preview stream size should never be null here."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_2
    iget-object p1, p0, Lf/l/a/u/a$b;->a:Lf/l/a/u/a;

    invoke-virtual {p1}, Lf/l/a/u/a;->b()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
