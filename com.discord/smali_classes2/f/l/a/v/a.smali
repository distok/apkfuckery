.class public abstract Lf/l/a/v/a;
.super Ljava/lang/Object;
.source "CameraPreview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/v/a$b;,
        Lf/l/a/v/a$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        "Output:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final i:Lf/l/a/b;


# instance fields
.field public a:Lf/l/a/v/a$c;

.field public b:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/v/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/v/a;->i:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2}, Lf/l/a/v/a;->l(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lf/l/a/v/a;->b:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public c(Lf/l/a/v/a$b;)V
    .locals 0
    .param p1    # Lf/l/a/v/a$b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    return-void
.end method

.method public final d(II)V
    .locals 5

    sget-object v0, Lf/l/a/v/a;->i:Lf/l/a/b;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dispatchOnSurfaceAvailable:"

    aput-object v3, v1, v2

    const-string v2, "w="

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    const-string v4, "h="

    aput-object v4, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x4

    aput-object v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iput p1, p0, Lf/l/a/v/a;->d:I

    iput p2, p0, Lf/l/a/v/a;->e:I

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/l/a/v/a;->c(Lf/l/a/v/a$b;)V

    :cond_0
    iget-object p1, p0, Lf/l/a/v/a;->a:Lf/l/a/v/a$c;

    if-eqz p1, :cond_1

    check-cast p1, Lf/l/a/m/j;

    invoke-virtual {p1}, Lf/l/a/m/j;->V()V

    :cond_1
    return-void
.end method

.method public final e()V
    .locals 6

    const/4 v0, 0x0

    iput v0, p0, Lf/l/a/v/a;->d:I

    iput v0, p0, Lf/l/a/v/a;->e:I

    iget-object v1, p0, Lf/l/a/v/a;->a:Lf/l/a/v/a$c;

    if-eqz v1, :cond_0

    check-cast v1, Lf/l/a/m/j;

    sget-object v2, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "onSurfaceDestroyed"

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v1, v0}, Lf/l/a/m/j;->N0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v1, v0}, Lf/l/a/m/j;->M0(Z)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final f(II)V
    .locals 8

    sget-object v0, Lf/l/a/v/a;->i:Lf/l/a/b;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "dispatchOnSurfaceSizeChanged:"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "w="

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-string v2, "h="

    const/4 v6, 0x3

    aput-object v2, v1, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    invoke-virtual {v0, v4, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget v0, p0, Lf/l/a/v/a;->d:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lf/l/a/v/a;->e:I

    if-eq p2, v0, :cond_2

    :cond_0
    iput p1, p0, Lf/l/a/v/a;->d:I

    iput p2, p0, Lf/l/a/v/a;->e:I

    if-lez p1, :cond_1

    if-lez p2, :cond_1

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/l/a/v/a;->c(Lf/l/a/v/a$b;)V

    :cond_1
    iget-object p1, p0, Lf/l/a/v/a;->a:Lf/l/a/v/a$c;

    if-eqz p1, :cond_2

    check-cast p1, Lf/l/a/m/h;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Lf/l/a/m/j;->h:Lf/l/a/b;

    new-array v0, v6, [Ljava/lang/Object;

    const-string v1, "onSurfaceChanged:"

    aput-object v1, v0, v3

    const-string v1, "Size is"

    aput-object v1, v0, v4

    sget-object v1, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {p1, v1}, Lf/l/a/m/h;->T0(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-virtual {p2, v4, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p2, p1, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    sget-object v0, Lf/l/a/m/v/b;->f:Lf/l/a/m/v/b;

    new-instance v1, Lf/l/a/m/i;

    invoke-direct {v1, p1}, Lf/l/a/m/i;-><init>(Lf/l/a/m/h;)V

    new-instance p1, Lf/l/a/m/v/c$c;

    invoke-direct {p1, p2, v0, v1}, Lf/l/a/m/v/c$c;-><init>(Lf/l/a/m/v/c;Lf/l/a/m/v/b;Ljava/lang/Runnable;)V

    new-instance v0, Lf/l/a/m/v/a$a;

    invoke-direct {v0, p2, p1}, Lf/l/a/m/v/a$a;-><init>(Lf/l/a/m/v/a;Ljava/lang/Runnable;)V

    const-string p1, "surface changed"

    invoke-virtual {p2, p1, v4, v0}, Lf/l/a/m/v/a;->e(Ljava/lang/String;ZLjava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    :cond_2
    return-void
.end method

.method public abstract g()Ljava/lang/Object;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TOutput;"
        }
    .end annotation
.end method

.method public abstract h()Ljava/lang/Class;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TOutput;>;"
        }
    .end annotation
.end method

.method public abstract i()Landroid/view/View;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public final j()Lf/l/a/w/b;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Lf/l/a/w/b;

    iget v1, p0, Lf/l/a/v/a;->d:I

    iget v2, p0, Lf/l/a/v/a;->e:I

    invoke-direct {v0, v1, v2}, Lf/l/a/w/b;-><init>(II)V

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget v0, p0, Lf/l/a/v/a;->d:I

    if-lez v0, :cond_0

    iget v0, p0, Lf/l/a/v/a;->e:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract l(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")TT;"
        }
    .end annotation
.end method

.method public m()V
    .locals 3
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lf/l/a/v/a;->i()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v1}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v2, Lf/l/a/v/a$a;

    invoke-direct {v2, p0, v1}, Lf/l/a/v/a$a;-><init>(Lf/l/a/v/a;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->c(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method

.method public n()V
    .locals 0

    return-void
.end method

.method public o()V
    .locals 0

    return-void
.end method

.method public p(I)V
    .locals 0

    iput p1, p0, Lf/l/a/v/a;->h:I

    return-void
.end method

.method public q(II)V
    .locals 5

    sget-object v0, Lf/l/a/v/a;->i:Lf/l/a/b;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "setStreamSize:"

    aput-object v3, v1, v2

    const-string v2, "desiredW="

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    const-string v4, "desiredH="

    aput-object v4, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x4

    aput-object v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iput p1, p0, Lf/l/a/v/a;->f:I

    iput p2, p0, Lf/l/a/v/a;->g:I

    if-lez p1, :cond_0

    if-lez p2, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lf/l/a/v/a;->c(Lf/l/a/v/a$b;)V

    :cond_0
    return-void
.end method

.method public r(Lf/l/a/v/a$c;)V
    .locals 6
    .param p1    # Lf/l/a/v/a$c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lf/l/a/v/a;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/l/a/v/a;->a:Lf/l/a/v/a$c;

    if-eqz v0, :cond_0

    check-cast v0, Lf/l/a/m/j;

    sget-object v1, Lf/l/a/m/j;->h:Lf/l/a/b;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "onSurfaceDestroyed"

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {v0, v4}, Lf/l/a/m/j;->N0(Z)Lcom/google/android/gms/tasks/Task;

    invoke-virtual {v0, v4}, Lf/l/a/m/j;->M0(Z)Lcom/google/android/gms/tasks/Task;

    :cond_0
    iput-object p1, p0, Lf/l/a/v/a;->a:Lf/l/a/v/a$c;

    invoke-virtual {p0}, Lf/l/a/v/a;->k()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/l/a/v/a;->a:Lf/l/a/v/a$c;

    if-eqz p1, :cond_1

    check-cast p1, Lf/l/a/m/j;

    invoke-virtual {p1}, Lf/l/a/m/j;->V()V

    :cond_1
    return-void
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
