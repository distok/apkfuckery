.class public Lf/l/a/v/c$a;
.super Ljava/lang/Object;
.source "GlCameraPreview.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/l/a/v/c;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/v/c;


# direct methods
.method public constructor <init>(Lf/l/a/v/c;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/v/c$a;->d:Lf/l/a/v/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lf/l/a/v/c$a;->d:Lf/l/a/v/c;

    iget-object v0, v0, Lf/l/a/v/c;->e:Lf/l/a/v/d$b;

    iget-object v1, v0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v1, v1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    iget-object v1, v0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v1, v1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->release()V

    iget-object v1, v0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iput-object v2, v1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    :cond_0
    iget-object v1, v0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v1, v1, Lf/l/a/v/d;->l:Lf/l/a/q/b;

    if-eqz v1, :cond_2

    iget v3, v1, Lf/l/a/q/b;->e:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    goto :goto_0

    :cond_1
    iget-object v3, v1, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    invoke-interface {v3}, Lf/l/a/n/b;->e()V

    iget v3, v1, Lf/l/a/q/b;->e:I

    invoke-static {v3}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    iput v4, v1, Lf/l/a/q/b;->e:I

    :goto_0
    iget-object v0, v0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iput-object v2, v0, Lf/l/a/v/d;->l:Lf/l/a/q/b;

    :cond_2
    return-void
.end method
