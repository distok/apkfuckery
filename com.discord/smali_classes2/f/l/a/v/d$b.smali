.class public Lf/l/a/v/d$b;
.super Ljava/lang/Object;
.source "GlCameraPreview.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/l/a/v/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/v/d;


# direct methods
.method public constructor <init>(Lf/l/a/v/d;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 9

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v0, p1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v1, p1, Lf/l/a/v/a;->f:I

    if-lez v1, :cond_a

    iget v1, p1, Lf/l/a/v/a;->g:I

    if-gtz v1, :cond_1

    goto/16 :goto_4

    :cond_1
    iget-object p1, p1, Lf/l/a/v/d;->l:Lf/l/a/q/b;

    iget-object p1, p1, Lf/l/a/q/b;->b:[F

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    iget-object v0, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v0, v0, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, p1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    iget-object v0, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget v0, v0, Lf/l/a/v/a;->h:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-eqz v0, :cond_2

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-static {p1, v8, v0, v0, v7}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    const/4 v2, 0x0

    iget-object v0, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget v0, v0, Lf/l/a/v/a;->h:I

    int-to-float v3, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    const/high16 v0, -0x41000000    # -0.5f

    invoke-static {p1, v8, v0, v0, v7}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    :cond_2
    iget-object v0, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-boolean v1, v0, Lf/l/a/v/a;->c:Z

    if-eqz v1, :cond_3

    iget v1, v0, Lf/l/a/v/d;->n:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v1, v2, v1

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    iget v0, v0, Lf/l/a/v/d;->o:F

    sub-float v0, v2, v0

    div-float/2addr v0, v3

    invoke-static {p1, v8, v1, v0, v7}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    iget-object v0, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget v1, v0, Lf/l/a/v/d;->n:F

    iget v0, v0, Lf/l/a/v/d;->o:F

    invoke-static {p1, v8, v1, v0, v2}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    :cond_3
    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v0, p1, Lf/l/a/v/d;->l:Lf/l/a/q/b;

    iget-object p1, p1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    iget-object p1, v0, Lf/l/a/q/b;->d:Lf/l/a/n/b;

    const/4 v3, -0x1

    if-eqz p1, :cond_5

    iget p1, v0, Lf/l/a/q/b;->e:I

    if-ne p1, v3, :cond_4

    goto :goto_0

    :cond_4
    iget-object p1, v0, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    invoke-interface {p1}, Lf/l/a/n/b;->e()V

    iget p1, v0, Lf/l/a/q/b;->e:I

    invoke-static {p1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    iput v3, v0, Lf/l/a/q/b;->e:I

    :goto_0
    iget-object p1, v0, Lf/l/a/q/b;->d:Lf/l/a/n/b;

    iput-object p1, v0, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    const/4 p1, 0x0

    iput-object p1, v0, Lf/l/a/q/b;->d:Lf/l/a/n/b;

    :cond_5
    iget p1, v0, Lf/l/a/q/b;->e:I

    if-ne p1, v3, :cond_9

    iget-object p1, v0, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    invoke-interface {p1}, Lf/l/a/n/b;->b()Ljava/lang/String;

    move-result-object p1

    iget-object v3, v0, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    invoke-interface {v3}, Lf/l/a/n/b;->g()Ljava/lang/String;

    move-result-object v3

    const-string v4, "vertexShaderSource"

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "fragmentShaderSource"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    new-array v5, v4, [Lf/l/b/d/c;

    new-instance v6, Lf/l/b/d/c;

    sget v7, Lf/l/b/c/a;->a:F

    const v7, 0x8b31

    invoke-direct {v6, v7, p1}, Lf/l/b/d/c;-><init>(ILjava/lang/String;)V

    aput-object v6, v5, v8

    new-instance p1, Lf/l/b/d/c;

    const v6, 0x8b30

    invoke-direct {p1, v6, v3}, Lf/l/b/d/c;-><init>(ILjava/lang/String;)V

    const/4 v3, 0x1

    aput-object p1, v5, v3

    const-string p1, "shaders"

    invoke-static {v5, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result p1

    const-string v6, "glCreateProgram"

    invoke-static {v6}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    if-eqz p1, :cond_8

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_6

    aget-object v7, v5, v6

    iget v7, v7, Lf/l/b/d/c;->a:I

    invoke-static {p1, v7}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v7, "glAttachShader"

    invoke-static {v7}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_6
    invoke-static {p1}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v4, v3, [I

    sget v5, Lf/l/b/c/a;->a:F

    const v5, 0x8b82

    invoke-static {p1, v5, v4, v8}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v4, v4, v8

    if-ne v4, v3, :cond_7

    iput p1, v0, Lf/l/a/q/b;->e:I

    iget-object v3, v0, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    invoke-interface {v3, p1}, Lf/l/a/n/b;->i(I)V

    const-string p1, "program creation"

    invoke-static {p1}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    const-string v0, "Could not link program: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Could not create program"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    :goto_2
    iget p1, v0, Lf/l/a/q/b;->e:I

    invoke-static {p1}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string p1, "glUseProgram(handle)"

    invoke-static {p1}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    iget-object p1, v0, Lf/l/a/q/b;->a:Lf/l/b/e/b;

    invoke-virtual {p1}, Lf/l/b/e/b;->a()V

    iget-object p1, v0, Lf/l/a/q/b;->c:Lf/l/a/n/b;

    iget-object v3, v0, Lf/l/a/q/b;->b:[F

    invoke-interface {p1, v1, v2, v3}, Lf/l/a/n/b;->d(J[F)V

    iget-object p1, v0, Lf/l/a/q/b;->a:Lf/l/b/e/b;

    invoke-virtual {p1}, Lf/l/b/e/b;->b()V

    invoke-static {v8}, Landroid/opengl/GLES20;->glUseProgram(I)V

    const-string p1, "glUseProgram(0)"

    invoke-static {p1}, Lf/l/b/a/a;->a(Ljava/lang/String;)V

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object p1, p1, Lf/l/a/v/d;->m:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/v/e;

    iget-object v1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object v2, v1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    iget v3, v1, Lf/l/a/v/a;->h:I

    iget v4, v1, Lf/l/a/v/d;->n:F

    iget v1, v1, Lf/l/a/v/d;->o:F

    invoke-interface {v0, v2, v3, v4, v1}, Lf/l/a/v/e;->a(Landroid/graphics/SurfaceTexture;IFF)V

    goto :goto_3

    :cond_a
    :goto_4
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0, p2, p3}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object p1, p1, Lf/l/a/v/d;->q:Lf/l/a/n/b;

    invoke-interface {p1, p2, p3}, Lf/l/a/n/b;->j(II)V

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-boolean v0, p1, Lf/l/a/v/d;->j:Z

    if-nez v0, :cond_0

    invoke-virtual {p1, p2, p3}, Lf/l/a/v/a;->d(II)V

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lf/l/a/v/d;->j:Z

    goto :goto_0

    :cond_0
    iget v0, p1, Lf/l/a/v/a;->d:I

    if-ne p2, v0, :cond_1

    iget v0, p1, Lf/l/a/v/a;->e:I

    if-eq p3, v0, :cond_2

    :cond_1
    invoke-virtual {p1, p2, p3}, Lf/l/a/v/a;->f(II)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object p2, p1, Lf/l/a/v/d;->q:Lf/l/a/n/b;

    if-nez p2, :cond_0

    new-instance p2, Lf/l/a/n/c;

    invoke-direct {p2}, Lf/l/a/n/c;-><init>()V

    iput-object p2, p1, Lf/l/a/v/d;->q:Lf/l/a/n/b;

    :cond_0
    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    new-instance p2, Lf/l/a/q/b;

    invoke-direct {p2}, Lf/l/a/q/b;-><init>()V

    iput-object p2, p1, Lf/l/a/v/d;->l:Lf/l/a/q/b;

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object p2, p1, Lf/l/a/v/d;->l:Lf/l/a/q/b;

    iget-object v0, p1, Lf/l/a/v/d;->q:Lf/l/a/n/b;

    iput-object v0, p2, Lf/l/a/q/b;->d:Lf/l/a/n/b;

    iget-object p2, p2, Lf/l/a/q/b;->a:Lf/l/b/e/b;

    iget p2, p2, Lf/l/b/e/b;->a:I

    new-instance v0, Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, p2}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object p1, p1, Lf/l/a/v/a;->b:Landroid/view/View;

    check-cast p1, Landroid/opengl/GLSurfaceView;

    new-instance v0, Lf/l/a/v/d$b$a;

    invoke-direct {v0, p0, p2}, Lf/l/a/v/d$b$a;-><init>(Lf/l/a/v/d$b;I)V

    invoke-virtual {p1, v0}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    iget-object p1, p0, Lf/l/a/v/d$b;->d:Lf/l/a/v/d;

    iget-object p1, p1, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    new-instance p2, Lf/l/a/v/d$b$b;

    invoke-direct {p2, p0}, Lf/l/a/v/d$b$b;-><init>(Lf/l/a/v/d$b;)V

    invoke-virtual {p1, p2}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    return-void
.end method
