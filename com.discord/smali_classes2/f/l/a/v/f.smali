.class public Lf/l/a/v/f;
.super Ljava/lang/Object;
.source "SurfaceCameraPreview.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field public final synthetic d:Lf/l/a/v/g;


# direct methods
.method public constructor <init>(Lf/l/a/v/g;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/v/f;->d:Lf/l/a/v/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    sget-object p1, Lf/l/a/v/g;->l:Lf/l/a/b;

    const/16 p2, 0x8

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v1, "callback:"

    aput-object v1, p2, v0

    const/4 v0, 0x1

    const-string v1, "surfaceChanged"

    aput-object v1, p2, v0

    const/4 v1, 0x2

    const-string v2, "w:"

    aput-object v2, p2, v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, p2, v2

    const/4 v1, 0x4

    const-string v2, "h:"

    aput-object v2, p2, v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, p2, v2

    const/4 v1, 0x6

    const-string v2, "dispatched:"

    aput-object v2, p2, v1

    iget-object v1, p0, Lf/l/a/v/f;->d:Lf/l/a/v/g;

    iget-boolean v1, v1, Lf/l/a/v/g;->j:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, p2, v2

    invoke-virtual {p1, v0, p2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/v/f;->d:Lf/l/a/v/g;

    iget-boolean p2, p1, Lf/l/a/v/g;->j:Z

    if-nez p2, :cond_0

    invoke-virtual {p1, p3, p4}, Lf/l/a/v/a;->d(II)V

    iget-object p1, p0, Lf/l/a/v/f;->d:Lf/l/a/v/g;

    iput-boolean v0, p1, Lf/l/a/v/g;->j:Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p3, p4}, Lf/l/a/v/a;->f(II)V

    :goto_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4

    sget-object p1, Lf/l/a/v/g;->l:Lf/l/a/b;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "callback:"

    aput-object v2, v0, v1

    const/4 v2, 0x1

    const-string v3, "surfaceDestroyed"

    aput-object v3, v0, v2

    invoke-virtual {p1, v2, v0}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lf/l/a/v/f;->d:Lf/l/a/v/g;

    invoke-virtual {p1}, Lf/l/a/v/a;->e()V

    iget-object p1, p0, Lf/l/a/v/f;->d:Lf/l/a/v/g;

    iput-boolean v1, p1, Lf/l/a/v/g;->j:Z

    return-void
.end method
