.class public Lf/l/a/v/d;
.super Lf/l/a/v/a;
.source "GlCameraPreview.java"

# interfaces
.implements Lf/l/a/v/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/v/d$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/l/a/v/a<",
        "Landroid/opengl/GLSurfaceView;",
        "Landroid/graphics/SurfaceTexture;",
        ">;",
        "Lf/l/a/v/b;",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public j:Z

.field public k:Landroid/graphics/SurfaceTexture;

.field public l:Lf/l/a/q/b;

.field public final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/l/a/v/e;",
            ">;"
        }
    .end annotation
.end field

.field public n:F
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public o:F
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public p:Landroid/view/View;

.field public q:Lf/l/a/n/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lf/l/a/v/a;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lf/l/a/v/d;->m:Ljava/util/Set;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lf/l/a/v/d;->n:F

    iput p1, p0, Lf/l/a/v/d;->o:F

    return-void
.end method


# virtual methods
.method public a(Lf/l/a/n/b;)V
    .locals 2
    .param p1    # Lf/l/a/n/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Lf/l/a/v/d;->q:Lf/l/a/n/b;

    invoke-virtual {p0}, Lf/l/a/v/a;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lf/l/a/v/a;->d:I

    iget v1, p0, Lf/l/a/v/a;->e:I

    invoke-interface {p1, v0, v1}, Lf/l/a/n/b;->j(II)V

    :cond_0
    iget-object v0, p0, Lf/l/a/v/a;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    new-instance v1, Lf/l/a/v/d$a;

    invoke-direct {v1, p0, p1}, Lf/l/a/v/d$a;-><init>(Lf/l/a/v/d;Lf/l/a/n/b;)V

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()Lf/l/a/n/b;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/v/d;->q:Lf/l/a/n/b;

    return-object v0
.end method

.method public c(Lf/l/a/v/a$b;)V
    .locals 4
    .param p1    # Lf/l/a/v/a$b;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget p1, p0, Lf/l/a/v/a;->f:I

    if-lez p1, :cond_3

    iget p1, p0, Lf/l/a/v/a;->g:I

    if-lez p1, :cond_3

    iget p1, p0, Lf/l/a/v/a;->d:I

    if-lez p1, :cond_3

    iget v0, p0, Lf/l/a/v/a;->e:I

    if-lez v0, :cond_3

    invoke-static {p1, v0}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object p1

    iget v0, p0, Lf/l/a/v/a;->f:I

    iget v1, p0, Lf/l/a/v/a;->g:I

    invoke-static {v0, v1}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v0

    invoke-virtual {p1}, Lf/l/a/w/a;->i()F

    move-result v1

    invoke-virtual {v0}, Lf/l/a/w/a;->i()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p1}, Lf/l/a/w/a;->i()F

    move-result p1

    invoke-virtual {v0}, Lf/l/a/w/a;->i()F

    move-result v0

    div-float/2addr p1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lf/l/a/w/a;->i()F

    move-result v0

    invoke-virtual {p1}, Lf/l/a/w/a;->i()F

    move-result p1

    div-float/2addr v0, p1

    const/high16 p1, 0x3f800000    # 1.0f

    :goto_0
    const v1, 0x3f828f5c    # 1.02f

    cmpl-float v2, v0, v1

    if-gtz v2, :cond_2

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Lf/l/a/v/a;->c:Z

    div-float v0, v3, v0

    iput v0, p0, Lf/l/a/v/d;->n:F

    div-float/2addr v3, p1

    iput v3, p0, Lf/l/a/v/d;->o:F

    iget-object p1, p0, Lf/l/a/v/a;->b:Landroid/view/View;

    check-cast p1, Landroid/opengl/GLSurfaceView;

    invoke-virtual {p1}, Landroid/opengl/GLSurfaceView;->requestRender()V

    :cond_3
    return-void
.end method

.method public g()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/v/d;->k:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public h()Ljava/lang/Class;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "Landroid/graphics/SurfaceTexture;",
            ">;"
        }
    .end annotation

    const-class v0, Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method public i()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lf/l/a/v/d;->p:Landroid/view/View;

    return-object v0
.end method

.method public l(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/otaliastudios/cameraview/R$b;->cameraview_gl_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    sget v0, Lcom/otaliastudios/cameraview/R$a;->gl_surface_view:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/opengl/GLSurfaceView;

    new-instance v2, Lf/l/a/v/d$b;

    invoke-direct {v2, p0}, Lf/l/a/v/d$b;-><init>(Lf/l/a/v/d;)V

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    invoke-virtual {v0, v2}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    new-instance v4, Lf/l/a/v/c;

    invoke-direct {v4, p0, v0, v2}, Lf/l/a/v/c;-><init>(Lf/l/a/v/d;Landroid/opengl/GLSurfaceView;Lf/l/a/v/d$b;)V

    invoke-interface {v3, v4}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p2, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iput-object p1, p0, Lf/l/a/v/d;->p:Landroid/view/View;

    return-object v0
.end method

.method public m()V
    .locals 1

    invoke-super {p0}, Lf/l/a/v/a;->m()V

    iget-object v0, p0, Lf/l/a/v/d;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, Lf/l/a/v/a;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    return-void
.end method

.method public o()V
    .locals 1

    iget-object v0, p0, Lf/l/a/v/a;->b:Landroid/view/View;

    check-cast v0, Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onResume()V

    return-void
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
