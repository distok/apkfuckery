.class public Lf/l/a/v/c;
.super Ljava/lang/Object;
.source "GlCameraPreview.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field public final synthetic d:Landroid/opengl/GLSurfaceView;

.field public final synthetic e:Lf/l/a/v/d$b;

.field public final synthetic f:Lf/l/a/v/d;


# direct methods
.method public constructor <init>(Lf/l/a/v/d;Landroid/opengl/GLSurfaceView;Lf/l/a/v/d$b;)V
    .locals 0

    iput-object p1, p0, Lf/l/a/v/c;->f:Lf/l/a/v/d;

    iput-object p2, p0, Lf/l/a/v/c;->d:Landroid/opengl/GLSurfaceView;

    iput-object p3, p0, Lf/l/a/v/c;->e:Lf/l/a/v/d$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object p1, p0, Lf/l/a/v/c;->f:Lf/l/a/v/d;

    invoke-virtual {p1}, Lf/l/a/v/a;->e()V

    iget-object p1, p0, Lf/l/a/v/c;->d:Landroid/opengl/GLSurfaceView;

    new-instance v0, Lf/l/a/v/c$a;

    invoke-direct {v0, p0}, Lf/l/a/v/c$a;-><init>(Lf/l/a/v/c;)V

    invoke-virtual {p1, v0}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    iget-object p1, p0, Lf/l/a/v/c;->f:Lf/l/a/v/d;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lf/l/a/v/d;->j:Z

    return-void
.end method
