.class public final enum Lf/l/a/l/f;
.super Ljava/lang/Enum;
.source "Flash.java"

# interfaces
.implements Lf/l/a/l/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/l/f;",
        ">;",
        "Lf/l/a/l/c;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/l/f;

.field public static final enum e:Lf/l/a/l/f;

.field public static final enum f:Lf/l/a/l/f;

.field public static final enum g:Lf/l/a/l/f;

.field public static final synthetic h:[Lf/l/a/l/f;


# instance fields
.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lf/l/a/l/f;

    const-string v1, "OFF"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/l/a/l/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    new-instance v1, Lf/l/a/l/f;

    const-string v3, "ON"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/l/a/l/f;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/l/a/l/f;->e:Lf/l/a/l/f;

    new-instance v3, Lf/l/a/l/f;

    const-string v5, "AUTO"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lf/l/a/l/f;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lf/l/a/l/f;->f:Lf/l/a/l/f;

    new-instance v5, Lf/l/a/l/f;

    const-string v7, "TORCH"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lf/l/a/l/f;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lf/l/a/l/f;->g:Lf/l/a/l/f;

    const/4 v7, 0x4

    new-array v7, v7, [Lf/l/a/l/f;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lf/l/a/l/f;->h:[Lf/l/a/l/f;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/l/a/l/f;->value:I

    return-void
.end method

.method public static f(I)Lf/l/a/l/f;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Lf/l/a/l/f;->values()[Lf/l/a/l/f;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    iget v3, v2, Lf/l/a/l/f;->value:I

    if-ne v3, p0, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object p0, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/l/f;
    .locals 1

    const-class v0, Lf/l/a/l/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/l/f;

    return-object p0
.end method

.method public static values()[Lf/l/a/l/f;
    .locals 1

    sget-object v0, Lf/l/a/l/f;->h:[Lf/l/a/l/f;

    invoke-virtual {v0}, [Lf/l/a/l/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/l/f;

    return-object v0
.end method


# virtual methods
.method public g()I
    .locals 1

    iget v0, p0, Lf/l/a/l/f;->value:I

    return v0
.end method
