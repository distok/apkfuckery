.class public final enum Lf/l/a/l/k;
.super Ljava/lang/Enum;
.source "Preview.java"

# interfaces
.implements Lf/l/a/l/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/l/k;",
        ">;",
        "Lf/l/a/l/c;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/l/k;

.field public static final enum e:Lf/l/a/l/k;

.field public static final enum f:Lf/l/a/l/k;

.field public static final synthetic g:[Lf/l/a/l/k;


# instance fields
.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lf/l/a/l/k;

    const-string v1, "SURFACE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/l/a/l/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/l/a/l/k;->d:Lf/l/a/l/k;

    new-instance v1, Lf/l/a/l/k;

    const-string v3, "TEXTURE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/l/a/l/k;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/l/a/l/k;->e:Lf/l/a/l/k;

    new-instance v3, Lf/l/a/l/k;

    const-string v5, "GL_SURFACE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lf/l/a/l/k;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lf/l/a/l/k;->f:Lf/l/a/l/k;

    const/4 v5, 0x3

    new-array v5, v5, [Lf/l/a/l/k;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lf/l/a/l/k;->g:[Lf/l/a/l/k;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/l/a/l/k;->value:I

    return-void
.end method

.method public static f(I)Lf/l/a/l/k;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Lf/l/a/l/k;->values()[Lf/l/a/l/k;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    iget v3, v2, Lf/l/a/l/k;->value:I

    if-ne v3, p0, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object p0, Lf/l/a/l/k;->f:Lf/l/a/l/k;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/l/k;
    .locals 1

    const-class v0, Lf/l/a/l/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/l/k;

    return-object p0
.end method

.method public static values()[Lf/l/a/l/k;
    .locals 1

    sget-object v0, Lf/l/a/l/k;->g:[Lf/l/a/l/k;

    invoke-virtual {v0}, [Lf/l/a/l/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/l/k;

    return-object v0
.end method


# virtual methods
.method public g()I
    .locals 1

    iget v0, p0, Lf/l/a/l/k;->value:I

    return v0
.end method
