.class public final enum Lf/l/a/l/e;
.super Ljava/lang/Enum;
.source "Facing.java"

# interfaces
.implements Lf/l/a/l/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/l/a/l/e;",
        ">;",
        "Lf/l/a/l/c;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/l/a/l/e;

.field public static final enum e:Lf/l/a/l/e;

.field public static final synthetic f:[Lf/l/a/l/e;


# instance fields
.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lf/l/a/l/e;

    const-string v1, "BACK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v2}, Lf/l/a/l/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lf/l/a/l/e;->d:Lf/l/a/l/e;

    new-instance v1, Lf/l/a/l/e;

    const-string v3, "FRONT"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lf/l/a/l/e;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lf/l/a/l/e;->e:Lf/l/a/l/e;

    const/4 v3, 0x2

    new-array v3, v3, [Lf/l/a/l/e;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lf/l/a/l/e;->f:[Lf/l/a/l/e;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lf/l/a/l/e;->value:I

    return-void
.end method

.method public static f(I)Lf/l/a/l/e;
    .locals 4
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-static {}, Lf/l/a/l/e;->values()[Lf/l/a/l/e;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    iget v3, v2, Lf/l/a/l/e;->value:I

    if-ne v3, p0, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lf/l/a/l/e;
    .locals 1

    const-class v0, Lf/l/a/l/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/l/a/l/e;

    return-object p0
.end method

.method public static values()[Lf/l/a/l/e;
    .locals 1

    sget-object v0, Lf/l/a/l/e;->f:[Lf/l/a/l/e;

    invoke-virtual {v0}, [Lf/l/a/l/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/l/a/l/e;

    return-object v0
.end method


# virtual methods
.method public g()I
    .locals 1

    iget v0, p0, Lf/l/a/l/e;->value:I

    return v0
.end method
