.class public Lf/l/a/o/a;
.super Lf/l/a/o/c;
.source "ByteBufferFrameManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/l/a/o/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/l/a/o/c<",
        "[B>;"
    }
.end annotation


# instance fields
.field public i:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "[B>;"
        }
    .end annotation
.end field

.field public j:Lf/l/a/o/a$a;

.field public final k:I


# direct methods
.method public constructor <init>(ILf/l/a/o/a$a;)V
    .locals 1
    .param p2    # Lf/l/a/o/a$a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const-class v0, [B

    invoke-direct {p0, p1, v0}, Lf/l/a/o/c;-><init>(ILjava/lang/Class;)V

    iput-object p2, p0, Lf/l/a/o/a;->j:Lf/l/a/o/a$a;

    const/4 p1, 0x0

    iput p1, p0, Lf/l/a/o/a;->k:I

    return-void
.end method


# virtual methods
.method public c(Ljava/lang/Object;Z)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, [B

    if-eqz p2, :cond_1

    array-length p2, p1

    iget v0, p0, Lf/l/a/o/c;->b:I

    if-ne p2, v0, :cond_1

    iget p2, p0, Lf/l/a/o/a;->k:I

    if-nez p2, :cond_0

    iget-object p2, p0, Lf/l/a/o/a;->j:Lf/l/a/o/a$a;

    check-cast p2, Lf/l/a/m/b;

    invoke-virtual {p2, p1}, Lf/l/a/m/b;->j1([B)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lf/l/a/o/a;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public d()V
    .locals 2

    invoke-super {p0}, Lf/l/a/o/c;->d()V

    iget v0, p0, Lf/l/a/o/a;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lf/l/a/o/a;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    :cond_0
    return-void
.end method

.method public e(ILf/l/a/w/b;Lf/l/a/m/t/a;)V
    .locals 1
    .param p2    # Lf/l/a/w/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/l/a/m/t/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2, p3}, Lf/l/a/o/c;->e(ILf/l/a/w/b;Lf/l/a/m/t/a;)V

    iget p1, p0, Lf/l/a/o/c;->b:I

    const/4 p2, 0x0

    :goto_0
    iget p3, p0, Lf/l/a/o/c;->a:I

    if-ge p2, p3, :cond_1

    iget p3, p0, Lf/l/a/o/a;->k:I

    if-nez p3, :cond_0

    iget-object p3, p0, Lf/l/a/o/a;->j:Lf/l/a/o/a$a;

    new-array v0, p1, [B

    check-cast p3, Lf/l/a/m/b;

    invoke-virtual {p3, v0}, Lf/l/a/m/b;->j1([B)V

    goto :goto_1

    :cond_0
    iget-object p3, p0, Lf/l/a/o/a;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    new-array v0, p1, [B

    invoke-virtual {p3, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
