.class public Lf/l/a/o/b;
.super Ljava/lang/Object;
.source "Frame.java"


# static fields
.field public static final e:Lf/l/a/b;


# instance fields
.field public final a:Lf/l/a/o/c;

.field public b:Ljava/lang/Object;

.field public c:J

.field public d:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/o/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/o/b;->e:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Lf/l/a/o/c;)V
    .locals 2
    .param p1    # Lf/l/a/o/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf/l/a/o/b;->b:Ljava/lang/Object;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lf/l/a/o/b;->c:J

    iput-wide v0, p0, Lf/l/a/o/b;->d:J

    iput-object p1, p0, Lf/l/a/o/b;->a:Lf/l/a/o/c;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 6

    iget-object v0, p0, Lf/l/a/o/b;->b:Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-wide v0, p0, Lf/l/a/o/b;->c:J

    return-wide v0

    :cond_1
    sget-object v0, Lf/l/a/o/b;->e:Lf/l/a/b;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Frame is dead! time:"

    aput-object v4, v3, v1

    iget-wide v4, p0, Lf/l/a/o/b;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v2

    const/4 v1, 0x2

    const-string v2, "lastTime:"

    aput-object v2, v3, v1

    iget-wide v1, p0, Lf/l/a/o/b;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v3, v2

    invoke-virtual {v0, v2, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "You should not access a released frame. If this frame was passed to a FrameProcessor, you can only use its contents synchronously, for the duration of the process() method."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 6

    iget-object v0, p0, Lf/l/a/o/b;->b:Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return-void

    :cond_1
    sget-object v0, Lf/l/a/o/b;->e:Lf/l/a/b;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "Frame with time"

    aput-object v4, v3, v1

    iget-wide v4, p0, Lf/l/a/o/b;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const-string v4, "is being released."

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/o/b;->b:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-object v1, p0, Lf/l/a/o/b;->b:Ljava/lang/Object;

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lf/l/a/o/b;->c:J

    iget-object v1, p0, Lf/l/a/o/b;->a:Lf/l/a/o/c;

    invoke-virtual {v1}, Lf/l/a/o/c;->b()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    iget-object v2, v1, Lf/l/a/o/c;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, p0}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lf/l/a/o/c;->c(Ljava/lang/Object;Z)V

    :goto_1
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    instance-of v0, p1, Lf/l/a/o/b;

    if-eqz v0, :cond_0

    check-cast p1, Lf/l/a/o/b;

    iget-wide v0, p1, Lf/l/a/o/b;->c:J

    iget-wide v2, p0, Lf/l/a/o/b;->c:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
