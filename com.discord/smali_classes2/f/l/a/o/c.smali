.class public abstract Lf/l/a/o/c;
.super Ljava/lang/Object;
.source "FrameManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final h:Lf/l/a/b;


# instance fields
.field public final a:I

.field public b:I

.field public c:Lf/l/a/w/b;

.field public d:I

.field public final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field

.field public f:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue<",
            "Lf/l/a/o/b;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lf/l/a/m/t/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lf/l/a/o/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lf/l/a/o/c;->h:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(ILjava/lang/Class;)V
    .locals 2
    .param p2    # Ljava/lang/Class;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lf/l/a/o/c;->b:I

    const/4 v1, 0x0

    iput-object v1, p0, Lf/l/a/o/c;->c:Lf/l/a/w/b;

    iput v0, p0, Lf/l/a/o/c;->d:I

    iput p1, p0, Lf/l/a/o/c;->a:I

    iput-object p2, p0, Lf/l/a/o/c;->e:Ljava/lang/Class;

    new-instance p2, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {p2, p1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object p2, p0, Lf/l/a/o/c;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;J)Lf/l/a/o/b;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;J)",
            "Lf/l/a/o/b;"
        }
    .end annotation

    invoke-virtual {p0}, Lf/l/a/o/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/l/a/o/c;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/l/a/o/b;

    const/4 v1, 0x2

    const-string v2, "getFrame for time:"

    const/4 v3, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    sget-object v6, Lf/l/a/o/c;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v3, v4

    const-string v2, "RECYCLING."

    aput-object v2, v3, v1

    invoke-virtual {v6, v5, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v1, p0, Lf/l/a/o/c;->g:Lf/l/a/m/t/a;

    sget-object v2, Lf/l/a/m/t/c;->e:Lf/l/a/m/t/c;

    sget-object v3, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    sget-object v4, Lf/l/a/m/t/b;->e:Lf/l/a/m/t/b;

    invoke-virtual {v1, v2, v3, v4}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    iget-object v1, p0, Lf/l/a/o/c;->g:Lf/l/a/m/t/a;

    sget-object v3, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v1, v2, v3, v4}, Lf/l/a/m/t/a;->c(Lf/l/a/m/t/c;Lf/l/a/m/t/c;Lf/l/a/m/t/b;)I

    iput-object p1, v0, Lf/l/a/o/b;->b:Ljava/lang/Object;

    iput-wide p2, v0, Lf/l/a/o/b;->c:J

    iput-wide p2, v0, Lf/l/a/o/b;->d:J

    return-object v0

    :cond_0
    sget-object v0, Lf/l/a/o/c;->h:Lf/l/a/b;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, v3, v4

    const-string p2, "NOT AVAILABLE."

    aput-object p2, v3, v1

    invoke-virtual {v0, v4, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-virtual {p0, p1, v5}, Lf/l/a/o/c;->c(Ljava/lang/Object;Z)V

    const/4 p1, 0x0

    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t call getFrame() after releasing or before setUp."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lf/l/a/o/c;->c:Lf/l/a/w/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract c(Ljava/lang/Object;Z)V
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation
.end method

.method public d()V
    .locals 5

    invoke-virtual {p0}, Lf/l/a/o/c;->b()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    sget-object v0, Lf/l/a/o/c;->h:Lf/l/a/b;

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "release called twice. Ignoring."

    aput-object v3, v2, v1

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    return-void

    :cond_0
    sget-object v0, Lf/l/a/o/c;->h:Lf/l/a/b;

    new-array v3, v2, [Ljava/lang/Object;

    const-string v4, "release: Clearing the frame and buffer queue."

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lf/l/a/o/c;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lf/l/a/o/c;->b:I

    const/4 v1, 0x0

    iput-object v1, p0, Lf/l/a/o/c;->c:Lf/l/a/w/b;

    iput v0, p0, Lf/l/a/o/c;->d:I

    iput-object v1, p0, Lf/l/a/o/c;->g:Lf/l/a/m/t/a;

    return-void
.end method

.method public e(ILf/l/a/w/b;Lf/l/a/m/t/a;)V
    .locals 2
    .param p2    # Lf/l/a/w/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lf/l/a/m/t/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p2, p0, Lf/l/a/o/c;->c:Lf/l/a/w/b;

    iput p1, p0, Lf/l/a/o/c;->d:I

    invoke-static {p1}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result p1

    iget v0, p2, Lf/l/a/w/b;->e:I

    iget p2, p2, Lf/l/a/w/b;->d:I

    mul-int v0, v0, p2

    mul-int v0, v0, p1

    int-to-long p1, v0

    long-to-double p1, p1

    const-wide/high16 v0, 0x4020000000000000L    # 8.0

    div-double/2addr p1, v0

    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p1, p1

    iput p1, p0, Lf/l/a/o/c;->b:I

    const/4 p1, 0x0

    :goto_0
    iget p2, p0, Lf/l/a/o/c;->a:I

    if-ge p1, p2, :cond_0

    iget-object p2, p0, Lf/l/a/o/c;->f:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Lf/l/a/o/b;

    invoke-direct {v0, p0}, Lf/l/a/o/b;-><init>(Lf/l/a/o/c;)V

    invoke-virtual {p2, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    iput-object p3, p0, Lf/l/a/o/c;->g:Lf/l/a/m/t/a;

    return-void
.end method
