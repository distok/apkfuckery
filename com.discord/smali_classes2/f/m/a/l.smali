.class public final Lf/m/a/l;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SwipeDismissTouchListener.kt"


# instance fields
.field public final synthetic a:Lf/m/a/k;

.field public final synthetic b:Landroid/view/ViewGroup$LayoutParams;

.field public final synthetic c:I


# direct methods
.method public constructor <init>(Lf/m/a/k;Landroid/view/ViewGroup$LayoutParams;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup$LayoutParams;",
            "I)V"
        }
    .end annotation

    iput-object p1, p0, Lf/m/a/l;->a:Lf/m/a/k;

    iput-object p2, p0, Lf/m/a/l;->b:Landroid/view/ViewGroup$LayoutParams;

    iput p3, p0, Lf/m/a/l;->c:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animation"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/m/a/l;->a:Lf/m/a/k;

    iget-object v0, p1, Lf/m/a/k;->o:Lf/m/a/k$a;

    iget-object p1, p1, Lf/m/a/k;->n:Landroid/view/View;

    invoke-interface {v0, p1}, Lf/m/a/k$a;->onDismiss(Landroid/view/View;)V

    iget-object p1, p0, Lf/m/a/l;->a:Lf/m/a/k;

    iget-object p1, p1, Lf/m/a/k;->n:Landroid/view/View;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lf/m/a/l;->a:Lf/m/a/k;

    iget-object p1, p1, Lf/m/a/k;->n:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    iget-object p1, p0, Lf/m/a/l;->b:Landroid/view/ViewGroup$LayoutParams;

    iget v0, p0, Lf/m/a/l;->c:I

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lf/m/a/l;->a:Lf/m/a/k;

    iget-object v0, v0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
