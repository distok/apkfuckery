.class public final Lf/m/a/k;
.super Ljava/lang/Object;
.source "SwipeDismissTouchListener.kt"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/m/a/k$a;
    }
.end annotation


# instance fields
.field public final d:I

.field public final e:I

.field public final f:J

.field public g:I

.field public h:F

.field public i:F

.field public j:Z

.field public k:I

.field public l:Landroid/view/VelocityTracker;

.field public m:F

.field public final n:Landroid/view/View;

.field public final o:Lf/m/a/k$a;


# direct methods
.method public constructor <init>(Landroid/view/View;Lf/m/a/k$a;)V
    .locals 1

    const-string v0, "mView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mCallbacks"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/m/a/k;->n:Landroid/view/View;

    iput-object p2, p0, Lf/m/a/k;->o:Lf/m/a/k$a;

    const/4 p2, 0x1

    iput p2, p0, Lf/m/a/k;->g:I

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p2

    const-string v0, "vc"

    invoke-static {p2, v0}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lf/m/a/k;->d:I

    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result p2

    mul-int/lit8 p2, p2, 0x10

    iput p2, p0, Lf/m/a/k;->e:I

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "mView.context"

    invoke-static {p1, p2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    int-to-long p1, p1

    iput-wide p1, p0, Lf/m/a/k;->f:J

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0xc
    .end annotation

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "motionEvent"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lf/m/a/k;->m:F

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget v0, p0, Lf/m/a/k;->g:I

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lf/m/a/k;->g:I

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_11

    const/high16 v6, 0x3f800000    # 1.0f

    if-eq v0, v4, :cond_5

    const/4 v7, 0x3

    if-eq v0, v2, :cond_2

    if-eq v0, v7, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    return v5

    :cond_1
    iget-object p1, p0, Lf/m/a/k;->l:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_10

    iget-object p2, p0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    iget-wide v6, p0, Lf/m/a/k;->f:J

    invoke-virtual {p2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v3, p0, Lf/m/a/k;->l:Landroid/view/VelocityTracker;

    iput v1, p0, Lf/m/a/k;->m:F

    iput v1, p0, Lf/m/a/k;->h:F

    iput v1, p0, Lf/m/a/k;->i:F

    iput-boolean v5, p0, Lf/m/a/k;->j:Z

    goto/16 :goto_8

    :cond_2
    iget-object p1, p0, Lf/m/a/k;->l:Landroid/view/VelocityTracker;

    if-eqz p1, :cond_10

    invoke-virtual {p1, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result p1

    iget v0, p0, Lf/m/a/k;->h:F

    sub-float/2addr p1, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v3, p0, Lf/m/a/k;->i:F

    sub-float/2addr v0, v3

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v8, p0, Lf/m/a/k;->d:I

    int-to-float v8, v8

    cmpl-float v3, v3, v8

    if-lez v3, :cond_4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    int-to-float v2, v2

    div-float/2addr v3, v2

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    iput-boolean v4, p0, Lf/m/a/k;->j:Z

    int-to-float v0, v5

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    iget v0, p0, Lf/m/a/k;->d:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lf/m/a/k;->d:I

    neg-int v0, v0

    :goto_0
    iput v0, p0, Lf/m/a/k;->k:I

    iget-object v0, p0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    const-string v2, "cancelEvent"

    invoke-static {v0, v2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result p2

    shl-int/lit8 p2, p2, 0x8

    or-int/2addr p2, v7

    invoke-virtual {v0, p2}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object p2, p0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    :cond_4
    iget-boolean p2, p0, Lf/m/a/k;->j:Z

    if-eqz p2, :cond_10

    iput p1, p0, Lf/m/a/k;->m:F

    iget-object p2, p0, Lf/m/a/k;->n:Landroid/view/View;

    iget v0, p0, Lf/m/a/k;->k:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTranslationX(F)V

    iget-object p2, p0, Lf/m/a/k;->n:Landroid/view/View;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    mul-float p1, p1, v0

    iget v0, p0, Lf/m/a/k;->g:I

    int-to-float v0, v0

    div-float/2addr p1, v0

    sub-float p1, v6, p1

    invoke-static {v6, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-virtual {p2, p1}, Landroid/view/View;->setAlpha(F)V

    return v4

    :cond_5
    iget-object v0, p0, Lf/m/a/k;->l:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_10

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    iget v8, p0, Lf/m/a/k;->h:F

    sub-float/2addr v7, v8

    invoke-virtual {v0, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    const/16 v8, 0x3e8

    invoke-virtual {v0, v8}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v11

    iget v12, p0, Lf/m/a/k;->g:I

    div-int/2addr v12, v2

    int-to-float v2, v12

    cmpl-float v2, v11, v2

    if-lez v2, :cond_7

    iget-boolean v2, p0, Lf/m/a/k;->j:Z

    if-eqz v2, :cond_7

    int-to-float v2, v5

    cmpl-float v2, v7, v2

    if-lez v2, :cond_6

    const/4 v2, 0x1

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    iget v2, p0, Lf/m/a/k;->e:I

    int-to-float v2, v2

    cmpg-float v2, v2, v9

    if-gtz v2, :cond_c

    cmpg-float v2, v10, v9

    if-gez v2, :cond_c

    iget-boolean v2, p0, Lf/m/a/k;->j:Z

    if-eqz v2, :cond_c

    int-to-float v2, v5

    cmpg-float v8, v8, v2

    if-gez v8, :cond_8

    const/4 v8, 0x1

    goto :goto_1

    :cond_8
    const/4 v8, 0x0

    :goto_1
    cmpg-float v7, v7, v2

    if-gez v7, :cond_9

    const/4 v7, 0x1

    goto :goto_2

    :cond_9
    const/4 v7, 0x0

    :goto_2
    if-ne v8, v7, :cond_a

    const/4 v7, 0x1

    goto :goto_3

    :cond_a
    const/4 v7, 0x0

    :goto_3
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v8

    cmpl-float v2, v8, v2

    if-lez v2, :cond_b

    goto :goto_4

    :cond_b
    const/4 v4, 0x0

    :goto_4
    move v2, v4

    move v4, v7

    goto :goto_5

    :cond_c
    const/4 v2, 0x0

    const/4 v4, 0x0

    :goto_5
    if-eqz v4, :cond_e

    iget-object v4, p0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    if-eqz v2, :cond_d

    iget v2, p0, Lf/m/a/k;->g:I

    goto :goto_6

    :cond_d
    iget v2, p0, Lf/m/a/k;->g:I

    neg-int v2, v2

    :goto_6
    int-to-float v2, v2

    invoke-virtual {v4, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget-wide v6, p0, Lf/m/a/k;->f:J

    invoke-virtual {v2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    new-instance v4, Lf/m/a/k$b;

    invoke-direct {v4, p0, p2, p1}, Lf/m/a/k$b;-><init>(Lf/m/a/k;Landroid/view/MotionEvent;Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_7

    :cond_e
    iget-boolean p2, p0, Lf/m/a/k;->j:Z

    if-eqz p2, :cond_f

    iget-object p2, p0, Lf/m/a/k;->n:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    iget-wide v6, p0, Lf/m/a/k;->f:J

    invoke-virtual {p2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    invoke-virtual {p2, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object p2, p0, Lf/m/a/k;->o:Lf/m/a/k$a;

    invoke-interface {p2, p1, v5}, Lf/m/a/k$a;->a(Landroid/view/View;Z)V

    :cond_f
    :goto_7
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v3, p0, Lf/m/a/k;->l:Landroid/view/VelocityTracker;

    iput v1, p0, Lf/m/a/k;->m:F

    iput v1, p0, Lf/m/a/k;->h:F

    iput v1, p0, Lf/m/a/k;->i:F

    iput-boolean v5, p0, Lf/m/a/k;->j:Z

    :cond_10
    :goto_8
    return v5

    :cond_11
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lf/m/a/k;->h:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lf/m/a/k;->i:F

    iget-object v0, p0, Lf/m/a/k;->o:Lf/m/a/k$a;

    invoke-interface {v0}, Lf/m/a/k$a;->b()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lf/m/a/k;->l:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_12

    invoke-virtual {v0, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_9

    :cond_12
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v3

    :cond_13
    :goto_9
    iget-object p2, p0, Lf/m/a/k;->o:Lf/m/a/k$a;

    invoke-interface {p2, p1, v4}, Lf/m/a/k$a;->a(Landroid/view/View;Z)V

    return v5
.end method
