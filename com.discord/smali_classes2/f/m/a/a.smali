.class public final Lf/m/a/a;
.super Ljava/lang/Object;
.source "Alert.kt"

# interfaces
.implements Lf/m/a/k$a;


# instance fields
.field public final synthetic d:Lf/m/a/b;


# direct methods
.method public constructor <init>(Lf/m/a/b;)V
    .locals 0

    iput-object p1, p0, Lf/m/a/a;->d:Lf/m/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Z)V
    .locals 0

    const-string p2, "view"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDismiss(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/m/a/a;->d:Lf/m/a/b;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->clearAnimation()V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lf/m/a/b;->setVisibility(I)V

    new-instance v0, Lf/m/a/d;

    invoke-direct {v0, p1}, Lf/m/a/d;-><init>(Lf/m/a/b;)V

    const/16 v1, 0x64

    int-to-long v1, v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
