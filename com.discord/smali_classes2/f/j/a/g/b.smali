.class public final Lf/j/a/g/b;
.super Ljava/lang/Object;
.source "KronosClockImpl.kt"

# interfaces
.implements Lcom/lyft/kronos/KronosClock;


# instance fields
.field public final a:Lf/j/a/g/d/h;

.field public final b:Lf/j/a/b;


# direct methods
.method public constructor <init>(Lf/j/a/g/d/h;Lf/j/a/b;)V
    .locals 1

    const-string v0, "ntpService"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fallbackClock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/j/a/g/b;->a:Lf/j/a/g/d/h;

    iput-object p2, p0, Lf/j/a/g/b;->b:Lf/j/a/b;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    invoke-virtual {p0}, Lf/j/a/g/b;->c()Lf/j/a/d;

    move-result-object v0

    iget-wide v0, v0, Lf/j/a/d;->a:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lf/j/a/g/b;->b:Lf/j/a/b;

    invoke-interface {v0}, Lf/j/a/b;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lf/j/a/d;
    .locals 4

    iget-object v0, p0, Lf/j/a/g/b;->a:Lf/j/a/g/d/h;

    invoke-interface {v0}, Lf/j/a/g/d/h;->a()Lf/j/a/d;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lf/j/a/d;

    iget-object v1, p0, Lf/j/a/g/b;->b:Lf/j/a/b;

    invoke-interface {v1}, Lf/j/a/b;->a()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lf/j/a/d;-><init>(JLjava/lang/Long;)V

    :goto_0
    return-object v0
.end method
