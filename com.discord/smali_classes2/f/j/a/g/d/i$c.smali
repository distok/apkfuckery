.class public final Lf/j/a/g/d/i$c;
.super Ljava/lang/Object;
.source "SntpService.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/j/a/g/d/i;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/j/a/g/d/i;


# direct methods
.method public constructor <init>(Lf/j/a/g/d/i;)V
    .locals 0

    iput-object p1, p0, Lf/j/a/g/d/i$c;->d:Lf/j/a/g/d/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    iget-object v0, p0, Lf/j/a/g/d/i$c;->d:Lf/j/a/g/d/i;

    invoke-virtual {v0}, Lf/j/a/g/d/i;->c()V

    iget-object v1, v0, Lf/j/a/g/d/i;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v4, Lf/j/a/g/d/i$a;->e:Lf/j/a/g/d/i$a;

    iget-object v5, v0, Lf/j/a/g/d/i;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v6, Lf/j/a/g/d/i$a;->f:Lf/j/a/g/d/i$a;

    invoke-virtual {v5, v6}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lf/j/a/g/d/i$a;

    if-eq v5, v6, :cond_5

    iget-object v5, v0, Lf/j/a/g/d/i;->e:Lf/j/a/b;

    invoke-interface {v5}, Lf/j/a/b;->b()J

    move-result-wide v5

    iget-object v7, v0, Lf/j/a/g/d/i;->g:Lf/j/a/e;

    if-eqz v7, :cond_1

    invoke-interface {v7, v2}, Lf/j/a/e;->a(Ljava/lang/String;)V

    :cond_1
    :try_start_0
    iget-object v7, v0, Lf/j/a/g/d/i;->d:Lf/j/a/g/d/e;

    iget-wide v8, v0, Lf/j/a/g/d/i;->i:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v2, v8}, Lf/j/a/g/d/e;->d(Ljava/lang/String;Ljava/lang/Long;)Lf/j/a/g/d/e$b;

    move-result-object v7

    const-string v8, "response"

    invoke-static {v7, v8}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lf/j/a/g/d/e$b;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v12, v8, v10

    if-ltz v12, :cond_3

    iget-object v8, v0, Lf/j/a/g/d/i;->f:Lf/j/a/g/d/f;

    invoke-interface {v8, v7}, Lf/j/a/g/d/f;->a(Lf/j/a/g/d/e$b;)V

    iget-wide v7, v7, Lf/j/a/g/d/e$b;->c:J

    iget-object v9, v0, Lf/j/a/g/d/i;->e:Lf/j/a/b;

    invoke-interface {v9}, Lf/j/a/b;->b()J

    move-result-wide v9

    sub-long/2addr v9, v5

    iget-object v5, v0, Lf/j/a/g/d/i;->g:Lf/j/a/e;

    if-eqz v5, :cond_2

    invoke-interface {v5, v7, v8, v9, v10}, Lf/j/a/e;->b(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    iget-object v2, v0, Lf/j/a/g/d/i;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v2, v0, Lf/j/a/g/d/i;->b:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v3, v0, Lf/j/a/g/d/i;->e:Lf/j/a/b;

    invoke-interface {v3}, Lf/j/a/b;->b()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    :try_start_1
    new-instance v5, Lcom/lyft/kronos/internal/ntp/NTPSyncException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Invalid time "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Lf/j/a/g/d/e$b;->a()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, " received from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/lyft/kronos/internal/ntp/NTPSyncException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    :try_start_2
    iget-object v6, v0, Lf/j/a/g/d/i;->g:Lf/j/a/e;

    if-eqz v6, :cond_4

    invoke-interface {v6, v2, v5}, Lf/j/a/e;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_4
    iget-object v2, v0, Lf/j/a/g/d/i;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v2, v0, Lf/j/a/g/d/i;->b:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v4, v0, Lf/j/a/g/d/i;->e:Lf/j/a/b;

    invoke-interface {v4}, Lf/j/a/b;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    goto :goto_0

    :catchall_1
    move-exception v1

    iget-object v2, v0, Lf/j/a/g/d/i;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v2, v0, Lf/j/a/g/d/i;->b:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v0, v0, Lf/j/a/g/d/i;->e:Lf/j/a/b;

    invoke-interface {v0}, Lf/j/a/b;->b()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    throw v1

    :cond_5
    :goto_0
    if-eqz v3, :cond_0

    :cond_6
    return-void
.end method
