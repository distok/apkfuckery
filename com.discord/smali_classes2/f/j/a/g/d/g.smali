.class public final Lf/j/a/g/d/g;
.super Ljava/lang/Object;
.source "SntpResponseCache.kt"

# interfaces
.implements Lf/j/a/g/d/f;


# instance fields
.field public final a:Lf/j/a/f;

.field public final b:Lf/j/a/b;


# direct methods
.method public constructor <init>(Lf/j/a/f;Lf/j/a/b;)V
    .locals 1

    const-string v0, "syncResponseCache"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceClock"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    iput-object p2, p0, Lf/j/a/g/d/g;->b:Lf/j/a/b;

    return-void
.end method


# virtual methods
.method public a(Lf/j/a/g/d/e$b;)V
    .locals 3

    const-string v0, "response"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    iget-wide v1, p1, Lf/j/a/g/d/e$b;->a:J

    invoke-interface {v0, v1, v2}, Lf/j/a/f;->f(J)V

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    iget-wide v1, p1, Lf/j/a/g/d/e$b;->b:J

    invoke-interface {v0, v1, v2}, Lf/j/a/f;->a(J)V

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    iget-wide v1, p1, Lf/j/a/g/d/e$b;->c:J

    invoke-interface {v0, v1, v2}, Lf/j/a/f;->b(J)V

    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    invoke-interface {v0}, Lf/j/a/f;->clear()V

    return-void
.end method

.method public get()Lf/j/a/g/d/e$b;
    .locals 9

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    invoke-interface {v0}, Lf/j/a/f;->e()J

    move-result-wide v2

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    invoke-interface {v0}, Lf/j/a/f;->c()J

    move-result-wide v4

    iget-object v0, p0, Lf/j/a/g/d/g;->a:Lf/j/a/f;

    invoke-interface {v0}, Lf/j/a/f;->d()J

    move-result-wide v6

    const-wide/16 v0, 0x0

    cmp-long v8, v4, v0

    if-nez v8, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lf/j/a/g/d/e$b;

    iget-object v8, p0, Lf/j/a/g/d/g;->b:Lf/j/a/b;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lf/j/a/g/d/e$b;-><init>(JJJLf/j/a/b;)V

    :goto_0
    return-object v0
.end method
