.class public final enum Lf/j/a/g/d/i$a;
.super Ljava/lang/Enum;
.source "SntpService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/j/a/g/d/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lf/j/a/g/d/i$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lf/j/a/g/d/i$a;

.field public static final enum e:Lf/j/a/g/d/i$a;

.field public static final enum f:Lf/j/a/g/d/i$a;

.field public static final enum g:Lf/j/a/g/d/i$a;

.field public static final synthetic h:[Lf/j/a/g/d/i$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lf/j/a/g/d/i$a;

    new-instance v1, Lf/j/a/g/d/i$a;

    const-string v2, "INIT"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lf/j/a/g/d/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/j/a/g/d/i$a;->d:Lf/j/a/g/d/i$a;

    aput-object v1, v0, v3

    new-instance v1, Lf/j/a/g/d/i$a;

    const-string v2, "IDLE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lf/j/a/g/d/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/j/a/g/d/i$a;->e:Lf/j/a/g/d/i$a;

    aput-object v1, v0, v3

    new-instance v1, Lf/j/a/g/d/i$a;

    const-string v2, "SYNCING"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lf/j/a/g/d/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/j/a/g/d/i$a;->f:Lf/j/a/g/d/i$a;

    aput-object v1, v0, v3

    new-instance v1, Lf/j/a/g/d/i$a;

    const-string v2, "STOPPED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lf/j/a/g/d/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lf/j/a/g/d/i$a;->g:Lf/j/a/g/d/i$a;

    aput-object v1, v0, v3

    sput-object v0, Lf/j/a/g/d/i$a;->h:[Lf/j/a/g/d/i$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lf/j/a/g/d/i$a;
    .locals 1

    const-class v0, Lf/j/a/g/d/i$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lf/j/a/g/d/i$a;

    return-object p0
.end method

.method public static values()[Lf/j/a/g/d/i$a;
    .locals 1

    sget-object v0, Lf/j/a/g/d/i$a;->h:[Lf/j/a/g/d/i$a;

    invoke-virtual {v0}, [Lf/j/a/g/d/i$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lf/j/a/g/d/i$a;

    return-object v0
.end method
