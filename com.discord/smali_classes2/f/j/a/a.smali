.class public final Lf/j/a/a;
.super Ljava/lang/Object;
.source "AndroidClockFactory.kt"


# direct methods
.method public static a(Landroid/content/Context;Lf/j/a/e;Ljava/util/List;JJJI)Lcom/lyft/kronos/KronosClock;
    .locals 15

    move-object v0, p0

    and-int/lit8 v1, p9, 0x2

    const/4 v6, 0x0

    and-int/lit8 v1, p9, 0x4

    if-eqz v1, :cond_0

    sget-object v1, Lf/j/a/c;->e:Lf/j/a/c;

    sget-object v1, Lf/j/a/c;->a:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v7, v1

    and-int/lit8 v1, p9, 0x8

    if-eqz v1, :cond_1

    sget-object v1, Lf/j/a/c;->e:Lf/j/a/c;

    sget-wide v1, Lf/j/a/c;->d:J

    move-wide v8, v1

    goto :goto_1

    :cond_1
    move-wide/from16 v8, p3

    :goto_1
    and-int/lit8 v1, p9, 0x10

    if-eqz v1, :cond_2

    sget-object v1, Lf/j/a/c;->e:Lf/j/a/c;

    sget-wide v1, Lf/j/a/c;->c:J

    move-wide v10, v1

    goto :goto_2

    :cond_2
    move-wide/from16 v10, p5

    :goto_2
    and-int/lit8 v1, p9, 0x20

    if-eqz v1, :cond_3

    sget-object v1, Lf/j/a/c;->e:Lf/j/a/c;

    sget-wide v1, Lf/j/a/c;->b:J

    move-wide v12, v1

    goto :goto_3

    :cond_3
    move-wide/from16 v12, p7

    :goto_3
    const-string v1, "context"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "ntpHosts"

    invoke-static {v7, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v14, Lf/j/a/g/a;

    invoke-direct {v14}, Lf/j/a/g/a;-><init>()V

    new-instance v2, Lf/j/a/g/c;

    const/4 v3, 0x0

    const-string v4, "com.lyft.kronos.shared_preferences"

    invoke-virtual {p0, v4, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "context.getSharedPrefere\u2026ME, Context.MODE_PRIVATE)"

    invoke-static {v0, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Lf/j/a/g/c;-><init>(Landroid/content/SharedPreferences;)V

    const-string v0, "localClock"

    invoke-static {v14, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "syncResponseCache"

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, v14, Lcom/lyft/kronos/KronosClock;

    if-nez v0, :cond_4

    new-instance v3, Lf/j/a/g/d/e;

    new-instance v0, Lf/j/a/g/d/d;

    invoke-direct {v0}, Lf/j/a/g/d/d;-><init>()V

    new-instance v1, Lf/j/a/g/d/b;

    invoke-direct {v1}, Lf/j/a/g/d/b;-><init>()V

    invoke-direct {v3, v14, v0, v1}, Lf/j/a/g/d/e;-><init>(Lf/j/a/b;Lf/j/a/g/d/c;Lf/j/a/g/d/a;)V

    new-instance v5, Lf/j/a/g/d/g;

    invoke-direct {v5, v2, v14}, Lf/j/a/g/d/g;-><init>(Lf/j/a/f;Lf/j/a/b;)V

    new-instance v0, Lf/j/a/g/d/i;

    move-object v2, v0

    move-object v4, v14

    invoke-direct/range {v2 .. v13}, Lf/j/a/g/d/i;-><init>(Lf/j/a/g/d/e;Lf/j/a/b;Lf/j/a/g/d/f;Lf/j/a/e;Ljava/util/List;JJJ)V

    new-instance v1, Lf/j/a/g/b;

    invoke-direct {v1, v0, v14}, Lf/j/a/g/b;-><init>(Lf/j/a/g/d/h;Lf/j/a/b;)V

    return-object v1

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Local clock should implement Clock instead of KronosClock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
