.class public Lf/i/a/a/e$d;
.super Ljava/lang/Object;
.source "ColorPickerDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/i/a/a/e;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/i/a/a/e;


# direct methods
.method public constructor <init>(Lf/i/a/a/e;)V
    .locals 0

    iput-object p1, p0, Lf/i/a/a/e$d;->d:Lf/i/a/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lf/i/a/a/e$d;->d:Lf/i/a/a/e;

    iget-object v0, p1, Lf/i/a/a/e;->d:Lf/i/a/a/g;

    if-eqz v0, :cond_0

    const-string v0, "ColorPickerDialog"

    const-string v1, "Using deprecated listener which may be remove in future releases"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lf/i/a/a/e;->d:Lf/i/a/a/g;

    iget p1, p1, Lf/i/a/a/e;->j:I

    invoke-interface {v0, p1}, Lf/i/a/a/g;->onColorReset(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    instance-of v1, v0, Lf/i/a/a/g;

    if-eqz v1, :cond_1

    check-cast v0, Lf/i/a/a/g;

    iget p1, p1, Lf/i/a/a/e;->j:I

    invoke-interface {v0, p1}, Lf/i/a/a/g;->onColorReset(I)V

    :goto_0
    iget-object p1, p0, Lf/i/a/a/e$d;->d:Lf/i/a/a/e;

    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The activity must implement ColorPickerDialogListener"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
