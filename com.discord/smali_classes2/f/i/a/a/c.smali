.class public Lf/i/a/a/c;
.super Ljava/lang/Object;
.source "ColorPaletteAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Lf/i/a/a/b$b;


# direct methods
.method public constructor <init>(Lf/i/a/a/b$b;I)V
    .locals 0

    iput-object p1, p0, Lf/i/a/a/c;->e:Lf/i/a/a/b$b;

    iput p2, p0, Lf/i/a/a/c;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lf/i/a/a/c;->e:Lf/i/a/a/b$b;

    iget-object p1, p1, Lf/i/a/a/b$b;->e:Lf/i/a/a/b;

    iget v0, p1, Lf/i/a/a/b;->f:I

    iget v1, p0, Lf/i/a/a/c;->d:I

    if-eq v0, v1, :cond_0

    iput v1, p1, Lf/i/a/a/b;->f:I

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    iget-object p1, p0, Lf/i/a/a/c;->e:Lf/i/a/a/b$b;

    iget-object p1, p1, Lf/i/a/a/b$b;->e:Lf/i/a/a/b;

    iget-object v0, p1, Lf/i/a/a/b;->d:Lf/i/a/a/b$a;

    iget-object p1, p1, Lf/i/a/a/b;->e:[I

    iget v1, p0, Lf/i/a/a/c;->d:I

    aget p1, p1, v1

    check-cast v0, Lf/i/a/a/e$h;

    iget-object v1, v0, Lf/i/a/a/e$h;->a:Lf/i/a/a/e;

    iget v2, v1, Lf/i/a/a/e;->h:I

    if-ne v2, p1, :cond_1

    invoke-static {v1, v2}, Lf/i/a/a/e;->f(Lf/i/a/a/e;I)V

    iget-object p1, v0, Lf/i/a/a/e$h;->a:Lf/i/a/a/e;

    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    goto :goto_0

    :cond_1
    iput p1, v1, Lf/i/a/a/e;->h:I

    iget-boolean v0, v1, Lf/i/a/a/e;->k:Z

    if-eqz v0, :cond_2

    invoke-virtual {v1, p1}, Lf/i/a/a/e;->g(I)V

    :cond_2
    :goto_0
    return-void
.end method
