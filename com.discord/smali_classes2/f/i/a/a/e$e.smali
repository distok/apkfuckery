.class public Lf/i/a/a/e$e;
.super Ljava/lang/Object;
.source "ColorPickerDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/i/a/a/e;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/i/a/a/e;


# direct methods
.method public constructor <init>(Lf/i/a/a/e;)V
    .locals 0

    iput-object p1, p0, Lf/i/a/a/e$e;->d:Lf/i/a/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lf/i/a/a/e$e;->d:Lf/i/a/a/e;

    iget-object v0, v0, Lf/i/a/a/e;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lf/i/a/a/e$e;->d:Lf/i/a/a/e;

    iget v1, v0, Lf/i/a/a/e;->i:I

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v2, :cond_0

    goto :goto_2

    :cond_0
    const/4 v1, 0x0

    iput v1, v0, Lf/i/a/a/e;->i:I

    check-cast p1, Landroid/widget/Button;

    iget v0, v0, Lf/i/a/a/e;->A:I

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget v0, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_presets:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    iget-object p1, p0, Lf/i/a/a/e$e;->d:Lf/i/a/a/e;

    iget-object v0, p1, Lf/i/a/a/e;->f:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Lf/i/a/a/e;->h()Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    iput v2, v0, Lf/i/a/a/e;->i:I

    check-cast p1, Landroid/widget/Button;

    iget v0, v0, Lf/i/a/a/e;->C:I

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    sget v0, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_custom:I

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    iget-object p1, p0, Lf/i/a/a/e$e;->d:Lf/i/a/a/e;

    iget-object v0, p1, Lf/i/a/a/e;->f:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Lf/i/a/a/e;->i()Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :goto_2
    return-void
.end method
