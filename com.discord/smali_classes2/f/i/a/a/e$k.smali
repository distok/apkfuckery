.class public final Lf/i/a/a/e$k;
.super Ljava/lang/Object;
.source "ColorPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/i/a/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "k"
.end annotation


# instance fields
.field public a:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public b:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public c:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public d:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public e:I
    .annotation build Landroidx/annotation/StringRes;
    .end annotation
.end field

.field public f:I

.field public g:[I

.field public h:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:I

.field public o:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public p:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public q:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public r:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public s:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public t:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public u:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public v:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public w:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public x:I
    .annotation build Landroidx/annotation/FontRes;
    .end annotation
.end field

.field public y:I
    .annotation build Landroidx/annotation/FontRes;
    .end annotation
.end field

.field public z:I
    .annotation build Landroidx/annotation/FontRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_default_title:I

    iput v0, p0, Lf/i/a/a/e$k;->a:I

    sget v0, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_presets:I

    iput v0, p0, Lf/i/a/a/e$k;->b:I

    sget v0, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_custom:I

    iput v0, p0, Lf/i/a/a/e$k;->c:I

    sget v0, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_select:I

    iput v0, p0, Lf/i/a/a/e$k;->d:I

    const/4 v0, 0x0

    iput v0, p0, Lf/i/a/a/e$k;->e:I

    const/4 v1, 0x1

    iput v1, p0, Lf/i/a/a/e$k;->f:I

    sget-object v2, Lf/i/a/a/e;->E:[I

    iput-object v2, p0, Lf/i/a/a/e$k;->g:[I

    const/high16 v2, -0x1000000

    iput v2, p0, Lf/i/a/a/e$k;->h:I

    iput-boolean v0, p0, Lf/i/a/a/e$k;->i:Z

    iput-boolean v1, p0, Lf/i/a/a/e$k;->j:Z

    iput-boolean v1, p0, Lf/i/a/a/e$k;->k:Z

    iput-boolean v1, p0, Lf/i/a/a/e$k;->l:Z

    iput-boolean v1, p0, Lf/i/a/a/e$k;->m:Z

    iput v1, p0, Lf/i/a/a/e$k;->n:I

    iput v0, p0, Lf/i/a/a/e$k;->o:I

    iput v0, p0, Lf/i/a/a/e$k;->p:I

    iput v0, p0, Lf/i/a/a/e$k;->q:I

    iput v0, p0, Lf/i/a/a/e$k;->r:I

    iput v0, p0, Lf/i/a/a/e$k;->s:I

    iput v0, p0, Lf/i/a/a/e$k;->t:I

    iput v0, p0, Lf/i/a/a/e$k;->u:I

    iput v0, p0, Lf/i/a/a/e$k;->v:I

    iput v0, p0, Lf/i/a/a/e$k;->w:I

    iput v0, p0, Lf/i/a/a/e$k;->x:I

    iput v0, p0, Lf/i/a/a/e$k;->y:I

    iput v0, p0, Lf/i/a/a/e$k;->z:I

    return-void
.end method


# virtual methods
.method public a()Lf/i/a/a/e;
    .locals 4

    new-instance v0, Lf/i/a/a/e;

    invoke-direct {v0}, Lf/i/a/a/e;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "id"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->f:I

    const-string v3, "dialogType"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->h:I

    const-string v3, "color"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lf/i/a/a/e$k;->g:[I

    const-string v3, "presets"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    iget-boolean v2, p0, Lf/i/a/a/e$k;->i:Z

    const-string v3, "alpha"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v2, p0, Lf/i/a/a/e$k;->k:Z

    const-string v3, "allowCustom"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v2, p0, Lf/i/a/a/e$k;->j:Z

    const-string v3, "allowPresets"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-boolean v2, p0, Lf/i/a/a/e$k;->l:Z

    const-string v3, "allowReset"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v2, p0, Lf/i/a/a/e$k;->a:I

    const-string v3, "dialogTitle"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-boolean v2, p0, Lf/i/a/a/e$k;->m:Z

    const-string v3, "showColorShades"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v2, p0, Lf/i/a/a/e$k;->n:I

    const-string v3, "colorShape"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->b:I

    const-string v3, "presetsButtonText"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->c:I

    const-string v3, "customButtonText"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->o:I

    const-string v3, "customButtonColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->v:I

    const-string v3, "customButtonTextColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->d:I

    const-string v3, "selectedButtonText"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->p:I

    const-string v3, "selectedButtonColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->w:I

    const-string v3, "selectedButtonTextColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->e:I

    const-string v3, "resetButtonText"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->y:I

    const-string v3, "buttonFont"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->r:I

    const-string v3, "titleTextColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->x:I

    const-string v3, "titleFont"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->q:I

    const-string v3, "dividerColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->s:I

    const-string v3, "backgroundColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->t:I

    const-string v3, "inputTextColor"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->u:I

    const-string v3, "inputBackground"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v2, p0, Lf/i/a/a/e$k;->z:I

    const-string v3, "inputFont"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method
