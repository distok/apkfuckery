.class public final Lf/b/a/a/g;
.super Ljava/lang/Object;
.source "FlexInputFragment.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic d:Lcom/lytefast/flexinput/fragment/FlexInputFragment;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V
    .locals 0

    iput-object p1, p0, Lf/b/a/a/g;->d:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    iget-object p1, p0, Lf/b/a/a/g;->d:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/b/a/a/g;->d:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->isHidden()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lf/b/a/a/g;->d:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onContentDialogDismissed()V

    :cond_1
    :goto_0
    return-void
.end method
