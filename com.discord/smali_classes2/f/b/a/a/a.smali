.class public Lf/b/a/a/a;
.super Landroidx/appcompat/app/AppCompatDialogFragment;
.source "AddContentDialogFragment.kt"


# static fields
.field public static final synthetic l:I


# instance fields
.field public d:Landroidx/viewpager/widget/ViewPager;

.field public e:Lcom/google/android/material/tabs/TabLayout;

.field public f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field public g:Landroid/widget/ImageView;

.field public h:Lcom/lytefast/flexinput/utils/SelectionAggregator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lytefast/flexinput/utils/SelectionAggregator<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public i:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

.field public final k:Lf/b/a/a/a$c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatDialogFragment;-><init>()V

    new-instance v0, Lf/b/a/a/a$c;

    invoke-direct {v0, p0}, Lf/b/a/a/a$c;-><init>(Lf/b/a/a/a;)V

    iput-object v0, p0, Lf/b/a/a/a;->k:Lf/b/a/a/a$c;

    return-void
.end method

.method public static final f(Lf/b/a/a/a;)V
    .locals 2

    iget-object v0, p0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_0

    new-instance v1, Lf/b/a/a/d;

    invoke-direct {v1, p0}, Lf/b/a/a/d;-><init>(Lf/b/a/a/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final g()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, "context ?: return dismissAllowingStateLoss()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/google/android/material/R$anim;->design_bottom_sheet_slide_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    const-string v2, "animation"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/material/R$integer;->bottom_sheet_slide_duration:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const v2, 0x10a0004

    invoke-virtual {v1, v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    iget-object v0, p0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->hide()V

    :cond_0
    iget-object v0, p0, Lf/b/a/a/a;->e:Lcom/google/android/material/tabs/TabLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget-object v0, p0, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    iget-object v0, p0, Lf/b/a/a/a;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    new-instance v0, Lf/b/a/a/a$b;

    invoke-direct {v0, p0}, Lf/b/a/a/a$b;-><init>(Lf/b/a/a/a;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void

    :cond_4
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x1750

    if-ne v0, p1, :cond_6

    if-nez p2, :cond_0

    goto/16 :goto_4

    :cond_0
    const/4 p1, -0x1

    const/4 v0, 0x0

    if-ne p1, p2, :cond_5

    if-nez p3, :cond_1

    goto/16 :goto_3

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p3}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object p2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.lytefast.flexinput.FlexInputCoordinator<kotlin.Any>"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Lf/b/a/b;

    if-nez p2, :cond_2

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p2

    if-eqz p2, :cond_4

    sget-object p3, Lcom/lytefast/flexinput/model/Attachment;->Companion:Lcom/lytefast/flexinput/model/Attachment$Companion;

    invoke-virtual {p3, p2, p1}, Lcom/lytefast/flexinput/model/Attachment$Companion;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object p1

    invoke-interface {v1, p1}, Lf/b/a/b;->e(Lcom/lytefast/flexinput/model/Attachment;)V

    goto :goto_2

    :cond_2
    invoke-virtual {p2}, Landroid/content/ClipData;->getItemCount()I

    move-result p3

    invoke-static {v0, p3}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object p3

    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p3, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, p3

    check-cast v2, Lx/h/o;

    invoke-virtual {v2}, Lx/h/o;->nextInt()I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    const-string v3, "clipData.getItemAt(it)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/net/Uri;

    sget-object v0, Lcom/lytefast/flexinput/model/Attachment;->Companion:Lcom/lytefast/flexinput/model/Attachment$Companion;

    const-string v2, "it"

    invoke-static {p3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p3, p1}, Lcom/lytefast/flexinput/model/Attachment$Companion;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object p3

    invoke-interface {v1, p3}, Lf/b/a/b;->e(Lcom/lytefast/flexinput/model/Attachment;)V

    goto :goto_1

    :cond_4
    :goto_2
    return-void

    :cond_5
    :goto_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "Error loading files"

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_6
    :goto_4
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PrivateResource"
        }
    .end annotation

    new-instance p1, Lf/b/a/a/a$d;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/lytefast/flexinput/R$h;->FlexInput_DialogWhenLarge:I

    invoke-direct {p1, p0, v0, v1}, Lf/b/a/a/a$d;-><init>(Lf/b/a/a/a;Landroid/content/Context;I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AppCompatDialog;->supportRequestWindowFeature(I)Z

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v1, Lcom/google/android/material/R$style;->Animation_AppCompat_Dialog:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    :cond_0
    return-object p1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "inflater"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget v2, Lcom/lytefast/flexinput/R$f;->dialog_add_content_pager_with_fab:I

    const/4 v3, 0x0

    move-object/from16 v4, p2

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    new-instance v4, Lf/b/a/a/a$a;

    invoke-direct {v4, v3, v0}, Lf/b/a/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/lytefast/flexinput/R$e;->content_pager:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroidx/viewpager/widget/ViewPager;

    iput-object v4, v0, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    sget v4, Lcom/lytefast/flexinput/R$e;->content_tabs:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/material/tabs/TabLayout;

    iput-object v4, v0, Lf/b/a/a/a;->e:Lcom/google/android/material/tabs/TabLayout;

    sget v4, Lcom/lytefast/flexinput/R$e;->action_btn:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v4, v0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    sget v4, Lcom/lytefast/flexinput/R$e;->launch_btn:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lf/b/a/a/a;->g:Landroid/widget/ImageView;

    if-eqz v4, :cond_0

    new-instance v5, Lf/b/a/a/a$a;

    invoke-direct {v5, v2, v0}, Lf/b/a/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v4, v0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v5, "null cannot be cast to non-null type com.google.android.material.floatingactionbutton.FloatingActionButton"

    invoke-static {v4, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/lytefast/flexinput/R$c;->brand_500:I

    invoke-static {v5, v6}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    invoke-static {v5}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v4

    instance-of v5, v4, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    if-eqz v5, :cond_f

    new-instance v5, Lf/b/a/c/d;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v6

    const-string v7, "childFragmentManager"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v4

    check-cast v7, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object v8, v7, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->y:[Lf/b/a/c/d$a;

    if-eqz v8, :cond_e

    array-length v9, v8

    if-nez v9, :cond_2

    const/4 v9, 0x1

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    :goto_0
    const/4 v10, 0x2

    if-eqz v9, :cond_3

    const/4 v8, 0x3

    new-array v8, v8, [Lf/b/a/c/d$a;

    new-instance v9, Lf/b/a/c/a;

    sget v11, Lcom/lytefast/flexinput/R$d;->ic_image_24dp:I

    sget v12, Lcom/lytefast/flexinput/R$g;->attachment_photos:I

    invoke-direct {v9, v11, v12}, Lf/b/a/c/a;-><init>(II)V

    aput-object v9, v8, v3

    new-instance v9, Lf/b/a/c/b;

    sget v11, Lcom/lytefast/flexinput/R$d;->ic_file_24dp:I

    sget v12, Lcom/lytefast/flexinput/R$g;->attachment_files:I

    invoke-direct {v9, v11, v12}, Lf/b/a/c/b;-><init>(II)V

    aput-object v9, v8, v2

    new-instance v9, Lf/b/a/c/c;

    sget v11, Lcom/lytefast/flexinput/R$d;->ic_add_a_photo_24dp:I

    sget v12, Lcom/lytefast/flexinput/R$g;->camera:I

    invoke-direct {v9, v11, v12}, Lf/b/a/c/c;-><init>(II)V

    aput-object v9, v8, v10

    :cond_3
    array-length v9, v8

    invoke-static {v8, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lf/b/a/c/d$a;

    invoke-direct {v5, v6, v8}, Lf/b/a/c/d;-><init>(Landroidx/fragment/app/FragmentManager;[Lf/b/a/c/d$a;)V

    const-string v6, "pagerAdapter"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    if-eqz v6, :cond_c

    iget-object v8, v0, Lf/b/a/a/a;->e:Lcom/google/android/material/tabs/TabLayout;

    if-eqz v8, :cond_c

    const-string v9, "context"

    invoke-static {v6, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, v9}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "tabLayout"

    invoke-static {v8, v9}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget v9, Lcom/lytefast/flexinput/R$c;->tab_color_selector:I

    invoke-static {v6, v9}, Landroidx/appcompat/content/res/AppCompatResources;->getColorStateList(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v8}, Lcom/google/android/material/tabs/TabLayout;->getTabCount()I

    move-result v9

    const/4 v11, 0x0

    :goto_1
    const-string v12, "iconColors"

    if-ge v11, v9, :cond_5

    invoke-virtual {v8, v11}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v13

    if-eqz v13, :cond_4

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/material/tabs/TabLayout$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v12

    if-eqz v12, :cond_4

    invoke-static {v12}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-static {v12, v6}, Landroidx/core/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    invoke-virtual {v13, v12}, Lcom/google/android/material/tabs/TabLayout$Tab;->setIcon(Landroid/graphics/drawable/Drawable;)Lcom/google/android/material/tabs/TabLayout$Tab;

    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_5
    iget-object v9, v5, Lf/b/a/c/d;->a:[Lf/b/a/c/d$a;

    new-instance v11, Ljava/util/ArrayList;

    array-length v13, v9

    invoke-direct {v11, v13}, Ljava/util/ArrayList;-><init>(I)V

    array-length v13, v9

    :goto_2
    if-ge v3, v13, :cond_7

    aget-object v14, v9, v3

    invoke-virtual {v8}, Lcom/google/android/material/tabs/TabLayout;->newTab()Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v15

    invoke-virtual {v14}, Lf/b/a/c/d$a;->getIcon()I

    move-result v10

    invoke-virtual {v15, v10}, Lcom/google/android/material/tabs/TabLayout$Tab;->setIcon(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v10

    const-string v15, "tabLayout.newTab()\n              .setIcon(it.icon)"

    invoke-static {v10, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/google/android/material/tabs/TabLayout$Tab;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    if-eqz v15, :cond_6

    invoke-static {v15}, Landroidx/core/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    invoke-static {v15, v6}, Landroidx/core/graphics/drawable/DrawableCompat;->setTintList(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V

    invoke-virtual {v10, v15}, Lcom/google/android/material/tabs/TabLayout$Tab;->setIcon(Landroid/graphics/drawable/Drawable;)Lcom/google/android/material/tabs/TabLayout$Tab;

    :cond_6
    invoke-virtual {v14}, Lf/b/a/c/d$a;->getContentDesc()I

    move-result v14

    invoke-virtual {v10, v14}, Lcom/google/android/material/tabs/TabLayout$Tab;->setContentDescription(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    const/4 v10, 0x2

    goto :goto_2

    :cond_7
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/material/tabs/TabLayout$Tab;

    invoke-virtual {v8, v6}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    goto :goto_3

    :cond_8
    iget-object v3, v0, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v3, :cond_9

    invoke-virtual {v3, v5}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    :cond_9
    iget-object v3, v0, Lf/b/a/a/a;->e:Lcom/google/android/material/tabs/TabLayout;

    if-eqz v3, :cond_a

    new-instance v5, Lf/b/a/a/b;

    invoke-direct {v5, v0}, Lf/b/a/a/b;-><init>(Lf/b/a/a/a;)V

    invoke-virtual {v3, v5}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    :cond_a
    iget-object v3, v0, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v3, :cond_b

    new-instance v5, Lf/b/a/a/c;

    invoke-direct {v5, v0}, Lf/b/a/a/c;-><init>(Lf/b/a/a/a;)V

    invoke-virtual {v3, v5}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    :cond_b
    iget-object v3, v0, Lf/b/a/a/a;->e:Lcom/google/android/material/tabs/TabLayout;

    if-eqz v3, :cond_c

    invoke-virtual {v3, v2}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lcom/google/android/material/tabs/TabLayout$Tab;->select()V

    :cond_c
    iget-object v2, v0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v2, :cond_d

    new-instance v3, Lf/b/a/a/a$a;

    const/4 v5, 0x2

    invoke-direct {v3, v5, v4}, Lf/b/a/a/a$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_d
    invoke-virtual {v7}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v2

    iget-object v3, v0, Lf/b/a/a/a;->k:Lf/b/a/a/a$c;

    invoke-virtual {v2, v3}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->addItemSelectionListener(Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;)Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v2

    iput-object v2, v0, Lf/b/a/a/a;->h:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    iget-object v2, v0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const-string v3, "null cannot be cast to non-null type android.view.View"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v3, Lf/b/a/a/a$e;

    invoke-direct {v3, v0}, Lf/b/a/a/a$e;-><init>(Lf/b/a/a/a;)V

    invoke-static {v2, v3}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    goto :goto_4

    :cond_e
    const-string v1, "pageSuppliers"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    throw v1

    :cond_f
    :goto_4
    return-object v1
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Lf/b/a/a/a;->h:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/b/a/a/a;->k:Lf/b/a/a/a$c;

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->removeItemSelectionListener(Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;)V

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onDestroyView()V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lf/b/a/a/a;->i:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_0

    const-string v2, "it"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    :cond_0
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    iget-object v0, p0, Lf/b/a/a/a;->f:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_0

    new-instance v1, Lf/b/a/a/a$f;

    invoke-direct {v1, p0}, Lf/b/a/a/a$f;-><init>(Lf/b/a/a/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 4

    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "it"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/google/android/material/R$anim;->design_bottom_sheet_slide_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    const-string v2, "animation"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/material/R$integer;->bottom_sheet_slide_duration:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const v2, 0x10a0004

    invoke-virtual {v1, v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    iget-object v0, p0, Lf/b/a/a/a;->e:Lcom/google/android/material/tabs/TabLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget-object v0, p0, Lf/b/a/a/a;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    return-void
.end method

.method public show(Landroidx/fragment/app/FragmentTransaction;Ljava/lang/String;)I
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PrivateResource"
        }
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/google/android/material/R$anim;->abc_grow_fade_in_from_bottom:I

    sget v1, Lcom/google/android/material/R$anim;->abc_shrink_fade_out_from_bottom:I

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/FragmentTransaction;->setCustomAnimations(II)Landroidx/fragment/app/FragmentTransaction;

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentTransaction;Ljava/lang/String;)I

    move-result p1

    return p1
.end method
