.class public final Lf/b/a/a/a$c;
.super Ljava/lang/Object;
.source "AddContentDialogFragment.kt"

# interfaces
.implements Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/b/a/a/a;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener<",
        "Lcom/lytefast/flexinput/model/Attachment<",
        "*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf/b/a/a/a;


# direct methods
.method public constructor <init>(Lf/b/a/a/a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/b/a/a/a$c;->a:Lf/b/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/lytefast/flexinput/model/Attachment;

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/b/a/a/a$c;->a:Lf/b/a/a/a;

    invoke-static {p1}, Lf/b/a/a/a;->f(Lf/b/a/a/a;)V

    return-void
.end method

.method public onItemUnselected(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/lytefast/flexinput/model/Attachment;

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/b/a/a/a$c;->a:Lf/b/a/a/a;

    invoke-static {p1}, Lf/b/a/a/a;->f(Lf/b/a/a/a;)V

    return-void
.end method

.method public unregister()V
    .locals 0

    return-void
.end method
