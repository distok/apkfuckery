.class public final Lf/b/a/a/n;
.super Ljava/lang/Object;
.source "FlexInputFragment.kt"

# interfaces
.implements Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener<",
        "Lcom/lytefast/flexinput/model/Attachment<",
        "+",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/lytefast/flexinput/fragment/FlexInputFragment;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf/b/a/a/n;->a:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/lytefast/flexinput/model/Attachment;

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/b/a/a/n;->a:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object v0, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->getAttachments()Ljava/util/ArrayList;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onAttachmentsUpdated(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public onItemUnselected(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/lytefast/flexinput/model/Attachment;

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lf/b/a/a/n;->a:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object v0, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->getAttachments()Ljava/util/ArrayList;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onAttachmentsUpdated(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public unregister()V
    .locals 0

    return-void
.end method
