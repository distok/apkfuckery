.class public final Lf/b/a/a/l;
.super Lx/m/c/k;
.source "FlexInputFragment.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V
    .locals 0

    iput-object p1, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lf/b/a/a/l;->invoke(Z)Z

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1
.end method

.method public final invoke(Z)Z
    .locals 3

    iget-object v0, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->j(Lcom/lytefast/flexinput/fragment/FlexInputFragment;Z)V

    iget-object v0, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_1

    sget p1, Lcom/lytefast/flexinput/R$d;->ic_expression_icon_cutout_24dp:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/lytefast/flexinput/R$d;->ic_emoji_24dp:I

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object p1, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/widget/ImageView;

    move-result-object p1

    iget-object v0, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    sget v1, Lcom/lytefast/flexinput/R$g;->toggle_emoji_keyboard:I

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lf/b/a/a/l;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->g(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 p1, 0x1

    return p1

    :cond_2
    const-string p1, "expressionBtnBadge"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method
