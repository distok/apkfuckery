.class public final Lf/b/a/a/m$a;
.super Ljava/lang/Object;
.source "FlexInputFragment.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf/b/a/a/m;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lf/b/a/a/m;


# direct methods
.method public constructor <init>(Lf/b/a/a/m;)V
    .locals 0

    iput-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Ljava/lang/Long;

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    sget-object v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->E:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/view/ViewGroup;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->j(Lcom/lytefast/flexinput/fragment/FlexInputFragment;Z)V

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/widget/ImageView;

    move-result-object p1

    sget v0, Lcom/lytefast/flexinput/R$d;->ic_keyboard_24dp:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    iget-object p1, p1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->n:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/widget/ImageView;

    move-result-object p1

    iget-object v1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object v1, v1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    sget v2, Lcom/lytefast/flexinput/R$g;->show_keyboard:I

    invoke-virtual {v1, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lf/b/a/a/m$a;->d:Lf/b/a/a/m;

    iget-object p1, p1, Lf/b/a/a/m;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {p1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->g(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const-string p1, "expressionBtnBadge"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_1
    :goto_0
    return-void
.end method
