.class public final Lf/b/a/c/d;
.super Landroidx/fragment/app/FragmentStatePagerAdapter;
.source "AddContentPagerAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/b/a/c/d$a;
    }
.end annotation


# instance fields
.field public final a:[Lf/b/a/c/d$a;


# direct methods
.method public varargs constructor <init>(Landroidx/fragment/app/FragmentManager;[Lf/b/a/c/d$a;)V
    .locals 1

    const-string v0, "childFragmentManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pageSuppliers"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroidx/fragment/app/FragmentStatePagerAdapter;-><init>(Landroidx/fragment/app/FragmentManager;I)V

    iput-object p2, p0, Lf/b/a/c/d;->a:[Lf/b/a/c/d$a;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lf/b/a/c/d;->a:[Lf/b/a/c/d$a;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Landroidx/fragment/app/Fragment;
    .locals 1

    iget-object v0, p0, Lf/b/a/c/d;->a:[Lf/b/a/c/d$a;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lf/b/a/c/d$a;->createFragment()Landroidx/fragment/app/Fragment;

    move-result-object p1

    return-object p1
.end method
