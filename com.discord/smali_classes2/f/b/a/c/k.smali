.class public final Lf/b/a/c/k;
.super Lx/j/h/a/g;
.source "ThumbnailViewHolder.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Landroid/graphics/Bitmap;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.lytefast.flexinput.adapters.ThumbnailViewHolder$ThumbnailBitmapGenerator$getThumbnailQ$2"
    f = "ThumbnailViewHolder.kt"
    l = {}
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $contentResolver:Landroid/content/ContentResolver;

.field public final synthetic $uri:Landroid/net/Uri;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field public final synthetic this$0:Lf/b/a/c/i$b;


# direct methods
.method public constructor <init>(Lf/b/a/c/i$b;Landroid/content/ContentResolver;Landroid/net/Uri;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lf/b/a/c/k;->this$0:Lf/b/a/c/i$b;

    iput-object p2, p0, Lf/b/a/c/k;->$contentResolver:Landroid/content/ContentResolver;

    iput-object p3, p0, Lf/b/a/c/k;->$uri:Landroid/net/Uri;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/b/a/c/k;

    iget-object v1, p0, Lf/b/a/c/k;->this$0:Lf/b/a/c/i$b;

    iget-object v2, p0, Lf/b/a/c/k;->$contentResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lf/b/a/c/k;->$uri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2, v3, p2}, Lf/b/a/c/k;-><init>(Lf/b/a/c/i$b;Landroid/content/ContentResolver;Landroid/net/Uri;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lf/b/a/c/k;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p2, Lkotlin/coroutines/Continuation;

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/b/a/c/k;

    iget-object v1, p0, Lf/b/a/c/k;->this$0:Lf/b/a/c/i$b;

    iget-object v2, p0, Lf/b/a/c/k;->$contentResolver:Landroid/content/ContentResolver;

    iget-object v3, p0, Lf/b/a/c/k;->$uri:Landroid/net/Uri;

    invoke-direct {v0, v1, v2, v3, p2}, Lf/b/a/c/k;-><init>(Lf/b/a/c/i$b;Landroid/content/ContentResolver;Landroid/net/Uri;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lf/b/a/c/k;->p$:Lkotlinx/coroutines/CoroutineScope;

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {v0, p1}, Lf/b/a/c/k;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget v0, p0, Lf/b/a/c/k;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    :try_start_0
    iget-object p1, p0, Lf/b/a/c/k;->$contentResolver:Landroid/content/ContentResolver;

    iget-object v0, p0, Lf/b/a/c/k;->$uri:Landroid/net/Uri;

    new-instance v1, Landroid/util/Size;

    sget v2, Lf/b/a/c/i;->f:I

    sget v3, Lf/b/a/c/i;->g:I

    invoke-direct {v1, v2, v3}, Landroid/util/Size;-><init>(II)V

    iget-object v2, p0, Lf/b/a/c/k;->this$0:Lf/b/a/c/i$b;

    iget-object v2, v2, Lf/b/a/c/i$b;->b:Landroid/os/CancellationSignal;

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/ContentResolver;->loadThumbnail(Landroid/net/Uri;Landroid/util/Size;Landroid/os/CancellationSignal;)Landroid/graphics/Bitmap;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    :goto_0
    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
