.class public final Lf/b/a/c/j;
.super Lx/j/h/a/g;
.source "ThumbnailViewHolder.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.lytefast.flexinput.adapters.ThumbnailViewHolder$ThumbnailBitmapGenerator$generate$1"
    f = "ThumbnailViewHolder.kt"
    l = {
        0x67
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $onGenerate:Lkotlin/jvm/functions/Function1;

.field public final synthetic $uri:Landroid/net/Uri;

.field public L$0:Ljava/lang/Object;

.field public L$1:Ljava/lang/Object;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field public final synthetic this$0:Lf/b/a/c/i$b;


# direct methods
.method public constructor <init>(Lf/b/a/c/i$b;Landroid/net/Uri;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lf/b/a/c/j;->this$0:Lf/b/a/c/i$b;

    iput-object p2, p0, Lf/b/a/c/j;->$uri:Landroid/net/Uri;

    iput-object p3, p0, Lf/b/a/c/j;->$onGenerate:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/b/a/c/j;

    iget-object v1, p0, Lf/b/a/c/j;->this$0:Lf/b/a/c/i$b;

    iget-object v2, p0, Lf/b/a/c/j;->$uri:Landroid/net/Uri;

    iget-object v3, p0, Lf/b/a/c/j;->$onGenerate:Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1, v2, v3, p2}, Lf/b/a/c/j;-><init>(Lf/b/a/c/i$b;Landroid/net/Uri;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lf/b/a/c/j;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p2, Lkotlin/coroutines/Continuation;

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/b/a/c/j;

    iget-object v1, p0, Lf/b/a/c/j;->this$0:Lf/b/a/c/i$b;

    iget-object v2, p0, Lf/b/a/c/j;->$uri:Landroid/net/Uri;

    iget-object v3, p0, Lf/b/a/c/j;->$onGenerate:Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1, v2, v3, p2}, Lf/b/a/c/j;-><init>(Lf/b/a/c/i$b;Landroid/net/Uri;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lf/b/a/c/j;->p$:Lkotlinx/coroutines/CoroutineScope;

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {v0, p1}, Lf/b/a/c/j;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    sget-object v0, Lx/j/g/a;->d:Lx/j/g/a;

    iget v1, p0, Lf/b/a/c/j;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lf/b/a/c/j;->L$1:Ljava/lang/Object;

    check-cast v0, Lf/b/a/c/i$b;

    iget-object v1, p0, Lf/b/a/c/j;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lf/b/a/c/j;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object v1, p0, Lf/b/a/c/j;->this$0:Lf/b/a/c/i$b;

    new-instance v3, Landroid/os/CancellationSignal;

    invoke-direct {v3}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v3, v1, Lf/b/a/c/i$b;->b:Landroid/os/CancellationSignal;

    iget-object v1, p0, Lf/b/a/c/j;->this$0:Lf/b/a/c/i$b;

    iget-object v3, v1, Lf/b/a/c/i$b;->d:Landroid/content/ContentResolver;

    iget-object v4, p0, Lf/b/a/c/j;->$uri:Landroid/net/Uri;

    iput-object p1, p0, Lf/b/a/c/j;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lf/b/a/c/j;->L$1:Ljava/lang/Object;

    iput v2, p0, Lf/b/a/c/j;->label:I

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Ly/a/h0;->b:Ly/a/v;

    new-instance v2, Lf/b/a/c/k;

    const/4 v5, 0x0

    invoke-direct {v2, v1, v3, v4, v5}, Lf/b/a/c/k;-><init>(Lf/b/a/c/i$b;Landroid/content/ContentResolver;Landroid/net/Uri;Lkotlin/coroutines/Continuation;)V

    invoke-static {p1, v2, p0}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    move-object v0, v1

    :goto_0
    check-cast p1, Landroid/graphics/Bitmap;

    iput-object p1, v0, Lf/b/a/c/i$b;->c:Landroid/graphics/Bitmap;

    iget-object p1, p0, Lf/b/a/c/j;->$onGenerate:Lkotlin/jvm/functions/Function1;

    iget-object v0, p0, Lf/b/a/c/j;->this$0:Lf/b/a/c/i$b;

    iget-object v0, v0, Lf/b/a/c/i$b;->c:Landroid/graphics/Bitmap;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
