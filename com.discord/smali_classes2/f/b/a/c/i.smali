.class public abstract Lf/b/a/c/i;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ThumbnailViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/b/a/c/i$b;,
        Lf/b/a/c/i$c;,
        Lf/b/a/c/i$a;
    }
.end annotation


# static fields
.field public static f:I

.field public static g:I

.field public static h:Landroid/graphics/drawable/ColorDrawable;

.field public static final i:Lf/b/a/c/i$a;


# instance fields
.field public d:Lf/b/a/c/i$b;

.field public e:Lf/b/a/c/i$c;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf/b/a/c/i$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf/b/a/c/i$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lf/b/a/c/i;->i:Lf/b/a/c/i$a;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "itemView.context.contentResolver"

    const-string v2, "itemView.context"

    if-eqz v0, :cond_1

    new-instance v0, Lf/b/a/c/i$b;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lf/b/a/c/i$b;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lf/b/a/c/i;->d:Lf/b/a/c/i$b;

    goto :goto_1

    :cond_1
    new-instance v0, Lf/b/a/c/i$c;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lf/b/a/c/i$c;-><init>(Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lf/b/a/c/i;->e:Lf/b/a/c/i$c;

    :goto_1
    return-void
.end method


# virtual methods
.method public abstract a()Lcom/facebook/drawee/view/SimpleDraweeView;
.end method

.method public final b()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lf/b/a/c/i;->d:Lf/b/a/c/i$b;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/b/a/c/i$b;->a()V

    :cond_1
    return-void
.end method

.method public final c(Lcom/lytefast/flexinput/model/Photo;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 11

    sget-object v0, Lf/b/a/c/i;->h:Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v2, "itemView"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget v2, Lcom/lytefast/flexinput/R$b;->flexInputDialogBackground:I

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    sput-object v0, Lf/b/a/c/i;->h:Landroid/graphics/drawable/ColorDrawable;

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    sput p2, Lf/b/a/c/i;->f:I

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    sput p2, Lf/b/a/c/i;->g:I

    :cond_2
    if-nez p1, :cond_3

    return-void

    :cond_3
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x1d

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-lt p2, p3, :cond_4

    const/4 p2, 0x1

    goto :goto_0

    :cond_4
    const/4 p2, 0x0

    :goto_0
    const-string p3, "uri"

    const/4 v2, 0x0

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lf/b/a/c/i;->a()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object p2

    check-cast p2, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    sget-object v1, Lf/b/a/c/i;->h:Landroid/graphics/drawable/ColorDrawable;

    sget-object v3, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v3, Lf/g/g/e/r;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {p2, v0, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->o(ILandroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->m(I)Lf/g/g/e/p;

    move-result-object p2

    invoke-virtual {p2, v3}, Lf/g/g/e/p;->r(Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V

    iget-object p2, p0, Lf/b/a/c/i;->d:Lf/b/a/c/i$b;

    if-eqz p2, :cond_a

    invoke-virtual {p1}, Lcom/lytefast/flexinput/model/Attachment;->getUri()Landroid/net/Uri;

    move-result-object p1

    new-instance v0, Lf/b/a/c/i$d;

    invoke-direct {v0, p0}, Lf/b/a/c/i$d;-><init>(Lf/b/a/c/i;)V

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "onGenerate"

    invoke-static {v0, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lf/b/a/c/i$b;->a()V

    sget-object v3, Ly/a/r0;->d:Ly/a/r0;

    sget-object p3, Ly/a/h0;->a:Ly/a/v;

    sget-object v4, Ly/a/s1/j;->b:Ly/a/e1;

    const/4 v5, 0x0

    new-instance v6, Lf/b/a/c/j;

    invoke-direct {v6, p2, p1, v0, v2}, Lf/b/a/c/j;-><init>(Lf/b/a/c/i$b;Landroid/net/Uri;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lf/h/a/f/f/n/g;->M(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Ly/a/x;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    move-result-object p1

    iput-object p1, p2, Lf/b/a/c/i$b;->a:Lkotlinx/coroutines/Job;

    goto/16 :goto_3

    :cond_5
    iget-object p2, p0, Lf/b/a/c/i;->e:Lf/b/a/c/i$c;

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Lcom/lytefast/flexinput/model/Attachment;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/lytefast/flexinput/model/Attachment;->getUri()Landroid/net/Uri;

    move-result-object p1

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lf/b/a/c/m;

    invoke-direct {p3, v3, v4}, Lf/b/a/c/m;-><init>(J)V

    iget-object v5, p2, Lf/b/a/c/i$c;->a:Landroid/content/ContentResolver;

    sget-object v6, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v7, "_id"

    filled-new-array {v7}, [Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v9, v8, [Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v0

    const/4 v10, 0x0

    const-string v8, "image_id = ? AND KIND = ?"

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_6

    goto :goto_1

    :cond_6
    :try_start_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p1

    sget-object p3, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {p3, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    goto :goto_2

    :catchall_0
    move-exception p1

    :try_start_1
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p2

    invoke-static {v0, p1}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p2

    :cond_7
    :goto_1
    iget-object p2, p2, Lf/b/a/c/i$c;->a:Landroid/content/ContentResolver;

    const-string v1, "contentResolver"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lf/b/a/c/l;

    invoke-direct {v1, p3, p2}, Lf/b/a/c/l;-><init>(Lf/b/a/c/m;Landroid/content/ContentResolver;)V

    invoke-static {v1}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    if-eqz v0, :cond_8

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_8
    :goto_2
    move-object v2, p1

    :cond_9
    invoke-virtual {p0}, Lf/b/a/c/i;->a()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-virtual {p0}, Lf/b/a/c/i;->a()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Landroid/net/Uri;Ljava/lang/Object;)V

    :cond_a
    :goto_3
    return-void
.end method
