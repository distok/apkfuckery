.class public final Lf/b/a/c/h;
.super Lx/m/c/k;
.source "FileListAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/io/File;",
        "Lkotlin/sequences/Sequence<",
        "+",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final d:Lf/b/a/c/h;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/b/a/c/h;

    invoke-direct {v0}, Lf/b/a/c/h;-><init>()V

    sput-object v0, Lf/b/a/c/h;->d:Lf/b/a/c/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lkotlin/sequences/Sequence<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getFileList"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->asSequence([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lx/r/f;->a:Lx/r/f;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lf/b/a/c/h;->a(Ljava/io/File;)Lkotlin/sequences/Sequence;

    move-result-object p1

    return-object p1
.end method
