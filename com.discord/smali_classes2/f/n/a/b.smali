.class public Lf/n/a/b;
.super Ljava/lang/Object;
.source "UCropActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/yalantis/ucrop/UCropActivity;


# direct methods
.method public constructor <init>(Lcom/yalantis/ucrop/UCropActivity;)V
    .locals 0

    iput-object p1, p0, Lf/n/a/b;->d:Lcom/yalantis/ucrop/UCropActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lf/n/a/b;->d:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->g:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    iget v3, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->i:F

    iget v4, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->j:F

    iput v4, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->i:F

    iput v3, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->j:F

    div-float/2addr v4, v3

    iput v4, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->g:F

    :cond_0
    invoke-virtual {v1}, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->b()V

    :cond_1
    iget v1, v1, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->g:F

    invoke-virtual {v0, v1}, Lf/n/a/l/a;->setTargetAspectRatio(F)V

    iget-object v0, p0, Lf/n/a/b;->d:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lf/n/a/l/a;->setImageToWrapCropBounds(Z)V

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lf/n/a/b;->d:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-ne v3, p1, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setSelected(Z)V

    goto :goto_0

    :cond_3
    return-void
.end method
