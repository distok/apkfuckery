.class public Lf/n/a/f;
.super Ljava/lang/Object;
.source "UCropActivity.java"

# interfaces
.implements Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView$a;


# instance fields
.field public final synthetic a:Lcom/yalantis/ucrop/UCropActivity;


# direct methods
.method public constructor <init>(Lcom/yalantis/ucrop/UCropActivity;)V
    .locals 0

    iput-object p1, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lf/n/a/l/a;->setImageToWrapCropBounds(Z)V

    return-void
.end method

.method public b(FF)V
    .locals 4

    const p2, 0x466a6000    # 15000.0f

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v1

    iget-object v2, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v2, v2, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v2}, Lf/n/a/l/a;->getMaxScale()F

    move-result v2

    iget-object v3, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v3, v3, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v3}, Lf/n/a/l/a;->getMinScale()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, p2

    mul-float v2, v2, p1

    add-float/2addr v2, v1

    iget-object p1, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result p1

    iget-object p2, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result p2

    invoke-virtual {v0, v2, p1, p2}, Lf/n/a/l/a;->l(FFF)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v1

    iget-object v2, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v2, v2, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v2}, Lf/n/a/l/a;->getMaxScale()F

    move-result v2

    iget-object v3, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v3, v3, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v3}, Lf/n/a/l/a;->getMinScale()F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, p2

    mul-float v2, v2, p1

    add-float/2addr v2, v1

    iget-object p1, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result p1

    iget-object p2, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result p2

    invoke-virtual {v0}, Lf/n/a/l/a;->getMinScale()F

    move-result v1

    cmpl-float v1, v2, v1

    if-ltz v1, :cond_1

    invoke-virtual {v0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v1

    div-float/2addr v2, v1

    invoke-virtual {v0, v2, p1, p2}, Lf/n/a/l/a;->f(FFF)V

    :cond_1
    :goto_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lf/n/a/f;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, v0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v0}, Lf/n/a/l/a;->i()V

    return-void
.end method
