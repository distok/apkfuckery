.class public Lf/n/a/j/a;
.super Landroid/os/AsyncTask;
.source "BitmapCropTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/graphics/Bitmap;

.field public final c:Landroid/graphics/RectF;

.field public final d:Landroid/graphics/RectF;

.field public e:F

.field public f:F

.field public final g:I

.field public final h:I

.field public final i:Landroid/graphics/Bitmap$CompressFormat;

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Lf/n/a/h/a;

.field public n:I

.field public o:I

.field public p:I

.field public q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Lf/n/a/i/c;Lf/n/a/i/a;Lf/n/a/h/a;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Bitmap;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lf/n/a/i/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lf/n/a/i/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lf/n/a/h/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/n/a/j/a;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    iget-object p1, p3, Lf/n/a/i/c;->a:Landroid/graphics/RectF;

    iput-object p1, p0, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget-object p1, p3, Lf/n/a/i/c;->b:Landroid/graphics/RectF;

    iput-object p1, p0, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget p1, p3, Lf/n/a/i/c;->c:F

    iput p1, p0, Lf/n/a/j/a;->e:F

    iget p1, p3, Lf/n/a/i/c;->d:F

    iput p1, p0, Lf/n/a/j/a;->f:F

    iget p1, p4, Lf/n/a/i/a;->a:I

    iput p1, p0, Lf/n/a/j/a;->g:I

    iget p1, p4, Lf/n/a/i/a;->b:I

    iput p1, p0, Lf/n/a/j/a;->h:I

    iget-object p1, p4, Lf/n/a/i/a;->c:Landroid/graphics/Bitmap$CompressFormat;

    iput-object p1, p0, Lf/n/a/j/a;->i:Landroid/graphics/Bitmap$CompressFormat;

    iget p1, p4, Lf/n/a/i/a;->d:I

    iput p1, p0, Lf/n/a/j/a;->j:I

    iget-object p1, p4, Lf/n/a/i/a;->e:Ljava/lang/String;

    iput-object p1, p0, Lf/n/a/j/a;->k:Ljava/lang/String;

    iget-object p1, p4, Lf/n/a/i/a;->f:Ljava/lang/String;

    iput-object p1, p0, Lf/n/a/j/a;->l:Ljava/lang/String;

    iput-object p5, p0, Lf/n/a/j/a;->m:Lf/n/a/h/a;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget v0, v1, Lf/n/a/j/a;->g:I

    const/4 v2, 0x0

    if-lez v0, :cond_2

    iget v0, v1, Lf/n/a/j/a;->h:I

    if-lez v0, :cond_2

    iget-object v0, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v3, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v0, v3

    iget-object v3, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget v4, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v3, v4

    iget v4, v1, Lf/n/a/j/a;->g:I

    int-to-float v5, v4

    cmpl-float v5, v0, v5

    if-gtz v5, :cond_0

    iget v5, v1, Lf/n/a/j/a;->h:I

    int-to-float v5, v5

    cmpl-float v5, v3, v5

    if-lez v5, :cond_2

    :cond_0
    int-to-float v4, v4

    div-float/2addr v4, v0

    iget v0, v1, Lf/n/a/j/a;->h:I

    int-to-float v0, v0

    div-float/2addr v0, v3

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget-object v3, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget-object v5, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v3, v4, v5, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v4, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    if-eq v4, v3, :cond_1

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    iput-object v3, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    iget v3, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v3, v0

    iput v3, v1, Lf/n/a/j/a;->e:F

    :cond_2
    iget v0, v1, Lf/n/a/j/a;->f:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_4

    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    iget v0, v1, Lf/n/a/j/a;->f:F

    iget-object v3, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v8, v0, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v3, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v0, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    if-eq v3, v0, :cond_3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    iput-object v0, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    :cond_4
    iget-object v0, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v3, v1, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v3

    iget v3, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Lf/n/a/j/a;->p:I

    iget-object v0, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v3, v1, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v3

    iget v3, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Lf/n/a/j/a;->q:I

    iget-object v0, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v3, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Lf/n/a/j/a;->n:I

    iget-object v0, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget v3, v1, Lf/n/a/j/a;->e:F

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Lf/n/a/j/a;->o:I

    iget v3, v1, Lf/n/a/j/a;->n:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/4 v3, 0x1

    add-int/2addr v0, v3

    iget v4, v1, Lf/n/a/j/a;->g:I

    if-lez v4, :cond_5

    iget v4, v1, Lf/n/a/j/a;->h:I

    if-gtz v4, :cond_7

    :cond_5
    iget-object v4, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, v1, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    int-to-float v0, v0

    cmpl-float v4, v4, v0

    if-gtz v4, :cond_7

    iget-object v4, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, v1, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v0

    if-gtz v4, :cond_7

    iget-object v4, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v5, v1, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v0

    if-gtz v4, :cond_7

    iget-object v4, v1, Lf/n/a/j/a;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, v1, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v0, v4, v0

    if-lez v0, :cond_6

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    :goto_0
    const/4 v0, 0x1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Should crop: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "BitmapCropTask"

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    if-eqz v0, :cond_e

    new-instance v0, Landroid/media/ExifInterface;

    iget-object v5, v1, Lf/n/a/j/a;->k:Ljava/lang/String;

    invoke-direct {v0, v5}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    iget v6, v1, Lf/n/a/j/a;->p:I

    iget v7, v1, Lf/n/a/j/a;->q:I

    iget v8, v1, Lf/n/a/j/a;->n:I

    iget v9, v1, Lf/n/a/j/a;->o:I

    invoke-static {v5, v6, v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v5

    iget-object v6, v1, Lf/n/a/j/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    if-nez v6, :cond_8

    goto :goto_2

    :cond_8
    :try_start_0
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    new-instance v7, Ljava/io/File;

    iget-object v8, v1, Lf/n/a/j/a;->l:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v4

    iget-object v6, v1, Lf/n/a/j/a;->i:Landroid/graphics/Bitmap$CompressFormat;

    iget v7, v1, Lf/n/a/j/a;->j:I

    invoke-virtual {v5, v6, v7, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_9

    :try_start_1
    invoke-interface {v4}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_9
    :goto_2
    iget-object v4, v1, Lf/n/a/j/a;->i:Landroid/graphics/Bitmap$CompressFormat;

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v4, v5}, Landroid/graphics/Bitmap$CompressFormat;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    iget v4, v1, Lf/n/a/j/a;->n:I

    iget v5, v1, Lf/n/a/j/a;->o:I

    iget-object v6, v1, Lf/n/a/j/a;->l:Ljava/lang/String;

    sget-object v7, Lf/n/a/k/b;->b:[B

    const-string v8, "FNumber"

    const-string v9, "DateTime"

    const-string v10, "DateTimeDigitized"

    const-string v11, "ExposureTime"

    const-string v12, "Flash"

    const-string v13, "FocalLength"

    const-string v14, "GPSAltitude"

    const-string v15, "GPSAltitudeRef"

    const-string v16, "GPSDateStamp"

    const-string v17, "GPSLatitude"

    const-string v18, "GPSLatitudeRef"

    const-string v19, "GPSLongitude"

    const-string v20, "GPSLongitudeRef"

    const-string v21, "GPSProcessingMethod"

    const-string v22, "GPSTimeStamp"

    const-string v23, "ISOSpeedRatings"

    const-string v24, "Make"

    const-string v25, "Model"

    const-string v26, "SubSecTime"

    const-string v27, "SubSecTimeDigitized"

    const-string v28, "SubSecTimeOriginal"

    const-string v29, "WhiteBalance"

    filled-new-array/range {v8 .. v29}, [Ljava/lang/String;

    move-result-object v7

    :try_start_2
    new-instance v8, Landroid/media/ExifInterface;

    invoke-direct {v8, v6}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    :goto_3
    const/16 v6, 0x16

    if-ge v2, v6, :cond_b

    aget-object v6, v7, v2

    invoke-virtual {v0, v6}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a

    invoke-virtual {v8, v6, v9}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_b
    const-string v0, "ImageWidth"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ImageLength"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Orientation"

    const-string v2, "0"

    invoke-virtual {v8, v0, v2}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v2, "ImageHeaderParser"

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    :goto_4
    return v3

    :catchall_0
    move-exception v0

    if-eqz v4, :cond_d

    :try_start_3
    invoke-interface {v4}, Ljava/io/Closeable;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    :cond_d
    throw v0

    :cond_e
    iget-object v0, v1, Lf/n/a/j/a;->k:Ljava/lang/String;

    iget-object v3, v1, Lf/n/a/j/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    goto :goto_5

    :cond_f
    :try_start_4
    new-instance v5, Ljava/io/FileInputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v5}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    new-instance v0, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    const-wide/16 v8, 0x0

    :try_start_6
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    move-object v7, v5

    move-object v12, v3

    invoke-virtual/range {v7 .. v12}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V

    if-eqz v3, :cond_10

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    :cond_10
    :goto_5
    return v2

    :catchall_1
    move-exception v0

    move-object v4, v3

    goto :goto_6

    :catchall_2
    move-exception v0

    :goto_6
    move-object v2, v4

    move-object v4, v5

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v2, v4

    :goto_7
    if-eqz v4, :cond_11

    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V

    :cond_11
    if-eqz v2, :cond_12

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    :cond_12
    throw v0
.end method

.method public doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    check-cast p1, [Ljava/lang/Void;

    iget-object p1, p0, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;

    if-nez p1, :cond_0

    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "ViewBitmap is null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "ViewBitmap is recycled"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lf/n/a/j/a;->d:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "CurrentImageRect is empty"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lf/n/a/j/a;->a()Z

    const/4 p1, 0x0

    iput-object p1, p0, Lf/n/a/j/a;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :goto_0
    return-object p1
.end method

.method public onPostExecute(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    check-cast p1, Ljava/lang/Throwable;

    iget-object v0, p0, Lf/n/a/j/a;->m:Lf/n/a/h/a;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    new-instance p1, Ljava/io/File;

    iget-object v0, p0, Lf/n/a/j/a;->l:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    iget-object v0, p0, Lf/n/a/j/a;->m:Lf/n/a/h/a;

    iget v1, p0, Lf/n/a/j/a;->p:I

    iget v2, p0, Lf/n/a/j/a;->q:I

    iget v3, p0, Lf/n/a/j/a;->n:I

    iget v4, p0, Lf/n/a/j/a;->o:I

    check-cast v0, Lf/n/a/g;

    iget-object v5, v0, Lf/n/a/g;->a:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v6, v5, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v6}, Lf/n/a/l/a;->getTargetAspectRatio()F

    move-result v6

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-string v8, "com.yalantis.ucrop.OutputUri"

    invoke-virtual {v7, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object p1

    const-string v7, "com.yalantis.ucrop.CropAspectRatio"

    invoke-virtual {p1, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object p1

    const-string v6, "com.yalantis.ucrop.ImageWidth"

    invoke-virtual {p1, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p1

    const-string v3, "com.yalantis.ucrop.ImageHeight"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p1

    const-string v3, "com.yalantis.ucrop.OffsetX"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "com.yalantis.ucrop.OffsetY"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p1

    const/4 v1, -0x1

    invoke-virtual {v5, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object p1, v0, Lf/n/a/g;->a:Lcom/yalantis/ucrop/UCropActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    check-cast v0, Lf/n/a/g;

    iget-object v1, v0, Lf/n/a/g;->a:Lcom/yalantis/ucrop/UCropActivity;

    invoke-virtual {v1, p1}, Lcom/yalantis/ucrop/UCropActivity;->b(Ljava/lang/Throwable;)V

    iget-object p1, v0, Lf/n/a/g;->a:Lcom/yalantis/ucrop/UCropActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void
.end method
