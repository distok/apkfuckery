.class public Lf/n/a/d;
.super Ljava/lang/Object;
.source "UCropActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/yalantis/ucrop/UCropActivity;


# direct methods
.method public constructor <init>(Lcom/yalantis/ucrop/UCropActivity;)V
    .locals 0

    iput-object p1, p0, Lf/n/a/d;->d:Lcom/yalantis/ucrop/UCropActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lf/n/a/d;->d:Lcom/yalantis/ucrop/UCropActivity;

    iget-object v0, p1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lf/n/a/l/a;->k(F)V

    iget-object p1, p1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lf/n/a/l/a;->setImageToWrapCropBounds(Z)V

    return-void
.end method
