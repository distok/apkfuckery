.class public Lf/n/a/l/a;
.super Lf/n/a/l/c;
.source "CropImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf/n/a/l/a$b;,
        Lf/n/a/l/a$a;
    }
.end annotation


# instance fields
.field public A:F

.field public B:I

.field public C:I

.field public D:J

.field public final s:Landroid/graphics/RectF;

.field public final t:Landroid/graphics/Matrix;

.field public u:F

.field public v:F

.field public w:Lf/n/a/h/c;

.field public x:Ljava/lang/Runnable;

.field public y:Ljava/lang/Runnable;

.field public z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lf/n/a/l/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lf/n/a/l/c;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    const/high16 p1, 0x41200000    # 10.0f

    iput p1, p0, Lf/n/a/l/a;->v:F

    const/4 p1, 0x0

    iput-object p1, p0, Lf/n/a/l/a;->y:Ljava/lang/Runnable;

    const/4 p1, 0x0

    iput p1, p0, Lf/n/a/l/a;->B:I

    iput p1, p0, Lf/n/a/l/a;->C:I

    const-wide/16 p1, 0x1f4

    iput-wide p1, p0, Lf/n/a/l/a;->D:J

    return-void
.end method


# virtual methods
.method public d()V
    .locals 8

    invoke-super {p0}, Lf/n/a/l/c;->d()V

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lf/n/a/l/a;->u:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    div-float v2, v1, v0

    iput v2, p0, Lf/n/a/l/a;->u:F

    :cond_1
    iget v2, p0, Lf/n/a/l/c;->h:I

    int-to-float v4, v2

    iget v5, p0, Lf/n/a/l/a;->u:F

    div-float v6, v4, v5

    float-to-int v6, v6

    iget v7, p0, Lf/n/a/l/c;->i:I

    if-le v6, v7, :cond_2

    int-to-float v4, v7

    mul-float v5, v5, v4

    float-to-int v5, v5

    sub-int/2addr v2, v5

    div-int/lit8 v2, v2, 0x2

    iget-object v6, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    int-to-float v7, v2

    add-int/2addr v5, v2

    int-to-float v2, v5

    invoke-virtual {v6, v7, v3, v2, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    :cond_2
    sub-int/2addr v7, v6

    div-int/lit8 v7, v7, 0x2

    iget-object v2, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    int-to-float v5, v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v2, v3, v5, v4, v6}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_0
    invoke-virtual {p0, v1, v0}, Lf/n/a/l/a;->h(FF)V

    iget-object v2, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v1

    iget-object v5, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v5, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    mul-float v1, v1, v4

    sub-float/2addr v2, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v2, v1

    iget-object v5, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    iget v6, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v6

    mul-float v0, v0, v4

    sub-float/2addr v3, v0

    div-float/2addr v3, v1

    iget v0, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v0

    iget-object v0, p0, Lf/n/a/l/c;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lf/n/a/l/c;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v0, p0, Lf/n/a/l/c;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lf/n/a/l/c;->g:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lf/n/a/l/c;->setImageMatrix(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lf/n/a/l/a;->w:Lf/n/a/h/c;

    if-eqz v0, :cond_3

    iget v1, p0, Lf/n/a/l/a;->u:F

    check-cast v0, Lf/n/a/l/d;

    iget-object v0, v0, Lf/n/a/l/d;->a:Lcom/yalantis/ucrop/view/UCropView;

    iget-object v0, v0, Lcom/yalantis/ucrop/view/UCropView;->e:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual {v0, v1}, Lcom/yalantis/ucrop/view/OverlayView;->setTargetAspectRatio(F)V

    :cond_3
    iget-object v0, p0, Lf/n/a/l/c;->j:Lf/n/a/l/c$a;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v1

    check-cast v0, Lcom/yalantis/ucrop/UCropActivity$a;

    invoke-virtual {v0, v1}, Lcom/yalantis/ucrop/UCropActivity$a;->b(F)V

    iget-object v0, p0, Lf/n/a/l/c;->j:Lf/n/a/l/c$a;

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v1

    check-cast v0, Lcom/yalantis/ucrop/UCropActivity$a;

    invoke-virtual {v0, v1}, Lcom/yalantis/ucrop/UCropActivity$a;->a(F)V

    :cond_4
    return-void
.end method

.method public f(FFF)V
    .locals 3

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v0

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v1

    mul-float v1, v1, p1

    invoke-virtual {p0}, Lf/n/a/l/a;->getMaxScale()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-super {p0, p1, p2, p3}, Lf/n/a/l/c;->f(FFF)V

    goto :goto_0

    :cond_0
    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v0

    mul-float v0, v0, p1

    invoke-virtual {p0}, Lf/n/a/l/a;->getMinScale()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-super {p0, p1, p2, p3}, Lf/n/a/l/c;->f(FFF)V

    :cond_1
    :goto_0
    return-void
.end method

.method public getCropBoundsChangeListener()Lf/n/a/h/c;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lf/n/a/l/a;->w:Lf/n/a/h/c;

    return-object v0
.end method

.method public getMaxScale()F
    .locals 1

    iget v0, p0, Lf/n/a/l/a;->z:F

    return v0
.end method

.method public getMinScale()F
    .locals 1

    iget v0, p0, Lf/n/a/l/a;->A:F

    return v0
.end method

.method public getTargetAspectRatio()F
    .locals 1

    iget v0, p0, Lf/n/a/l/a;->u:F

    return v0
.end method

.method public final h(FF)V
    .locals 2

    iget-object v0, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v0, p1

    iget-object v1, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget-object v1, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v1, p2

    iget-object p2, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result p2

    div-float/2addr p2, p1

    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    iput p1, p0, Lf/n/a/l/a;->A:F

    iget p2, p0, Lf/n/a/l/a;->v:F

    mul-float p1, p1, p2

    iput p1, p0, Lf/n/a/l/a;->z:F

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lf/n/a/l/a;->x:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lf/n/a/l/a;->y:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public j([F)Z
    .locals 2

    iget-object v0, p0, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setRotate(F)V

    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object p1

    iget-object v0, p0, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    iget-object v0, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->x(Landroid/graphics/RectF;)[F

    move-result-object v0

    iget-object v1, p0, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-static {p1}, Lf/h/a/f/f/n/g;->e0([F)Landroid/graphics/RectF;

    move-result-object p1

    invoke-static {v0}, Lf/h/a/f/f/n/g;->e0([F)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result p1

    return p1
.end method

.method public k(F)V
    .locals 2

    iget-object v0, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lf/n/a/l/c;->e(FFF)V

    return-void
.end method

.method public l(FFF)V
    .locals 1

    invoke-virtual {p0}, Lf/n/a/l/a;->getMaxScale()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v0

    div-float/2addr p1, v0

    invoke-virtual {p0, p1, p2, p3}, Lf/n/a/l/a;->f(FFF)V

    :cond_0
    return-void
.end method

.method public setCropBoundsChangeListener(Lf/n/a/h/c;)V
    .locals 0
    .param p1    # Lf/n/a/h/c;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lf/n/a/l/a;->w:Lf/n/a/h/c;

    return-void
.end method

.method public setCropRect(Landroid/graphics/RectF;)V
    .locals 5

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lf/n/a/l/a;->u:F

    iget-object v0, p0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p0}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget p1, p1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr p1, v4

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {p0, v0, p1}, Lf/n/a/l/a;->h(FF)V

    :goto_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lf/n/a/l/a;->setImageToWrapCropBounds(Z)V

    return-void
.end method

.method public setImageToWrapCropBounds(Z)V
    .locals 20

    move-object/from16 v11, p0

    iget-boolean v0, v11, Lf/n/a/l/c;->n:Z

    if-eqz v0, :cond_6

    iget-object v0, v11, Lf/n/a/l/c;->d:[F

    invoke-virtual {v11, v0}, Lf/n/a/l/a;->j([F)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v11, Lf/n/a/l/c;->e:[F

    const/4 v1, 0x0

    aget v4, v0, v1

    const/4 v2, 0x1

    aget v5, v0, v2

    invoke-virtual/range {p0 .. p0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v8

    iget-object v0, v11, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    sub-float/2addr v0, v4

    iget-object v3, v11, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    sub-float/2addr v3, v5

    iget-object v6, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v6}, Landroid/graphics/Matrix;->reset()V

    iget-object v6, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v6, v0, v3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v6, v11, Lf/n/a/l/c;->d:[F

    array-length v7, v6

    invoke-static {v6, v7}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v6

    iget-object v7, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v7, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-virtual {v11, v6}, Lf/n/a/l/a;->j([F)Z

    move-result v10

    const/4 v6, 0x2

    const/4 v7, 0x4

    const/4 v12, 0x0

    if-eqz v10, :cond_4

    iget-object v0, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual/range {p0 .. p0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    iget-object v0, v11, Lf/n/a/l/c;->d:[F

    array-length v3, v0

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    iget-object v3, v11, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-static {v3}, Lf/h/a/f/f/n/g;->x(Landroid/graphics/RectF;)[F

    move-result-object v3

    iget-object v13, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v13, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    iget-object v13, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v13, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->e0([F)Landroid/graphics/RectF;

    move-result-object v0

    invoke-static {v3}, Lf/h/a/f/f/n/g;->e0([F)Landroid/graphics/RectF;

    move-result-object v3

    iget v13, v0, Landroid/graphics/RectF;->left:F

    iget v14, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v13, v14

    iget v14, v0, Landroid/graphics/RectF;->top:F

    iget v15, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v14, v15

    iget v15, v0, Landroid/graphics/RectF;->right:F

    iget v9, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v15, v9

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v3

    new-array v3, v7, [F

    cmpl-float v7, v13, v12

    if-lez v7, :cond_0

    goto :goto_0

    :cond_0
    const/4 v13, 0x0

    :goto_0
    aput v13, v3, v1

    cmpl-float v7, v14, v12

    if-lez v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v14, 0x0

    :goto_1
    aput v14, v3, v2

    cmpg-float v7, v15, v12

    if-gez v7, :cond_2

    goto :goto_2

    :cond_2
    const/4 v15, 0x0

    :goto_2
    aput v15, v3, v6

    cmpg-float v7, v0, v12

    if-gez v7, :cond_3

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    const/4 v7, 0x3

    aput v0, v3, v7

    iget-object v0, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual/range {p0 .. p0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v7

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->setRotate(F)V

    iget-object v0, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    aget v0, v3, v1

    aget v1, v3, v6

    add-float/2addr v0, v1

    neg-float v0, v0

    aget v1, v3, v2

    const/4 v2, 0x3

    aget v2, v3, v2

    add-float/2addr v1, v2

    neg-float v1, v1

    move v6, v0

    move v7, v1

    move/from16 v17, v8

    const/4 v9, 0x0

    goto/16 :goto_4

    :cond_4
    new-instance v9, Landroid/graphics/RectF;

    iget-object v12, v11, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-direct {v9, v12}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget-object v12, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v12}, Landroid/graphics/Matrix;->reset()V

    iget-object v12, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual/range {p0 .. p0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v13

    invoke-virtual {v12, v13}, Landroid/graphics/Matrix;->setRotate(F)V

    iget-object v12, v11, Lf/n/a/l/a;->t:Landroid/graphics/Matrix;

    invoke-virtual {v12, v9}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v12, v11, Lf/n/a/l/c;->d:[F

    new-array v13, v6, [F

    aget v14, v12, v1

    aget v15, v12, v6

    sub-float/2addr v14, v15

    float-to-double v14, v14

    move/from16 v17, v8

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    aget v18, v12, v2

    const/16 v16, 0x3

    aget v19, v12, v16

    sub-float v2, v18, v19

    float-to-double v1, v2

    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    add-double/2addr v1, v14

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x0

    aput v1, v13, v2

    aget v1, v12, v6

    const/4 v2, 0x4

    aget v2, v12, v2

    sub-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    const/4 v6, 0x3

    aget v6, v12, v6

    const/4 v14, 0x5

    aget v12, v12, v14

    sub-float/2addr v6, v12

    float-to-double v14, v6

    invoke-static {v14, v15, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    add-double/2addr v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x1

    aput v1, v13, v2

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v1

    const/4 v6, 0x0

    aget v6, v13, v6

    div-float/2addr v1, v6

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v6

    aget v2, v13, v2

    div-float/2addr v6, v2

    invoke-static {v1, v6}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float v1, v1, v17

    sub-float v1, v1, v17

    move v6, v0

    move v9, v1

    move v7, v3

    :goto_4
    if-eqz p1, :cond_5

    new-instance v12, Lf/n/a/l/a$a;

    iget-wide v2, v11, Lf/n/a/l/a;->D:J

    move-object v0, v12

    move-object/from16 v1, p0

    move/from16 v8, v17

    invoke-direct/range {v0 .. v10}, Lf/n/a/l/a$a;-><init>(Lf/n/a/l/a;JFFFFFFZ)V

    iput-object v12, v11, Lf/n/a/l/a;->x:Ljava/lang/Runnable;

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto :goto_5

    :cond_5
    invoke-virtual {v11, v6, v7}, Lf/n/a/l/c;->g(FF)V

    if-nez v10, :cond_6

    add-float v8, v17, v9

    iget-object v0, v11, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    iget-object v1, v11, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-virtual {v11, v8, v0, v1}, Lf/n/a/l/a;->l(FFF)V

    :cond_6
    :goto_5
    return-void
.end method

.method public setImageToWrapCropBoundsAnimDuration(J)V
    .locals 3
    .param p1    # J
        .annotation build Landroidx/annotation/IntRange;
            from = 0x64L
        .end annotation
    .end param

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iput-wide p1, p0, Lf/n/a/l/a;->D:J

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Animation duration cannot be negative value."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMaxResultImageSizeX(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/IntRange;
            from = 0xaL
        .end annotation
    .end param

    iput p1, p0, Lf/n/a/l/a;->B:I

    return-void
.end method

.method public setMaxResultImageSizeY(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroidx/annotation/IntRange;
            from = 0xaL
        .end annotation
    .end param

    iput p1, p0, Lf/n/a/l/a;->C:I

    return-void
.end method

.method public setMaxScaleMultiplier(F)V
    .locals 0

    iput p1, p0, Lf/n/a/l/a;->v:F

    return-void
.end method

.method public setTargetAspectRatio(F)V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    iput p1, p0, Lf/n/a/l/a;->u:F

    return-void

    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr p1, v0

    iput p1, p0, Lf/n/a/l/a;->u:F

    goto :goto_0

    :cond_1
    iput p1, p0, Lf/n/a/l/a;->u:F

    :goto_0
    iget-object p1, p0, Lf/n/a/l/a;->w:Lf/n/a/h/c;

    if-eqz p1, :cond_2

    iget v0, p0, Lf/n/a/l/a;->u:F

    check-cast p1, Lf/n/a/l/d;

    iget-object p1, p1, Lf/n/a/l/d;->a:Lcom/yalantis/ucrop/view/UCropView;

    iget-object p1, p1, Lcom/yalantis/ucrop/view/UCropView;->e:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual {p1, v0}, Lcom/yalantis/ucrop/view/OverlayView;->setTargetAspectRatio(F)V

    :cond_2
    return-void
.end method
