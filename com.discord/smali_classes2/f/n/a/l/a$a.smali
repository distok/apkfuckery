.class public Lf/n/a/l/a$a;
.super Ljava/lang/Object;
.source "CropImageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf/n/a/l/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lf/n/a/l/a;",
            ">;"
        }
    .end annotation
.end field

.field public final e:J

.field public final f:J

.field public final g:F

.field public final h:F

.field public final i:F

.field public final j:F

.field public final k:F

.field public final l:F

.field public final m:Z


# direct methods
.method public constructor <init>(Lf/n/a/l/a;JFFFFFFZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lf/n/a/l/a$a;->d:Ljava/lang/ref/WeakReference;

    iput-wide p2, p0, Lf/n/a/l/a$a;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lf/n/a/l/a$a;->f:J

    iput p4, p0, Lf/n/a/l/a$a;->g:F

    iput p5, p0, Lf/n/a/l/a$a;->h:F

    iput p6, p0, Lf/n/a/l/a$a;->i:F

    iput p7, p0, Lf/n/a/l/a$a;->j:F

    iput p8, p0, Lf/n/a/l/a$a;->k:F

    iput p9, p0, Lf/n/a/l/a$a;->l:F

    iput-boolean p10, p0, Lf/n/a/l/a$a;->m:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lf/n/a/l/a$a;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/n/a/l/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lf/n/a/l/a$a;->e:J

    iget-wide v5, p0, Lf/n/a/l/a$a;->f:J

    sub-long/2addr v1, v5

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-float v1, v1

    iget v2, p0, Lf/n/a/l/a$a;->i:F

    iget-wide v3, p0, Lf/n/a/l/a$a;->e:J

    long-to-float v3, v3

    div-float v4, v1, v3

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    mul-float v6, v4, v4

    mul-float v6, v6, v4

    add-float/2addr v6, v5

    mul-float v2, v2, v6

    const/4 v4, 0x0

    add-float/2addr v2, v4

    iget v5, p0, Lf/n/a/l/a$a;->j:F

    mul-float v6, v6, v5

    add-float/2addr v6, v4

    iget v5, p0, Lf/n/a/l/a$a;->l:F

    invoke-static {v1, v4, v5, v3}, Lf/h/a/f/f/n/g;->n(FFFF)F

    move-result v3

    iget-wide v4, p0, Lf/n/a/l/a$a;->e:J

    long-to-float v4, v4

    cmpg-float v1, v1, v4

    if-gez v1, :cond_2

    iget-object v1, v0, Lf/n/a/l/c;->e:[F

    const/4 v4, 0x0

    aget v4, v1, v4

    iget v5, p0, Lf/n/a/l/a$a;->g:F

    sub-float/2addr v4, v5

    sub-float/2addr v2, v4

    const/4 v4, 0x1

    aget v1, v1, v4

    iget v4, p0, Lf/n/a/l/a$a;->h:F

    sub-float/2addr v1, v4

    sub-float/2addr v6, v1

    invoke-virtual {v0, v2, v6}, Lf/n/a/l/c;->g(FF)V

    iget-boolean v1, p0, Lf/n/a/l/a$a;->m:Z

    if-nez v1, :cond_1

    iget v1, p0, Lf/n/a/l/a$a;->k:F

    add-float/2addr v1, v3

    iget-object v2, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lf/n/a/l/a;->l(FFF)V

    :cond_1
    iget-object v1, v0, Lf/n/a/l/c;->d:[F

    invoke-virtual {v0, v1}, Lf/n/a/l/a;->j([F)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method
