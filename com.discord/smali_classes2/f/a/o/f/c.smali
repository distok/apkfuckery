.class public final synthetic Lf/a/o/f/c;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/settings/WidgetSettingsLanguage;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsLanguage;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/f/c;->d:Lcom/discord/widgets/settings/WidgetSettingsLanguage;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lf/a/o/f/c;->d:Lcom/discord/widgets/settings/WidgetSettingsLanguage;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0xfa5

    invoke-static {p1, v0}, Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;->show(Landroidx/fragment/app/Fragment;I)V

    return-void
.end method
