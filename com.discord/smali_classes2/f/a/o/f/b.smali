.class public final synthetic Lf/a/o/f/b;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/settings/WidgetSettingsLanguage;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsLanguage;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/f/b;->d:Lcom/discord/widgets/settings/WidgetSettingsLanguage;

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lf/a/o/f/b;->d:Lcom/discord/widgets/settings/WidgetSettingsLanguage;

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/Integer;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p2

    invoke-virtual {v0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/discord/stores/StoreUserSettings;->setLocale(Lcom/discord/app/AppActivity;Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
