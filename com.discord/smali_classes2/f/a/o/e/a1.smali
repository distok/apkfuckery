.class public final synthetic Lf/a/o/e/a1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvitesListItem;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvitesListItem;Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/a1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvitesListItem;

    iput-object p2, p0, Lf/a/o/e/a1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lf/a/o/e/a1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvitesListItem;

    iget-object v0, p0, Lf/a/o/e/a1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;

    iget-object p1, p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Adapter;

    iget-object v0, v0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;->invite:Lcom/discord/models/domain/ModelInvite;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelInvite;->getCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Adapter;->onInviteSelected(Ljava/lang/String;)V

    return-void
.end method
