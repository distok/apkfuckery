.class public final synthetic Lf/a/o/e/z;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration;Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/z;->d:Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration;

    iput-object p2, p0, Lf/a/o/e/z;->e:Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lf/a/o/e/z;->d:Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration;

    iget-object v0, p0, Lf/a/o/e/z;->e:Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    iget-object v0, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditIntegration$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {v1, v2, v3, v4, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;->launch(JJLandroid/content/Context;)V

    return-void
.end method
