.class public final synthetic Lf/a/o/e/x1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;

.field public final synthetic e:J


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/x1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;

    iput-wide p2, p0, Lf/a/o/e/x1;->e:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lf/a/o/e/x1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;

    iget-wide v1, p0, Lf/a/o/e/x1;->e:J

    check-cast p1, Lcom/discord/models/domain/ModelGuildRole;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v3

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {v1, v2, v3, v4, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;->launch(JJLandroid/content/Context;)V

    return-void
.end method
