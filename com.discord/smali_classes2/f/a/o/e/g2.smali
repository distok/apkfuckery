.class public final synthetic Lf/a/o/e/g2;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:J


# direct methods
.method public synthetic constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/o/e/g2;->d:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-wide v0, p0, Lf/a/o/e/g2;->d:J

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/h2;

    invoke-direct {v1, p1}, Lf/a/o/e/h2;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
