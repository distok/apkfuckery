.class public final synthetic Lf/a/o/e/g1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action2;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/g1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

    iput-object p2, p0, Lf/a/o/e/g1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lf/a/o/e/g1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

    iget-object v1, p0, Lf/a/o/e/g1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;

    check-cast p1, Landroid/view/MenuItem;

    check-cast p2, Landroid/content/Context;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x7f0a06a0

    if-ne p1, p2, :cond_0

    iget-object p1, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide p1

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/discord/widgets/user/WidgetPruneUsers;->create(JLandroidx/fragment/app/FragmentManager;)V

    :cond_0
    return-void
.end method
