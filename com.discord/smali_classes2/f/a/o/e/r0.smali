.class public final synthetic Lf/a/o/e/r0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/r0;->d:Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;

    iput-object p2, p0, Lf/a/o/e/r0;->e:Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lf/a/o/e/r0;->d:Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;

    iget-object v0, p0, Lf/a/o/e/r0;->e:Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->isVerifiedServer()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->isPartneredServer()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    iget-object v0, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember$Model;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-static {v1, v2, v3, v4, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->create(JJLandroidx/fragment/app/FragmentActivity;)V

    goto :goto_1

    :cond_1
    :goto_0
    const v0, 0x7f12186c

    invoke-static {p1, v0}, Lf/a/b/p;->g(Landroidx/fragment/app/Fragment;I)V

    :goto_1
    return-void
.end method
