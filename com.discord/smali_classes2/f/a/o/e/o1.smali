.class public final synthetic Lf/a/o/e/o1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:J


# direct methods
.method public synthetic constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/o/e/o1;->d:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    iget-wide v0, p0, Lf/a/o/e/o1;->d:J

    check-cast p1, Lcom/discord/models/domain/ModelGuildRole;

    sget v2, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;->d:I

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/discord/stores/StoreGuilds;->observeRoles(JLjava/util/Collection;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/a2;

    invoke-direct {v1, p1}, Lf/a/o/e/a2;-><init>(Lcom/discord/models/domain/ModelGuildRole;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/o/e/w1;->d:Lf/a/o/e/w1;

    invoke-virtual {p1, v0}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
