.class public final synthetic Lf/a/o/e/e1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field public static final synthetic d:Lf/a/o/e/e1;


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/a/o/e/e1;

    invoke-direct {v0}, Lf/a/o/e/e1;-><init>()V

    sput-object v0, Lf/a/o/e/e1;->d:Lf/a/o/e/e1;

    return-void
.end method

.method public synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;

    check-cast p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;

    sget v0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->a:I

    iget-object v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->userDisplayName:Ljava/lang/String;

    iget-object v1, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->userDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->userId:J

    iget-wide p1, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->userId:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    :goto_0
    return v0
.end method
