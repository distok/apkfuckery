.class public final synthetic Lf/a/o/e/j1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/j1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;

    iput-object p2, p0, Lf/a/o/e/j1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lf/a/o/e/j1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;

    iget-object v0, p0, Lf/a/o/e/j1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->roles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, v0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->roles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;

    iget-object p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;->memberSelectedListener:Lrx/functions/Action2;

    iget-wide v2, v0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lrx/functions/Action2;->call(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
