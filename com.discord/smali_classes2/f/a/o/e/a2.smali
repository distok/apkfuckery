.class public final synthetic Lf/a/o/e/a2;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:Lcom/discord/models/domain/ModelGuildRole;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelGuildRole;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/a2;->d:Lcom/discord/models/domain/ModelGuildRole;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lf/a/o/e/a2;->d:Lcom/discord/models/domain/ModelGuildRole;

    check-cast p1, Ljava/util/Map;

    sget v1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;->d:I

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelGuildRole;

    return-object p1
.end method
