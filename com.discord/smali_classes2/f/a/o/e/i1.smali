.class public final synthetic Lf/a/o/e/i1;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action2;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/i1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

    iput-object p2, p0, Lf/a/o/e/i1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    iget-object v0, p0, Lf/a/o/e/i1;->d:Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

    iget-object v1, p0, Lf/a/o/e/i1;->e:Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;

    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object v3, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v3

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_1

    if-nez v2, :cond_1

    const p1, 0x7f1203f8

    invoke-static {v0, p1}, Lf/a/b/p;->g(Landroidx/fragment/app/Fragment;I)V

    goto :goto_2

    :cond_1
    iget-object v3, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->roles:Ljava/util/Map;

    invoke-static {v3, p2}, Lcom/discord/models/domain/ModelGuildRole;->getHighestRole(Ljava/util/Map;Ljava/util/Collection;)Lcom/discord/models/domain/ModelGuildRole;

    move-result-object p2

    iget-object v3, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->myHighestRole:Lcom/discord/models/domain/ModelGuildRole;

    invoke-static {v3, p2}, Lcom/discord/models/domain/ModelGuildRole;->rankIsHigher(Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->isOwner()Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    iget-object p1, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->myHighestRole:Lcom/discord/models/domain/ModelGuildRole;

    invoke-static {p1, p2}, Lcom/discord/models/domain/ModelGuildRole;->rankEquals(Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Z

    move-result p1

    if-eqz p1, :cond_3

    const p1, 0x7f1203f9

    invoke-static {v0, p1}, Lf/a/b/p;->g(Landroidx/fragment/app/Fragment;I)V

    goto :goto_2

    :cond_3
    const p1, 0x7f1203f7

    invoke-static {v0, p1}, Lf/a/b/p;->g(Landroidx/fragment/app/Fragment;I)V

    goto :goto_2

    :cond_4
    :goto_1
    iget-object p2, v1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v1, v2, p1, p2, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;->launch(JJLandroid/app/Activity;)V

    :goto_2
    return-void
.end method
