.class public final synthetic Lf/a/o/e/v0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:J


# direct methods
.method public synthetic constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/o/e/v0;->d:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-wide v0, p0, Lf/a/o/e/v0;->d:J

    check-cast p1, Ljava/util/Map;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->y(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object p1

    new-instance v2, Lf/a/o/e/x0;

    invoke-direct {v2, v0, v1}, Lf/a/o/e/x0;-><init>(J)V

    invoke-virtual {p1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lg0/l/a/r2;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lg0/l/a/r2;-><init>(I)V

    new-instance v1, Lg0/l/a/u;

    iget-object p1, p1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v1, p1, v0}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lg0/l/e/l;->d:Lg0/l/e/l;

    invoke-virtual {p1, v0}, Lrx/Observable;->x(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->a0()Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
