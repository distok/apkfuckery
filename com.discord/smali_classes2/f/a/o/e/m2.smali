.class public final synthetic Lf/a/o/e/m2;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsSecurity;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsSecurity;Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/m2;->d:Lcom/discord/widgets/servers/WidgetServerSettingsSecurity;

    iput-object p2, p0, Lf/a/o/e/m2;->e:Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$Model;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lf/a/o/e/m2;->d:Lcom/discord/widgets/servers/WidgetServerSettingsSecurity;

    iget-object v0, p0, Lf/a/o/e/m2;->e:Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$Model;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$Model;->guildId:J

    iget-boolean v0, v0, Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$Model;->isMfaEnabled:Z

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v3, "TAG_TOGGLE_MFA_DIALOG"

    invoke-static {v1, v2, v0, p1, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsSecurity$ToggleMfaDialog;->show(JZLandroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
