.class public final synthetic Lf/a/o/e/l;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/servers/WidgetServerSettingsChannels;

.field public final synthetic e:Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/e/l;->d:Lcom/discord/widgets/servers/WidgetServerSettingsChannels;

    iput-object p2, p0, Lf/a/o/e/l;->e:Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lf/a/o/e/l;->d:Lcom/discord/widgets/servers/WidgetServerSettingsChannels;

    iget-object v1, p0, Lf/a/o/e/l;->e:Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    check-cast p1, Ljava/lang/Long;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-static {v1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$300(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelChannel;

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$400(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    const-wide/16 v4, 0x10

    invoke-static {v4, v5, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->isVoiceChannel()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v3, v4, p1}, Lcom/discord/widgets/voice/settings/WidgetVoiceChannelSettings;->launch(JLandroid/content/Context;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v3, v4, p1}, Lcom/discord/widgets/channels/WidgetTextChannelSettings;->launch(JLandroid/content/Context;)V

    :cond_2
    :goto_0
    return-object v2
.end method
