.class public final synthetic Lf/a/o/g/o;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Action3;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/g/o;->d:Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lf/a/o/g/o;->d:Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;

    sget p1, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->a:I

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object p1

    invoke-static {p3}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;->access$300(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;)Lcom/discord/models/domain/ModelGuild;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide p2

    invoke-virtual {p1, p2, p3}, Lcom/discord/stores/StoreGuildSelected;->set(J)V

    invoke-static {v0}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;->access$500(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;)Lrx/functions/Action0;

    move-result-object p1

    invoke-interface {p1}, Lrx/functions/Action0;->call()V

    return-void
.end method
