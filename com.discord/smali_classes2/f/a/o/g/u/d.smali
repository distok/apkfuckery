.class public final synthetic Lf/a/o/g/u/d;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

.field public final synthetic e:Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/g/u/d;->d:Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    iput-object p2, p0, Lf/a/o/g/u/d;->e:Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lf/a/o/g/u/d;->d:Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    iget-object v0, p0, Lf/a/o/g/u/d;->e:Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    new-instance v2, Lcom/discord/restapi/RestAPIParams$EmptyBody;

    invoke-direct {v2}, Lcom/discord/restapi/RestAPIParams$EmptyBody;-><init>()V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/rest/RestAPI;->postAuthVerifyResend(Lcom/discord/restapi/RestAPIParams$EmptyBody;)Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    invoke-static {p1}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/o/g/u/c;

    invoke-direct {v2, p1, v0}, Lf/a/o/g/u/c;-><init>(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V

    invoke-static {v2, p1}, Lf/a/b/r;->m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;

    move-result-object p1

    invoke-virtual {v1, p1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
