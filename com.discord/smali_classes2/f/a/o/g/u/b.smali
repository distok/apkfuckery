.class public final synthetic Lf/a/o/g/u/b;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic d:Lcom/discord/widgets/user/email/WidgetUserEmailVerify;


# direct methods
.method public synthetic constructor <init>(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf/a/o/g/u/b;->d:Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lf/a/o/g/u/b;->d:Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getMode()Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/widgets/user/email/WidgetUserEmailUpdate;->launch(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;)V

    return-void
.end method
