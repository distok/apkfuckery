.class public final synthetic Lf/a/o/g/k;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lg0/k/b;


# instance fields
.field public final synthetic d:J


# direct methods
.method public synthetic constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lf/a/o/g/k;->d:J

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    iget-wide v0, p0, Lf/a/o/g/k;->d:J

    check-cast p1, Ljava/util/List;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsersMutualGuilds()Lcom/discord/stores/StoreUsersMutualGuilds;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/discord/stores/StoreUsersMutualGuilds;->get(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v3

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/discord/stores/StoreUser;->observeUsers(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v4

    invoke-static {}, Lcom/discord/stores/StoreStream;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/discord/stores/StoreUserPresence;->observePresencesForUsers(Ljava/util/Collection;)Lrx/Observable;

    move-result-object v5

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v6

    invoke-static {}, Lcom/discord/stores/StoreStream;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreApplicationStreaming;->getStreamsByUser()Lrx/Observable;

    move-result-object v7

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreUser;->observeUser(J)Lrx/Observable;

    move-result-object v8

    sget-object v9, Lf/a/o/g/l;->a:Lf/a/o/g/l;

    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x3e8

    invoke-static/range {v3 .. v12}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
