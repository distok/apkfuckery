.class public final Loupson/apng/exceptions/BadApng;
.super Ljava/lang/Exception;
.source "customException.kt"


# instance fields
.field private final message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    iput-object v0, p0, Loupson/apng/exceptions/BadApng;->message:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    iput-object p1, p0, Loupson/apng/exceptions/BadApng;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Loupson/apng/exceptions/BadApng;->message:Ljava/lang/String;

    return-object v0
.end method
