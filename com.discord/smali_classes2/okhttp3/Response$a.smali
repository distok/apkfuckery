.class public Lokhttp3/Response$a;
.super Ljava/lang/Object;
.source "Response.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Lb0/a0;

.field public b:Lb0/z;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Lb0/w;

.field public f:Lokhttp3/Headers$a;

.field public g:Lokhttp3/ResponseBody;

.field public h:Lokhttp3/Response;

.field public i:Lokhttp3/Response;

.field public j:Lokhttp3/Response;

.field public k:J

.field public l:J

.field public m:Lb0/g0/g/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lokhttp3/Response$a;->c:I

    new-instance v0, Lokhttp3/Headers$a;

    invoke-direct {v0}, Lokhttp3/Headers$a;-><init>()V

    iput-object v0, p0, Lokhttp3/Response$a;->f:Lokhttp3/Headers$a;

    return-void
.end method

.method public constructor <init>(Lokhttp3/Response;)V
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lokhttp3/Response$a;->c:I

    iget-object v0, p1, Lokhttp3/Response;->d:Lb0/a0;

    iput-object v0, p0, Lokhttp3/Response$a;->a:Lb0/a0;

    iget-object v0, p1, Lokhttp3/Response;->e:Lb0/z;

    iput-object v0, p0, Lokhttp3/Response$a;->b:Lb0/z;

    iget v0, p1, Lokhttp3/Response;->g:I

    iput v0, p0, Lokhttp3/Response$a;->c:I

    iget-object v0, p1, Lokhttp3/Response;->f:Ljava/lang/String;

    iput-object v0, p0, Lokhttp3/Response$a;->d:Ljava/lang/String;

    iget-object v0, p1, Lokhttp3/Response;->h:Lb0/w;

    iput-object v0, p0, Lokhttp3/Response$a;->e:Lb0/w;

    iget-object v0, p1, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v0}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v0

    iput-object v0, p0, Lokhttp3/Response$a;->f:Lokhttp3/Headers$a;

    iget-object v0, p1, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    iput-object v0, p0, Lokhttp3/Response$a;->g:Lokhttp3/ResponseBody;

    iget-object v0, p1, Lokhttp3/Response;->k:Lokhttp3/Response;

    iput-object v0, p0, Lokhttp3/Response$a;->h:Lokhttp3/Response;

    iget-object v0, p1, Lokhttp3/Response;->l:Lokhttp3/Response;

    iput-object v0, p0, Lokhttp3/Response$a;->i:Lokhttp3/Response;

    iget-object v0, p1, Lokhttp3/Response;->m:Lokhttp3/Response;

    iput-object v0, p0, Lokhttp3/Response$a;->j:Lokhttp3/Response;

    iget-wide v0, p1, Lokhttp3/Response;->n:J

    iput-wide v0, p0, Lokhttp3/Response$a;->k:J

    iget-wide v0, p1, Lokhttp3/Response;->o:J

    iput-wide v0, p0, Lokhttp3/Response$a;->l:J

    iget-object p1, p1, Lokhttp3/Response;->p:Lb0/g0/g/c;

    iput-object p1, p0, Lokhttp3/Response$a;->m:Lb0/g0/g/c;

    return-void
.end method


# virtual methods
.method public a()Lokhttp3/Response;
    .locals 18

    move-object/from16 v0, p0

    iget v5, v0, Lokhttp3/Response$a;->c:I

    if-ltz v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v2, v0, Lokhttp3/Response$a;->a:Lb0/a0;

    if-eqz v2, :cond_3

    iget-object v3, v0, Lokhttp3/Response$a;->b:Lb0/z;

    if-eqz v3, :cond_2

    iget-object v4, v0, Lokhttp3/Response$a;->d:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v6, v0, Lokhttp3/Response$a;->e:Lb0/w;

    iget-object v1, v0, Lokhttp3/Response$a;->f:Lokhttp3/Headers$a;

    invoke-virtual {v1}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v7

    iget-object v8, v0, Lokhttp3/Response$a;->g:Lokhttp3/ResponseBody;

    iget-object v9, v0, Lokhttp3/Response$a;->h:Lokhttp3/Response;

    iget-object v10, v0, Lokhttp3/Response$a;->i:Lokhttp3/Response;

    iget-object v11, v0, Lokhttp3/Response$a;->j:Lokhttp3/Response;

    iget-wide v12, v0, Lokhttp3/Response$a;->k:J

    iget-wide v14, v0, Lokhttp3/Response$a;->l:J

    iget-object v1, v0, Lokhttp3/Response$a;->m:Lb0/g0/g/c;

    new-instance v17, Lokhttp3/Response;

    move-object/from16 v16, v1

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lokhttp3/Response;-><init>(Lb0/a0;Lb0/z;Ljava/lang/String;ILb0/w;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLb0/g0/g/c;)V

    return-object v17

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "message == null"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "protocol == null"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "request == null"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const-string v1, "code < 0: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lokhttp3/Response$a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public b(Lokhttp3/Response;)Lokhttp3/Response$a;
    .locals 1

    const-string v0, "cacheResponse"

    invoke-virtual {p0, v0, p1}, Lokhttp3/Response$a;->c(Ljava/lang/String;Lokhttp3/Response;)V

    iput-object p1, p0, Lokhttp3/Response$a;->i:Lokhttp3/Response;

    return-object p0
.end method

.method public final c(Ljava/lang/String;Lokhttp3/Response;)V
    .locals 2

    if-eqz p2, :cond_8

    iget-object v0, p2, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_7

    iget-object v0, p2, Lokhttp3/Response;->k:Lokhttp3/Response;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p2, Lokhttp3/Response;->l:Lokhttp3/Response;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    iget-object p2, p2, Lokhttp3/Response;->m:Lokhttp3/Response;

    if-nez p2, :cond_3

    const/4 v1, 0x1

    :cond_3
    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    const-string p2, ".priorResponse != null"

    invoke-static {p1, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_5
    const-string p2, ".cacheResponse != null"

    invoke-static {p1, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    const-string p2, ".networkResponse != null"

    invoke-static {p1, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_7
    const-string p2, ".body != null"

    invoke-static {p1, p2}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_8
    :goto_3
    return-void
.end method

.method public d(Lokhttp3/Headers;)Lokhttp3/Response$a;
    .locals 1

    const-string v0, "headers"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object p1

    iput-object p1, p0, Lokhttp3/Response$a;->f:Lokhttp3/Headers$a;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lokhttp3/Response$a;
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lokhttp3/Response$a;->d:Ljava/lang/String;

    return-object p0
.end method

.method public f(Lb0/z;)Lokhttp3/Response$a;
    .locals 1

    const-string v0, "protocol"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lokhttp3/Response$a;->b:Lb0/z;

    return-object p0
.end method

.method public g(Lb0/a0;)Lokhttp3/Response$a;
    .locals 1

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lokhttp3/Response$a;->a:Lb0/a0;

    return-object p0
.end method
