.class public Lg0/l/a/m2;
.super Lrx/Subscriber;
.source "OperatorTakeWhile.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:I

.field public e:Z

.field public final synthetic f:Lrx/Subscriber;

.field public final synthetic g:Lg0/l/a/l2;


# direct methods
.method public constructor <init>(Lg0/l/a/l2;Lrx/Subscriber;ZLrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/m2;->g:Lg0/l/a/l2;

    iput-object p4, p0, Lg0/l/a/m2;->f:Lrx/Subscriber;

    invoke-direct {p0, p2, p3}, Lrx/Subscriber;-><init>(Lrx/Subscriber;Z)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/m2;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/m2;->f:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/m2;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/m2;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lg0/l/a/m2;->g:Lg0/l/a/l2;

    iget-object v1, v1, Lg0/l/a/l2;->d:Lrx/functions/Func2;

    iget v2, p0, Lg0/l/a/m2;->d:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lg0/l/a/m2;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lrx/functions/Func2;->call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lg0/l/a/m2;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iput-boolean v0, p0, Lg0/l/a/m2;->e:Z

    iget-object p1, p0, Lg0/l/a/m2;->f:Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    iput-boolean v0, p0, Lg0/l/a/m2;->e:Z

    iget-object v0, p0, Lg0/l/a/m2;->f:Lrx/Subscriber;

    invoke-static {v1, v0, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    return-void
.end method
