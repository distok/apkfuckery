.class public Lg0/l/a/d1;
.super Ljava/lang/Object;
.source "OperatorOnBackpressureBuffer.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/d1$a;,
        Lg0/l/a/d1$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lg0/a$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lg0/a;->a:I

    sget-object v0, Lg0/a$a;->a:Lg0/a$a;

    iput-object v0, p0, Lg0/l/a/d1;->d:Lg0/a$b;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/d1$a;

    iget-object v1, p0, Lg0/l/a/d1;->d:Lg0/a$b;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v2, v2, v1}, Lg0/l/a/d1$a;-><init>(Lrx/Subscriber;Ljava/lang/Long;Lrx/functions/Action0;Lg0/a$b;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v1, v0, Lg0/l/a/d1$a;->h:Lg0/l/e/c;

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-object v0
.end method
