.class public Lg0/l/a/a0;
.super Ljava/lang/Object;
.source "OnSubscribeRedo.java"

# interfaces
.implements Lrx/Producer;


# instance fields
.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicLong;

.field public final synthetic e:Lg0/l/b/a;

.field public final synthetic f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic g:Lrx/Scheduler$Worker;

.field public final synthetic h:Lrx/functions/Action0;


# direct methods
.method public constructor <init>(Lg0/l/a/b0;Ljava/util/concurrent/atomic/AtomicLong;Lg0/l/b/a;Ljava/util/concurrent/atomic/AtomicBoolean;Lrx/Scheduler$Worker;Lrx/functions/Action0;)V
    .locals 0

    iput-object p2, p0, Lg0/l/a/a0;->d:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p3, p0, Lg0/l/a/a0;->e:Lg0/l/b/a;

    iput-object p4, p0, Lg0/l/a/a0;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p5, p0, Lg0/l/a/a0;->g:Lrx/Scheduler$Worker;

    iput-object p6, p0, Lg0/l/a/a0;->h:Lrx/functions/Action0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public l(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    iget-object v0, p0, Lg0/l/a/a0;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v0, p1, p2}, Ly/a/g0;->p(Ljava/util/concurrent/atomic/AtomicLong;J)J

    iget-object v0, p0, Lg0/l/a/a0;->e:Lg0/l/b/a;

    invoke-virtual {v0, p1, p2}, Lg0/l/b/a;->l(J)V

    iget-object p1, p0, Lg0/l/a/a0;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lg0/l/a/a0;->g:Lrx/Scheduler$Worker;

    iget-object p2, p0, Lg0/l/a/a0;->h:Lrx/functions/Action0;

    invoke-virtual {p1, p2}, Lrx/Scheduler$Worker;->a(Lrx/functions/Action0;)Lrx/Subscription;

    :cond_0
    return-void
.end method
