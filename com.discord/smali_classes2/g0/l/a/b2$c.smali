.class public final Lg0/l/a/b2$c;
.super Lrx/Subscriber;
.source "OperatorSwitch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/b2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "Lrx/Observable<",
        "+TT;>;>;"
    }
.end annotation


# static fields
.field public static final p:Ljava/lang/Throwable;


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public final e:Lrx/subscriptions/SerialSubscription;

.field public final f:Z

.field public final g:Ljava/util/concurrent/atomic/AtomicLong;

.field public final h:Lg0/l/e/m/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/e/m/e<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field public j:Z

.field public k:J

.field public l:Lrx/Producer;

.field public volatile m:Z

.field public n:Ljava/lang/Throwable;

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Terminal error"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v0, Lg0/l/a/b2$c;->p:Ljava/lang/Throwable;

    return-void
.end method

.method public constructor <init>(Lrx/Subscriber;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/b2$c;->d:Lrx/Subscriber;

    new-instance p1, Lrx/subscriptions/SerialSubscription;

    invoke-direct {p1}, Lrx/subscriptions/SerialSubscription;-><init>()V

    iput-object p1, p0, Lg0/l/a/b2$c;->e:Lrx/subscriptions/SerialSubscription;

    iput-boolean p2, p0, Lg0/l/a/b2$c;->f:Z

    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance p1, Lg0/l/e/m/e;

    sget p2, Lg0/l/e/h;->f:I

    invoke-direct {p1, p2}, Lg0/l/e/m/e;-><init>(I)V

    iput-object p1, p0, Lg0/l/a/b2$c;->h:Lg0/l/e/m/e;

    return-void
.end method


# virtual methods
.method public a(ZZLjava/lang/Throwable;Lg0/l/e/m/e;Lrx/Subscriber;Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/Throwable;",
            "Lg0/l/e/m/e<",
            "Ljava/lang/Object;",
            ">;",
            "Lrx/Subscriber<",
            "-TT;>;Z)Z"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/b2$c;->f:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    if-eqz p6, :cond_3

    if-eqz p3, :cond_0

    invoke-interface {p5, p3}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-interface {p5}, Lg0/g;->onCompleted()V

    :goto_0
    return v1

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p4}, Lg0/l/e/m/e;->clear()V

    invoke-interface {p5, p3}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return v1

    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    if-eqz p6, :cond_3

    invoke-interface {p5}, Lg0/g;->onCompleted()V

    return v1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public b()V
    .locals 20

    move-object/from16 v8, p0

    monitor-enter p0

    :try_start_0
    iget-boolean v0, v8, Lg0/l/a/b2$c;->i:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, v8, Lg0/l/a/b2$c;->j:Z

    monitor-exit p0

    return-void

    :cond_0
    iput-boolean v1, v8, Lg0/l/a/b2$c;->i:Z

    iget-boolean v0, v8, Lg0/l/a/b2$c;->o:Z

    iget-wide v1, v8, Lg0/l/a/b2$c;->k:J

    iget-object v3, v8, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    if-eqz v3, :cond_1

    sget-object v4, Lg0/l/a/b2$c;->p:Ljava/lang/Throwable;

    if-eq v3, v4, :cond_1

    iget-boolean v5, v8, Lg0/l/a/b2$c;->f:Z

    if-nez v5, :cond_1

    iput-object v4, v8, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v9, v8, Lg0/l/a/b2$c;->h:Lg0/l/e/m/e;

    iget-object v10, v8, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v11, v8, Lg0/l/a/b2$c;->d:Lrx/Subscriber;

    iget-boolean v4, v8, Lg0/l/a/b2$c;->m:Z

    move-wide v12, v1

    move-object v14, v3

    move v15, v4

    :goto_0
    const-wide/16 v1, 0x0

    move-wide/from16 v16, v1

    :cond_2
    :goto_1
    cmp-long v18, v16, v12

    if-eqz v18, :cond_6

    invoke-virtual {v11}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_3

    return-void

    :cond_3
    invoke-virtual {v9}, Lg0/l/e/m/e;->isEmpty()Z

    move-result v19

    move-object/from16 v1, p0

    move v2, v15

    move v3, v0

    move-object v4, v14

    move-object v5, v9

    move-object v6, v11

    move/from16 v7, v19

    invoke-virtual/range {v1 .. v7}, Lg0/l/a/b2$c;->a(ZZLjava/lang/Throwable;Lg0/l/e/m/e;Lrx/Subscriber;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    return-void

    :cond_4
    if-eqz v19, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {v9}, Lg0/l/e/m/e;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg0/l/a/b2$b;

    invoke-virtual {v9}, Lg0/l/e/m/e;->poll()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    iget-wide v5, v1, Lg0/l/a/b2$b;->d:J

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    invoke-interface {v11, v2}, Lg0/g;->onNext(Ljava/lang/Object;)V

    const-wide/16 v1, 0x1

    add-long v16, v16, v1

    goto :goto_1

    :cond_6
    :goto_2
    if-nez v18, :cond_8

    invoke-virtual {v11}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_7

    return-void

    :cond_7
    iget-boolean v2, v8, Lg0/l/a/b2$c;->m:Z

    invoke-virtual {v9}, Lg0/l/e/m/e;->isEmpty()Z

    move-result v7

    move-object/from16 v1, p0

    move v3, v0

    move-object v4, v14

    move-object v5, v9

    move-object v6, v11

    invoke-virtual/range {v1 .. v7}, Lg0/l/a/b2$c;->a(ZZLjava/lang/Throwable;Lg0/l/e/m/e;Lrx/Subscriber;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    monitor-enter p0

    :try_start_1
    iget-wide v0, v8, Lg0/l/a/b2$c;->k:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-eqz v4, :cond_9

    sub-long v0, v0, v16

    iput-wide v0, v8, Lg0/l/a/b2$c;->k:J

    :cond_9
    move-wide v12, v0

    iget-boolean v0, v8, Lg0/l/a/b2$c;->j:Z

    const/4 v1, 0x0

    if-nez v0, :cond_a

    iput-boolean v1, v8, Lg0/l/a/b2$c;->i:Z

    monitor-exit p0

    return-void

    :cond_a
    iput-boolean v1, v8, Lg0/l/a/b2$c;->j:Z

    iget-boolean v15, v8, Lg0/l/a/b2$c;->m:Z

    iget-boolean v0, v8, Lg0/l/a/b2$c;->o:Z

    iget-object v14, v8, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    if-eqz v14, :cond_b

    sget-object v1, Lg0/l/a/b2$c;->p:Ljava/lang/Throwable;

    if-eq v14, v1, :cond_b

    iget-boolean v2, v8, Lg0/l/a/b2$c;->f:Z

    if-nez v2, :cond_b

    iput-object v1, v8, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    :cond_b
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public c(Ljava/lang/Throwable;)Z
    .locals 5

    iget-object v0, p0, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    sget-object v1, Lg0/l/a/b2$c;->p:Ljava/lang/Throwable;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    const/4 v1, 0x1

    if-nez v0, :cond_1

    iput-object p1, p0, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    goto :goto_0

    :cond_1
    instance-of v3, v0, Lrx/exceptions/CompositeException;

    if-eqz v3, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    check-cast v0, Lrx/exceptions/CompositeException;

    invoke-virtual {v0}, Lrx/exceptions/CompositeException;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p1, Lrx/exceptions/CompositeException;

    invoke-direct {p1, v2}, Lrx/exceptions/CompositeException;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    goto :goto_0

    :cond_2
    new-instance v3, Lrx/exceptions/CompositeException;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Throwable;

    aput-object v0, v4, v2

    aput-object p1, v4, v1

    invoke-direct {v3, v4}, Lrx/exceptions/CompositeException;-><init>([Ljava/lang/Throwable;)V

    iput-object v3, p0, Lg0/l/a/b2$c;->n:Ljava/lang/Throwable;

    :goto_0
    return v1
.end method

.method public onCompleted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/b2$c;->m:Z

    invoke-virtual {p0}, Lg0/l/a/b2$c;->b()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lg0/l/a/b2$c;->c(Ljava/lang/Throwable;)Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/b2$c;->m:Z

    invoke-virtual {p0}, Lg0/l/a/b2$c;->b()V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lrx/Observable;

    iget-object v0, p0, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v0

    iget-object v2, p0, Lg0/l/a/b2$c;->e:Lrx/subscriptions/SerialSubscription;

    iget-object v2, v2, Lrx/subscriptions/SerialSubscription;->d:Lg0/l/d/a;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrx/Subscription;

    sget-object v3, Lg0/l/d/b;->d:Lg0/l/d/b;

    if-ne v2, v3, :cond_0

    sget-object v2, Lg0/r/c;->a:Lg0/r/c$a;

    :cond_0
    if-eqz v2, :cond_1

    invoke-interface {v2}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    monitor-enter p0

    :try_start_0
    new-instance v2, Lg0/l/a/b2$b;

    invoke-direct {v2, v0, v1, p0}, Lg0/l/a/b2$b;-><init>(JLg0/l/a/b2$c;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/b2$c;->o:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lg0/l/a/b2$c;->l:Lrx/Producer;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lg0/l/a/b2$c;->e:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v0, v2}, Lrx/subscriptions/SerialSubscription;->a(Lrx/Subscription;)V

    invoke-virtual {p1, v2}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
