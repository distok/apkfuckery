.class public final Lg0/l/a/o0;
.super Ljava/lang/Object;
.source "OnSubscribeToMap.java"

# interfaces
.implements Lrx/Observable$a;
.implements Lrx/functions/Func0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/o0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "Ljava/util/Map<",
        "TK;TV;>;>;",
        "Lrx/functions/Func0<",
        "Ljava/util/Map<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "-TT;+TK;>;"
        }
    .end annotation
.end field

.field public final f:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "-TT;+TV;>;"
        }
    .end annotation
.end field

.field public final g:Lrx/functions/Func0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func0<",
            "+",
            "Ljava/util/Map<",
            "TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;Lg0/k/b;Lg0/k/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;",
            "Lg0/k/b<",
            "-TT;+TK;>;",
            "Lg0/k/b<",
            "-TT;+TV;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/o0;->d:Lrx/Observable;

    iput-object p2, p0, Lg0/l/a/o0;->e:Lg0/k/b;

    iput-object p3, p0, Lg0/l/a/o0;->f:Lg0/k/b;

    iput-object p0, p0, Lg0/l/a/o0;->g:Lrx/functions/Func0;

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public call(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lrx/Subscriber;

    :try_start_0
    iget-object v0, p0, Lg0/l/a/o0;->g:Lrx/functions/Func0;

    invoke-interface {v0}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Lg0/l/a/o0$a;

    iget-object v2, p0, Lg0/l/a/o0;->e:Lg0/k/b;

    iget-object v3, p0, Lg0/l/a/o0;->f:Lg0/k/b;

    invoke-direct {v1, p1, v0, v2, v3}, Lg0/l/a/o0$a;-><init>(Lrx/Subscriber;Ljava/util/Map;Lg0/k/b;Lg0/k/b;)V

    iget-object p1, p0, Lg0/l/a/o0;->d:Lrx/Observable;

    iget-object v0, v1, Lg0/l/a/d;->d:Lrx/Subscriber;

    invoke-virtual {v0, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    new-instance v2, Lg0/l/a/d$a;

    invoke-direct {v2, v1}, Lg0/l/a/d$a;-><init>(Lg0/l/a/d;)V

    invoke-virtual {v0, v2}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    invoke-virtual {p1, v1}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
