.class public Lg0/l/a/h0;
.super Ljava/lang/Object;
.source "OnSubscribeSingle.java"

# interfaces
.implements Lg0/h$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/h$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/h0;->d:Lrx/Observable;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lg0/i;

    new-instance v0, Lg0/l/a/g0;

    invoke-direct {v0, p0, p1}, Lg0/l/a/g0;-><init>(Lg0/l/a/h0;Lg0/i;)V

    iget-object p1, p1, Lg0/i;->d:Lrx/internal/util/SubscriptionList;

    invoke-virtual {p1, v0}, Lrx/internal/util/SubscriptionList;->a(Lrx/Subscription;)V

    iget-object p1, p0, Lg0/l/a/h0;->d:Lrx/Observable;

    invoke-virtual {p1, v0}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
