.class public final Lg0/l/a/r1$b;
.super Ljava/lang/Object;
.source "OperatorScan.java"

# interfaces
.implements Lrx/Producer;
.implements Lg0/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/r1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Producer;",
        "Lg0/g<",
        "TR;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TR;>;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:J

.field public final i:Ljava/util/concurrent/atomic/AtomicLong;

.field public volatile j:Lrx/Producer;

.field public volatile k:Z

.field public l:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lrx/Subscriber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lrx/Subscriber<",
            "-TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lg0/l/a/r1$b;->d:Lrx/Subscriber;

    invoke-static {}, Lg0/l/e/n/y;->b()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Lg0/l/e/n/s;

    invoke-direct {p2}, Lg0/l/e/n/s;-><init>()V

    goto :goto_0

    :cond_0
    new-instance p2, Lg0/l/e/m/f;

    invoke-direct {p2}, Lg0/l/e/m/f;-><init>()V

    :goto_0
    iput-object p2, p0, Lg0/l/a/r1$b;->e:Ljava/util/Queue;

    if-nez p1, :cond_1

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_1
    invoke-interface {p2, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/r1$b;->i:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method


# virtual methods
.method public a(ZZLrx/Subscriber;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lrx/Subscriber<",
            "-TR;>;)Z"
        }
    .end annotation

    invoke-virtual {p3}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    if-eqz p1, :cond_2

    iget-object p1, p0, Lg0/l/a/r1$b;->l:Ljava/lang/Throwable;

    if-eqz p1, :cond_1

    invoke-interface {p3, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return v1

    :cond_1
    if-eqz p2, :cond_2

    invoke-interface {p3}, Lg0/g;->onCompleted()V

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public b()V
    .locals 14

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lg0/l/a/r1$b;->f:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lg0/l/a/r1$b;->g:Z

    monitor-exit p0

    return-void

    :cond_0
    iput-boolean v1, p0, Lg0/l/a/r1$b;->f:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    iget-object v0, p0, Lg0/l/a/r1$b;->d:Lrx/Subscriber;

    iget-object v2, p0, Lg0/l/a/r1$b;->e:Ljava/util/Queue;

    iget-object v3, p0, Lg0/l/a/r1$b;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    :goto_0
    iget-boolean v6, p0, Lg0/l/a/r1$b;->k:Z

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v7

    invoke-virtual {p0, v6, v7, v0}, Lg0/l/a/r1$b;->a(ZZLrx/Subscriber;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_4

    :cond_1
    const-wide/16 v6, 0x0

    move-wide v8, v6

    :goto_1
    const/4 v10, 0x0

    cmp-long v11, v8, v4

    if-eqz v11, :cond_5

    iget-boolean v11, p0, Lg0/l/a/r1$b;->k:Z

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v12

    if-nez v12, :cond_2

    const/4 v13, 0x1

    goto :goto_2

    :cond_2
    const/4 v13, 0x0

    :goto_2
    invoke-virtual {p0, v11, v13, v0}, Lg0/l/a/r1$b;->a(ZZLrx/Subscriber;)Z

    move-result v11

    if-eqz v11, :cond_3

    goto :goto_4

    :cond_3
    if-eqz v13, :cond_4

    goto :goto_3

    :cond_4
    invoke-static {v12}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    :try_start_1
    invoke-interface {v0, v10}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-static {v1, v0, v10}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    goto :goto_4

    :cond_5
    :goto_3
    cmp-long v11, v8, v6

    if-eqz v11, :cond_6

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v11, v4, v6

    if-eqz v11, :cond_6

    invoke-static {v3, v8, v9}, Ly/a/g0;->x(Ljava/util/concurrent/atomic/AtomicLong;J)J

    move-result-wide v4

    :cond_6
    monitor-enter p0

    :try_start_2
    iget-boolean v6, p0, Lg0/l/a/r1$b;->g:Z

    if-nez v6, :cond_7

    iput-boolean v10, p0, Lg0/l/a/r1$b;->f:Z

    monitor-exit p0

    :goto_4
    return-void

    :cond_7
    iput-boolean v10, p0, Lg0/l/a/r1$b;->g:Z

    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method public l(J)V
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_4

    if-eqz v2, :cond_3

    iget-object v0, p0, Lg0/l/a/r1$b;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v0, p1, p2}, Ly/a/g0;->p(Ljava/util/concurrent/atomic/AtomicLong;J)J

    iget-object v0, p0, Lg0/l/a/r1$b;->j:Lrx/Producer;

    if-nez v0, :cond_1

    iget-object v1, p0, Lg0/l/a/r1$b;->i:Ljava/util/concurrent/atomic/AtomicLong;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lg0/l/a/r1$b;->j:Lrx/Producer;

    if-nez v0, :cond_0

    iget-wide v2, p0, Lg0/l/a/r1$b;->h:J

    invoke-static {v2, v3, p1, p2}, Ly/a/g0;->c(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lg0/l/a/r1$b;->h:J

    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {v0, p1, p2}, Lrx/Producer;->l(J)V

    :cond_2
    invoke-virtual {p0}, Lg0/l/a/r1$b;->b()V

    :cond_3
    return-void

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "n >= required but it was "

    invoke-static {v1, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCompleted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/r1$b;->k:Z

    invoke-virtual {p0}, Lg0/l/a/r1$b;->b()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/r1$b;->l:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/r1$b;->k:Z

    invoke-virtual {p0}, Lg0/l/a/r1$b;->b()V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/r1$b;->e:Ljava/util/Queue;

    if-nez p1, :cond_0

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lg0/l/a/r1$b;->b()V

    return-void
.end method
