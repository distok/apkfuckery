.class public final Lg0/l/a/c1;
.super Ljava/lang/Object;
.source "OperatorObserveOn.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/c1$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Scheduler;

.field public final e:Z

.field public final f:I


# direct methods
.method public constructor <init>(Lrx/Scheduler;ZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/c1;->d:Lrx/Scheduler;

    iput-boolean p2, p0, Lg0/l/a/c1;->e:Z

    if-lez p3, :cond_0

    goto :goto_0

    :cond_0
    sget p3, Lg0/l/e/h;->f:I

    :goto_0
    iput p3, p0, Lg0/l/a/c1;->f:I

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/c1;->d:Lrx/Scheduler;

    instance-of v1, v0, Lg0/l/c/m;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lg0/l/a/c1$a;

    iget-boolean v2, p0, Lg0/l/a/c1;->e:Z

    iget v3, p0, Lg0/l/a/c1;->f:I

    invoke-direct {v1, v0, p1, v2, v3}, Lg0/l/a/c1$a;-><init>(Lrx/Scheduler;Lrx/Subscriber;ZI)V

    iget-object p1, v1, Lg0/l/a/c1$a;->d:Lrx/Subscriber;

    new-instance v0, Lg0/l/a/b1;

    invoke-direct {v0, v1}, Lg0/l/a/b1;-><init>(Lg0/l/a/c1$a;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    iget-object v0, v1, Lg0/l/a/c1$a;->e:Lrx/Scheduler$Worker;

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    move-object p1, v1

    :goto_0
    return-object p1
.end method
