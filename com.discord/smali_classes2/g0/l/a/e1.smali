.class public final Lg0/l/a/e1;
.super Ljava/lang/Object;
.source "OperatorOnBackpressureLatest.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/e1$c;,
        Lg0/l/a/e1$b;,
        Lg0/l/a/e1$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/e1$b;

    invoke-direct {v0, p1}, Lg0/l/a/e1$b;-><init>(Lrx/Subscriber;)V

    new-instance v1, Lg0/l/a/e1$c;

    invoke-direct {v1, v0}, Lg0/l/a/e1$c;-><init>(Lg0/l/a/e1$b;)V

    iput-object v1, v0, Lg0/l/a/e1$b;->parent:Lg0/l/a/e1$c;

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-object v1
.end method
