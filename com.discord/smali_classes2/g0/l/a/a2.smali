.class public final Lg0/l/a/a2;
.super Ljava/lang/Object;
.source "OperatorSubscribeOn.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/a2$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Scheduler;

.field public final e:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final f:Z


# direct methods
.method public constructor <init>(Lrx/Observable;Lrx/Scheduler;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;",
            "Lrx/Scheduler;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lg0/l/a/a2;->d:Lrx/Scheduler;

    iput-object p1, p0, Lg0/l/a/a2;->e:Lrx/Observable;

    iput-boolean p3, p0, Lg0/l/a/a2;->f:Z

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/a2;->d:Lrx/Scheduler;

    invoke-virtual {v0}, Lrx/Scheduler;->a()Lrx/Scheduler$Worker;

    move-result-object v0

    new-instance v1, Lg0/l/a/a2$a;

    iget-boolean v2, p0, Lg0/l/a/a2;->f:Z

    iget-object v3, p0, Lg0/l/a/a2;->e:Lrx/Observable;

    invoke-direct {v1, p1, v2, v0, v3}, Lg0/l/a/a2$a;-><init>(Lrx/Subscriber;ZLrx/Scheduler$Worker;Lrx/Observable;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {v0, v1}, Lrx/Scheduler$Worker;->a(Lrx/functions/Action0;)Lrx/Subscription;

    return-void
.end method
