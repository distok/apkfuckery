.class public Lg0/l/a/s0$a;
.super Ljava/lang/Object;
.source "OperatorDebounceWithTime.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/a/s0;->onNext(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:I

.field public final synthetic e:Lg0/l/a/s0;


# direct methods
.method public constructor <init>(Lg0/l/a/s0;I)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/s0$a;->e:Lg0/l/a/s0;

    iput p2, p0, Lg0/l/a/s0$a;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    iget-object v0, p0, Lg0/l/a/s0$a;->e:Lg0/l/a/s0;

    iget-object v1, v0, Lg0/l/a/s0;->d:Lg0/l/a/t0$a;

    iget v2, p0, Lg0/l/a/s0$a;->d:I

    iget-object v3, v0, Lg0/l/a/s0;->h:Lrx/observers/SerializedSubscriber;

    iget-object v0, v0, Lg0/l/a/s0;->e:Lrx/Subscriber;

    monitor-enter v1

    :try_start_0
    iget-boolean v4, v1, Lg0/l/a/t0$a;->e:Z

    if-nez v4, :cond_2

    iget-boolean v4, v1, Lg0/l/a/t0$a;->c:Z

    if-eqz v4, :cond_2

    iget v4, v1, Lg0/l/a/t0$a;->a:I

    if-eq v2, v4, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lg0/l/a/t0$a;->b:Ljava/lang/Object;

    const/4 v4, 0x0

    iput-object v4, v1, Lg0/l/a/t0$a;->b:Ljava/lang/Object;

    const/4 v4, 0x0

    iput-boolean v4, v1, Lg0/l/a/t0$a;->c:Z

    const/4 v5, 0x1

    iput-boolean v5, v1, Lg0/l/a/t0$a;->e:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {v3, v2}, Lrx/observers/SerializedSubscriber;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-enter v1

    :try_start_2
    iget-boolean v0, v1, Lg0/l/a/t0$a;->d:Z

    if-nez v0, :cond_1

    iput-boolean v4, v1, Lg0/l/a/t0$a;->e:Z

    monitor-exit v1

    goto :goto_1

    :cond_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v3}, Lrx/observers/SerializedSubscriber;->onCompleted()V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v1

    invoke-static {v1, v0, v2}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    :goto_0
    :try_start_4
    monitor-exit v1

    :goto_1
    return-void

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method
