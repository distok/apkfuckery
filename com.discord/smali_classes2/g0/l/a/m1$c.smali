.class public final Lg0/l/a/m1$c;
.super Ljava/util/concurrent/atomic/AtomicLong;
.source "OperatorReplay.java"

# interfaces
.implements Lrx/Producer;
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/m1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicLong;",
        "Lrx/Producer;",
        "Lrx/Subscription;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x3dcf6c3b2e70d8baL


# instance fields
.field public child:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public emitting:Z

.field public index:Ljava/lang/Object;

.field public missed:Z

.field public final parent:Lg0/l/a/m1$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/m1$f<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final totalRequested:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(Lg0/l/a/m1$f;Lrx/Subscriber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/l/a/m1$f<",
            "TT;>;",
            "Lrx/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/m1$c;->parent:Lg0/l/a/m1$f;

    iput-object p2, p0, Lg0/l/a/m1$c;->child:Lrx/Subscriber;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 7

    :cond_0
    iget-object v0, p0, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    add-long v2, v0, p1

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-gez v6, :cond_1

    const-wide v2, 0x7fffffffffffffffL

    :cond_1
    iget-object v4, p0, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4, v0, v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method public b(J)J
    .locals 7

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_3

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    return-wide v4

    :cond_1
    sub-long v4, v2, p1

    cmp-long v6, v4, v0

    if-ltz v6, :cond_2

    invoke-virtual {p0, v2, v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    return-wide v4

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "More produced ("

    const-string v4, ") than requested ("

    invoke-static {v1, p1, p2, v4}, Lf/e/c/a/a;->K(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, ")"

    invoke-static {p1, v2, v3, p2}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cant produce zero or less"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isUnsubscribed()Z
    .locals 5

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l(J)V
    .locals 8

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    const-wide/high16 v5, -0x8000000000000000L

    cmp-long v7, v3, v5

    if-nez v7, :cond_1

    return-void

    :cond_1
    cmp-long v5, v3, v0

    if-ltz v5, :cond_2

    if-nez v2, :cond_2

    return-void

    :cond_2
    add-long v5, v3, p1

    cmp-long v7, v5, v0

    if-gez v7, :cond_3

    const-wide v5, 0x7fffffffffffffffL

    :cond_3
    invoke-virtual {p0, v3, v4, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, p1, p2}, Lg0/l/a/m1$c;->a(J)V

    iget-object p1, p0, Lg0/l/a/m1$c;->parent:Lg0/l/a/m1$f;

    invoke-virtual {p1, p0}, Lg0/l/a/m1$f;->c(Lg0/l/a/m1$c;)V

    iget-object p1, p0, Lg0/l/a/m1$c;->parent:Lg0/l/a/m1$f;

    iget-object p1, p1, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    invoke-interface {p1, p0}, Lg0/l/a/m1$e;->h(Lg0/l/a/m1$c;)V

    return-void
.end method

.method public unsubscribe()V
    .locals 8

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-eqz v4, :cond_7

    invoke-virtual {p0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_7

    iget-object v0, p0, Lg0/l/a/m1$c;->parent:Lg0/l/a/m1$f;

    iget-boolean v1, v0, Lg0/l/a/m1$f;->f:Z

    if-eqz v1, :cond_0

    goto :goto_2

    :cond_0
    iget-object v1, v0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Lg0/l/a/m1$f;->f:Z

    if-eqz v2, :cond_1

    monitor-exit v1

    goto :goto_2

    :cond_1
    iget-object v2, v0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    iget-object v3, v2, Lg0/l/e/f;->d:[Ljava/lang/Object;

    iget v4, v2, Lg0/l/e/f;->a:I

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-static {v5}, Lg0/l/e/f;->b(I)I

    move-result v5

    and-int/2addr v5, v4

    aget-object v6, v3, v5

    const/4 v7, 0x1

    if-nez v6, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v6, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v2, v5, v3, v4}, Lg0/l/e/f;->c(I[Ljava/lang/Object;I)Z

    goto :goto_0

    :cond_3
    add-int/2addr v5, v7

    and-int/2addr v5, v4

    aget-object v6, v3, v5

    if-nez v6, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {v6, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v2, v5, v3, v4}, Lg0/l/e/f;->c(I[Ljava/lang/Object;I)Z

    :goto_0
    iget-object v2, v0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    iget v2, v2, Lg0/l/e/f;->b:I

    if-nez v2, :cond_5

    goto :goto_1

    :cond_5
    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_6

    sget-object v2, Lg0/l/a/m1$f;->s:[Lg0/l/a/m1$c;

    iput-object v2, v0, Lg0/l/a/m1$f;->h:[Lg0/l/a/m1$c;

    :cond_6
    iget-wide v2, v0, Lg0/l/a/m1$f;->i:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Lg0/l/a/m1$f;->i:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    iget-object v0, p0, Lg0/l/a/m1$c;->parent:Lg0/l/a/m1$f;

    invoke-virtual {v0, p0}, Lg0/l/a/m1$f;->c(Lg0/l/a/m1$c;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lg0/l/a/m1$c;->child:Lrx/Subscriber;

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    :goto_3
    return-void
.end method
