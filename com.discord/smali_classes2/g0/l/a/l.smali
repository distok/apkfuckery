.class public final Lg0/l/a/l;
.super Ljava/lang/Object;
.source "OnSubscribeCreate.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/l$e;,
        Lg0/l/a/l$b;,
        Lg0/l/a/l$d;,
        Lg0/l/a/l$c;,
        Lg0/l/a/l$f;,
        Lg0/l/a/l$g;,
        Lg0/l/a/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lrx/Emitter<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public final e:Lrx/Emitter$BackpressureMode;


# direct methods
.method public constructor <init>(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action1<",
            "Lrx/Emitter<",
            "TT;>;>;",
            "Lrx/Emitter$BackpressureMode;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/l;->d:Lrx/functions/Action1;

    iput-object p2, p0, Lg0/l/a/l;->e:Lrx/Emitter$BackpressureMode;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/l;->e:Lrx/Emitter$BackpressureMode;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Lg0/l/a/l$b;

    sget v1, Lg0/l/e/h;->f:I

    invoke-direct {v0, p1, v1}, Lg0/l/a/l$b;-><init>(Lrx/Subscriber;I)V

    goto :goto_0

    :cond_0
    new-instance v0, Lg0/l/a/l$e;

    invoke-direct {v0, p1}, Lg0/l/a/l$e;-><init>(Lrx/Subscriber;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lg0/l/a/l$c;

    invoke-direct {v0, p1}, Lg0/l/a/l$c;-><init>(Lrx/Subscriber;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lg0/l/a/l$d;

    invoke-direct {v0, p1}, Lg0/l/a/l$d;-><init>(Lrx/Subscriber;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lg0/l/a/l$g;

    invoke-direct {v0, p1}, Lg0/l/a/l$g;-><init>(Lrx/Subscriber;)V

    :goto_0
    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    iget-object p1, p0, Lg0/l/a/l;->d:Lrx/functions/Action1;

    invoke-interface {p1, v0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    return-void
.end method
