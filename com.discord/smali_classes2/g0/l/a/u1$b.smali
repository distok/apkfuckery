.class public final Lg0/l/a/u1$b;
.super Lrx/Subscriber;
.source "OperatorSingle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/u1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public final e:Z

.field public final f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(Lrx/Subscriber;ZLjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;ZTT;)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    iput-boolean p2, p0, Lg0/l/a/u1$b;->e:Z

    iput-object p3, p0, Lg0/l/a/u1$b;->f:Ljava/lang/Object;

    const-wide/16 p1, 0x2

    invoke-virtual {p0, p1, p2}, Lrx/Subscriber;->request(J)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 4

    iget-boolean v0, p0, Lg0/l/a/u1$b;->i:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lg0/l/a/u1$b;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    new-instance v1, Lg0/l/b/c;

    iget-object v2, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    iget-object v3, p0, Lg0/l/a/u1$b;->g:Ljava/lang/Object;

    invoke-direct {v1, v2, v3}, Lg0/l/b/c;-><init>(Lrx/Subscriber;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lg0/l/a/u1$b;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    new-instance v1, Lg0/l/b/c;

    iget-object v2, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    iget-object v3, p0, Lg0/l/a/u1$b;->f:Ljava/lang/Object;

    invoke-direct {v1, v2, v3}, Lg0/l/b/c;-><init>(Lrx/Subscriber;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "Sequence contains no elements"

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/u1$b;->i:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/u1$b;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lg0/l/a/u1$b;->h:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lg0/l/a/u1$b;->i:Z

    iget-object p1, p0, Lg0/l/a/u1$b;->d:Lrx/Subscriber;

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Sequence contains too many elements"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lg0/l/a/u1$b;->g:Ljava/lang/Object;

    iput-boolean v1, p0, Lg0/l/a/u1$b;->h:Z

    :cond_1
    :goto_0
    return-void
.end method
