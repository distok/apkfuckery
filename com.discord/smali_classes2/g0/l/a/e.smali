.class public abstract Lg0/l/a/e;
.super Lg0/l/a/d;
.source "DeferredScalarSubscriberSafe.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/a/d<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field public h:Z


# direct methods
.method public constructor <init>(Lrx/Subscriber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TR;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lg0/l/a/d;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 7

    iget-boolean v0, p0, Lg0/l/a/e;->h:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/e;->h:Z

    iget-boolean v1, p0, Lg0/l/a/d;->e:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lg0/l/a/d;->f:Ljava/lang/Object;

    iget-object v2, p0, Lg0/l/a/d;->d:Lrx/Subscriber;

    :cond_1
    iget-object v3, p0, Lg0/l/a/d;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_6

    const/4 v5, 0x3

    if-eq v3, v5, :cond_6

    invoke-virtual {v2}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_0

    :cond_2
    if-ne v3, v0, :cond_4

    invoke-interface {v2, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v2}, Lg0/g;->onCompleted()V

    :cond_3
    iget-object v0, p0, Lg0/l/a/d;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicInteger;->lazySet(I)V

    goto :goto_0

    :cond_4
    iput-object v1, p0, Lg0/l/a/d;->f:Ljava/lang/Object;

    iget-object v3, p0, Lg0/l/a/d;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lg0/l/a/d;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    :cond_6
    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/e;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/e;->h:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lg0/l/a/d;->f:Ljava/lang/Object;

    iget-object v0, p0, Lg0/l/a/d;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
