.class public final Lg0/l/a/m1$g;
.super Lg0/l/a/m1$b;
.source "OperatorReplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/m1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/a/m1$b<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x51dae9f17ccbb88eL


# instance fields
.field public final limit:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lg0/l/a/m1$b;-><init>()V

    iput p1, p0, Lg0/l/a/m1$g;->limit:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget v0, p0, Lg0/l/a/m1$b;->size:I

    iget v1, p0, Lg0/l/a/m1$g;->limit:I

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/m1$d;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/m1$d;

    if-eqz v0, :cond_0

    iget v1, p0, Lg0/l/a/m1$b;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lg0/l/a/m1$b;->size:I

    invoke-virtual {p0, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Empty list!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method
