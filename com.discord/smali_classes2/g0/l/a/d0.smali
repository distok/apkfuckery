.class public Lg0/l/a/d0;
.super Lrx/Subscriber;
.source "OnSubscribeRefCount.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lrx/Subscriber;

.field public final synthetic e:Lrx/subscriptions/CompositeSubscription;

.field public final synthetic f:Lg0/l/a/f0;


# direct methods
.method public constructor <init>(Lg0/l/a/f0;Lrx/Subscriber;Lrx/Subscriber;Lrx/subscriptions/CompositeSubscription;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iput-object p3, p0, Lg0/l/a/d0;->d:Lrx/Subscriber;

    iput-object p4, p0, Lg0/l/a/d0;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lg0/l/a/d0;->e:Lrx/subscriptions/CompositeSubscription;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->d:Lg0/m/c;

    instance-of v1, v0, Lrx/Subscription;

    if-eqz v1, :cond_0

    check-cast v0, Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lg0/l/a/d0;->f:Lg0/l/a/f0;

    iget-object v1, v1, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public onCompleted()V
    .locals 1

    invoke-virtual {p0}, Lg0/l/a/d0;->a()V

    iget-object v0, p0, Lg0/l/a/d0;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0}, Lg0/l/a/d0;->a()V

    iget-object v0, p0, Lg0/l/a/d0;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/d0;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method
