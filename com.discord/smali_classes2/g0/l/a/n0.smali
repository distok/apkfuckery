.class public final Lg0/l/a/n0;
.super Ljava/lang/Object;
.source "OnSubscribeTimerPeriodically.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:J

.field public final e:J

.field public final f:Ljava/util/concurrent/TimeUnit;

.field public final g:Lrx/Scheduler;


# direct methods
.method public constructor <init>(JJLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lg0/l/a/n0;->d:J

    iput-wide p3, p0, Lg0/l/a/n0;->e:J

    iput-object p5, p0, Lg0/l/a/n0;->f:Ljava/util/concurrent/TimeUnit;

    iput-object p6, p0, Lg0/l/a/n0;->g:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 8

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/n0;->g:Lrx/Scheduler;

    invoke-virtual {v0}, Lrx/Scheduler;->a()Lrx/Scheduler$Worker;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    new-instance v2, Lg0/l/a/m0;

    invoke-direct {v2, p0, p1, v1}, Lg0/l/a/m0;-><init>(Lg0/l/a/n0;Lrx/Subscriber;Lrx/Scheduler$Worker;)V

    iget-wide v3, p0, Lg0/l/a/n0;->d:J

    iget-wide v5, p0, Lg0/l/a/n0;->e:J

    iget-object v7, p0, Lg0/l/a/n0;->f:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v7}, Lrx/Scheduler$Worker;->c(Lrx/functions/Action0;JJLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    return-void
.end method
