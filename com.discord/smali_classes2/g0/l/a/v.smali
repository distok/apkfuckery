.class public final Lg0/l/a/v;
.super Ljava/lang/Object;
.source "OnSubscribeMap.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/v$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TR;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "-TT;+TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;Lg0/k/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;",
            "Lg0/k/b<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/v;->d:Lrx/Observable;

    iput-object p2, p0, Lg0/l/a/v;->e:Lg0/k/b;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/v$a;

    iget-object v1, p0, Lg0/l/a/v;->e:Lg0/k/b;

    invoke-direct {v0, p1, v1}, Lg0/l/a/v$a;-><init>(Lrx/Subscriber;Lg0/k/b;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object p1, p0, Lg0/l/a/v;->d:Lrx/Observable;

    invoke-virtual {p1, v0}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
