.class public Lg0/l/a/m1$b;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "OperatorReplay.java"

# interfaces
.implements Lg0/l/a/m1$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/m1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference<",
        "Lg0/l/a/m1$d;",
        ">;",
        "Lg0/l/a/m1$e<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2090aef8efde5e9eL


# instance fields
.field public index:J

.field public size:I

.field public tail:Lg0/l/a/m1$d;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v0, Lg0/l/a/m1$d;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lg0/l/a/m1$d;-><init>(Ljava/lang/Object;J)V

    iput-object v0, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    invoke-virtual {p0, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public final complete()V
    .locals 6

    sget-object v0, Lg0/l/a/h;->a:Ljava/lang/Object;

    new-instance v1, Lg0/l/a/m1$d;

    iget-wide v2, p0, Lg0/l/a/m1$b;->index:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lg0/l/a/m1$b;->index:J

    invoke-direct {v1, v0, v2, v3}, Lg0/l/a/m1$d;-><init>(Ljava/lang/Object;J)V

    iget-object v0, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iput-object v1, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    iget v0, p0, Lg0/l/a/m1$b;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lg0/l/a/m1$b;->size:I

    return-void
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_0
    new-instance v0, Lg0/l/a/m1$d;

    iget-wide v1, p0, Lg0/l/a/m1$b;->index:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lg0/l/a/m1$b;->index:J

    invoke-direct {v0, p1, v1, v2}, Lg0/l/a/m1$d;-><init>(Ljava/lang/Object;J)V

    iget-object p1, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iput-object v0, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    iget p1, p0, Lg0/l/a/m1$b;->size:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lg0/l/a/m1$b;->size:I

    invoke-virtual {p0}, Lg0/l/a/m1$b;->a()V

    return-void
.end method

.method public final h(Lg0/l/a/m1$c;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/l/a/m1$c<",
            "TT;>;)V"
        }
    .end annotation

    monitor-enter p1

    :try_start_0
    iget-boolean v0, p1, Lg0/l/a/m1$c;->emitting:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, p1, Lg0/l/a/m1$c;->missed:Z

    monitor-exit p1

    return-void

    :cond_0
    iput-boolean v1, p1, Lg0/l/a/m1$c;->emitting:Z

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_0
    invoke-virtual {p1}, Lg0/l/a/m1$c;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p1, Lg0/l/a/m1$c;->index:Ljava/lang/Object;

    check-cast v0, Lg0/l/a/m1$d;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/m1$d;

    iput-object v0, p1, Lg0/l/a/m1$c;->index:Ljava/lang/Object;

    iget-wide v1, v0, Lg0/l/a/m1$d;->index:J

    invoke-virtual {p1, v1, v2}, Lg0/l/a/m1$c;->a(J)V

    :cond_2
    invoke-virtual {p1}, Lg0/l/a/m1$c;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_3

    return-void

    :cond_3
    iget-object v1, p1, Lg0/l/a/m1$c;->child:Lrx/Subscriber;

    if-nez v1, :cond_4

    return-void

    :cond_4
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-wide v6, v4

    :goto_1
    cmp-long v8, v6, v2

    if-eqz v8, :cond_8

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lg0/l/a/m1$d;

    if-eqz v8, :cond_8

    iget-object v0, v8, Lg0/l/a/m1$d;->value:Ljava/lang/Object;

    const/4 v9, 0x0

    :try_start_1
    invoke-static {v1, v0}, Lg0/l/a/h;->a(Lg0/g;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iput-object v9, p1, Lg0/l/a/m1$c;->index:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_5
    const-wide/16 v9, 0x1

    add-long/2addr v6, v9

    invoke-virtual {p1}, Lg0/l/a/m1$c;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_6

    return-void

    :cond_6
    move-object v0, v8

    goto :goto_1

    :catchall_0
    move-exception v2

    iput-object v9, p1, Lg0/l/a/m1$c;->index:Ljava/lang/Object;

    invoke-static {v2}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-virtual {p1}, Lg0/l/a/m1$c;->unsubscribe()V

    instance-of p1, v0, Lg0/l/a/h$c;

    if-nez p1, :cond_7

    invoke-static {v0}, Lg0/l/a/h;->c(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    invoke-static {v0}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {v2, p1}, Lrx/exceptions/OnErrorThrowable;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-interface {v1, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_7
    return-void

    :cond_8
    cmp-long v1, v6, v4

    if-eqz v1, :cond_9

    iput-object v0, p1, Lg0/l/a/m1$c;->index:Ljava/lang/Object;

    const-wide v0, 0x7fffffffffffffffL

    cmp-long v4, v2, v0

    if-eqz v4, :cond_9

    invoke-virtual {p1, v6, v7}, Lg0/l/a/m1$c;->b(J)J

    :cond_9
    monitor-enter p1

    :try_start_2
    iget-boolean v0, p1, Lg0/l/a/m1$c;->missed:Z

    const/4 v1, 0x0

    if-nez v0, :cond_a

    iput-boolean v1, p1, Lg0/l/a/m1$c;->emitting:Z

    monitor-exit p1

    return-void

    :cond_a
    iput-boolean v1, p1, Lg0/l/a/m1$c;->missed:Z

    monitor-exit p1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method

.method public final j(Ljava/lang/Throwable;)V
    .locals 5

    new-instance v0, Lg0/l/a/h$c;

    invoke-direct {v0, p1}, Lg0/l/a/h$c;-><init>(Ljava/lang/Throwable;)V

    new-instance p1, Lg0/l/a/m1$d;

    iget-wide v1, p0, Lg0/l/a/m1$b;->index:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lg0/l/a/m1$b;->index:J

    invoke-direct {p1, v0, v1, v2}, Lg0/l/a/m1$d;-><init>(Ljava/lang/Object;J)V

    iget-object v0, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iput-object p1, p0, Lg0/l/a/m1$b;->tail:Lg0/l/a/m1$d;

    iget p1, p0, Lg0/l/a/m1$b;->size:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lg0/l/a/m1$b;->size:I

    return-void
.end method
