.class public Lg0/l/a/q0$b$a;
.super Ljava/lang/Object;
.source "OperatorBufferWithTime.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/a/q0$b;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Ljava/util/List;

.field public final synthetic e:Lg0/l/a/q0$b;


# direct methods
.method public constructor <init>(Lg0/l/a/q0$b;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/q0$b$a;->e:Lg0/l/a/q0$b;

    iput-object p2, p0, Lg0/l/a/q0$b$a;->d:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    iget-object v0, p0, Lg0/l/a/q0$b$a;->e:Lg0/l/a/q0$b;

    iget-object v1, p0, Lg0/l/a/q0$b$a;->d:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-boolean v2, v0, Lg0/l/a/q0$b;->g:Z

    if-eqz v2, :cond_0

    monitor-exit v0

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lg0/l/a/q0$b;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-ne v3, v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_3

    :try_start_1
    iget-object v2, v0, Lg0/l/a/q0$b;->d:Lrx/Subscriber;

    invoke-interface {v2, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-static {v1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lg0/l/a/q0$b;->onError(Ljava/lang/Throwable;)V

    :cond_3
    :goto_1
    return-void

    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method
