.class public Lg0/l/a/c0;
.super Ljava/lang/Object;
.source "OnSubscribeRefCount.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lrx/Subscriber;

.field public final synthetic e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic f:Lg0/l/a/f0;


# direct methods
.method public constructor <init>(Lg0/l/a/f0;Lrx/Subscriber;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/c0;->f:Lg0/l/a/f0;

    iput-object p2, p0, Lg0/l/a/c0;->d:Lrx/Subscriber;

    iput-object p3, p0, Lg0/l/a/c0;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Lrx/Subscription;

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lg0/l/a/c0;->f:Lg0/l/a/f0;

    iget-object v1, v1, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v1, p1}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    iget-object p1, p0, Lg0/l/a/c0;->f:Lg0/l/a/f0;

    iget-object v1, p0, Lg0/l/a/c0;->d:Lrx/Subscriber;

    iget-object v2, p1, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    new-instance v3, Lg0/l/a/e0;

    invoke-direct {v3, p1, v2}, Lg0/l/a/e0;-><init>(Lg0/l/a/f0;Lrx/subscriptions/CompositeSubscription;)V

    new-instance v4, Lg0/r/a;

    invoke-direct {v4, v3}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {v1, v4}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v3, p1, Lg0/l/a/f0;->d:Lg0/m/c;

    new-instance v4, Lg0/l/a/d0;

    invoke-direct {v4, p1, v1, v1, v2}, Lg0/l/a/d0;-><init>(Lg0/l/a/f0;Lrx/Subscriber;Lrx/Subscriber;Lrx/subscriptions/CompositeSubscription;)V

    invoke-virtual {v3, v4}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lg0/l/a/c0;->f:Lg0/l/a/f0;

    iget-object p1, p1, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {p1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-object p1, p0, Lg0/l/a/c0;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void

    :catchall_0
    move-exception p1

    iget-object v1, p0, Lg0/l/a/c0;->f:Lg0/l/a/f0;

    iget-object v1, v1, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-object v1, p0, Lg0/l/a/c0;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw p1
.end method
