.class public Lg0/l/a/x1;
.super Lrx/Subscriber;
.source "OperatorSkipWhile.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Z

.field public e:I

.field public final synthetic f:Lrx/Subscriber;

.field public final synthetic g:Lg0/l/a/z1;


# direct methods
.method public constructor <init>(Lg0/l/a/z1;Lrx/Subscriber;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/x1;->g:Lg0/l/a/z1;

    iput-object p3, p0, Lg0/l/a/x1;->f:Lrx/Subscriber;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/x1;->d:Z

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/l/a/x1;->f:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/x1;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/x1;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/x1;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lg0/l/a/x1;->g:Lg0/l/a/z1;

    iget-object v0, v0, Lg0/l/a/z1;->d:Lrx/functions/Func2;

    iget v1, p0, Lg0/l/a/x1;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lg0/l/a/x1;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lrx/functions/Func2;->call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lg0/l/a/x1;->d:Z

    iget-object v0, p0, Lg0/l/a/x1;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lg0/l/a/x1;->f:Lrx/Subscriber;

    invoke-static {v0, v1, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void
.end method
