.class public final Lg0/l/a/d1$a;
.super Lrx/Subscriber;
.source "OperatorOnBackpressureBuffer.java"

# interfaces
.implements Lg0/l/e/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/d1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;",
        "Lg0/l/e/c$a;"
    }
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/concurrent/atomic/AtomicLong;

.field public final f:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final h:Lg0/l/e/c;

.field public final i:Lrx/functions/Action0;

.field public final j:Lg0/a$b;


# direct methods
.method public constructor <init>(Lrx/Subscriber;Ljava/lang/Long;Lrx/functions/Action0;Lg0/a$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;",
            "Ljava/lang/Long;",
            "Lrx/functions/Action0;",
            "Lg0/a$b;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    new-instance p2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object p2, p0, Lg0/l/a/d1$a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lg0/l/a/d1$a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lg0/l/a/d1$a;->f:Lrx/Subscriber;

    const/4 p1, 0x0

    iput-object p1, p0, Lg0/l/a/d1$a;->e:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p1, p0, Lg0/l/a/d1$a;->i:Lrx/functions/Action0;

    new-instance p1, Lg0/l/e/c;

    invoke-direct {p1, p0}, Lg0/l/e/c;-><init>(Lg0/l/e/c$a;)V

    iput-object p1, p0, Lg0/l/a/d1$a;->h:Lg0/l/e/c;

    iput-object p4, p0, Lg0/l/a/d1$a;->j:Lg0/a$b;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    iget-object v0, p0, Lg0/l/a/d1$a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/d1$a;->h:Lg0/l/e/c;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lg0/l/e/c;->terminated:Z

    invoke-virtual {v0}, Lg0/l/e/c;->a()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lg0/l/a/d1$a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/d1$a;->h:Lg0/l/e/c;

    iget-boolean v1, v0, Lg0/l/e/c;->terminated:Z

    if-nez v1, :cond_0

    iput-object p1, v0, Lg0/l/e/c;->exception:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lg0/l/e/c;->terminated:Z

    invoke-virtual {v0}, Lg0/l/e/c;->a()V

    :cond_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/d1$a;->e:Ljava/util/concurrent/atomic/AtomicLong;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    :goto_0
    const/4 v1, 0x1

    goto :goto_3

    :cond_0
    iget-object v0, p0, Lg0/l/a/d1$a;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-gtz v0, :cond_4

    :try_start_0
    iget-object v0, p0, Lg0/l/a/d1$a;->j:Lg0/a$b;

    invoke-interface {v0}, Lg0/a$b;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lg0/l/a/d1$a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    iget-object v5, p0, Lg0/l/a/d1$a;->e:Ljava/util/concurrent/atomic/AtomicLong;

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J
    :try_end_0
    .catch Lrx/exceptions/MissingBackpressureException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v5, p0, Lg0/l/a/d1$a;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    iget-object v5, p0, Lg0/l/a/d1$a;->f:Lrx/Subscriber;

    invoke-interface {v5, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget-object v5, p0, Lg0/l/a/d1$a;->i:Lrx/functions/Action0;

    if-eqz v5, :cond_3

    :try_start_1
    invoke-interface {v5}, Lrx/functions/Action0;->call()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    iget-object v3, p0, Lg0/l/a/d1$a;->h:Lg0/l/e/c;

    iget-boolean v4, v3, Lg0/l/e/c;->terminated:Z

    if-nez v4, :cond_5

    iput-object v0, v3, Lg0/l/e/c;->exception:Ljava/lang/Throwable;

    iput-boolean v2, v3, Lg0/l/e/c;->terminated:Z

    invoke-virtual {v3}, Lg0/l/e/c;->a()V

    goto :goto_3

    :cond_3
    :goto_2
    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lg0/l/a/d1$a;->e:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v5, 0x1

    sub-long v5, v3, v5

    invoke-virtual {v0, v3, v4, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_5
    :goto_3
    if-nez v1, :cond_6

    return-void

    :cond_6
    iget-object v0, p0, Lg0/l/a/d1$a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-nez p1, :cond_7

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_7
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    iget-object p1, p0, Lg0/l/a/d1$a;->h:Lg0/l/e/c;

    invoke-virtual {p1}, Lg0/l/e/c;->a()V

    return-void
.end method

.method public onStart()V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    return-void
.end method
