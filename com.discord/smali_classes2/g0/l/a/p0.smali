.class public Lg0/l/a/p0;
.super Ljava/lang/Object;
.source "OperatorBufferWithTime.java"

# interfaces
.implements Lrx/functions/Action0;


# instance fields
.field public final synthetic d:Lg0/l/a/q0$a;


# direct methods
.method public constructor <init>(Lg0/l/a/q0$a;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/p0;->d:Lg0/l/a/q0$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    iget-object v0, p0, Lg0/l/a/p0;->d:Lg0/l/a/q0$a;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lg0/l/a/q0$a;->g:Z

    if-eqz v1, :cond_0

    monitor-exit v0

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lg0/l/a/q0$a;->f:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lg0/l/a/q0$a;->f:Ljava/util/List;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, v0, Lg0/l/a/q0$a;->d:Lrx/Subscriber;

    invoke-interface {v2, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lg0/l/a/q0$a;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method
