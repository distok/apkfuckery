.class public final Lg0/l/a/q0;
.super Ljava/lang/Object;
.source "OperatorBufferWithTime.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/q0$a;,
        Lg0/l/a/q0$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "Ljava/util/List<",
        "TT;>;TT;>;"
    }
.end annotation


# instance fields
.field public final d:J

.field public final e:J

.field public final f:Ljava/util/concurrent/TimeUnit;

.field public final g:I

.field public final h:Lrx/Scheduler;


# direct methods
.method public constructor <init>(JJLjava/util/concurrent/TimeUnit;ILrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lg0/l/a/q0;->d:J

    iput-wide p3, p0, Lg0/l/a/q0;->e:J

    iput-object p5, p0, Lg0/l/a/q0;->f:Ljava/util/concurrent/TimeUnit;

    iput p6, p0, Lg0/l/a/q0;->g:I

    iput-object p7, p0, Lg0/l/a/q0;->h:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/q0;->h:Lrx/Scheduler;

    invoke-virtual {v0}, Lrx/Scheduler;->a()Lrx/Scheduler$Worker;

    move-result-object v0

    new-instance v1, Lrx/observers/SerializedSubscriber;

    invoke-direct {v1, p1}, Lrx/observers/SerializedSubscriber;-><init>(Lrx/Subscriber;)V

    iget-wide v2, p0, Lg0/l/a/q0;->d:J

    iget-wide v4, p0, Lg0/l/a/q0;->e:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    new-instance v2, Lg0/l/a/q0$a;

    invoke-direct {v2, p0, v1, v0}, Lg0/l/a/q0$a;-><init>(Lg0/l/a/q0;Lrx/Subscriber;Lrx/Scheduler$Worker;)V

    invoke-virtual {v2, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v2}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v3, v2, Lg0/l/a/q0$a;->e:Lrx/Scheduler$Worker;

    new-instance v4, Lg0/l/a/p0;

    invoke-direct {v4, v2}, Lg0/l/a/p0;-><init>(Lg0/l/a/q0$a;)V

    iget-object p1, v2, Lg0/l/a/q0$a;->h:Lg0/l/a/q0;

    iget-wide v7, p1, Lg0/l/a/q0;->d:J

    iget-object v9, p1, Lg0/l/a/q0;->f:Ljava/util/concurrent/TimeUnit;

    move-wide v5, v7

    invoke-virtual/range {v3 .. v9}, Lrx/Scheduler$Worker;->c(Lrx/functions/Action0;JJLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    goto :goto_0

    :cond_0
    new-instance v2, Lg0/l/a/q0$b;

    invoke-direct {v2, p0, v1, v0}, Lg0/l/a/q0$b;-><init>(Lg0/l/a/q0;Lrx/Subscriber;Lrx/Scheduler$Worker;)V

    invoke-virtual {v2, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v2}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {v2}, Lg0/l/a/q0$b;->a()V

    iget-object v3, v2, Lg0/l/a/q0$b;->e:Lrx/Scheduler$Worker;

    new-instance v4, Lg0/l/a/r0;

    invoke-direct {v4, v2}, Lg0/l/a/r0;-><init>(Lg0/l/a/q0$b;)V

    iget-object p1, v2, Lg0/l/a/q0$b;->h:Lg0/l/a/q0;

    iget-wide v7, p1, Lg0/l/a/q0;->e:J

    iget-object v9, p1, Lg0/l/a/q0;->f:Ljava/util/concurrent/TimeUnit;

    move-wide v5, v7

    invoke-virtual/range {v3 .. v9}, Lrx/Scheduler$Worker;->c(Lrx/functions/Action0;JJLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    :goto_0
    return-object v2
.end method
