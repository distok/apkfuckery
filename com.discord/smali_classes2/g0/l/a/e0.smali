.class public Lg0/l/a/e0;
.super Ljava/lang/Object;
.source "OnSubscribeRefCount.java"

# interfaces
.implements Lrx/functions/Action0;


# instance fields
.field public final synthetic d:Lrx/subscriptions/CompositeSubscription;

.field public final synthetic e:Lg0/l/a/f0;


# direct methods
.method public constructor <init>(Lg0/l/a/f0;Lrx/subscriptions/CompositeSubscription;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iput-object p2, p0, Lg0/l/a/e0;->d:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 2

    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lg0/l/a/e0;->d:Lrx/subscriptions/CompositeSubscription;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->d:Lg0/m/c;

    instance-of v1, v0, Lrx/Subscription;

    if-eqz v1, :cond_0

    check-cast v0, Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lg0/l/a/f0;->e:Lrx/subscriptions/CompositeSubscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v0, v0, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lg0/l/a/e0;->e:Lg0/l/a/f0;

    iget-object v1, v1, Lg0/l/a/f0;->g:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
