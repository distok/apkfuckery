.class public Lg0/l/a/e2$a;
.super Ljava/lang/Object;
.source "OperatorTake.java"

# interfaces
.implements Lrx/Producer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/a/e2;->setProducer(Lrx/Producer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/atomic/AtomicLong;

.field public final synthetic e:Lrx/Producer;

.field public final synthetic f:Lg0/l/a/e2;


# direct methods
.method public constructor <init>(Lg0/l/a/e2;Lrx/Producer;)V
    .locals 2

    iput-object p1, p0, Lg0/l/a/e2$a;->f:Lg0/l/a/e2;

    iput-object p2, p0, Lg0/l/a/e2$a;->e:Lrx/Producer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v0, 0x0

    invoke-direct {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object p1, p0, Lg0/l/a/e2$a;->d:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method


# virtual methods
.method public l(J)V
    .locals 9

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_2

    iget-object v2, p0, Lg0/l/a/e2$a;->f:Lg0/l/a/e2;

    iget-boolean v2, v2, Lg0/l/a/e2;->e:Z

    if-nez v2, :cond_2

    :cond_0
    iget-object v2, p0, Lg0/l/a/e2$a;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-object v4, p0, Lg0/l/a/e2$a;->f:Lg0/l/a/e2;

    iget-object v4, v4, Lg0/l/a/e2;->g:Lg0/l/a/f2;

    iget v4, v4, Lg0/l/a/f2;->d:I

    int-to-long v4, v4

    sub-long/2addr v4, v2

    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    cmp-long v6, v4, v0

    if-nez v6, :cond_1

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lg0/l/a/e2$a;->d:Ljava/util/concurrent/atomic/AtomicLong;

    add-long v7, v2, v4

    invoke-virtual {v6, v2, v3, v7, v8}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p1, p0, Lg0/l/a/e2$a;->e:Lrx/Producer;

    invoke-interface {p1, v4, v5}, Lrx/Producer;->l(J)V

    :cond_2
    :goto_0
    return-void
.end method
