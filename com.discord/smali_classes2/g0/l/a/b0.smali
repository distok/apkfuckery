.class public final Lg0/l/a/b0;
.super Ljava/lang/Object;
.source "OnSubscribeRedo.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "-",
            "Lrx/Observable<",
            "+",
            "Lg0/f<",
            "*>;>;+",
            "Lrx/Observable<",
            "*>;>;"
        }
    .end annotation
.end field

.field public final f:Z

.field public final g:Z


# direct methods
.method public constructor <init>(Lrx/Observable;Lg0/k/b;ZZLrx/Scheduler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;",
            "Lg0/k/b<",
            "-",
            "Lrx/Observable<",
            "+",
            "Lg0/f<",
            "*>;>;+",
            "Lrx/Observable<",
            "*>;>;ZZ",
            "Lrx/Scheduler;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/b0;->d:Lrx/Observable;

    iput-object p2, p0, Lg0/l/a/b0;->e:Lg0/k/b;

    iput-boolean p3, p0, Lg0/l/a/b0;->f:Z

    iput-boolean p4, p0, Lg0/l/a/b0;->g:Z

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 14

    check-cast p1, Lrx/Subscriber;

    new-instance v8, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x1

    invoke-direct {v8, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    new-instance v9, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v9}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    new-instance v10, Lg0/l/c/m$a;

    invoke-direct {v10}, Lg0/l/c/m$a;-><init>()V

    invoke-virtual {p1, v10}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    new-instance v6, Lrx/subscriptions/SerialSubscription;

    invoke-direct {v6}, Lrx/subscriptions/SerialSubscription;-><init>()V

    invoke-virtual {p1, v6}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    new-instance v7, Lrx/subjects/SerializedSubject;

    invoke-direct {v7, v0}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    sget-object v0, Lg0/n/a;->a:Lg0/g;

    new-instance v1, Lg0/n/d;

    invoke-direct {v1, v0}, Lg0/n/d;-><init>(Lg0/g;)V

    invoke-virtual {v7, v1}, Lrx/Observable;->P(Lrx/Subscriber;)Lrx/Subscription;

    new-instance v11, Lg0/l/b/a;

    invoke-direct {v11}, Lg0/l/b/a;-><init>()V

    new-instance v12, Lg0/l/a/w;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, v7

    move-object v4, v11

    move-object v5, v9

    invoke-direct/range {v0 .. v6}, Lg0/l/a/w;-><init>(Lg0/l/a/b0;Lrx/Subscriber;Lrx/subjects/Subject;Lg0/l/b/a;Ljava/util/concurrent/atomic/AtomicLong;Lrx/subscriptions/SerialSubscription;)V

    iget-object v0, p0, Lg0/l/a/b0;->e:Lg0/k/b;

    new-instance v1, Lg0/l/a/y;

    invoke-direct {v1, p0}, Lg0/l/a/y;-><init>(Lg0/l/a/b0;)V

    new-instance v2, Lg0/l/a/u;

    iget-object v3, v7, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v3, v1}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lrx/Observable;

    new-instance v13, Lg0/l/a/z;

    move-object v0, v13

    move-object v1, p0

    move-object v3, p1

    move-object v4, v9

    move-object v5, v10

    move-object v6, v12

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lg0/l/a/z;-><init>(Lg0/l/a/b0;Lrx/Observable;Lrx/Subscriber;Ljava/util/concurrent/atomic/AtomicLong;Lrx/Scheduler$Worker;Lrx/functions/Action0;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v10, v13}, Lg0/l/c/m$a;->a(Lrx/functions/Action0;)Lrx/Subscription;

    new-instance v7, Lg0/l/a/a0;

    move-object v0, v7

    move-object v2, v9

    move-object v3, v11

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lg0/l/a/a0;-><init>(Lg0/l/a/b0;Ljava/util/concurrent/atomic/AtomicLong;Lg0/l/b/a;Ljava/util/concurrent/atomic/AtomicBoolean;Lrx/Scheduler$Worker;Lrx/functions/Action0;)V

    invoke-virtual {p1, v7}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-void
.end method
