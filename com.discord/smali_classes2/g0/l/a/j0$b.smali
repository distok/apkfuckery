.class public final Lg0/l/a/j0$b;
.super Lrx/Subscriber;
.source "OnSubscribeTimeoutTimedWithFallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/j0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/j0$b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public final e:J

.field public final f:Ljava/util/concurrent/TimeUnit;

.field public final g:Lrx/Scheduler$Worker;

.field public final h:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "+TT;>;"
        }
    .end annotation
.end field

.field public final i:Lg0/l/b/a;

.field public final j:Ljava/util/concurrent/atomic/AtomicLong;

.field public final k:Lg0/l/d/a;

.field public final l:Lg0/l/d/a;

.field public m:J


# direct methods
.method public constructor <init>(Lrx/Subscriber;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler$Worker;Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler$Worker;",
            "Lrx/Observable<",
            "+TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/j0$b;->d:Lrx/Subscriber;

    iput-wide p2, p0, Lg0/l/a/j0$b;->e:J

    iput-object p4, p0, Lg0/l/a/j0$b;->f:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lg0/l/a/j0$b;->g:Lrx/Scheduler$Worker;

    iput-object p6, p0, Lg0/l/a/j0$b;->h:Lrx/Observable;

    new-instance p1, Lg0/l/b/a;

    invoke-direct {p1}, Lg0/l/b/a;-><init>()V

    iput-object p1, p0, Lg0/l/a/j0$b;->i:Lg0/l/b/a;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/j0$b;->j:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance p1, Lg0/l/d/a;

    invoke-direct {p1}, Lg0/l/d/a;-><init>()V

    iput-object p1, p0, Lg0/l/a/j0$b;->k:Lg0/l/d/a;

    new-instance p2, Lg0/l/d/a;

    invoke-direct {p2, p0}, Lg0/l/d/a;-><init>(Lrx/Subscription;)V

    iput-object p2, p0, Lg0/l/a/j0$b;->l:Lg0/l/d/a;

    invoke-virtual {p0, p5}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p0, p1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 5

    iget-object v0, p0, Lg0/l/a/j0$b;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg0/l/a/j0$b;->k:Lg0/l/d/a;

    invoke-virtual {v0}, Lg0/l/d/a;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/j0$b;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    iget-object v0, p0, Lg0/l/a/j0$b;->g:Lrx/Scheduler$Worker;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 5

    iget-object v0, p0, Lg0/l/a/j0$b;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg0/l/a/j0$b;->k:Lg0/l/d/a;

    invoke-virtual {v0}, Lg0/l/d/a;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/j0$b;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    iget-object p1, p0, Lg0/l/a/j0$b;->g:Lrx/Scheduler$Worker;

    invoke-interface {p1}, Lrx/Subscription;->unsubscribe()V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/j0$b;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    iget-object v2, p0, Lg0/l/a/j0$b;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v3, 0x1

    add-long v5, v0, v3

    invoke-virtual {v2, v0, v1, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lg0/l/a/j0$b;->k:Lg0/l/d/a;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    iget-wide v0, p0, Lg0/l/a/j0$b;->m:J

    add-long/2addr v0, v3

    iput-wide v0, p0, Lg0/l/a/j0$b;->m:J

    iget-object v0, p0, Lg0/l/a/j0$b;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lg0/l/a/j0$b;->k:Lg0/l/d/a;

    iget-object v0, p0, Lg0/l/a/j0$b;->g:Lrx/Scheduler$Worker;

    new-instance v1, Lg0/l/a/j0$b$a;

    invoke-direct {v1, p0, v5, v6}, Lg0/l/a/j0$b$a;-><init>(Lg0/l/a/j0$b;J)V

    iget-wide v2, p0, Lg0/l/a/j0$b;->e:J

    iget-object v4, p0, Lg0/l/a/j0$b;->f:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object v0

    invoke-virtual {p1, v0}, Lg0/l/d/a;->a(Lrx/Subscription;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/j0$b;->i:Lg0/l/b/a;

    invoke-virtual {v0, p1}, Lg0/l/b/a;->c(Lrx/Producer;)V

    return-void
.end method
