.class public final Lg0/l/a/j1;
.super Ljava/lang/Object;
.source "OperatorPublish.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/j1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Lrx/Subscriber;

    :cond_0
    :goto_0
    iget-object v0, p0, Lg0/l/a/j1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/k1$b;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_1
    new-instance v1, Lg0/l/a/k1$b;

    iget-object v2, p0, Lg0/l/a/j1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, v2}, Lg0/l/a/k1$b;-><init>(Ljava/util/concurrent/atomic/AtomicReference;)V

    new-instance v2, Lg0/l/a/l1;

    invoke-direct {v2, v1}, Lg0/l/a/l1;-><init>(Lg0/l/a/k1$b;)V

    new-instance v3, Lg0/r/a;

    invoke-direct {v3, v2}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {v1, v3}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v2, p0, Lg0/l/a/j1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    move-object v0, v1

    :cond_3
    new-instance v1, Lg0/l/a/k1$a;

    invoke-direct {v1, v0, p1}, Lg0/l/a/k1$a;-><init>(Lg0/l/a/k1$b;Lrx/Subscriber;)V

    :cond_4
    iget-object v2, v0, Lg0/l/a/k1$b;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lg0/l/a/k1$a;

    sget-object v3, Lg0/l/a/k1$b;->l:[Lg0/l/a/k1$a;

    const/4 v4, 0x0

    if-ne v2, v3, :cond_5

    goto :goto_1

    :cond_5
    array-length v3, v2

    add-int/lit8 v5, v3, 0x1

    new-array v5, v5, [Lg0/l/a/k1$a;

    invoke-static {v2, v4, v5, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-object v1, v5, v3

    iget-object v3, v0, Lg0/l/a/k1$b;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v2, v5}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_0

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-void
.end method
