.class public final Lg0/l/a/k2$a;
.super Lrx/Subscriber;
.source "OperatorTakeUntilPredicate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/k2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public e:Z

.field public final synthetic f:Lg0/l/a/k2;


# direct methods
.method public constructor <init>(Lg0/l/a/k2;Lrx/Subscriber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lg0/l/a/k2$a;->f:Lg0/l/a/k2;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p2, p0, Lg0/l/a/k2$a;->d:Lrx/Subscriber;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/k2$a;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/k2$a;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/k2$a;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/k2$a;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/k2$a;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lg0/l/a/k2$a;->f:Lg0/l/a/k2;

    iget-object v1, v1, Lg0/l/a/k2;->d:Lg0/k/b;

    invoke-interface {v1, p1}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    iput-boolean v0, p0, Lg0/l/a/k2$a;->e:Z

    iget-object p1, p0, Lg0/l/a/k2$a;->d:Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    iput-boolean v0, p0, Lg0/l/a/k2$a;->e:Z

    iget-object v0, p0, Lg0/l/a/k2$a;->d:Lrx/Subscriber;

    invoke-static {v1, v0, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    return-void
.end method
