.class public final Lg0/l/a/m1$f;
.super Lrx/Subscriber;
.source "OperatorReplay.java"

# interfaces
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/m1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;",
        "Lrx/Subscription;"
    }
.end annotation


# static fields
.field public static final s:[Lg0/l/a/m1$c;


# instance fields
.field public final d:Lg0/l/a/m1$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/m1$e<",
            "TT;>;"
        }
    .end annotation
.end field

.field public e:Z

.field public volatile f:Z

.field public final g:Lg0/l/e/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/e/f<",
            "Lg0/l/a/m1$c<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public h:[Lg0/l/a/m1$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lg0/l/a/m1$c<",
            "TT;>;"
        }
    .end annotation
.end field

.field public volatile i:J

.field public j:J

.field public final k:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public l:Z

.field public m:Z

.field public n:J

.field public o:J

.field public volatile p:Lrx/Producer;

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lg0/l/a/m1$c<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lg0/l/a/m1$c;

    sput-object v0, Lg0/l/a/m1$f;->s:[Lg0/l/a/m1$c;

    return-void
.end method

.method public constructor <init>(Lg0/l/a/m1$e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/l/a/m1$e<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    new-instance p1, Lg0/l/e/f;

    invoke-direct {p1}, Lg0/l/e/f;-><init>()V

    iput-object p1, p0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    sget-object p1, Lg0/l/a/m1$f;->s:[Lg0/l/a/m1$c;

    iput-object p1, p0, Lg0/l/a/m1$f;->h:[Lg0/l/a/m1$c;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lg0/l/a/m1$f;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    return-void
.end method


# virtual methods
.method public a()[Lg0/l/a/m1$c;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lg0/l/a/m1$c<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    iget-object v1, v1, Lg0/l/e/f;->d:[Ljava/lang/Object;

    array-length v2, v1

    new-array v3, v2, [Lg0/l/a/m1$c;

    const/4 v4, 0x0

    invoke-static {v1, v4, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    monitor-exit v0

    return-object v3

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public b(JJ)V
    .locals 6

    iget-wide v0, p0, Lg0/l/a/m1$f;->o:J

    iget-object v2, p0, Lg0/l/a/m1$f;->p:Lrx/Producer;

    sub-long p3, p1, p3

    const-wide/16 v3, 0x0

    cmp-long v5, p3, v3

    if-eqz v5, :cond_3

    iput-wide p1, p0, Lg0/l/a/m1$f;->n:J

    if-eqz v2, :cond_1

    cmp-long p1, v0, v3

    if-eqz p1, :cond_0

    iput-wide v3, p0, Lg0/l/a/m1$f;->o:J

    add-long/2addr v0, p3

    invoke-interface {v2, v0, v1}, Lrx/Producer;->l(J)V

    goto :goto_0

    :cond_0
    invoke-interface {v2, p3, p4}, Lrx/Producer;->l(J)V

    goto :goto_0

    :cond_1
    add-long/2addr v0, p3

    cmp-long p1, v0, v3

    if-gez p1, :cond_2

    const-wide v0, 0x7fffffffffffffffL

    :cond_2
    iput-wide v0, p0, Lg0/l/a/m1$f;->o:J

    goto :goto_0

    :cond_3
    cmp-long p1, v0, v3

    if-eqz p1, :cond_4

    if-eqz v2, :cond_4

    iput-wide v3, p0, Lg0/l/a/m1$f;->o:J

    invoke-interface {v2, v0, v1}, Lrx/Producer;->l(J)V

    :cond_4
    :goto_0
    return-void
.end method

.method public c(Lg0/l/a/m1$c;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/l/a/m1$c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lg0/l/a/m1$f;->l:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    iget-object v0, p0, Lg0/l/a/m1$f;->q:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg0/l/a/m1$f;->q:Ljava/util/List;

    :cond_1
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iput-boolean v1, p0, Lg0/l/a/m1$f;->r:Z

    :goto_0
    iput-boolean v1, p0, Lg0/l/a/m1$f;->m:Z

    monitor-exit p0

    return-void

    :cond_3
    iput-boolean v1, p0, Lg0/l/a/m1$f;->l:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-wide v0, p0, Lg0/l/a/m1$f;->n:J

    const/4 v2, 0x0

    if-eqz p1, :cond_4

    iget-object p1, p1, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lg0/l/a/m1$f;->a()[Lg0/l/a/m1$c;

    move-result-object p1

    array-length v3, p1

    move-wide v4, v0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v3, :cond_6

    aget-object v7, p1, v6

    if-eqz v7, :cond_5

    iget-object v7, v7, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v7

    invoke-static {v4, v5, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_6
    move-wide v3, v4

    :goto_2
    invoke-virtual {p0, v3, v4, v0, v1}, Lg0/l/a/m1$f;->b(JJ)V

    :goto_3
    invoke-virtual {p0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result p1

    if-eqz p1, :cond_7

    return-void

    :cond_7
    monitor-enter p0

    :try_start_1
    iget-boolean p1, p0, Lg0/l/a/m1$f;->m:Z

    if-nez p1, :cond_8

    iput-boolean v2, p0, Lg0/l/a/m1$f;->l:Z

    monitor-exit p0

    return-void

    :cond_8
    iput-boolean v2, p0, Lg0/l/a/m1$f;->m:Z

    iget-object p1, p0, Lg0/l/a/m1$f;->q:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lg0/l/a/m1$f;->q:Ljava/util/List;

    iget-boolean v0, p0, Lg0/l/a/m1$f;->r:Z

    iput-boolean v2, p0, Lg0/l/a/m1$f;->r:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-wide v3, p0, Lg0/l/a/m1$f;->n:J

    if-eqz p1, :cond_9

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-wide v5, v3

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg0/l/a/m1$c;

    iget-object v1, v1, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    goto :goto_4

    :cond_9
    move-wide v5, v3

    :cond_a
    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lg0/l/a/m1$f;->a()[Lg0/l/a/m1$c;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v0, :cond_c

    aget-object v7, p1, v1

    if-eqz v7, :cond_b

    iget-object v7, v7, Lg0/l/a/m1$c;->totalRequested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_c
    invoke-virtual {p0, v5, v6, v3, v4}, Lg0/l/a/m1$f;->b(JJ)V

    goto :goto_3

    :catchall_0
    move-exception p1

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public d()V
    .locals 7

    iget-object v0, p0, Lg0/l/a/m1$f;->h:[Lg0/l/a/m1$c;

    iget-wide v1, p0, Lg0/l/a/m1$f;->j:J

    iget-wide v3, p0, Lg0/l/a/m1$f;->i:J

    const/4 v5, 0x0

    cmp-long v6, v1, v3

    if-eqz v6, :cond_1

    iget-object v1, p0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lg0/l/a/m1$f;->h:[Lg0/l/a/m1$c;

    iget-object v2, p0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    iget-object v2, v2, Lg0/l/e/f;->d:[Ljava/lang/Object;

    array-length v3, v2

    array-length v4, v0

    if-eq v4, v3, :cond_0

    new-array v0, v3, [Lg0/l/a/m1$c;

    iput-object v0, p0, Lg0/l/a/m1$f;->h:[Lg0/l/a/m1$c;

    :cond_0
    invoke-static {v2, v5, v0, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-wide v2, p0, Lg0/l/a/m1$f;->i:J

    iput-wide v2, p0, Lg0/l/a/m1$f;->j:J

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :goto_0
    iget-object v1, p0, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    array-length v2, v0

    :goto_1
    if-ge v5, v2, :cond_3

    aget-object v3, v0, v5

    if-eqz v3, :cond_2

    invoke-interface {v1, v3}, Lg0/l/a/m1$e;->h(Lg0/l/a/m1$c;)V

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method public onCompleted()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/m1$f;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/m1$f;->e:Z

    :try_start_0
    iget-object v0, p0, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    invoke-interface {v0}, Lg0/l/a/m1$e;->complete()V

    invoke-virtual {p0}, Lg0/l/a/m1$f;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    throw v0

    :cond_0
    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/m1$f;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/m1$f;->e:Z

    :try_start_0
    iget-object v0, p0, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    invoke-interface {v0, p1}, Lg0/l/a/m1$e;->j(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lg0/l/a/m1$f;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    throw p1

    :cond_0
    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/m1$f;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    invoke-interface {v0, p1}, Lg0/l/a/m1$e;->e(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lg0/l/a/m1$f;->d()V

    :cond_0
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/m1$f;->p:Lrx/Producer;

    if-nez v0, :cond_0

    iput-object p1, p0, Lg0/l/a/m1$f;->p:Lrx/Producer;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lg0/l/a/m1$f;->c(Lg0/l/a/m1$c;)V

    invoke-virtual {p0}, Lg0/l/a/m1$f;->d()V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Only a single producer can be set on a Subscriber."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
