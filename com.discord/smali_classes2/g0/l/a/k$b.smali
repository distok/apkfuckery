.class public final Lg0/l/a/k$b;
.super Lrx/Subscriber;
.source "OnSubscribeConcatMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TR;>;"
    }
.end annotation


# instance fields
.field public final d:Lg0/l/a/k$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/k$c<",
            "TT;TR;>;"
        }
    .end annotation
.end field

.field public e:J


# direct methods
.method public constructor <init>(Lg0/l/a/k$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/l/a/k$c<",
            "TT;TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$b;->d:Lg0/l/a/k$c;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 6

    iget-object v0, p0, Lg0/l/a/k$b;->d:Lg0/l/a/k$c;

    iget-wide v1, p0, Lg0/l/a/k$b;->e:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    iget-object v3, v0, Lg0/l/a/k$c;->g:Lg0/l/b/a;

    invoke-virtual {v3, v1, v2}, Lg0/l/b/a;->b(J)V

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lg0/l/a/k$c;->m:Z

    invoke-virtual {v0}, Lg0/l/a/k$c;->a()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 5

    iget-object v0, p0, Lg0/l/a/k$b;->d:Lg0/l/a/k$c;

    iget-wide v1, p0, Lg0/l/a/k$b;->e:J

    iget-object v3, v0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v3, p1}, Lg0/l/e/d;->f(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/Throwable;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget p1, v0, Lg0/l/a/k$c;->f:I

    if-nez p1, :cond_2

    iget-object p1, v0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, Lg0/l/e/d;->h(Ljava/util/concurrent/atomic/AtomicReference;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lg0/l/e/d;->g(Ljava/lang/Throwable;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v1, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_1
    invoke-virtual {v0}, Lrx/Subscriber;->unsubscribe()V

    goto :goto_0

    :cond_2
    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-eqz p1, :cond_3

    iget-object p1, v0, Lg0/l/a/k$c;->g:Lg0/l/b/a;

    invoke-virtual {p1, v1, v2}, Lg0/l/b/a;->b(J)V

    :cond_3
    const/4 p1, 0x0

    iput-boolean p1, v0, Lg0/l/a/k$c;->m:Z

    invoke-virtual {v0}, Lg0/l/a/k$c;->a()V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iget-wide v0, p0, Lg0/l/a/k$b;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lg0/l/a/k$b;->e:J

    iget-object v0, p0, Lg0/l/a/k$b;->d:Lg0/l/a/k$c;

    iget-object v0, v0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/k$b;->d:Lg0/l/a/k$c;

    iget-object v0, v0, Lg0/l/a/k$c;->g:Lg0/l/b/a;

    invoke-virtual {v0, p1}, Lg0/l/b/a;->c(Lrx/Producer;)V

    return-void
.end method
