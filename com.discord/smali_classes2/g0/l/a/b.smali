.class public Lg0/l/a/b;
.super Ljava/lang/Object;
.source "BlockingOperatorMostRecent.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Ljava/lang/Object;

.field public final synthetic e:Lg0/l/a/c;


# direct methods
.method public constructor <init>(Lg0/l/a/c;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/b;->e:Lg0/l/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lg0/l/a/b;->e:Lg0/l/a/c;

    iget-object v0, v0, Lg0/l/a/c;->d:Ljava/lang/Object;

    iput-object v0, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    invoke-static {v0}, Lg0/l/a/h;->c(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    if-nez v1, :cond_0

    iget-object v1, p0, Lg0/l/a/b;->e:Lg0/l/a/c;

    iget-object v1, v1, Lg0/l/a/c;->d:Ljava/lang/Object;

    iput-object v1, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    invoke-static {v1}, Lg0/l/a/h;->c(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    instance-of v2, v1, Lg0/l/a/h$c;

    if-nez v2, :cond_1

    invoke-static {v1}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v0, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    return-object v1

    :cond_1
    :try_start_1
    check-cast v1, Lg0/l/a/h$c;

    iget-object v1, v1, Lg0/l/a/h$c;->e:Ljava/lang/Throwable;

    invoke-static {v1}, Ly/a/g0;->y(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    :try_start_2
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    iput-object v0, p0, Lg0/l/a/b;->d:Ljava/lang/Object;

    throw v1
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Read only iterator"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
