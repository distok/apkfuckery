.class public Lg0/l/a/w$a;
.super Lrx/Subscriber;
.source "OnSubscribeRedo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/a/w;->call()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Z

.field public final synthetic e:Lg0/l/a/w;


# direct methods
.method public constructor <init>(Lg0/l/a/w;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    iget-boolean v0, p0, Lg0/l/a/w$a;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/w$a;->d:Z

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object v0, v0, Lg0/l/a/w;->e:Lrx/subjects/Subject;

    sget-object v1, Lg0/f;->d:Lg0/f;

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 4

    iget-boolean v0, p0, Lg0/l/a/w$a;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/w$a;->d:Z

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object v0, v0, Lg0/l/a/w;->e:Lrx/subjects/Subject;

    new-instance v1, Lg0/f;

    sget-object v2, Lg0/f$a;->e:Lg0/f$a;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, p1}, Lg0/f;-><init>(Lg0/f$a;Ljava/lang/Object;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/w$a;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object v0, v0, Lg0/l/a/w;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_0
    iget-object p1, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object p1, p1, Lg0/l/a/w;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    const-wide/16 v4, 0x1

    cmp-long p1, v0, v2

    if-eqz p1, :cond_1

    iget-object p1, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object p1, p1, Lg0/l/a/w;->g:Ljava/util/concurrent/atomic/AtomicLong;

    sub-long v2, v0, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result p1

    if-eqz p1, :cond_0

    :cond_1
    iget-object p1, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object p1, p1, Lg0/l/a/w;->f:Lg0/l/b/a;

    invoke-virtual {p1, v4, v5}, Lg0/l/b/a;->b(J)V

    :cond_2
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/w$a;->e:Lg0/l/a/w;

    iget-object v0, v0, Lg0/l/a/w;->f:Lg0/l/b/a;

    invoke-virtual {v0, p1}, Lg0/l/b/a;->c(Lrx/Producer;)V

    return-void
.end method
