.class public Lg0/l/a/x;
.super Lrx/Subscriber;
.source "OnSubscribeRedo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "Lg0/f<",
        "*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lrx/Subscriber;

.field public final synthetic e:Lg0/l/a/y;


# direct methods
.method public constructor <init>(Lg0/l/a/y;Lrx/Subscriber;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/x;->e:Lg0/l/a/y;

    iput-object p3, p0, Lg0/l/a/x;->d:Lrx/Subscriber;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/l/a/x;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/x;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lg0/f;

    iget-object v0, p1, Lg0/f;->a:Lg0/f$a;

    sget-object v1, Lg0/f$a;->f:Lg0/f$a;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lg0/l/a/x;->e:Lg0/l/a/y;

    iget-object v1, v1, Lg0/l/a/y;->d:Lg0/l/a/b0;

    iget-boolean v1, v1, Lg0/l/a/b0;->f:Z

    if-eqz v1, :cond_1

    iget-object p1, p0, Lg0/l/a/x;->d:Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    goto :goto_1

    :cond_1
    sget-object v1, Lg0/f$a;->e:Lg0/f$a;

    if-ne v0, v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_3

    iget-object v0, p0, Lg0/l/a/x;->e:Lg0/l/a/y;

    iget-object v0, v0, Lg0/l/a/y;->d:Lg0/l/a/b0;

    iget-boolean v0, v0, Lg0/l/a/b0;->g:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lg0/l/a/x;->d:Lrx/Subscriber;

    iget-object p1, p1, Lg0/f;->b:Ljava/lang/Throwable;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lg0/l/a/x;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-interface {p1, v0, v1}, Lrx/Producer;->l(J)V

    return-void
.end method
