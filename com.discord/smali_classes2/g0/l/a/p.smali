.class public Lg0/l/a/p;
.super Ljava/lang/Object;
.source "OnSubscribeFlattenIterable.java"

# interfaces
.implements Lrx/Producer;


# instance fields
.field public final synthetic d:Lg0/l/a/q$a;


# direct methods
.method public constructor <init>(Lg0/l/a/q;Lg0/l/a/q$a;)V
    .locals 0

    iput-object p2, p0, Lg0/l/a/p;->d:Lg0/l/a/q$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public l(J)V
    .locals 4

    iget-object v0, p0, Lg0/l/a/p;->d:Lg0/l/a/q$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-lez v3, :cond_0

    iget-object v1, v0, Lg0/l/a/q$a;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v1, p1, p2}, Ly/a/g0;->p(Ljava/util/concurrent/atomic/AtomicLong;J)J

    invoke-virtual {v0}, Lg0/l/a/q$a;->b()V

    goto :goto_0

    :cond_0
    if-ltz v3, :cond_1

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "n >= 0 required but it was "

    invoke-static {v1, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
