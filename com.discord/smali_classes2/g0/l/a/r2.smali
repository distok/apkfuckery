.class public final Lg0/l/a/r2;
.super Ljava/lang/Object;
.source "OperatorToObservableSortedList.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/r2$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "Ljava/util/List<",
        "TT;>;TT;>;"
    }
.end annotation


# static fields
.field public static final e:Ljava/util/Comparator;


# instance fields
.field public final d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg0/l/a/r2$b;

    invoke-direct {v0}, Lg0/l/a/r2$b;-><init>()V

    sput-object v0, Lg0/l/a/r2;->e:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object p1, Lg0/l/a/r2;->e:Ljava/util/Comparator;

    iput-object p1, p0, Lg0/l/a/r2;->d:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lrx/functions/Func2;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Func2<",
            "-TT;-TT;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p2, Lg0/l/a/r2$a;

    invoke-direct {p2, p0, p1}, Lg0/l/a/r2$a;-><init>(Lg0/l/a/r2;Lrx/functions/Func2;)V

    iput-object p2, p0, Lg0/l/a/r2;->d:Ljava/util/Comparator;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/b/b;

    invoke-direct {v0, p1}, Lg0/l/b/b;-><init>(Lrx/Subscriber;)V

    new-instance v1, Lg0/l/a/s2;

    invoke-direct {v1, p0, v0, p1}, Lg0/l/a/s2;-><init>(Lg0/l/a/r2;Lg0/l/b/b;Lrx/Subscriber;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-object v1
.end method
