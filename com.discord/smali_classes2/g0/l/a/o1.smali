.class public final Lg0/l/a/o1;
.super Ljava/lang/Object;
.source "OperatorReplay.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicReference;

.field public final synthetic e:Lrx/functions/Func0;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/atomic/AtomicReference;Lrx/functions/Func0;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/o1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p2, p0, Lg0/l/a/o1;->e:Lrx/functions/Func0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 7

    check-cast p1, Lrx/Subscriber;

    :goto_0
    iget-object v0, p0, Lg0/l/a/o1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/m1$f;

    if-nez v0, :cond_1

    new-instance v1, Lg0/l/a/m1$f;

    iget-object v2, p0, Lg0/l/a/o1;->e:Lrx/functions/Func0;

    invoke-interface {v2}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lg0/l/a/m1$e;

    invoke-direct {v1, v2}, Lg0/l/a/m1$f;-><init>(Lg0/l/a/m1$e;)V

    new-instance v2, Lg0/l/a/p1;

    invoke-direct {v2, v1}, Lg0/l/a/p1;-><init>(Lg0/l/a/m1$f;)V

    new-instance v3, Lg0/r/a;

    invoke-direct {v3, v2}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {v1, v3}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v2, p0, Lg0/l/a/o1;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :cond_1
    new-instance v1, Lg0/l/a/m1$c;

    invoke-direct {v1, v0, p1}, Lg0/l/a/m1$c;-><init>(Lg0/l/a/m1$f;Lrx/Subscriber;)V

    iget-boolean v2, v0, Lg0/l/a/m1$f;->f:Z

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    iget-object v2, v0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    monitor-enter v2

    :try_start_0
    iget-boolean v3, v0, Lg0/l/a/m1$f;->f:Z

    if-eqz v3, :cond_3

    monitor-exit v2

    goto :goto_1

    :cond_3
    iget-object v3, v0, Lg0/l/a/m1$f;->g:Lg0/l/e/f;

    invoke-virtual {v3, v1}, Lg0/l/e/f;->a(Ljava/lang/Object;)Z

    iget-wide v3, v0, Lg0/l/a/m1$f;->i:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v0, Lg0/l/a/m1$f;->i:J

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v0, v0, Lg0/l/a/m1$f;->d:Lg0/l/a/m1$e;

    invoke-interface {v0, v1}, Lg0/l/a/m1$e;->h(Lg0/l/a/m1$c;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
