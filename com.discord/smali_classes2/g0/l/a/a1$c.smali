.class public final Lg0/l/a/a1$c;
.super Lrx/Subscriber;
.source "OperatorMerge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/a1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final i:I


# instance fields
.field public final d:Lg0/l/a/a1$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/a1$e<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:J

.field public volatile f:Z

.field public volatile g:Lg0/l/e/h;

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget v0, Lg0/l/e/h;->f:I

    div-int/lit8 v0, v0, 0x4

    sput v0, Lg0/l/a/a1$c;->i:I

    return-void
.end method

.method public constructor <init>(Lg0/l/a/a1$e;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/l/a/a1$e<",
            "TT;>;J)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/a1$c;->d:Lg0/l/a/a1$e;

    iput-wide p2, p0, Lg0/l/a/a1$c;->e:J

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 1

    iget v0, p0, Lg0/l/a/a1$c;->h:I

    long-to-int p2, p1

    sub-int/2addr v0, p2

    sget p1, Lg0/l/a/a1$c;->i:I

    if-le v0, p1, :cond_0

    iput v0, p0, Lg0/l/a/a1$c;->h:I

    return-void

    :cond_0
    sget p1, Lg0/l/e/h;->f:I

    iput p1, p0, Lg0/l/a/a1$c;->h:I

    sub-int/2addr p1, v0

    if-lez p1, :cond_1

    int-to-long p1, p1

    invoke-virtual {p0, p1, p2}, Lrx/Subscriber;->request(J)V

    :cond_1
    return-void
.end method

.method public onCompleted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/a1$c;->f:Z

    iget-object v0, p0, Lg0/l/a/a1$c;->d:Lg0/l/a/a1$e;

    invoke-virtual {v0}, Lg0/l/a/a1$e;->b()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/a1$c;->d:Lg0/l/a/a1$e;

    invoke-virtual {v0}, Lg0/l/a/a1$e;->d()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/a1$c;->f:Z

    iget-object p1, p0, Lg0/l/a/a1$c;->d:Lg0/l/a/a1$e;

    invoke-virtual {p1}, Lg0/l/a/a1$e;->b()V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/a1$c;->d:Lg0/l/a/a1$e;

    iget-object v1, v0, Lg0/l/a/a1$e;->g:Lg0/l/a/a1$d;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    cmp-long v7, v1, v3

    if-eqz v7, :cond_1

    monitor-enter v0

    :try_start_0
    iget-object v1, v0, Lg0/l/a/a1$e;->g:Lg0/l/a/a1$d;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v1

    iget-boolean v7, v0, Lg0/l/a/a1$e;->l:Z

    if-nez v7, :cond_0

    cmp-long v7, v1, v3

    if-eqz v7, :cond_0

    iput-boolean v5, v0, Lg0/l/a/a1$e;->l:Z

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_a

    iget-object v3, p0, Lg0/l/a/a1$c;->g:Lg0/l/e/h;

    if-eqz v3, :cond_5

    iget-object v3, v3, Lg0/l/e/h;->d:Ljava/util/Queue;

    if-eqz v3, :cond_3

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {v0, p0, p1}, Lg0/l/a/a1$e;->f(Lg0/l/a/a1$c;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lg0/l/a/a1$e;->c()V

    goto :goto_8

    :cond_5
    :goto_4
    :try_start_1
    iget-object v3, v0, Lg0/l/a/a1$e;->d:Lrx/Subscriber;

    invoke-interface {v3, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_5

    :catchall_1
    move-exception p1

    :try_start_2
    iget-boolean v3, v0, Lg0/l/a/a1$e;->e:Z

    if-nez v3, :cond_6

    invoke-static {p1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    :try_start_3
    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    invoke-virtual {p0, p1}, Lg0/l/a/a1$c;->onError(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_8

    :catchall_2
    move-exception p1

    goto :goto_6

    :cond_6
    :try_start_4
    invoke-virtual {v0}, Lg0/l/a/a1$e;->d()Ljava/util/Queue;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :goto_5
    const-wide v3, 0x7fffffffffffffffL

    cmp-long p1, v1, v3

    if-eqz p1, :cond_7

    iget-object p1, v0, Lg0/l/a/a1$e;->g:Lg0/l/a/a1$d;

    invoke-virtual {p1, v5}, Lg0/l/a/a1$d;->a(I)J

    :cond_7
    const-wide/16 v1, 0x1

    invoke-virtual {p0, v1, v2}, Lg0/l/a/a1$c;->a(J)V

    monitor-enter v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :try_start_5
    iget-boolean p1, v0, Lg0/l/a/a1$e;->m:Z

    if-nez p1, :cond_8

    iput-boolean v6, v0, Lg0/l/a/a1$e;->l:Z

    monitor-exit v0

    goto :goto_8

    :cond_8
    iput-boolean v6, v0, Lg0/l/a/a1$e;->m:Z

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    invoke-virtual {v0}, Lg0/l/a/a1$e;->c()V

    goto :goto_8

    :catchall_3
    move-exception p1

    :try_start_6
    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :try_start_7
    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_4
    move-exception p1

    const/4 v5, 0x0

    :goto_6
    if-nez v5, :cond_9

    monitor-enter v0

    :try_start_8
    iput-boolean v6, v0, Lg0/l/a/a1$e;->l:Z

    monitor-exit v0

    goto :goto_7

    :catchall_5
    move-exception p1

    monitor-exit v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    throw p1

    :cond_9
    :goto_7
    throw p1

    :cond_a
    invoke-virtual {v0, p0, p1}, Lg0/l/a/a1$e;->f(Lg0/l/a/a1$c;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lg0/l/a/a1$e;->b()V

    :goto_8
    return-void
.end method

.method public onStart()V
    .locals 2

    sget v0, Lg0/l/e/h;->f:I

    iput v0, p0, Lg0/l/a/a1$c;->h:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    return-void
.end method
