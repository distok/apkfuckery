.class public Lg0/l/a/w;
.super Ljava/lang/Object;
.source "OnSubscribeRedo.java"

# interfaces
.implements Lrx/functions/Action0;


# instance fields
.field public final synthetic d:Lrx/Subscriber;

.field public final synthetic e:Lrx/subjects/Subject;

.field public final synthetic f:Lg0/l/b/a;

.field public final synthetic g:Ljava/util/concurrent/atomic/AtomicLong;

.field public final synthetic h:Lrx/subscriptions/SerialSubscription;

.field public final synthetic i:Lg0/l/a/b0;


# direct methods
.method public constructor <init>(Lg0/l/a/b0;Lrx/Subscriber;Lrx/subjects/Subject;Lg0/l/b/a;Ljava/util/concurrent/atomic/AtomicLong;Lrx/subscriptions/SerialSubscription;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/w;->i:Lg0/l/a/b0;

    iput-object p2, p0, Lg0/l/a/w;->d:Lrx/Subscriber;

    iput-object p3, p0, Lg0/l/a/w;->e:Lrx/subjects/Subject;

    iput-object p4, p0, Lg0/l/a/w;->f:Lg0/l/b/a;

    iput-object p5, p0, Lg0/l/a/w;->g:Ljava/util/concurrent/atomic/AtomicLong;

    iput-object p6, p0, Lg0/l/a/w;->h:Lrx/subscriptions/SerialSubscription;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 2

    iget-object v0, p0, Lg0/l/a/w;->d:Lrx/Subscriber;

    invoke-virtual {v0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lg0/l/a/w$a;

    invoke-direct {v0, p0}, Lg0/l/a/w$a;-><init>(Lg0/l/a/w;)V

    iget-object v1, p0, Lg0/l/a/w;->h:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v1, v0}, Lrx/subscriptions/SerialSubscription;->a(Lrx/Subscription;)V

    iget-object v1, p0, Lg0/l/a/w;->i:Lg0/l/a/b0;

    iget-object v1, v1, Lg0/l/a/b0;->d:Lrx/Observable;

    invoke-virtual {v1, v0}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
