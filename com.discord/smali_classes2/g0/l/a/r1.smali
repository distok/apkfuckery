.class public final Lg0/l/a/r1;
.super Ljava/lang/Object;
.source "OperatorScan.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/r1$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TR;TT;>;"
    }
.end annotation


# static fields
.field public static final f:Ljava/lang/Object;


# instance fields
.field public final d:Lrx/functions/Func0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func0<",
            "TR;>;"
        }
    .end annotation
.end field

.field public final e:Lrx/functions/Func2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func2<",
            "TR;-TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lg0/l/a/r1;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lrx/functions/Func2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lrx/functions/Func2<",
            "TR;-TT;TR;>;)V"
        }
    .end annotation

    new-instance v0, Lg0/l/a/r1$a;

    invoke-direct {v0, p1}, Lg0/l/a/r1$a;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lg0/l/a/r1;->d:Lrx/functions/Func0;

    iput-object p2, p0, Lg0/l/a/r1;->e:Lrx/functions/Func2;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/r1;->d:Lrx/functions/Func0;

    invoke-interface {v0}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lg0/l/a/r1;->f:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    new-instance v0, Lg0/l/a/s1;

    invoke-direct {v0, p0, p1, p1}, Lg0/l/a/s1;-><init>(Lg0/l/a/r1;Lrx/Subscriber;Lrx/Subscriber;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lg0/l/a/r1$b;

    invoke-direct {v1, v0, p1}, Lg0/l/a/r1$b;-><init>(Ljava/lang/Object;Lrx/Subscriber;)V

    new-instance v2, Lg0/l/a/t1;

    invoke-direct {v2, p0, v0, v1}, Lg0/l/a/t1;-><init>(Lg0/l/a/r1;Ljava/lang/Object;Lg0/l/a/r1$b;)V

    invoke-virtual {p1, v2}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method
