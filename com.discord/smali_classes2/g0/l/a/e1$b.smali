.class public final Lg0/l/a/e1$b;
.super Ljava/util/concurrent/atomic/AtomicLong;
.source "OperatorOnBackpressureLatest.java"

# interfaces
.implements Lrx/Producer;
.implements Lrx/Subscription;
.implements Lg0/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/e1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicLong;",
        "Lrx/Producer;",
        "Lrx/Subscription;",
        "Lg0/g<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final d:Ljava/lang/Object;

.field private static final serialVersionUID:J = -0x12ef4cd3e08498a2L


# instance fields
.field public final child:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public volatile done:Z

.field public emitting:Z

.field public missed:Z

.field public parent:Lg0/l/a/e1$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/e1$c<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public terminal:Ljava/lang/Throwable;

.field public final value:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lg0/l/a/e1$b;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lrx/Subscriber;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/e1$b;->child:Lrx/Subscriber;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v0, Lg0/l/a/e1$b;->d:Ljava/lang/Object;

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lg0/l/a/e1$b;->value:Ljava/util/concurrent/atomic/AtomicReference;

    const-wide/high16 v0, -0x4000000000000000L    # -2.0

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->lazySet(J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 12

    sget-object v0, Lg0/l/a/e1$b;->d:Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lg0/l/a/e1$b;->emitting:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lg0/l/a/e1$b;->missed:Z

    monitor-exit p0

    return-void

    :cond_0
    iput-boolean v2, p0, Lg0/l/a/e1$b;->emitting:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lg0/l/a/e1$b;->missed:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    :goto_0
    :try_start_1
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    const-wide/high16 v5, -0x8000000000000000L

    cmp-long v7, v3, v5

    if-nez v7, :cond_1

    goto :goto_3

    :cond_1
    iget-object v5, p0, Lg0/l/a/e1$b;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    const-wide/16 v6, 0x0

    cmp-long v8, v3, v6

    if-lez v8, :cond_4

    if-eq v5, v0, :cond_4

    iget-object v3, p0, Lg0/l/a/e1$b;->child:Lrx/Subscriber;

    invoke-interface {v3, v5}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object v3, p0, Lg0/l/a/e1$b;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v5, v0}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    const-wide/16 v3, 0x1

    :cond_2
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    cmp-long v5, v8, v6

    if-gez v5, :cond_3

    goto :goto_1

    :cond_3
    sub-long v10, v8, v3

    invoke-virtual {p0, v8, v9, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_1
    move-object v5, v0

    :cond_4
    if-ne v5, v0, :cond_6

    iget-boolean v3, p0, Lg0/l/a/e1$b;->done:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lg0/l/a/e1$b;->terminal:Ljava/lang/Throwable;

    if-eqz v3, :cond_5

    iget-object v4, p0, Lg0/l/a/e1$b;->child:Lrx/Subscriber;

    invoke-interface {v4, v3}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lg0/l/a/e1$b;->child:Lrx/Subscriber;

    invoke-interface {v3}, Lg0/g;->onCompleted()V

    :cond_6
    :goto_2
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    iget-boolean v3, p0, Lg0/l/a/e1$b;->missed:Z

    if-nez v3, :cond_7

    iput-boolean v1, p0, Lg0/l/a/e1$b;->emitting:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_3
    return-void

    :cond_7
    :try_start_4
    iput-boolean v1, p0, Lg0/l/a/e1$b;->missed:Z

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v2, 0x0

    :goto_4
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_4

    :catchall_3
    move-exception v0

    const/4 v2, 0x0

    :goto_5
    if-nez v2, :cond_8

    monitor-enter p0

    :try_start_7
    iput-boolean v1, p0, Lg0/l/a/e1$b;->emitting:Z

    monitor-exit p0

    goto :goto_6

    :catchall_4
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    throw v0

    :cond_8
    :goto_6
    throw v0

    :catchall_5
    move-exception v0

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    throw v0
.end method

.method public isUnsubscribed()Z
    .locals 5

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l(J)V
    .locals 10

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_5

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v6, v2, v4

    if-nez v6, :cond_1

    return-void

    :cond_1
    const-wide/high16 v4, -0x4000000000000000L    # -2.0

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v8, v2, v4

    if-nez v8, :cond_2

    move-wide v4, p1

    goto :goto_0

    :cond_2
    add-long v4, v2, p1

    cmp-long v9, v4, v0

    if-gez v9, :cond_3

    move-wide v4, v6

    :cond_3
    :goto_0
    invoke-virtual {p0, v2, v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v8, :cond_4

    iget-object p1, p0, Lg0/l/a/e1$b;->parent:Lg0/l/a/e1$c;

    invoke-virtual {p1, v6, v7}, Lrx/Subscriber;->request(J)V

    :cond_4
    invoke-virtual {p0}, Lg0/l/a/e1$b;->a()V

    :cond_5
    return-void
.end method

.method public onCompleted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/e1$b;->done:Z

    invoke-virtual {p0}, Lg0/l/a/e1$b;->a()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/e1$b;->terminal:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/e1$b;->done:Z

    invoke-virtual {p0}, Lg0/l/a/e1$b;->a()V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/e1$b;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lg0/l/a/e1$b;->a()V

    return-void
.end method

.method public unsubscribe()V
    .locals 5

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    :cond_0
    return-void
.end method
