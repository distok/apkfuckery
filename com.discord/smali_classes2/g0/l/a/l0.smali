.class public final Lg0/l/a/l0;
.super Ljava/lang/Object;
.source "OnSubscribeTimerOnce.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:J

.field public final e:Ljava/util/concurrent/TimeUnit;

.field public final f:Lrx/Scheduler;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lg0/l/a/l0;->d:J

    iput-object p3, p0, Lg0/l/a/l0;->e:Ljava/util/concurrent/TimeUnit;

    iput-object p4, p0, Lg0/l/a/l0;->f:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/l0;->f:Lrx/Scheduler;

    invoke-virtual {v0}, Lrx/Scheduler;->a()Lrx/Scheduler$Worker;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    new-instance v1, Lg0/l/a/k0;

    invoke-direct {v1, p0, p1}, Lg0/l/a/k0;-><init>(Lg0/l/a/l0;Lrx/Subscriber;)V

    iget-wide v2, p0, Lg0/l/a/l0;->d:J

    iget-object p1, p0, Lg0/l/a/l0;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, p1}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    return-void
.end method
