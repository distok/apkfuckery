.class public Lg0/l/a/n;
.super Ljava/lang/Object;
.source "OnSubscribeDoOnEach.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/n$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lg0/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/g<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public final e:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;Lg0/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;",
            "Lg0/g<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/n;->e:Lrx/Observable;

    iput-object p2, p0, Lg0/l/a/n;->d:Lg0/g;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/a/n;->e:Lrx/Observable;

    new-instance v1, Lg0/l/a/n$a;

    iget-object v2, p0, Lg0/l/a/n;->d:Lg0/g;

    invoke-direct {v1, p1, v2}, Lg0/l/a/n$a;-><init>(Lrx/Subscriber;Lg0/g;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
