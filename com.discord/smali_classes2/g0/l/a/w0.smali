.class public Lg0/l/a/w0;
.super Lrx/Subscriber;
.source "OperatorDistinctUntilChanged.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TU;"
        }
    .end annotation
.end field

.field public e:Z

.field public final synthetic f:Lrx/Subscriber;

.field public final synthetic g:Lg0/l/a/x0;


# direct methods
.method public constructor <init>(Lg0/l/a/x0;Lrx/Subscriber;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/w0;->g:Lg0/l/a/x0;

    iput-object p3, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lg0/l/a/w0;->g:Lg0/l/a/x0;

    iget-object v0, v0, Lg0/l/a/x0;->d:Lg0/k/b;

    invoke-interface {v0, p1}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v1, p0, Lg0/l/a/w0;->d:Ljava/lang/Object;

    iput-object v0, p0, Lg0/l/a/w0;->d:Ljava/lang/Object;

    iget-boolean v2, p0, Lg0/l/a/w0;->e:Z

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lg0/l/a/w0;->g:Lg0/l/a/x0;

    iget-object v2, v2, Lg0/l/a/x0;->e:Lrx/functions/Func2;

    invoke-interface {v2, v1, v0}, Lrx/functions/Func2;->call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object v1, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-static {p1, v1, v0}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/w0;->e:Z

    iget-object v0, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lg0/l/a/w0;->f:Lrx/Subscriber;

    invoke-static {v0, v1, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void
.end method
