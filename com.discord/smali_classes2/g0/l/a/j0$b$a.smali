.class public final Lg0/l/a/j0$b$a;
.super Ljava/lang/Object;
.source "OnSubscribeTimeoutTimedWithFallback.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/j0$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final d:J

.field public final synthetic e:Lg0/l/a/j0$b;


# direct methods
.method public constructor <init>(Lg0/l/a/j0$b;J)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/j0$b$a;->e:Lg0/l/a/j0$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lg0/l/a/j0$b$a;->d:J

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    iget-object v0, p0, Lg0/l/a/j0$b$a;->e:Lg0/l/a/j0$b;

    iget-wide v1, p0, Lg0/l/a/j0$b$a;->d:J

    iget-object v3, v0, Lg0/l/a/j0$b;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide v4, 0x7fffffffffffffffL

    invoke-virtual {v3, v1, v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lrx/Subscriber;->unsubscribe()V

    iget-object v1, v0, Lg0/l/a/j0$b;->h:Lrx/Observable;

    if-nez v1, :cond_1

    iget-object v0, v0, Lg0/l/a/j0$b;->d:Lrx/Subscriber;

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v1}, Ljava/util/concurrent/TimeoutException;-><init>()V

    invoke-interface {v0, v1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    iget-wide v1, v0, Lg0/l/a/j0$b;->m:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_2

    iget-object v3, v0, Lg0/l/a/j0$b;->i:Lg0/l/b/a;

    invoke-virtual {v3, v1, v2}, Lg0/l/b/a;->b(J)V

    :cond_2
    new-instance v1, Lg0/l/a/j0$a;

    iget-object v2, v0, Lg0/l/a/j0$b;->d:Lrx/Subscriber;

    iget-object v3, v0, Lg0/l/a/j0$b;->i:Lg0/l/b/a;

    invoke-direct {v1, v2, v3}, Lg0/l/a/j0$a;-><init>(Lrx/Subscriber;Lg0/l/b/a;)V

    iget-object v2, v0, Lg0/l/a/j0$b;->l:Lg0/l/d/a;

    invoke-virtual {v2, v1}, Lg0/l/d/a;->a(Lrx/Subscription;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v0, Lg0/l/a/j0$b;->h:Lrx/Observable;

    invoke-virtual {v0, v1}, Lrx/Observable;->P(Lrx/Subscriber;)Lrx/Subscription;

    :cond_3
    :goto_0
    return-void
.end method
