.class public final Lg0/l/a/t0;
.super Ljava/lang/Object;
.source "OperatorDebounceWithTime.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/t0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final d:J

.field public final e:Ljava/util/concurrent/TimeUnit;

.field public final f:Lrx/Scheduler;


# direct methods
.method public constructor <init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lg0/l/a/t0;->d:J

    iput-object p3, p0, Lg0/l/a/t0;->e:Ljava/util/concurrent/TimeUnit;

    iput-object p4, p0, Lg0/l/a/t0;->f:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    move-object v2, p1

    check-cast v2, Lrx/Subscriber;

    iget-object p1, p0, Lg0/l/a/t0;->f:Lrx/Scheduler;

    invoke-virtual {p1}, Lrx/Scheduler;->a()Lrx/Scheduler$Worker;

    move-result-object v4

    new-instance v5, Lrx/observers/SerializedSubscriber;

    invoke-direct {v5, v2}, Lrx/observers/SerializedSubscriber;-><init>(Lrx/Subscriber;)V

    new-instance v3, Lrx/subscriptions/SerialSubscription;

    invoke-direct {v3}, Lrx/subscriptions/SerialSubscription;-><init>()V

    invoke-virtual {v5, v4}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {v5, v3}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    new-instance p1, Lg0/l/a/s0;

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lg0/l/a/s0;-><init>(Lg0/l/a/t0;Lrx/Subscriber;Lrx/subscriptions/SerialSubscription;Lrx/Scheduler$Worker;Lrx/observers/SerializedSubscriber;)V

    return-object p1
.end method
