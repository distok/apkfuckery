.class public final Lg0/l/a/i;
.super Ljava/lang/Object;
.source "OnSubscribeCombineLatest.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/i$a;,
        Lg0/l/a/i$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TR;>;"
    }
.end annotation


# instance fields
.field public final d:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "+",
            "Lrx/Observable<",
            "+TT;>;>;"
        }
    .end annotation
.end field

.field public final e:Lrx/functions/FuncN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/FuncN<",
            "+TR;>;"
        }
    .end annotation
.end field

.field public final f:I


# direct methods
.method public constructor <init>(Ljava/lang/Iterable;Lrx/functions/FuncN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lrx/Observable<",
            "+TT;>;>;",
            "Lrx/functions/FuncN<",
            "+TR;>;)V"
        }
    .end annotation

    sget v0, Lg0/l/e/h;->f:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/i;->d:Ljava/lang/Iterable;

    iput-object p2, p0, Lg0/l/a/i;->e:Lrx/functions/FuncN;

    iput v0, p0, Lg0/l/a/i;->f:I

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 8

    move-object v1, p1

    check-cast v1, Lrx/Subscriber;

    iget-object p1, p0, Lg0/l/a/i;->d:Ljava/lang/Iterable;

    instance-of v0, p1, Ljava/util/List;

    const/4 v6, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lrx/Observable;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lrx/Observable;

    array-length v0, p1

    move v3, v0

    goto :goto_1

    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [Lrx/Observable;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lrx/Observable;

    array-length v4, v0

    if-ne v2, v4, :cond_1

    shr-int/lit8 v4, v2, 0x2

    add-int/2addr v4, v2

    new-array v4, v4, [Lrx/Observable;

    invoke-static {v0, v6, v4, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v4

    :cond_1
    add-int/lit8 v4, v2, 0x1

    aput-object v3, v0, v2

    move v2, v4

    goto :goto_0

    :cond_2
    move-object p1, v0

    move v3, v2

    :goto_1
    if-nez v3, :cond_3

    invoke-interface {v1}, Lg0/g;->onCompleted()V

    goto :goto_4

    :cond_3
    new-instance v7, Lg0/l/a/i$b;

    iget-object v2, p0, Lg0/l/a/i;->e:Lrx/functions/FuncN;

    iget v4, p0, Lg0/l/a/i;->f:I

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v5}, Lg0/l/a/i$b;-><init>(Lrx/Subscriber;Lrx/functions/FuncN;IIZ)V

    iget-object v0, v7, Lg0/l/a/i$b;->subscribers:[Lg0/l/a/i$a;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_4

    new-instance v3, Lg0/l/a/i$a;

    invoke-direct {v3, v7, v2}, Lg0/l/a/i$a;-><init>(Lg0/l/a/i$b;I)V

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicInteger;->lazySet(I)V

    iget-object v2, v7, Lg0/l/a/i$b;->actual:Lrx/Subscriber;

    invoke-virtual {v2, v7}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v2, v7, Lg0/l/a/i$b;->actual:Lrx/Subscriber;

    invoke-virtual {v2, v7}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    :goto_3
    if-ge v6, v1, :cond_6

    iget-boolean v2, v7, Lg0/l/a/i$b;->cancelled:Z

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    aget-object v2, p1, v6

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Lrx/Observable;->P(Lrx/Subscriber;)Lrx/Subscription;

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_6
    :goto_4
    return-void
.end method
