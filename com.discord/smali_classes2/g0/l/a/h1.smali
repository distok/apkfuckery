.class public Lg0/l/a/h1;
.super Lrx/Subscriber;
.source "OperatorOnErrorResumeNextViaFunction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Z

.field public e:J

.field public final synthetic f:Lrx/Subscriber;

.field public final synthetic g:Lg0/l/b/a;

.field public final synthetic h:Lrx/subscriptions/SerialSubscription;

.field public final synthetic i:Lg0/l/a/i1;


# direct methods
.method public constructor <init>(Lg0/l/a/i1;Lrx/Subscriber;Lg0/l/b/a;Lrx/subscriptions/SerialSubscription;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/h1;->i:Lg0/l/a/i1;

    iput-object p2, p0, Lg0/l/a/h1;->f:Lrx/Subscriber;

    iput-object p3, p0, Lg0/l/a/h1;->g:Lg0/l/b/a;

    iput-object p4, p0, Lg0/l/a/h1;->h:Lrx/subscriptions/SerialSubscription;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/h1;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/h1;->d:Z

    iget-object v0, p0, Lg0/l/a/h1;->f:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 6

    iget-boolean v0, p0, Lg0/l/a/h1;->d:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/h1;->d:Z

    :try_start_0
    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    new-instance v0, Lg0/l/a/h1$a;

    invoke-direct {v0, p0}, Lg0/l/a/h1$a;-><init>(Lg0/l/a/h1;)V

    iget-object v1, p0, Lg0/l/a/h1;->h:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v1, v0}, Lrx/subscriptions/SerialSubscription;->a(Lrx/Subscription;)V

    iget-wide v1, p0, Lg0/l/a/h1;->e:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    iget-object v3, p0, Lg0/l/a/h1;->g:Lg0/l/b/a;

    invoke-virtual {v3, v1, v2}, Lg0/l/b/a;->b(J)V

    :cond_1
    iget-object v1, p0, Lg0/l/a/h1;->i:Lg0/l/a/i1;

    iget-object v1, v1, Lg0/l/a/i1;->d:Lg0/k/b;

    invoke-interface {v1, p1}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Observable;

    invoke-virtual {p1, v0}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lg0/l/a/h1;->f:Lrx/Subscriber;

    invoke-static {p1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/h1;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Lg0/l/a/h1;->e:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lg0/l/a/h1;->e:J

    iget-object v0, p0, Lg0/l/a/h1;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/h1;->g:Lg0/l/b/a;

    invoke-virtual {v0, p1}, Lg0/l/b/a;->c(Lrx/Producer;)V

    return-void
.end method
