.class public final Lg0/l/a/j0;
.super Ljava/lang/Object;
.source "OnSubscribeTimeoutTimedWithFallback.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/j0$a;,
        Lg0/l/a/j0$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:J

.field public final f:Ljava/util/concurrent/TimeUnit;

.field public final g:Lrx/Scheduler;

.field public final h:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/Observable;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "TT;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            "Lrx/Scheduler;",
            "Lrx/Observable<",
            "+TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/j0;->d:Lrx/Observable;

    iput-wide p2, p0, Lg0/l/a/j0;->e:J

    iput-object p4, p0, Lg0/l/a/j0;->f:Ljava/util/concurrent/TimeUnit;

    iput-object p5, p0, Lg0/l/a/j0;->g:Lrx/Scheduler;

    iput-object p6, p0, Lg0/l/a/j0;->h:Lrx/Observable;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 8

    check-cast p1, Lrx/Subscriber;

    new-instance v7, Lg0/l/a/j0$b;

    iget-wide v2, p0, Lg0/l/a/j0;->e:J

    iget-object v4, p0, Lg0/l/a/j0;->f:Ljava/util/concurrent/TimeUnit;

    iget-object v0, p0, Lg0/l/a/j0;->g:Lrx/Scheduler;

    invoke-virtual {v0}, Lrx/Scheduler;->a()Lrx/Scheduler$Worker;

    move-result-object v5

    iget-object v6, p0, Lg0/l/a/j0;->h:Lrx/Observable;

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lg0/l/a/j0$b;-><init>(Lrx/Subscriber;JLjava/util/concurrent/TimeUnit;Lrx/Scheduler$Worker;Lrx/Observable;)V

    iget-object v0, v7, Lg0/l/a/j0$b;->l:Lg0/l/d/a;

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v0, v7, Lg0/l/a/j0$b;->i:Lg0/l/b/a;

    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    iget-object p1, v7, Lg0/l/a/j0$b;->k:Lg0/l/d/a;

    iget-object v0, v7, Lg0/l/a/j0$b;->g:Lrx/Scheduler$Worker;

    new-instance v1, Lg0/l/a/j0$b$a;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v7, v2, v3}, Lg0/l/a/j0$b$a;-><init>(Lg0/l/a/j0$b;J)V

    iget-wide v2, v7, Lg0/l/a/j0$b;->e:J

    iget-object v4, v7, Lg0/l/a/j0$b;->f:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object v0

    invoke-virtual {p1, v0}, Lg0/l/d/a;->a(Lrx/Subscription;)Z

    iget-object p1, p0, Lg0/l/a/j0;->d:Lrx/Observable;

    invoke-virtual {p1, v7}, Lrx/Observable;->P(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
