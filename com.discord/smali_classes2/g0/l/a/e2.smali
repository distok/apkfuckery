.class public Lg0/l/a/e2;
.super Lrx/Subscriber;
.source "OperatorTake.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:I

.field public e:Z

.field public final synthetic f:Lrx/Subscriber;

.field public final synthetic g:Lg0/l/a/f2;


# direct methods
.method public constructor <init>(Lg0/l/a/f2;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/e2;->g:Lg0/l/a/f2;

    iput-object p2, p0, Lg0/l/a/e2;->f:Lrx/Subscriber;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/e2;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/e2;->e:Z

    iget-object v0, p0, Lg0/l/a/e2;->f:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/e2;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/e2;->e:Z

    :try_start_0
    iget-object v0, p0, Lg0/l/a/e2;->f:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    throw p1

    :cond_0
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lg0/l/a/e2;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lg0/l/a/e2;->d:I

    iget-object v2, p0, Lg0/l/a/e2;->g:Lg0/l/a/f2;

    iget v2, v2, Lg0/l/a/f2;->d:I

    if-ge v0, v2, :cond_1

    const/4 v0, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lg0/l/a/e2;->f:Lrx/Subscriber;

    invoke-interface {v2, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    if-eqz v1, :cond_1

    iget-boolean p1, p0, Lg0/l/a/e2;->e:Z

    if-nez p1, :cond_1

    iput-boolean v0, p0, Lg0/l/a/e2;->e:Z

    :try_start_0
    iget-object p1, p0, Lg0/l/a/e2;->f:Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    goto :goto_1

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    throw p1

    :cond_1
    :goto_1
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 2

    iget-object v0, p0, Lg0/l/a/e2;->f:Lrx/Subscriber;

    new-instance v1, Lg0/l/a/e2$a;

    invoke-direct {v1, p0, p1}, Lg0/l/a/e2$a;-><init>(Lg0/l/a/e2;Lrx/Producer;)V

    invoke-virtual {v0, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-void
.end method
