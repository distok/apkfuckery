.class public final Lg0/l/a/i$b;
.super Ljava/util/concurrent/atomic/AtomicInteger;
.source "OnSubscribeCombineLatest.java"

# interfaces
.implements Lrx/Producer;
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lrx/Producer;",
        "Lrx/Subscription;"
    }
.end annotation


# static fields
.field public static final d:Ljava/lang/Object;

.field private static final serialVersionUID:J = 0x76e7117251786db1L


# instance fields
.field public active:I

.field public final actual:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TR;>;"
        }
    .end annotation
.end field

.field public final bufferSize:I

.field public volatile cancelled:Z

.field public final combiner:Lrx/functions/FuncN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/FuncN<",
            "+TR;>;"
        }
    .end annotation
.end field

.field public complete:I

.field public final delayError:Z

.field public volatile done:Z

.field public final error:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public final latest:[Ljava/lang/Object;

.field public final queue:Lg0/l/e/m/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/e/m/e<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final requested:Ljava/util/concurrent/atomic/AtomicLong;

.field public final subscribers:[Lg0/l/a/i$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lg0/l/a/i$a<",
            "TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lg0/l/a/i$b;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lrx/Subscriber;Lrx/functions/FuncN;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TR;>;",
            "Lrx/functions/FuncN<",
            "+TR;>;IIZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object p1, p0, Lg0/l/a/i$b;->actual:Lrx/Subscriber;

    iput-object p2, p0, Lg0/l/a/i$b;->combiner:Lrx/functions/FuncN;

    iput p4, p0, Lg0/l/a/i$b;->bufferSize:I

    iput-boolean p5, p0, Lg0/l/a/i$b;->delayError:Z

    new-array p1, p3, [Ljava/lang/Object;

    iput-object p1, p0, Lg0/l/a/i$b;->latest:[Ljava/lang/Object;

    sget-object p2, Lg0/l/a/i$b;->d:Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    new-array p1, p3, [Lg0/l/a/i$a;

    iput-object p1, p0, Lg0/l/a/i$b;->subscribers:[Lg0/l/a/i$a;

    new-instance p1, Lg0/l/e/m/e;

    invoke-direct {p1, p4}, Lg0/l/e/m/e;-><init>(I)V

    iput-object p1, p0, Lg0/l/a/i$b;->queue:Lg0/l/e/m/e;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p1, p0, Lg0/l/a/i$b;->requested:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lg0/l/a/i$b;->error:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Queue;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue<",
            "*>;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Queue;->clear()V

    iget-object p1, p0, Lg0/l/a/i$b;->subscribers:[Lg0/l/a/i$a;

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    invoke-virtual {v2}, Lrx/Subscriber;->unsubscribe()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(ZZLrx/Subscriber;Ljava/util/Queue;Z)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lrx/Subscriber<",
            "*>;",
            "Ljava/util/Queue<",
            "*>;Z)Z"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/i$b;->cancelled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0, p4}, Lg0/l/a/i$b;->a(Ljava/util/Queue;)V

    return v1

    :cond_0
    if-eqz p1, :cond_4

    if-eqz p5, :cond_2

    if-eqz p2, :cond_4

    iget-object p1, p0, Lg0/l/a/i$b;->error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    if-eqz p1, :cond_1

    invoke-interface {p3, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    invoke-interface {p3}, Lg0/g;->onCompleted()V

    :goto_0
    return v1

    :cond_2
    iget-object p1, p0, Lg0/l/a/i$b;->error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    if-eqz p1, :cond_3

    invoke-virtual {p0, p4}, Lg0/l/a/i$b;->a(Ljava/util/Queue;)V

    invoke-interface {p3, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return v1

    :cond_3
    if-eqz p2, :cond_4

    invoke-interface {p3}, Lg0/g;->onCompleted()V

    return v1

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method public c(Ljava/lang/Object;I)V
    .locals 8

    iget-object v0, p0, Lg0/l/a/i$b;->subscribers:[Lg0/l/a/i$a;

    aget-object v0, v0, p2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lg0/l/a/i$b;->latest:[Ljava/lang/Object;

    array-length v2, v1

    aget-object v3, v1, p2

    iget v4, p0, Lg0/l/a/i$b;->active:I

    sget-object v5, Lg0/l/a/i$b;->d:Ljava/lang/Object;

    if-ne v3, v5, :cond_0

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lg0/l/a/i$b;->active:I

    :cond_0
    iget v6, p0, Lg0/l/a/i$b;->complete:I

    if-nez p1, :cond_1

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lg0/l/a/i$b;->complete:I

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v1, p2

    :goto_0
    const/4 p2, 0x0

    const/4 v1, 0x1

    if-ne v4, v2, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eq v6, v2, :cond_3

    if-nez p1, :cond_4

    if-ne v3, v5, :cond_4

    :cond_3
    const/4 p2, 0x1

    :cond_4
    if-nez p2, :cond_7

    if-eqz p1, :cond_5

    if-eqz v4, :cond_5

    iget-object p2, p0, Lg0/l/a/i$b;->queue:Lg0/l/e/m/e;

    iget-object v1, p0, Lg0/l/a/i$b;->latest:[Ljava/lang/Object;

    invoke-virtual {v1}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lg0/l/e/m/e;->e(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    if-nez p1, :cond_8

    iget-object p2, p0, Lg0/l/a/i$b;->error:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_8

    if-eq v3, v5, :cond_6

    iget-boolean p2, p0, Lg0/l/a/i$b;->delayError:Z

    if-nez p2, :cond_8

    :cond_6
    iput-boolean v1, p0, Lg0/l/a/i$b;->done:Z

    goto :goto_2

    :cond_7
    iput-boolean v1, p0, Lg0/l/a/i$b;->done:Z

    :cond_8
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_9

    if-eqz p1, :cond_9

    const-wide/16 p1, 0x1

    invoke-virtual {v0, p1, p2}, Lrx/Subscriber;->request(J)V

    return-void

    :cond_9
    invoke-virtual {p0}, Lg0/l/a/i$b;->d()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public d()V
    .locals 19

    move-object/from16 v7, p0

    invoke-virtual/range {p0 .. p0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v8, v7, Lg0/l/a/i$b;->queue:Lg0/l/e/m/e;

    iget-object v9, v7, Lg0/l/a/i$b;->actual:Lrx/Subscriber;

    iget-boolean v0, v7, Lg0/l/a/i$b;->delayError:Z

    iget-object v10, v7, Lg0/l/a/i$b;->requested:Ljava/util/concurrent/atomic/AtomicLong;

    const/4 v11, 0x1

    const/4 v12, 0x1

    :cond_1
    iget-boolean v2, v7, Lg0/l/a/i$b;->done:Z

    invoke-virtual {v8}, Lg0/l/e/m/e;->isEmpty()Z

    move-result v3

    move-object/from16 v1, p0

    move-object v4, v9

    move-object v5, v8

    move v6, v0

    invoke-virtual/range {v1 .. v6}, Lg0/l/a/i$b;->b(ZZLrx/Subscriber;Ljava/util/Queue;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    :cond_2
    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v13

    const-wide/16 v5, 0x0

    :goto_0
    cmp-long v1, v5, v13

    if-eqz v1, :cond_7

    iget-boolean v2, v7, Lg0/l/a/i$b;->done:Z

    invoke-virtual {v8}, Lg0/l/e/m/e;->peek()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lg0/l/a/i$a;

    if-nez v4, :cond_3

    const/16 v16, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    const/16 v16, 0x0

    :goto_1
    move-object/from16 v1, p0

    move/from16 v3, v16

    move-object v15, v4

    move-object v4, v9

    move-wide/from16 v17, v5

    move-object v5, v8

    move v6, v0

    invoke-virtual/range {v1 .. v6}, Lg0/l/a/i$b;->b(ZZLrx/Subscriber;Ljava/util/Queue;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    return-void

    :cond_4
    if-eqz v16, :cond_5

    move-wide/from16 v3, v17

    goto :goto_2

    :cond_5
    invoke-virtual {v8}, Lg0/l/e/m/e;->poll()Ljava/lang/Object;

    invoke-virtual {v8}, Lg0/l/e/m/e;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    if-nez v1, :cond_6

    iput-boolean v11, v7, Lg0/l/a/i$b;->cancelled:Z

    invoke-virtual {v7, v8}, Lg0/l/a/i$b;->a(Ljava/util/Queue;)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Broken queue?! Sender received but not the array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v9, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void

    :cond_6
    :try_start_0
    iget-object v2, v7, Lg0/l/a/i$b;->combiner:Lrx/functions/FuncN;

    invoke-interface {v2, v1}, Lrx/functions/FuncN;->call([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v9, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    const-wide/16 v1, 0x1

    invoke-virtual {v15, v1, v2}, Lrx/Subscriber;->request(J)V

    move-wide/from16 v3, v17

    add-long v5, v3, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v11, v7, Lg0/l/a/i$b;->cancelled:Z

    invoke-virtual {v7, v8}, Lg0/l/a/i$b;->a(Ljava/util/Queue;)V

    invoke-interface {v9, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void

    :cond_7
    move-wide v3, v5

    :goto_2
    const-wide/16 v1, 0x0

    cmp-long v5, v3, v1

    if-eqz v5, :cond_8

    const-wide v1, 0x7fffffffffffffffL

    cmp-long v5, v13, v1

    if-eqz v5, :cond_8

    invoke-static {v10, v3, v4}, Ly/a/g0;->x(Ljava/util/concurrent/atomic/AtomicLong;J)J

    :cond_8
    neg-int v1, v12

    invoke-virtual {v7, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v12

    if-nez v12, :cond_1

    return-void
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/i$b;->cancelled:Z

    return v0
.end method

.method public l(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_1

    if-eqz v2, :cond_0

    iget-object v0, p0, Lg0/l/a/i$b;->requested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v0, p1, p2}, Ly/a/g0;->p(Ljava/util/concurrent/atomic/AtomicLong;J)J

    invoke-virtual {p0}, Lg0/l/a/i$b;->d()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "n >= required but it was "

    invoke-static {v1, p1, p2}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unsubscribe()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/i$b;->cancelled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/i$b;->cancelled:Z

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/i$b;->queue:Lg0/l/e/m/e;

    invoke-virtual {p0, v0}, Lg0/l/a/i$b;->a(Ljava/util/Queue;)V

    :cond_0
    return-void
.end method
