.class public final Lg0/l/a/b2$b;
.super Lrx/Subscriber;
.source "OperatorSwitch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/b2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:J

.field public final e:Lg0/l/a/b2$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/b2$c<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLg0/l/a/b2$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lg0/l/a/b2$c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-wide p1, p0, Lg0/l/a/b2$b;->d:J

    iput-object p3, p0, Lg0/l/a/b2$b;->e:Lg0/l/a/b2$c;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 6

    iget-object v0, p0, Lg0/l/a/b2$b;->e:Lg0/l/a/b2$c;

    iget-wide v1, p0, Lg0/l/a/b2$b;->d:J

    monitor-enter v0

    :try_start_0
    iget-object v3, v0, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-eqz v5, :cond_0

    monitor-exit v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, v0, Lg0/l/a/b2$c;->o:Z

    const/4 v1, 0x0

    iput-object v1, v0, Lg0/l/a/b2$c;->l:Lrx/Producer;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lg0/l/a/b2$c;->b()V

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 6

    iget-object v0, p0, Lg0/l/a/b2$b;->e:Lg0/l/a/b2$c;

    iget-wide v1, p0, Lg0/l/a/b2$b;->d:J

    monitor-enter v0

    :try_start_0
    iget-object v3, v0, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-nez v5, :cond_0

    invoke-virtual {v0, p1}, Lg0/l/a/b2$c;->c(Ljava/lang/Throwable;)Z

    move-result v1

    const/4 v2, 0x0

    iput-boolean v2, v0, Lg0/l/a/b2$c;->o:Z

    const/4 v2, 0x0

    iput-object v2, v0, Lg0/l/a/b2$c;->l:Lrx/Producer;

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lg0/l/a/b2$c;->b()V

    goto :goto_1

    :cond_1
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/b2$b;->e:Lg0/l/a/b2$c;

    monitor-enter v0

    :try_start_0
    iget-object v1, v0, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v1

    iget-wide v3, p0, Lg0/l/a/b2$b;->d:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_0

    monitor-exit v0

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lg0/l/a/b2$c;->h:Lg0/l/e/m/e;

    if-nez p1, :cond_1

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_1
    invoke-virtual {v1, p0, p1}, Lg0/l/e/m/e;->e(Ljava/lang/Object;Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lg0/l/a/b2$c;->b()V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 6

    iget-object v0, p0, Lg0/l/a/b2$b;->e:Lg0/l/a/b2$c;

    iget-wide v1, p0, Lg0/l/a/b2$b;->d:J

    monitor-enter v0

    :try_start_0
    iget-object v3, v0, Lg0/l/a/b2$c;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-eqz v5, :cond_0

    monitor-exit v0

    goto :goto_0

    :cond_0
    iget-wide v1, v0, Lg0/l/a/b2$c;->k:J

    iput-object p1, v0, Lg0/l/a/b2$c;->l:Lrx/Producer;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1, v1, v2}, Lrx/Producer;->l(J)V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
