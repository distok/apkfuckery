.class public Lg0/l/a/u0;
.super Lrx/Subscriber;
.source "OperatorDelay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Z

.field public final synthetic e:Lrx/Scheduler$Worker;

.field public final synthetic f:Lrx/Subscriber;

.field public final synthetic g:Lg0/l/a/v0;


# direct methods
.method public constructor <init>(Lg0/l/a/v0;Lrx/Subscriber;Lrx/Scheduler$Worker;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/u0;->g:Lg0/l/a/v0;

    iput-object p3, p0, Lg0/l/a/u0;->e:Lrx/Scheduler$Worker;

    iput-object p4, p0, Lg0/l/a/u0;->f:Lrx/Subscriber;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 5

    iget-object v0, p0, Lg0/l/a/u0;->e:Lrx/Scheduler$Worker;

    new-instance v1, Lg0/l/a/u0$a;

    invoke-direct {v1, p0}, Lg0/l/a/u0$a;-><init>(Lg0/l/a/u0;)V

    iget-object v2, p0, Lg0/l/a/u0;->g:Lg0/l/a/v0;

    iget-wide v3, v2, Lg0/l/a/v0;->d:J

    iget-object v2, v2, Lg0/l/a/v0;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v3, v4, v2}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lg0/l/a/u0;->e:Lrx/Scheduler$Worker;

    new-instance v1, Lg0/l/a/u0$b;

    invoke-direct {v1, p0, p1}, Lg0/l/a/u0$b;-><init>(Lg0/l/a/u0;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lrx/Scheduler$Worker;->a(Lrx/functions/Action0;)Lrx/Subscription;

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/u0;->e:Lrx/Scheduler$Worker;

    new-instance v1, Lg0/l/a/u0$c;

    invoke-direct {v1, p0, p1}, Lg0/l/a/u0$c;-><init>(Lg0/l/a/u0;Ljava/lang/Object;)V

    iget-object p1, p0, Lg0/l/a/u0;->g:Lg0/l/a/v0;

    iget-wide v2, p1, Lg0/l/a/v0;->d:J

    iget-object p1, p1, Lg0/l/a/v0;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, p1}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    return-void
.end method
