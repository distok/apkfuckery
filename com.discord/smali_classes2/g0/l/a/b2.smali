.class public final Lg0/l/a/b2;
.super Ljava/lang/Object;
.source "OperatorSwitch.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/b2$b;,
        Lg0/l/a/b2$c;,
        Lg0/l/a/b2$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;",
        "Lrx/Observable<",
        "+TT;>;>;"
    }
.end annotation


# instance fields
.field public final d:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lg0/l/a/b2;->d:Z

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/b2$c;

    iget-boolean v1, p0, Lg0/l/a/b2;->d:Z

    invoke-direct {v0, p1, v1}, Lg0/l/a/b2$c;-><init>(Lrx/Subscriber;Z)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object p1, v0, Lg0/l/a/b2$c;->d:Lrx/Subscriber;

    iget-object v1, v0, Lg0/l/a/b2$c;->e:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object p1, v0, Lg0/l/a/b2$c;->d:Lrx/Subscriber;

    new-instance v1, Lg0/l/a/c2;

    invoke-direct {v1, v0}, Lg0/l/a/c2;-><init>(Lg0/l/a/b2$c;)V

    new-instance v2, Lg0/r/a;

    invoke-direct {v2, v1}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {p1, v2}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object p1, v0, Lg0/l/a/b2$c;->d:Lrx/Subscriber;

    new-instance v1, Lg0/l/a/d2;

    invoke-direct {v1, v0}, Lg0/l/a/d2;-><init>(Lg0/l/a/b2$c;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-object v0
.end method
