.class public final Lg0/l/a/k$a;
.super Ljava/lang/Object;
.source "OnSubscribeConcatMap.java"

# interfaces
.implements Lrx/Producer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Producer;"
    }
.end annotation


# instance fields
.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field public final e:Lg0/l/a/k$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/k$c<",
            "TT;TR;>;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lg0/l/a/k$c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lg0/l/a/k$c<",
            "TT;TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$a;->d:Ljava/lang/Object;

    iput-object p2, p0, Lg0/l/a/k$a;->e:Lg0/l/a/k$c;

    return-void
.end method


# virtual methods
.method public l(J)V
    .locals 3

    iget-boolean v0, p0, Lg0/l/a/k$a;->f:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/k$a;->f:Z

    iget-object p1, p0, Lg0/l/a/k$a;->e:Lg0/l/a/k$c;

    iget-object p2, p0, Lg0/l/a/k$a;->d:Ljava/lang/Object;

    iget-object v0, p1, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v0, p2}, Lg0/g;->onNext(Ljava/lang/Object;)V

    const-wide/16 v0, 0x1

    iget-object p2, p1, Lg0/l/a/k$c;->g:Lg0/l/b/a;

    invoke-virtual {p2, v0, v1}, Lg0/l/b/a;->b(J)V

    const/4 p2, 0x0

    iput-boolean p2, p1, Lg0/l/a/k$c;->m:Z

    invoke-virtual {p1}, Lg0/l/a/k$c;->a()V

    :cond_0
    return-void
.end method
