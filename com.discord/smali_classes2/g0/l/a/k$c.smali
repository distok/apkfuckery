.class public final Lg0/l/a/k$c;
.super Lrx/Subscriber;
.source "OnSubscribeConcatMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TR;>;"
        }
    .end annotation
.end field

.field public final e:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "-TT;+",
            "Lrx/Observable<",
            "+TR;>;>;"
        }
    .end annotation
.end field

.field public final f:I

.field public final g:Lg0/l/b/a;

.field public final h:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final j:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Lrx/subscriptions/SerialSubscription;

.field public volatile l:Z

.field public volatile m:Z


# direct methods
.method public constructor <init>(Lrx/Subscriber;Lg0/k/b;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TR;>;",
            "Lg0/k/b<",
            "-TT;+",
            "Lrx/Observable<",
            "+TR;>;>;II)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    iput-object p2, p0, Lg0/l/a/k$c;->e:Lg0/k/b;

    iput p4, p0, Lg0/l/a/k$c;->f:I

    new-instance p1, Lg0/l/b/a;

    invoke-direct {p1}, Lg0/l/b/a;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$c;->g:Lg0/l/b/a;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$c;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {}, Lg0/l/e/n/y;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lg0/l/e/n/n;

    invoke-direct {p1, p3}, Lg0/l/e/n/n;-><init>(I)V

    goto :goto_0

    :cond_0
    new-instance p1, Lg0/l/e/m/c;

    invoke-direct {p1, p3}, Lg0/l/e/m/c;-><init>(I)V

    :goto_0
    iput-object p1, p0, Lg0/l/a/k$c;->h:Ljava/util/Queue;

    new-instance p1, Lrx/subscriptions/SerialSubscription;

    invoke-direct {p1}, Lrx/subscriptions/SerialSubscription;-><init>()V

    iput-object p1, p0, Lg0/l/a/k$c;->k:Lrx/subscriptions/SerialSubscription;

    int-to-long p1, p3

    invoke-virtual {p0, p1, p2}, Lrx/Subscriber;->request(J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    iget-object v0, p0, Lg0/l/a/k$c;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lg0/l/a/k$c;->f:I

    :cond_1
    :goto_0
    iget-object v1, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-virtual {v1}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    :cond_2
    iget-boolean v1, p0, Lg0/l/a/k$c;->m:Z

    if-nez v1, :cond_d

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    iget-object v2, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lg0/l/e/d;->h(Ljava/util/concurrent/atomic/AtomicReference;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lg0/l/e/d;->g(Ljava/lang/Throwable;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_3
    return-void

    :cond_4
    iget-boolean v2, p0, Lg0/l/a/k$c;->l:Z

    iget-object v3, p0, Lg0/l/a/k$c;->h:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_5

    const/4 v4, 0x1

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :goto_1
    if-eqz v2, :cond_8

    if-eqz v4, :cond_8

    iget-object v0, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Lg0/l/e/d;->h(Ljava/util/concurrent/atomic/AtomicReference;)Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    goto :goto_2

    :cond_6
    invoke-static {v0}, Lg0/l/e/d;->g(Ljava/lang/Throwable;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_7
    :goto_2
    return-void

    :cond_8
    if-nez v4, :cond_d

    :try_start_0
    iget-object v2, p0, Lg0/l/a/k$c;->e:Lg0/k/b;

    invoke-static {v3}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrx/Observable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The source returned by the mapper was null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lg0/l/a/k$c;->b(Ljava/lang/Throwable;)V

    return-void

    :cond_9
    sget-object v3, Lg0/l/a/f;->e:Lrx/Observable;

    const-wide/16 v4, 0x1

    if-eq v2, v3, :cond_c

    instance-of v3, v2, Lg0/l/e/j;

    if-eqz v3, :cond_a

    check-cast v2, Lg0/l/e/j;

    iput-boolean v1, p0, Lg0/l/a/k$c;->m:Z

    iget-object v1, p0, Lg0/l/a/k$c;->g:Lg0/l/b/a;

    new-instance v3, Lg0/l/a/k$a;

    iget-object v2, v2, Lg0/l/e/j;->e:Ljava/lang/Object;

    invoke-direct {v3, v2, p0}, Lg0/l/a/k$a;-><init>(Ljava/lang/Object;Lg0/l/a/k$c;)V

    invoke-virtual {v1, v3}, Lg0/l/b/a;->c(Lrx/Producer;)V

    goto :goto_3

    :cond_a
    new-instance v3, Lg0/l/a/k$b;

    invoke-direct {v3, p0}, Lg0/l/a/k$b;-><init>(Lg0/l/a/k$c;)V

    iget-object v6, p0, Lg0/l/a/k$c;->k:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v6, v3}, Lrx/subscriptions/SerialSubscription;->a(Lrx/Subscription;)V

    invoke-virtual {v3}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v6

    if-nez v6, :cond_b

    iput-boolean v1, p0, Lg0/l/a/k$c;->m:Z

    invoke-virtual {v2, v3}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    :goto_3
    invoke-virtual {p0, v4, v5}, Lrx/Subscriber;->request(J)V

    goto :goto_4

    :cond_b
    return-void

    :cond_c
    invoke-virtual {p0, v4, v5}, Lrx/Subscriber;->request(J)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-virtual {p0, v0}, Lg0/l/a/k$c;->b(Ljava/lang/Throwable;)V

    return-void

    :cond_d
    :goto_4
    iget-object v1, p0, Lg0/l/a/k$c;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    if-nez v1, :cond_1

    return-void
.end method

.method public b(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    iget-object v0, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0, p1}, Lg0/l/e/d;->f(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, Lg0/l/e/d;->h(Ljava/util/concurrent/atomic/AtomicReference;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lg0/l/e/d;->g(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onCompleted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/k$c;->l:Z

    invoke-virtual {p0}, Lg0/l/a/k$c;->a()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0, p1}, Lg0/l/e/d;->f(Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg0/l/a/k$c;->l:Z

    iget p1, p0, Lg0/l/a/k$c;->f:I

    if-nez p1, :cond_1

    iget-object p1, p0, Lg0/l/a/k$c;->j:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, Lg0/l/e/d;->h(Ljava/util/concurrent/atomic/AtomicReference;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lg0/l/e/d;->g(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/k$c;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :cond_0
    iget-object p1, p0, Lg0/l/a/k$c;->k:Lrx/subscriptions/SerialSubscription;

    iget-object p1, p1, Lrx/subscriptions/SerialSubscription;->d:Lg0/l/d/a;

    invoke-virtual {p1}, Lg0/l/d/a;->unsubscribe()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lg0/l/a/k$c;->a()V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/k$c;->h:Ljava/util/Queue;

    if-nez p1, :cond_0

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    new-instance p1, Lrx/exceptions/MissingBackpressureException;

    invoke-direct {p1}, Lrx/exceptions/MissingBackpressureException;-><init>()V

    invoke-virtual {p0, p1}, Lg0/l/a/k$c;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lg0/l/a/k$c;->a()V

    :goto_0
    return-void
.end method
