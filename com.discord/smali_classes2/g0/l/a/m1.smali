.class public final Lg0/l/a/m1;
.super Lg0/m/c;
.source "OperatorReplay.java"

# interfaces
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/m1$g;,
        Lg0/l/a/m1$b;,
        Lg0/l/a/m1$d;,
        Lg0/l/a/m1$h;,
        Lg0/l/a/m1$e;,
        Lg0/l/a/m1$c;,
        Lg0/l/a/m1$f;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/m/c<",
        "TT;>;",
        "Lrx/Subscription;"
    }
.end annotation


# static fields
.field public static final h:Lrx/functions/Func0;


# instance fields
.field public final e:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "+TT;>;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lg0/l/a/m1$f<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public final g:Lrx/functions/Func0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func0<",
            "+",
            "Lg0/l/a/m1$e<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg0/l/a/m1$a;

    invoke-direct {v0}, Lg0/l/a/m1$a;-><init>()V

    sput-object v0, Lg0/l/a/m1;->h:Lrx/functions/Func0;

    return-void
.end method

.method public constructor <init>(Lrx/Observable$a;Lrx/Observable;Ljava/util/concurrent/atomic/AtomicReference;Lrx/functions/Func0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable$a<",
            "TT;>;",
            "Lrx/Observable<",
            "+TT;>;",
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lg0/l/a/m1$f<",
            "TT;>;>;",
            "Lrx/functions/Func0<",
            "+",
            "Lg0/l/a/m1$e<",
            "TT;>;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lg0/m/c;-><init>(Lrx/Observable$a;)V

    iput-object p2, p0, Lg0/l/a/m1;->e:Lrx/Observable;

    iput-object p3, p0, Lg0/l/a/m1;->f:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p4, p0, Lg0/l/a/m1;->g:Lrx/functions/Func0;

    return-void
.end method


# virtual methods
.method public f0(Lrx/functions/Action1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action1<",
            "-",
            "Lrx/Subscription;",
            ">;)V"
        }
    .end annotation

    :goto_0
    iget-object v0, p0, Lg0/l/a/m1;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/m1$f;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    new-instance v1, Lg0/l/a/m1$f;

    iget-object v2, p0, Lg0/l/a/m1;->g:Lrx/functions/Func0;

    invoke-interface {v2}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lg0/l/a/m1$e;

    invoke-direct {v1, v2}, Lg0/l/a/m1$f;-><init>(Lg0/l/a/m1$e;)V

    new-instance v2, Lg0/l/a/p1;

    invoke-direct {v2, v1}, Lg0/l/a/p1;-><init>(Lg0/l/a/m1$f;)V

    new-instance v3, Lg0/r/a;

    invoke-direct {v3, v2}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {v1, v3}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v2, p0, Lg0/l/a/m1;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, v1

    :cond_2
    iget-object v1, v0, Lg0/l/a/m1$f;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_3

    iget-object v1, v0, Lg0/l/a/m1$f;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    check-cast p1, Lg0/l/a/c0;

    invoke-virtual {p1, v0}, Lg0/l/a/c0;->call(Ljava/lang/Object;)V

    if-eqz v2, :cond_4

    iget-object p1, p0, Lg0/l/a/m1;->e:Lrx/Observable;

    invoke-virtual {p1, v0}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    :cond_4
    return-void
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/a/m1;->f:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/a/m1$f;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public unsubscribe()V
    .locals 2

    iget-object v0, p0, Lg0/l/a/m1;->f:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    return-void
.end method
