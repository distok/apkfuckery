.class public Lg0/l/a/t1;
.super Lrx/Subscriber;
.source "OperatorScan.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field public final synthetic e:Ljava/lang/Object;

.field public final synthetic f:Lg0/l/a/r1$b;

.field public final synthetic g:Lg0/l/a/r1;


# direct methods
.method public constructor <init>(Lg0/l/a/r1;Ljava/lang/Object;Lg0/l/a/r1$b;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/t1;->g:Lg0/l/a/r1;

    iput-object p2, p0, Lg0/l/a/t1;->e:Ljava/lang/Object;

    iput-object p3, p0, Lg0/l/a/t1;->f:Lg0/l/a/r1$b;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p2, p0, Lg0/l/a/t1;->d:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/l/a/t1;->f:Lg0/l/a/r1$b;

    invoke-virtual {v0}, Lg0/l/a/r1$b;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/t1;->f:Lg0/l/a/r1$b;

    iput-object p1, v0, Lg0/l/a/r1$b;->l:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lg0/l/a/r1$b;->k:Z

    invoke-virtual {v0}, Lg0/l/a/r1$b;->b()V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/t1;->d:Ljava/lang/Object;

    :try_start_0
    iget-object v1, p0, Lg0/l/a/t1;->g:Lg0/l/a/r1;

    iget-object v1, v1, Lg0/l/a/r1;->e:Lrx/functions/Func2;

    invoke-interface {v1, v0, p1}, Lrx/functions/Func2;->call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object p1, p0, Lg0/l/a/t1;->d:Ljava/lang/Object;

    iget-object v0, p0, Lg0/l/a/t1;->f:Lg0/l/a/r1$b;

    invoke-virtual {v0, p1}, Lg0/l/a/r1$b;->onNext(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v0, p0, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 7

    iget-object v0, p0, Lg0/l/a/t1;->f:Lg0/l/a/r1$b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lg0/l/a/r1$b;->i:Ljava/util/concurrent/atomic/AtomicLong;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lg0/l/a/r1$b;->j:Lrx/Producer;

    if-nez v2, :cond_2

    iget-wide v2, v0, Lg0/l/a/r1$b;->h:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v6, v2, v4

    if-eqz v6, :cond_0

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    :cond_0
    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lg0/l/a/r1$b;->h:J

    iput-object p1, v0, Lg0/l/a/r1$b;->j:Lrx/Producer;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    invoke-interface {p1, v2, v3}, Lrx/Producer;->l(J)V

    :cond_1
    invoke-virtual {v0}, Lg0/l/a/r1$b;->b()V

    return-void

    :cond_2
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t set more than one Producer!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
