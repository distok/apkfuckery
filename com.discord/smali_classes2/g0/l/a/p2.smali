.class public Lg0/l/a/p2;
.super Lrx/Subscriber;
.source "OperatorToObservableList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:Z

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final synthetic f:Lg0/l/b/b;

.field public final synthetic g:Lrx/Subscriber;


# direct methods
.method public constructor <init>(Lg0/l/a/q2;Lg0/l/b/b;Lrx/Subscriber;)V
    .locals 0

    iput-object p2, p0, Lg0/l/a/p2;->f:Lg0/l/b/b;

    iput-object p3, p0, Lg0/l/a/p2;->g:Lrx/Subscriber;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lg0/l/a/p2;->e:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    iget-boolean v0, p0, Lg0/l/a/p2;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/p2;->d:Z

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lg0/l/a/p2;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    iput-object v1, p0, Lg0/l/a/p2;->e:Ljava/util/List;

    iget-object v1, p0, Lg0/l/a/p2;->f:Lg0/l/b/b;

    invoke-virtual {v1, v0}, Lg0/l/b/b;->b(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-virtual {p0, v0}, Lg0/l/a/p2;->onError(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/p2;->g:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/l/a/p2;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/p2;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    return-void
.end method
