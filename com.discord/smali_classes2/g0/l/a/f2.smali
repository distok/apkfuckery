.class public final Lg0/l/a/f2;
.super Ljava/lang/Object;
.source "OperatorTake.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;TT;>;"
    }
.end annotation


# instance fields
.field public final d:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p1, :cond_0

    iput p1, p0, Lg0/l/a/f2;->d:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "limit >= 0 required but it was "

    invoke-static {v1, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/e2;

    invoke-direct {v0, p0, p1}, Lg0/l/a/e2;-><init>(Lg0/l/a/f2;Lrx/Subscriber;)V

    iget v1, p0, Lg0/l/a/f2;->d:I

    if-nez v1, :cond_0

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    invoke-virtual {v0}, Lrx/Subscriber;->unsubscribe()V

    :cond_0
    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    return-object v0
.end method
