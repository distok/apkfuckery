.class public final enum Lg0/l/a/f;
.super Ljava/lang/Enum;
.source "EmptyObservableHolder.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lg0/l/a/f;",
        ">;",
        "Lrx/Observable$a<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lg0/l/a/f;

.field public static final e:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final synthetic f:[Lg0/l/a/f;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lg0/l/a/f;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lg0/l/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lg0/l/a/f;->d:Lg0/l/a/f;

    const/4 v1, 0x1

    new-array v1, v1, [Lg0/l/a/f;

    aput-object v0, v1, v2

    sput-object v1, Lg0/l/a/f;->f:[Lg0/l/a/f;

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v0

    sput-object v0, Lg0/l/a/f;->e:Lrx/Observable;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lg0/l/a/f;
    .locals 1

    const-class v0, Lg0/l/a/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lg0/l/a/f;

    return-object p0
.end method

.method public static values()[Lg0/l/a/f;
    .locals 1

    sget-object v0, Lg0/l/a/f;->f:[Lg0/l/a/f;

    invoke-virtual {v0}, [Lg0/l/a/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lg0/l/a/f;

    return-object v0
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    return-void
.end method
