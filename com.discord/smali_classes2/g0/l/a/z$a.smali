.class public Lg0/l/a/z$a;
.super Lrx/Subscriber;
.source "OnSubscribeRedo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/a/z;->call()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lg0/l/a/z;


# direct methods
.method public constructor <init>(Lg0/l/a/z;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    iget-object v0, v0, Lg0/l/a/z;->e:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    iget-object v0, v0, Lg0/l/a/z;->e:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4

    iget-object p1, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    iget-object p1, p1, Lg0/l/a/z;->e:Lrx/Subscriber;

    invoke-virtual {p1}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    iget-object p1, p1, Lg0/l/a/z;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    iget-object p1, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    iget-object v0, p1, Lg0/l/a/z;->g:Lrx/Scheduler$Worker;

    iget-object p1, p1, Lg0/l/a/z;->h:Lrx/functions/Action0;

    invoke-virtual {v0, p1}, Lrx/Scheduler$Worker;->a(Lrx/functions/Action0;)Lrx/Subscription;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lg0/l/a/z$a;->d:Lg0/l/a/z;

    iget-object p1, p1, Lg0/l/a/z;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-interface {p1, v0, v1}, Lrx/Producer;->l(J)V

    return-void
.end method
