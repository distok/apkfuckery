.class public Lg0/l/a/m0;
.super Ljava/lang/Object;
.source "OnSubscribeTimerPeriodically.java"

# interfaces
.implements Lrx/functions/Action0;


# instance fields
.field public d:J

.field public final synthetic e:Lrx/Subscriber;

.field public final synthetic f:Lrx/Scheduler$Worker;


# direct methods
.method public constructor <init>(Lg0/l/a/n0;Lrx/Subscriber;Lrx/Scheduler$Worker;)V
    .locals 0

    iput-object p2, p0, Lg0/l/a/m0;->e:Lrx/Subscriber;

    iput-object p3, p0, Lg0/l/a/m0;->f:Lrx/Scheduler$Worker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lg0/l/a/m0;->e:Lrx/Subscriber;

    iget-wide v1, p0, Lg0/l/a/m0;->d:J

    const-wide/16 v3, 0x1

    add-long/2addr v3, v1

    iput-wide v3, p0, Lg0/l/a/m0;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    iget-object v1, p0, Lg0/l/a/m0;->f:Lrx/Scheduler$Worker;

    invoke-interface {v1}, Lrx/Subscription;->unsubscribe()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v1, p0, Lg0/l/a/m0;->e:Lrx/Subscriber;

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-interface {v1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :catchall_1
    move-exception v1

    iget-object v2, p0, Lg0/l/a/m0;->e:Lrx/Subscriber;

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-interface {v2, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    throw v1
.end method
