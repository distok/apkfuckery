.class public final Lg0/l/a/q;
.super Ljava/lang/Object;
.source "OnSubscribeFlattenIterable.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/q$b;,
        Lg0/l/a/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TR;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "+TT;>;"
        }
    .end annotation
.end field

.field public final e:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "-TT;+",
            "Ljava/lang/Iterable<",
            "+TR;>;>;"
        }
    .end annotation
.end field

.field public final f:I


# direct methods
.method public constructor <init>(Lrx/Observable;Lg0/k/b;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "+TT;>;",
            "Lg0/k/b<",
            "-TT;+",
            "Ljava/lang/Iterable<",
            "+TR;>;>;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/a/q;->d:Lrx/Observable;

    iput-object p2, p0, Lg0/l/a/q;->e:Lg0/k/b;

    iput p3, p0, Lg0/l/a/q;->f:I

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/q$a;

    iget-object v1, p0, Lg0/l/a/q;->e:Lg0/k/b;

    iget v2, p0, Lg0/l/a/q;->f:I

    invoke-direct {v0, p1, v1, v2}, Lg0/l/a/q$a;-><init>(Lrx/Subscriber;Lg0/k/b;I)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    new-instance v1, Lg0/l/a/p;

    invoke-direct {v1, p0, v0}, Lg0/l/a/p;-><init>(Lg0/l/a/q;Lg0/l/a/q$a;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    iget-object p1, p0, Lg0/l/a/q;->d:Lrx/Observable;

    invoke-virtual {p1, v0}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    return-void
.end method
