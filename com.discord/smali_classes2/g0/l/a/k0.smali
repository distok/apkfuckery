.class public Lg0/l/a/k0;
.super Ljava/lang/Object;
.source "OnSubscribeTimerOnce.java"

# interfaces
.implements Lrx/functions/Action0;


# instance fields
.field public final synthetic d:Lrx/Subscriber;


# direct methods
.method public constructor <init>(Lg0/l/a/l0;Lrx/Subscriber;)V
    .locals 0

    iput-object p2, p0, Lg0/l/a/k0;->d:Lrx/Subscriber;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lg0/l/a/k0;->d:Lrx/Subscriber;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lg0/l/a/k0;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lg0/l/a/k0;->d:Lrx/Subscriber;

    invoke-static {v0}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-interface {v1, v0}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method
