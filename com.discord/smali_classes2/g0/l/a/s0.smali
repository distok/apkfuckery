.class public Lg0/l/a/s0;
.super Lrx/Subscriber;
.source "OperatorDebounceWithTime.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lg0/l/a/t0$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/t0$a<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final e:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "*>;"
        }
    .end annotation
.end field

.field public final synthetic f:Lrx/subscriptions/SerialSubscription;

.field public final synthetic g:Lrx/Scheduler$Worker;

.field public final synthetic h:Lrx/observers/SerializedSubscriber;

.field public final synthetic i:Lg0/l/a/t0;


# direct methods
.method public constructor <init>(Lg0/l/a/t0;Lrx/Subscriber;Lrx/subscriptions/SerialSubscription;Lrx/Scheduler$Worker;Lrx/observers/SerializedSubscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/s0;->i:Lg0/l/a/t0;

    iput-object p3, p0, Lg0/l/a/s0;->f:Lrx/subscriptions/SerialSubscription;

    iput-object p4, p0, Lg0/l/a/s0;->g:Lrx/Scheduler$Worker;

    iput-object p5, p0, Lg0/l/a/s0;->h:Lrx/observers/SerializedSubscriber;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    new-instance p1, Lg0/l/a/t0$a;

    invoke-direct {p1}, Lg0/l/a/t0$a;-><init>()V

    iput-object p1, p0, Lg0/l/a/s0;->d:Lg0/l/a/t0$a;

    iput-object p0, p0, Lg0/l/a/s0;->e:Lrx/Subscriber;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 6

    iget-object v0, p0, Lg0/l/a/s0;->d:Lg0/l/a/t0$a;

    iget-object v1, p0, Lg0/l/a/s0;->h:Lrx/observers/SerializedSubscriber;

    monitor-enter v0

    :try_start_0
    iget-boolean v2, v0, Lg0/l/a/t0$a;->e:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    iput-boolean v3, v0, Lg0/l/a/t0$a;->d:Z

    monitor-exit v0

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lg0/l/a/t0$a;->b:Ljava/lang/Object;

    iget-boolean v4, v0, Lg0/l/a/t0$a;->c:Z

    const/4 v5, 0x0

    iput-object v5, v0, Lg0/l/a/t0$a;->b:Ljava/lang/Object;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lg0/l/a/t0$a;->c:Z

    iput-boolean v3, v0, Lg0/l/a/t0$a;->e:Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v4, :cond_1

    :try_start_1
    invoke-virtual {v1, v2}, Lrx/observers/SerializedSubscriber;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v0, p0, v2}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lrx/observers/SerializedSubscriber;->onCompleted()V

    :goto_1
    return-void

    :catchall_1
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/s0;->h:Lrx/observers/SerializedSubscriber;

    iget-object v0, v0, Lrx/observers/SerializedSubscriber;->d:Lg0/g;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lrx/Subscriber;->unsubscribe()V

    iget-object p1, p0, Lg0/l/a/s0;->d:Lg0/l/a/t0$a;

    monitor-enter p1

    :try_start_0
    iget v0, p1, Lg0/l/a/t0$a;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lg0/l/a/t0$a;->a:I

    const/4 v0, 0x0

    iput-object v0, p1, Lg0/l/a/t0$a;->b:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lg0/l/a/t0$a;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/a/s0;->d:Lg0/l/a/t0$a;

    monitor-enter v0

    :try_start_0
    iput-object p1, v0, Lg0/l/a/t0$a;->b:Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lg0/l/a/t0$a;->c:Z

    iget v1, v0, Lg0/l/a/t0$a;->a:I

    add-int/2addr v1, p1

    iput v1, v0, Lg0/l/a/t0$a;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    iget-object p1, p0, Lg0/l/a/s0;->f:Lrx/subscriptions/SerialSubscription;

    iget-object v0, p0, Lg0/l/a/s0;->g:Lrx/Scheduler$Worker;

    new-instance v2, Lg0/l/a/s0$a;

    invoke-direct {v2, p0, v1}, Lg0/l/a/s0$a;-><init>(Lg0/l/a/s0;I)V

    iget-object v1, p0, Lg0/l/a/s0;->i:Lg0/l/a/t0;

    iget-wide v3, v1, Lg0/l/a/t0;->d:J

    iget-object v1, v1, Lg0/l/a/t0;->e:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4, v1}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subscriptions/SerialSubscription;->a(Lrx/Subscription;)V

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public onStart()V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    return-void
.end method
