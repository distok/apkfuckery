.class public final Lg0/l/a/t2$a$a;
.super Lrx/Subscriber;
.source "OperatorZip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/t2$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final d:Lg0/l/e/h;

.field public final synthetic e:Lg0/l/a/t2$a;


# direct methods
.method public constructor <init>(Lg0/l/a/t2$a;)V
    .locals 2

    iput-object p1, p0, Lg0/l/a/t2$a$a;->e:Lg0/l/a/t2$a;

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    sget p1, Lg0/l/e/h;->f:I

    invoke-static {}, Lg0/l/e/n/y;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lg0/l/e/h;

    const/4 v0, 0x1

    sget v1, Lg0/l/e/h;->f:I

    invoke-direct {p1, v0, v1}, Lg0/l/e/h;-><init>(ZI)V

    goto :goto_0

    :cond_0
    new-instance p1, Lg0/l/e/h;

    invoke-direct {p1}, Lg0/l/e/h;-><init>()V

    :goto_0
    iput-object p1, p0, Lg0/l/a/t2$a$a;->d:Lg0/l/e/h;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    iget-object v0, p0, Lg0/l/a/t2$a$a;->d:Lg0/l/e/h;

    iget-object v1, v0, Lg0/l/e/h;->e:Ljava/lang/Object;

    if-nez v1, :cond_0

    sget-object v1, Lg0/l/a/h;->a:Ljava/lang/Object;

    iput-object v1, v0, Lg0/l/e/h;->e:Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lg0/l/a/t2$a$a;->e:Lg0/l/a/t2$a;

    invoke-virtual {v0}, Lg0/l/a/t2$a;->b()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/t2$a$a;->e:Lg0/l/a/t2$a;

    iget-object v0, v0, Lg0/l/a/t2$a;->child:Lg0/g;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lg0/l/a/t2$a$a;->d:Lg0/l/e/h;

    invoke-virtual {v0, p1}, Lg0/l/e/h;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Lrx/exceptions/MissingBackpressureException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object v0, p0, Lg0/l/a/t2$a$a;->e:Lg0/l/a/t2$a;

    iget-object v0, v0, Lg0/l/a/t2$a;->child:Lg0/g;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    :goto_0
    iget-object p1, p0, Lg0/l/a/t2$a$a;->e:Lg0/l/a/t2$a;

    invoke-virtual {p1}, Lg0/l/a/t2$a;->b()V

    return-void
.end method

.method public onStart()V
    .locals 2

    sget v0, Lg0/l/e/h;->f:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lrx/Subscriber;->request(J)V

    return-void
.end method
