.class public Lg0/l/a/v1;
.super Lrx/Subscriber;
.source "OperatorSkip.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public d:I

.field public final synthetic e:Lrx/Subscriber;

.field public final synthetic f:Lg0/l/a/w1;


# direct methods
.method public constructor <init>(Lg0/l/a/w1;Lrx/Subscriber;Lrx/Subscriber;)V
    .locals 0

    iput-object p1, p0, Lg0/l/a/v1;->f:Lg0/l/a/w1;

    iput-object p3, p0, Lg0/l/a/v1;->e:Lrx/Subscriber;

    invoke-direct {p0, p2}, Lrx/Subscriber;-><init>(Lrx/Subscriber;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/l/a/v1;->e:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/v1;->e:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget v0, p0, Lg0/l/a/v1;->d:I

    iget-object v1, p0, Lg0/l/a/v1;->f:Lg0/l/a/w1;

    iget v1, v1, Lg0/l/a/w1;->d:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lg0/l/a/v1;->e:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lg0/l/a/v1;->d:I

    :goto_0
    return-void
.end method

.method public setProducer(Lrx/Producer;)V
    .locals 2

    iget-object v0, p0, Lg0/l/a/v1;->e:Lrx/Subscriber;

    invoke-virtual {v0, p1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    iget-object v0, p0, Lg0/l/a/v1;->f:Lg0/l/a/w1;

    iget v0, v0, Lg0/l/a/w1;->d:I

    int-to-long v0, v0

    invoke-interface {p1, v0, v1}, Lrx/Producer;->l(J)V

    return-void
.end method
