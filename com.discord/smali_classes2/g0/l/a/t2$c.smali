.class public final Lg0/l/a/t2$c;
.super Lrx/Subscriber;
.source "OperatorZip.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/a/t2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/Subscriber<",
        "[",
        "Lrx/Observable;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TR;>;"
        }
    .end annotation
.end field

.field public final e:Lg0/l/a/t2$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/t2$a<",
            "TR;>;"
        }
    .end annotation
.end field

.field public final f:Lg0/l/a/t2$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/a/t2$b<",
            "TR;>;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(Lg0/l/a/t2;Lrx/Subscriber;Lg0/l/a/t2$a;Lg0/l/a/t2$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TR;>;",
            "Lg0/l/a/t2$a<",
            "TR;>;",
            "Lg0/l/a/t2$b<",
            "TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lrx/Subscriber;-><init>()V

    iput-object p2, p0, Lg0/l/a/t2$c;->d:Lrx/Subscriber;

    iput-object p3, p0, Lg0/l/a/t2$c;->e:Lg0/l/a/t2$a;

    iput-object p4, p0, Lg0/l/a/t2$c;->f:Lg0/l/a/t2$b;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-boolean v0, p0, Lg0/l/a/t2$c;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0/l/a/t2$c;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/l/a/t2$c;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, [Lrx/Observable;

    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/l/a/t2$c;->g:Z

    iget-object v0, p0, Lg0/l/a/t2$c;->e:Lg0/l/a/t2$a;

    iget-object v1, p0, Lg0/l/a/t2$c;->f:Lg0/l/a/t2$b;

    invoke-virtual {v0, p1, v1}, Lg0/l/a/t2$a;->a([Lrx/Observable;Ljava/util/concurrent/atomic/AtomicLong;)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p1, p0, Lg0/l/a/t2$c;->d:Lrx/Subscriber;

    invoke-interface {p1}, Lg0/g;->onCompleted()V

    :goto_1
    return-void
.end method
