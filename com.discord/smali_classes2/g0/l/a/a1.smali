.class public final Lg0/l/a/a1;
.super Ljava/lang/Object;
.source "OperatorMerge.java"

# interfaces
.implements Lrx/Observable$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/a/a1$c;,
        Lg0/l/a/a1$e;,
        Lg0/l/a/a1$d;,
        Lg0/l/a/a1$a;,
        Lg0/l/a/a1$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$b<",
        "TT;",
        "Lrx/Observable<",
        "+TT;>;>;"
    }
.end annotation


# instance fields
.field public final d:Z


# direct methods
.method public constructor <init>(ZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lg0/l/a/a1;->d:Z

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/l/a/a1$e;

    iget-boolean v1, p0, Lg0/l/a/a1;->d:Z

    const v2, 0x7fffffff

    invoke-direct {v0, p1, v1, v2}, Lg0/l/a/a1$e;-><init>(Lrx/Subscriber;ZI)V

    new-instance v1, Lg0/l/a/a1$d;

    invoke-direct {v1, v0}, Lg0/l/a/a1$d;-><init>(Lg0/l/a/a1$e;)V

    iput-object v1, v0, Lg0/l/a/a1$e;->g:Lg0/l/a/a1$d;

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-object v0
.end method
