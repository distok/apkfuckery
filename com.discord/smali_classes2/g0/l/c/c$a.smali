.class public final Lg0/l/c/c$a;
.super Lrx/Scheduler$Worker;
.source "ExecutorScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/c/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/Executor;

.field public final e:Lrx/subscriptions/CompositeSubscription;

.field public final f:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lg0/l/c/j;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final h:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 2

    invoke-direct {p0}, Lrx/Scheduler$Worker;-><init>()V

    iput-object p1, p0, Lg0/l/c/c$a;->d:Ljava/util/concurrent/Executor;

    new-instance p1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object p1, p0, Lg0/l/c/c$a;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object p1, p0, Lg0/l/c/c$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    sget-object p1, Lg0/l/c/d;->d:Lg0/l/c/d;

    iget-object p1, p1, Lg0/l/c/d;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v0, Lg0/l/c/d;->b:[Ljava/util/concurrent/ScheduledExecutorService;

    if-ne p1, v0, :cond_0

    sget-object p1, Lg0/l/c/d;->c:Ljava/util/concurrent/ScheduledExecutorService;

    goto :goto_0

    :cond_0
    sget v0, Lg0/l/c/d;->e:I

    add-int/lit8 v0, v0, 0x1

    array-length v1, p1

    if-lt v0, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    sput v0, Lg0/l/c/d;->e:I

    aget-object p1, p1, v0

    :goto_0
    iput-object p1, p0, Lg0/l/c/c$a;->h:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method


# virtual methods
.method public a(Lrx/functions/Action0;)Lrx/Subscription;
    .locals 2

    iget-object v0, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lg0/r/c;->a:Lg0/r/c$a;

    return-object p1

    :cond_0
    invoke-static {p1}, Lg0/o/l;->d(Lrx/functions/Action0;)Lrx/functions/Action0;

    move-result-object p1

    new-instance v0, Lg0/l/c/j;

    iget-object v1, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0, p1, v1}, Lg0/l/c/j;-><init>(Lrx/functions/Action0;Lrx/subscriptions/CompositeSubscription;)V

    iget-object p1, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    iget-object p1, p0, Lg0/l/c/c$a;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    iget-object p1, p0, Lg0/l/c/c$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result p1

    if-nez p1, :cond_1

    :try_start_0
    iget-object p1, p0, Lg0/l/c/c$a;->d:Ljava/util/concurrent/Executor;

    invoke-interface {p1, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object v1, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v1, v0}, Lrx/subscriptions/CompositeSubscription;->c(Lrx/Subscription;)V

    iget-object v0, p0, Lg0/l/c/c$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    throw p1

    :cond_1
    :goto_0
    return-object v0
.end method

.method public b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 5

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-gtz v2, :cond_0

    invoke-virtual {p0, p1}, Lg0/l/c/c$a;->a(Lrx/functions/Action0;)Lrx/Subscription;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    if-eqz v0, :cond_1

    sget-object p1, Lg0/r/c;->a:Lg0/r/c$a;

    return-object p1

    :cond_1
    invoke-static {p1}, Lg0/o/l;->d(Lrx/functions/Action0;)Lrx/functions/Action0;

    move-result-object p1

    new-instance v0, Lg0/r/b;

    invoke-direct {v0}, Lg0/r/b;-><init>()V

    new-instance v1, Lg0/r/b;

    invoke-direct {v1}, Lg0/r/b;-><init>()V

    invoke-virtual {v1, v0}, Lg0/r/b;->a(Lrx/Subscription;)V

    iget-object v2, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v2, v1}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    new-instance v2, Lg0/l/c/c$a$a;

    invoke-direct {v2, p0, v1}, Lg0/l/c/c$a$a;-><init>(Lg0/l/c/c$a;Lg0/r/b;)V

    new-instance v3, Lg0/r/a;

    invoke-direct {v3, v2}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    new-instance v2, Lg0/l/c/j;

    new-instance v4, Lg0/l/c/c$a$b;

    invoke-direct {v4, p0, v1, p1, v3}, Lg0/l/c/c$a$b;-><init>(Lg0/l/c/c$a;Lg0/r/b;Lrx/functions/Action0;Lrx/Subscription;)V

    invoke-direct {v2, v4}, Lg0/l/c/j;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {v0, v2}, Lg0/r/b;->a(Lrx/Subscription;)V

    :try_start_0
    iget-object p1, p0, Lg0/l/c/c$a;->h:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p1, v2, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    invoke-virtual {v2, p1}, Lg0/l/c/j;->a(Ljava/util/concurrent/Future;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    :catch_0
    move-exception p1

    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    return v0
.end method

.method public run()V
    .locals 2

    :cond_0
    iget-object v0, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lg0/l/c/c$a;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    return-void

    :cond_1
    iget-object v0, p0, Lg0/l/c/c$a;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/c/j;

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget-object v1, v0, Lg0/l/c/j;->cancel:Lrx/internal/util/SubscriptionList;

    iget-boolean v1, v1, Lrx/internal/util/SubscriptionList;->e:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v1, v1, Lrx/subscriptions/CompositeSubscription;->e:Z

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lg0/l/c/j;->run()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lg0/l/c/c$a;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    return-void

    :cond_4
    :goto_0
    iget-object v0, p0, Lg0/l/c/c$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    return-void
.end method

.method public unsubscribe()V
    .locals 1

    iget-object v0, p0, Lg0/l/c/c$a;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    iget-object v0, p0, Lg0/l/c/c$a;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    return-void
.end method
