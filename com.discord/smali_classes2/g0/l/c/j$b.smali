.class public final Lg0/l/c/j$b;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "ScheduledAction.java"

# interfaces
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/c/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x36e5888d681586eL


# instance fields
.field public final parent:Lrx/internal/util/SubscriptionList;

.field public final s:Lg0/l/c/j;


# direct methods
.method public constructor <init>(Lg0/l/c/j;Lrx/internal/util/SubscriptionList;)V
    .locals 0

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lg0/l/c/j$b;->s:Lg0/l/c/j;

    iput-object p2, p0, Lg0/l/c/j$b;->parent:Lrx/internal/util/SubscriptionList;

    return-void
.end method


# virtual methods
.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/c/j$b;->s:Lg0/l/c/j;

    iget-object v0, v0, Lg0/l/c/j;->cancel:Lrx/internal/util/SubscriptionList;

    iget-boolean v0, v0, Lrx/internal/util/SubscriptionList;->e:Z

    return v0
.end method

.method public unsubscribe()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lg0/l/c/j$b;->parent:Lrx/internal/util/SubscriptionList;

    iget-object v1, p0, Lg0/l/c/j$b;->s:Lg0/l/c/j;

    iget-boolean v2, v0, Lrx/internal/util/SubscriptionList;->e:Z

    if-nez v2, :cond_2

    monitor-enter v0

    :try_start_0
    iget-object v2, v0, Lrx/internal/util/SubscriptionList;->d:Ljava/util/List;

    iget-boolean v3, v0, Lrx/internal/util/SubscriptionList;->e:Z

    if-nez v3, :cond_1

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v2

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lg0/l/c/j;->unsubscribe()V

    goto :goto_1

    :cond_1
    :goto_0
    :try_start_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_2
    :goto_1
    return-void
.end method
