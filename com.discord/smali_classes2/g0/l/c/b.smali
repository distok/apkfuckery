.class public final Lg0/l/c/b;
.super Lrx/Scheduler;
.source "EventLoopsScheduler.java"

# interfaces
.implements Lg0/l/c/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/c/b$c;,
        Lg0/l/c/b$a;,
        Lg0/l/c/b$b;
    }
.end annotation


# static fields
.field public static final c:I

.field public static final d:Lg0/l/c/b$c;

.field public static final e:Lg0/l/c/b$b;


# instance fields
.field public final a:Ljava/util/concurrent/ThreadFactory;

.field public final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lg0/l/c/b$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-string v0, "rx.scheduler.max-computation-threads"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v2

    if-lez v0, :cond_0

    if-le v0, v2, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    sput v0, Lg0/l/c/b;->c:I

    new-instance v0, Lg0/l/c/b$c;

    sget-object v2, Lg0/l/e/i;->d:Ljava/util/concurrent/ThreadFactory;

    invoke-direct {v0, v2}, Lg0/l/c/b$c;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lg0/l/c/b;->d:Lg0/l/c/b$c;

    invoke-virtual {v0}, Lg0/l/c/g;->unsubscribe()V

    new-instance v0, Lg0/l/c/b$b;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lg0/l/c/b$b;-><init>(Ljava/util/concurrent/ThreadFactory;I)V

    sput-object v0, Lg0/l/c/b;->e:Lg0/l/c/b$b;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ThreadFactory;)V
    .locals 4

    invoke-direct {p0}, Lrx/Scheduler;-><init>()V

    iput-object p1, p0, Lg0/l/c/b;->a:Ljava/util/concurrent/ThreadFactory;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lg0/l/c/b;->e:Lg0/l/c/b$b;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lg0/l/c/b;->b:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v2, Lg0/l/c/b$b;

    sget v3, Lg0/l/c/b;->c:I

    invoke-direct {v2, p1, v3}, Lg0/l/c/b$b;-><init>(Ljava/util/concurrent/ThreadFactory;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, v2, Lg0/l/c/b$b;->b:[Lg0/l/c/b$c;

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    invoke-virtual {v2}, Lg0/l/c/g;->unsubscribe()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lrx/Scheduler$Worker;
    .locals 2

    new-instance v0, Lg0/l/c/b$a;

    iget-object v1, p0, Lg0/l/c/b;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg0/l/c/b$b;

    invoke-virtual {v1}, Lg0/l/c/b$b;->a()Lg0/l/c/b$c;

    move-result-object v1

    invoke-direct {v0, v1}, Lg0/l/c/b$a;-><init>(Lg0/l/c/b$c;)V

    return-object v0
.end method

.method public shutdown()V
    .locals 4

    :cond_0
    iget-object v0, p0, Lg0/l/c/b;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/c/b$b;

    sget-object v1, Lg0/l/c/b;->e:Lg0/l/c/b$b;

    if-ne v0, v1, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lg0/l/c/b;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lg0/l/c/b$b;->b:[Lg0/l/c/b$c;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lg0/l/c/g;->unsubscribe()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
