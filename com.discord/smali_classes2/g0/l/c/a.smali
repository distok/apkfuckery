.class public final Lg0/l/c/a;
.super Lrx/Scheduler;
.source "CachedThreadScheduler.java"

# interfaces
.implements Lg0/l/c/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/c/a$c;,
        Lg0/l/c/a$b;,
        Lg0/l/c/a$a;
    }
.end annotation


# static fields
.field public static final c:J

.field public static final d:Ljava/util/concurrent/TimeUnit;

.field public static final e:Lg0/l/c/a$c;

.field public static final f:Lg0/l/c/a$a;


# instance fields
.field public final a:Ljava/util/concurrent/ThreadFactory;

.field public final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lg0/l/c/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lg0/l/c/a;->d:Ljava/util/concurrent/TimeUnit;

    new-instance v0, Lg0/l/c/a$c;

    sget-object v1, Lg0/l/e/i;->d:Ljava/util/concurrent/ThreadFactory;

    invoke-direct {v0, v1}, Lg0/l/c/a$c;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lg0/l/c/a;->e:Lg0/l/c/a$c;

    invoke-virtual {v0}, Lg0/l/c/g;->unsubscribe()V

    new-instance v0, Lg0/l/c/a$a;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3, v1}, Lg0/l/c/a$a;-><init>(Ljava/util/concurrent/ThreadFactory;JLjava/util/concurrent/TimeUnit;)V

    sput-object v0, Lg0/l/c/a;->f:Lg0/l/c/a$a;

    invoke-virtual {v0}, Lg0/l/c/a$a;->a()V

    const-string v0, "rx.io-scheduler.keepalive"

    const/16 v1, 0x3c

    invoke-static {v0, v1}, Ljava/lang/Integer;->getInteger(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lg0/l/c/a;->c:J

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ThreadFactory;)V
    .locals 6

    invoke-direct {p0}, Lrx/Scheduler;-><init>()V

    iput-object p1, p0, Lg0/l/c/a;->a:Ljava/util/concurrent/ThreadFactory;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lg0/l/c/a;->f:Lg0/l/c/a$a;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lg0/l/c/a;->b:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v2, Lg0/l/c/a$a;

    sget-wide v3, Lg0/l/c/a;->c:J

    sget-object v5, Lg0/l/c/a;->d:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v2, p1, v3, v4, v5}, Lg0/l/c/a$a;-><init>(Ljava/util/concurrent/ThreadFactory;JLjava/util/concurrent/TimeUnit;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {v2}, Lg0/l/c/a$a;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lrx/Scheduler$Worker;
    .locals 2

    new-instance v0, Lg0/l/c/a$b;

    iget-object v1, p0, Lg0/l/c/a;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg0/l/c/a$a;

    invoke-direct {v0, v1}, Lg0/l/c/a$b;-><init>(Lg0/l/c/a$a;)V

    return-object v0
.end method

.method public shutdown()V
    .locals 3

    :cond_0
    iget-object v0, p0, Lg0/l/c/a;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/c/a$a;

    sget-object v1, Lg0/l/c/a;->f:Lg0/l/c/a$a;

    if-ne v0, v1, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lg0/l/c/a;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lg0/l/c/a$a;->a()V

    return-void
.end method
