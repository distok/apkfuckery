.class public final Lg0/l/c/a$b;
.super Lrx/Scheduler$Worker;
.source "CachedThreadScheduler.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/c/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final d:Lrx/subscriptions/CompositeSubscription;

.field public final e:Lg0/l/c/a$a;

.field public final f:Lg0/l/c/a$c;

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lg0/l/c/a$a;)V
    .locals 2

    invoke-direct {p0}, Lrx/Scheduler$Worker;-><init>()V

    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lg0/l/c/a$b;->d:Lrx/subscriptions/CompositeSubscription;

    iput-object p1, p0, Lg0/l/c/a$b;->e:Lg0/l/c/a$a;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lg0/l/c/a$b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v0, p1, Lg0/l/c/a$a;->d:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lg0/l/c/a;->e:Lg0/l/c/a$c;

    goto :goto_1

    :cond_0
    iget-object v0, p1, Lg0/l/c/a$a;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lg0/l/c/a$a;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/c/a$c;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_1
    new-instance v0, Lg0/l/c/a$c;

    iget-object v1, p1, Lg0/l/c/a$a;->a:Ljava/util/concurrent/ThreadFactory;

    invoke-direct {v0, v1}, Lg0/l/c/a$c;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    iget-object p1, p1, Lg0/l/c/a$a;->d:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    :goto_0
    move-object p1, v0

    :goto_1
    iput-object p1, p0, Lg0/l/c/a$b;->f:Lg0/l/c/a$c;

    return-void
.end method


# virtual methods
.method public a(Lrx/functions/Action0;)Lrx/Subscription;
    .locals 3

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lg0/l/c/a$b;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 2

    iget-object v0, p0, Lg0/l/c/a$b;->d:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lg0/r/c;->a:Lg0/r/c$a;

    return-object p1

    :cond_0
    iget-object v0, p0, Lg0/l/c/a$b;->f:Lg0/l/c/a$c;

    new-instance v1, Lg0/l/c/a$b$a;

    invoke-direct {v1, p0, p1}, Lg0/l/c/a$b$a;-><init>(Lg0/l/c/a$b;Lrx/functions/Action0;)V

    invoke-virtual {v0, v1, p2, p3, p4}, Lg0/l/c/g;->f(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lg0/l/c/j;

    move-result-object p1

    iget-object p2, p0, Lg0/l/c/a$b;->d:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    iget-object p2, p0, Lg0/l/c/a$b;->d:Lrx/subscriptions/CompositeSubscription;

    iget-object p3, p1, Lg0/l/c/j;->cancel:Lrx/internal/util/SubscriptionList;

    new-instance p4, Lg0/l/c/j$c;

    invoke-direct {p4, p1, p2}, Lg0/l/c/j$c;-><init>(Lg0/l/c/j;Lrx/subscriptions/CompositeSubscription;)V

    invoke-virtual {p3, p4}, Lrx/internal/util/SubscriptionList;->a(Lrx/Subscription;)V

    return-object p1
.end method

.method public call()V
    .locals 6

    iget-object v0, p0, Lg0/l/c/a$b;->e:Lg0/l/c/a$a;

    iget-object v1, p0, Lg0/l/c/a$b;->f:Lg0/l/c/a$c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v4, v0, Lg0/l/c/a$a;->b:J

    add-long/2addr v2, v4

    iput-wide v2, v1, Lg0/l/c/a$c;->l:J

    iget-object v0, v0, Lg0/l/c/a$a;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/c/a$b;->d:Lrx/subscriptions/CompositeSubscription;

    iget-boolean v0, v0, Lrx/subscriptions/CompositeSubscription;->e:Z

    return v0
.end method

.method public unsubscribe()V
    .locals 3

    iget-object v0, p0, Lg0/l/c/a$b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg0/l/c/a$b;->f:Lg0/l/c/a$c;

    invoke-virtual {v0, p0}, Lg0/l/c/g;->a(Lrx/functions/Action0;)Lrx/Subscription;

    :cond_0
    iget-object v0, p0, Lg0/l/c/a$b;->d:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    return-void
.end method
