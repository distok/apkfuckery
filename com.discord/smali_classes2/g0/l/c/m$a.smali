.class public final Lg0/l/c/m$a;
.super Lrx/Scheduler$Worker;
.source "TrampolineScheduler.java"

# interfaces
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/c/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final e:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lg0/l/c/m$b;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lg0/r/a;

.field public final g:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lrx/Scheduler$Worker;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lg0/l/c/m$a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lg0/l/c/m$a;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Lg0/r/a;

    invoke-direct {v0}, Lg0/r/a;-><init>()V

    iput-object v0, p0, Lg0/l/c/m$a;->f:Lg0/r/a;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lg0/l/c/m$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public a(Lrx/functions/Action0;)Lrx/Subscription;
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lg0/l/c/m$a;->d(Lrx/functions/Action0;J)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p2

    add-long/2addr p2, v0

    new-instance p4, Lg0/l/c/l;

    invoke-direct {p4, p1, p0, p2, p3}, Lg0/l/c/l;-><init>(Lrx/functions/Action0;Lrx/Scheduler$Worker;J)V

    invoke-virtual {p0, p4, p2, p3}, Lg0/l/c/m$a;->d(Lrx/functions/Action0;J)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public final d(Lrx/functions/Action0;J)Lrx/Subscription;
    .locals 2

    sget-object v0, Lg0/r/c;->a:Lg0/r/c$a;

    iget-object v1, p0, Lg0/l/c/m$a;->f:Lg0/r/a;

    invoke-virtual {v1}, Lg0/r/a;->isUnsubscribed()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lg0/l/c/m$b;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    iget-object p3, p0, Lg0/l/c/m$a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p3

    invoke-direct {v1, p1, p2, p3}, Lg0/l/c/m$b;-><init>(Lrx/functions/Action0;Ljava/lang/Long;I)V

    iget-object p1, p0, Lg0/l/c/m$a;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lg0/l/c/m$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result p1

    if-nez p1, :cond_3

    :cond_1
    iget-object p1, p0, Lg0/l/c/m$a;->e:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {p1}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lg0/l/c/m$b;

    if-eqz p1, :cond_2

    iget-object p1, p1, Lg0/l/c/m$b;->d:Lrx/functions/Action0;

    invoke-interface {p1}, Lrx/functions/Action0;->call()V

    :cond_2
    iget-object p1, p0, Lg0/l/c/m$a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result p1

    if-gtz p1, :cond_1

    return-object v0

    :cond_3
    new-instance p1, Lg0/l/c/m$a$a;

    invoke-direct {p1, p0, v1}, Lg0/l/c/m$a$a;-><init>(Lg0/l/c/m$a;Lg0/l/c/m$b;)V

    new-instance p2, Lg0/r/a;

    invoke-direct {p2, p1}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    return-object p2
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/c/m$a;->f:Lg0/r/a;

    invoke-virtual {v0}, Lg0/r/a;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public unsubscribe()V
    .locals 1

    iget-object v0, p0, Lg0/l/c/m$a;->f:Lg0/r/a;

    invoke-virtual {v0}, Lg0/r/a;->unsubscribe()V

    return-void
.end method
