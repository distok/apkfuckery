.class public final Lg0/l/c/b$a;
.super Lrx/Scheduler$Worker;
.source "EventLoopsScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final d:Lrx/internal/util/SubscriptionList;

.field public final e:Lrx/subscriptions/CompositeSubscription;

.field public final f:Lrx/internal/util/SubscriptionList;

.field public final g:Lg0/l/c/b$c;


# direct methods
.method public constructor <init>(Lg0/l/c/b$c;)V
    .locals 5

    invoke-direct {p0}, Lrx/Scheduler$Worker;-><init>()V

    new-instance v0, Lrx/internal/util/SubscriptionList;

    invoke-direct {v0}, Lrx/internal/util/SubscriptionList;-><init>()V

    iput-object v0, p0, Lg0/l/c/b$a;->d:Lrx/internal/util/SubscriptionList;

    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, p0, Lg0/l/c/b$a;->e:Lrx/subscriptions/CompositeSubscription;

    new-instance v2, Lrx/internal/util/SubscriptionList;

    const/4 v3, 0x2

    new-array v3, v3, [Lrx/Subscription;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-direct {v2, v3}, Lrx/internal/util/SubscriptionList;-><init>([Lrx/Subscription;)V

    iput-object v2, p0, Lg0/l/c/b$a;->f:Lrx/internal/util/SubscriptionList;

    iput-object p1, p0, Lg0/l/c/b$a;->g:Lg0/l/c/b$c;

    return-void
.end method


# virtual methods
.method public a(Lrx/functions/Action0;)Lrx/Subscription;
    .locals 3

    iget-object v0, p0, Lg0/l/c/b$a;->f:Lrx/internal/util/SubscriptionList;

    iget-boolean v0, v0, Lrx/internal/util/SubscriptionList;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lg0/r/c;->a:Lg0/r/c$a;

    return-object p1

    :cond_0
    iget-object v0, p0, Lg0/l/c/b$a;->g:Lg0/l/c/b$c;

    new-instance v1, Lg0/l/c/b$a$a;

    invoke-direct {v1, p0, p1}, Lg0/l/c/b$a$a;-><init>(Lg0/l/c/b$a;Lrx/functions/Action0;)V

    iget-object p1, p0, Lg0/l/c/b$a;->d:Lrx/internal/util/SubscriptionList;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lg0/o/l;->d(Lrx/functions/Action0;)Lrx/functions/Action0;

    move-result-object v1

    new-instance v2, Lg0/l/c/j;

    invoke-direct {v2, v1, p1}, Lg0/l/c/j;-><init>(Lrx/functions/Action0;Lrx/internal/util/SubscriptionList;)V

    invoke-virtual {p1, v2}, Lrx/internal/util/SubscriptionList;->a(Lrx/Subscription;)V

    iget-object p1, v0, Lg0/l/c/g;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    invoke-virtual {v2, p1}, Lg0/l/c/j;->a(Ljava/util/concurrent/Future;)V

    return-object v2
.end method

.method public b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 5

    iget-object v0, p0, Lg0/l/c/b$a;->f:Lrx/internal/util/SubscriptionList;

    iget-boolean v0, v0, Lrx/internal/util/SubscriptionList;->e:Z

    if-eqz v0, :cond_0

    sget-object p1, Lg0/r/c;->a:Lg0/r/c$a;

    return-object p1

    :cond_0
    iget-object v0, p0, Lg0/l/c/b$a;->g:Lg0/l/c/b$c;

    new-instance v1, Lg0/l/c/b$a$b;

    invoke-direct {v1, p0, p1}, Lg0/l/c/b$a$b;-><init>(Lg0/l/c/b$a;Lrx/functions/Action0;)V

    iget-object p1, p0, Lg0/l/c/b$a;->e:Lrx/subscriptions/CompositeSubscription;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lg0/o/l;->d(Lrx/functions/Action0;)Lrx/functions/Action0;

    move-result-object v1

    new-instance v2, Lg0/l/c/j;

    invoke-direct {v2, v1, p1}, Lg0/l/c/j;-><init>(Lrx/functions/Action0;Lrx/subscriptions/CompositeSubscription;)V

    invoke-virtual {p1, v2}, Lrx/subscriptions/CompositeSubscription;->a(Lrx/Subscription;)V

    const-wide/16 v3, 0x0

    cmp-long p1, p2, v3

    if-gtz p1, :cond_1

    iget-object p1, v0, Lg0/l/c/g;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p1, v2}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object p1, v0, Lg0/l/c/g;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {p1, v2, p2, p3, p4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    :goto_0
    invoke-virtual {v2, p1}, Lg0/l/c/j;->a(Ljava/util/concurrent/Future;)V

    return-object v2
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/c/b$a;->f:Lrx/internal/util/SubscriptionList;

    iget-boolean v0, v0, Lrx/internal/util/SubscriptionList;->e:Z

    return v0
.end method

.method public unsubscribe()V
    .locals 1

    iget-object v0, p0, Lg0/l/c/b$a;->f:Lrx/internal/util/SubscriptionList;

    invoke-virtual {v0}, Lrx/internal/util/SubscriptionList;->unsubscribe()V

    return-void
.end method
