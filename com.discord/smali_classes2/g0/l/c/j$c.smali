.class public final Lg0/l/c/j$c;
.super Ljava/util/concurrent/atomic/AtomicBoolean;
.source "ScheduledAction.java"

# interfaces
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/c/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x36e5888d681586eL


# instance fields
.field public final parent:Lrx/subscriptions/CompositeSubscription;

.field public final s:Lg0/l/c/j;


# direct methods
.method public constructor <init>(Lg0/l/c/j;Lrx/subscriptions/CompositeSubscription;)V
    .locals 0

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lg0/l/c/j$c;->s:Lg0/l/c/j;

    iput-object p2, p0, Lg0/l/c/j$c;->parent:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method


# virtual methods
.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lg0/l/c/j$c;->s:Lg0/l/c/j;

    iget-object v0, v0, Lg0/l/c/j;->cancel:Lrx/internal/util/SubscriptionList;

    iget-boolean v0, v0, Lrx/internal/util/SubscriptionList;->e:Z

    return v0
.end method

.method public unsubscribe()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg0/l/c/j$c;->parent:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lg0/l/c/j$c;->s:Lg0/l/c/j;

    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->c(Lrx/Subscription;)V

    :cond_0
    return-void
.end method
