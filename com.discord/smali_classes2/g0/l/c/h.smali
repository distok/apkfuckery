.class public final Lg0/l/c/h;
.super Ljava/lang/Object;
.source "SchedulePeriodicHelper.java"

# interfaces
.implements Lrx/functions/Action0;


# instance fields
.field public d:J

.field public e:J

.field public f:J

.field public final synthetic g:J

.field public final synthetic h:J

.field public final synthetic i:Lrx/functions/Action0;

.field public final synthetic j:Lg0/l/d/a;

.field public final synthetic k:Lg0/l/c/i$a;

.field public final synthetic l:Lrx/Scheduler$Worker;

.field public final synthetic m:J


# direct methods
.method public constructor <init>(JJLrx/functions/Action0;Lg0/l/d/a;Lg0/l/c/i$a;Lrx/Scheduler$Worker;J)V
    .locals 0

    iput-wide p1, p0, Lg0/l/c/h;->g:J

    iput-wide p3, p0, Lg0/l/c/h;->h:J

    iput-object p5, p0, Lg0/l/c/h;->i:Lrx/functions/Action0;

    iput-object p6, p0, Lg0/l/c/h;->j:Lg0/l/d/a;

    iput-object p7, p0, Lg0/l/c/h;->k:Lg0/l/c/i$a;

    iput-object p8, p0, Lg0/l/c/h;->l:Lrx/Scheduler$Worker;

    iput-wide p9, p0, Lg0/l/c/h;->m:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lg0/l/c/h;->e:J

    iput-wide p3, p0, Lg0/l/c/h;->f:J

    return-void
.end method


# virtual methods
.method public call()V
    .locals 11

    iget-object v0, p0, Lg0/l/c/h;->i:Lrx/functions/Action0;

    invoke-interface {v0}, Lrx/functions/Action0;->call()V

    iget-object v0, p0, Lg0/l/c/h;->j:Lg0/l/d/a;

    invoke-virtual {v0}, Lg0/l/d/a;->isUnsubscribed()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lg0/l/c/h;->k:Lg0/l/c/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lg0/l/c/i$a;->a()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lg0/l/c/h;->l:Lrx/Scheduler$Worker;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    :goto_0
    sget-wide v2, Lg0/l/c/i;->a:J

    add-long v4, v0, v2

    iget-wide v6, p0, Lg0/l/c/h;->e:J

    const-wide/16 v8, 0x1

    cmp-long v10, v4, v6

    if-ltz v10, :cond_2

    iget-wide v4, p0, Lg0/l/c/h;->m:J

    add-long/2addr v6, v4

    add-long/2addr v6, v2

    cmp-long v2, v0, v6

    if-ltz v2, :cond_1

    goto :goto_1

    :cond_1
    iget-wide v2, p0, Lg0/l/c/h;->f:J

    iget-wide v6, p0, Lg0/l/c/h;->d:J

    add-long/2addr v6, v8

    iput-wide v6, p0, Lg0/l/c/h;->d:J

    mul-long v6, v6, v4

    add-long/2addr v6, v2

    goto :goto_2

    :cond_2
    :goto_1
    iget-wide v2, p0, Lg0/l/c/h;->m:J

    add-long v6, v0, v2

    iget-wide v4, p0, Lg0/l/c/h;->d:J

    add-long/2addr v4, v8

    iput-wide v4, p0, Lg0/l/c/h;->d:J

    mul-long v2, v2, v4

    sub-long v2, v6, v2

    iput-wide v2, p0, Lg0/l/c/h;->f:J

    :goto_2
    iput-wide v0, p0, Lg0/l/c/h;->e:J

    sub-long/2addr v6, v0

    iget-object v0, p0, Lg0/l/c/h;->j:Lg0/l/d/a;

    iget-object v1, p0, Lg0/l/c/h;->l:Lrx/Scheduler$Worker;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, p0, v6, v7, v2}, Lrx/Scheduler$Worker;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object v1

    invoke-virtual {v0, v1}, Lg0/l/d/a;->a(Lrx/Subscription;)Z

    :cond_3
    return-void
.end method
