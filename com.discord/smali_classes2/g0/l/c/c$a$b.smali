.class public Lg0/l/c/c$a$b;
.super Ljava/lang/Object;
.source "ExecutorScheduler.java"

# interfaces
.implements Lrx/functions/Action0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/c/c$a;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lg0/r/b;

.field public final synthetic e:Lrx/functions/Action0;

.field public final synthetic f:Lrx/Subscription;

.field public final synthetic g:Lg0/l/c/c$a;


# direct methods
.method public constructor <init>(Lg0/l/c/c$a;Lg0/r/b;Lrx/functions/Action0;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lg0/l/c/c$a$b;->g:Lg0/l/c/c$a;

    iput-object p2, p0, Lg0/l/c/c$a$b;->d:Lg0/r/b;

    iput-object p3, p0, Lg0/l/c/c$a$b;->e:Lrx/functions/Action0;

    iput-object p4, p0, Lg0/l/c/c$a$b;->f:Lrx/Subscription;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    iget-object v0, p0, Lg0/l/c/c$a$b;->d:Lg0/r/b;

    invoke-virtual {v0}, Lg0/r/b;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lg0/l/c/c$a$b;->g:Lg0/l/c/c$a;

    iget-object v1, p0, Lg0/l/c/c$a$b;->e:Lrx/functions/Action0;

    invoke-virtual {v0, v1}, Lg0/l/c/c$a;->a(Lrx/functions/Action0;)Lrx/Subscription;

    move-result-object v0

    iget-object v1, p0, Lg0/l/c/c$a$b;->d:Lg0/r/b;

    invoke-virtual {v1, v0}, Lg0/r/b;->a(Lrx/Subscription;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lg0/l/c/j;

    if-ne v1, v2, :cond_1

    check-cast v0, Lg0/l/c/j;

    iget-object v1, p0, Lg0/l/c/c$a$b;->f:Lrx/Subscription;

    iget-object v0, v0, Lg0/l/c/j;->cancel:Lrx/internal/util/SubscriptionList;

    invoke-virtual {v0, v1}, Lrx/internal/util/SubscriptionList;->a(Lrx/Subscription;)V

    :cond_1
    return-void
.end method
