.class public abstract Lg0/l/e/n/a;
.super Lg0/l/e/n/c;
.source "BaseLinkedQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/e/n/c<",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final e:J


# instance fields
.field public consumerNode:Lg0/l/e/m/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/e/m/b<",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lg0/l/e/n/a;

    const-string v1, "consumerNode"

    invoke-static {v0, v1}, Lg0/l/e/n/y;->a(Ljava/lang/Class;Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lg0/l/e/n/a;->e:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lg0/l/e/n/c;-><init>()V

    return-void
.end method


# virtual methods
.method public final d()Lg0/l/e/m/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lg0/l/e/m/b<",
            "TE;>;"
        }
    .end annotation

    sget-object v0, Lg0/l/e/n/y;->a:Lsun/misc/Unsafe;

    sget-wide v1, Lg0/l/e/n/a;->e:J

    invoke-virtual {v0, p0, v1, v2}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/e/m/b;

    return-object v0
.end method
