.class public abstract Lg0/l/e/n/l;
.super Lg0/l/e/n/j;
.source "SpmcArrayQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/e/n/j<",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final i:J


# instance fields
.field private volatile producerIndex:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lg0/l/e/n/l;

    const-string v1, "producerIndex"

    invoke-static {v0, v1}, Lg0/l/e/n/y;->a(Ljava/lang/Class;Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lg0/l/e/n/l;->i:J

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lg0/l/e/n/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final k()J
    .locals 2

    iget-wide v0, p0, Lg0/l/e/n/l;->producerIndex:J

    return-wide v0
.end method

.method public final l(J)V
    .locals 6

    sget-object v0, Lg0/l/e/n/y;->a:Lsun/misc/Unsafe;

    sget-wide v2, Lg0/l/e/n/l;->i:J

    move-object v1, p0

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->putOrderedLong(Ljava/lang/Object;JJ)V

    return-void
.end method
