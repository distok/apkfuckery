.class public abstract Lg0/l/e/n/d;
.super Lg0/l/e/n/b;
.source "BaseLinkedQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/e/n/b<",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final d:J


# instance fields
.field public producerNode:Lg0/l/e/m/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/l/e/m/b<",
            "TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lg0/l/e/n/d;

    const-string v1, "producerNode"

    invoke-static {v0, v1}, Lg0/l/e/n/y;->a(Ljava/lang/Class;Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lg0/l/e/n/d;->d:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lg0/l/e/n/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final c()Lg0/l/e/m/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lg0/l/e/m/b<",
            "TE;>;"
        }
    .end annotation

    sget-object v0, Lg0/l/e/n/y;->a:Lsun/misc/Unsafe;

    sget-wide v1, Lg0/l/e/n/d;->d:J

    invoke-virtual {v0, p0, v1, v2}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/e/m/b;

    return-object v0
.end method
