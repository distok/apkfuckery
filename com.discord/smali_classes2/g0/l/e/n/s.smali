.class public final Lg0/l/e/n/s;
.super Lg0/l/e/n/a;
.source "SpscLinkedQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/e/n/s<",
        "TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lg0/l/e/n/a;-><init>()V

    new-instance v0, Lg0/l/e/m/b;

    invoke-direct {v0}, Lg0/l/e/m/b;-><init>()V

    iput-object v0, p0, Lg0/l/e/n/d;->producerNode:Lg0/l/e/m/b;

    iput-object v0, p0, Lg0/l/e/n/a;->consumerNode:Lg0/l/e/m/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final isEmpty()Z
    .locals 2

    invoke-virtual {p0}, Lg0/l/e/n/a;->d()Lg0/l/e/m/b;

    move-result-object v0

    invoke-virtual {p0}, Lg0/l/e/n/d;->c()Lg0/l/e/m/b;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public offer(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    const-string v0, "null elements not allowed"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v0, Lg0/l/e/m/b;

    invoke-direct {v0, p1}, Lg0/l/e/m/b;-><init>(Ljava/lang/Object;)V

    iget-object p1, p0, Lg0/l/e/n/d;->producerNode:Lg0/l/e/m/b;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    iput-object v0, p0, Lg0/l/e/n/d;->producerNode:Lg0/l/e/m/b;

    const/4 p1, 0x1

    return p1
.end method

.method public peek()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/e/n/a;->consumerNode:Lg0/l/e/m/b;

    invoke-virtual {v0}, Lg0/l/e/m/b;->c()Lg0/l/e/m/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lg0/l/e/m/b;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public poll()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    iget-object v0, p0, Lg0/l/e/n/a;->consumerNode:Lg0/l/e/m/b;

    invoke-virtual {v0}, Lg0/l/e/m/b;->c()Lg0/l/e/m/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lg0/l/e/m/b;->a()Ljava/lang/Object;

    move-result-object v1

    iput-object v0, p0, Lg0/l/e/n/a;->consumerNode:Lg0/l/e/m/b;

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final size()I
    .locals 4

    invoke-virtual {p0}, Lg0/l/e/n/a;->d()Lg0/l/e/m/b;

    move-result-object v0

    invoke-virtual {p0}, Lg0/l/e/n/d;->c()Lg0/l/e/m/b;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    if-eq v0, v1, :cond_1

    const v3, 0x7fffffff

    if-ge v2, v3, :cond_1

    :goto_1
    invoke-virtual {v0}, Lg0/l/e/m/b;->c()Lg0/l/e/m/b;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    move-object v0, v3

    goto :goto_0

    :cond_1
    return v2
.end method
