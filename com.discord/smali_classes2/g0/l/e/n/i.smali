.class public abstract Lg0/l/e/n/i;
.super Lg0/l/e/n/k;
.source "SpmcArrayQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lg0/l/e/n/k<",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final j:J


# instance fields
.field private volatile consumerIndex:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lg0/l/e/n/i;

    const-string v1, "consumerIndex"

    invoke-static {v0, v1}, Lg0/l/e/n/y;->a(Ljava/lang/Class;Ljava/lang/String;)J

    move-result-wide v0

    sput-wide v0, Lg0/l/e/n/i;->j:J

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lg0/l/e/n/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final p(JJ)Z
    .locals 8

    sget-object v0, Lg0/l/e/n/y;->a:Lsun/misc/Unsafe;

    sget-wide v2, Lg0/l/e/n/i;->j:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result p1

    return p1
.end method

.method public final r()J
    .locals 2

    iget-wide v0, p0, Lg0/l/e/n/i;->consumerIndex:J

    return-wide v0
.end method
