.class public final enum Lg0/l/e/l;
.super Ljava/lang/Enum;
.source "UtilityFunctions.java"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lg0/l/e/l;",
        ">;",
        "Lg0/k/b<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lg0/l/e/l;

.field public static final synthetic e:[Lg0/l/e/l;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lg0/l/e/l;

    const-string v1, "INSTANCE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lg0/l/e/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lg0/l/e/l;->d:Lg0/l/e/l;

    const/4 v1, 0x1

    new-array v1, v1, [Lg0/l/e/l;

    aput-object v0, v1, v2

    sput-object v1, Lg0/l/e/l;->e:[Lg0/l/e/l;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lg0/l/e/l;
    .locals 1

    const-class v0, Lg0/l/e/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lg0/l/e/l;

    return-object p0
.end method

.method public static values()[Lg0/l/e/l;
    .locals 1

    sget-object v0, Lg0/l/e/l;->e:[Lg0/l/e/l;

    invoke-virtual {v0}, [Lg0/l/e/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lg0/l/e/l;

    return-object v0
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    return-object p1
.end method
