.class public Lg0/l/e/j$a;
.super Ljava/lang/Object;
.source "ScalarSynchronousObservable.java"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/e/j;->g0(Lrx/Scheduler;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lrx/functions/Action0;",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lg0/l/c/b;


# direct methods
.method public constructor <init>(Lg0/l/e/j;Lg0/l/c/b;)V
    .locals 0

    iput-object p2, p0, Lg0/l/e/j$a;->d:Lg0/l/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Lrx/functions/Action0;

    iget-object v0, p0, Lg0/l/e/j$a;->d:Lg0/l/c/b;

    iget-object v0, v0, Lg0/l/c/b;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/l/c/b$b;

    invoke-virtual {v0}, Lg0/l/c/b$b;->a()Lg0/l/c/b$c;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, p1, v2, v3, v1}, Lg0/l/c/g;->f(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lg0/l/c/j;

    move-result-object p1

    return-object p1
.end method
