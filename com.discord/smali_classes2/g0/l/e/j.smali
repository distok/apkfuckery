.class public final Lg0/l/e/j;
.super Lrx/Observable;
.source "ScalarSynchronousObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/l/e/j$g;,
        Lg0/l/e/j$f;,
        Lg0/l/e/j$e;,
        Lg0/l/e/j$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/Observable<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final f:Z


# instance fields
.field public final e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-string v0, "rx.just.strong-mode"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lg0/l/e/j;->f:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    new-instance v0, Lg0/l/e/j$d;

    invoke-direct {v0, p1}, Lg0/l/e/j$d;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lg0/o/l;->a(Lrx/Observable$a;)Lrx/Observable$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lrx/Observable;-><init>(Lrx/Observable$a;)V

    iput-object p1, p0, Lg0/l/e/j;->e:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public f0(Lg0/k/b;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lg0/k/b<",
            "-TT;+",
            "Lrx/Observable<",
            "+TR;>;>;)",
            "Lrx/Observable<",
            "TR;>;"
        }
    .end annotation

    new-instance v0, Lg0/l/e/j$c;

    invoke-direct {v0, p0, p1}, Lg0/l/e/j$c;-><init>(Lg0/l/e/j;Lg0/k/b;)V

    invoke-static {v0}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public g0(Lrx/Scheduler;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Scheduler;",
            ")",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    instance-of v0, p1, Lg0/l/c/b;

    if-eqz v0, :cond_0

    check-cast p1, Lg0/l/c/b;

    new-instance v0, Lg0/l/e/j$a;

    invoke-direct {v0, p0, p1}, Lg0/l/e/j$a;-><init>(Lg0/l/e/j;Lg0/l/c/b;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lg0/l/e/j$b;

    invoke-direct {v0, p0, p1}, Lg0/l/e/j$b;-><init>(Lg0/l/e/j;Lrx/Scheduler;)V

    :goto_0
    new-instance p1, Lg0/l/e/j$e;

    iget-object v1, p0, Lg0/l/e/j;->e:Ljava/lang/Object;

    invoke-direct {p1, v1, v0}, Lg0/l/e/j$e;-><init>(Ljava/lang/Object;Lg0/k/b;)V

    invoke-static {p1}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
