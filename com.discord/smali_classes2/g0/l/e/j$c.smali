.class public Lg0/l/e/j$c;
.super Ljava/lang/Object;
.source "ScalarSynchronousObservable.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg0/l/e/j;->f0(Lg0/k/b;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TR;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lg0/k/b;

.field public final synthetic e:Lg0/l/e/j;


# direct methods
.method public constructor <init>(Lg0/l/e/j;Lg0/k/b;)V
    .locals 0

    iput-object p1, p0, Lg0/l/e/j$c;->e:Lg0/l/e/j;

    iput-object p2, p0, Lg0/l/e/j$c;->d:Lg0/k/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/e/j$c;->d:Lg0/k/b;

    iget-object v1, p0, Lg0/l/e/j$c;->e:Lg0/l/e/j;

    iget-object v1, v1, Lg0/l/e/j;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Observable;

    instance-of v1, v0, Lg0/l/e/j;

    if-eqz v1, :cond_1

    check-cast v0, Lg0/l/e/j;

    iget-object v0, v0, Lg0/l/e/j;->e:Ljava/lang/Object;

    sget-boolean v1, Lg0/l/e/j;->f:Z

    if-eqz v1, :cond_0

    new-instance v1, Lg0/l/b/c;

    invoke-direct {v1, p1, v0}, Lg0/l/b/c;-><init>(Lrx/Subscriber;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lg0/l/e/j$g;

    invoke-direct {v1, p1, v0}, Lg0/l/e/j$g;-><init>(Lrx/Subscriber;Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    goto :goto_1

    :cond_1
    new-instance v1, Lg0/n/e;

    invoke-direct {v1, p1, p1}, Lg0/n/e;-><init>(Lrx/Subscriber;Lrx/Subscriber;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->d0(Lrx/Subscriber;)Lrx/Subscription;

    :goto_1
    return-void
.end method
