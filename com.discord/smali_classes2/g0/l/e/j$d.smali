.class public final Lg0/l/e/j$d;
.super Ljava/lang/Object;
.source "ScalarSynchronousObservable.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/l/e/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/l/e/j$d;->d:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lg0/l/e/j$d;->d:Ljava/lang/Object;

    sget-boolean v1, Lg0/l/e/j;->f:Z

    if-eqz v1, :cond_0

    new-instance v1, Lg0/l/b/c;

    invoke-direct {v1, p1, v0}, Lg0/l/b/c;-><init>(Lrx/Subscriber;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lg0/l/e/j$g;

    invoke-direct {v1, p1, v0}, Lg0/l/e/j$g;-><init>(Lrx/Subscriber;Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    return-void
.end method
