.class public final Lg0/p/a;
.super Ljava/lang/Object;
.source "Schedulers.java"


# static fields
.field public static final d:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lg0/p/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lrx/Scheduler;

.field public final b:Lrx/Scheduler;

.field public final c:Lrx/Scheduler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lg0/p/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lg0/o/o;->f:Lg0/o/o;

    invoke-virtual {v0}, Lg0/o/o;->e()Lg0/o/p;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lg0/l/e/i;

    const-string v1, "RxComputationScheduler-"

    invoke-direct {v0, v1}, Lg0/l/e/i;-><init>(Ljava/lang/String;)V

    new-instance v1, Lg0/l/c/b;

    invoke-direct {v1, v0}, Lg0/l/c/b;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lg0/p/a;->a:Lrx/Scheduler;

    new-instance v0, Lg0/l/e/i;

    const-string v1, "RxIoScheduler-"

    invoke-direct {v0, v1}, Lg0/l/e/i;-><init>(Ljava/lang/String;)V

    new-instance v1, Lg0/l/c/a;

    invoke-direct {v1, v0}, Lg0/l/c/a;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lg0/p/a;->b:Lrx/Scheduler;

    new-instance v0, Lg0/l/e/i;

    const-string v1, "RxNewThreadScheduler-"

    invoke-direct {v0, v1}, Lg0/l/e/i;-><init>(Ljava/lang/String;)V

    new-instance v1, Lg0/l/c/f;

    invoke-direct {v1, v0}, Lg0/l/c/f;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lg0/p/a;->c:Lrx/Scheduler;

    return-void
.end method

.method public static a()Lrx/Scheduler;
    .locals 1

    invoke-static {}, Lg0/p/a;->b()Lg0/p/a;

    move-result-object v0

    iget-object v0, v0, Lg0/p/a;->a:Lrx/Scheduler;

    return-object v0
.end method

.method public static b()Lg0/p/a;
    .locals 3

    :goto_0
    sget-object v0, Lg0/p/a;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg0/p/a;

    if-eqz v1, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Lg0/p/a;

    invoke-direct {v1}, Lg0/p/a;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v1

    :cond_1
    monitor-enter v1

    :try_start_0
    iget-object v0, v1, Lg0/p/a;->a:Lrx/Scheduler;

    instance-of v2, v0, Lg0/l/c/k;

    if-eqz v2, :cond_2

    check-cast v0, Lg0/l/c/k;

    invoke-interface {v0}, Lg0/l/c/k;->shutdown()V

    :cond_2
    iget-object v0, v1, Lg0/p/a;->b:Lrx/Scheduler;

    instance-of v2, v0, Lg0/l/c/k;

    if-eqz v2, :cond_3

    check-cast v0, Lg0/l/c/k;

    invoke-interface {v0}, Lg0/l/c/k;->shutdown()V

    :cond_3
    iget-object v0, v1, Lg0/p/a;->c:Lrx/Scheduler;

    instance-of v2, v0, Lg0/l/c/k;

    if-eqz v2, :cond_4

    check-cast v0, Lg0/l/c/k;

    invoke-interface {v0}, Lg0/l/c/k;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c()Lrx/Scheduler;
    .locals 1

    invoke-static {}, Lg0/p/a;->b()Lg0/p/a;

    move-result-object v0

    iget-object v0, v0, Lg0/p/a;->b:Lrx/Scheduler;

    return-object v0
.end method
