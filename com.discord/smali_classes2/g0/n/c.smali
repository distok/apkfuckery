.class public Lg0/n/c;
.super Ljava/lang/Object;
.source "SerializedObserver.java"

# interfaces
.implements Lg0/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/n/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/g<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lg0/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/g<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public e:Z

.field public volatile f:Z

.field public g:Lg0/n/c$a;


# direct methods
.method public constructor <init>(Lg0/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/g<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg0/n/c;->d:Lg0/g;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    iget-boolean v0, p0, Lg0/n/c;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lg0/n/c;->f:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/n/c;->f:Z

    iget-boolean v1, p0, Lg0/n/c;->e:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lg0/n/c;->g:Lg0/n/c$a;

    if-nez v0, :cond_2

    new-instance v0, Lg0/n/c$a;

    invoke-direct {v0}, Lg0/n/c$a;-><init>()V

    iput-object v0, p0, Lg0/n/c;->g:Lg0/n/c$a;

    :cond_2
    sget-object v1, Lg0/l/a/h;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lg0/n/c$a;->a(Ljava/lang/Object;)V

    monitor-exit p0

    return-void

    :cond_3
    iput-boolean v0, p0, Lg0/n/c;->e:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lg0/n/c;->d:Lg0/g;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2

    invoke-static {p1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    iget-boolean v0, p0, Lg0/n/c;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lg0/n/c;->f:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/n/c;->f:Z

    iget-boolean v1, p0, Lg0/n/c;->e:Z

    if-eqz v1, :cond_3

    iget-object v0, p0, Lg0/n/c;->g:Lg0/n/c$a;

    if-nez v0, :cond_2

    new-instance v0, Lg0/n/c$a;

    invoke-direct {v0}, Lg0/n/c$a;-><init>()V

    iput-object v0, p0, Lg0/n/c;->g:Lg0/n/c$a;

    :cond_2
    new-instance v1, Lg0/l/a/h$c;

    invoke-direct {v1, p1}, Lg0/l/a/h$c;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lg0/n/c$a;->a(Ljava/lang/Object;)V

    monitor-exit p0

    return-void

    :cond_3
    iput-boolean v0, p0, Lg0/n/c;->e:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lg0/n/c;->d:Lg0/g;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lg0/n/c;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lg0/n/c;->f:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    iget-boolean v0, p0, Lg0/n/c;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lg0/n/c;->g:Lg0/n/c$a;

    if-nez v0, :cond_2

    new-instance v0, Lg0/n/c$a;

    invoke-direct {v0}, Lg0/n/c$a;-><init>()V

    iput-object v0, p0, Lg0/n/c;->g:Lg0/n/c$a;

    :cond_2
    if-nez p1, :cond_3

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_3
    invoke-virtual {v0, p1}, Lg0/n/c$a;->a(Ljava/lang/Object;)V

    monitor-exit p0

    return-void

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/n/c;->e:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    iget-object v1, p0, Lg0/n/c;->d:Lg0/g;

    invoke-interface {v1, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_5
    :goto_0
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lg0/n/c;->g:Lg0/n/c$a;

    const/4 v2, 0x0

    if-nez v1, :cond_6

    iput-boolean v2, p0, Lg0/n/c;->e:Z

    monitor-exit p0

    return-void

    :cond_6
    const/4 v3, 0x0

    iput-object v3, p0, Lg0/n/c;->g:Lg0/n/c$a;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v1, v1, Lg0/n/c$a;->a:[Ljava/lang/Object;

    array-length v3, v1

    :goto_1
    if-ge v2, v3, :cond_5

    aget-object v4, v1, v2

    if-nez v4, :cond_7

    goto :goto_0

    :cond_7
    :try_start_3
    iget-object v5, p0, Lg0/n/c;->d:Lg0/g;

    invoke-static {v5, v4}, Lg0/l/a/h;->a(Lg0/g;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iput-boolean v0, p0, Lg0/n/c;->f:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    iput-boolean v0, p0, Lg0/n/c;->f:Z

    invoke-static {v1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lg0/n/c;->d:Lg0/g;

    invoke-static {v1, p1}, Lrx/exceptions/OnErrorThrowable;->a(Ljava/lang/Throwable;Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void

    :catchall_1
    move-exception p1

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1

    :catchall_2
    move-exception v1

    iput-boolean v0, p0, Lg0/n/c;->f:Z

    iget-object v0, p0, Lg0/n/c;->d:Lg0/g;

    invoke-static {v1, v0, p1}, Ly/a/g0;->Q(Ljava/lang/Throwable;Lg0/g;Ljava/lang/Object;)V

    return-void

    :catchall_3
    move-exception p1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw p1
.end method
