.class public Lg0/j/b/b$a;
.super Lrx/Scheduler$Worker;
.source "LooperScheduler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/j/b/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final d:Landroid/os/Handler;

.field public final e:Lg0/j/a/b;

.field public volatile f:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Lrx/Scheduler$Worker;-><init>()V

    iput-object p1, p0, Lg0/j/b/b$a;->d:Landroid/os/Handler;

    sget-object p1, Lg0/j/a/a;->b:Lg0/j/a/a;

    invoke-virtual {p1}, Lg0/j/a/a;->a()Lg0/j/a/b;

    move-result-object p1

    iput-object p1, p0, Lg0/j/b/b$a;->e:Lg0/j/a/b;

    return-void
.end method


# virtual methods
.method public a(Lrx/functions/Action0;)Lrx/Subscription;
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x0

    invoke-virtual {p0, p1, v1, v2, v0}, Lg0/j/b/b$a;->b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public b(Lrx/functions/Action0;JLjava/util/concurrent/TimeUnit;)Lrx/Subscription;
    .locals 3

    sget-object v0, Lg0/r/c;->a:Lg0/r/c$a;

    iget-boolean v1, p0, Lg0/j/b/b$a;->f:Z

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lg0/j/b/b$a;->e:Lg0/j/a/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lg0/j/b/b$b;

    iget-object v2, p0, Lg0/j/b/b$a;->d:Landroid/os/Handler;

    invoke-direct {v1, p1, v2}, Lg0/j/b/b$b;-><init>(Lrx/functions/Action0;Landroid/os/Handler;)V

    invoke-static {v2, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object p1

    iput-object p0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lg0/j/b/b$a;->d:Landroid/os/Handler;

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p2

    invoke-virtual {v2, p1, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-boolean p1, p0, Lg0/j/b/b$a;->f:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lg0/j/b/b$a;->d:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-object v0

    :cond_1
    return-object v1
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-boolean v0, p0, Lg0/j/b/b$a;->f:Z

    return v0
.end method

.method public unsubscribe()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/j/b/b$a;->f:Z

    iget-object v0, p0, Lg0/j/b/b$a;->d:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method
