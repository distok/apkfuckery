.class public final Lg0/q/a$c;
.super Ljava/lang/Object;
.source "ReplaySubject.java"

# interfaces
.implements Lg0/q/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/q/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/q/a$c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/q/a$a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:I

.field public volatile b:Lg0/q/a$c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/q/a$c$a<",
            "TT;>;"
        }
    .end annotation
.end field

.field public c:Lg0/q/a$c$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/q/a$c$a<",
            "TT;>;"
        }
    .end annotation
.end field

.field public d:I

.field public volatile e:Z

.field public f:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lg0/q/a$c;->a:I

    new-instance p1, Lg0/q/a$c$a;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lg0/q/a$c$a;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lg0/q/a$c;->c:Lg0/q/a$c$a;

    iput-object p1, p0, Lg0/q/a$c;->b:Lg0/q/a$c$a;

    return-void
.end method


# virtual methods
.method public a(Lg0/q/a$b;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/q/a$b<",
            "TT;>;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_0
    iget-object v2, v1, Lg0/q/a$b;->actual:Lrx/Subscriber;

    const/4 v4, 0x1

    :cond_1
    iget-object v5, v1, Lg0/q/a$b;->requested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v5

    iget-object v7, v1, Lg0/q/a$b;->node:Ljava/lang/Object;

    check-cast v7, Lg0/q/a$c$a;

    const-wide/16 v8, 0x0

    if-nez v7, :cond_2

    iget-object v7, v0, Lg0/q/a$c;->b:Lg0/q/a$c$a;

    :cond_2
    move-wide v10, v8

    :goto_0
    const/4 v12, 0x0

    const/4 v13, 0x0

    cmp-long v14, v10, v5

    if-eqz v14, :cond_8

    invoke-virtual {v2}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v15

    if-eqz v15, :cond_3

    iput-object v13, v1, Lg0/q/a$b;->node:Ljava/lang/Object;

    return-void

    :cond_3
    iget-boolean v15, v0, Lg0/q/a$c;->e:Z

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v3, v16

    check-cast v3, Lg0/q/a$c$a;

    if-nez v3, :cond_4

    const/16 v16, 0x1

    goto :goto_1

    :cond_4
    const/16 v16, 0x0

    :goto_1
    if-eqz v15, :cond_6

    if-eqz v16, :cond_6

    iput-object v13, v1, Lg0/q/a$b;->node:Ljava/lang/Object;

    iget-object v1, v0, Lg0/q/a$c;->f:Ljava/lang/Throwable;

    if-eqz v1, :cond_5

    invoke-interface {v2, v1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_5
    invoke-interface {v2}, Lg0/g;->onCompleted()V

    :goto_2
    return-void

    :cond_6
    if-eqz v16, :cond_7

    goto :goto_3

    :cond_7
    iget-object v7, v3, Lg0/q/a$c$a;->value:Ljava/lang/Object;

    invoke-interface {v2, v7}, Lg0/g;->onNext(Ljava/lang/Object;)V

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    move-object v7, v3

    goto :goto_0

    :cond_8
    :goto_3
    if-nez v14, :cond_c

    invoke-virtual {v2}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v3

    if-eqz v3, :cond_9

    iput-object v13, v1, Lg0/q/a$b;->node:Ljava/lang/Object;

    return-void

    :cond_9
    iget-boolean v3, v0, Lg0/q/a$c;->e:Z

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v14

    if-nez v14, :cond_a

    const/4 v12, 0x1

    :cond_a
    if-eqz v3, :cond_c

    if-eqz v12, :cond_c

    iput-object v13, v1, Lg0/q/a$b;->node:Ljava/lang/Object;

    iget-object v1, v0, Lg0/q/a$c;->f:Ljava/lang/Throwable;

    if-eqz v1, :cond_b

    invoke-interface {v2, v1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_b
    invoke-interface {v2}, Lg0/g;->onCompleted()V

    :goto_4
    return-void

    :cond_c
    cmp-long v3, v10, v8

    if-eqz v3, :cond_d

    const-wide v8, 0x7fffffffffffffffL

    cmp-long v3, v5, v8

    if-eqz v3, :cond_d

    iget-object v3, v1, Lg0/q/a$b;->requested:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {v3, v10, v11}, Ly/a/g0;->x(Ljava/util/concurrent/atomic/AtomicLong;J)J

    :cond_d
    iput-object v7, v1, Lg0/q/a$b;->node:Ljava/lang/Object;

    neg-int v3, v4

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v4

    if-nez v4, :cond_1

    return-void
.end method
