.class public final Lg0/q/a;
.super Lrx/subjects/Subject;
.source "ReplaySubject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/q/a$b;,
        Lg0/q/a$c;,
        Lg0/q/a$a;,
        Lg0/q/a$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/subjects/Subject<",
        "TT;TT;>;"
    }
.end annotation


# static fields
.field public static final f:[Ljava/lang/Object;


# instance fields
.field public final e:Lg0/q/a$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/q/a$d<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lg0/q/a;->f:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lg0/q/a$d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/q/a$d<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lrx/subjects/Subject;-><init>(Lrx/Observable$a;)V

    iput-object p1, p0, Lg0/q/a;->e:Lg0/q/a$d;

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/q/a;->e:Lg0/q/a$d;

    invoke-virtual {v0}, Lg0/q/a$d;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/q/a;->e:Lg0/q/a$d;

    invoke-virtual {v0, p1}, Lg0/q/a$d;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/q/a;->e:Lg0/q/a$d;

    invoke-virtual {v0, p1}, Lg0/q/a$d;->onNext(Ljava/lang/Object;)V

    return-void
.end method
