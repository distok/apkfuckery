.class public final Lg0/q/c;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "SubjectSubscriptionManager.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0/q/c$b;,
        Lg0/q/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference<",
        "Lg0/q/c$a<",
        "TT;>;>;",
        "Lrx/Observable$a<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x53c184d753c8b010L


# instance fields
.field public active:Z

.field public volatile latest:Ljava/lang/Object;

.field public onAdded:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lg0/q/c$b<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public onStart:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lg0/q/c$b<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field public onTerminated:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lg0/q/c$b<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lg0/q/c$a;->e:Lg0/q/c$a;

    invoke-direct {p0, v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/q/c;->active:Z

    sget-object v0, Lg0/k/a;->a:Lg0/k/a$b;

    iput-object v0, p0, Lg0/q/c;->onStart:Lrx/functions/Action1;

    iput-object v0, p0, Lg0/q/c;->onAdded:Lrx/functions/Action1;

    iput-object v0, p0, Lg0/q/c;->onTerminated:Lrx/functions/Action1;

    return-void
.end method


# virtual methods
.method public a(Lg0/q/c$b;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/q/c$b<",
            "TT;>;)V"
        }
    .end annotation

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/q/c$a;

    iget-boolean v1, v0, Lg0/q/c$a;->a:Z

    if-eqz v1, :cond_1

    return-void

    :cond_1
    sget-object v1, Lg0/q/c$a;->e:Lg0/q/c$a;

    iget-object v2, v0, Lg0/q/c$a;->b:[Lg0/q/c$b;

    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    aget-object v5, v2, v4

    if-ne v5, p1, :cond_2

    goto :goto_2

    :cond_2
    if-nez v3, :cond_3

    :goto_0
    move-object v1, v0

    goto :goto_2

    :cond_3
    add-int/lit8 v5, v3, -0x1

    new-array v6, v5, [Lg0/q/c$b;

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_1
    if-ge v7, v3, :cond_6

    aget-object v9, v2, v7

    if-eq v9, p1, :cond_5

    if-ne v8, v5, :cond_4

    goto :goto_0

    :cond_4
    add-int/lit8 v10, v8, 0x1

    aput-object v9, v6, v8

    move v8, v10

    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_6
    if-nez v8, :cond_7

    goto :goto_2

    :cond_7
    if-ge v8, v5, :cond_8

    new-array v1, v8, [Lg0/q/c$b;

    invoke-static {v6, v4, v1, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v6, v1

    :cond_8
    new-instance v1, Lg0/q/c$a;

    iget-boolean v2, v0, Lg0/q/c$a;->a:Z

    invoke-direct {v1, v2, v6}, Lg0/q/c$a;-><init>(Z[Lg0/q/c$b;)V

    :goto_2
    if-eq v1, v0, :cond_9

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_9
    return-void
.end method

.method public b(Ljava/lang/Object;)[Lg0/q/c$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")[",
            "Lg0/q/c$b<",
            "TT;>;"
        }
    .end annotation

    iput-object p1, p0, Lg0/q/c;->latest:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lg0/q/c;->active:Z

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lg0/q/c$a;

    iget-boolean p1, p1, Lg0/q/c$a;->a:Z

    if-eqz p1, :cond_0

    sget-object p1, Lg0/q/c$a;->c:[Lg0/q/c$b;

    return-object p1

    :cond_0
    sget-object p1, Lg0/q/c$a;->d:Lg0/q/c$a;

    invoke-virtual {p0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lg0/q/c$a;

    iget-object p1, p1, Lg0/q/c$a;->b:[Lg0/q/c$b;

    return-object p1
.end method

.method public call(Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/q/c$b;

    invoke-direct {v0, p1}, Lg0/q/c$b;-><init>(Lrx/Subscriber;)V

    new-instance v1, Lg0/q/b;

    invoke-direct {v1, p0, v0}, Lg0/q/b;-><init>(Lg0/q/c;Lg0/q/c$b;)V

    new-instance v2, Lg0/r/a;

    invoke-direct {v2, v1}, Lg0/r/a;-><init>(Lrx/functions/Action0;)V

    invoke-virtual {p1, v2}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    iget-object v1, p0, Lg0/q/c;->onStart:Lrx/functions/Action1;

    invoke-interface {v1, v0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg0/q/c$a;

    iget-boolean v2, v1, Lg0/q/c$a;->a:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v1, p0, Lg0/q/c;->onTerminated:Lrx/functions/Action1;

    invoke-interface {v1, v0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v2, v1, Lg0/q/c$a;->b:[Lg0/q/c$b;

    array-length v4, v2

    add-int/lit8 v5, v4, 0x1

    new-array v5, v5, [Lg0/q/c$b;

    invoke-static {v2, v3, v5, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-object v0, v5, v4

    new-instance v2, Lg0/q/c$a;

    iget-boolean v3, v1, Lg0/q/c$a;->a:Z

    invoke-direct {v2, v3, v5}, Lg0/q/c$a;-><init>(Z[Lg0/q/c$b;)V

    invoke-virtual {p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lg0/q/c;->onAdded:Lrx/functions/Action1;

    invoke-interface {v1, v0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lrx/Subscriber;->isUnsubscribed()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0, v0}, Lg0/q/c;->a(Lg0/q/c$b;)V

    :cond_2
    return-void
.end method
