.class public final Lg0/q/a$d;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ReplaySubject.java"

# interfaces
.implements Lrx/Observable$a;
.implements Lg0/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/q/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference<",
        "[",
        "Lg0/q/a$b<",
        "TT;>;>;",
        "Lrx/Observable$a<",
        "TT;>;",
        "Lg0/g<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static final d:[Lg0/q/a$b;

.field public static final e:[Lg0/q/a$b;

.field private static final serialVersionUID:J = 0x529b0a217109d450L


# instance fields
.field public final buffer:Lg0/q/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/q/a$a<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    new-array v1, v0, [Lg0/q/a$b;

    sput-object v1, Lg0/q/a$d;->d:[Lg0/q/a$b;

    new-array v0, v0, [Lg0/q/a$b;

    sput-object v0, Lg0/q/a$d;->e:[Lg0/q/a$b;

    return-void
.end method

.method public constructor <init>(Lg0/q/a$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/q/a$a<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lg0/q/a$d;->buffer:Lg0/q/a$a;

    sget-object p1, Lg0/q/a$d;->d:[Lg0/q/a$b;

    invoke-virtual {p0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Lg0/q/a$b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg0/q/a$b<",
            "TT;>;)V"
        }
    .end annotation

    sget-object v0, Lg0/q/a$d;->d:[Lg0/q/a$b;

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lg0/q/a$b;

    sget-object v2, Lg0/q/a$d;->e:[Lg0/q/a$b;

    if-eq v1, v2, :cond_6

    if-ne v1, v0, :cond_1

    goto :goto_3

    :cond_1
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_3

    aget-object v5, v1, v4

    if-ne v5, p1, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v4, -0x1

    :goto_1
    if-gez v4, :cond_4

    return-void

    :cond_4
    const/4 v5, 0x1

    if-ne v2, v5, :cond_5

    move-object v6, v0

    goto :goto_2

    :cond_5
    add-int/lit8 v6, v2, -0x1

    new-array v6, v6, [Lg0/q/a$b;

    invoke-static {v1, v3, v6, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v4, 0x1

    sub-int/2addr v2, v4

    sub-int/2addr v2, v5

    invoke-static {v1, v3, v6, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_2
    invoke-virtual {p0, v1, v6}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_6
    :goto_3
    return-void
.end method

.method public call(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lrx/Subscriber;

    new-instance v0, Lg0/q/a$b;

    invoke-direct {v0, p1, p0}, Lg0/q/a$b;-><init>(Lrx/Subscriber;Lg0/q/a$d;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    :cond_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lg0/q/a$b;

    sget-object v1, Lg0/q/a$d;->e:[Lg0/q/a$b;

    const/4 v2, 0x0

    if-ne p1, v1, :cond_1

    goto :goto_0

    :cond_1
    array-length v1, p1

    add-int/lit8 v3, v1, 0x1

    new-array v3, v3, [Lg0/q/a$b;

    invoke-static {p1, v2, v3, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-object v0, v3, v1

    invoke-virtual {p0, p1, v3}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lg0/q/a$b;->isUnsubscribed()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0, v0}, Lg0/q/a$d;->a(Lg0/q/a$b;)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lg0/q/a$d;->buffer:Lg0/q/a$a;

    check-cast p1, Lg0/q/a$c;

    invoke-virtual {p1, v0}, Lg0/q/a$c;->a(Lg0/q/a$b;)V

    :goto_1
    return-void
.end method

.method public onCompleted()V
    .locals 6

    iget-object v0, p0, Lg0/q/a$d;->buffer:Lg0/q/a$a;

    move-object v1, v0

    check-cast v1, Lg0/q/a$c;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lg0/q/a$c;->e:Z

    sget-object v1, Lg0/q/a$d;->e:[Lg0/q/a$b;

    invoke-virtual {p0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lg0/q/a$b;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    move-object v5, v0

    check-cast v5, Lg0/q/a$c;

    invoke-virtual {v5, v4}, Lg0/q/a$c;->a(Lg0/q/a$b;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 6

    iget-object v0, p0, Lg0/q/a$d;->buffer:Lg0/q/a$a;

    move-object v1, v0

    check-cast v1, Lg0/q/a$c;

    iput-object p1, v1, Lg0/q/a$c;->f:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, v1, Lg0/q/a$c;->e:Z

    const/4 p1, 0x0

    sget-object v1, Lg0/q/a$d;->e:[Lg0/q/a$b;

    invoke-virtual {p0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lg0/q/a$b;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    move-object v5, v0

    check-cast v5, Lg0/q/a$c;

    :try_start_0
    invoke-virtual {v5, v4}, Lg0/q/a$c;->a(Lg0/q/a$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Ly/a/g0;->O(Ljava/util/List;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/q/a$d;->buffer:Lg0/q/a$a;

    move-object v1, v0

    check-cast v1, Lg0/q/a$c;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lg0/q/a$c$a;

    invoke-direct {v2, p1}, Lg0/q/a$c$a;-><init>(Ljava/lang/Object;)V

    iget-object p1, v1, Lg0/q/a$c;->c:Lg0/q/a$c$a;

    invoke-virtual {p1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iput-object v2, v1, Lg0/q/a$c;->c:Lg0/q/a$c$a;

    iget p1, v1, Lg0/q/a$c;->d:I

    iget v2, v1, Lg0/q/a$c;->a:I

    if-ne p1, v2, :cond_0

    iget-object p1, v1, Lg0/q/a$c;->b:Lg0/q/a$c$a;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lg0/q/a$c$a;

    iput-object p1, v1, Lg0/q/a$c;->b:Lg0/q/a$c$a;

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, 0x1

    iput p1, v1, Lg0/q/a$c;->d:I

    :goto_0
    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lg0/q/a$b;

    array-length v1, p1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    move-object v4, v0

    check-cast v4, Lg0/q/a$c;

    invoke-virtual {v4, v3}, Lg0/q/a$c;->a(Lg0/q/a$b;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
