.class public final Lg0/q/c$b;
.super Ljava/lang/Object;
.source "SubjectSubscriptionManager.java"

# interfaces
.implements Lg0/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg0/q/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/g<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lrx/Subscriber;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Subscriber<",
            "-TT;>;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>(Lrx/Subscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/q/c$b;->e:Z

    iput-object p1, p0, Lg0/q/c$b;->d:Lrx/Subscriber;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1

    iget-boolean v0, p0, Lg0/q/c$b;->h:Z

    if-nez v0, :cond_2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lg0/q/c$b;->e:Z

    iget-boolean v0, p0, Lg0/q/c$b;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lg0/q/c$b;->g:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg0/q/c$b;->g:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lg0/q/c$b;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return-void

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg0/q/c$b;->h:Z

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_2
    :goto_0
    iget-object v0, p0, Lg0/q/c$b;->d:Lrx/Subscriber;

    invoke-static {v0, p1}, Lg0/l/a/h;->a(Lg0/g;Ljava/lang/Object;)Z

    return-void
.end method

.method public onCompleted()V
    .locals 1

    iget-object v0, p0, Lg0/q/c$b;->d:Lrx/Subscriber;

    invoke-interface {v0}, Lg0/g;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lg0/q/c$b;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lg0/q/c$b;->d:Lrx/Subscriber;

    invoke-interface {v0, p1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    return-void
.end method
