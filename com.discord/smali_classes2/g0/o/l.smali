.class public final Lg0/o/l;
.super Ljava/lang/Object;
.source "RxJavaHooks.java"


# static fields
.field public static volatile a:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile b:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Lrx/Observable$a;",
            "Lrx/Observable$a;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile c:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Lg0/h$a;",
            "Lg0/h$a;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile d:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Lg0/d$a;",
            "Lg0/d$a;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile e:Lrx/functions/Func2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func2<",
            "Lrx/Observable;",
            "Lrx/Observable$a;",
            "Lrx/Observable$a;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile f:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Lrx/functions/Action0;",
            "Lrx/functions/Action0;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile g:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Lrx/Subscription;",
            "Lrx/Subscription;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile h:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Ljava/lang/Throwable;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public static volatile i:Lg0/k/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/k/b<",
            "Lrx/Observable$b;",
            "Lrx/Observable$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg0/o/f;

    invoke-direct {v0}, Lg0/o/f;-><init>()V

    sput-object v0, Lg0/o/l;->a:Lrx/functions/Action1;

    new-instance v0, Lg0/o/g;

    invoke-direct {v0}, Lg0/o/g;-><init>()V

    sput-object v0, Lg0/o/l;->e:Lrx/functions/Func2;

    new-instance v0, Lg0/o/h;

    invoke-direct {v0}, Lg0/o/h;-><init>()V

    sput-object v0, Lg0/o/l;->g:Lg0/k/b;

    new-instance v0, Lg0/o/i;

    invoke-direct {v0}, Lg0/o/i;-><init>()V

    sput-object v0, Lg0/o/l;->f:Lg0/k/b;

    new-instance v0, Lg0/o/j;

    invoke-direct {v0}, Lg0/o/j;-><init>()V

    sput-object v0, Lg0/o/l;->h:Lg0/k/b;

    new-instance v0, Lg0/o/k;

    invoke-direct {v0}, Lg0/o/k;-><init>()V

    sput-object v0, Lg0/o/l;->i:Lg0/k/b;

    new-instance v0, Lg0/o/c;

    invoke-direct {v0}, Lg0/o/c;-><init>()V

    sput-object v0, Lg0/o/l;->b:Lg0/k/b;

    new-instance v0, Lg0/o/d;

    invoke-direct {v0}, Lg0/o/d;-><init>()V

    sput-object v0, Lg0/o/l;->c:Lg0/k/b;

    new-instance v0, Lg0/o/e;

    invoke-direct {v0}, Lg0/o/e;-><init>()V

    sput-object v0, Lg0/o/l;->d:Lg0/k/b;

    return-void
.end method

.method public static a(Lrx/Observable$a;)Lrx/Observable$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$a<",
            "TT;>;)",
            "Lrx/Observable$a<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lg0/o/l;->b:Lg0/k/b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lrx/Observable$a;

    :cond_0
    return-object p0
.end method

.method public static b(Ljava/lang/Throwable;)V
    .locals 4

    sget-object v0, Lg0/o/l;->a:Lrx/functions/Action1;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v2, "The onError handler threw an Exception. It shouldn\'t. => "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-interface {v1, v0, p0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static c(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1

    sget-object v0, Lg0/o/l;->h:Lg0/k/b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Throwable;

    :cond_0
    return-object p0
.end method

.method public static d(Lrx/functions/Action0;)Lrx/functions/Action0;
    .locals 1

    sget-object v0, Lg0/o/l;->f:Lg0/k/b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lg0/k/b;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lrx/functions/Action0;

    :cond_0
    return-object p0
.end method
