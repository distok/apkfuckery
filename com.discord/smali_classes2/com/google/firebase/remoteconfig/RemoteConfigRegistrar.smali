.class public Lcom/google/firebase/remoteconfig/RemoteConfigRegistrar;
.super Ljava/lang/Object;
.source "RemoteConfigRegistrar.java"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambda$getComponents$0(Lf/h/c/m/e;)Lf/h/c/a0/i;
    .locals 9

    new-instance v6, Lf/h/c/a0/i;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/content/Context;

    const-class v0, Lf/h/c/c;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lf/h/c/c;

    const-class v0, Lf/h/c/v/g;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lf/h/c/v/g;

    const-class v0, Lf/h/c/j/c/a;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/c/j/c/a;

    const-string v4, "frc"

    monitor-enter v0

    :try_start_0
    iget-object v5, v0, Lf/h/c/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v0, Lf/h/c/j/c/a;->a:Ljava/util/Map;

    new-instance v7, Lf/h/c/j/b;

    iget-object v8, v0, Lf/h/c/j/c/a;->c:Lf/h/c/k/a/a;

    invoke-direct {v7, v8, v4}, Lf/h/c/j/b;-><init>(Lf/h/c/k/a/a;Ljava/lang/String;)V

    invoke-interface {v5, v4, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v5, v0, Lf/h/c/j/c/a;->a:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/h/c/j/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    const-class v0, Lf/h/c/k/a/a;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    move-object v5, p0

    check-cast v5, Lf/h/c/k/a/a;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lf/h/c/a0/i;-><init>(Landroid/content/Context;Lf/h/c/c;Lf/h/c/v/g;Lf/h/c/j/b;Lf/h/c/k/a/a;)V

    return-object v6

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Lf/h/c/m/d;

    const-class v2, Lf/h/c/a0/i;

    invoke-static {v2}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v2

    const-class v3, Landroid/content/Context;

    new-instance v4, Lf/h/c/m/o;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/c;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/v/g;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/j/c/a;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/k/a/a;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v6, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v3, Lf/h/c/a0/j;->a:Lf/h/c/a0/j;

    invoke-virtual {v2, v3}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v2, v0}, Lf/h/c/m/d$b;->d(I)Lf/h/c/m/d$b;

    invoke-virtual {v2}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v0

    aput-object v0, v1, v6

    const-string v0, "fire-rc"

    const-string v2, "20.0.1"

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
