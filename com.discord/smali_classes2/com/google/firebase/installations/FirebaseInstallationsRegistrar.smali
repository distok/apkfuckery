.class public Lcom/google/firebase/installations/FirebaseInstallationsRegistrar;
.super Ljava/lang/Object;
.source "FirebaseInstallationsRegistrar.java"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic lambda$getComponents$0(Lf/h/c/m/e;)Lf/h/c/v/g;
    .locals 4

    new-instance v0, Lf/h/c/v/f;

    const-class v1, Lf/h/c/c;

    invoke-interface {p0, v1}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/c;

    const-class v2, Lf/h/c/z/h;

    invoke-interface {p0, v2}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object v2

    const-class v3, Lf/h/c/s/d;

    invoke-interface {p0, v3}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object p0

    invoke-direct {v0, v1, v2, p0}, Lf/h/c/v/f;-><init>(Lf/h/c/c;Lf/h/c/u/a;Lf/h/c/u/a;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lf/h/c/m/d;

    const-class v1, Lf/h/c/v/g;

    invoke-static {v1}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v1

    const-class v2, Lf/h/c/c;

    new-instance v3, Lf/h/c/m/o;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/s/d;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/z/h;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v2, Lf/h/c/v/h;->a:Lf/h/c/v/h;

    invoke-virtual {v1, v2}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v1}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "fire-installations"

    const-string v2, "16.3.4"

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
