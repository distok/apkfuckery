.class public Lcom/google/firebase/analytics/connector/internal/AnalyticsConnectorRegistrar;
.super Ljava/lang/Object;
.source "com.google.android.gms:play-services-measurement-api@@18.0.0"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Lf/h/c/m/d;

    const-class v2, Lf/h/c/k/a/a;

    invoke-static {v2}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v2

    const-class v3, Lf/h/c/c;

    new-instance v4, Lf/h/c/m/o;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Landroid/content/Context;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/r/d;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v3, Lf/h/c/k/a/c/a;->a:Lf/h/c/m/f;

    invoke-virtual {v2, v3}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v2, v0}, Lf/h/c/m/d$b;->d(I)Lf/h/c/m/d$b;

    invoke-virtual {v2}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v0

    aput-object v0, v1, v6

    const-string v0, "fire-analytics"

    const-string v2, "18.0.0"

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
