.class public final Lcom/google/firebase/iid/Registrar;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-iid@@21.0.0"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/iid/Registrar$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic lambda$getComponents$0$Registrar(Lf/h/c/m/e;)Lcom/google/firebase/iid/FirebaseInstanceId;
    .locals 5

    new-instance v0, Lcom/google/firebase/iid/FirebaseInstanceId;

    const-class v1, Lf/h/c/c;

    invoke-interface {p0, v1}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/c;

    const-class v2, Lf/h/c/z/h;

    invoke-interface {p0, v2}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object v2

    const-class v3, Lf/h/c/s/d;

    invoke-interface {p0, v3}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object v3

    const-class v4, Lf/h/c/v/g;

    invoke-interface {p0, v4}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lf/h/c/v/g;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/firebase/iid/FirebaseInstanceId;-><init>(Lf/h/c/c;Lf/h/c/u/a;Lf/h/c/u/a;Lf/h/c/v/g;)V

    return-object v0
.end method

.method public static final synthetic lambda$getComponents$1$Registrar(Lf/h/c/m/e;)Lf/h/c/t/e0/a;
    .locals 2

    new-instance v0, Lcom/google/firebase/iid/Registrar$a;

    const-class v1, Lcom/google/firebase/iid/FirebaseInstanceId;

    invoke-interface {p0, v1}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/firebase/iid/FirebaseInstanceId;

    invoke-direct {v0, p0}, Lcom/google/firebase/iid/Registrar$a;-><init>(Lcom/google/firebase/iid/FirebaseInstanceId;)V

    return-object v0
.end method


# virtual methods
.method public final getComponents()Ljava/util/List;
    .locals 6
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const-class v0, Lcom/google/firebase/iid/FirebaseInstanceId;

    invoke-static {v0}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v1

    const-class v2, Lf/h/c/c;

    new-instance v3, Lf/h/c/m/o;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/z/h;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/s/d;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/v/g;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v2, Lf/h/c/t/r;->a:Lf/h/c/m/f;

    invoke-virtual {v1, v2}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v1, v4}, Lf/h/c/m/d$b;->d(I)Lf/h/c/m/d$b;

    invoke-virtual {v1}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v1

    const-class v2, Lf/h/c/t/e0/a;

    invoke-static {v2}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v2

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v0, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v0, Lf/h/c/t/s;->a:Lf/h/c/m/f;

    invoke-virtual {v2, v0}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v2}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v0

    const-string v2, "fire-iid"

    const-string v3, "21.0.0"

    invoke-static {v2, v3}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lf/h/c/m/d;

    aput-object v1, v3, v5

    aput-object v0, v3, v4

    const/4 v0, 0x2

    aput-object v2, v3, v0

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
