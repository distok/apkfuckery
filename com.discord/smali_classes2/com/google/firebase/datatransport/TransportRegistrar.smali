.class public Lcom/google/firebase/datatransport/TransportRegistrar;
.super Ljava/lang/Object;
.source "TransportRegistrar.java"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic lambda$getComponents$0(Lf/h/c/m/e;)Lf/h/a/b/g;
    .locals 1

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {p0}, Lf/h/a/b/j/n;->b(Landroid/content/Context;)V

    invoke-static {}, Lf/h/a/b/j/n;->a()Lf/h/a/b/j/n;

    move-result-object p0

    sget-object v0, Lf/h/a/b/i/a;->g:Lf/h/a/b/i/a;

    invoke-virtual {p0, v0}, Lf/h/a/b/j/n;->c(Lf/h/a/b/j/d;)Lf/h/a/b/g;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const-class v0, Lf/h/a/b/g;

    invoke-static {v0}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v0

    const-class v1, Landroid/content/Context;

    new-instance v2, Lf/h/c/m/o;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, v1, v3, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v0, v2}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v1, Lf/h/c/o/a;->a:Lf/h/c/o/a;

    invoke-virtual {v0, v1}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v0}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
