.class public final Lcom/google/firebase/dynamiclinks/internal/FirebaseDynamicLinkRegistrar;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-dynamic-links@@19.1.1"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getComponents()Ljava/util/List;
    .locals 5
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const-class v0, Lf/h/c/p/a;

    invoke-static {v0}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v0

    const-class v1, Lf/h/c/c;

    new-instance v2, Lf/h/c/m/o;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, v1, v3, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v0, v2}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v1, Lf/h/c/k/a/a;

    new-instance v2, Lf/h/c/m/o;

    invoke-direct {v2, v1, v4, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v0, v2}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v1, Lf/h/c/p/b/f;->a:Lf/h/c/m/f;

    invoke-virtual {v0, v1}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v0}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v0

    new-array v1, v3, [Lf/h/c/m/d;

    aput-object v0, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
