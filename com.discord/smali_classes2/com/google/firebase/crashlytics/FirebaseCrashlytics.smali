.class public Lcom/google/firebase/crashlytics/FirebaseCrashlytics;
.super Ljava/lang/Object;
.source "FirebaseCrashlytics.java"


# instance fields
.field public final a:Lf/h/c/n/d/k/k0;


# direct methods
.method public constructor <init>(Lf/h/c/n/d/k/k0;)V
    .locals 0
    .param p1    # Lf/h/c/n/d/k/k0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    return-void
.end method

.method public static getInstance()Lcom/google/firebase/crashlytics/FirebaseCrashlytics;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Lf/h/c/c;->b()Lf/h/c/c;

    move-result-object v0

    const-class v1, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;

    invoke-virtual {v0}, Lf/h/c/c;->a()V

    iget-object v0, v0, Lf/h/c/c;->d:Lf/h/c/m/k;

    invoke-virtual {v0, v1}, Lf/h/c/m/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;

    const-string v1, "FirebaseCrashlytics component is not present."

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public checkForUnsentReports()Lcom/google/android/gms/tasks/Task;
    .locals 4
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/tasks/Task<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v1, "checkForUnsentReports should only be called once per execution."

    invoke-virtual {v0, v1}, Lf/h/c/n/d/b;->b(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->t(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lf/h/c/n/d/k/x;->v:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    :goto_0
    return-object v0
.end method

.method public deleteUnsentReports()V
    .locals 3

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->w:Lcom/google/android/gms/tasks/TaskCompletionSource;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    iget-object v0, v0, Lf/h/c/n/d/k/x;->x:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-void
.end method

.method public didCrashOnPreviousExecution()Z
    .locals 1

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    iget-boolean v0, v0, Lf/h/c/n/d/k/k0;->g:Z

    return v0
.end method

.method public log(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lf/h/c/n/d/k/k0;->d:J

    sub-long/2addr v1, v3

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v3, v0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v4, Lf/h/c/n/d/k/n;

    invoke-direct {v4, v0, v1, v2, p1}, Lf/h/c/n/d/k/n;-><init>(Lf/h/c/n/d/k/x;JLjava/lang/String;)V

    invoke-virtual {v3, v4}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public recordException(Ljava/lang/Throwable;)V
    .locals 5
    .param p1    # Ljava/lang/Throwable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    if-nez p1, :cond_0

    sget-object p1, Lf/h/c/n/d/b;->a:Lf/h/c/n/d/b;

    const-string v0, "Crashlytics is ignoring a request to log a null exception."

    invoke-virtual {p1, v0}, Lf/h/c/n/d/b;->g(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iget-object v3, v0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v4, Lf/h/c/n/d/k/o;

    invoke-direct {v4, v0, v2, p1, v1}, Lf/h/c/n/d/k/o;-><init>(Lf/h/c/n/d/k/x;Ljava/util/Date;Ljava/lang/Throwable;Ljava/lang/Thread;)V

    new-instance p1, Lf/h/c/n/d/k/j;

    invoke-direct {p1, v3, v4}, Lf/h/c/n/d/k/j;-><init>(Lf/h/c/n/d/k/i;Ljava/lang/Runnable;)V

    invoke-virtual {v3, p1}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public sendUnsentReports()V
    .locals 3

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->w:Lcom/google/android/gms/tasks/TaskCompletionSource;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/tasks/TaskCompletionSource;->b(Ljava/lang/Object;)Z

    iget-object v0, v0, Lf/h/c/n/d/k/x;->x:Lcom/google/android/gms/tasks/TaskCompletionSource;

    iget-object v0, v0, Lcom/google/android/gms/tasks/TaskCompletionSource;->a:Lf/h/a/f/p/b0;

    return-void
.end method

.method public setCrashlyticsCollectionEnabled(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-virtual {v0, p1}, Lf/h/c/n/d/k/k0;->d(Ljava/lang/Boolean;)V

    return-void
.end method

.method public setCrashlyticsCollectionEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lf/h/c/n/d/k/k0;->d(Ljava/lang/Boolean;)V

    return-void
.end method

.method public setCustomKey(Ljava/lang/String;D)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/k/k0;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCustomKey(Ljava/lang/String;F)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/k/k0;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCustomKey(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/k/k0;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCustomKey(Ljava/lang/String;J)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/k/k0;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCustomKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/k/k0;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCustomKey(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lf/h/c/n/d/k/k0;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/google/firebase/crashlytics/FirebaseCrashlytics;->a:Lf/h/c/n/d/k/k0;

    iget-object v0, v0, Lf/h/c/n/d/k/k0;->h:Lf/h/c/n/d/k/x;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lf/h/c/n/d/k/g1;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, v1, Lf/h/c/n/d/k/g1;->a:Ljava/lang/String;

    iget-object p1, v0, Lf/h/c/n/d/k/x;->e:Lf/h/c/n/d/k/g1;

    iget-object v1, v0, Lf/h/c/n/d/k/x;->f:Lf/h/c/n/d/k/i;

    new-instance v2, Lf/h/c/n/d/k/p;

    invoke-direct {v2, v0, p1}, Lf/h/c/n/d/k/p;-><init>(Lf/h/c/n/d/k/x;Lf/h/c/n/d/k/g1;)V

    invoke-virtual {v1, v2}, Lf/h/c/n/d/k/i;->b(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method
