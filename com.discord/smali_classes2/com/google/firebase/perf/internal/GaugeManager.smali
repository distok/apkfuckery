.class public Lcom/google/firebase/perf/internal/GaugeManager;
.super Ljava/lang/Object;
.source "GaugeManager.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field private static final APPROX_NUMBER_OF_DATA_POINTS_PER_GAUGE_METRIC:J = 0x14L

.field private static final INVALID_GAUGE_COLLECTION_FREQUENCY:J = -0x1L

.field private static final TIME_TO_WAIT_BEFORE_FLUSHING_GAUGES_QUEUE_MS:J = 0x14L

.field private static final logger:Lf/h/c/y/h/a;

.field private static sharedInstance:Lcom/google/firebase/perf/internal/GaugeManager;


# instance fields
.field private applicationProcessState:Lf/h/c/y/m/d;

.field private final configResolver:Lf/h/c/y/d/a;

.field private final cpuGaugeCollector:Lf/h/c/y/e/c;

.field private gaugeManagerDataCollectionJob:Ljava/util/concurrent/ScheduledFuture;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final gaugeManagerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private gaugeMetadataManager:Lf/h/c/y/g/i;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final memoryGaugeCollector:Lf/h/c/y/e/f;

.field private sessionId:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final transportManager:Lf/h/c/y/k/l;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v0

    sput-object v0, Lcom/google/firebase/perf/internal/GaugeManager;->logger:Lf/h/c/y/h/a;

    new-instance v0, Lcom/google/firebase/perf/internal/GaugeManager;

    invoke-direct {v0}, Lcom/google/firebase/perf/internal/GaugeManager;-><init>()V

    sput-object v0, Lcom/google/firebase/perf/internal/GaugeManager;->sharedInstance:Lcom/google/firebase/perf/internal/GaugeManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    sget-object v2, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    invoke-static {}, Lf/h/c/y/d/a;->f()Lf/h/c/y/d/a;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v0, Lf/h/c/y/e/c;->i:Lf/h/c/y/e/c;

    if-nez v0, :cond_0

    new-instance v0, Lf/h/c/y/e/c;

    invoke-direct {v0}, Lf/h/c/y/e/c;-><init>()V

    sput-object v0, Lf/h/c/y/e/c;->i:Lf/h/c/y/e/c;

    :cond_0
    sget-object v5, Lf/h/c/y/e/c;->i:Lf/h/c/y/e/c;

    sget-object v6, Lf/h/c/y/e/f;->g:Lf/h/c/y/e/f;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/firebase/perf/internal/GaugeManager;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lf/h/c/y/k/l;Lf/h/c/y/d/a;Lf/h/c/y/g/i;Lf/h/c/y/e/c;Lf/h/c/y/e/f;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;Lf/h/c/y/k/l;Lf/h/c/y/d/a;Lf/h/c/y/g/i;Lf/h/c/y/e/c;Lf/h/c/y/e/f;)V
    .locals 1
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lf/h/c/y/m/d;->d:Lf/h/c/y/m/d;

    iput-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->applicationProcessState:Lf/h/c/y/m/d;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->sessionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeManagerDataCollectionJob:Ljava/util/concurrent/ScheduledFuture;

    iput-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeManagerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p2, p0, Lcom/google/firebase/perf/internal/GaugeManager;->transportManager:Lf/h/c/y/k/l;

    iput-object p3, p0, Lcom/google/firebase/perf/internal/GaugeManager;->configResolver:Lf/h/c/y/d/a;

    iput-object p4, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    iput-object p5, p0, Lcom/google/firebase/perf/internal/GaugeManager;->cpuGaugeCollector:Lf/h/c/y/e/c;

    iput-object p6, p0, Lcom/google/firebase/perf/internal/GaugeManager;->memoryGaugeCollector:Lf/h/c/y/e/f;

    return-void
.end method

.method private static collectGaugeMetricOnce(Lf/h/c/y/e/c;Lf/h/c/y/e/f;Lcom/google/firebase/perf/util/Timer;)V
    .locals 7

    monitor-enter p0

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lf/h/c/y/e/c;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lf/h/c/y/e/b;

    invoke-direct {v4, p0, p2}, Lf/h/c/y/e/b;-><init>(Lf/h/c/y/e/c;Lcom/google/firebase/perf/util/Timer;)V

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v1, v2, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception v3

    :try_start_1
    sget-object v4, Lf/h/c/y/e/c;->g:Lf/h/c/y/h/a;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to collect Cpu Metric: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/util/concurrent/RejectedExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    monitor-enter p1

    :try_start_2
    iget-object p0, p1, Lf/h/c/y/e/f;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lf/h/c/y/e/e;

    invoke-direct {v3, p1, p2}, Lf/h/c/y/e/e;-><init>(Lf/h/c/y/e/f;Lcom/google/firebase/perf/util/Timer;)V

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v3, v1, v2, p2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_2
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception p0

    goto :goto_2

    :catch_1
    move-exception p0

    :try_start_3
    sget-object p2, Lf/h/c/y/e/f;->f:Lf/h/c/y/h/a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to collect Memory Metric: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/util/concurrent/RejectedExecutionException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p2, p0, v0}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    monitor-exit p1

    return-void

    :goto_2
    monitor-exit p1

    throw p0

    :goto_3
    monitor-exit p0

    throw p1
.end method

.method private getCpuGaugeCollectionFrequencyMs(Lf/h/c/y/m/d;)J
    .locals 9

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const-wide/16 v0, -0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    if-eq p1, v4, :cond_5

    const/4 v5, 0x2

    if-eq p1, v5, :cond_0

    move-wide v5, v0

    goto/16 :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->configResolver:Lf/h/c/y/d/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v5, Lf/h/c/y/d/k;

    monitor-enter v5

    :try_start_0
    sget-object v6, Lf/h/c/y/d/k;->a:Lf/h/c/y/d/k;

    if-nez v6, :cond_1

    new-instance v6, Lf/h/c/y/d/k;

    invoke-direct {v6}, Lf/h/c/y/d/k;-><init>()V

    sput-object v6, Lf/h/c/y/d/k;->a:Lf/h/c/y/d/k;

    :cond_1
    sget-object v6, Lf/h/c/y/d/k;->a:Lf/h/c/y/d/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v5

    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->i(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object p1, p1, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "com.google.firebase.perf.SessionsCpuCaptureFrequencyBackgroundMs"

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-static {v7, p1, v6, v5}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :cond_4
    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v5

    throw p1

    :cond_5
    iget-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->configResolver:Lf/h/c/y/d/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v5, Lf/h/c/y/d/l;

    monitor-enter v5

    :try_start_1
    sget-object v6, Lf/h/c/y/d/l;->a:Lf/h/c/y/d/l;

    if-nez v6, :cond_6

    new-instance v6, Lf/h/c/y/d/l;

    invoke-direct {v6}, Lf/h/c/y/d/l;-><init>()V

    sput-object v6, Lf/h/c/y/d/l;->a:Lf/h/c/y/d/l;

    :cond_6
    sget-object v6, Lf/h/c/y/d/l;->a:Lf/h/c/y/d/l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v5

    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->i(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_7
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object p1, p1, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "com.google.firebase.perf.SessionsCpuCaptureFrequencyForegroundMs"

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-static {v7, p1, v6, v5}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_8
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_9
    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v5, 0x64

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    :goto_0
    sget-object p1, Lf/h/c/y/e/c;->g:Lf/h/c/y/h/a;

    cmp-long p1, v5, v2

    if-gtz p1, :cond_a

    goto :goto_1

    :cond_a
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_b

    return-wide v0

    :cond_b
    return-wide v5

    :catchall_1
    move-exception p1

    monitor-exit v5

    throw p1
.end method

.method private getGaugeMetadata()Lf/h/c/y/m/g;
    .locals 5

    invoke-static {}, Lf/h/c/y/m/g;->H()Lf/h/c/y/m/g$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    iget-object v1, v1, Lf/h/c/y/g/i;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/g;

    invoke-static {v2, v1}, Lf/h/c/y/m/g;->B(Lf/h/c/y/m/g;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    sget-object v2, Lf/h/c/y/l/f;->h:Lf/h/c/y/l/f;

    iget-object v1, v1, Lf/h/c/y/g/i;->c:Landroid/app/ActivityManager$MemoryInfo;

    iget-wide v3, v1, Landroid/app/ActivityManager$MemoryInfo;->totalMem:J

    invoke-virtual {v2, v3, v4}, Lf/h/c/y/l/f;->f(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Lf/h/c/y/l/g;->b(J)I

    move-result v1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v3, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v3, Lf/h/c/y/m/g;

    invoke-static {v3, v1}, Lf/h/c/y/m/g;->E(Lf/h/c/y/m/g;I)V

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    iget-object v1, v1, Lf/h/c/y/g/i;->a:Ljava/lang/Runtime;

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lf/h/c/y/l/f;->f(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Lf/h/c/y/l/g;->b(J)I

    move-result v1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/g;

    invoke-static {v2, v1}, Lf/h/c/y/m/g;->C(Lf/h/c/y/m/g;I)V

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    sget-object v2, Lf/h/c/y/l/f;->f:Lf/h/c/y/l/f;

    iget-object v1, v1, Lf/h/c/y/g/i;->b:Landroid/app/ActivityManager;

    invoke-virtual {v1}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v2, v3, v4}, Lf/h/c/y/l/f;->f(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Lf/h/c/y/l/g;->b(J)I

    move-result v1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/g;

    invoke-static {v2, v1}, Lf/h/c/y/m/g;->D(Lf/h/c/y/m/g;I)V

    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/g;

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/google/firebase/perf/internal/GaugeManager;
    .locals 2

    const-class v0, Lcom/google/firebase/perf/internal/GaugeManager;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/firebase/perf/internal/GaugeManager;->sharedInstance:Lcom/google/firebase/perf/internal/GaugeManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getMemoryGaugeCollectionFrequencyMs(Lf/h/c/y/m/d;)J
    .locals 9

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const-wide/16 v0, -0x1

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    if-eq p1, v4, :cond_5

    const/4 v5, 0x2

    if-eq p1, v5, :cond_0

    move-wide v5, v0

    goto/16 :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->configResolver:Lf/h/c/y/d/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v5, Lf/h/c/y/d/n;

    monitor-enter v5

    :try_start_0
    sget-object v6, Lf/h/c/y/d/n;->a:Lf/h/c/y/d/n;

    if-nez v6, :cond_1

    new-instance v6, Lf/h/c/y/d/n;

    invoke-direct {v6}, Lf/h/c/y/d/n;-><init>()V

    sput-object v6, Lf/h/c/y/d/n;->a:Lf/h/c/y/d/n;

    :cond_1
    sget-object v6, Lf/h/c/y/d/n;->a:Lf/h/c/y/d/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v5

    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->i(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object p1, p1, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "com.google.firebase.perf.SessionsMemoryCaptureFrequencyBackgroundMs"

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-static {v7, p1, v6, v5}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :cond_4
    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v5

    throw p1

    :cond_5
    iget-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->configResolver:Lf/h/c/y/d/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-class v5, Lf/h/c/y/d/o;

    monitor-enter v5

    :try_start_1
    sget-object v6, Lf/h/c/y/d/o;->a:Lf/h/c/y/d/o;

    if-nez v6, :cond_6

    new-instance v6, Lf/h/c/y/d/o;

    invoke-direct {v6}, Lf/h/c/y/d/o;-><init>()V

    sput-object v6, Lf/h/c/y/d/o;->a:Lf/h/c/y/d/o;

    :cond_6
    sget-object v6, Lf/h/c/y/d/o;->a:Lf/h/c/y/d/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v5

    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->i(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_7
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->l(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object p1, p1, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "com.google.firebase.perf.SessionsMemoryCaptureFrequencyForegroundMs"

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-static {v7, p1, v6, v5}, Lf/e/c/a/a;->c0(Ljava/lang/Long;Lf/h/c/y/d/v;Ljava/lang/String;Lf/h/c/y/l/e;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_8
    invoke-virtual {p1, v6}, Lf/h/c/y/d/a;->d(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v5

    invoke-virtual {v5}, Lf/h/c/y/l/e;->b()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {p1, v7, v8}, Lf/h/c/y/d/a;->o(J)Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-virtual {v5}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto :goto_0

    :cond_9
    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v5, 0x64

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    :goto_0
    sget-object p1, Lf/h/c/y/e/f;->f:Lf/h/c/y/h/a;

    cmp-long p1, v5, v2

    if-gtz p1, :cond_a

    goto :goto_1

    :cond_a
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_b

    return-wide v0

    :cond_b
    return-wide v5

    :catchall_1
    move-exception p1

    monitor-exit v5

    throw p1
.end method

.method public static synthetic lambda$startCollectingGauges$0(Lcom/google/firebase/perf/internal/GaugeManager;Ljava/lang/String;Lf/h/c/y/m/d;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/firebase/perf/internal/GaugeManager;->syncFlush(Ljava/lang/String;Lf/h/c/y/m/d;)V

    return-void
.end method

.method public static synthetic lambda$stopCollectingGauges$1(Lcom/google/firebase/perf/internal/GaugeManager;Ljava/lang/String;Lf/h/c/y/m/d;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/firebase/perf/internal/GaugeManager;->syncFlush(Ljava/lang/String;Lf/h/c/y/m/d;)V

    return-void
.end method

.method private startCollectingCpuMetrics(JLcom/google/firebase/perf/util/Timer;)Z
    .locals 10

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    cmp-long v3, p1, v0

    if-nez v3, :cond_0

    sget-object p1, Lcom/google/firebase/perf/internal/GaugeManager;->logger:Lf/h/c/y/h/a;

    new-array p2, v2, [Ljava/lang/Object;

    const-string p3, "Invalid Cpu Metrics collection frequency. Did not collect Cpu Metrics."

    invoke-virtual {p1, p3, p2}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_0
    iget-object v3, p0, Lcom/google/firebase/perf/internal/GaugeManager;->cpuGaugeCollector:Lf/h/c/y/e/c;

    iget-wide v4, v3, Lf/h/c/y/e/c;->d:J

    const/4 v6, 0x1

    cmp-long v7, v4, v0

    if-eqz v7, :cond_5

    const-wide/16 v7, 0x0

    cmp-long v9, v4, v7

    if-nez v9, :cond_1

    goto :goto_1

    :cond_1
    cmp-long v4, p1, v7

    if-gtz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    iget-object v4, v3, Lf/h/c/y/e/c;->a:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v4, :cond_4

    iget-wide v7, v3, Lf/h/c/y/e/c;->c:J

    cmp-long v5, v7, p1

    if-eqz v5, :cond_5

    invoke-interface {v4, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    const/4 v2, 0x0

    iput-object v2, v3, Lf/h/c/y/e/c;->a:Ljava/util/concurrent/ScheduledFuture;

    iput-wide v0, v3, Lf/h/c/y/e/c;->c:J

    invoke-virtual {v3, p1, p2, p3}, Lf/h/c/y/e/c;->a(JLcom/google/firebase/perf/util/Timer;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v3, p1, p2, p3}, Lf/h/c/y/e/c;->a(JLcom/google/firebase/perf/util/Timer;)V

    :cond_5
    :goto_1
    return v6
.end method

.method private startCollectingGauges(Lf/h/c/y/m/d;Lcom/google/firebase/perf/util/Timer;)J
    .locals 7

    invoke-direct {p0, p1}, Lcom/google/firebase/perf/internal/GaugeManager;->getCpuGaugeCollectionFrequencyMs(Lf/h/c/y/m/d;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p2}, Lcom/google/firebase/perf/internal/GaugeManager;->startCollectingCpuMetrics(JLcom/google/firebase/perf/util/Timer;)Z

    move-result v2

    const-wide/16 v3, -0x1

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move-wide v0, v3

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/firebase/perf/internal/GaugeManager;->getMemoryGaugeCollectionFrequencyMs(Lf/h/c/y/m/d;)J

    move-result-wide v5

    invoke-direct {p0, v5, v6, p2}, Lcom/google/firebase/perf/internal/GaugeManager;->startCollectingMemoryMetrics(JLcom/google/firebase/perf/util/Timer;)Z

    move-result p1

    if-eqz p1, :cond_2

    cmp-long p1, v0, v3

    if-nez p1, :cond_1

    move-wide v0, v5

    goto :goto_1

    :cond_1
    invoke-static {v0, v1, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    move-wide v0, p1

    :cond_2
    :goto_1
    return-wide v0
.end method

.method private startCollectingMemoryMetrics(JLcom/google/firebase/perf/util/Timer;)Z
    .locals 9

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    cmp-long v3, p1, v0

    if-nez v3, :cond_0

    sget-object p1, Lcom/google/firebase/perf/internal/GaugeManager;->logger:Lf/h/c/y/h/a;

    new-array p2, v2, [Ljava/lang/Object;

    const-string p3, "Invalid Memory Metrics collection frequency. Did not collect Memory Metrics."

    invoke-virtual {p1, p3, p2}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    :cond_0
    iget-object v3, p0, Lcom/google/firebase/perf/internal/GaugeManager;->memoryGaugeCollector:Lf/h/c/y/e/f;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    cmp-long v7, p1, v4

    if-gtz v7, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    iget-object v4, v3, Lf/h/c/y/e/f;->d:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v4, :cond_3

    iget-wide v7, v3, Lf/h/c/y/e/f;->e:J

    cmp-long v5, v7, p1

    if-eqz v5, :cond_4

    invoke-interface {v4, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    const/4 v2, 0x0

    iput-object v2, v3, Lf/h/c/y/e/f;->d:Ljava/util/concurrent/ScheduledFuture;

    iput-wide v0, v3, Lf/h/c/y/e/f;->e:J

    invoke-virtual {v3, p1, p2, p3}, Lf/h/c/y/e/f;->a(JLcom/google/firebase/perf/util/Timer;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v3, p1, p2, p3}, Lf/h/c/y/e/f;->a(JLcom/google/firebase/perf/util/Timer;)V

    :cond_4
    :goto_1
    return v6
.end method

.method private syncFlush(Ljava/lang/String;Lf/h/c/y/m/d;)V
    .locals 3

    invoke-static {}, Lf/h/c/y/m/h;->L()Lf/h/c/y/m/h$b;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->cpuGaugeCollector:Lf/h/c/y/e/c;

    iget-object v1, v1, Lf/h/c/y/e/c;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->cpuGaugeCollector:Lf/h/c/y/e/c;

    iget-object v1, v1, Lf/h/c/y/e/c;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/y/m/e;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/h;

    invoke-static {v2, v1}, Lf/h/c/y/m/h;->E(Lf/h/c/y/m/h;Lf/h/c/y/m/e;)V

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->memoryGaugeCollector:Lf/h/c/y/e/f;

    iget-object v1, v1, Lf/h/c/y/e/f;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->memoryGaugeCollector:Lf/h/c/y/e/f;

    iget-object v1, v1, Lf/h/c/y/e/f;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/y/m/b;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/h;

    invoke-static {v2, v1}, Lf/h/c/y/m/h;->C(Lf/h/c/y/m/h;Lf/h/c/y/m/b;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/h;

    invoke-static {v1, p1}, Lf/h/c/y/m/h;->B(Lf/h/c/y/m/h;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->transportManager:Lf/h/c/y/k/l;

    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/h;

    iget-object v1, p1, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lf/h/c/y/k/k;

    invoke-direct {v2, p1, v0, p2}, Lf/h/c/y/k/k;-><init>(Lf/h/c/y/k/l;Lf/h/c/y/m/h;Lf/h/c/y/m/d;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public collectGaugeMetricOnce(Lcom/google/firebase/perf/util/Timer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->cpuGaugeCollector:Lf/h/c/y/e/c;

    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->memoryGaugeCollector:Lf/h/c/y/e/f;

    invoke-static {v0, v1, p1}, Lcom/google/firebase/perf/internal/GaugeManager;->collectGaugeMetricOnce(Lf/h/c/y/e/c;Lf/h/c/y/e/f;Lcom/google/firebase/perf/util/Timer;)V

    return-void
.end method

.method public logGaugeMetadata(Ljava/lang/String;Lf/h/c/y/m/d;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    if-eqz v0, :cond_0

    invoke-static {}, Lf/h/c/y/m/h;->L()Lf/h/c/y/m/h$b;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/h;

    invoke-static {v1, p1}, Lf/h/c/y/m/h;->B(Lf/h/c/y/m/h;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/firebase/perf/internal/GaugeManager;->getGaugeMetadata()Lf/h/c/y/m/g;

    move-result-object p1

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v1, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v1, Lf/h/c/y/m/h;

    invoke-static {v1, p1}, Lf/h/c/y/m/h;->D(Lf/h/c/y/m/h;Lf/h/c/y/m/g;)V

    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object p1

    check-cast p1, Lf/h/c/y/m/h;

    iget-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->transportManager:Lf/h/c/y/k/l;

    iget-object v1, v0, Lf/h/c/y/k/l;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lf/h/c/y/k/k;

    invoke-direct {v2, v0, p1, p2}, Lf/h/c/y/k/k;-><init>(Lf/h/c/y/k/l;Lf/h/c/y/m/h;Lf/h/c/y/m/d;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setApplicationContext(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lf/h/c/y/g/i;

    invoke-direct {v0, p1}, Lf/h/c/y/g/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeMetadataManager:Lf/h/c/y/g/i;

    return-void
.end method

.method public startCollectingGauges(Lcom/google/firebase/perf/internal/PerfSession;Lf/h/c/y/m/d;)V
    .locals 12

    iget-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->sessionId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/firebase/perf/internal/GaugeManager;->stopCollectingGauges()V

    :cond_0
    iget-object v0, p1, Lcom/google/firebase/perf/internal/PerfSession;->f:Lcom/google/firebase/perf/util/Timer;

    invoke-direct {p0, p2, v0}, Lcom/google/firebase/perf/internal/GaugeManager;->startCollectingGauges(Lf/h/c/y/m/d;Lcom/google/firebase/perf/util/Timer;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    cmp-long v5, v0, v2

    if-nez v5, :cond_1

    sget-object p1, Lcom/google/firebase/perf/internal/GaugeManager;->logger:Lf/h/c/y/h/a;

    new-array p2, v4, [Ljava/lang/Object;

    const-string v0, "Invalid gauge collection frequency. Unable to start collecting Gauges."

    invoke-virtual {p1, v0, p2}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object p1, p1, Lcom/google/firebase/perf/internal/PerfSession;->d:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->sessionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/firebase/perf/internal/GaugeManager;->applicationProcessState:Lf/h/c/y/m/d;

    :try_start_0
    iget-object v5, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeManagerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lf/h/c/y/g/g;

    invoke-direct {v6, p0, p1, p2}, Lf/h/c/y/g/g;-><init>(Lcom/google/firebase/perf/internal/GaugeManager;Ljava/lang/String;Lf/h/c/y/m/d;)V

    const-wide/16 p1, 0x14

    mul-long v9, v0, p1

    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v7, v9

    invoke-interface/range {v5 .. v11}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    iput-object p1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeManagerDataCollectionJob:Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object p2, Lcom/google/firebase/perf/internal/GaugeManager;->logger:Lf/h/c/y/h/a;

    const-string v0, "Unable to start collecting Gauges: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/concurrent/RejectedExecutionException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v4, [Ljava/lang/Object;

    invoke-virtual {p2, p1, v0}, Lf/h/c/y/h/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public stopCollectingGauges()V
    .locals 8

    iget-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->sessionId:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/firebase/perf/internal/GaugeManager;->applicationProcessState:Lf/h/c/y/m/d;

    iget-object v2, p0, Lcom/google/firebase/perf/internal/GaugeManager;->cpuGaugeCollector:Lf/h/c/y/e/c;

    iget-object v3, v2, Lf/h/c/y/e/c;->a:Ljava/util/concurrent/ScheduledFuture;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {v3, v7}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    iput-object v6, v2, Lf/h/c/y/e/c;->a:Ljava/util/concurrent/ScheduledFuture;

    iput-wide v4, v2, Lf/h/c/y/e/c;->c:J

    :goto_0
    iget-object v2, p0, Lcom/google/firebase/perf/internal/GaugeManager;->memoryGaugeCollector:Lf/h/c/y/e/f;

    iget-object v3, v2, Lf/h/c/y/e/f;->d:Ljava/util/concurrent/ScheduledFuture;

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    invoke-interface {v3, v7}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    iput-object v6, v2, Lf/h/c/y/e/f;->d:Ljava/util/concurrent/ScheduledFuture;

    iput-wide v4, v2, Lf/h/c/y/e/f;->e:J

    :goto_1
    iget-object v2, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeManagerDataCollectionJob:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v2, :cond_3

    invoke-interface {v2, v7}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    :cond_3
    iget-object v2, p0, Lcom/google/firebase/perf/internal/GaugeManager;->gaugeManagerExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lf/h/c/y/g/h;

    invoke-direct {v3, p0, v0, v1}, Lf/h/c/y/g/h;-><init>(Lcom/google/firebase/perf/internal/GaugeManager;Ljava/lang/String;Lf/h/c/y/m/d;)V

    const-wide/16 v0, 0x14

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    iput-object v6, p0, Lcom/google/firebase/perf/internal/GaugeManager;->sessionId:Ljava/lang/String;

    sget-object v0, Lf/h/c/y/m/d;->d:Lf/h/c/y/m/d;

    iput-object v0, p0, Lcom/google/firebase/perf/internal/GaugeManager;->applicationProcessState:Lf/h/c/y/m/d;

    return-void
.end method
