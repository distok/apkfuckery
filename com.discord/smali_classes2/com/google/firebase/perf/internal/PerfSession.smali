.class public Lcom/google/firebase/perf/internal/PerfSession;
.super Ljava/lang/Object;
.source "PerfSession.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/firebase/perf/internal/PerfSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/google/firebase/perf/util/Timer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/firebase/perf/internal/PerfSession$a;

    invoke-direct {v0}, Lcom/google/firebase/perf/internal/PerfSession$a;-><init>()V

    sput-object v0, Lcom/google/firebase/perf/internal/PerfSession;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;Lcom/google/firebase/perf/internal/PerfSession$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/firebase/perf/internal/PerfSession;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x1

    :cond_0
    iput-boolean p2, p0, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    const-class p2, Lcom/google/firebase/perf/util/Timer;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/perf/util/Timer;

    iput-object p1, p0, Lcom/google/firebase/perf/internal/PerfSession;->f:Lcom/google/firebase/perf/util/Timer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lf/h/c/y/l/a;)V
    .locals 0
    .annotation build Landroidx/annotation/VisibleForTesting;
        otherwise = 0x3
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    iput-object p1, p0, Lcom/google/firebase/perf/internal/PerfSession;->d:Ljava/lang/String;

    new-instance p1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {p1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    iput-object p1, p0, Lcom/google/firebase/perf/internal/PerfSession;->f:Lcom/google/firebase/perf/util/Timer;

    return-void
.end method

.method public static b(Ljava/util/List;)[Lf/h/c/y/m/q;
    .locals 8
    .param p0    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/firebase/perf/internal/PerfSession;",
            ">;)[",
            "Lf/h/c/y/m/q;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lf/h/c/y/m/q;

    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/firebase/perf/internal/PerfSession;

    invoke-virtual {v2}, Lcom/google/firebase/perf/internal/PerfSession;->a()Lf/h/c/y/m/q;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_2

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/firebase/perf/internal/PerfSession;

    invoke-virtual {v6}, Lcom/google/firebase/perf/internal/PerfSession;->a()Lf/h/c/y/m/q;

    move-result-object v6

    if-nez v5, :cond_1

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/firebase/perf/internal/PerfSession;

    iget-boolean v7, v7, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    if-eqz v7, :cond_1

    aput-object v6, v0, v1

    aput-object v2, v0, v4

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    aput-object v6, v0, v4

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    if-nez v5, :cond_3

    aput-object v2, v0, v1

    :cond_3
    return-object v0
.end method

.method public static c()Lcom/google/firebase/perf/internal/PerfSession;
    .locals 14

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/firebase/perf/internal/PerfSession;

    new-instance v2, Lf/h/c/y/l/a;

    invoke-direct {v2}, Lf/h/c/y/l/a;-><init>()V

    invoke-direct {v1, v0, v2}, Lcom/google/firebase/perf/internal/PerfSession;-><init>(Ljava/lang/String;Lf/h/c/y/l/a;)V

    invoke-static {}, Lf/h/c/y/d/a;->f()Lf/h/c/y/d/a;

    move-result-object v2

    invoke-virtual {v2}, Lf/h/c/y/d/a;->p()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    if-eqz v3, :cond_5

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    const-class v3, Lf/h/c/y/d/p;

    monitor-enter v3

    :try_start_0
    sget-object v9, Lf/h/c/y/d/p;->a:Lf/h/c/y/d/p;

    if-nez v9, :cond_0

    new-instance v9, Lf/h/c/y/d/p;

    invoke-direct {v9}, Lf/h/c/y/d/p;-><init>()V

    sput-object v9, Lf/h/c/y/d/p;->a:Lf/h/c/y/d/p;

    :cond_0
    sget-object v9, Lf/h/c/y/d/p;->a:Lf/h/c/y/d/p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    iget-object v3, v2, Lf/h/c/y/d/a;->a:Lf/h/c/y/l/d;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v10, "sessions_sampling_percentage"

    sget-object v11, Lf/h/c/y/l/e;->b:Lf/h/c/y/l/e;

    invoke-virtual {v3, v10}, Lf/h/c/y/l/d;->a(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v3, v3, Lf/h/c/y/l/d;->a:Landroid/os/Bundle;

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-static {v3}, Lf/h/c/y/l/e;->c(Ljava/lang/Object;)Lf/h/c/y/l/e;

    move-result-object v11
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    sget-object v12, Lf/h/c/y/l/d;->b:Lf/h/c/y/h/a;

    new-array v13, v6, [Ljava/lang/Object;

    aput-object v10, v13, v4

    invoke-virtual {v3}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v13, v5

    const-string v3, "Metadata key %s contains type other than float: %s"

    invoke-static {v3, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v10, v4, [Ljava/lang/Object;

    invoke-virtual {v12, v3, v10}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {v11}, Lf/h/c/y/l/e;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v11}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/high16 v10, 0x42c80000    # 100.0f

    div-float/2addr v3, v10

    invoke-virtual {v2, v3}, Lf/h/c/y/d/a;->q(F)Z

    move-result v10

    if-eqz v10, :cond_2

    goto :goto_1

    :cond_2
    iget-object v3, v2, Lf/h/c/y/d/a;->b:Lcom/google/firebase/perf/internal/RemoteConfigManager;

    const-string v10, "fpr_vc_session_sampling_rate"

    invoke-virtual {v3, v10}, Lcom/google/firebase/perf/internal/RemoteConfigManager;->getFloat(Ljava/lang/String;)Lf/h/c/y/l/e;

    move-result-object v3

    invoke-virtual {v3}, Lf/h/c/y/l/e;->b()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    invoke-virtual {v2, v10}, Lf/h/c/y/d/a;->q(F)Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v2, v2, Lf/h/c/y/d/a;->c:Lf/h/c/y/d/v;

    const-string v9, "com.google.firebase.perf.SessionSamplingRate"

    invoke-virtual {v3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    invoke-virtual {v2, v9, v10}, Lf/h/c/y/d/v;->c(Ljava/lang/String;F)Z

    invoke-virtual {v3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto :goto_1

    :cond_3
    invoke-virtual {v2, v9}, Lf/h/c/y/d/a;->c(Lf/h/c/y/d/u;)Lf/h/c/y/l/e;

    move-result-object v3

    invoke-virtual {v3}, Lf/h/c/y/l/e;->b()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {v2, v9}, Lf/h/c/y/d/a;->q(F)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v3}, Lf/h/c/y/l/e;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto :goto_1

    :cond_4
    const v2, 0x3c23d70a    # 0.01f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    :goto_1
    float-to-double v2, v3

    cmpg-double v9, v7, v2

    if-gez v9, :cond_5

    const/4 v2, 0x1

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    const/4 v2, 0x0

    :goto_2
    iput-boolean v2, v1, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    invoke-static {}, Lf/h/c/y/h/a;->c()Lf/h/c/y/h/a;

    move-result-object v2

    const-string v3, "Creating a new %s Session: %s"

    new-array v6, v6, [Ljava/lang/Object;

    iget-boolean v7, v1, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    if-eqz v7, :cond_6

    const-string v7, "Verbose"

    goto :goto_3

    :cond_6
    const-string v7, "Non Verbose"

    :goto_3
    aput-object v7, v6, v4

    aput-object v0, v6, v5

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Lf/h/c/y/h/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v1
.end method


# virtual methods
.method public a()Lf/h/c/y/m/q;
    .locals 3

    invoke-static {}, Lf/h/c/y/m/q;->F()Lf/h/c/y/m/q$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/firebase/perf/internal/PerfSession;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/q;

    invoke-static {v2, v1}, Lf/h/c/y/m/q;->B(Lf/h/c/y/m/q;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    if-eqz v1, :cond_0

    sget-object v1, Lf/h/c/y/m/s;->e:Lf/h/c/y/m/s;

    invoke-virtual {v0}, Lf/h/e/r$a;->r()V

    iget-object v2, v0, Lf/h/e/r$a;->e:Lf/h/e/r;

    check-cast v2, Lf/h/c/y/m/q;

    invoke-static {v2, v1}, Lf/h/c/y/m/q;->C(Lf/h/c/y/m/q;Lf/h/c/y/m/s;)V

    :cond_0
    invoke-virtual {v0}, Lf/h/e/r$a;->p()Lf/h/e/r;

    move-result-object v0

    check-cast v0, Lf/h/c/y/m/q;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p2, p0, Lcom/google/firebase/perf/internal/PerfSession;->d:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/google/firebase/perf/internal/PerfSession;->e:Z

    int-to-byte p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByte(B)V

    iget-object p2, p0, Lcom/google/firebase/perf/internal/PerfSession;->f:Lcom/google/firebase/perf/util/Timer;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
