.class public Lcom/google/firebase/perf/FirebasePerfRegistrar;
.super Ljava/lang/Object;
.source "FirebasePerfRegistrar.java"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic lambda$getComponents$0(Lf/h/c/m/e;)Lf/h/c/y/c;
    .locals 5

    new-instance v0, Lf/h/c/y/c;

    const-class v1, Lf/h/c/c;

    invoke-interface {p0, v1}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/c/c;

    const-class v2, Lf/h/c/a0/i;

    invoke-interface {p0, v2}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object v2

    const-class v3, Lf/h/c/v/g;

    invoke-interface {p0, v3}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf/h/c/v/g;

    const-class v4, Lf/h/a/b/g;

    invoke-interface {p0, v4}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object p0

    invoke-direct {v0, v1, v2, v3, p0}, Lf/h/c/y/c;-><init>(Lf/h/c/c;Lf/h/c/u/a;Lf/h/c/v/g;Lf/h/c/u/a;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 7
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Lf/h/c/m/d;

    const-class v2, Lf/h/c/y/c;

    invoke-static {v2}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v2

    const-class v3, Lf/h/c/c;

    new-instance v4, Lf/h/c/m/o;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/a0/i;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/c/v/g;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v6}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v3, Lf/h/a/b/g;

    new-instance v4, Lf/h/c/m/o;

    invoke-direct {v4, v3, v5, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v2, v4}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v3, Lf/h/c/y/b;->a:Lf/h/c/y/b;

    invoke-virtual {v2, v3}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v2, v0}, Lf/h/c/m/d$b;->d(I)Lf/h/c/m/d$b;

    invoke-virtual {v2}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v0

    aput-object v0, v1, v6

    const-string v0, "fire-perf"

    const-string v2, "19.0.10"

    invoke-static {v0, v2}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
