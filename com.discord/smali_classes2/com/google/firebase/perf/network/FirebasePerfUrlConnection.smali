.class public Lcom/google/firebase/perf/network/FirebasePerfUrlConnection;
.super Ljava/lang/Object;
.source "FirebasePerfUrlConnection.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContent(Ljava/net/URL;)Ljava/lang/Object;
    .locals 6
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->c()V

    iget-wide v2, v1, Lcom/google/firebase/perf/util/Timer;->d:J

    new-instance v4, Lf/h/c/y/f/a;

    invoke-direct {v4, v0}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    :try_start_0
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    instance-of v5, v0, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v5, :cond_0

    new-instance v5, Lf/h/c/y/j/d;

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v5, v0, v1, v4}, Lf/h/c/y/j/d;-><init>(Ljavax/net/ssl/HttpsURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    invoke-virtual {v5}, Lf/h/c/y/j/d;->getContent()Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    instance-of v5, v0, Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    new-instance v5, Lf/h/c/y/j/c;

    check-cast v0, Ljava/net/HttpURLConnection;

    invoke-direct {v5, v0, v1, v4}, Lf/h/c/y/j/c;-><init>(Ljava/net/HttpURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    invoke-virtual {v5}, Lf/h/c/y/j/c;->getContent()Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContent()Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    invoke-virtual {v4, v2, v3}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v4, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    invoke-virtual {p0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    invoke-static {v4}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method

.method public static getContent(Ljava/net/URL;[Ljava/lang/Class;)Ljava/lang/Object;
    .locals 6
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->c()V

    iget-wide v2, v1, Lcom/google/firebase/perf/util/Timer;->d:J

    new-instance v4, Lf/h/c/y/f/a;

    invoke-direct {v4, v0}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    :try_start_0
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    instance-of v5, v0, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v5, :cond_0

    new-instance v5, Lf/h/c/y/j/d;

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v5, v0, v1, v4}, Lf/h/c/y/j/d;-><init>(Ljavax/net/ssl/HttpsURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    iget-object v0, v5, Lf/h/c/y/j/d;->a:Lf/h/c/y/j/e;

    invoke-virtual {v0, p1}, Lf/h/c/y/j/e;->c([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    instance-of v5, v0, Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    new-instance v5, Lf/h/c/y/j/c;

    check-cast v0, Ljava/net/HttpURLConnection;

    invoke-direct {v5, v0, v1, v4}, Lf/h/c/y/j/c;-><init>(Ljava/net/HttpURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    iget-object v0, v5, Lf/h/c/y/j/c;->a:Lf/h/c/y/j/e;

    invoke-virtual {v0, p1}, Lf/h/c/y/j/e;->c([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p1}, Ljava/net/URLConnection;->getContent([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception p1

    invoke-virtual {v4, v2, v3}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    invoke-virtual {p0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    invoke-static {v4}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw p1
.end method

.method public static instrument(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    instance-of v0, p0, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v0, :cond_0

    new-instance v0, Lf/h/c/y/j/d;

    check-cast p0, Ljavax/net/ssl/HttpsURLConnection;

    new-instance v1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    sget-object v2, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v3, Lf/h/c/y/f/a;

    invoke-direct {v3, v2}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    invoke-direct {v0, p0, v1, v3}, Lf/h/c/y/j/d;-><init>(Ljavax/net/ssl/HttpsURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    return-object v0

    :cond_0
    instance-of v0, p0, Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_1

    new-instance v0, Lf/h/c/y/j/c;

    check-cast p0, Ljava/net/HttpURLConnection;

    new-instance v1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    sget-object v2, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v3, Lf/h/c/y/f/a;

    invoke-direct {v3, v2}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    invoke-direct {v0, p0, v1, v3}, Lf/h/c/y/j/c;-><init>(Ljava/net/HttpURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    return-object v0

    :cond_1
    return-object p0
.end method

.method public static openStream(Ljava/net/URL;)Ljava/io/InputStream;
    .locals 6
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v1, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v1}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->c()V

    iget-wide v2, v1, Lcom/google/firebase/perf/util/Timer;->d:J

    new-instance v4, Lf/h/c/y/f/a;

    invoke-direct {v4, v0}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    :try_start_0
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    instance-of v5, v0, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v5, :cond_0

    new-instance v5, Lf/h/c/y/j/d;

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v5, v0, v1, v4}, Lf/h/c/y/j/d;-><init>(Ljavax/net/ssl/HttpsURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    invoke-virtual {v5}, Lf/h/c/y/j/d;->getInputStream()Ljava/io/InputStream;

    move-result-object p0

    goto :goto_0

    :cond_0
    instance-of v5, v0, Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    new-instance v5, Lf/h/c/y/j/c;

    check-cast v0, Ljava/net/HttpURLConnection;

    invoke-direct {v5, v0, v1, v4}, Lf/h/c/y/j/c;-><init>(Ljava/net/HttpURLConnection;Lcom/google/firebase/perf/util/Timer;Lf/h/c/y/f/a;)V

    invoke-virtual {v5}, Lf/h/c/y/j/c;->getInputStream()Ljava/io/InputStream;

    move-result-object p0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    invoke-virtual {v4, v2, v3}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    invoke-virtual {v1}, Lcom/google/firebase/perf/util/Timer;->a()J

    move-result-wide v1

    invoke-virtual {v4, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    invoke-virtual {p0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    invoke-static {v4}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method
