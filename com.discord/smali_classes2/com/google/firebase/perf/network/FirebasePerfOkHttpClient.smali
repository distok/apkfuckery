.class public Lcom/google/firebase/perf/network/FirebasePerfOkHttpClient;
.super Ljava/lang/Object;
.source "FirebasePerfOkHttpClient.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lokhttp3/Response;Lf/h/c/y/f/a;JJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lokhttp3/Response;->d:Lb0/a0;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, v0, Lb0/a0;->b:Lb0/x;

    invoke-virtual {v1}, Lb0/x;->k()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    iget-object v1, v0, Lb0/a0;->c:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lf/h/c/y/f/a;->c(Ljava/lang/String;)Lf/h/c/y/f/a;

    iget-object v0, v0, Lb0/a0;->e:Lokhttp3/RequestBody;

    const-wide/16 v1, -0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentLength()J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    invoke-virtual {p1, v3, v4}, Lf/h/c/y/f/a;->e(J)Lf/h/c/y/f/a;

    :cond_1
    iget-object v0, p0, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->a()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-eqz v5, :cond_2

    invoke-virtual {p1, v3, v4}, Lf/h/c/y/f/a;->h(J)Lf/h/c/y/f/a;

    :cond_2
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->b()Lokhttp3/MediaType;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lokhttp3/MediaType;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lf/h/c/y/f/a;->g(Ljava/lang/String;)Lf/h/c/y/f/a;

    :cond_3
    iget p0, p0, Lokhttp3/Response;->g:I

    invoke-virtual {p1, p0}, Lf/h/c/y/f/a;->d(I)Lf/h/c/y/f/a;

    invoke-virtual {p1, p2, p3}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    invoke-virtual {p1, p4, p5}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    invoke-virtual {p1}, Lf/h/c/y/f/a;->b()Lf/h/c/y/m/n;

    return-void
.end method

.method public static enqueue(Lb0/e;Lb0/f;)V
    .locals 7
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    new-instance v3, Lcom/google/firebase/perf/util/Timer;

    invoke-direct {v3}, Lcom/google/firebase/perf/util/Timer;-><init>()V

    iget-wide v4, v3, Lcom/google/firebase/perf/util/Timer;->d:J

    new-instance v6, Lf/h/c/y/j/g;

    sget-object v2, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    move-object v0, v6

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lf/h/c/y/j/g;-><init>(Lb0/f;Lf/h/c/y/k/l;Lcom/google/firebase/perf/util/Timer;J)V

    invoke-interface {p0, v6}, Lb0/e;->t(Lb0/f;)V

    return-void
.end method

.method public static execute(Lb0/e;)Lokhttp3/Response;
    .locals 12
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lf/h/c/y/k/l;->u:Lf/h/c/y/k/l;

    new-instance v7, Lf/h/c/y/f/a;

    invoke-direct {v7, v0}, Lf/h/c/y/f/a;-><init>(Lf/h/c/y/k/l;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v8

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    :try_start_0
    invoke-interface {p0}, Lb0/e;->execute()Lokhttp3/Response;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v5

    move-object v1, v0

    move-object v2, v7

    move-wide v3, v8

    invoke-static/range {v1 .. v6}, Lcom/google/firebase/perf/network/FirebasePerfOkHttpClient;->a(Lokhttp3/Response;Lf/h/c/y/f/a;JJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-interface {p0}, Lb0/e;->c()Lb0/a0;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-object v1, p0, Lb0/a0;->b:Lb0/x;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lb0/x;->k()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lf/h/c/y/f/a;->k(Ljava/lang/String;)Lf/h/c/y/f/a;

    :cond_0
    iget-object p0, p0, Lb0/a0;->c:Ljava/lang/String;

    if-eqz p0, :cond_1

    invoke-virtual {v7, p0}, Lf/h/c/y/f/a;->c(Ljava/lang/String;)Lf/h/c/y/f/a;

    :cond_1
    invoke-virtual {v7, v8, v9}, Lf/h/c/y/f/a;->f(J)Lf/h/c/y/f/a;

    sget-object p0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    sub-long/2addr v1, v10

    invoke-virtual {p0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v1

    invoke-virtual {v7, v1, v2}, Lf/h/c/y/f/a;->i(J)Lf/h/c/y/f/a;

    invoke-static {v7}, Lf/h/c/y/j/h;->c(Lf/h/c/y/f/a;)V

    throw v0
.end method
