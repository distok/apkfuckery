.class public Lcom/google/firebase/abt/component/AbtRegistrar;
.super Ljava/lang/Object;
.source "AbtRegistrar.java"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic lambda$getComponents$0(Lf/h/c/m/e;)Lf/h/c/j/c/a;
    .locals 3

    new-instance v0, Lf/h/c/j/c/a;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, Lf/h/c/k/a/a;

    invoke-interface {p0, v2}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lf/h/c/k/a/a;

    invoke-direct {v0, v1, p0}, Lf/h/c/j/c/a;-><init>(Landroid/content/Context;Lf/h/c/k/a/a;)V

    return-object v0
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lf/h/c/m/d;

    const-class v1, Lf/h/c/j/c/a;

    invoke-static {v1}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v1

    const-class v2, Landroid/content/Context;

    new-instance v3, Lf/h/c/m/o;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/k/a/a;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v2, Lf/h/c/j/c/b;->a:Lf/h/c/j/c/b;

    invoke-virtual {v1, v2}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v1}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "fire-abt"

    const-string v2, "20.0.0"

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
