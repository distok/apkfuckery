.class public Lcom/google/firebase/messaging/FirebaseMessaging;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-messaging@@21.0.0"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/messaging/FirebaseMessaging$a;
    }
.end annotation


# static fields
.field public static g:Lf/h/a/b/g;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "FirebaseUnknownNullness"
        }
    .end annotation

    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lf/h/c/c;

.field public final c:Lcom/google/firebase/iid/FirebaseInstanceId;

.field public final d:Lcom/google/firebase/messaging/FirebaseMessaging$a;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:Lcom/google/android/gms/tasks/Task;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tasks/Task<",
            "Lf/h/c/x/y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf/h/c/c;Lcom/google/firebase/iid/FirebaseInstanceId;Lf/h/c/u/a;Lf/h/c/u/a;Lf/h/c/v/g;Lf/h/a/b/g;Lf/h/c/r/d;)V
    .locals 13
    .param p6    # Lf/h/a/b/g;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf/h/c/c;",
            "Lcom/google/firebase/iid/FirebaseInstanceId;",
            "Lf/h/c/u/a<",
            "Lf/h/c/z/h;",
            ">;",
            "Lf/h/c/u/a<",
            "Lf/h/c/s/d;",
            ">;",
            "Lf/h/c/v/g;",
            "Lf/h/a/b/g;",
            "Lf/h/c/r/d;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v2, p1

    move-object v7, p2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    const-string v1, "com.google.firebase.iid.FirebaseInstanceIdReceiver"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    sput-object p6, Lcom/google/firebase/messaging/FirebaseMessaging;->g:Lf/h/a/b/g;

    iput-object v2, v0, Lcom/google/firebase/messaging/FirebaseMessaging;->b:Lf/h/c/c;

    iput-object v7, v0, Lcom/google/firebase/messaging/FirebaseMessaging;->c:Lcom/google/firebase/iid/FirebaseInstanceId;

    new-instance v1, Lcom/google/firebase/messaging/FirebaseMessaging$a;

    move-object/from16 v3, p7

    invoke-direct {v1, p0, v3}, Lcom/google/firebase/messaging/FirebaseMessaging$a;-><init>(Lcom/google/firebase/messaging/FirebaseMessaging;Lf/h/c/r/d;)V

    iput-object v1, v0, Lcom/google/firebase/messaging/FirebaseMessaging;->d:Lcom/google/firebase/messaging/FirebaseMessaging$a;

    invoke-virtual {p1}, Lf/h/c/c;->a()V

    iget-object v8, v2, Lf/h/c/c;->a:Landroid/content/Context;

    iput-object v8, v0, Lcom/google/firebase/messaging/FirebaseMessaging;->a:Landroid/content/Context;

    new-instance v1, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v3, Lf/h/a/f/f/n/k/a;

    const-string v4, "Firebase-Messaging-Init"

    invoke-direct {v3, v4}, Lf/h/a/f/f/n/k/a;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    invoke-direct {v1, v4, v3}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    iput-object v1, v0, Lcom/google/firebase/messaging/FirebaseMessaging;->e:Ljava/util/concurrent/Executor;

    new-instance v3, Lf/h/c/x/g;

    invoke-direct {v3, p0, p2}, Lf/h/c/x/g;-><init>(Lcom/google/firebase/messaging/FirebaseMessaging;Lcom/google/firebase/iid/FirebaseInstanceId;)V

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    new-instance v9, Lf/h/c/t/q;

    invoke-direct {v9, v8}, Lf/h/c/t/q;-><init>(Landroid/content/Context;)V

    new-instance v10, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lf/h/a/f/f/n/k/a;

    const-string v3, "Firebase-Messaging-Topics-Io"

    invoke-direct {v1, v3}, Lf/h/a/f/f/n/k/a;-><init>(Ljava/lang/String;)V

    invoke-direct {v10, v4, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    sget v1, Lf/h/c/x/y;->j:I

    new-instance v11, Lf/h/c/t/n;

    move-object v1, v11

    move-object v2, p1

    move-object v3, v9

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lf/h/c/t/n;-><init>(Lf/h/c/c;Lf/h/c/t/q;Lf/h/c/u/a;Lf/h/c/u/a;Lf/h/c/v/g;)V

    new-instance v12, Lf/h/c/x/x;

    move-object v1, v12

    move-object v2, v8

    move-object v3, v10

    move-object v4, p2

    move-object v5, v9

    move-object v6, v11

    invoke-direct/range {v1 .. v6}, Lf/h/c/x/x;-><init>(Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/firebase/iid/FirebaseInstanceId;Lf/h/c/t/q;Lf/h/c/t/n;)V

    invoke-static {v10, v12}, Lf/h/a/f/f/n/g;->f(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    iput-object v1, v0, Lcom/google/firebase/messaging/FirebaseMessaging;->f:Lcom/google/android/gms/tasks/Task;

    new-instance v10, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v8}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v9, Lf/h/a/f/f/n/k/a;

    const-string v2, "Firebase-Messaging-Trigger-Topics-Io"

    invoke-direct {v9, v2}, Lf/h/a/f/f/n/k/a;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-wide/16 v5, 0x1e

    move-object v2, v10

    invoke-direct/range {v2 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    new-instance v2, Lf/h/c/x/h;

    invoke-direct {v2, p0}, Lf/h/c/x/h;-><init>(Lcom/google/firebase/messaging/FirebaseMessaging;)V

    check-cast v1, Lf/h/a/f/p/b0;

    iget-object v3, v1, Lf/h/a/f/p/b0;->b:Lf/h/a/f/p/y;

    new-instance v4, Lf/h/a/f/p/u;

    sget v5, Lf/h/a/f/p/c0;->a:I

    invoke-direct {v4, v10, v2}, Lf/h/a/f/p/u;-><init>(Ljava/util/concurrent/Executor;Lf/h/a/f/p/e;)V

    invoke-virtual {v3, v4}, Lf/h/a/f/p/y;->b(Lf/h/a/f/p/z;)V

    invoke-virtual {v1}, Lf/h/a/f/p/b0;->w()V

    return-void

    :catch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "FirebaseMessaging and FirebaseInstanceId versions not compatible. Update to latest version of firebase-messaging."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static declared-synchronized getInstance(Lf/h/c/c;)Lcom/google/firebase/messaging/FirebaseMessaging;
    .locals 2
    .param p0    # Lf/h/c/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-class v0, Lcom/google/firebase/messaging/FirebaseMessaging;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lf/h/c/c;->a()V

    iget-object p0, p0, Lf/h/c/c;->d:Lf/h/c/m/k;

    invoke-virtual {p0, v0}, Lf/h/c/m/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/google/firebase/messaging/FirebaseMessaging;

    const-string v1, "Firebase Messaging component is not present"

    invoke-static {p0, v1}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method
