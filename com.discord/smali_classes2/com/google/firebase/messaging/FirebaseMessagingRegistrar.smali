.class public Lcom/google/firebase/messaging/FirebaseMessagingRegistrar;
.super Ljava/lang/Object;
.source "com.google.firebase:firebase-messaging@@21.0.0"

# interfaces
.implements Lf/h/c/m/g;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/firebase/messaging/FirebaseMessagingRegistrar$b;,
        Lcom/google/firebase/messaging/FirebaseMessagingRegistrar$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static determineFactory(Lf/h/a/b/g;)Lf/h/a/b/g;
    .locals 3
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    if-eqz p0, :cond_1

    sget-object v0, Lf/h/a/b/i/a;->g:Lf/h/a/b/i/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/h/a/b/i/a;->f:Ljava/util/Set;

    new-instance v1, Lf/h/a/b/b;

    const-string v2, "json"

    invoke-direct {v1, v2}, Lf/h/a/b/b;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    return-object p0

    :cond_1
    :goto_0
    new-instance p0, Lcom/google/firebase/messaging/FirebaseMessagingRegistrar$c;

    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingRegistrar$c;-><init>()V

    return-object p0
.end method

.method public static final synthetic lambda$getComponents$0$FirebaseMessagingRegistrar(Lf/h/c/m/e;)Lcom/google/firebase/messaging/FirebaseMessaging;
    .locals 9

    new-instance v8, Lcom/google/firebase/messaging/FirebaseMessaging;

    const-class v0, Lf/h/c/c;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lf/h/c/c;

    const-class v0, Lcom/google/firebase/iid/FirebaseInstanceId;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/firebase/iid/FirebaseInstanceId;

    const-class v0, Lf/h/c/z/h;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object v3

    const-class v0, Lf/h/c/s/d;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->b(Ljava/lang/Class;)Lf/h/c/u/a;

    move-result-object v4

    const-class v0, Lf/h/c/v/g;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lf/h/c/v/g;

    const-class v0, Lf/h/a/b/g;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/h/a/b/g;

    invoke-static {v0}, Lcom/google/firebase/messaging/FirebaseMessagingRegistrar;->determineFactory(Lf/h/a/b/g;)Lf/h/a/b/g;

    move-result-object v6

    const-class v0, Lf/h/c/r/d;

    invoke-interface {p0, v0}, Lf/h/c/m/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    move-object v7, p0

    check-cast v7, Lf/h/c/r/d;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/google/firebase/messaging/FirebaseMessaging;-><init>(Lf/h/c/c;Lcom/google/firebase/iid/FirebaseInstanceId;Lf/h/c/u/a;Lf/h/c/u/a;Lf/h/c/v/g;Lf/h/a/b/g;Lf/h/c/r/d;)V

    return-object v8
.end method


# virtual methods
.method public getComponents()Ljava/util/List;
    .locals 6
    .annotation build Landroidx/annotation/Keep;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lf/h/c/m/d<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lf/h/c/m/d;

    const-class v1, Lcom/google/firebase/messaging/FirebaseMessaging;

    invoke-static {v1}, Lf/h/c/m/d;->a(Ljava/lang/Class;)Lf/h/c/m/d$b;

    move-result-object v1

    const-class v2, Lf/h/c/c;

    new-instance v3, Lf/h/c/m/o;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lcom/google/firebase/iid/FirebaseInstanceId;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/z/h;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/s/d;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v4}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/a/b/g;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v5, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/v/g;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    const-class v2, Lf/h/c/r/d;

    new-instance v3, Lf/h/c/m/o;

    invoke-direct {v3, v2, v4, v5}, Lf/h/c/m/o;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v1, v3}, Lf/h/c/m/d$b;->a(Lf/h/c/m/o;)Lf/h/c/m/d$b;

    sget-object v2, Lf/h/c/x/l;->a:Lf/h/c/m/f;

    invoke-virtual {v1, v2}, Lf/h/c/m/d$b;->c(Lf/h/c/m/f;)Lf/h/c/m/d$b;

    invoke-virtual {v1, v4}, Lf/h/c/m/d$b;->d(I)Lf/h/c/m/d$b;

    invoke-virtual {v1}, Lf/h/c/m/d$b;->b()Lf/h/c/m/d;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "fire-fcm"

    const-string v2, "20.1.7_1p"

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->j(Ljava/lang/String;Ljava/lang/String;)Lf/h/c/m/d;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
