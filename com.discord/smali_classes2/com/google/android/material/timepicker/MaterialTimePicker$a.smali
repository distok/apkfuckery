.class public Lcom/google/android/material/timepicker/MaterialTimePicker$a;
.super Ljava/lang/Object;
.source "MaterialTimePicker.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/material/timepicker/MaterialTimePicker;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/android/material/timepicker/MaterialTimePicker;


# direct methods
.method public constructor <init>(Lcom/google/android/material/timepicker/MaterialTimePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/material/timepicker/MaterialTimePicker$a;->d:Lcom/google/android/material/timepicker/MaterialTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/material/timepicker/MaterialTimePicker$a;->d:Lcom/google/android/material/timepicker/MaterialTimePicker;

    invoke-static {p1}, Lcom/google/android/material/timepicker/MaterialTimePicker;->access$000(Lcom/google/android/material/timepicker/MaterialTimePicker;)Lcom/google/android/material/timepicker/MaterialTimePicker$OnTimeSetListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/material/timepicker/MaterialTimePicker$a;->d:Lcom/google/android/material/timepicker/MaterialTimePicker;

    invoke-static {p1}, Lcom/google/android/material/timepicker/MaterialTimePicker;->access$000(Lcom/google/android/material/timepicker/MaterialTimePicker;)Lcom/google/android/material/timepicker/MaterialTimePicker$OnTimeSetListener;

    move-result-object p1

    iget-object v0, p0, Lcom/google/android/material/timepicker/MaterialTimePicker$a;->d:Lcom/google/android/material/timepicker/MaterialTimePicker;

    invoke-interface {p1, v0}, Lcom/google/android/material/timepicker/MaterialTimePicker$OnTimeSetListener;->onTimeSet(Lcom/google/android/material/timepicker/MaterialTimePicker;)V

    :cond_0
    iget-object p1, p0, Lcom/google/android/material/timepicker/MaterialTimePicker$a;->d:Lcom/google/android/material/timepicker/MaterialTimePicker;

    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->dismiss()V

    return-void
.end method
