.class public final Lcom/google/android/material/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f120006

.field public static final abc_action_bar_up_description:I = 0x7f120007

.field public static final abc_action_menu_overflow_description:I = 0x7f120008

.field public static final abc_action_mode_done:I = 0x7f120009

.field public static final abc_activity_chooser_view_see_all:I = 0x7f12000a

.field public static final abc_activitychooserview_choose_application:I = 0x7f12000b

.field public static final abc_capital_off:I = 0x7f12000c

.field public static final abc_capital_on:I = 0x7f12000d

.field public static final abc_menu_alt_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_delete_shortcut_label:I = 0x7f120010

.field public static final abc_menu_enter_shortcut_label:I = 0x7f120011

.field public static final abc_menu_function_shortcut_label:I = 0x7f120012

.field public static final abc_menu_meta_shortcut_label:I = 0x7f120013

.field public static final abc_menu_shift_shortcut_label:I = 0x7f120014

.field public static final abc_menu_space_shortcut_label:I = 0x7f120015

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120016

.field public static final abc_prepend_shortcut_label:I = 0x7f120017

.field public static final abc_search_hint:I = 0x7f120018

.field public static final abc_searchview_description_clear:I = 0x7f120019

.field public static final abc_searchview_description_query:I = 0x7f12001a

.field public static final abc_searchview_description_search:I = 0x7f12001b

.field public static final abc_searchview_description_submit:I = 0x7f12001c

.field public static final abc_searchview_description_voice:I = 0x7f12001d

.field public static final abc_shareactionprovider_share_with:I = 0x7f12001e

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f12001f

.field public static final abc_toolbar_collapse_description:I = 0x7f120020

.field public static final appbar_scrolling_view_behavior:I = 0x7f12010d

.field public static final bottom_sheet_behavior:I = 0x7f120386

.field public static final character_counter_content_description:I = 0x7f12047a

.field public static final character_counter_overflowed_content_description:I = 0x7f12047b

.field public static final character_counter_pattern:I = 0x7f12047c

.field public static final chip_text:I = 0x7f120485

.field public static final clear_text_end_icon_content_description:I = 0x7f120494

.field public static final error_icon_content_description:I = 0x7f1206e6

.field public static final exposed_dropdown_menu_content_description:I = 0x7f12072e

.field public static final fab_transformation_scrim_behavior:I = 0x7f120739

.field public static final fab_transformation_sheet_behavior:I = 0x7f12073a

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120cda

.field public static final icon_content_description:I = 0x7f120d67

.field public static final item_view_role_description:I = 0x7f120e8d

.field public static final material_clock_display_divider:I = 0x7f120fbd

.field public static final material_clock_toggle_content_description:I = 0x7f120fbe

.field public static final material_hour_selection:I = 0x7f120fbf

.field public static final material_hour_suffix:I = 0x7f120fc0

.field public static final material_minute_selection:I = 0x7f120fc1

.field public static final material_minute_suffix:I = 0x7f120fc2

.field public static final material_slider_range_end:I = 0x7f120fc3

.field public static final material_slider_range_start:I = 0x7f120fc4

.field public static final material_timepicker_am:I = 0x7f120fc5

.field public static final material_timepicker_hour:I = 0x7f120fc6

.field public static final material_timepicker_minute:I = 0x7f120fc7

.field public static final material_timepicker_pm:I = 0x7f120fc8

.field public static final material_timepicker_select_time:I = 0x7f120fc9

.field public static final mtrl_badge_numberless_content_description:I = 0x7f121086

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f121087

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f121088

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f121089

.field public static final mtrl_picker_a11y_next_month:I = 0x7f12108a

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f12108b

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12108c

.field public static final mtrl_picker_cancel:I = 0x7f12108d

.field public static final mtrl_picker_confirm:I = 0x7f12108e

.field public static final mtrl_picker_date_header_selected:I = 0x7f12108f

.field public static final mtrl_picker_date_header_title:I = 0x7f121090

.field public static final mtrl_picker_date_header_unselected:I = 0x7f121091

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f121092

.field public static final mtrl_picker_invalid_format:I = 0x7f121093

.field public static final mtrl_picker_invalid_format_example:I = 0x7f121094

.field public static final mtrl_picker_invalid_format_use:I = 0x7f121095

.field public static final mtrl_picker_invalid_range:I = 0x7f121096

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f121097

.field public static final mtrl_picker_out_of_range:I = 0x7f121098

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f121099

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f12109a

.field public static final mtrl_picker_range_header_selected:I = 0x7f12109b

.field public static final mtrl_picker_range_header_title:I = 0x7f12109c

.field public static final mtrl_picker_range_header_unselected:I = 0x7f12109d

.field public static final mtrl_picker_save:I = 0x7f12109e

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f12109f

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f1210a0

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f1210a1

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f1210a2

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f1210a3

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f1210a4

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f1210a5

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f1210a6

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f1210a7

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f1210a8

.field public static final password_toggle_content_description:I = 0x7f121257

.field public static final path_password_eye:I = 0x7f121259

.field public static final path_password_eye_mask_strike_through:I = 0x7f12125a

.field public static final path_password_eye_mask_visible:I = 0x7f12125b

.field public static final path_password_strike_through:I = 0x7f12125c

.field public static final search_menu_title:I = 0x7f121624

.field public static final status_bar_notification_info_overflow:I = 0x7f1216ea


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
