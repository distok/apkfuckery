.class public final Lcom/google/android/material/transition/MaterialContainerTransform$d;
.super Landroid/graphics/drawable/Drawable;
.source "MaterialContainerTransform.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/transition/MaterialContainerTransform;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field public final A:Lf/h/a/g/k/d;

.field public final B:Z

.field public final C:Landroid/graphics/Paint;

.field public final D:Landroid/graphics/Path;

.field public E:Lf/h/a/g/k/c;

.field public F:Lf/h/a/g/k/f;

.field public G:Landroid/graphics/RectF;

.field public H:F

.field public I:F

.field public final a:Landroid/view/View;

.field public final b:Landroid/graphics/RectF;

.field public final c:Lcom/google/android/material/shape/ShapeAppearanceModel;

.field public final d:F

.field public final e:Landroid/view/View;

.field public final f:Landroid/graphics/RectF;

.field public final g:Lcom/google/android/material/shape/ShapeAppearanceModel;

.field public final h:F

.field public final i:Landroid/graphics/Paint;

.field public final j:Landroid/graphics/Paint;

.field public final k:Landroid/graphics/Paint;

.field public final l:Landroid/graphics/Paint;

.field public final m:Landroid/graphics/Paint;

.field public final n:Lf/h/a/g/k/g;

.field public final o:Landroid/graphics/PathMeasure;

.field public final p:F

.field public final q:[F

.field public final r:Z

.field public final s:Z

.field public final t:Lcom/google/android/material/shape/MaterialShapeDrawable;

.field public final u:Landroid/graphics/RectF;

.field public final v:Landroid/graphics/RectF;

.field public final w:Landroid/graphics/RectF;

.field public final x:Landroid/graphics/RectF;

.field public final y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

.field public final z:Lf/h/a/g/k/a;


# direct methods
.method public constructor <init>(Landroidx/transition/PathMotion;Landroid/view/View;Landroid/graphics/RectF;Lcom/google/android/material/shape/ShapeAppearanceModel;FLandroid/view/View;Landroid/graphics/RectF;Lcom/google/android/material/shape/ShapeAppearanceModel;FIIIIZZLf/h/a/g/k/a;Lf/h/a/g/k/d;Lcom/google/android/material/transition/MaterialContainerTransform$c;ZLcom/google/android/material/transition/MaterialContainerTransform$a;)V
    .locals 12

    move-object v0, p0

    move-object v1, p3

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->i:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->j:Landroid/graphics/Paint;

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->k:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->l:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iput-object v5, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->m:Landroid/graphics/Paint;

    new-instance v6, Lf/h/a/g/k/g;

    invoke-direct {v6}, Lf/h/a/g/k/g;-><init>()V

    iput-object v6, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    const/4 v6, 0x2

    new-array v7, v6, [F

    iput-object v7, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->q:[F

    new-instance v8, Lcom/google/android/material/shape/MaterialShapeDrawable;

    invoke-direct {v8}, Lcom/google/android/material/shape/MaterialShapeDrawable;-><init>()V

    iput-object v8, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->t:Lcom/google/android/material/shape/MaterialShapeDrawable;

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    iput-object v9, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    new-instance v10, Landroid/graphics/Path;

    invoke-direct {v10}, Landroid/graphics/Path;-><init>()V

    iput-object v10, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->D:Landroid/graphics/Path;

    move-object v10, p2

    iput-object v10, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->a:Landroid/view/View;

    iput-object v1, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->b:Landroid/graphics/RectF;

    move-object/from16 v10, p4

    iput-object v10, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->c:Lcom/google/android/material/shape/ShapeAppearanceModel;

    move/from16 v10, p5

    iput v10, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->d:F

    move-object/from16 v10, p6

    iput-object v10, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->e:Landroid/view/View;

    move-object/from16 v10, p7

    iput-object v10, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->f:Landroid/graphics/RectF;

    move-object/from16 v11, p8

    iput-object v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->g:Lcom/google/android/material/shape/ShapeAppearanceModel;

    move/from16 v11, p9

    iput v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->h:F

    move/from16 v11, p14

    iput-boolean v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->r:Z

    move/from16 v11, p15

    iput-boolean v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->s:Z

    move-object/from16 v11, p16

    iput-object v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->z:Lf/h/a/g/k/a;

    move-object/from16 v11, p17

    iput-object v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->A:Lf/h/a/g/k/d;

    move-object/from16 v11, p18

    iput-object v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    move/from16 v11, p19

    iput-boolean v11, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->B:Z

    move/from16 v11, p10

    invoke-virtual {v2, v11}, Landroid/graphics/Paint;->setColor(I)V

    move/from16 v2, p11

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setColor(I)V

    move/from16 v2, p12

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v2, 0x0

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setFillColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {v8, v6}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setShadowCompatibilityMode(I)V

    invoke-virtual {v8, v2}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setShadowBitmapDrawingEnable(Z)V

    const v3, -0x777778

    invoke-virtual {v8, v3}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setShadowColor(I)V

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, p3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v3, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v4, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v4, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->w:Landroid/graphics/RectF;

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v3, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    invoke-static {p3}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->c(Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-static/range {p7 .. p7}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->c(Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v6, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget v8, v4, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    move-object v10, p1

    invoke-virtual {p1, v6, v3, v8, v4}, Landroidx/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v3

    new-instance v4, Landroid/graphics/PathMeasure;

    invoke-direct {v4, v3, v2}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    iput-object v4, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->o:Landroid/graphics/PathMeasure;

    invoke-virtual {v4}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v3

    iput v3, v0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->p:F

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    aput v3, v7, v2

    iget v1, v1, Landroid/graphics/RectF;->top:F

    const/4 v2, 0x1

    aput v1, v7, v2

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lf/h/a/g/k/k;->a:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/LinearGradient;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object p1, v1

    move p2, v3

    move p3, v4

    move/from16 p4, v6

    move/from16 p5, v7

    move/from16 p6, p13

    move/from16 p7, p13

    move-object/from16 p8, v2

    invoke-direct/range {p1 .. p8}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->e(F)V

    return-void
.end method

.method public static c(Landroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 2

    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget p0, p0, Landroid/graphics/RectF;->top:F

    invoke-direct {v0, v1, p0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->k:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->d(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->w:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->F:Lf/h/a/g/k/f;

    iget v5, v0, Lf/h/a/g/k/f;->b:F

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->E:Lf/h/a/g/k/c;

    iget v6, v0, Lf/h/a/g/k/c;->b:I

    new-instance v7, Lcom/google/android/material/transition/MaterialContainerTransform$d$b;

    invoke-direct {v7, p0}, Lcom/google/android/material/transition/MaterialContainerTransform$d$b;-><init>(Lcom/google/android/material/transition/MaterialContainerTransform$d;)V

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lf/h/a/g/k/k;->g(Landroid/graphics/Canvas;Landroid/graphics/Rect;FFFILf/h/a/g/k/k$a;)V

    return-void
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->j:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->d(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget v4, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->F:Lf/h/a/g/k/f;

    iget v5, v0, Lf/h/a/g/k/f;->a:F

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->E:Lf/h/a/g/k/c;

    iget v6, v0, Lf/h/a/g/k/c;->a:I

    new-instance v7, Lcom/google/android/material/transition/MaterialContainerTransform$d$a;

    invoke-direct {v7, p0}, Lcom/google/android/material/transition/MaterialContainerTransform$d$a;-><init>(Lcom/google/android/material/transition/MaterialContainerTransform$d;)V

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lf/h/a/g/k/k;->g(Landroid/graphics/Canvas;Landroid/graphics/Rect;FFFILf/h/a/g/k/k$a;)V

    return-void
.end method

.method public final d(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->m:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->B:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->s:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->H:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    iget-object v2, v2, Lf/h/a/g/k/g;->a:Landroid/graphics/Path;

    sget-object v4, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    const/16 v2, 0x1c

    if-le v0, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    iget-object v2, v2, Lf/h/a/g/k/g;->e:Lcom/google/android/material/shape/ShapeAppearanceModel;

    iget-object v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->G:Landroid/graphics/RectF;

    invoke-virtual {v2, v4}, Lcom/google/android/material/shape/ShapeAppearanceModel;->isRoundRect(Landroid/graphics/RectF;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->G:Landroid/graphics/RectF;

    invoke-interface {v2, v4}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v2

    iget-object v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->G:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v2, v2, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    iget-object v2, v2, Lf/h/a/g/k/g;->a:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->t:Lcom/google/android/material/shape/MaterialShapeDrawable;

    iget-object v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->G:Landroid/graphics/RectF;

    iget v5, v4, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget v6, v4, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    iget v7, v4, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v2, v5, v6, v7, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->t:Lcom/google/android/material/shape/MaterialShapeDrawable;

    iget v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->H:F

    invoke-virtual {v2, v4}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setElevation(F)V

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->t:Lcom/google/android/material/shape/MaterialShapeDrawable;

    iget v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->H:F

    const/high16 v5, 0x3f400000    # 0.75f

    mul-float v4, v4, v5

    float-to-int v4, v4

    invoke-virtual {v2, v4}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setShadowVerticalOffset(I)V

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->t:Lcom/google/android/material/shape/MaterialShapeDrawable;

    iget-object v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    iget-object v4, v4, Lf/h/a/g/k/g;->e:Lcom/google/android/material/shape/ShapeAppearanceModel;

    invoke-virtual {v2, v4}, Lcom/google/android/material/shape/MaterialShapeDrawable;->setShapeAppearanceModel(Lcom/google/android/material/shape/ShapeAppearanceModel;)V

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->t:Lcom/google/android/material/shape/MaterialShapeDrawable;

    invoke-virtual {v2, p1}, Lcom/google/android/material/shape/MaterialShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_4
    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    const/16 v4, 0x17

    if-lt v0, v4, :cond_5

    iget-object v0, v2, Lf/h/a/g/k/g;->a:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    goto :goto_2

    :cond_5
    iget-object v0, v2, Lf/h/a/g/k/g;->b:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    iget-object v0, v2, Lf/h/a/g/k/g;->c:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    :goto_2
    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->i:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->d(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->E:Lf/h/a/g/k/c;

    iget-boolean v0, v0, Lf/h/a/g/k/c;->c:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0, p1}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->b(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, p1}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->a(Landroid/graphics/Canvas;)V

    goto :goto_3

    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->a(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, p1}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->b(Landroid/graphics/Canvas;)V

    :goto_3
    iget-boolean v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->B:Z

    if-eqz v0, :cond_8

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->D:Landroid/graphics/Path;

    const v2, -0xff01

    invoke-static {v0}, Lcom/google/android/material/transition/MaterialContainerTransform$d;->c(Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v0

    iget v4, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->I:F

    cmpl-float v3, v4, v3

    if-nez v3, :cond_7

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_4

    :cond_7
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    const/16 v1, -0x100

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    const v1, -0xff0100

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    const v1, -0xff0001

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->w:Landroid/graphics/RectF;

    const v1, -0xffff01

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->C:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_8
    return-void
.end method

.method public final e(F)V
    .locals 14

    iput p1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->I:F

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->m:Landroid/graphics/Paint;

    iget-boolean v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->r:Z

    const/high16 v2, 0x437f0000    # 255.0f

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    invoke-static {v3, v2, p1}, Lf/h/a/g/k/k;->d(FFF)F

    move-result v1

    goto :goto_0

    :cond_0
    invoke-static {v2, v3, p1}, Lf/h/a/g/k/k;->d(FFF)F

    move-result v1

    :goto_0
    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->d:F

    iget v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->h:F

    invoke-static {v0, v1, p1}, Lf/h/a/g/k/k;->d(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->H:F

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->l:Landroid/graphics/Paint;

    const/high16 v2, 0x2d000000

    invoke-virtual {v1, v0, v3, v0, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->o:Landroid/graphics/PathMeasure;

    iget v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->p:F

    mul-float v1, v1, p1

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->q:[F

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->q:[F

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v4, 0x1

    aget v0, v0, v4

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v5, v5, Lcom/google/android/material/transition/MaterialContainerTransform$c;->b:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v5}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->access$1000(Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v5}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v5, v5, Lcom/google/android/material/transition/MaterialContainerTransform$c;->b:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v5}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->access$1100(Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v5}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->A:Lf/h/a/g/k/d;

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->b:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v10

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->b:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v11

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->f:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v12

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->f:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v13

    move v7, p1

    invoke-interface/range {v6 .. v13}, Lf/h/a/g/k/d;->a(FFFFFFF)Lf/h/a/g/k/f;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->F:Lf/h/a/g/k/f;

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    iget v7, v5, Lf/h/a/g/k/f;->c:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sub-float v9, v2, v7

    add-float/2addr v7, v2

    iget v5, v5, Lf/h/a/g/k/f;->d:F

    add-float/2addr v5, v0

    invoke-virtual {v6, v9, v0, v7, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->w:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->F:Lf/h/a/g/k/f;

    iget v7, v6, Lf/h/a/g/k/f;->e:F

    div-float/2addr v7, v8

    sub-float v8, v2, v7

    add-float/2addr v7, v2

    iget v2, v6, Lf/h/a/g/k/f;->f:F

    add-float/2addr v2, v0

    invoke-virtual {v5, v8, v0, v7, v2}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->w:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v0, v0, Lcom/google/android/material/transition/MaterialContainerTransform$c;->c:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v0}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->access$1000(Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v2, v2, Lcom/google/android/material/transition/MaterialContainerTransform$c;->c:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v2}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->access$1100(Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v2}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->A:Lf/h/a/g/k/d;

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->F:Lf/h/a/g/k/f;

    invoke-interface {v5, v6}, Lf/h/a/g/k/d;->b(Lf/h/a/g/k/f;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    goto :goto_1

    :cond_1
    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    :goto_1
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v3, v7, v0, v2, p1}, Lf/h/a/g/k/k;->e(FFFFF)F

    move-result v0

    if-eqz v5, :cond_2

    goto :goto_2

    :cond_2
    sub-float v0, v7, v0

    :goto_2
    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->A:Lf/h/a/g/k/d;

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->F:Lf/h/a/g/k/f;

    invoke-interface {v2, v6, v0, v5}, Lf/h/a/g/k/d;->c(Landroid/graphics/RectF;FLf/h/a/g/k/f;)V

    new-instance v0, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    invoke-static {v6, v8}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iget-object v8, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v9, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-direct {v0, v2, v5, v6, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->G:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->n:Lf/h/a/g/k/g;

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->c:Lcom/google/android/material/shape/ShapeAppearanceModel;

    iget-object v5, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->g:Lcom/google/android/material/shape/ShapeAppearanceModel;

    iget-object v6, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->u:Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->v:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->x:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v10, v10, Lcom/google/android/material/transition/MaterialContainerTransform$c;->d:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->getStart()F

    move-result v11

    invoke-virtual {v10}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->getEnd()F

    move-result v10

    sget-object v12, Lf/h/a/g/k/k;->a:Landroid/graphics/RectF;

    cmpg-float v12, p1, v11

    if-gez v12, :cond_3

    goto/16 :goto_4

    :cond_3
    cmpl-float v12, p1, v10

    if-lez v12, :cond_4

    move-object v2, v5

    goto/16 :goto_4

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v12

    invoke-interface {v12, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v12

    cmpl-float v12, v12, v3

    if-nez v12, :cond_5

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopRightCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v12

    invoke-interface {v12, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v12

    cmpl-float v12, v12, v3

    if-nez v12, :cond_5

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getBottomRightCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v12

    invoke-interface {v12, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v12

    cmpl-float v12, v12, v3

    if-nez v12, :cond_5

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getBottomLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v12

    invoke-interface {v12, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v12

    cmpl-float v3, v12, v3

    if-eqz v3, :cond_6

    :cond_5
    const/4 v1, 0x1

    :cond_6
    if-eqz v1, :cond_7

    move-object v1, v2

    goto :goto_3

    :cond_7
    move-object v1, v5

    :goto_3
    invoke-virtual {v1}, Lcom/google/android/material/shape/ShapeAppearanceModel;->toBuilder()Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v4

    invoke-interface {v3, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v3

    invoke-interface {v4, v9}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v4

    invoke-static {v3, v4, v11, v10, p1}, Lf/h/a/g/k/k;->e(FFFFF)F

    move-result v3

    new-instance v4, Lcom/google/android/material/shape/AbsoluteCornerSize;

    invoke-direct {v4, v3}, Lcom/google/android/material/shape/AbsoluteCornerSize;-><init>(F)V

    invoke-virtual {v1, v4}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->setTopLeftCornerSize(Lcom/google/android/material/shape/CornerSize;)Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopRightCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getTopRightCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v4

    invoke-interface {v3, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v3

    invoke-interface {v4, v9}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v4

    invoke-static {v3, v4, v11, v10, p1}, Lf/h/a/g/k/k;->e(FFFFF)F

    move-result v3

    new-instance v4, Lcom/google/android/material/shape/AbsoluteCornerSize;

    invoke-direct {v4, v3}, Lcom/google/android/material/shape/AbsoluteCornerSize;-><init>(F)V

    invoke-virtual {v1, v4}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->setTopRightCornerSize(Lcom/google/android/material/shape/CornerSize;)Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getBottomLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getBottomLeftCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v4

    invoke-interface {v3, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v3

    invoke-interface {v4, v9}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v4

    invoke-static {v3, v4, v11, v10, p1}, Lf/h/a/g/k/k;->e(FFFFF)F

    move-result v3

    new-instance v4, Lcom/google/android/material/shape/AbsoluteCornerSize;

    invoke-direct {v4, v3}, Lcom/google/android/material/shape/AbsoluteCornerSize;-><init>(F)V

    invoke-virtual {v1, v4}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->setBottomLeftCornerSize(Lcom/google/android/material/shape/CornerSize;)Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getBottomRightCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v2

    invoke-virtual {v5}, Lcom/google/android/material/shape/ShapeAppearanceModel;->getBottomRightCornerSize()Lcom/google/android/material/shape/CornerSize;

    move-result-object v3

    invoke-interface {v2, v6}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v2

    invoke-interface {v3, v9}, Lcom/google/android/material/shape/CornerSize;->getCornerSize(Landroid/graphics/RectF;)F

    move-result v3

    invoke-static {v2, v3, v11, v10, p1}, Lf/h/a/g/k/k;->e(FFFFF)F

    move-result v2

    new-instance v3, Lcom/google/android/material/shape/AbsoluteCornerSize;

    invoke-direct {v3, v2}, Lcom/google/android/material/shape/AbsoluteCornerSize;-><init>(F)V

    invoke-virtual {v1, v3}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->setBottomRightCornerSize(Lcom/google/android/material/shape/CornerSize;)Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/material/shape/ShapeAppearanceModel$Builder;->build()Lcom/google/android/material/shape/ShapeAppearanceModel;

    move-result-object v2

    :goto_4
    iput-object v2, v0, Lf/h/a/g/k/g;->e:Lcom/google/android/material/shape/ShapeAppearanceModel;

    iget-object v1, v0, Lf/h/a/g/k/g;->d:Lcom/google/android/material/shape/ShapeAppearancePathProvider;

    iget-object v3, v0, Lf/h/a/g/k/g;->b:Landroid/graphics/Path;

    invoke-virtual {v1, v2, v7, v8, v3}, Lcom/google/android/material/shape/ShapeAppearancePathProvider;->calculatePath(Lcom/google/android/material/shape/ShapeAppearanceModel;FLandroid/graphics/RectF;Landroid/graphics/Path;)V

    iget-object v1, v0, Lf/h/a/g/k/g;->d:Lcom/google/android/material/shape/ShapeAppearancePathProvider;

    iget-object v2, v0, Lf/h/a/g/k/g;->e:Lcom/google/android/material/shape/ShapeAppearanceModel;

    iget-object v3, v0, Lf/h/a/g/k/g;->c:Landroid/graphics/Path;

    invoke-virtual {v1, v2, v7, v9, v3}, Lcom/google/android/material/shape/ShapeAppearancePathProvider;->calculatePath(Lcom/google/android/material/shape/ShapeAppearanceModel;FLandroid/graphics/RectF;Landroid/graphics/Path;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_8

    iget-object v1, v0, Lf/h/a/g/k/g;->a:Landroid/graphics/Path;

    iget-object v2, v0, Lf/h/a/g/k/g;->b:Landroid/graphics/Path;

    iget-object v0, v0, Lf/h/a/g/k/g;->c:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Op;->UNION:Landroid/graphics/Path$Op;

    invoke-virtual {v1, v2, v0, v3}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    :cond_8
    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v0, v0, Lcom/google/android/material/transition/MaterialContainerTransform$c;->a:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v0}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->access$1000(Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-static {v0}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->y:Lcom/google/android/material/transition/MaterialContainerTransform$c;

    iget-object v1, v1, Lcom/google/android/material/transition/MaterialContainerTransform$c;->a:Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;

    invoke-static {v1}, Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;->access$1100(Lcom/google/android/material/transition/MaterialContainerTransform$ProgressThresholds;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v1}, Landroidx/core/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->z:Lf/h/a/g/k/a;

    invoke-interface {v2, p1, v0, v1}, Lf/h/a/g/k/a;->a(FFF)Lf/h/a/g/k/c;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->E:Lf/h/a/g/k/c;

    iget-object p1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->j:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->j:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->E:Lf/h/a/g/k/c;

    iget v0, v0, Lf/h/a/g/k/c;->a:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_9
    iget-object p1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->k:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getColor()I

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->k:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/material/transition/MaterialContainerTransform$d;->E:Lf/h/a/g/k/c;

    iget v0, v0, Lf/h/a/g/k/c;->b:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_a
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Setting alpha on is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Setting a color filter is not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
