.class public final Lcom/google/android/exoplayer2/ui/R$f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final exo_controls_fastforward_description:I = 0x7f1206f7

.field public static final exo_controls_fullscreen_description:I = 0x7f1206f8

.field public static final exo_controls_hide:I = 0x7f1206f9

.field public static final exo_controls_next_description:I = 0x7f1206fa

.field public static final exo_controls_pause_description:I = 0x7f1206fb

.field public static final exo_controls_play_description:I = 0x7f1206fc

.field public static final exo_controls_previous_description:I = 0x7f1206fd

.field public static final exo_controls_repeat_all_description:I = 0x7f1206fe

.field public static final exo_controls_repeat_off_description:I = 0x7f1206ff

.field public static final exo_controls_repeat_one_description:I = 0x7f120700

.field public static final exo_controls_rewind_description:I = 0x7f120701

.field public static final exo_controls_show:I = 0x7f120702

.field public static final exo_controls_shuffle_off_description:I = 0x7f120703

.field public static final exo_controls_shuffle_on_description:I = 0x7f120704

.field public static final exo_controls_stop_description:I = 0x7f120705

.field public static final exo_controls_vr_description:I = 0x7f120706

.field public static final exo_download_completed:I = 0x7f120707

.field public static final exo_download_description:I = 0x7f120708

.field public static final exo_download_downloading:I = 0x7f120709

.field public static final exo_download_failed:I = 0x7f12070a

.field public static final exo_download_notification_channel_name:I = 0x7f12070b

.field public static final exo_download_removing:I = 0x7f12070c

.field public static final exo_item_list:I = 0x7f12070d

.field public static final exo_track_bitrate:I = 0x7f12070e

.field public static final exo_track_mono:I = 0x7f12070f

.field public static final exo_track_resolution:I = 0x7f120710

.field public static final exo_track_role_alternate:I = 0x7f120711

.field public static final exo_track_role_closed_captions:I = 0x7f120712

.field public static final exo_track_role_commentary:I = 0x7f120713

.field public static final exo_track_role_supplementary:I = 0x7f120714

.field public static final exo_track_selection_auto:I = 0x7f120715

.field public static final exo_track_selection_none:I = 0x7f120716

.field public static final exo_track_selection_title_audio:I = 0x7f120717

.field public static final exo_track_selection_title_text:I = 0x7f120718

.field public static final exo_track_selection_title_video:I = 0x7f120719

.field public static final exo_track_stereo:I = 0x7f12071a

.field public static final exo_track_surround:I = 0x7f12071b

.field public static final exo_track_surround_5_point_1:I = 0x7f12071c

.field public static final exo_track_surround_7_point_1:I = 0x7f12071d

.field public static final exo_track_unknown:I = 0x7f12071e

.field public static final status_bar_notification_info_overflow:I = 0x7f1216ea


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
