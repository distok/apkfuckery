.class public final Lcom/google/android/exoplayer2/ui/PlayerControlView$b;
.super Ljava/lang/Object;
.source "PlayerControlView.java"

# interfaces
.implements Lf/h/a/c/m0$a;
.implements Lf/h/a/c/g1/f$a;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/PlayerControlView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/android/exoplayer2/ui/PlayerControlView;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/ui/PlayerControlView;Lcom/google/android/exoplayer2/ui/PlayerControlView$a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic C(Lf/h/a/c/j0;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->c(Lf/h/a/c/m0$a;Lf/h/a/c/j0;)V

    return-void
.end method

.method public D(Z)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    sget v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g0:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->o()V

    return-void
.end method

.method public synthetic a()V
    .locals 0

    invoke-static {p0}, Lf/h/a/c/l0;->h(Lf/h/a/c/m0$a;)V

    return-void
.end method

.method public b(Lf/h/a/c/g1/f;J)V
    .locals 2

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    iget-object v0, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->r:Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->s:Ljava/util/Formatter;

    invoke-static {v1, p1, p2, p3}, Lf/h/a/c/i1/a0;->n(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public c(Lf/h/a/c/g1/f;JZ)V
    .locals 6

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->Q:Z

    if-nez p4, :cond_3

    iget-object p4, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->J:Lf/h/a/c/m0;

    if-eqz p4, :cond_3

    invoke-interface {p4}, Lf/h/a/c/m0;->A()Lf/h/a/c/t0;

    move-result-object v1

    iget-boolean v2, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->P:Z

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lf/h/a/c/t0;->p()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lf/h/a/c/t0;->o()I

    move-result v2

    :goto_0
    iget-object v3, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->u:Lf/h/a/c/t0$c;

    invoke-virtual {v1, v0, v3}, Lf/h/a/c/t0;->m(ILf/h/a/c/t0$c;)Lf/h/a/c/t0$c;

    move-result-object v3

    invoke-virtual {v3}, Lf/h/a/c/t0$c;->a()J

    move-result-wide v3

    cmp-long v5, p2, v3

    if-gez v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v5, v2, -0x1

    if-ne v0, v5, :cond_1

    move-wide p2, v3

    goto :goto_1

    :cond_1
    sub-long/2addr p2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p4}, Lf/h/a/c/m0;->m()I

    move-result v0

    :goto_1
    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->K:Lf/h/a/c/v;

    check-cast p1, Lf/h/a/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p4, v0, p2, p3}, Lf/h/a/c/m0;->e(IJ)V

    :cond_3
    return-void
.end method

.method public d(Lf/h/a/c/g1/f;J)V
    .locals 2

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->Q:Z

    iget-object v0, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->r:Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->s:Ljava/util/Formatter;

    invoke-static {v1, p1, p2, p3}, Lf/h/a/c/i1/a0;->n(Ljava/lang/StringBuilder;Ljava/util/Formatter;J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public synthetic e(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->d(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public synthetic f(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->b(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public g(I)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    sget v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g0:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->m()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->r()V

    return-void
.end method

.method public synthetic k(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->e(Lf/h/a/c/m0$a;Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    return-void
.end method

.method public l(Lf/h/a/c/t0;I)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    sget p2, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g0:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->m()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->r()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    iget-object v1, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->J:Lf/h/a/c/m0;

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g:Landroid/view/View;

    if-ne v2, p1, :cond_1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->e(Lf/h/a/c/m0;)V

    goto/16 :goto_6

    :cond_1
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->f:Landroid/view/View;

    if-ne v2, p1, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->f(Lf/h/a/c/m0;)V

    goto/16 :goto_6

    :cond_2
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->j:Landroid/view/View;

    if-ne v2, p1, :cond_3

    invoke-interface {v1}, Lf/h/a/c/m0;->i()Z

    move-result p1

    if-eqz p1, :cond_10

    iget p1, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->S:I

    if-lez p1, :cond_10

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->h(Lf/h/a/c/m0;J)V

    goto/16 :goto_6

    :cond_3
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->k:Landroid/view/View;

    if-ne v2, p1, :cond_4

    invoke-interface {v1}, Lf/h/a/c/m0;->i()Z

    move-result p1

    if-eqz p1, :cond_10

    iget p1, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->R:I

    if-lez p1, :cond_10

    neg-int p1, p1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->h(Lf/h/a/c/m0;J)V

    goto/16 :goto_6

    :cond_4
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->h:Landroid/view/View;

    const/4 v3, 0x1

    if-ne v2, p1, :cond_7

    invoke-interface {v1}, Lf/h/a/c/m0;->r()I

    move-result p1

    if-ne p1, v3, :cond_5

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->M:Lf/h/a/c/k0;

    if-eqz p1, :cond_6

    invoke-interface {p1}, Lf/h/a/c/k0;->a()V

    goto :goto_0

    :cond_5
    invoke-interface {v1}, Lf/h/a/c/m0;->r()I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_6

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-interface {v1}, Lf/h/a/c/m0;->m()I

    move-result v0

    const-wide v4, -0x7fffffffffffffffL    # -4.9E-324

    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->K:Lf/h/a/c/v;

    check-cast p1, Lf/h/a/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v0, v4, v5}, Lf/h/a/c/m0;->e(IJ)V

    :cond_6
    :goto_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    iget-object p1, p1, Lcom/google/android/exoplayer2/ui/PlayerControlView;->K:Lf/h/a/c/v;

    check-cast p1, Lf/h/a/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v3}, Lf/h/a/c/m0;->n(Z)V

    goto :goto_6

    :cond_7
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->i:Landroid/view/View;

    const/4 v4, 0x0

    if-ne v2, p1, :cond_8

    iget-object p1, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->K:Lf/h/a/c/v;

    check-cast p1, Lf/h/a/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v4}, Lf/h/a/c/m0;->n(Z)V

    goto :goto_6

    :cond_8
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->l:Landroid/widget/ImageView;

    if-ne v2, p1, :cond_f

    iget-object p1, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->K:Lf/h/a/c/v;

    invoke-interface {v1}, Lf/h/a/c/m0;->y()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    iget v2, v2, Lcom/google/android/exoplayer2/ui/PlayerControlView;->V:I

    const/4 v5, 0x1

    :goto_1
    const/4 v6, 0x2

    if-gt v5, v6, :cond_e

    add-int v7, v0, v5

    rem-int/lit8 v7, v7, 0x3

    if-eqz v7, :cond_c

    if-eq v7, v3, :cond_a

    if-eq v7, v6, :cond_9

    goto :goto_2

    :cond_9
    and-int/lit8 v6, v2, 0x2

    if-eqz v6, :cond_b

    goto :goto_3

    :cond_a
    and-int/lit8 v6, v2, 0x1

    if-eqz v6, :cond_b

    goto :goto_3

    :cond_b
    :goto_2
    const/4 v6, 0x0

    goto :goto_4

    :cond_c
    :goto_3
    const/4 v6, 0x1

    :goto_4
    if-eqz v6, :cond_d

    move v0, v7

    goto :goto_5

    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_e
    :goto_5
    check-cast p1, Lf/h/a/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v0}, Lf/h/a/c/m0;->u(I)V

    goto :goto_6

    :cond_f
    iget-object v2, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->m:Landroid/widget/ImageView;

    if-ne v2, p1, :cond_10

    iget-object p1, v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->K:Lf/h/a/c/v;

    invoke-interface {v1}, Lf/h/a/c/m0;->C()Z

    move-result v0

    xor-int/2addr v0, v3

    check-cast p1, Lf/h/a/c/w;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v1, v0}, Lf/h/a/c/m0;->g(Z)V

    :cond_10
    :goto_6
    return-void
.end method

.method public p(Z)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    sget v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g0:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->q()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->m()V

    return-void
.end method

.method public s(ZI)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    sget p2, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g0:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->n()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->o()V

    return-void
.end method

.method public synthetic t(Lf/h/a/c/t0;Ljava/lang/Object;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lf/h/a/c/l0;->k(Lf/h/a/c/m0$a;Lf/h/a/c/t0;Ljava/lang/Object;I)V

    return-void
.end method

.method public u(I)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    sget v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;->g0:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->p()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerControlView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerControlView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->m()V

    return-void
.end method

.method public synthetic z(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/l0;->l(Lf/h/a/c/m0$a;Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V

    return-void
.end method
