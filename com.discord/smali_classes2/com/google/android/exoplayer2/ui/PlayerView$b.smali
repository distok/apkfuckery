.class public final Lcom/google/android/exoplayer2/ui/PlayerView$b;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Lf/h/a/c/m0$a;
.implements Lf/h/a/c/e1/j;
.implements Lf/h/a/c/j1/q;
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lf/h/a/c/g1/h/g;
.implements Lcom/google/android/exoplayer2/ui/PlayerControlView$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/PlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/android/exoplayer2/ui/PlayerView;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerView$a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic B(II)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/j1/p;->a(Lf/h/a/c/j1/q;II)V

    return-void
.end method

.method public synthetic C(Lf/h/a/c/j0;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->c(Lf/h/a/c/m0$a;Lf/h/a/c/j0;)V

    return-void
.end method

.method public synthetic D(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->a(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public synthetic a()V
    .locals 0

    invoke-static {p0}, Lf/h/a/c/l0;->h(Lf/h/a/c/m0$a;)V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    sget v0, Lcom/google/android/exoplayer2/ui/PlayerView;->D:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->l()V

    return-void
.end method

.method public c(IIIF)V
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    if-eqz p2, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    int-to-float p1, p1

    mul-float p1, p1, p4

    int-to-float p2, p2

    div-float/2addr p1, p2

    goto :goto_1

    :cond_1
    :goto_0
    const/high16 p1, 0x3f800000    # 1.0f

    :goto_1
    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-object p4, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->g:Landroid/view/View;

    instance-of v1, p4, Landroid/view/TextureView;

    if-eqz v1, :cond_6

    const/16 v1, 0x5a

    if-eq p3, v1, :cond_2

    const/16 v1, 0x10e

    if-ne p3, v1, :cond_3

    :cond_2
    div-float/2addr v0, p1

    move p1, v0

    :cond_3
    iget p2, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->B:I

    if-eqz p2, :cond_4

    invoke-virtual {p4, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_4
    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iput p3, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->B:I

    if-eqz p3, :cond_5

    iget-object p2, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->g:Landroid/view/View;

    invoke-virtual {p2, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_5
    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-object p3, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->g:Landroid/view/View;

    check-cast p3, Landroid/view/TextureView;

    iget p2, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->B:I

    invoke-static {p3, p2}, Lcom/google/android/exoplayer2/ui/PlayerView;->a(Landroid/view/TextureView;I)V

    :cond_6
    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-object p3, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->e:Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;

    iget-object p2, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->g:Landroid/view/View;

    if-eqz p3, :cond_8

    instance-of p2, p2, Lf/h/a/c/g1/h/h;

    if-eqz p2, :cond_7

    const/4 p1, 0x0

    :cond_7
    invoke-virtual {p3, p1}, Lcom/google/android/exoplayer2/ui/AspectRatioFrameLayout;->setAspectRatio(F)V

    :cond_8
    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-object v0, v0, Lcom/google/android/exoplayer2/ui/PlayerView;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public synthetic e(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->d(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public synthetic f(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->b(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public g(I)V
    .locals 1

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    sget v0, Lcom/google/android/exoplayer2/ui/PlayerView;->D:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->e()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-boolean v0, p1, Lcom/google/android/exoplayer2/ui/PlayerView;->z:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->d()V

    :cond_0
    return-void
.end method

.method public synthetic k(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->e(Lf/h/a/c/m0$a;Lcom/google/android/exoplayer2/ExoPlaybackException;)V

    return-void
.end method

.method public synthetic l(Lf/h/a/c/t0;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lf/h/a/c/l0;->j(Lf/h/a/c/m0$a;Lf/h/a/c/t0;I)V

    return-void
.end method

.method public m(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/a/c/e1/b;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-object v0, v0, Lcom/google/android/exoplayer2/ui/PlayerView;->i:Lcom/google/android/exoplayer2/ui/SubtitleView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer2/ui/SubtitleView;->setCues(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    check-cast p1, Landroid/view/TextureView;

    iget-object p2, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget p2, p2, Lcom/google/android/exoplayer2/ui/PlayerView;->B:I

    invoke-static {p1, p2}, Lcom/google/android/exoplayer2/ui/PlayerView;->a(Landroid/view/TextureView;I)V

    return-void
.end method

.method public synthetic p(Z)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->i(Lf/h/a/c/m0$a;Z)V

    return-void
.end method

.method public s(ZI)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    sget p2, Lcom/google/android/exoplayer2/ui/PlayerView;->D:I

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->k()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->m()V

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->e()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    iget-boolean p2, p1, Lcom/google/android/exoplayer2/ui/PlayerView;->z:Z

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->d()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/ui/PlayerView;->f(Z)V

    :goto_0
    return-void
.end method

.method public synthetic t(Lf/h/a/c/t0;Ljava/lang/Object;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lf/h/a/c/l0;->k(Lf/h/a/c/m0$a;Lf/h/a/c/t0;Ljava/lang/Object;I)V

    return-void
.end method

.method public synthetic u(I)V
    .locals 0

    invoke-static {p0, p1}, Lf/h/a/c/l0;->g(Lf/h/a/c/m0$a;I)V

    return-void
.end method

.method public z(Lcom/google/android/exoplayer2/source/TrackGroupArray;Lf/h/a/c/f1/g;)V
    .locals 0

    iget-object p1, p0, Lcom/google/android/exoplayer2/ui/PlayerView$b;->d:Lcom/google/android/exoplayer2/ui/PlayerView;

    sget p2, Lcom/google/android/exoplayer2/ui/PlayerView;->D:I

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer2/ui/PlayerView;->n(Z)V

    return-void
.end method
