.class public final Lcom/google/android/exoplayer2/ui/R$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final exo_controls_fastforward:I = 0x7f080203

.field public static final exo_controls_fullscreen_enter:I = 0x7f080204

.field public static final exo_controls_fullscreen_exit:I = 0x7f080205

.field public static final exo_controls_next:I = 0x7f080206

.field public static final exo_controls_pause:I = 0x7f080207

.field public static final exo_controls_play:I = 0x7f080208

.field public static final exo_controls_previous:I = 0x7f080209

.field public static final exo_controls_repeat_all:I = 0x7f08020a

.field public static final exo_controls_repeat_off:I = 0x7f08020b

.field public static final exo_controls_repeat_one:I = 0x7f08020c

.field public static final exo_controls_rewind:I = 0x7f08020d

.field public static final exo_controls_shuffle_off:I = 0x7f08020e

.field public static final exo_controls_shuffle_on:I = 0x7f08020f

.field public static final exo_controls_vr:I = 0x7f080210

.field public static final exo_edit_mode_logo:I = 0x7f080211

.field public static final exo_icon_circular_play:I = 0x7f080212

.field public static final exo_icon_fastforward:I = 0x7f080213

.field public static final exo_icon_fullscreen_enter:I = 0x7f080214

.field public static final exo_icon_fullscreen_exit:I = 0x7f080215

.field public static final exo_icon_next:I = 0x7f080216

.field public static final exo_icon_pause:I = 0x7f080217

.field public static final exo_icon_play:I = 0x7f080218

.field public static final exo_icon_previous:I = 0x7f080219

.field public static final exo_icon_repeat_all:I = 0x7f08021a

.field public static final exo_icon_repeat_off:I = 0x7f08021b

.field public static final exo_icon_repeat_one:I = 0x7f08021c

.field public static final exo_icon_rewind:I = 0x7f08021d

.field public static final exo_icon_shuffle_off:I = 0x7f08021e

.field public static final exo_icon_shuffle_on:I = 0x7f08021f

.field public static final exo_icon_stop:I = 0x7f080220

.field public static final exo_icon_vr:I = 0x7f080221

.field public static final exo_notification_fastforward:I = 0x7f080222

.field public static final exo_notification_next:I = 0x7f080223

.field public static final exo_notification_pause:I = 0x7f080224

.field public static final exo_notification_play:I = 0x7f080225

.field public static final exo_notification_previous:I = 0x7f080226

.field public static final exo_notification_rewind:I = 0x7f080227

.field public static final exo_notification_small_icon:I = 0x7f080228

.field public static final exo_notification_stop:I = 0x7f080229

.field public static final notification_action_background:I = 0x7f080565

.field public static final notification_bg:I = 0x7f080566

.field public static final notification_bg_low:I = 0x7f080567

.field public static final notification_bg_low_normal:I = 0x7f080568

.field public static final notification_bg_low_pressed:I = 0x7f080569

.field public static final notification_bg_normal:I = 0x7f08056a

.field public static final notification_bg_normal_pressed:I = 0x7f08056b

.field public static final notification_icon_background:I = 0x7f08056c

.field public static final notification_template_icon_bg:I = 0x7f08056d

.field public static final notification_template_icon_low_bg:I = 0x7f08056e

.field public static final notification_tile_bg:I = 0x7f08056f

.field public static final notify_panel_notification_icon_bg:I = 0x7f080570


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
