.class public Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;
.super Ljava/lang/Object;
.source "DefaultAudioSink.java"

# interfaces
.implements Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/audio/DefaultAudioSink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# instance fields
.field public final a:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

.field public final b:Lf/h/a/c/w0/v;

.field public final c:Lf/h/a/c/w0/x;


# direct methods
.method public varargs constructor <init>([Lcom/google/android/exoplayer2/audio/AudioProcessor;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->a:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Lf/h/a/c/w0/v;

    invoke-direct {v1}, Lf/h/a/c/w0/v;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->b:Lf/h/a/c/w0/v;

    new-instance v2, Lf/h/a/c/w0/x;

    invoke-direct {v2}, Lf/h/a/c/w0/x;-><init>()V

    iput-object v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->c:Lf/h/a/c/w0/x;

    array-length v3, p1

    aput-object v1, v0, v3

    array-length p1, p1

    add-int/lit8 p1, p1, 0x1

    aput-object v2, v0, p1

    return-void
.end method


# virtual methods
.method public a(Lf/h/a/c/j0;)Lf/h/a/c/j0;
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->b:Lf/h/a/c/w0/v;

    iget-boolean v1, p1, Lf/h/a/c/j0;->c:Z

    iput-boolean v1, v0, Lf/h/a/c/w0/v;->j:Z

    new-instance v0, Lf/h/a/c/j0;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->c:Lf/h/a/c/w0/x;

    iget v2, p1, Lf/h/a/c/j0;->a:F

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const v3, 0x3dcccccd    # 0.1f

    const/high16 v4, 0x41000000    # 8.0f

    invoke-static {v2, v3, v4}, Lf/h/a/c/i1/a0;->e(FFF)F

    move-result v2

    iget v5, v1, Lf/h/a/c/w0/x;->c:F

    const/4 v6, 0x1

    cmpl-float v5, v5, v2

    if-eqz v5, :cond_0

    iput v2, v1, Lf/h/a/c/w0/x;->c:F

    iput-boolean v6, v1, Lf/h/a/c/w0/x;->i:Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->c:Lf/h/a/c/w0/x;

    iget v5, p1, Lf/h/a/c/j0;->b:F

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v5, v3, v4}, Lf/h/a/c/i1/a0;->e(FFF)F

    move-result v3

    iget v4, v1, Lf/h/a/c/w0/x;->d:F

    cmpl-float v4, v4, v3

    if-eqz v4, :cond_1

    iput v3, v1, Lf/h/a/c/w0/x;->d:F

    iput-boolean v6, v1, Lf/h/a/c/w0/x;->i:Z

    :cond_1
    iget-boolean p1, p1, Lf/h/a/c/j0;->c:Z

    invoke-direct {v0, v2, v3, p1}, Lf/h/a/c/j0;-><init>(FFZ)V

    return-object v0
.end method

.method public b(J)J
    .locals 13

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->c:Lf/h/a/c/w0/x;

    iget-wide v5, v0, Lf/h/a/c/w0/x;->o:J

    const-wide/16 v1, 0x400

    cmp-long v3, v5, v1

    if-ltz v3, :cond_1

    iget-object v1, v0, Lf/h/a/c/w0/x;->h:Lcom/google/android/exoplayer2/audio/AudioProcessor$a;

    iget v1, v1, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->a:I

    iget-object v2, v0, Lf/h/a/c/w0/x;->g:Lcom/google/android/exoplayer2/audio/AudioProcessor$a;

    iget v2, v2, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->a:I

    if-ne v1, v2, :cond_0

    iget-wide v3, v0, Lf/h/a/c/w0/x;->n:J

    move-wide v1, p1

    invoke-static/range {v1 .. v6}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide p1

    goto :goto_0

    :cond_0
    iget-wide v3, v0, Lf/h/a/c/w0/x;->n:J

    int-to-long v0, v1

    mul-long v9, v3, v0

    int-to-long v0, v2

    mul-long v11, v5, v0

    move-wide v7, p1

    invoke-static/range {v7 .. v12}, Lf/h/a/c/i1/a0;->x(JJJ)J

    move-result-wide p1

    goto :goto_0

    :cond_1
    iget v0, v0, Lf/h/a/c/w0/x;->c:F

    float-to-double v0, v0

    long-to-double p1, p1

    mul-double v0, v0, p1

    double-to-long p1, v0

    :goto_0
    return-wide p1
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->b:Lf/h/a/c/w0/v;

    iget-wide v0, v0, Lf/h/a/c/w0/v;->q:J

    return-wide v0
.end method
