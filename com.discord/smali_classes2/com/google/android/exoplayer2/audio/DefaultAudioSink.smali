.class public final Lcom/google/android/exoplayer2/audio/DefaultAudioSink;
.super Ljava/lang/Object;
.source "DefaultAudioSink.java"

# interfaces
.implements Lcom/google/android/exoplayer2/audio/AudioSink;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;,
        Lcom/google/android/exoplayer2/audio/DefaultAudioSink$f;,
        Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;,
        Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;,
        Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;,
        Lcom/google/android/exoplayer2/audio/DefaultAudioSink$InvalidAudioTrackTimestampException;
    }
.end annotation


# instance fields
.field public A:J

.field public B:F

.field public C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

.field public D:[Ljava/nio/ByteBuffer;

.field public E:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public F:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public G:[B

.field public H:I

.field public I:I

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:I

.field public N:Lf/h/a/c/w0/o;

.field public O:Z

.field public P:J

.field public final a:Lf/h/a/c/w0/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;

.field public final c:Lf/h/a/c/w0/q;

.field public final d:Lf/h/a/c/w0/y;

.field public final e:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

.field public final f:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

.field public final g:Landroid/os/ConditionVariable;

.field public final h:Lf/h/a/c/w0/n;

.field public final i:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/google/android/exoplayer2/audio/AudioSink$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

.field public m:Landroid/media/AudioTrack;

.field public n:Lf/h/a/c/w0/i;

.field public o:Lf/h/a/c/j0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public p:Lf/h/a/c/j0;

.field public q:J

.field public r:J

.field public s:Ljava/nio/ByteBuffer;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public t:I

.field public u:J

.field public v:J

.field public w:J

.field public x:J

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>(Lf/h/a/c/w0/j;[Lcom/google/android/exoplayer2/audio/AudioProcessor;)V
    .locals 6
    .param p1    # Lf/h/a/c/w0/j;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;

    invoke-direct {v0, p2}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;-><init>([Lcom/google/android/exoplayer2/audio/AudioProcessor;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->a:Lf/h/a/c/w0/j;

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->b:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;

    new-instance p1, Landroid/os/ConditionVariable;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g:Landroid/os/ConditionVariable;

    new-instance p1, Lf/h/a/c/w0/n;

    new-instance v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$f;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$f;-><init>(Lcom/google/android/exoplayer2/audio/DefaultAudioSink;Lcom/google/android/exoplayer2/audio/DefaultAudioSink$a;)V

    invoke-direct {p1, v1}, Lf/h/a/c/w0/n;-><init>(Lf/h/a/c/w0/n$a;)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    new-instance p1, Lf/h/a/c/w0/q;

    invoke-direct {p1}, Lf/h/a/c/w0/q;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->c:Lf/h/a/c/w0/q;

    new-instance v1, Lf/h/a/c/w0/y;

    invoke-direct {v1}, Lf/h/a/c/w0/y;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d:Lf/h/a/c/w0/y;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Lf/h/a/c/w0/p;

    new-instance v4, Lf/h/a/c/w0/u;

    invoke-direct {v4}, Lf/h/a/c/w0/u;-><init>()V

    const/4 v5, 0x0

    aput-object v4, v3, v5

    aput-object p1, v3, p2

    const/4 p1, 0x2

    aput-object v1, v3, p1

    invoke-static {v2, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iget-object p1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$d;->a:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    invoke-static {v2, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-array p1, v5, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->e:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    new-array p1, p2, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    new-instance p2, Lf/h/a/c/w0/s;

    invoke-direct {p2}, Lf/h/a/c/w0/s;-><init>()V

    aput-object p2, p1, v5

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->f:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->B:F

    iput v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    sget-object p1, Lf/h/a/c/w0/i;->f:Lf/h/a/c/w0/i;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->n:Lf/h/a/c/w0/i;

    iput v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    new-instance p1, Lf/h/a/c/w0/o;

    const/4 p2, 0x0

    invoke-direct {p1, v5, p2}, Lf/h/a/c/w0/o;-><init>(IF)V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->N:Lf/h/a/c/w0/o;

    sget-object p1, Lf/h/a/c/j0;->e:Lf/h/a/c/j0;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    const/4 p1, -0x1

    iput p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    new-array p1, v5, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    new-array p1, v5, [Ljava/nio/ByteBuffer;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->D:[Ljava/nio/ByteBuffer;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    return-void
.end method


# virtual methods
.method public final a(Lf/h/a/c/j0;J)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->b:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$b;->a(Lf/h/a/c/j0;)Lf/h/a/c/j0;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object p1, Lf/h/a/c/j0;->e:Lf/h/a/c/j0;

    :goto_0
    move-object v1, p1

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    new-instance v7, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g()J

    move-result-wide v4

    invoke-virtual {p2, v4, v5}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a(J)J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;-><init>(Lf/h/a/c/j0;JJLcom/google/android/exoplayer2/audio/DefaultAudioSink$a;)V

    invoke-virtual {p1, v7}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-object p1, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->k:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    array-length p3, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p3, :cond_2

    aget-object v1, p1, v0

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->flush()V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p3, p1, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lcom/google/android/exoplayer2/audio/AudioProcessor;

    iput-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    new-array p1, p1, [Ljava/nio/ByteBuffer;

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->D:[Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->e()V

    return-void
.end method

.method public b(IIII[III)V
    .locals 19
    .param p5    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException;
        }
    .end annotation

    move-object/from16 v1, p0

    move/from16 v0, p1

    move/from16 v2, p2

    sget v3, Lf/h/a/c/i1/a0;->a:I

    const/16 v4, 0x8

    const/4 v5, 0x6

    const/16 v6, 0x15

    if-ge v3, v6, :cond_0

    if-ne v2, v4, :cond_0

    if-nez p5, :cond_0

    new-array v3, v5, [I

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aput v4, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v3, p5

    :cond_1
    invoke-static/range {p1 .. p1}, Lf/h/a/c/i1/a0;->s(I)Z

    move-result v8

    const/4 v4, 0x4

    const/4 v5, 0x1

    if-eqz v8, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v7, 0x1

    const/16 v16, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    const/16 v16, 0x0

    :goto_1
    iget-object v15, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->e:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    if-eqz v16, :cond_5

    iget-object v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d:Lf/h/a/c/w0/y;

    move/from16 v9, p6

    iput v9, v7, Lf/h/a/c/w0/y;->i:I

    move/from16 v9, p7

    iput v9, v7, Lf/h/a/c/w0/y;->j:I

    iget-object v7, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->c:Lf/h/a/c/w0/q;

    iput-object v3, v7, Lf/h/a/c/w0/q;->i:[I

    new-instance v3, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;

    move/from16 v10, p3

    invoke-direct {v3, v10, v2, v0}, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;-><init>(III)V

    array-length v7, v15

    const/4 v9, 0x0

    move-object v9, v3

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v7, :cond_4

    aget-object v3, v15, v11

    :try_start_0
    invoke-interface {v3, v9}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->c(Lcom/google/android/exoplayer2/audio/AudioProcessor$a;)Lcom/google/android/exoplayer2/audio/AudioProcessor$a;

    move-result-object v12
    :try_end_0
    .catch Lcom/google/android/exoplayer2/audio/AudioProcessor$UnhandledAudioFormatException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-interface {v3}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v9, v12

    :cond_3
    add-int/lit8 v11, v11, 0x1

    move-object v3, v12

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v2, v0

    new-instance v0, Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException;

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_4
    iget v7, v3, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->a:I

    iget v9, v3, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->b:I

    iget v3, v3, Lcom/google/android/exoplayer2/audio/AudioProcessor$a;->c:I

    move v14, v3

    move v12, v7

    goto :goto_3

    :cond_5
    move/from16 v10, p3

    move v14, v0

    move v9, v2

    move v12, v10

    :goto_3
    sget v3, Lf/h/a/c/i1/a0;->a:I

    const/16 v7, 0x1c

    if-gt v3, v7, :cond_8

    if-nez v8, :cond_8

    const/4 v7, 0x7

    if-ne v9, v7, :cond_6

    const/16 v4, 0x8

    goto :goto_4

    :cond_6
    const/4 v7, 0x3

    if-eq v9, v7, :cond_7

    if-eq v9, v4, :cond_7

    const/4 v4, 0x5

    if-ne v9, v4, :cond_8

    :cond_7
    const/4 v4, 0x6

    goto :goto_4

    :cond_8
    move v4, v9

    :goto_4
    const/16 v7, 0x1a

    if-gt v3, v7, :cond_9

    sget-object v7, Lf/h/a/c/i1/a0;->b:Ljava/lang/String;

    const-string v11, "fugu"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    if-nez v8, :cond_9

    if-ne v4, v5, :cond_9

    const/4 v4, 0x2

    :cond_9
    packed-switch v4, :pswitch_data_0

    goto :goto_6

    :pswitch_0
    const/16 v4, 0x17

    if-lt v3, v4, :cond_a

    goto :goto_5

    :cond_a
    if-lt v3, v6, :cond_b

    :goto_5
    const/16 v3, 0x18fc

    const/16 v13, 0x18fc

    goto :goto_7

    :pswitch_1
    const/16 v3, 0x4fc

    const/16 v13, 0x4fc

    goto :goto_7

    :pswitch_2
    const/16 v3, 0xfc

    const/16 v13, 0xfc

    goto :goto_7

    :pswitch_3
    const/16 v3, 0xdc

    const/16 v13, 0xdc

    goto :goto_7

    :pswitch_4
    const/16 v3, 0xcc

    const/16 v13, 0xcc

    goto :goto_7

    :pswitch_5
    const/16 v3, 0x1c

    const/16 v13, 0x1c

    goto :goto_7

    :pswitch_6
    const/16 v3, 0xc

    const/16 v13, 0xc

    goto :goto_7

    :pswitch_7
    const/4 v3, 0x4

    const/4 v13, 0x4

    goto :goto_7

    :cond_b
    :goto_6
    const/4 v3, 0x0

    const/4 v13, 0x0

    :goto_7
    if-eqz v13, :cond_10

    if-eqz v8, :cond_c

    invoke-static/range {p1 .. p2}, Lf/h/a/c/i1/a0;->l(II)I

    move-result v0

    goto :goto_8

    :cond_c
    const/4 v0, -0x1

    :goto_8
    if-eqz v8, :cond_d

    invoke-static {v14, v9}, Lf/h/a/c/i1/a0;->l(II)I

    move-result v2

    move v11, v2

    goto :goto_9

    :cond_d
    const/4 v2, -0x1

    const/4 v11, -0x1

    :goto_9
    if-eqz v16, :cond_e

    const/4 v2, 0x1

    const/16 v17, 0x1

    goto :goto_a

    :cond_e
    const/4 v2, 0x0

    const/16 v17, 0x0

    :goto_a
    new-instance v2, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    move-object v7, v2

    move v9, v0

    move/from16 v10, p3

    move-object v0, v15

    move/from16 v15, p4

    move-object/from16 v18, v0

    invoke-direct/range {v7 .. v18}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;-><init>(ZIIIIIIIZZ[Lcom/google/android/exoplayer2/audio/AudioProcessor;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v0

    if-eqz v0, :cond_f

    iput-object v2, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    goto :goto_b

    :cond_f
    iput-object v2, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    :goto_b
    return-void

    :cond_10
    new-instance v0, Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException;

    const-string v2, "Unsupported channel count: "

    invoke-static {v2, v9}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer2/audio/AudioSink$ConfigurationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$WriteException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v0, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v0, v0

    :goto_0
    iput v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    :goto_1
    const/4 v0, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v6, v5

    const-wide v7, -0x7fffffffffffffffL    # -4.9E-324

    if-ge v4, v6, :cond_4

    aget-object v4, v5, v4

    if-eqz v0, :cond_2

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->d()V

    :cond_2
    invoke-virtual {p0, v7, v8}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m(J)V

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->g()Z

    move-result v0

    if-nez v0, :cond_3

    return v3

    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->F:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_5

    invoke-virtual {p0, v0, v7, v8}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q(Ljava/nio/ByteBuffer;J)V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->F:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_5

    return v3

    :cond_5
    iput v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    return v2
.end method

.method public d()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->u:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->v:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->w:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->x:J

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->y:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    iput-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;

    iget-object v3, v3, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;->a:Lf/h/a/c/j0;

    iput-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->clear()V

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q:J

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->r:J

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d:Lf/h/a/c/w0/y;

    iput-wide v0, v3, Lf/h/a/c/w0/y;->o:J

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->e()V

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->F:Ljava/nio/ByteBuffer;

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->K:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->J:Z

    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->I:I

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    iget-object v3, v3, Lf/h/a/c/w0/n;->c:Landroid/media/AudioTrack;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v3

    const/4 v5, 0x3

    if-ne v3, v5, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->pause()V

    :cond_3
    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    if-eqz v5, :cond_4

    iput-object v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    :cond_4
    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    iput-wide v0, v5, Lf/h/a/c/w0/n;->j:J

    iput v2, v5, Lf/h/a/c/w0/n;->u:I

    iput v2, v5, Lf/h/a/c/w0/n;->t:I

    iput-wide v0, v5, Lf/h/a/c/w0/n;->k:J

    iput-object v4, v5, Lf/h/a/c/w0/n;->c:Landroid/media/AudioTrack;

    iput-object v4, v5, Lf/h/a/c/w0/n;->f:Lf/h/a/c/w0/m;

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    new-instance v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$a;

    invoke-direct {v0, p0, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$a;-><init>(Lcom/google/android/exoplayer2/audio/DefaultAudioSink;Landroid/media/AudioTrack;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_5
    return-void
.end method

.method public final e()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->flush()V

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->D:[Ljava/nio/ByteBuffer;

    invoke-interface {v1}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->getOutput()Ljava/nio/ByteBuffer;

    move-result-object v1

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f()Lf/h/a/c/j0;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;

    iget-object v0, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$e;->a:Lf/h/a/c/j0;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    :goto_0
    return-object v0
.end method

.method public final g()J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a:Z

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->w:J

    iget v0, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->d:I

    int-to-long v3, v0

    div-long/2addr v1, v3

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->x:J

    :goto_0
    return-wide v1
.end method

.method public h(Ljava/nio/ByteBuffer;J)Z
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$InitializationException;,
            Lcom/google/android/exoplayer2/audio/AudioSink$WriteException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_1

    if-ne v1, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Lf/g/j/k/a;->d(Z)V

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    const/4 v7, 0x0

    if-eqz v4, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->c()Z

    move-result v4

    if-nez v4, :cond_2

    return v6

    :cond_2
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-object v8, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget v9, v8, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    if-ne v9, v10, :cond_3

    iget v9, v8, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->e:I

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->e:I

    if-ne v9, v10, :cond_3

    iget v8, v8, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->f:I

    iget v4, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->f:I

    if-ne v8, v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-nez v4, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->i()Z

    move-result v4

    if-eqz v4, :cond_4

    return v6

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    goto :goto_3

    :cond_5
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iput-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iput-object v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    :goto_3
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->a(Lf/h/a/c/j0;J)V

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v4

    const/4 v6, 0x3

    const/4 v7, 0x6

    const/4 v8, 0x5

    const/16 v9, 0x10

    if-nez v4, :cond_12

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g:Landroid/os/ConditionVariable;

    invoke-virtual {v4}, Landroid/os/ConditionVariable;->block()V

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v10, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    iget-object v11, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->n:Lf/h/a/c/w0/i;

    iget v15, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    sget v14, Lf/h/a/c/i1/a0;->a:I

    const/16 v12, 0x15

    if-lt v14, v12, :cond_9

    if-eqz v10, :cond_7

    new-instance v10, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v10}, Landroid/media/AudioAttributes$Builder;-><init>()V

    invoke-virtual {v10, v6}, Landroid/media/AudioAttributes$Builder;->setContentType(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/media/AudioAttributes$Builder;->setFlags(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v9

    goto :goto_4

    :cond_7
    invoke-virtual {v11}, Lf/h/a/c/w0/i;->a()Landroid/media/AudioAttributes;

    move-result-object v9

    :goto_4
    move-object/from16 v17, v9

    new-instance v9, Landroid/media/AudioFormat$Builder;

    invoke-direct {v9}, Landroid/media/AudioFormat$Builder;-><init>()V

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->f:I

    invoke-virtual {v9, v10}, Landroid/media/AudioFormat$Builder;->setChannelMask(I)Landroid/media/AudioFormat$Builder;

    move-result-object v9

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    invoke-virtual {v9, v10}, Landroid/media/AudioFormat$Builder;->setEncoding(I)Landroid/media/AudioFormat$Builder;

    move-result-object v9

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->e:I

    invoke-virtual {v9, v10}, Landroid/media/AudioFormat$Builder;->setSampleRate(I)Landroid/media/AudioFormat$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/media/AudioFormat$Builder;->build()Landroid/media/AudioFormat;

    move-result-object v18

    new-instance v9, Landroid/media/AudioTrack;

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->h:I

    const/16 v20, 0x1

    if-eqz v15, :cond_8

    move/from16 v21, v15

    goto :goto_5

    :cond_8
    const/4 v15, 0x0

    const/16 v21, 0x0

    :goto_5
    move-object/from16 v16, v9

    move/from16 v19, v10

    invoke-direct/range {v16 .. v21}, Landroid/media/AudioTrack;-><init>(Landroid/media/AudioAttributes;Landroid/media/AudioFormat;III)V

    goto :goto_6

    :cond_9
    iget v9, v11, Lf/h/a/c/w0/i;->c:I

    invoke-static {v9}, Lf/h/a/c/i1/a0;->m(I)I

    move-result v17

    if-nez v15, :cond_a

    new-instance v9, Landroid/media/AudioTrack;

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->e:I

    iget v11, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->f:I

    iget v12, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    iget v13, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->h:I

    const/16 v22, 0x1

    move-object/from16 v16, v9

    move/from16 v18, v10

    move/from16 v19, v11

    move/from16 v20, v12

    move/from16 v21, v13

    invoke-direct/range {v16 .. v22}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    :goto_6
    move v6, v14

    goto :goto_7

    :cond_a
    new-instance v9, Landroid/media/AudioTrack;

    iget v10, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->e:I

    iget v11, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->f:I

    iget v13, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    iget v12, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->h:I

    const/16 v18, 0x1

    move/from16 v19, v12

    move-object v12, v9

    move/from16 v16, v13

    move/from16 v13, v17

    move v6, v14

    move v14, v10

    move v10, v15

    move v15, v11

    move/from16 v17, v19

    move/from16 v19, v10

    invoke-direct/range {v12 .. v19}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    :goto_7
    invoke-virtual {v9}, Landroid/media/AudioTrack;->getState()I

    move-result v10

    if-ne v10, v5, :cond_11

    iput-object v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v4

    iget v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    if-eq v9, v4, :cond_c

    iput v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    iget-object v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j:Lcom/google/android/exoplayer2/audio/AudioSink$a;

    if-eqz v9, :cond_c

    check-cast v9, Lf/h/a/c/w0/t$b;

    iget-object v10, v9, Lf/h/a/c/w0/t$b;->a:Lf/h/a/c/w0/t;

    iget-object v10, v10, Lf/h/a/c/w0/t;->w0:Lf/h/a/c/w0/l$a;

    iget-object v11, v10, Lf/h/a/c/w0/l$a;->a:Landroid/os/Handler;

    if-eqz v11, :cond_b

    new-instance v12, Lf/h/a/c/w0/c;

    invoke-direct {v12, v10, v4}, Lf/h/a/c/w0/c;-><init>(Lf/h/a/c/w0/l$a;I)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_b
    iget-object v4, v9, Lf/h/a/c/w0/t$b;->a:Lf/h/a/c/w0/t;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->p:Lf/h/a/c/j0;

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->a(Lf/h/a/c/j0;J)V

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    iget-object v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iget-object v10, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget v11, v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    iget v12, v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->d:I

    iget v10, v10, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->h:I

    iput-object v9, v4, Lf/h/a/c/w0/n;->c:Landroid/media/AudioTrack;

    iput v12, v4, Lf/h/a/c/w0/n;->d:I

    iput v10, v4, Lf/h/a/c/w0/n;->e:I

    new-instance v13, Lf/h/a/c/w0/m;

    invoke-direct {v13, v9}, Lf/h/a/c/w0/m;-><init>(Landroid/media/AudioTrack;)V

    iput-object v13, v4, Lf/h/a/c/w0/n;->f:Lf/h/a/c/w0/m;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v9

    iput v9, v4, Lf/h/a/c/w0/n;->g:I

    const/16 v9, 0x17

    if-ge v6, v9, :cond_e

    if-eq v11, v8, :cond_d

    if-ne v11, v7, :cond_e

    :cond_d
    const/4 v6, 0x1

    goto :goto_8

    :cond_e
    const/4 v6, 0x0

    :goto_8
    iput-boolean v6, v4, Lf/h/a/c/w0/n;->h:Z

    invoke-static {v11}, Lf/h/a/c/i1/a0;->s(I)Z

    move-result v6

    iput-boolean v6, v4, Lf/h/a/c/w0/n;->o:Z

    if-eqz v6, :cond_f

    div-int/2addr v10, v12

    int-to-long v9, v10

    invoke-virtual {v4, v9, v10}, Lf/h/a/c/w0/n;->a(J)J

    move-result-wide v9

    goto :goto_9

    :cond_f
    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    :goto_9
    iput-wide v9, v4, Lf/h/a/c/w0/n;->i:J

    const-wide/16 v9, 0x0

    iput-wide v9, v4, Lf/h/a/c/w0/n;->q:J

    iput-wide v9, v4, Lf/h/a/c/w0/n;->r:J

    iput-wide v9, v4, Lf/h/a/c/w0/n;->s:J

    const/4 v6, 0x0

    iput-boolean v6, v4, Lf/h/a/c/w0/n;->n:Z

    const-wide v11, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v11, v4, Lf/h/a/c/w0/n;->v:J

    iput-wide v11, v4, Lf/h/a/c/w0/n;->w:J

    iput-wide v9, v4, Lf/h/a/c/w0/n;->m:J

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o()V

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->N:Lf/h/a/c/w0/o;

    iget v4, v4, Lf/h/a/c/w0/o;->a:I

    if-eqz v4, :cond_10

    iget-object v6, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v6, v4}, Landroid/media/AudioTrack;->attachAuxEffect(I)I

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iget-object v6, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->N:Lf/h/a/c/w0/o;

    iget v6, v6, Lf/h/a/c/w0/o;->b:F

    invoke-virtual {v4, v6}, Landroid/media/AudioTrack;->setAuxEffectSendLevel(F)I

    :cond_10
    iget-boolean v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->L:Z

    if-eqz v4, :cond_12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->k()V

    goto :goto_a

    :cond_11
    :try_start_0
    invoke-virtual {v9}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    new-instance v1, Lcom/google/android/exoplayer2/audio/AudioSink$InitializationException;

    iget v2, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->e:I

    iget v3, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->f:I

    iget v4, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->h:I

    invoke-direct {v1, v10, v2, v3, v4}, Lcom/google/android/exoplayer2/audio/AudioSink$InitializationException;-><init>(IIII)V

    throw v1

    :cond_12
    :goto_a
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g()J

    move-result-wide v9

    iget-object v6, v4, Lf/h/a/c/w0/n;->c:Landroid/media/AudioTrack;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v6

    iget-boolean v11, v4, Lf/h/a/c/w0/n;->h:Z

    const/4 v12, 0x2

    if-eqz v11, :cond_14

    if-ne v6, v12, :cond_13

    const/4 v6, 0x0

    iput-boolean v6, v4, Lf/h/a/c/w0/n;->n:Z

    goto :goto_b

    :cond_13
    if-ne v6, v5, :cond_14

    invoke-virtual {v4}, Lf/h/a/c/w0/n;->b()J

    move-result-wide v13

    const-wide/16 v15, 0x0

    cmp-long v11, v13, v15

    if-nez v11, :cond_14

    :goto_b
    const/4 v4, 0x0

    goto :goto_c

    :cond_14
    iget-boolean v11, v4, Lf/h/a/c/w0/n;->n:Z

    invoke-virtual {v4, v9, v10}, Lf/h/a/c/w0/n;->c(J)Z

    move-result v9

    iput-boolean v9, v4, Lf/h/a/c/w0/n;->n:Z

    if-eqz v11, :cond_15

    if-nez v9, :cond_15

    if-eq v6, v5, :cond_15

    iget-object v6, v4, Lf/h/a/c/w0/n;->a:Lf/h/a/c/w0/n$a;

    if-eqz v6, :cond_15

    iget v9, v4, Lf/h/a/c/w0/n;->e:I

    iget-wide v10, v4, Lf/h/a/c/w0/n;->i:J

    invoke-static {v10, v11}, Lf/h/a/c/u;->b(J)J

    move-result-wide v10

    invoke-interface {v6, v9, v10, v11}, Lf/h/a/c/w0/n$a;->a(IJ)V

    :cond_15
    const/4 v4, 0x1

    :goto_c
    if-nez v4, :cond_16

    const/4 v1, 0x0

    return v1

    :cond_16
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    const-string v6, "AudioTrack"

    if-nez v4, :cond_33

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-nez v4, :cond_17

    return v5

    :cond_17
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v9, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a:Z

    if-nez v9, :cond_2b

    iget v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->y:I

    if-nez v9, :cond_2b

    iget v4, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->g:I

    const/16 v9, 0xe

    const/4 v10, -0x1

    if-eq v4, v9, :cond_24

    const/16 v9, 0x11

    if-eq v4, v9, :cond_23

    const/16 v9, 0x12

    const/16 v11, 0xa

    if-eq v4, v9, :cond_1f

    packed-switch v4, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected audio encoding: "

    invoke-static {v2, v4}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    invoke-static {v4}, Lf/h/a/c/a1/o;->c(I)Z

    move-result v7

    if-nez v7, :cond_18

    goto/16 :goto_17

    :cond_18
    ushr-int/lit8 v7, v4, 0x13

    const/4 v8, 0x3

    and-int/2addr v7, v8

    if-ne v7, v5, :cond_19

    goto/16 :goto_17

    :cond_19
    ushr-int/lit8 v9, v4, 0x11

    and-int/2addr v9, v8

    if-nez v9, :cond_1a

    goto/16 :goto_17

    :cond_1a
    ushr-int/lit8 v13, v4, 0xc

    const/16 v14, 0xf

    and-int/2addr v13, v14

    ushr-int/2addr v4, v11

    and-int/2addr v4, v8

    if-eqz v13, :cond_2a

    if-eq v13, v14, :cond_2a

    if-ne v4, v8, :cond_1b

    goto/16 :goto_17

    :cond_1b
    invoke-static {v7, v9}, Lf/h/a/c/a1/o;->b(II)I

    move-result v10

    goto/16 :goto_17

    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    const/4 v11, -0x2

    if-eq v9, v11, :cond_1e

    if-eq v9, v10, :cond_1d

    const/16 v10, 0x1f

    if-eq v9, v10, :cond_1c

    add-int/lit8 v9, v4, 0x4

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    and-int/2addr v9, v5

    shl-int/lit8 v7, v9, 0x6

    add-int/2addr v4, v8

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    goto :goto_e

    :cond_1c
    add-int/lit8 v8, v4, 0x5

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    and-int/lit8 v8, v8, 0x7

    shl-int/lit8 v8, v8, 0x4

    add-int/2addr v4, v7

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    goto :goto_d

    :cond_1d
    add-int/lit8 v7, v4, 0x4

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    and-int/lit8 v7, v7, 0x7

    shl-int/lit8 v8, v7, 0x4

    add-int/lit8 v4, v4, 0x7

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    :goto_d
    and-int/lit8 v4, v4, 0x3c

    goto :goto_f

    :cond_1e
    add-int/lit8 v8, v4, 0x5

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    and-int/2addr v8, v5

    shl-int/lit8 v7, v8, 0x6

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    :goto_e
    and-int/lit16 v4, v4, 0xfc

    move v8, v7

    :goto_f
    shr-int/2addr v4, v12

    or-int/2addr v4, v8

    add-int/2addr v4, v5

    mul-int/lit8 v10, v4, 0x20

    goto/16 :goto_17

    :cond_1f
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v8

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xf8

    const/4 v8, 0x3

    shr-int/2addr v4, v8

    if-le v4, v11, :cond_20

    const/4 v4, 0x1

    goto :goto_10

    :cond_20
    const/4 v4, 0x0

    :goto_10
    if-eqz v4, :cond_22

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xc0

    shr-int/2addr v4, v7

    const/4 v7, 0x3

    if-ne v4, v7, :cond_21

    const/4 v4, 0x3

    goto :goto_11

    :cond_21
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit8 v4, v4, 0x30

    shr-int/lit8 v4, v4, 0x4

    :goto_11
    sget-object v7, Lf/h/a/c/w0/g;->a:[I

    aget v4, v7, v4

    mul-int/lit16 v4, v4, 0x100

    :goto_12
    move v10, v4

    goto/16 :goto_17

    :cond_22
    const/16 v10, 0x600

    goto/16 :goto_17

    :cond_23
    const/16 v4, 0x10

    new-array v4, v4, [B

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    new-instance v7, Lf/h/a/c/i1/q;

    invoke-direct {v7, v4}, Lf/h/a/c/i1/q;-><init>([B)V

    invoke-static {v7}, Lf/h/a/c/w0/h;->b(Lf/h/a/c/i1/q;)Lf/h/a/c/w0/h$b;

    move-result-object v4

    iget v10, v4, Lf/h/a/c/w0/h$b;->c:I

    goto :goto_17

    :cond_24
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v7

    add-int/lit8 v7, v7, -0xa

    move v8, v4

    :goto_13
    if-gt v8, v7, :cond_26

    add-int/lit8 v9, v8, 0x4

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v9

    const v11, -0x1000001

    and-int/2addr v9, v11

    const v11, -0x45908d08

    if-ne v9, v11, :cond_25

    sub-int/2addr v8, v4

    goto :goto_14

    :cond_25
    add-int/lit8 v8, v8, 0x1

    goto :goto_13

    :cond_26
    const/4 v8, -0x1

    :goto_14
    if-ne v8, v10, :cond_27

    const/4 v4, 0x0

    const/4 v10, 0x0

    goto :goto_17

    :cond_27
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v8

    add-int/lit8 v4, v4, 0x7

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    const/16 v7, 0xbb

    if-ne v4, v7, :cond_28

    const/4 v4, 0x1

    goto :goto_15

    :cond_28
    const/4 v4, 0x0

    :goto_15
    const/16 v7, 0x28

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v9

    add-int/2addr v9, v8

    if-eqz v4, :cond_29

    const/16 v4, 0x9

    goto :goto_16

    :cond_29
    const/16 v4, 0x8

    :goto_16
    add-int/2addr v9, v4

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    shr-int/lit8 v4, v4, 0x4

    and-int/lit8 v4, v4, 0x7

    shl-int v4, v7, v4

    mul-int/lit8 v4, v4, 0x10

    goto :goto_12

    :cond_2a
    :goto_17
    iput v10, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->y:I

    if-nez v10, :cond_2b

    return v5

    :cond_2b
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    if-eqz v4, :cond_2d

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->c()Z

    move-result v4

    if-nez v4, :cond_2c

    const/4 v1, 0x0

    return v1

    :cond_2c
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    const/4 v7, 0x0

    iput-object v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->o:Lf/h/a/c/j0;

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->a(Lf/h/a/c/j0;J)V

    :cond_2d
    iget v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    if-nez v4, :cond_2e

    const-wide/16 v7, 0x0

    invoke-static {v7, v8, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    iput-wide v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->A:J

    iput v5, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    goto :goto_19

    :cond_2e
    iget-wide v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->A:J

    iget-object v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v10, v9, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a:Z

    if-eqz v10, :cond_2f

    iget-wide v10, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->u:J

    iget v13, v9, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->b:I

    int-to-long v13, v13

    div-long/2addr v10, v13

    goto :goto_18

    :cond_2f
    iget-wide v10, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->v:J

    :goto_18
    iget-object v13, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d:Lf/h/a/c/w0/y;

    iget-wide v13, v13, Lf/h/a/c/w0/y;->o:J

    sub-long/2addr v10, v13

    const-wide/32 v13, 0xf4240

    mul-long v10, v10, v13

    iget v9, v9, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->c:I

    int-to-long v13, v9

    div-long/2addr v10, v13

    add-long/2addr v10, v7

    if-ne v4, v5, :cond_30

    sub-long v7, v10, v2

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(J)J

    move-result-wide v7

    const-wide/32 v13, 0x30d40

    cmp-long v4, v7, v13

    if-lez v4, :cond_30

    const-string v4, "Discontinuity detected [expected "

    const-string v7, ", got "

    invoke-static {v4, v10, v11, v7}, Lf/e/c/a/a;->K(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput v12, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    :cond_30
    iget v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    if-ne v4, v12, :cond_31

    sub-long v7, v2, v10

    iget-wide v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->A:J

    add-long/2addr v9, v7

    iput-wide v9, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->A:J

    iput v5, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->z:I

    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j:Lcom/google/android/exoplayer2/audio/AudioSink$a;

    if-eqz v4, :cond_31

    const-wide/16 v9, 0x0

    cmp-long v11, v7, v9

    if-eqz v11, :cond_31

    check-cast v4, Lf/h/a/c/w0/t$b;

    iget-object v7, v4, Lf/h/a/c/w0/t$b;->a:Lf/h/a/c/w0/t;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v4, Lf/h/a/c/w0/t$b;->a:Lf/h/a/c/w0/t;

    iput-boolean v5, v4, Lf/h/a/c/w0/t;->H0:Z

    :cond_31
    :goto_19
    iget-object v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v4, v4, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a:Z

    if-eqz v4, :cond_32

    iget-wide v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->u:J

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    int-to-long v9, v4

    add-long/2addr v7, v9

    iput-wide v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->u:J

    goto :goto_1a

    :cond_32
    iget-wide v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->v:J

    iget v4, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->y:I

    int-to-long v9, v4

    add-long/2addr v7, v9

    iput-wide v7, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->v:J

    :goto_1a
    iput-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    :cond_33
    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean v1, v1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->i:Z

    if-eqz v1, :cond_34

    invoke-virtual {v0, v2, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m(J)V

    goto :goto_1b

    :cond_34
    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q(Ljava/nio/ByteBuffer;J)V

    :goto_1b
    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-nez v1, :cond_35

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    return v5

    :cond_35
    iget-object v1, v0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g()J

    move-result-wide v2

    iget-wide v7, v1, Lf/h/a/c/w0/n;->w:J

    const-wide v9, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v4, v7, v9

    if-eqz v4, :cond_36

    const-wide/16 v7, 0x0

    cmp-long v4, v2, v7

    if-lez v4, :cond_36

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v7, v1, Lf/h/a/c/w0/n;->w:J

    sub-long/2addr v2, v7

    const-wide/16 v7, 0xc8

    cmp-long v1, v2, v7

    if-ltz v1, :cond_36

    const/4 v1, 0x1

    goto :goto_1c

    :cond_36
    const/4 v1, 0x0

    :goto_1c
    if-eqz v1, :cond_37

    const-string v1, "Resetting stalled audio track"

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    return v5

    :cond_37
    const/4 v1, 0x0

    return v1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public i()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lf/h/a/c/w0/n;->c(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->L:Z

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    iget-object v0, v0, Lf/h/a/c/w0/n;->f:Lf/h/a/c/w0/m;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lf/h/a/c/w0/m;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 7

    iget-boolean v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->K:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->K:Z

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->g()J

    move-result-wide v1

    invoke-virtual {v0}, Lf/h/a/c/w0/n;->b()J

    move-result-wide v3

    iput-wide v3, v0, Lf/h/a/c/w0/n;->x:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    iput-wide v3, v0, Lf/h/a/c/w0/n;->v:J

    iput-wide v1, v0, Lf/h/a/c/w0/n;->y:J

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    :cond_0
    return-void
.end method

.method public final m(J)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$WriteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v0, v0

    move v1, v0

    :goto_0
    if-ltz v1, :cond_5

    if-lez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->D:[Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v1, -0x1

    aget-object v2, v2, v3

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->E:Ljava/nio/ByteBuffer;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/google/android/exoplayer2/audio/AudioProcessor;->a:Ljava/nio/ByteBuffer;

    :goto_1
    if-ne v1, v0, :cond_2

    invoke-virtual {p0, v2, p1, p2}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->q(Ljava/nio/ByteBuffer;J)V

    goto :goto_2

    :cond_2
    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->C:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    aget-object v3, v3, v1

    invoke-interface {v3, v2}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->b(Ljava/nio/ByteBuffer;)V

    invoke-interface {v3}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->getOutput()Ljava/nio/ByteBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->D:[Ljava/nio/ByteBuffer;

    aput-object v3, v4, v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_4

    return-void

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public n()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->d()V

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->e:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->reset()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->f:[Lcom/google/android/exoplayer2/audio/AudioProcessor;

    array-length v1, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    invoke-interface {v4}, Lcom/google/android/exoplayer2/audio/AudioProcessor;->reset()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->M:I

    iput-boolean v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->L:Z

    return-void
.end method

.method public final o()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->j()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lf/h/a/c/i1/a0;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->B:F

    invoke-virtual {v0, v1}, Landroid/media/AudioTrack;->setVolume(F)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->B:F

    invoke-virtual {v0, v1, v1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    :goto_0
    return-void
.end method

.method public p(II)Z
    .locals 3

    invoke-static {p2}, Lf/h/a/c/i1/a0;->s(I)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    const/4 p1, 0x4

    if-ne p2, p1, :cond_1

    sget p1, Lf/h/a/c/i1/a0;->a:I

    const/16 p2, 0x15

    if-lt p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->a:Lf/h/a/c/w0/j;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lf/h/a/c/w0/j;->a:[I

    invoke-static {v0, p2}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p2

    if-ltz p2, :cond_3

    const/4 p2, 0x1

    goto :goto_1

    :cond_3
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_4

    const/4 p2, -0x1

    if-eq p1, p2, :cond_5

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->a:Lf/h/a/c/w0/j;

    iget p2, p2, Lf/h/a/c/w0/j;->b:I

    if-gt p1, p2, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :cond_5
    :goto_2
    return v1
.end method

.method public final q(Ljava/nio/ByteBuffer;J)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer2/audio/AudioSink$WriteException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->F:Ljava/nio/ByteBuffer;

    const/16 v1, 0x15

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lf/g/j/k/a;->d(Z)V

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->F:Ljava/nio/ByteBuffer;

    sget v0, Lf/h/a/c/i1/a0;->a:I

    if-ge v0, v1, :cond_5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->G:[B

    if-eqz v4, :cond_3

    array-length v4, v4

    if-ge v4, v0, :cond_4

    :cond_3
    new-array v4, v0, [B

    iput-object v4, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->G:[B

    :cond_4
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->G:[B

    invoke-virtual {p1, v5, v2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    invoke-virtual {p1, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->H:I

    :cond_5
    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    sget v4, Lf/h/a/c/i1/a0;->a:I

    if-ge v4, v1, :cond_6

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->h:Lf/h/a/c/w0/n;

    iget-wide v3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->w:J

    invoke-virtual {p2}, Lf/h/a/c/w0/n;->b()J

    move-result-wide v5

    iget p3, p2, Lf/h/a/c/w0/n;->d:I

    int-to-long v7, p3

    mul-long v5, v5, v7

    sub-long/2addr v3, v5

    long-to-int p3, v3

    iget p2, p2, Lf/h/a/c/w0/n;->e:I

    sub-int/2addr p2, p3

    if-lez p2, :cond_f

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->G:[B

    iget v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->H:I

    invoke-virtual {p3, v1, v2, p2}, Landroid/media/AudioTrack;->write([BII)I

    move-result v2

    if-lez v2, :cond_f

    iget p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->H:I

    add-int/2addr p2, v2

    iput p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->H:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result p2

    add-int/2addr p2, v2

    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_4

    :cond_6
    iget-boolean v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->O:Z

    if-eqz v1, :cond_e

    const-wide v5, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v1, p2, v5

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    :goto_2
    invoke-static {v1}, Lf/g/j/k/a;->s(Z)V

    iget-object v6, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    const-wide/16 v7, 0x3e8

    const/16 v1, 0x1a

    if-lt v4, v1, :cond_8

    const/4 v9, 0x1

    mul-long v10, p2, v7

    move-object v7, p1

    move v8, v0

    invoke-virtual/range {v6 .. v11}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;IIJ)I

    move-result v2

    goto :goto_4

    :cond_8
    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_9

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    sget-object v4, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    const v4, 0x55550001

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    :cond_9
    iget v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    const/4 v4, 0x4

    invoke-virtual {v1, v4, v0}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    const/16 v4, 0x8

    mul-long p2, p2, v7

    invoke-virtual {v1, v4, p2, p3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iput v0, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    :cond_a
    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result p2

    if-lez p2, :cond_c

    iget-object p3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->s:Ljava/nio/ByteBuffer;

    invoke-virtual {v6, p3, p2, v3}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result p3

    if-gez p3, :cond_b

    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    move v2, p3

    goto :goto_4

    :cond_b
    if-ge p3, p2, :cond_c

    goto :goto_4

    :cond_c
    invoke-virtual {v6, p1, v0, v3}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result p1

    if-gez p1, :cond_d

    iput v2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    goto :goto_3

    :cond_d
    iget p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    sub-int/2addr p2, p1

    iput p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->t:I

    :goto_3
    move v2, p1

    goto :goto_4

    :cond_e
    iget-object p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->m:Landroid/media/AudioTrack;

    invoke-virtual {p2, p1, v0, v3}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v2

    :cond_f
    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->P:J

    if-ltz v2, :cond_13

    iget-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->l:Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;

    iget-boolean p1, p1, Lcom/google/android/exoplayer2/audio/DefaultAudioSink$c;->a:Z

    if-eqz p1, :cond_10

    iget-wide p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->w:J

    int-to-long v3, v2

    add-long/2addr p2, v3

    iput-wide p2, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->w:J

    :cond_10
    if-ne v2, v0, :cond_12

    if-nez p1, :cond_11

    iget-wide p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->x:J

    iget p3, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->y:I

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->x:J

    :cond_11
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DefaultAudioSink;->F:Ljava/nio/ByteBuffer;

    :cond_12
    return-void

    :cond_13
    new-instance p1, Lcom/google/android/exoplayer2/audio/AudioSink$WriteException;

    invoke-direct {p1, v2}, Lcom/google/android/exoplayer2/audio/AudioSink$WriteException;-><init>(I)V

    throw p1
.end method
