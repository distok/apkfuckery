.class public final Lcom/google/android/exoplayer2/upstream/Loader$g;
.super Ljava/lang/Object;
.source "Loader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/upstream/Loader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation


# instance fields
.field public final d:Lcom/google/android/exoplayer2/upstream/Loader$f;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/upstream/Loader$f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer2/upstream/Loader$g;->d:Lcom/google/android/exoplayer2/upstream/Loader$f;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer2/upstream/Loader$g;->d:Lcom/google/android/exoplayer2/upstream/Loader$f;

    check-cast v0, Lf/h/a/c/d1/r;

    iget-object v1, v0, Lf/h/a/c/d1/r;->v:[Lf/h/a/c/d1/u;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x0

    if-ge v3, v2, :cond_1

    aget-object v5, v1, v3

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lf/h/a/c/d1/u;->p(Z)V

    iget-object v6, v5, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    if-eqz v6, :cond_0

    invoke-interface {v6}, Lcom/google/android/exoplayer2/drm/DrmSession;->release()V

    iput-object v4, v5, Lf/h/a/c/d1/u;->f:Lcom/google/android/exoplayer2/drm/DrmSession;

    iput-object v4, v5, Lf/h/a/c/d1/u;->e:Lcom/google/android/exoplayer2/Format;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lf/h/a/c/d1/r;->n:Lf/h/a/c/d1/r$b;

    iget-object v1, v0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lf/h/a/c/a1/h;->release()V

    iput-object v4, v0, Lf/h/a/c/d1/r$b;->b:Lf/h/a/c/a1/h;

    :cond_2
    return-void
.end method
