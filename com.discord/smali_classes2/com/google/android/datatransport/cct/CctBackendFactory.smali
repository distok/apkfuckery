.class public Lcom/google/android/datatransport/cct/CctBackendFactory;
.super Ljava/lang/Object;
.source "CctBackendFactory.java"

# interfaces
.implements Lf/h/a/b/j/q/d;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lf/h/a/b/j/q/h;)Lf/h/a/b/j/q/m;
    .locals 3

    new-instance v0, Lf/h/a/b/i/d;

    invoke-virtual {p1}, Lf/h/a/b/j/q/h;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lf/h/a/b/j/q/h;->d()Lf/h/a/b/j/v/a;

    move-result-object v2

    invoke-virtual {p1}, Lf/h/a/b/j/q/h;->c()Lf/h/a/b/j/v/a;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lf/h/a/b/i/d;-><init>(Landroid/content/Context;Lf/h/a/b/j/v/a;Lf/h/a/b/j/v/a;)V

    return-object v0
.end method
