.class public final Lcom/google/android/gms/analytics/AnalyticsReceiver;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field public a:Lf/h/a/f/i/h/s0;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .annotation build Landroidx/annotation/RequiresPermission;
        allOf = {
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->a:Lf/h/a/f/i/h/s0;

    if-nez v0, :cond_0

    new-instance v0, Lf/h/a/f/i/h/s0;

    invoke-direct {v0}, Lf/h/a/f/i/h/s0;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->a:Lf/h/a/f/i/h/s0;

    :cond_0
    sget-object v0, Lf/h/a/f/i/h/s0;->a:Ljava/lang/Object;

    invoke-static {p1}, Lf/h/a/f/i/h/g;->b(Landroid/content/Context;)Lf/h/a/f/i/h/g;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    if-nez p2, :cond_1

    const-string p1, "AnalyticsReceiver called with null intent"

    invoke-virtual {v0, p1}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    const-string v1, "Local AnalyticsReceiver got"

    invoke-virtual {v0, v1, p2}, Lf/h/a/f/i/h/d;->b(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v1, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-static {p1}, Lf/h/a/f/i/h/t0;->c(Landroid/content/Context;)Z

    move-result p2

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.gms.analytics.AnalyticsService"

    invoke-direct {v2, p1, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lf/h/a/f/i/h/s0;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    if-nez p2, :cond_2

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_2
    :try_start_1
    sget-object p2, Lf/h/a/f/i/h/s0;->b:Lf/h/a/f/o/a;

    if-nez p2, :cond_3

    new-instance p2, Lf/h/a/f/o/a;

    const/4 v1, 0x1

    const-string v3, "Analytics WakeLock"

    invoke-direct {p2, p1, v1, v3}, Lf/h/a/f/o/a;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    sput-object p2, Lf/h/a/f/i/h/s0;->b:Lf/h/a/f/o/a;

    const/4 p1, 0x0

    iget-object v1, p2, Lf/h/a/f/o/a;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, p1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iput-boolean p1, p2, Lf/h/a/f/o/a;->g:Z

    :cond_3
    sget-object p1, Lf/h/a/f/i/h/s0;->b:Lf/h/a/f/o/a;

    const-wide/16 v3, 0x3e8

    invoke-virtual {p1, v3, v4}, Lf/h/a/f/o/a;->a(J)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :try_start_2
    const-string p1, "Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {v0, p1}, Lf/h/a/f/i/h/d;->y(Ljava/lang/String;)V

    :goto_0
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :cond_4
    :goto_1
    return-void
.end method
