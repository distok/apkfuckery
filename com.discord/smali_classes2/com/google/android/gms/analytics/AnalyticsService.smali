.class public final Lcom/google/android/gms/analytics/AnalyticsService;
.super Landroid/app/Service;

# interfaces
.implements Lf/h/a/f/i/h/x0;


# instance fields
.field public d:Lf/h/a/f/i/h/t0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/h/a/f/i/h/t0<",
            "Lcom/google/android/gms/analytics/AnalyticsService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/job/JobParameters;Z)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public final b(I)Z
    .locals 0

    invoke-virtual {p0, p1}, Landroid/app/Service;->stopSelfResult(I)Z

    move-result p1

    return p1
.end method

.method public final c()Lf/h/a/f/i/h/t0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/h/a/f/i/h/t0<",
            "Lcom/google/android/gms/analytics/AnalyticsService;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/analytics/AnalyticsService;->d:Lf/h/a/f/i/h/t0;

    if-nez v0, :cond_0

    new-instance v0, Lf/h/a/f/i/h/t0;

    invoke-direct {v0, p0}, Lf/h/a/f/i/h/t0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/AnalyticsService;->d:Lf/h/a/f/i/h/t0;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/AnalyticsService;->d:Lf/h/a/f/i/h/t0;

    return-object v0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/AnalyticsService;->c()Lf/h/a/f/i/h/t0;

    const/4 p1, 0x0

    return-object p1
.end method

.method public final onCreate()V
    .locals 2
    .annotation build Landroidx/annotation/RequiresPermission;
        allOf = {
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE"
        }
    .end annotation

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/AnalyticsService;->c()Lf/h/a/f/i/h/t0;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/i/h/t0;->b:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->b(Landroid/content/Context;)Lf/h/a/f/i/h/g;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    const-string v1, "Local AnalyticsService is starting up"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    return-void
.end method

.method public final onDestroy()V
    .locals 2
    .annotation build Landroidx/annotation/RequiresPermission;
        allOf = {
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/AnalyticsService;->c()Lf/h/a/f/i/h/t0;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/i/h/t0;->b:Landroid/content/Context;

    invoke-static {v0}, Lf/h/a/f/i/h/g;->b(Landroid/content/Context;)Lf/h/a/f/i/h/g;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/i/h/g;->c()Lf/h/a/f/i/h/m0;

    move-result-object v0

    const-string v1, "Local AnalyticsService is shutting down"

    invoke-virtual {v0, v1}, Lf/h/a/f/i/h/d;->w(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 0
    .annotation build Landroidx/annotation/RequiresPermission;
        allOf = {
            "android.permission.INTERNET",
            "android.permission.ACCESS_NETWORK_STATE"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/AnalyticsService;->c()Lf/h/a/f/i/h/t0;

    move-result-object p2

    invoke-virtual {p2, p1, p3}, Lf/h/a/f/i/h/t0;->a(Landroid/content/Intent;I)I

    const/4 p1, 0x2

    return p1
.end method
