.class public Lcom/google/android/gms/safetynet/SafetyNetClient;
.super Lf/h/a/f/f/h/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/h/a/f/f/h/b<",
        "Lf/h/a/f/f/h/a$d$c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 5
    .param p1    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/h/a/f/m/a;->c:Lf/h/a/f/f/h/a;

    new-instance v1, Lf/h/a/f/f/h/i/a;

    invoke-direct {v1}, Lf/h/a/f/f/h/i/a;-><init>()V

    const-string v2, "StatusExceptionMapper must not be null."

    invoke-static {v1, v2}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const-string v3, "Looper must not be null."

    invoke-static {v2, v3}, Lf/g/j/k/a;->q(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lf/h/a/f/f/h/b$a;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4, v2}, Lf/h/a/f/f/h/b$a;-><init>(Lf/h/a/f/f/h/i/n;Landroid/accounts/Account;Landroid/os/Looper;)V

    invoke-direct {p0, p1, v0, v4, v3}, Lf/h/a/f/f/h/b;-><init>(Landroid/app/Activity;Lf/h/a/f/f/h/a;Lf/h/a/f/f/h/a$d;Lf/h/a/f/f/h/b$a;)V

    return-void
.end method
