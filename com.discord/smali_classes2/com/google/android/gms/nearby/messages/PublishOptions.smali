.class public final Lcom/google/android/gms/nearby/messages/PublishOptions;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/nearby/messages/Strategy;

.field public final b:Lf/h/a/f/k/b/b;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/nearby/messages/Strategy;->l:Lcom/google/android/gms/nearby/messages/Strategy;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/nearby/messages/Strategy;Lf/h/a/f/k/b/b;Lf/h/a/f/k/b/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/nearby/messages/PublishOptions;->a:Lcom/google/android/gms/nearby/messages/Strategy;

    iput-object p2, p0, Lcom/google/android/gms/nearby/messages/PublishOptions;->b:Lf/h/a/f/k/b/b;

    return-void
.end method
