.class public final Lcom/google/android/gms/internal/icing/zzw;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source "com.google.firebase:firebase-appindexing@@19.1.0"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/internal/icing/zzw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Lcom/google/android/gms/internal/icing/zzi;

.field public final e:J

.field public f:I

.field public final g:Ljava/lang/String;

.field public final h:Lcom/google/android/gms/internal/icing/zzh;

.field public final i:Z

.field public j:I

.field public k:I

.field public final l:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/f/i/i/u;

    invoke-direct {v0}, Lf/h/a/f/i/i/u;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/icing/zzw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/icing/zzi;JILjava/lang/String;Lcom/google/android/gms/internal/icing/zzh;ZIILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/icing/zzw;->d:Lcom/google/android/gms/internal/icing/zzi;

    iput-wide p2, p0, Lcom/google/android/gms/internal/icing/zzw;->e:J

    iput p4, p0, Lcom/google/android/gms/internal/icing/zzw;->f:I

    iput-object p5, p0, Lcom/google/android/gms/internal/icing/zzw;->g:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/icing/zzw;->h:Lcom/google/android/gms/internal/icing/zzh;

    iput-boolean p7, p0, Lcom/google/android/gms/internal/icing/zzw;->i:Z

    iput p8, p0, Lcom/google/android/gms/internal/icing/zzw;->j:I

    iput p9, p0, Lcom/google/android/gms/internal/icing/zzw;->k:I

    iput-object p10, p0, Lcom/google/android/gms/internal/icing/zzw;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/internal/icing/zzw;->d:Lcom/google/android/gms/internal/icing/zzi;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-wide v2, p0, Lcom/google/android/gms/internal/icing/zzw;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget v2, p0, Lcom/google/android/gms/internal/icing/zzw;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/google/android/gms/internal/icing/zzw;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string v2, "UsageInfo[documentId=%s, timestamp=%d, usageType=%d, status=%d]"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lf/g/j/k/a;->d1(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/internal/icing/zzw;->d:Lcom/google/android/gms/internal/icing/zzi;

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, p2, v3}, Lf/g/j/k/a;->Y0(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/internal/icing/zzw;->e:J

    const/16 v2, 0x8

    invoke-static {p1, v1, v2}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    const/4 v1, 0x3

    iget v4, p0, Lcom/google/android/gms/internal/icing/zzw;->f:I

    const/4 v5, 0x4

    invoke-static {p1, v1, v5}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/google/android/gms/internal/icing/zzw;->g:Ljava/lang/String;

    invoke-static {p1, v5, v1, v3}, Lf/g/j/k/a;->Z0(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x5

    iget-object v4, p0, Lcom/google/android/gms/internal/icing/zzw;->h:Lcom/google/android/gms/internal/icing/zzh;

    invoke-static {p1, v1, v4, p2, v3}, Lf/g/j/k/a;->Y0(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 p2, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/internal/icing/zzw;->i:Z

    invoke-static {p1, p2, v5}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 p2, 0x7

    iget v1, p0, Lcom/google/android/gms/internal/icing/zzw;->j:I

    invoke-static {p1, p2, v5}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/google/android/gms/internal/icing/zzw;->k:I

    invoke-static {p1, v2, v5}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    const/16 p2, 0x9

    iget-object v1, p0, Lcom/google/android/gms/internal/icing/zzw;->l:Ljava/lang/String;

    invoke-static {p1, p2, v1, v3}, Lf/g/j/k/a;->Z0(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v0}, Lf/g/j/k/a;->i1(Landroid/os/Parcel;I)V

    return-void
.end method
