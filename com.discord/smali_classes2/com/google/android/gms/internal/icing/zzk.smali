.class public final Lcom/google/android/gms/internal/icing/zzk;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source "com.google.firebase:firebase-appindexing@@19.1.0"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/internal/icing/zzk;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:I


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:Lcom/google/android/gms/internal/icing/zzt;

.field public final f:I

.field public final g:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-string v0, "-1"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/gms/internal/icing/zzk;->h:I

    new-instance v0, Lf/h/a/f/i/i/o;

    invoke-direct {v0}, Lf/h/a/f/i/i/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/icing/zzk;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/internal/icing/zzm;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/icing/zzm;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/internal/icing/zzt;I[B)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    sget v0, Lcom/google/android/gms/internal/icing/zzk;->h:I

    const/4 v1, 0x0

    if-eq p3, v0, :cond_3

    sget-object v2, Lf/h/a/f/i/i/q;->a:[Ljava/lang/String;

    if-ltz p3, :cond_1

    sget-object v2, Lf/h/a/f/i/i/q;->a:[Ljava/lang/String;

    array-length v3, v2

    if-lt p3, v3, :cond_0

    goto :goto_0

    :cond_0
    aget-object v2, v2, p3

    goto :goto_1

    :cond_1
    :goto_0
    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v2, 0x1

    :goto_3
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Invalid section type "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lf/g/j/k/a;->h(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/icing/zzk;->d:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/internal/icing/zzk;->e:Lcom/google/android/gms/internal/icing/zzt;

    iput p3, p0, Lcom/google/android/gms/internal/icing/zzk;->f:I

    iput-object p4, p0, Lcom/google/android/gms/internal/icing/zzk;->g:[B

    if-eq p3, v0, :cond_6

    sget-object p2, Lf/h/a/f/i/i/q;->a:[Ljava/lang/String;

    if-ltz p3, :cond_5

    sget-object p2, Lf/h/a/f/i/i/q;->a:[Ljava/lang/String;

    array-length v0, p2

    if-lt p3, v0, :cond_4

    goto :goto_4

    :cond_4
    aget-object p2, p2, p3

    goto :goto_5

    :cond_5
    :goto_4
    move-object p2, v1

    :goto_5
    if-nez p2, :cond_6

    invoke-static {v4, v5, p3}, Lf/e/c/a/a;->c(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_6
    if-eqz p1, :cond_7

    if-eqz p4, :cond_7

    const-string v1, "Both content and blobContent set"

    :cond_7
    :goto_6
    if-nez v1, :cond_8

    return-void

    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/16 v0, 0x4f45

    invoke-static {p1, v0}, Lf/g/j/k/a;->d1(Landroid/os/Parcel;I)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/internal/icing/zzk;->d:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lf/g/j/k/a;->Z0(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/internal/icing/zzk;->e:Lcom/google/android/gms/internal/icing/zzt;

    invoke-static {p1, v1, v2, p2, v3}, Lf/g/j/k/a;->Y0(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    iget p2, p0, Lcom/google/android/gms/internal/icing/zzk;->f:I

    const/4 v1, 0x4

    invoke-static {p1, v1, v1}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 p2, 0x5

    iget-object v1, p0, Lcom/google/android/gms/internal/icing/zzk;->g:[B

    invoke-static {p1, p2, v1, v3}, Lf/g/j/k/a;->U0(Landroid/os/Parcel;I[BZ)V

    invoke-static {p1, v0}, Lf/g/j/k/a;->i1(Landroid/os/Parcel;I)V

    return-void
.end method
