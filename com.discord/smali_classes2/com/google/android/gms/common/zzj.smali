.class public final Lcom/google/android/gms/common/zzj;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source "com.google.android.gms:play-services-basement@@17.4.0"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/google/android/gms/common/zzj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:Lf/h/a/f/f/s;

.field public final f:Z

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf/h/a/f/f/a0;

    invoke-direct {v0}, Lf/h/a/f/f/a0;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/zzj;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/IBinder;ZZ)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/zzj;->d:Ljava/lang/String;

    const-string p1, "Could not unwrap certificate"

    const-string v0, "GoogleCertificatesQuery"

    const/4 v1, 0x0

    if-nez p2, :cond_0

    goto :goto_2

    :cond_0
    :try_start_0
    sget v2, Lf/h/a/f/f/s;->b:I

    const-string v2, "com.google.android.gms.common.internal.ICertData"

    invoke-interface {p2, v2}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v2

    instance-of v3, v2, Lf/h/a/f/f/k/i0;

    if-eqz v3, :cond_1

    check-cast v2, Lf/h/a/f/f/k/i0;

    goto :goto_0

    :cond_1
    new-instance v2, Lf/h/a/f/f/k/j0;

    invoke-direct {v2, p2}, Lf/h/a/f/f/k/j0;-><init>(Landroid/os/IBinder;)V

    :goto_0
    invoke-interface {v2}, Lf/h/a/f/f/k/i0;->b()Lf/h/a/f/g/a;

    move-result-object p2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p2, :cond_2

    move-object p2, v1

    goto :goto_1

    :cond_2
    invoke-static {p2}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [B

    :goto_1
    if-eqz p2, :cond_3

    new-instance v1, Lf/h/a/f/f/v;

    invoke-direct {v1, p2}, Lf/h/a/f/f/v;-><init>([B)V

    goto :goto_2

    :cond_3
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_0
    move-exception p2

    invoke-static {v0, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    iput-object v1, p0, Lcom/google/android/gms/common/zzj;->e:Lf/h/a/f/f/s;

    iput-boolean p3, p0, Lcom/google/android/gms/common/zzj;->f:Z

    iput-boolean p4, p0, Lcom/google/android/gms/common/zzj;->g:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lf/h/a/f/f/s;ZZ)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/common/zzj;->d:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/common/zzj;->e:Lf/h/a/f/f/s;

    iput-boolean p3, p0, Lcom/google/android/gms/common/zzj;->f:Z

    iput-boolean p4, p0, Lcom/google/android/gms/common/zzj;->g:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/16 p2, 0x4f45

    invoke-static {p1, p2}, Lf/g/j/k/a;->d1(Landroid/os/Parcel;I)I

    move-result p2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/common/zzj;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lf/g/j/k/a;->Z0(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/common/zzj;->e:Lf/h/a/f/f/s;

    if-nez v1, :cond_0

    const-string v1, "GoogleCertificatesQuery"

    const-string v3, "certificate binder is null"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-static {p1, v0, v1, v2}, Lf/g/j/k/a;->W0(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/common/zzj;->f:Z

    const/4 v2, 0x4

    invoke-static {p1, v0, v2}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/zzj;->g:Z

    invoke-static {p1, v2, v2}, Lf/g/j/k/a;->g1(Landroid/os/Parcel;II)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-static {p1, p2}, Lf/g/j/k/a;->i1(Landroid/os/Parcel;I)V

    return-void
.end method
