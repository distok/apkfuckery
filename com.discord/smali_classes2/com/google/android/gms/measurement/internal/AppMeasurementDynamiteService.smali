.class public Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;
.super Lf/h/a/f/i/j/dc;
.source "com.google.android.gms:play-services-measurement-sdk@@18.0.0"


# annotations
.annotation build Lcom/google/android/gms/common/util/DynamiteApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;,
        Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;
    }
.end annotation


# instance fields
.field public a:Lf/h/a/f/j/b/u4;

.field public final b:Ljava/util/Map;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "listenerMap"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lf/h/a/f/j/b/z5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/h/a/f/i/j/dc;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    new-instance v0, Landroidx/collection/ArrayMap;

    invoke-direct {v0}, Landroidx/collection/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->b:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public beginAdUnitExposure(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->A()Lf/h/a/f/j/b/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/f/j/b/a;->v(Ljava/lang/String;J)V

    return-void
.end method

.method public clearConditionalUserProperty(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/f/j/b/c6;->R(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public clearMeasurementEnabled(J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object p2

    new-instance v0, Lf/h/a/f/j/b/u6;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lf/h/a/f/j/b/u6;-><init>(Lf/h/a/f/j/b/c6;Ljava/lang/Boolean;)V

    invoke-virtual {p2, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public endAdUnitExposure(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->A()Lf/h/a/f/j/b/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/f/j/b/a;->y(Ljava/lang/String;J)V

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to perform action before initialize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public generateEventId(Lf/h/a/f/i/j/fc;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/t9;->t0()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Lf/h/a/f/j/b/t9;->K(Lf/h/a/f/i/j/fc;J)V

    return-void
.end method

.method public getAppInstanceId(Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/a6;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/j/b/a6;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/fc;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCachedAppInstanceId(Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/c6;->g:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void
.end method

.method public getConditionalUserProperties(Ljava/lang/String;Ljava/lang/String;Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/z8;

    invoke-direct {v1, p0, p3, p1, p2}, Lf/h/a/f/j/b/z8;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/fc;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentScreenClass(Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->w()Lf/h/a/f/j/b/h7;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void
.end method

.method public getCurrentScreenName(Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->w()Lf/h/a/f/j/b/h7;

    move-result-object v0

    iget-object v0, v0, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void
.end method

.method public getGmpAppId(Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/c6;->O()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void
.end method

.method public getMaxUserProperties(Ljava/lang/String;Lf/h/a/f/i/j/fc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    invoke-static {p1}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object p1

    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, Lf/h/a/f/j/b/t9;->J(Lf/h/a/f/i/j/fc;I)V

    return-void
.end method

.method public getTestFlag(Lf/h/a/f/i/j/fc;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    if-eqz p2, :cond_4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v6, Lf/h/a/f/j/b/d6;

    invoke-direct {v6, v0, v2}, Lf/h/a/f/j/b/d6;-><init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-wide/16 v3, 0x3a98

    const-string v5, "boolean test flag value"

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/j/b/r4;->s(Ljava/util/concurrent/atomic/AtomicReference;JLjava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, p1, v0}, Lf/h/a/f/j/b/t9;->O(Lf/h/a/f/i/j/fc;Z)V

    :goto_0
    return-void

    :cond_1
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v6, Lf/h/a/f/j/b/s6;

    invoke-direct {v6, v0, v2}, Lf/h/a/f/j/b/s6;-><init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-wide/16 v3, 0x3a98

    const-string v5, "int test flag value"

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/j/b/r4;->s(Ljava/util/concurrent/atomic/AtomicReference;JLjava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, p1, v0}, Lf/h/a/f/j/b/t9;->J(Lf/h/a/f/i/j/fc;I)V

    return-void

    :cond_2
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v6, Lf/h/a/f/j/b/v6;

    invoke-direct {v6, v0, v2}, Lf/h/a/f/j/b/v6;-><init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-wide/16 v3, 0x3a98

    const-string v5, "double test flag value"

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/j/b/r4;->s(Ljava/util/concurrent/atomic/AtomicReference;JLjava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "r"

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    :try_start_0
    invoke-interface {p1, v2}, Lf/h/a/f/i/j/fc;->f(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object p2, p2, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v0, "Error returning double value to wrapper"

    invoke-virtual {p2, v0, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_3
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v6, Lf/h/a/f/j/b/t6;

    invoke-direct {v6, v0, v2}, Lf/h/a/f/j/b/t6;-><init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-wide/16 v3, 0x3a98

    const-string v5, "long test flag value"

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/j/b/r4;->s(Ljava/util/concurrent/atomic/AtomicReference;JLjava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, Lf/h/a/f/j/b/t9;->K(Lf/h/a/f/i/j/fc;J)V

    return-void

    :cond_4
    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->t()Lf/h/a/f/j/b/t9;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v6, Lf/h/a/f/j/b/m6;

    invoke-direct {v6, v0, v2}, Lf/h/a/f/j/b/m6;-><init>(Lf/h/a/f/j/b/c6;Ljava/util/concurrent/atomic/AtomicReference;)V

    const-wide/16 v3, 0x3a98

    const-string v5, "String test flag value"

    invoke-virtual/range {v1 .. v6}, Lf/h/a/f/j/b/r4;->s(Ljava/util/concurrent/atomic/AtomicReference;JLjava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lf/h/a/f/j/b/t9;->M(Lf/h/a/f/i/j/fc;Ljava/lang/String;)V

    return-void
.end method

.method public getUserProperties(Ljava/lang/String;Ljava/lang/String;ZLf/h/a/f/i/j/fc;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v7, Lf/h/a/f/j/b/a7;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lf/h/a/f/j/b/a7;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/fc;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v7}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public initForTests(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    return-void
.end method

.method public initialize(Lf/h/a/f/g/a;Lcom/google/android/gms/internal/measurement/zzae;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    if-nez v0, :cond_0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-static {p1, p2, p3}, Lf/h/a/f/j/b/u4;->b(Landroid/content/Context;Lcom/google/android/gms/internal/measurement/zzae;Ljava/lang/Long;)Lf/h/a/f/j/b/u4;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    return-void

    :cond_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string p2, "Attempting to initialize multiple times"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void
.end method

.method public isDataCollectionEnabled(Lf/h/a/f/i/j/fc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object v0

    new-instance v1, Lf/h/a/f/j/b/x9;

    invoke-direct {v1, p0, p1}, Lf/h/a/f/j/b/x9;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/fc;)V

    invoke-virtual {v0, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public logEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    move-object v0, p0

    iget-object v1, v0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v1}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v2

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    move-wide/from16 v8, p6

    invoke-virtual/range {v2 .. v9}, Lf/h/a/f/j/b/c6;->I(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZZJ)V

    return-void
.end method

.method public logEventAndBundle(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lf/h/a/f/i/j/fc;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    invoke-static {p2}, Lf/g/j/k/a;->o(Ljava/lang/String;)Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    if-eqz p3, :cond_0

    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :goto_0
    const-string v1, "_o"

    const-string v5, "app"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/measurement/internal/zzaq;

    new-instance v4, Lcom/google/android/gms/measurement/internal/zzap;

    invoke-direct {v4, p3}, Lcom/google/android/gms/measurement/internal/zzap;-><init>(Landroid/os/Bundle;)V

    move-object v2, v0

    move-object v3, p2

    move-wide v6, p5

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/measurement/internal/zzaq;-><init>(Ljava/lang/String;Lcom/google/android/gms/measurement/internal/zzap;Ljava/lang/String;J)V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object p2

    new-instance p3, Lf/h/a/f/j/b/y7;

    invoke-direct {p3, p0, p4, v0, p1}, Lf/h/a/f/j/b/y7;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/fc;Lcom/google/android/gms/measurement/internal/zzaq;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public logHealthData(ILjava/lang/String;Lf/h/a/f/g/a;Lf/h/a/f/g/a;Lf/h/a/f/g/a;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    const/4 v0, 0x0

    if-nez p3, :cond_0

    move-object v6, v0

    goto :goto_0

    :cond_0
    invoke-static {p3}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p3

    move-object v6, p3

    :goto_0
    if-nez p4, :cond_1

    move-object v7, v0

    goto :goto_1

    :cond_1
    invoke-static {p4}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p3

    move-object v7, p3

    :goto_1
    if-nez p5, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {p5}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object v0

    :goto_2
    move-object v8, v0

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    move v2, p1

    move-object v5, p2

    invoke-virtual/range {v1 .. v8}, Lf/h/a/f/j/b/q3;->w(IZZLjava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public onActivityCreated(Lf/h/a/f/g/a;Landroid/os/Bundle;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p3

    iget-object p3, p3, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-eqz p3, :cond_0

    iget-object p4, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p4}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p4

    invoke-virtual {p4}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p3, p1, p2}, Lf/h/a/f/j/b/y6;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Lf/h/a/f/g/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-eqz p2, :cond_0

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/y6;->onActivityDestroyed(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivityPaused(Lf/h/a/f/g/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-eqz p2, :cond_0

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/y6;->onActivityPaused(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivityResumed(Lf/h/a/f/g/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-eqz p2, :cond_0

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p3

    invoke-virtual {p3}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p2, p1}, Lf/h/a/f/j/b/y6;->onActivityResumed(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method public onActivitySaveInstanceState(Lf/h/a/f/g/a;Lf/h/a/f/i/j/fc;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p3

    iget-object p3, p3, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p3, p1, p4}, Lf/h/a/f/j/b/y6;->onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    :cond_0
    :try_start_0
    invoke-interface {p2, p4}, Lf/h/a/f/i/j/fc;->f(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string p3, "Error returning bundle value to wrapper"

    invoke-virtual {p2, p3, p1}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onActivityStarted(Lf/h/a/f/g/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    invoke-virtual {p2}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public onActivityStopped(Lf/h/a/f/g/a;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    iget-object p2, p2, Lf/h/a/f/j/b/c6;->c:Lf/h/a/f/j/b/y6;

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    invoke-virtual {p2}, Lf/h/a/f/j/b/c6;->M()V

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public performAction(Landroid/os/Bundle;Lf/h/a/f/i/j/fc;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Lf/h/a/f/i/j/fc;->f(Landroid/os/Bundle;)V

    return-void
.end method

.method public registerOnMeasurementEventListener(Lf/h/a/f/i/j/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->b:Ljava/util/Map;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->b:Ljava/util/Map;

    invoke-interface {p1}, Lf/h/a/f/i/j/c;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/j/b/z5;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/c;)V

    iget-object v2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->b:Ljava/util/Map;

    invoke-interface {p1}, Lf/h/a/f/i/j/c;->a()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, p1, Lf/h/a/f/j/b/c6;->e:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v0, "OnEventListener already registered"

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public resetAnalyticsData(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    iget-object v1, v0, Lf/h/a/f/j/b/c6;->g:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v2, Lf/h/a/f/j/b/l6;

    invoke-direct {v2, v0, p1, p2}, Lf/h/a/f/j/b/l6;-><init>(Lf/h/a/f/j/b/c6;J)V

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setConditionalUserProperty(Landroid/os/Bundle;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->f:Lf/h/a/f/j/b/s3;

    const-string p2, "Conditional user property must not be null"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lf/h/a/f/j/b/c6;->y(Landroid/os/Bundle;J)V

    return-void
.end method

.method public setConsent(Landroid/os/Bundle;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->H0:Lf/h/a/f/j/b/j3;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x1e

    invoke-virtual {v0, p1, v1, p2, p3}, Lf/h/a/f/j/b/c6;->x(Landroid/os/Bundle;IJ)V

    :cond_0
    return-void
.end method

.method public setConsentThirdParty(Landroid/os/Bundle;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-static {}, Lf/h/a/f/i/j/t8;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object v1, v1, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    sget-object v2, Lf/h/a/f/j/b/p;->I0:Lf/h/a/f/j/b/j3;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Lf/h/a/f/j/b/c;->u(Ljava/lang/String;Lf/h/a/f/j/b/j3;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    invoke-virtual {v0, p1, v1, p2, p3}, Lf/h/a/f/j/b/c6;->x(Landroid/os/Bundle;IJ)V

    :cond_0
    return-void
.end method

.method public setCurrentScreen(Lf/h/a/f/g/a;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p4, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p4}, Lf/h/a/f/j/b/u4;->w()Lf/h/a/f/j/b/h7;

    move-result-object p4

    invoke-static {p1}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    iget-object p5, p4, Lf/h/a/f/j/b/s5;->a:Lf/h/a/f/j/b/u4;

    iget-object p5, p5, Lf/h/a/f/j/b/u4;->g:Lf/h/a/f/j/b/c;

    invoke-virtual {p5}, Lf/h/a/f/j/b/c;->z()Ljava/lang/Boolean;

    move-result-object p5

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p5

    if-nez p5, :cond_0

    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string p2, "setCurrentScreen cannot be called while screen reporting is disabled."

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_0
    iget-object p5, p4, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    if-nez p5, :cond_1

    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string p2, "setCurrentScreen cannot be called while no activity active"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1
    iget-object p5, p4, Lf/h/a/f/j/b/h7;->f:Ljava/util/Map;

    invoke-interface {p5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    if-nez p5, :cond_2

    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string p2, "setCurrentScreen must be called with an activity in the activity lifecycle"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_2
    if-nez p3, :cond_3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Lf/h/a/f/j/b/h7;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_3
    iget-object p5, p4, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    iget-object p5, p5, Lf/h/a/f/j/b/i7;->b:Ljava/lang/String;

    invoke-static {p5, p3}, Lf/h/a/f/j/b/t9;->q0(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p5

    iget-object v0, p4, Lf/h/a/f/j/b/h7;->c:Lf/h/a/f/j/b/i7;

    iget-object v0, v0, Lf/h/a/f/j/b/i7;->a:Ljava/lang/String;

    invoke-static {v0, p2}, Lf/h/a/f/j/b/t9;->q0(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz p5, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    const-string p2, "setCurrentScreen cannot be called with the same class and name"

    invoke-virtual {p1, p2}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const/16 p5, 0x64

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p5, :cond_6

    :cond_5
    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "Invalid screen name length in setCurrentScreen. Length"

    invoke-virtual {p1, p3, p2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    if-eqz p3, :cond_8

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p5, :cond_8

    :cond_7
    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->k:Lf/h/a/f/j/b/s3;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const-string p3, "Invalid class name length in setCurrentScreen. Length"

    invoke-virtual {p1, p3, p2}, Lf/h/a/f/j/b/s3;->b(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    :cond_8
    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p5

    iget-object p5, p5, Lf/h/a/f/j/b/q3;->n:Lf/h/a/f/j/b/s3;

    if-nez p2, :cond_9

    const-string v0, "null"

    goto :goto_0

    :cond_9
    move-object v0, p2

    :goto_0
    const-string v1, "Setting current screen to name, class"

    invoke-virtual {p5, v1, v0, p3}, Lf/h/a/f/j/b/s3;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    new-instance p5, Lf/h/a/f/j/b/i7;

    invoke-virtual {p4}, Lf/h/a/f/j/b/s5;->e()Lf/h/a/f/j/b/t9;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/t9;->t0()J

    move-result-wide v0

    invoke-direct {p5, p2, p3, v0, v1}, Lf/h/a/f/j/b/i7;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iget-object p2, p4, Lf/h/a/f/j/b/h7;->f:Ljava/util/Map;

    invoke-interface {p2, p1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p2, 0x1

    invoke-virtual {p4, p1, p5, p2}, Lf/h/a/f/j/b/h7;->z(Landroid/app/Activity;Lf/h/a/f/j/b/i7;Z)V

    :goto_1
    return-void
.end method

.method public setDataCollectionEnabled(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v2, Lf/h/a/f/j/b/g6;

    invoke-direct {v2, v0, p1}, Lf/h/a/f/j/b/g6;-><init>(Lf/h/a/f/j/b/c6;Z)V

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setDefaultEventParameters(Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, p1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    move-object p1, v1

    :goto_0
    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v2, Lf/h/a/f/j/b/b6;

    invoke-direct {v2, v0, p1}, Lf/h/a/f/j/b/b6;-><init>(Lf/h/a/f/j/b/c6;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setEventInterceptor(Lf/h/a/f/i/j/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    new-instance v0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/c;)V

    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/a/f/j/b/r4;->y()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p1

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/c6;->B(Lf/h/a/f/j/b/w5;)V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->f()Lf/h/a/f/j/b/r4;

    move-result-object p1

    new-instance v1, Lf/h/a/f/j/b/w9;

    invoke-direct {v1, p0, v0}, Lf/h/a/f/j/b/w9;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$a;)V

    invoke-virtual {p1, v1}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setInstanceIdProvider(Lf/h/a/f/i/j/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    return-void
.end method

.method public setMeasurementEnabled(ZJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object p2, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p2}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2}, Lf/h/a/f/j/b/a5;->t()V

    invoke-virtual {p2}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object p3

    new-instance v0, Lf/h/a/f/j/b/u6;

    invoke-direct {v0, p2, p1}, Lf/h/a/f/j/b/u6;-><init>(Lf/h/a/f/j/b/c6;Ljava/lang/Boolean;)V

    invoke-virtual {p3, v0}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setMinimumSessionDuration(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v2, Lf/h/a/f/j/b/i6;

    invoke-direct {v2, v0, p1, p2}, Lf/h/a/f/j/b/i6;-><init>(Lf/h/a/f/j/b/c6;J)V

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSessionTimeoutDuration(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    invoke-virtual {v0}, Lf/h/a/f/j/b/s5;->f()Lf/h/a/f/j/b/r4;

    move-result-object v1

    new-instance v2, Lf/h/a/f/j/b/h6;

    invoke-direct {v2, v0, p1, p2}, Lf/h/a/f/j/b/h6;-><init>(Lf/h/a/f/j/b/c6;J)V

    invoke-virtual {v1, v2}, Lf/h/a/f/j/b/r4;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setUserId(Ljava/lang/String;J)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {v0}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "_id"

    const/4 v5, 0x1

    move-object v4, p1

    move-wide v6, p2

    invoke-virtual/range {v1 .. v7}, Lf/h/a/f/j/b/c6;->L(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZJ)V

    return-void
.end method

.method public setUserProperty(Ljava/lang/String;Ljava/lang/String;Lf/h/a/f/g/a;ZJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    invoke-static {p3}, Lf/h/a/f/g/b;->h(Lf/h/a/f/g/a;)Ljava/lang/Object;

    move-result-object v3

    iget-object p3, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p3}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lf/h/a/f/j/b/c6;->L(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;ZJ)V

    return-void
.end method

.method public unregisterOnMeasurementEventListener(Lf/h/a/f/i/j/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->g()V

    iget-object v0, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->b:Ljava/util/Map;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->b:Ljava/util/Map;

    invoke-interface {p1}, Lf/h/a/f/i/j/c;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/h/a/f/j/b/z5;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService$b;-><init>(Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;Lf/h/a/f/i/j/c;)V

    :cond_0
    iget-object p1, p0, Lcom/google/android/gms/measurement/internal/AppMeasurementDynamiteService;->a:Lf/h/a/f/j/b/u4;

    invoke-virtual {p1}, Lf/h/a/f/j/b/u4;->s()Lf/h/a/f/j/b/c6;

    move-result-object p1

    invoke-virtual {p1}, Lf/h/a/f/j/b/a5;->t()V

    iget-object v0, p1, Lf/h/a/f/j/b/c6;->e:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lf/h/a/f/j/b/s5;->g()Lf/h/a/f/j/b/q3;

    move-result-object p1

    iget-object p1, p1, Lf/h/a/f/j/b/q3;->i:Lf/h/a/f/j/b/s3;

    const-string v0, "OnEventListener had not been registered"

    invoke-virtual {p1, v0}, Lf/h/a/f/j/b/s3;->a(Ljava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method
