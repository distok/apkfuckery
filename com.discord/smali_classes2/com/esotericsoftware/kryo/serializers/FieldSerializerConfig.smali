.class public Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;
.super Ljava/lang/Object;
.source "FieldSerializerConfig.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private cachedFieldNameStrategy:Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;

.field private copyTransient:Z

.field private fieldsCanBeNull:Z

.field private fixedFieldTypes:Z

.field private ignoreSyntheticFields:Z

.field private optimizedGenerics:Z

.field private serializeTransient:Z

.field private setFieldsAsAccessible:Z

.field private useAsm:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->fieldsCanBeNull:Z

    iput-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->setFieldsAsAccessible:Z

    iput-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->ignoreSyntheticFields:Z

    iput-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->copyTransient:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->serializeTransient:Z

    iput-boolean v1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->optimizedGenerics:Z

    sget-object v1, Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;->DEFAULT:Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;

    iput-object v1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->cachedFieldNameStrategy:Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;

    sget-boolean v1, Lcom/esotericsoftware/kryo/serializers/FieldSerializer;->unsafeAvailable:Z

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->useAsm:Z

    sget-object v0, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method


# virtual methods
.method public clone()Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->clone()Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;

    move-result-object v0

    return-object v0
.end method

.method public getCachedFieldNameStrategy()Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;
    .locals 1

    iget-object v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->cachedFieldNameStrategy:Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;

    return-object v0
.end method

.method public isCopyTransient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->copyTransient:Z

    return v0
.end method

.method public isFieldsCanBeNull()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->fieldsCanBeNull:Z

    return v0
.end method

.method public isFixedFieldTypes()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->fixedFieldTypes:Z

    return v0
.end method

.method public isIgnoreSyntheticFields()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->ignoreSyntheticFields:Z

    return v0
.end method

.method public isOptimizedGenerics()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->optimizedGenerics:Z

    return v0
.end method

.method public isSerializeTransient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->serializeTransient:Z

    return v0
.end method

.method public isSetFieldsAsAccessible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->setFieldsAsAccessible:Z

    return v0
.end method

.method public isUseAsm()Z
    .locals 1

    iget-boolean v0, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->useAsm:Z

    return v0
.end method

.method public setCachedFieldNameStrategy(Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;)V
    .locals 0

    iput-object p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->cachedFieldNameStrategy:Lcom/esotericsoftware/kryo/serializers/FieldSerializer$CachedFieldNameStrategy;

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method

.method public setCopyTransient(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->copyTransient:Z

    return-void
.end method

.method public setFieldsAsAccessible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->setFieldsAsAccessible:Z

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method

.method public setFieldsCanBeNull(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->fieldsCanBeNull:Z

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method

.method public setFixedFieldTypes(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->fixedFieldTypes:Z

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method

.method public setIgnoreSyntheticFields(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->ignoreSyntheticFields:Z

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method

.method public setOptimizedGenerics(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->optimizedGenerics:Z

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method

.method public setSerializeTransient(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->serializeTransient:Z

    return-void
.end method

.method public setUseAsm(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->useAsm:Z

    if-nez p1, :cond_0

    sget-boolean p1, Lcom/esotericsoftware/kryo/serializers/FieldSerializer;->unsafeAvailable:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/esotericsoftware/kryo/serializers/FieldSerializerConfig;->useAsm:Z

    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    :cond_0
    sget-object p1, Lf/f/a/a;->a:Lf/f/a/a$a;

    return-void
.end method
