.class public Lcom/miguelgaeta/backgrounded/Backgrounded;
.super Ljava/lang/Object;
.source "Backgrounded.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;
    }
.end annotation


# static fields
.field private static final emitter:Lrx/subjects/SerializedSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/SerializedSubject<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static lifecycle:Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lrx/subjects/SerializedSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-direct {v0, v1}, Lrx/subjects/SerializedSubject;-><init>(Lrx/subjects/Subject;)V

    sput-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->emitter:Lrx/subjects/SerializedSubject;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic access$000()Lrx/subjects/SerializedSubject;
    .locals 1

    sget-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->emitter:Lrx/subjects/SerializedSubject;

    return-object v0
.end method

.method private static checkInitialized()V
    .locals 2

    sget-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->lifecycle:Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Backgrounded utility is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static destroy(Landroid/app/Application;)V
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->lifecycle:Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;

    invoke-virtual {p0, v0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_0
    return-void
.end method

.method public static get()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->checkInitialized()V

    sget-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->emitter:Lrx/subjects/SerializedSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/app/Application;)V
    .locals 1

    new-instance v0, Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;

    invoke-direct {v0}, Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;-><init>()V

    sput-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->lifecycle:Lcom/miguelgaeta/backgrounded/Backgrounded$Lifecycle;

    if-eqz p0, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_0
    return-void
.end method

.method public static isBackgrounded()Z
    .locals 3

    invoke-static {}, Lcom/miguelgaeta/backgrounded/Backgrounded;->checkInitialized()V

    sget-object v0, Lcom/miguelgaeta/backgrounded/Backgrounded;->emitter:Lrx/subjects/SerializedSubject;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    new-instance v2, Lg0/l/a/a;

    invoke-direct {v2, v1, v0}, Lg0/l/a/a;-><init>(Ljava/lang/Object;Lrx/Observable;)V

    invoke-virtual {v2}, Lg0/l/a/a;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
