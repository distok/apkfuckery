.class public final enum Lcom/miguelgaeta/media_picker/RequestType;
.super Ljava/lang/Enum;
.source "RequestType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miguelgaeta/media_picker/RequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miguelgaeta/media_picker/RequestType;

.field public static final enum CAMERA:Lcom/miguelgaeta/media_picker/RequestType;

.field public static final enum CHOOSER:Lcom/miguelgaeta/media_picker/RequestType;

.field public static final enum CROP:Lcom/miguelgaeta/media_picker/RequestType;

.field public static final enum DOCUMENTS:Lcom/miguelgaeta/media_picker/RequestType;

.field public static final enum GALLERY:Lcom/miguelgaeta/media_picker/RequestType;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    new-instance v0, Lcom/miguelgaeta/media_picker/RequestType;

    const-string v1, "CAMERA"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miguelgaeta/media_picker/RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miguelgaeta/media_picker/RequestType;->CAMERA:Lcom/miguelgaeta/media_picker/RequestType;

    new-instance v1, Lcom/miguelgaeta/media_picker/RequestType;

    const-string v3, "GALLERY"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/miguelgaeta/media_picker/RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/miguelgaeta/media_picker/RequestType;->GALLERY:Lcom/miguelgaeta/media_picker/RequestType;

    new-instance v3, Lcom/miguelgaeta/media_picker/RequestType;

    const-string v5, "DOCUMENTS"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/miguelgaeta/media_picker/RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/miguelgaeta/media_picker/RequestType;->DOCUMENTS:Lcom/miguelgaeta/media_picker/RequestType;

    new-instance v5, Lcom/miguelgaeta/media_picker/RequestType;

    const-string v7, "CROP"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/miguelgaeta/media_picker/RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/miguelgaeta/media_picker/RequestType;->CROP:Lcom/miguelgaeta/media_picker/RequestType;

    new-instance v7, Lcom/miguelgaeta/media_picker/RequestType;

    const-string v9, "CHOOSER"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/miguelgaeta/media_picker/RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/miguelgaeta/media_picker/RequestType;->CHOOSER:Lcom/miguelgaeta/media_picker/RequestType;

    const/4 v9, 0x5

    new-array v9, v9, [Lcom/miguelgaeta/media_picker/RequestType;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    sput-object v9, Lcom/miguelgaeta/media_picker/RequestType;->$VALUES:[Lcom/miguelgaeta/media_picker/RequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static create(I)Lcom/miguelgaeta/media_picker/RequestType;
    .locals 1

    const/16 v0, 0x320

    if-eq p0, v0, :cond_1

    const/16 v0, 0x321

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    sget-object p0, Lcom/miguelgaeta/media_picker/RequestType;->DOCUMENTS:Lcom/miguelgaeta/media_picker/RequestType;

    return-object p0

    :pswitch_1
    sget-object p0, Lcom/miguelgaeta/media_picker/RequestType;->GALLERY:Lcom/miguelgaeta/media_picker/RequestType;

    return-object p0

    :pswitch_2
    sget-object p0, Lcom/miguelgaeta/media_picker/RequestType;->CAMERA:Lcom/miguelgaeta/media_picker/RequestType;

    return-object p0

    :cond_0
    sget-object p0, Lcom/miguelgaeta/media_picker/RequestType;->CHOOSER:Lcom/miguelgaeta/media_picker/RequestType;

    return-object p0

    :cond_1
    sget-object p0, Lcom/miguelgaeta/media_picker/RequestType;->CROP:Lcom/miguelgaeta/media_picker/RequestType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x309
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miguelgaeta/media_picker/RequestType;
    .locals 1

    const-class v0, Lcom/miguelgaeta/media_picker/RequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miguelgaeta/media_picker/RequestType;

    return-object p0
.end method

.method public static values()[Lcom/miguelgaeta/media_picker/RequestType;
    .locals 1

    sget-object v0, Lcom/miguelgaeta/media_picker/RequestType;->$VALUES:[Lcom/miguelgaeta/media_picker/RequestType;

    invoke-virtual {v0}, [Lcom/miguelgaeta/media_picker/RequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miguelgaeta/media_picker/RequestType;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    const/16 v0, 0x321

    return v0

    :cond_1
    const/16 v0, 0x320

    return v0

    :cond_2
    const/16 v0, 0x30b

    return v0

    :cond_3
    const/16 v0, 0x30a

    return v0

    :cond_4
    const/16 v0, 0x309

    return v0
.end method
