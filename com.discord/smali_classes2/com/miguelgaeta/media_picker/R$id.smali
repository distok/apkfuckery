.class public final Lcom/miguelgaeta/media_picker/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miguelgaeta/media_picker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0a003f

.field public static final action_bar_activity_content:I = 0x7f0a0040

.field public static final action_bar_container:I = 0x7f0a0041

.field public static final action_bar_root:I = 0x7f0a0042

.field public static final action_bar_spinner:I = 0x7f0a0043

.field public static final action_bar_subtitle:I = 0x7f0a0044

.field public static final action_bar_title:I = 0x7f0a0046

.field public static final action_container:I = 0x7f0a004a

.field public static final action_context_bar:I = 0x7f0a004b

.field public static final action_divider:I = 0x7f0a004c

.field public static final action_image:I = 0x7f0a004d

.field public static final action_menu_divider:I = 0x7f0a004e

.field public static final action_menu_presenter:I = 0x7f0a004f

.field public static final action_mode_bar:I = 0x7f0a0050

.field public static final action_mode_bar_stub:I = 0x7f0a0051

.field public static final action_mode_close_button:I = 0x7f0a0052

.field public static final action_text:I = 0x7f0a0053

.field public static final actions:I = 0x7f0a0054

.field public static final activity_chooser_view_content:I = 0x7f0a005e

.field public static final add:I = 0x7f0a0060

.field public static final alertTitle:I = 0x7f0a007e

.field public static final async:I = 0x7f0a00a2

.field public static final blocking:I = 0x7f0a00ff

.field public static final bottom:I = 0x7f0a0127

.field public static final buttonPanel:I = 0x7f0a012f

.field public static final checkbox:I = 0x7f0a0295

.field public static final chronometer:I = 0x7f0a02a2

.field public static final content:I = 0x7f0a02f3

.field public static final contentPanel:I = 0x7f0a02f4

.field public static final custom:I = 0x7f0a0320

.field public static final customPanel:I = 0x7f0a0321

.field public static final decor_content_parent:I = 0x7f0a032b

.field public static final default_activity_button:I = 0x7f0a032c

.field public static final edit_query:I = 0x7f0a039a

.field public static final end:I = 0x7f0a03c7

.field public static final expand_activities_button:I = 0x7f0a03e4

.field public static final expanded_menu:I = 0x7f0a03e5

.field public static final forever:I = 0x7f0a0466

.field public static final group_divider:I = 0x7f0a04b8

.field public static final home:I = 0x7f0a0566

.field public static final icon:I = 0x7f0a057a

.field public static final icon_group:I = 0x7f0a057c

.field public static final image:I = 0x7f0a0588

.field public static final image_view_crop:I = 0x7f0a0593

.field public static final image_view_logo:I = 0x7f0a0594

.field public static final image_view_state_aspect_ratio:I = 0x7f0a0595

.field public static final image_view_state_rotate:I = 0x7f0a0596

.field public static final image_view_state_scale:I = 0x7f0a0597

.field public static final info:I = 0x7f0a05a9

.field public static final italic:I = 0x7f0a05e6

.field public static final layout_aspect_ratio:I = 0x7f0a0638

.field public static final layout_rotate_wheel:I = 0x7f0a0639

.field public static final layout_scale_wheel:I = 0x7f0a063a

.field public static final left:I = 0x7f0a063f

.field public static final line1:I = 0x7f0a0641

.field public static final line3:I = 0x7f0a0642

.field public static final listMode:I = 0x7f0a0645

.field public static final list_item:I = 0x7f0a0646

.field public static final menu_crop:I = 0x7f0a068c

.field public static final menu_loader:I = 0x7f0a0694

.field public static final message:I = 0x7f0a06a9

.field public static final multiply:I = 0x7f0a06cc

.field public static final none:I = 0x7f0a06f3

.field public static final normal:I = 0x7f0a06f4

.field public static final notification_background:I = 0x7f0a06fe

.field public static final notification_main_column:I = 0x7f0a06ff

.field public static final notification_main_column_container:I = 0x7f0a0700

.field public static final parentPanel:I = 0x7f0a074d

.field public static final progress_circular:I = 0x7f0a0801

.field public static final progress_horizontal:I = 0x7f0a0804

.field public static final radio:I = 0x7f0a0814

.field public static final right:I = 0x7f0a083b

.field public static final right_icon:I = 0x7f0a083c

.field public static final right_side:I = 0x7f0a083d

.field public static final rotate_scroll_wheel:I = 0x7f0a0865

.field public static final scale_scroll_wheel:I = 0x7f0a086e

.field public static final screen:I = 0x7f0a086f

.field public static final scrollIndicatorDown:I = 0x7f0a0875

.field public static final scrollIndicatorUp:I = 0x7f0a0876

.field public static final scrollView:I = 0x7f0a0877

.field public static final search_badge:I = 0x7f0a087a

.field public static final search_bar:I = 0x7f0a087b

.field public static final search_button:I = 0x7f0a087c

.field public static final search_close_btn:I = 0x7f0a087d

.field public static final search_edit_frame:I = 0x7f0a087e

.field public static final search_go_btn:I = 0x7f0a0880

.field public static final search_mag_icon:I = 0x7f0a0881

.field public static final search_plate:I = 0x7f0a0882

.field public static final search_src_text:I = 0x7f0a0885

.field public static final search_voice_btn:I = 0x7f0a0891

.field public static final select_dialog_listview:I = 0x7f0a0894

.field public static final shortcut:I = 0x7f0a0a02

.field public static final spacer:I = 0x7f0a0a12

.field public static final split_action_bar:I = 0x7f0a0a17

.field public static final src_atop:I = 0x7f0a0a1b

.field public static final src_in:I = 0x7f0a0a1c

.field public static final src_over:I = 0x7f0a0a1d

.field public static final start:I = 0x7f0a0a20

.field public static final state_aspect_ratio:I = 0x7f0a0a25

.field public static final state_rotate:I = 0x7f0a0a26

.field public static final state_scale:I = 0x7f0a0a27

.field public static final submenuarrow:I = 0x7f0a0a6d

.field public static final submit_area:I = 0x7f0a0a6e

.field public static final tabMode:I = 0x7f0a0a7e

.field public static final tag_transition_group:I = 0x7f0a0a91

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0a92

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0a93

.field public static final text:I = 0x7f0a0a9e

.field public static final text2:I = 0x7f0a0aa0

.field public static final textSpacerNoButtons:I = 0x7f0a0aa2

.field public static final textSpacerNoTitle:I = 0x7f0a0aa3

.field public static final text_view_rotate:I = 0x7f0a0aa8

.field public static final text_view_scale:I = 0x7f0a0aa9

.field public static final time:I = 0x7f0a0ab5

.field public static final title:I = 0x7f0a0ab6

.field public static final titleDividerNoCustom:I = 0x7f0a0ab7

.field public static final title_template:I = 0x7f0a0ab8

.field public static final toolbar:I = 0x7f0a0abb

.field public static final toolbar_title:I = 0x7f0a0abd

.field public static final top:I = 0x7f0a0abf

.field public static final topPanel:I = 0x7f0a0ac0

.field public static final ucrop:I = 0x7f0a0ad5

.field public static final ucrop_frame:I = 0x7f0a0ad6

.field public static final ucrop_photobox:I = 0x7f0a0ad7

.field public static final uniform:I = 0x7f0a0ade

.field public static final up:I = 0x7f0a0ae0

.field public static final view_overlay:I = 0x7f0a0b64

.field public static final wrap_content:I = 0x7f0a0bea

.field public static final wrapper_controls:I = 0x7f0a0bec

.field public static final wrapper_reset_rotate:I = 0x7f0a0bed

.field public static final wrapper_rotate_by_angle:I = 0x7f0a0bee

.field public static final wrapper_states:I = 0x7f0a0bef


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
