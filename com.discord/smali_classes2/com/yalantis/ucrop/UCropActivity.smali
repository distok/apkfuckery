.class public Lcom/yalantis/ucrop/UCropActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "UCropActivity.java"


# static fields
.field public static final G:Landroid/graphics/Bitmap$CompressFormat;


# instance fields
.field public A:Landroid/view/View;

.field public B:Landroid/graphics/Bitmap$CompressFormat;

.field public C:I

.field public D:[I

.field public E:Lf/n/a/l/c$a;

.field public final F:Landroid/view/View$OnClickListener;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation
.end field

.field public j:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public k:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Lcom/yalantis/ucrop/view/UCropView;

.field public p:Lcom/yalantis/ucrop/view/GestureCropImageView;

.field public q:Lcom/yalantis/ucrop/view/OverlayView;

.field public r:Landroid/view/ViewGroup;

.field public s:Landroid/view/ViewGroup;

.field public t:Landroid/view/ViewGroup;

.field public u:Landroid/view/ViewGroup;

.field public v:Landroid/view/ViewGroup;

.field public w:Landroid/view/ViewGroup;

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public y:Landroid/widget/TextView;

.field public z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    sput-object v0, Lcom/yalantis/ucrop/UCropActivity;->G:Landroid/graphics/Bitmap$CompressFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/yalantis/ucrop/UCropActivity;->n:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->x:Ljava/util/List;

    sget-object v0, Lcom/yalantis/ucrop/UCropActivity;->G:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->B:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v0, 0x5a

    iput v0, p0, Lcom/yalantis/ucrop/UCropActivity;->C:I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->D:[I

    new-instance v0, Lcom/yalantis/ucrop/UCropActivity$a;

    invoke-direct {v0, p0}, Lcom/yalantis/ucrop/UCropActivity$a;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    iput-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->E:Lf/n/a/l/c$a;

    new-instance v0, Lcom/yalantis/ucrop/UCropActivity$b;

    invoke-direct {v0, p0}, Lcom/yalantis/ucrop/UCropActivity$b;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    iput-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->F:Landroid/view/View$OnClickListener;

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    iget-object v1, p0, Lcom/yalantis/ucrop/UCropActivity;->D:[I

    aget v2, v1, p1

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-eq v2, v4, :cond_1

    aget v1, v1, p1

    if-ne v1, v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/yalantis/ucrop/view/GestureCropImageView;->setScaleEnabled(Z)V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    iget-object v1, p0, Lcom/yalantis/ucrop/UCropActivity;->D:[I

    aget v2, v1, p1

    if-eq v2, v4, :cond_2

    aget p1, v1, p1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    :cond_2
    const/4 v3, 0x1

    :cond_3
    invoke-virtual {v0, v3}, Lcom/yalantis/ucrop/view/GestureCropImageView;->setRotateEnabled(Z)V

    return-void
.end method

.method public b(Ljava/lang/Throwable;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.yalantis.ucrop.Error"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object p1

    const/16 v0, 0x60

    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method

.method public final c(I)V
    .locals 7
    .param p1    # I
        .annotation build Landroidx/annotation/IdRes;
        .end annotation
    .end param

    iget-boolean v0, p0, Lcom/yalantis/ucrop/UCropActivity;->m:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->r:Landroid/view/ViewGroup;

    sget v1, Lcom/yalantis/ucrop/R$d;->state_aspect_ratio:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->s:Landroid/view/ViewGroup;

    sget v4, Lcom/yalantis/ucrop/R$d;->state_rotate:I

    if-ne p1, v4, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->t:Landroid/view/ViewGroup;

    sget v5, Lcom/yalantis/ucrop/R$d;->state_scale:I

    if-ne p1, v5, :cond_3

    const/4 v6, 0x1

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->u:Landroid/view/ViewGroup;

    const/16 v6, 0x8

    if-ne p1, v1, :cond_4

    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->v:Landroid/view/ViewGroup;

    if-ne p1, v4, :cond_5

    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    const/16 v1, 0x8

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->w:Landroid/view/ViewGroup;

    if-ne p1, v5, :cond_6

    const/4 v6, 0x0

    :cond_6
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-ne p1, v5, :cond_7

    invoke-virtual {p0, v3}, Lcom/yalantis/ucrop/UCropActivity;->a(I)V

    goto :goto_5

    :cond_7
    if-ne p1, v4, :cond_8

    invoke-virtual {p0, v2}, Lcom/yalantis/ucrop/UCropActivity;->a(I)V

    goto :goto_5

    :cond_8
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Lcom/yalantis/ucrop/UCropActivity;->a(I)V

    :goto_5
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 23

    move-object/from16 v1, p0

    invoke-super/range {p0 .. p1}, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/yalantis/ucrop/R$e;->ucrop_activity_photobox:I

    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget v2, Lcom/yalantis/ucrop/R$a;->ucrop_color_statusbar:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const-string v3, "com.yalantis.ucrop.StatusBarColor"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->f:I

    sget v2, Lcom/yalantis/ucrop/R$a;->ucrop_color_toolbar:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const-string v3, "com.yalantis.ucrop.ToolbarColor"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->e:I

    sget v2, Lcom/yalantis/ucrop/R$a;->ucrop_color_widget_active:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const-string v3, "com.yalantis.ucrop.UcropColorWidgetActive"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    sget v2, Lcom/yalantis/ucrop/R$a;->ucrop_color_toolbar_widget:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const-string v3, "com.yalantis.ucrop.UcropToolbarWidgetColor"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->h:I

    sget v2, Lcom/yalantis/ucrop/R$c;->ucrop_ic_cross:I

    const-string v3, "com.yalantis.ucrop.UcropToolbarCancelDrawable"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->j:I

    sget v2, Lcom/yalantis/ucrop/R$c;->ucrop_ic_done:I

    const-string v3, "com.yalantis.ucrop.UcropToolbarCropDrawable"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->k:I

    const-string v2, "com.yalantis.ucrop.UcropToolbarTitleText"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/yalantis/ucrop/R$g;->ucrop_label_edit_photo:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->d:Ljava/lang/String;

    sget v2, Lcom/yalantis/ucrop/R$a;->ucrop_color_default_logo:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const-string v3, "com.yalantis.ucrop.UcropLogoColor"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->l:I

    const-string v2, "com.yalantis.ucrop.HideBottomControls"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const/4 v4, 0x1

    xor-int/2addr v2, v4

    iput-boolean v2, v1, Lcom/yalantis/ucrop/UCropActivity;->m:Z

    sget v2, Lcom/yalantis/ucrop/R$a;->ucrop_color_crop_background:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    const-string v5, "com.yalantis.ucrop.UcropRootViewBackgroundColor"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->i:I

    iget v2, v1, Lcom/yalantis/ucrop/UCropActivity;->f:I

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    if-eqz v5, :cond_1

    const/high16 v6, -0x80000000

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {v5, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    :cond_1
    sget v2, Lcom/yalantis/ucrop/R$d;->toolbar:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/Toolbar;

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->e:I

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->h:I

    invoke-virtual {v2, v5}, Landroidx/appcompat/widget/Toolbar;->setTitleTextColor(I)V

    sget v5, Lcom/yalantis/ucrop/R$d;->toolbar_title:I

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iget v6, v1, Lcom/yalantis/ucrop/UCropActivity;->h:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v6, v1, Lcom/yalantis/ucrop/UCropActivity;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->j:I

    invoke-static {v1, v5}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget v6, v1, Lcom/yalantis/ucrop/UCropActivity;->h:I

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v5}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v3}, Landroidx/appcompat/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    :cond_2
    sget v2, Lcom/yalantis/ucrop/R$d;->ucrop:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/yalantis/ucrop/view/UCropView;

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->o:Lcom/yalantis/ucrop/view/UCropView;

    invoke-virtual {v2}, Lcom/yalantis/ucrop/view/UCropView;->getCropImageView()Lcom/yalantis/ucrop/view/GestureCropImageView;

    move-result-object v2

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->o:Lcom/yalantis/ucrop/view/UCropView;

    invoke-virtual {v2}, Lcom/yalantis/ucrop/view/UCropView;->getOverlayView()Lcom/yalantis/ucrop/view/OverlayView;

    move-result-object v2

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    iget-object v5, v1, Lcom/yalantis/ucrop/UCropActivity;->E:Lf/n/a/l/c$a;

    invoke-virtual {v2, v5}, Lf/n/a/l/c;->setTransformImageListener(Lf/n/a/l/c$a;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->image_view_logo:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->l:I

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v5, v6}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->ucrop_frame:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->i:I

    invoke-virtual {v2, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iget-boolean v2, v1, Lcom/yalantis/ucrop/UCropActivity;->m:Z

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    const-string v9, "com.yalantis.ucrop.AspectRatioOptions"

    const-string v10, "com.yalantis.ucrop.AspectRatioSelectedByDefault"

    if-eqz v2, :cond_7

    sget v2, Lcom/yalantis/ucrop/R$d;->ucrop_photobox:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget v11, Lcom/yalantis/ucrop/R$e;->ucrop_controls:I

    invoke-static {v1, v11, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v2, Lcom/yalantis/ucrop/R$d;->state_aspect_ratio:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->r:Landroid/view/ViewGroup;

    iget-object v11, v1, Lcom/yalantis/ucrop/UCropActivity;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v11}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->state_rotate:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->s:Landroid/view/ViewGroup;

    iget-object v11, v1, Lcom/yalantis/ucrop/UCropActivity;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v11}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->state_scale:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->t:Landroid/view/ViewGroup;

    iget-object v11, v1, Lcom/yalantis/ucrop/UCropActivity;->F:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v11}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->layout_aspect_ratio:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, v1, Lcom/yalantis/ucrop/UCropActivity;->u:Landroid/view/ViewGroup;

    sget v11, Lcom/yalantis/ucrop/R$d;->layout_rotate_wheel:I

    invoke-virtual {v1, v11}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, v1, Lcom/yalantis/ucrop/UCropActivity;->v:Landroid/view/ViewGroup;

    sget v11, Lcom/yalantis/ucrop/R$d;->layout_scale_wheel:I

    invoke-virtual {v1, v11}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iput-object v11, v1, Lcom/yalantis/ucrop/UCropActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v10, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    const/high16 v13, 0x3f800000    # 1.0f

    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_4

    :cond_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Lcom/yalantis/ucrop/model/AspectRatio;

    invoke-direct {v11, v8, v13, v13}, Lcom/yalantis/ucrop/model/AspectRatio;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v11, Lcom/yalantis/ucrop/model/AspectRatio;

    const/high16 v14, 0x40800000    # 4.0f

    const/high16 v15, 0x40400000    # 3.0f

    invoke-direct {v11, v8, v15, v14}, Lcom/yalantis/ucrop/model/AspectRatio;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v11, Lcom/yalantis/ucrop/model/AspectRatio;

    sget v14, Lcom/yalantis/ucrop/R$g;->ucrop_label_original:I

    invoke-virtual {v1, v14}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v11, v14, v6, v6}, Lcom/yalantis/ucrop/model/AspectRatio;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v11, Lcom/yalantis/ucrop/model/AspectRatio;

    const/high16 v14, 0x40000000    # 2.0f

    invoke-direct {v11, v8, v15, v14}, Lcom/yalantis/ucrop/model/AspectRatio;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v11, Lcom/yalantis/ucrop/model/AspectRatio;

    const/high16 v14, 0x41800000    # 16.0f

    const/high16 v15, 0x41100000    # 9.0f

    invoke-direct {v11, v8, v14, v15}, Lcom/yalantis/ucrop/model/AspectRatio;-><init>(Ljava/lang/String;FF)V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x2

    :cond_4
    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v14, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput v13, v14, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/yalantis/ucrop/model/AspectRatio;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v15

    sget v5, Lcom/yalantis/ucrop/R$e;->ucrop_aspect_ratio:I

    invoke-virtual {v15, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    invoke-virtual {v5, v14}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;

    iget v8, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    invoke-virtual {v15, v8}, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->setActiveColor(I)V

    invoke-virtual {v15, v13}, Lcom/yalantis/ucrop/view/widget/AspectRatioTextView;->setAspectRatio(Lcom/yalantis/ucrop/model/AspectRatio;)V

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v8, v1, Lcom/yalantis/ucrop/UCropActivity;->x:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, -0x1

    const/4 v8, 0x0

    goto :goto_1

    :cond_5
    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->x:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    new-instance v8, Lf/n/a/b;

    invoke-direct {v8, v1}, Lf/n/a/b;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_6
    sget v2, Lcom/yalantis/ucrop/R$d;->text_view_rotate:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->y:Landroid/widget/TextView;

    sget v2, Lcom/yalantis/ucrop/R$d;->rotate_scroll_wheel:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;

    new-instance v8, Lf/n/a/c;

    invoke-direct {v8, v1}, Lf/n/a/c;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    invoke-virtual {v5, v8}, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;->setScrollingListener(Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView$a;)V

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    invoke-virtual {v2, v5}, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;->setMiddleLineColor(I)V

    sget v2, Lcom/yalantis/ucrop/R$d;->wrapper_reset_rotate:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v5, Lf/n/a/d;

    invoke-direct {v5, v1}, Lf/n/a/d;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->wrapper_rotate_by_angle:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v5, Lf/n/a/e;

    invoke-direct {v5, v1}, Lf/n/a/e;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/yalantis/ucrop/R$d;->text_view_scale:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->z:Landroid/widget/TextView;

    sget v2, Lcom/yalantis/ucrop/R$d;->scale_scroll_wheel:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;

    new-instance v8, Lf/n/a/f;

    invoke-direct {v8, v1}, Lf/n/a/f;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    invoke-virtual {v5, v8}, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;->setScrollingListener(Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView$a;)V

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;

    iget v5, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    invoke-virtual {v2, v5}, Lcom/yalantis/ucrop/view/widget/HorizontalProgressWheelView;->setMiddleLineColor(I)V

    sget v2, Lcom/yalantis/ucrop/R$d;->image_view_state_scale:I

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget v5, Lcom/yalantis/ucrop/R$d;->image_view_state_rotate:I

    invoke-virtual {v1, v5}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    sget v8, Lcom/yalantis/ucrop/R$d;->image_view_state_aspect_ratio:I

    invoke-virtual {v1, v8}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    new-instance v11, Lf/n/a/k/d;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v12

    iget v13, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    invoke-direct {v11, v12, v13}, Lf/n/a/k/d;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Lf/n/a/k/d;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    iget v12, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    invoke-direct {v2, v11, v12}, Lf/n/a/k/d;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Lf/n/a/k/d;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget v11, v1, Lcom/yalantis/ucrop/UCropActivity;->g:I

    invoke-direct {v2, v5, v11}, Lf/n/a/k/d;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_7
    const-string v2, "com.yalantis.ucrop.InputUri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v18, v2

    check-cast v18, Landroid/net/Uri;

    const-string v2, "com.yalantis.ucrop.OutputUri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object/from16 v19, v2

    check-cast v19, Landroid/net/Uri;

    const-string v2, "com.yalantis.ucrop.CompressionFormatName"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-static {v2}, Landroid/graphics/Bitmap$CompressFormat;->valueOf(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v8

    goto :goto_3

    :cond_8
    const/4 v8, 0x0

    :goto_3
    if-nez v8, :cond_9

    sget-object v8, Lcom/yalantis/ucrop/UCropActivity;->G:Landroid/graphics/Bitmap$CompressFormat;

    :cond_9
    iput-object v8, v1, Lcom/yalantis/ucrop/UCropActivity;->B:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x5a

    const-string v5, "com.yalantis.ucrop.CompressionQuality"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/yalantis/ucrop/UCropActivity;->C:I

    const-string v2, "com.yalantis.ucrop.AllowedGestures"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v2

    const/4 v5, 0x3

    if-eqz v2, :cond_a

    array-length v8, v2

    if-ne v8, v5, :cond_a

    iput-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->D:[I

    :cond_a
    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    const-string v8, "com.yalantis.ucrop.MaxBitmapSize"

    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v2, v8}, Lf/n/a/l/c;->setMaxBitmapSize(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    const/high16 v8, 0x41200000    # 10.0f

    const-string v11, "com.yalantis.ucrop.MaxScaleMultiplier"

    invoke-virtual {v0, v11, v8}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v8

    invoke-virtual {v2, v8}, Lf/n/a/l/a;->setMaxScaleMultiplier(F)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    const/16 v8, 0x1f4

    const-string v11, "com.yalantis.ucrop.ImageToCropBoundsAnimDuration"

    invoke-virtual {v0, v11, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    int-to-long v11, v8

    invoke-virtual {v2, v11, v12}, Lf/n/a/l/a;->setImageToWrapCropBoundsAnimDuration(J)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    const-string v8, "com.yalantis.ucrop.FreeStyleCrop"

    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setFreestyleCropEnabled(Z)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v11, Lcom/yalantis/ucrop/R$a;->ucrop_color_default_dimmed:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    const-string v11, "com.yalantis.ucrop.DimmedLayerColor"

    invoke-virtual {v0, v11, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setDimmedColor(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    const-string v8, "com.yalantis.ucrop.CircleDimmedLayer"

    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setCircleDimmedLayer(Z)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    const-string v8, "com.yalantis.ucrop.ShowCropFrame"

    invoke-virtual {v0, v8, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setShowCropFrame(Z)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v11, Lcom/yalantis/ucrop/R$a;->ucrop_color_default_crop_frame:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    const-string v11, "com.yalantis.ucrop.CropFrameColor"

    invoke-virtual {v0, v11, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setCropFrameColor(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v11, Lcom/yalantis/ucrop/R$b;->ucrop_default_crop_frame_stoke_width:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const-string v11, "com.yalantis.ucrop.CropFrameStrokeWidth"

    invoke-virtual {v0, v11, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setCropFrameStrokeWidth(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    const-string v8, "com.yalantis.ucrop.ShowCropGrid"

    invoke-virtual {v0, v8, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setShowCropGrid(Z)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    const-string v8, "com.yalantis.ucrop.CropGridRowCount"

    invoke-virtual {v0, v8, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/yalantis/ucrop/view/OverlayView;->setCropGridRowCount(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    const-string v8, "com.yalantis.ucrop.CropGridColumnCount"

    invoke-virtual {v0, v8, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/yalantis/ucrop/view/OverlayView;->setCropGridColumnCount(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/yalantis/ucrop/R$a;->ucrop_color_default_crop_grid:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    const-string v8, "com.yalantis.ucrop.CropGridColor"

    invoke-virtual {v0, v8, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/yalantis/ucrop/view/OverlayView;->setCropGridColor(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->q:Lcom/yalantis/ucrop/view/OverlayView;

    invoke-virtual/range {p0 .. p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/yalantis/ucrop/R$b;->ucrop_default_crop_grid_stoke_width:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const-string v8, "com.yalantis.ucrop.CropGridStrokeWidth"

    invoke-virtual {v0, v8, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/yalantis/ucrop/view/OverlayView;->setCropGridStrokeWidth(I)V

    const-string v2, "com.yalantis.ucrop.AspectRatioX"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v2

    const-string v7, "com.yalantis.ucrop.AspectRatioY"

    invoke-virtual {v0, v7, v6}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v7

    invoke-virtual {v0, v10, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    cmpl-float v10, v2, v6

    if-lez v10, :cond_c

    cmpl-float v10, v7, v6

    if-lez v10, :cond_c

    iget-object v6, v1, Lcom/yalantis/ucrop/UCropActivity;->r:Landroid/view/ViewGroup;

    if-eqz v6, :cond_b

    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_b
    iget-object v6, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    div-float/2addr v2, v7

    invoke-virtual {v6, v2}, Lf/n/a/l/a;->setTargetAspectRatio(F)V

    goto :goto_4

    :cond_c
    if-eqz v9, :cond_d

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v8, v2, :cond_d

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/yalantis/ucrop/model/AspectRatio;

    iget v6, v6, Lcom/yalantis/ucrop/model/AspectRatio;->e:F

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/yalantis/ucrop/model/AspectRatio;

    iget v7, v7, Lcom/yalantis/ucrop/model/AspectRatio;->f:F

    div-float/2addr v6, v7

    invoke-virtual {v2, v6}, Lf/n/a/l/a;->setTargetAspectRatio(F)V

    goto :goto_4

    :cond_d
    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v2, v6}, Lf/n/a/l/a;->setTargetAspectRatio(F)V

    :goto_4
    const-string v2, "com.yalantis.ucrop.MaxSizeX"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v6, "com.yalantis.ucrop.MaxSizeY"

    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v2, :cond_e

    if-lez v0, :cond_e

    iget-object v6, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v6, v2}, Lf/n/a/l/a;->setMaxResultImageSizeX(I)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v2, v0}, Lf/n/a/l/a;->setMaxResultImageSizeY(I)V

    :cond_e
    if-eqz v18, :cond_f

    if-eqz v19, :cond_f

    :try_start_0
    iget-object v0, v1, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    invoke-virtual {v0}, Lf/n/a/l/c;->getMaxBitmapSize()I

    move-result v21

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v17

    new-instance v2, Lf/n/a/l/b;

    invoke-direct {v2, v0}, Lf/n/a/l/b;-><init>(Lf/n/a/l/c;)V

    new-instance v0, Lf/n/a/j/b;

    move-object/from16 v16, v0

    move/from16 v20, v21

    move-object/from16 v22, v2

    invoke-direct/range {v16 .. v22}, Lf/n/a/j/b;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;IILf/n/a/h/b;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    invoke-virtual {v1, v0}, Lcom/yalantis/ucrop/UCropActivity;->b(Ljava/lang/Throwable;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    goto :goto_5

    :cond_f
    new-instance v0, Ljava/lang/NullPointerException;

    sget v2, Lcom/yalantis/ucrop/R$g;->ucrop_error_input_data_is_absent:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/yalantis/ucrop/UCropActivity;->b(Ljava/lang/Throwable;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    :goto_5
    iget-boolean v0, v1, Lcom/yalantis/ucrop/UCropActivity;->m:Z

    if-eqz v0, :cond_11

    iget-object v0, v1, Lcom/yalantis/ucrop/UCropActivity;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_10

    sget v0, Lcom/yalantis/ucrop/R$d;->state_aspect_ratio:I

    invoke-virtual {v1, v0}, Lcom/yalantis/ucrop/UCropActivity;->c(I)V

    goto :goto_6

    :cond_10
    sget v0, Lcom/yalantis/ucrop/R$d;->state_scale:I

    invoke-virtual {v1, v0}, Lcom/yalantis/ucrop/UCropActivity;->c(I)V

    goto :goto_6

    :cond_11
    invoke-virtual {v1, v3}, Lcom/yalantis/ucrop/UCropActivity;->a(I)V

    :goto_6
    iget-object v0, v1, Lcom/yalantis/ucrop/UCropActivity;->A:Landroid/view/View;

    if-nez v0, :cond_12

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, v1, Lcom/yalantis/ucrop/UCropActivity;->A:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    sget v2, Lcom/yalantis/ucrop/R$d;->toolbar:I

    invoke-virtual {v0, v5, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->A:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, v1, Lcom/yalantis/ucrop/UCropActivity;->A:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    :cond_12
    sget v0, Lcom/yalantis/ucrop/R$d;->ucrop_photobox:I

    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iget-object v2, v1, Lcom/yalantis/ucrop/UCropActivity;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/yalantis/ucrop/R$f;->ucrop_menu_activity:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v0, Lcom/yalantis/ucrop/R$d;->menu_loader:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/yalantis/ucrop/UCropActivity;->h:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    sget v1, Lcom/yalantis/ucrop/R$g;->ucrop_mutate_exception_hint:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v2

    const-string v1, "%s - %s"

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "UCropActivity"

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    :cond_0
    sget v0, Lcom/yalantis/ucrop/R$d;->menu_crop:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    iget v0, p0, Lcom/yalantis/ucrop/UCropActivity;->k:I

    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/yalantis/ucrop/UCropActivity;->h:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_1
    return v2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 13

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/yalantis/ucrop/R$d;->menu_crop:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->A:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iput-boolean v1, p0, Lcom/yalantis/ucrop/UCropActivity;->n:Z

    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->supportInvalidateOptionsMenu()V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    iget-object v4, p0, Lcom/yalantis/ucrop/UCropActivity;->B:Landroid/graphics/Bitmap$CompressFormat;

    iget v5, p0, Lcom/yalantis/ucrop/UCropActivity;->C:I

    new-instance v11, Lf/n/a/g;

    invoke-direct {v11, p0}, Lf/n/a/g;-><init>(Lcom/yalantis/ucrop/UCropActivity;)V

    invoke-virtual {v0}, Lf/n/a/l/a;->i()V

    const/4 v12, 0x0

    invoke-virtual {v0, v12}, Lf/n/a/l/a;->setImageToWrapCropBounds(Z)V

    new-instance v9, Lf/n/a/i/c;

    iget-object v1, v0, Lf/n/a/l/a;->s:Landroid/graphics/RectF;

    iget-object v2, v0, Lf/n/a/l/c;->d:[F

    invoke-static {v2}, Lf/h/a/f/f/n/g;->e0([F)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v3

    invoke-virtual {v0}, Lf/n/a/l/c;->getCurrentAngle()F

    move-result v6

    invoke-direct {v9, v1, v2, v3, v6}, Lf/n/a/i/c;-><init>(Landroid/graphics/RectF;Landroid/graphics/RectF;FF)V

    new-instance v10, Lf/n/a/i/a;

    iget v2, v0, Lf/n/a/l/a;->B:I

    iget v3, v0, Lf/n/a/l/a;->C:I

    invoke-virtual {v0}, Lf/n/a/l/c;->getImageInputPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lf/n/a/l/c;->getImageOutputPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lf/n/a/l/c;->getExifInfo()Lf/n/a/i/b;

    move-result-object v8

    move-object v1, v10

    invoke-direct/range {v1 .. v8}, Lf/n/a/i/a;-><init>(IILandroid/graphics/Bitmap$CompressFormat;ILjava/lang/String;Ljava/lang/String;Lf/n/a/i/b;)V

    new-instance v1, Lf/n/a/j/a;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v0}, Lf/n/a/l/c;->getViewBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    move-object v6, v1

    invoke-direct/range {v6 .. v11}, Lf/n/a/j/a;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Lf/n/a/i/c;Lf/n/a/i/a;Lf/n/a/h/a;)V

    new-array v0, v12, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    sget v0, Lcom/yalantis/ucrop/R$d;->menu_crop:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/yalantis/ucrop/UCropActivity;->n:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/yalantis/ucrop/R$d;->menu_loader:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/yalantis/ucrop/UCropActivity;->n:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStop()V

    iget-object v0, p0, Lcom/yalantis/ucrop/UCropActivity;->p:Lcom/yalantis/ucrop/view/GestureCropImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/n/a/l/a;->i()V

    :cond_0
    return-void
.end method
