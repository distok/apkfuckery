.class public Lcom/yalantis/ucrop/view/GestureCropImageView;
.super Lf/n/a/l/a;
.source "GestureCropImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yalantis/ucrop/view/GestureCropImageView$c;,
        Lcom/yalantis/ucrop/view/GestureCropImageView$b;,
        Lcom/yalantis/ucrop/view/GestureCropImageView$d;
    }
.end annotation


# instance fields
.field public E:Landroid/view/ScaleGestureDetector;

.field public F:Lf/n/a/k/c;

.field public G:Landroid/view/GestureDetector;

.field public H:F

.field public I:F

.field public J:Z

.field public K:Z

.field public L:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lf/n/a/l/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->J:Z

    iput-boolean p1, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->K:Z

    const/4 p1, 0x5

    iput p1, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->L:I

    return-void
.end method


# virtual methods
.method public c()V
    .locals 5

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lf/n/a/l/c;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/yalantis/ucrop/view/GestureCropImageView$b;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/yalantis/ucrop/view/GestureCropImageView$b;-><init>(Lcom/yalantis/ucrop/view/GestureCropImageView;Lcom/yalantis/ucrop/view/GestureCropImageView$a;)V

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->G:Landroid/view/GestureDetector;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/yalantis/ucrop/view/GestureCropImageView$d;

    invoke-direct {v2, p0, v3}, Lcom/yalantis/ucrop/view/GestureCropImageView$d;-><init>(Lcom/yalantis/ucrop/view/GestureCropImageView;Lcom/yalantis/ucrop/view/GestureCropImageView$a;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->E:Landroid/view/ScaleGestureDetector;

    new-instance v0, Lf/n/a/k/c;

    new-instance v1, Lcom/yalantis/ucrop/view/GestureCropImageView$c;

    invoke-direct {v1, p0, v3}, Lcom/yalantis/ucrop/view/GestureCropImageView$c;-><init>(Lcom/yalantis/ucrop/view/GestureCropImageView;Lcom/yalantis/ucrop/view/GestureCropImageView$a;)V

    invoke-direct {v0, v1}, Lf/n/a/k/c;-><init>(Lf/n/a/k/c$a;)V

    iput-object v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->F:Lf/n/a/k/c;

    return-void
.end method

.method public getDoubleTapScaleSteps()I
    .locals 1

    iget v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->L:I

    return v0
.end method

.method public getDoubleTapTargetScale()F
    .locals 5

    invoke-virtual {p0}, Lf/n/a/l/c;->getCurrentScale()F

    move-result v0

    invoke-virtual {p0}, Lf/n/a/l/a;->getMaxScale()F

    move-result v1

    invoke-virtual {p0}, Lf/n/a/l/a;->getMinScale()F

    move-result v2

    div-float/2addr v1, v2

    float-to-double v1, v1

    iget v3, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->L:I

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    div-float/2addr v4, v3

    float-to-double v3, v4

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float v0, v0, v1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lf/n/a/l/a;->i()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float/2addr v3, v0

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr v3, v0

    iput v3, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->H:F

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    add-float/2addr v4, v3

    div-float/2addr v4, v0

    iput v4, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->I:F

    :cond_1
    iget-object v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->G:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->K:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->E:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_2
    iget-boolean v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->J:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->F:Lf/n/a/k/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_b

    const/4 v5, -0x1

    if-eq v3, v2, :cond_a

    const/4 v6, 0x2

    if-eq v3, v6, :cond_5

    const/4 v1, 0x5

    if-eq v3, v1, :cond_4

    const/4 v1, 0x6

    if-eq v3, v1, :cond_3

    goto/16 :goto_1

    :cond_3
    iput v5, v0, Lf/n/a/k/c;->f:I

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, v0, Lf/n/a/k/c;->a:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, v0, Lf/n/a/k/c;->b:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    iput v1, v0, Lf/n/a/k/c;->f:I

    iput v4, v0, Lf/n/a/k/c;->g:F

    iput-boolean v2, v0, Lf/n/a/k/c;->h:Z

    goto/16 :goto_1

    :cond_5
    iget v3, v0, Lf/n/a/k/c;->e:I

    if-eq v3, v5, :cond_c

    iget v3, v0, Lf/n/a/k/c;->f:I

    if-eq v3, v5, :cond_c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    iget v5, v0, Lf/n/a/k/c;->f:I

    if-le v3, v5, :cond_c

    iget v3, v0, Lf/n/a/k/c;->e:I

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v5, v0, Lf/n/a/k/c;->e:I

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    iget v6, v0, Lf/n/a/k/c;->f:I

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    iget v7, v0, Lf/n/a/k/c;->f:I

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    iget-boolean v8, v0, Lf/n/a/k/c;->h:Z

    if-eqz v8, :cond_6

    iput v4, v0, Lf/n/a/k/c;->g:F

    iput-boolean v1, v0, Lf/n/a/k/c;->h:Z

    goto :goto_0

    :cond_6
    iget v1, v0, Lf/n/a/k/c;->a:F

    iget v4, v0, Lf/n/a/k/c;->b:F

    iget v8, v0, Lf/n/a/k/c;->c:F

    iget v9, v0, Lf/n/a/k/c;->d:F

    sub-float/2addr v4, v9

    float-to-double v9, v4

    sub-float/2addr v1, v8

    float-to-double v11, v1

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v1, v8

    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    double-to-float v1, v8

    sub-float v4, v7, v5

    float-to-double v8, v4

    sub-float v4, v6, v3

    float-to-double v10, v4

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v4, v8

    float-to-double v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    double-to-float v4, v8

    const/high16 v8, 0x43b40000    # 360.0f

    rem-float/2addr v4, v8

    rem-float/2addr v1, v8

    sub-float/2addr v4, v1

    iput v4, v0, Lf/n/a/k/c;->g:F

    const/high16 v1, -0x3ccc0000    # -180.0f

    cmpg-float v1, v4, v1

    if-gez v1, :cond_7

    add-float/2addr v4, v8

    iput v4, v0, Lf/n/a/k/c;->g:F

    goto :goto_0

    :cond_7
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v4, v1

    if-lez v1, :cond_8

    sub-float/2addr v4, v8

    iput v4, v0, Lf/n/a/k/c;->g:F

    :cond_8
    :goto_0
    iget-object v1, v0, Lf/n/a/k/c;->i:Lf/n/a/k/c$a;

    if-eqz v1, :cond_9

    check-cast v1, Lcom/yalantis/ucrop/view/GestureCropImageView$c;

    iget-object v1, v1, Lcom/yalantis/ucrop/view/GestureCropImageView$c;->a:Lcom/yalantis/ucrop/view/GestureCropImageView;

    iget v4, v0, Lf/n/a/k/c;->g:F

    iget v8, v1, Lcom/yalantis/ucrop/view/GestureCropImageView;->H:F

    iget v9, v1, Lcom/yalantis/ucrop/view/GestureCropImageView;->I:F

    invoke-virtual {v1, v4, v8, v9}, Lf/n/a/l/c;->e(FFF)V

    :cond_9
    iput v6, v0, Lf/n/a/k/c;->a:F

    iput v7, v0, Lf/n/a/k/c;->b:F

    iput v3, v0, Lf/n/a/k/c;->c:F

    iput v5, v0, Lf/n/a/k/c;->d:F

    goto :goto_1

    :cond_a
    iput v5, v0, Lf/n/a/k/c;->e:I

    goto :goto_1

    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, v0, Lf/n/a/k/c;->c:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, v0, Lf/n/a/k/c;->d:F

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    iput v1, v0, Lf/n/a/k/c;->e:I

    iput v4, v0, Lf/n/a/k/c;->g:F

    iput-boolean v2, v0, Lf/n/a/k/c;->h:Z

    :cond_c
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    and-int/lit16 p1, p1, 0xff

    if-ne p1, v2, :cond_d

    invoke-virtual {p0, v2}, Lf/n/a/l/a;->setImageToWrapCropBounds(Z)V

    :cond_d
    return v2
.end method

.method public setDoubleTapScaleSteps(I)V
    .locals 0

    iput p1, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->L:I

    return-void
.end method

.method public setRotateEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->J:Z

    return-void
.end method

.method public setScaleEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/yalantis/ucrop/view/GestureCropImageView;->K:Z

    return-void
.end method
