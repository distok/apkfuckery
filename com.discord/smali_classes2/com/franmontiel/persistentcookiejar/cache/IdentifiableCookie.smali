.class public Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;
.super Ljava/lang/Object;
.source "IdentifiableCookie.java"


# instance fields
.field public a:Lb0/n;


# direct methods
.method public constructor <init>(Lb0/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    instance-of v0, p1, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    check-cast p1, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;

    iget-object v0, p1, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v0, v0, Lb0/n;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v2, v2, Lb0/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v0, v0, Lb0/n;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v2, v2, Lb0/n;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v0, v0, Lb0/n;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v2, v2, Lb0/n;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-boolean v0, p1, Lb0/n;->f:Z

    iget-object v2, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-boolean v3, v2, Lb0/n;->f:Z

    if-ne v0, v3, :cond_1

    iget-boolean p1, p1, Lb0/n;->i:Z

    iget-boolean v0, v2, Lb0/n;->i:Z

    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v0, v0, Lb0/n;->a:Ljava/lang/String;

    const/16 v1, 0x20f

    const/16 v2, 0x1f

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->T(Ljava/lang/String;II)I

    move-result v0

    iget-object v1, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v1, v1, Lb0/n;->d:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->T(Ljava/lang/String;II)I

    move-result v0

    iget-object v1, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-object v1, v1, Lb0/n;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lf/e/c/a/a;->T(Ljava/lang/String;II)I

    move-result v0

    iget-object v1, p0, Lcom/franmontiel/persistentcookiejar/cache/IdentifiableCookie;->a:Lb0/n;

    iget-boolean v2, v1, Lb0/n;->f:Z

    xor-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, v1, Lb0/n;->i:Z

    xor-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method
