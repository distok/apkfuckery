.class public Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;
.super Landroidx/preference/Preference;
.source "ColorPreferenceCompat.java"

# interfaces
.implements Lf/i/a/a/g;


# instance fields
.field public d:I

.field public e:Z

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:I

.field public m:[I

.field public n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 p1, -0x1000000

    iput p1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->d:I

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setPersistent(Z)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_showDialog:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->e:Z

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_dialogType:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->f:I

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_colorShape:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->g:I

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_allowPresets:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->h:Z

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_allowCustom:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->i:Z

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_showAlphaSlider:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->j:Z

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_showColorShades:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->k:Z

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_previewSize:I

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->l:I

    sget v0, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_colorPresets:I

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    sget v1, Lcom/jaredrummler/android/colorpicker/R$f;->ColorPreference_cpv_dialogTitle:I

    sget v2, Lcom/jaredrummler/android/colorpicker/R$e;->cpv_default_title:I

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->n:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->m:[I

    goto :goto_0

    :cond_0
    sget-object v0, Lf/i/a/a/e;->E:[I

    iput-object v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->m:[I

    :goto_0
    iget v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->g:I

    if-ne v0, p1, :cond_2

    iget v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->l:I

    if-ne v0, p1, :cond_1

    sget p1, Lcom/jaredrummler/android/colorpicker/R$d;->cpv_preference_circle_large:I

    goto :goto_1

    :cond_1
    sget p1, Lcom/jaredrummler/android/colorpicker/R$d;->cpv_preference_circle:I

    :goto_1
    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setWidgetLayoutResource(I)V

    goto :goto_3

    :cond_2
    iget v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->l:I

    if-ne v0, p1, :cond_3

    sget p1, Lcom/jaredrummler/android/colorpicker/R$d;->cpv_preference_square_large:I

    goto :goto_2

    :cond_3
    sget p1, Lcom/jaredrummler/android/colorpicker/R$d;->cpv_preference_square:I

    :goto_2
    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setWidgetLayoutResource(I)V

    :goto_3
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public f()Landroidx/fragment/app/FragmentActivity;
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Landroidx/fragment/app/FragmentActivity;

    if-eqz v1, :cond_0

    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    return-object v0

    :cond_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Landroidx/fragment/app/FragmentActivity;

    if-eqz v1, :cond_1

    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error getting activity from context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onAttached()V
    .locals 3

    invoke-super {p0}, Landroidx/preference/Preference;->onAttached()V

    iget-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->f()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "color_"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lf/i/a/a/e;

    if-eqz v0, :cond_0

    iput-object p0, v0, Lf/i/a/a/e;->d:Lf/i/a/a/g;

    :cond_0
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/jaredrummler/android/colorpicker/R$c;->cpv_preference_preview_color_panel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/jaredrummler/android/colorpicker/ColorPanelView;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->d:I

    invoke-virtual {p1, v0}, Lcom/jaredrummler/android/colorpicker/ColorPanelView;->setColor(I)V

    :cond_0
    return-void
.end method

.method public onClick()V
    .locals 4

    invoke-super {p0}, Landroidx/preference/Preference;->onClick()V

    iget-boolean v0, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lf/i/a/a/e;->E:[I

    new-instance v0, Lf/i/a/a/e$k;

    invoke-direct {v0}, Lf/i/a/a/e$k;-><init>()V

    iget v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->f:I

    iput v1, v0, Lf/i/a/a/e$k;->f:I

    iget v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->n:I

    iput v1, v0, Lf/i/a/a/e$k;->a:I

    iget v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->g:I

    iput v1, v0, Lf/i/a/a/e$k;->n:I

    iget-object v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->m:[I

    iput-object v1, v0, Lf/i/a/a/e$k;->g:[I

    iget-boolean v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->h:Z

    iput-boolean v1, v0, Lf/i/a/a/e$k;->j:Z

    iget-boolean v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->i:Z

    iput-boolean v1, v0, Lf/i/a/a/e$k;->k:Z

    iget-boolean v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->j:Z

    iput-boolean v1, v0, Lf/i/a/a/e$k;->i:Z

    iget-boolean v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->k:Z

    iput-boolean v1, v0, Lf/i/a/a/e$k;->m:Z

    iget v1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->d:I

    iput v1, v0, Lf/i/a/a/e$k;->h:I

    invoke-virtual {v0}, Lf/i/a/a/e$k;->a()Lf/i/a/a/e;

    move-result-object v0

    iput-object p0, v0, Lf/i/a/a/e;->d:Lf/i/a/a/g;

    invoke-virtual {p0}, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->f()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "color_"

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_0
    return-void
.end method

.method public onColorReset(I)V
    .locals 0

    return-void
.end method

.method public onColorSelected(II)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    iput p2, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->d:I

    invoke-virtual {p0, p2}, Landroidx/preference/Preference;->persistInt(I)Z

    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyChanged()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->callChangeListener(Ljava/lang/Object;)Z

    return-void
.end method

.method public onDialogDismissed(I)V
    .locals 0

    return-void
.end method

.method public onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    const/high16 v0, -0x1000000

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public onSetInitialValue(Ljava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/preference/Preference;->onSetInitialValue(Ljava/lang/Object;)V

    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->d:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->persistInt(I)Z

    goto :goto_0

    :cond_0
    const/high16 p1, -0x1000000

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->getPersistedInt(I)I

    move-result p1

    iput p1, p0, Lcom/jaredrummler/android/colorpicker/ColorPreferenceCompat;->d:I

    :goto_0
    return-void
.end method
