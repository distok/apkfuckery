.class public Lcom/otaliastudios/cameraview/CameraView$b$b;
.super Ljava/lang/Object;
.source "CameraView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/otaliastudios/cameraview/CameraView$b;->b(Lf/l/a/o/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lf/l/a/o/b;

.field public final synthetic e:Lcom/otaliastudios/cameraview/CameraView$b;


# direct methods
.method public constructor <init>(Lcom/otaliastudios/cameraview/CameraView$b;Lf/l/a/o/b;)V
    .locals 0

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->e:Lcom/otaliastudios/cameraview/CameraView$b;

    iput-object p2, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->d:Lf/l/a/o/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->e:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dispatchFrame: executing. Passing"

    aput-object v3, v1, v2

    iget-object v3, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->d:Lf/l/a/o/b;

    invoke-virtual {v3}, Lf/l/a/o/b;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const/4 v3, 0x2

    const-string v5, "to processors."

    aput-object v5, v1, v3

    invoke-virtual {v0, v2, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->e:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/l/a/o/d;

    :try_start_0
    iget-object v5, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->d:Lf/l/a/o/b;

    invoke-interface {v1, v5}, Lf/l/a/o/d;->a(Lf/l/a/o/b;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v5, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->e:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v5, v5, Lcom/otaliastudios/cameraview/CameraView$b;->b:Lf/l/a/b;

    new-array v6, v3, [Ljava/lang/Object;

    const-string v7, "Frame processor crashed:"

    aput-object v7, v6, v2

    aput-object v1, v6, v4

    invoke-virtual {v5, v3, v6}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$b;->d:Lf/l/a/o/b;

    invoke-virtual {v0}, Lf/l/a/o/b;->b()V

    return-void
.end method
