.class public Lcom/otaliastudios/cameraview/CameraView;
.super Landroid/widget/FrameLayout;
.source "CameraView.java"

# interfaces
.implements Landroidx/lifecycle/LifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/otaliastudios/cameraview/CameraView$b;
    }
.end annotation


# static fields
.field public static final F:Ljava/lang/String;

.field public static final G:Lf/l/a/b;


# instance fields
.field public A:Lf/l/a/q/c;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public B:Lf/l/a/r/c;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public C:Z

.field public D:Z

.field public E:Lf/l/a/t/b;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Lf/l/a/p/a;",
            "Lf/l/a/p/b;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lf/l/a/l/k;

.field public i:Lf/l/a/l/d;

.field public j:Lf/l/a/n/b;

.field public k:I

.field public l:Landroid/os/Handler;

.field public m:Ljava/util/concurrent/Executor;

.field public n:Lcom/otaliastudios/cameraview/CameraView$b;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public o:Lf/l/a/v/a;

.field public p:Lf/l/a/q/d;

.field public q:Lf/l/a/m/j;

.field public r:Lf/l/a/w/b;

.field public s:Landroid/media/MediaActionSound;

.field public t:Lf/l/a/r/a;

.field public u:Ljava/util/List;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/a;",
            ">;"
        }
    .end annotation
.end field

.field public v:Ljava/util/List;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/l/a/o/d;",
            ">;"
        }
    .end annotation
.end field

.field public w:Landroidx/lifecycle/Lifecycle;

.field public x:Lf/l/a/p/e;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public y:Lf/l/a/p/g;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field

.field public z:Lf/l/a/p/f;
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/otaliastudios/cameraview/CameraView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/otaliastudios/cameraview/CameraView;->F:Ljava/lang/String;

    new-instance v1, Lf/l/a/b;

    invoke-direct {v1, v0}, Lf/l/a/b;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 43
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct/range {p0 .. p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v2, Ljava/util/HashMap;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v2, v0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, v0, Lcom/otaliastudios/cameraview/CameraView;->u:Ljava/util/List;

    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, v0, Lcom/otaliastudios/cameraview/CameraView;->v:Ljava/util/List;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->isInEditMode()Z

    move-result v2

    iput-boolean v2, v0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-eqz v2, :cond_0

    goto/16 :goto_8

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setWillNotDraw(Z)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget-object v4, Lcom/otaliastudios/cameraview/R$c;->CameraView:[I

    move-object/from16 v5, p2

    invoke-virtual {v3, v5, v4, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    sget v4, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPreview:I

    sget-object v5, Lf/l/a/l/k;->f:Lf/l/a/l/k;

    invoke-virtual {v5}, Lf/l/a/l/k;->g()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    sget v5, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFacing:I

    sget-object v6, Lf/l/a/l/e;->d:Lf/l/a/l/e;

    invoke-static {v6}, Lf/l/a/e;->a(Lf/l/a/l/e;)Z

    move-result v7

    if-eqz v7, :cond_1

    goto :goto_0

    :cond_1
    sget-object v7, Lf/l/a/l/e;->e:Lf/l/a/l/e;

    invoke-static {v7}, Lf/l/a/e;->a(Lf/l/a/l/e;)Z

    move-result v8

    if-eqz v8, :cond_2

    move-object v6, v7

    :cond_2
    :goto_0
    invoke-virtual {v6}, Lf/l/a/l/e;->g()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v5

    sget v6, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFlash:I

    sget-object v7, Lf/l/a/l/f;->d:Lf/l/a/l/f;

    invoke-virtual {v7}, Lf/l/a/l/f;->g()I

    move-result v7

    invoke-virtual {v3, v6, v7}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v6

    sget v7, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGrid:I

    sget-object v8, Lf/l/a/l/g;->d:Lf/l/a/l/g;

    invoke-virtual {v8}, Lf/l/a/l/g;->g()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v7

    sget v8, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraWhiteBalance:I

    sget-object v9, Lf/l/a/l/m;->d:Lf/l/a/l/m;

    invoke-virtual {v9}, Lf/l/a/l/m;->g()I

    move-result v9

    invoke-virtual {v3, v8, v9}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v8

    sget v9, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraMode:I

    sget-object v10, Lf/l/a/l/i;->d:Lf/l/a/l/i;

    invoke-virtual {v10}, Lf/l/a/l/i;->g()I

    move-result v10

    invoke-virtual {v3, v9, v10}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v9

    sget v10, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraHdr:I

    sget-object v11, Lf/l/a/l/h;->d:Lf/l/a/l/h;

    invoke-virtual {v11}, Lf/l/a/l/h;->g()I

    move-result v11

    invoke-virtual {v3, v10, v11}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v10

    sget v11, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraAudio:I

    sget-object v12, Lf/l/a/l/a;->e:Lf/l/a/l/a;

    invoke-virtual {v12}, Lf/l/a/l/a;->g()I

    move-result v12

    invoke-virtual {v3, v11, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoCodec:I

    sget-object v13, Lf/l/a/l/l;->d:Lf/l/a/l/l;

    invoke-virtual {v13}, Lf/l/a/l/l;->g()I

    move-result v13

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraAudioCodec:I

    sget-object v14, Lf/l/a/l/b;->d:Lf/l/a/l/b;

    invoke-virtual {v14}, Lf/l/a/l/b;->g()I

    move-result v14

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraEngine:I

    sget-object v15, Lf/l/a/l/d;->d:Lf/l/a/l/d;

    invoke-virtual {v15}, Lf/l/a/l/d;->g()I

    move-result v15

    invoke-virtual {v3, v14, v15}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v14

    sget v15, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureFormat:I

    sget-object v16, Lf/l/a/l/j;->d:Lf/l/a/l/j;

    invoke-virtual/range {v16 .. v16}, Lf/l/a/l/j;->g()I

    move-result v2

    invoke-virtual {v3, v15, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    sget v15, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPlaySounds:I

    move/from16 p2, v12

    const/4 v12, 0x1

    invoke-virtual {v3, v15, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v15

    move/from16 v16, v2

    sget v2, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraUseDeviceOrientation:I

    invoke-virtual {v3, v2, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraExperimental:I

    move/from16 v19, v13

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    iput-boolean v12, v0, Lcom/otaliastudios/cameraview/CameraView;->C:Z

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraRequestPermissions:I

    const/4 v13, 0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    iput-boolean v12, v0, Lcom/otaliastudios/cameraview/CameraView;->f:Z

    invoke-static {v4}, Lf/l/a/l/k;->f(I)Lf/l/a/l/k;

    move-result-object v4

    iput-object v4, v0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    invoke-static {v14}, Lf/l/a/l/d;->f(I)Lf/l/a/l/d;

    move-result-object v4

    iput-object v4, v0, Lcom/otaliastudios/cameraview/CameraView;->i:Lf/l/a/l/d;

    sget v4, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGridColor:I

    sget v12, Lf/l/a/q/c;->i:I

    invoke-virtual {v3, v4, v12}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoMaxSize:I

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v12

    float-to-long v13, v12

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoMaxDuration:I

    move-wide/from16 v21, v13

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoBitRate:I

    invoke-virtual {v3, v14, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v14

    move/from16 v23, v14

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraAudioBitRate:I

    invoke-virtual {v3, v14, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v14

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPreviewFrameRate:I

    move/from16 v24, v12

    const/4 v12, 0x0

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v13

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPreviewFrameRateExact:I

    move/from16 v25, v13

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraAutoFocusResetDelay:I

    move/from16 v26, v12

    const/16 v12, 0xbb8

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    int-to-long v12, v12

    move-wide/from16 v27, v12

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureMetering:I

    const/4 v13, 0x1

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSnapshotMetering:I

    move/from16 v29, v12

    const/4 v12, 0x0

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v13

    move/from16 v30, v13

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraSnapshotMaxWidth:I

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    move/from16 v31, v13

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraSnapshotMaxHeight:I

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    move/from16 v32, v13

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFrameProcessingMaxWidth:I

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    move/from16 v33, v13

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFrameProcessingMaxHeight:I

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    move/from16 v34, v13

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFrameProcessingFormat:I

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFrameProcessingPoolSize:I

    move/from16 v35, v13

    const/4 v13, 0x2

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFrameProcessingExecutors:I

    move/from16 v36, v12

    const/4 v12, 0x1

    invoke-virtual {v3, v13, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    new-instance v13, Ljava/util/ArrayList;

    move/from16 v18, v12

    const/4 v12, 0x3

    invoke-direct {v13, v12}, Ljava/util/ArrayList;-><init>(I)V

    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeMinWidth:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v38

    if-eqz v38, :cond_3

    move/from16 v38, v14

    const/4 v14, 0x0

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    invoke-static {v12}, Lf/h/a/f/f/n/g;->R(I)Lf/l/a/w/c;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move/from16 v38, v14

    const/4 v14, 0x0

    :goto_1
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeMaxWidth:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    invoke-static {v12}, Lf/h/a/f/f/n/g;->P(I)Lf/l/a/w/c;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeMinHeight:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    invoke-static {v12}, Lf/h/a/f/f/n/g;->Q(I)Lf/l/a/w/c;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeMaxHeight:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    invoke-static {v12}, Lf/h/a/f/f/n/g;->O(I)Lf/l/a/w/c;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeMinArea:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    new-instance v14, Lf/l/a/w/l;

    invoke-direct {v14, v12}, Lf/l/a/w/l;-><init>(I)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeMaxArea:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v14

    if-eqz v14, :cond_8

    const/4 v14, 0x0

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    new-instance v14, Lf/l/a/w/k;

    invoke-direct {v14, v12}, Lf/l/a/w/k;-><init>(I)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v12

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeAspectRatio:I

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v14

    if-eqz v14, :cond_9

    invoke-virtual {v3, v12}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lf/l/a/w/a;->h(Ljava/lang/String;)Lf/l/a/w/a;

    move-result-object v12

    invoke-virtual {v12}, Lf/l/a/w/a;->i()F

    move-result v12

    new-instance v14, Lf/l/a/w/h;

    move/from16 v39, v11

    const/4 v11, 0x0

    invoke-direct {v14, v12, v11}, Lf/l/a/w/h;-><init>(FF)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v11

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    move/from16 v39, v11

    :goto_2
    sget v11, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeSmallest:I

    const/4 v12, 0x0

    invoke-virtual {v3, v11, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v11

    if-eqz v11, :cond_a

    new-instance v11, Lf/l/a/w/j;

    invoke-direct {v11}, Lf/l/a/w/j;-><init>()V

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    sget v11, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraPictureSizeBiggest:I

    invoke-virtual {v3, v11, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v11

    if-eqz v11, :cond_b

    new-instance v11, Lf/l/a/w/i;

    invoke-direct {v11}, Lf/l/a/w/i;-><init>()V

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-virtual {v13}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_c

    new-array v11, v12, [Lf/l/a/w/c;

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Lf/l/a/w/c;

    invoke-static {v11}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v11

    goto :goto_3

    :cond_c
    new-instance v11, Lf/l/a/w/i;

    invoke-direct {v11}, Lf/l/a/w/i;-><init>()V

    :goto_3
    new-instance v12, Ljava/util/ArrayList;

    const/4 v13, 0x3

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(I)V

    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeMinWidth:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v14

    if-eqz v14, :cond_d

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    invoke-static {v13}, Lf/h/a/f/f/n/g;->R(I)Lf/l/a/w/c;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_d
    const/4 v14, 0x0

    :goto_4
    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeMaxWidth:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_e

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    invoke-static {v13}, Lf/h/a/f/f/n/g;->P(I)Lf/l/a/w/c;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeMinHeight:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_f

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    invoke-static {v13}, Lf/h/a/f/f/n/g;->Q(I)Lf/l/a/w/c;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_f
    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeMaxHeight:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_10

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    invoke-static {v13}, Lf/h/a/f/f/n/g;->O(I)Lf/l/a/w/c;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_10
    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeMinArea:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v17

    if-eqz v17, :cond_11

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    new-instance v14, Lf/l/a/w/l;

    invoke-direct {v14, v13}, Lf/l/a/w/l;-><init>(I)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_11
    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeMaxArea:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v14

    if-eqz v14, :cond_12

    const/4 v14, 0x0

    invoke-virtual {v3, v13, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    new-instance v14, Lf/l/a/w/k;

    invoke-direct {v14, v13}, Lf/l/a/w/k;-><init>(I)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    sget v13, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeAspectRatio:I

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v14

    if-eqz v14, :cond_13

    invoke-virtual {v3, v13}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lf/l/a/w/a;->h(Ljava/lang/String;)Lf/l/a/w/a;

    move-result-object v13

    invoke-virtual {v13}, Lf/l/a/w/a;->i()F

    move-result v13

    new-instance v14, Lf/l/a/w/h;

    move-object/from16 v37, v11

    const/4 v11, 0x0

    invoke-direct {v14, v13, v11}, Lf/l/a/w/h;-><init>(FF)V

    invoke-static {v14}, Lf/h/a/f/f/n/g;->j0(Lf/l/a/w/n;)Lf/l/a/w/c;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_13
    move-object/from16 v37, v11

    :goto_5
    sget v11, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeSmallest:I

    const/4 v13, 0x0

    invoke-virtual {v3, v11, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v11

    if-eqz v11, :cond_14

    new-instance v11, Lf/l/a/w/j;

    invoke-direct {v11}, Lf/l/a/w/j;-><init>()V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_14
    sget v11, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraVideoSizeBiggest:I

    invoke-virtual {v3, v11, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v11

    if-eqz v11, :cond_15

    new-instance v11, Lf/l/a/w/i;

    invoke-direct {v11}, Lf/l/a/w/i;-><init>()V

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_15
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_16

    new-array v11, v13, [Lf/l/a/w/c;

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Lf/l/a/w/c;

    invoke-static {v11}, Lf/h/a/f/f/n/g;->b([Lf/l/a/w/c;)Lf/l/a/w/c;

    move-result-object v11

    goto :goto_6

    :cond_16
    new-instance v11, Lf/l/a/w/i;

    invoke-direct {v11}, Lf/l/a/w/i;-><init>()V

    :goto_6
    sget v12, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGestureTap:I

    sget-object v13, Lf/l/a/p/b;->d:Lf/l/a/p/b;

    invoke-virtual {v13}, Lf/l/a/p/b;->h()I

    move-result v14

    invoke-virtual {v3, v12, v14}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGestureLongTap:I

    move/from16 v17, v12

    invoke-virtual {v13}, Lf/l/a/p/b;->h()I

    move-result v12

    invoke-virtual {v3, v14, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGesturePinch:I

    move/from16 v20, v12

    invoke-virtual {v13}, Lf/l/a/p/b;->h()I

    move-result v12

    invoke-virtual {v3, v14, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGestureScrollHorizontal:I

    move/from16 v40, v12

    invoke-virtual {v13}, Lf/l/a/p/b;->h()I

    move-result v12

    invoke-virtual {v3, v14, v12}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraGestureScrollVertical:I

    invoke-virtual {v13}, Lf/l/a/p/b;->h()I

    move-result v13

    invoke-virtual {v3, v14, v13}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v13

    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraAutoFocusMarker:I

    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v41, 0x0

    if-eqz v14, :cond_17

    :try_start_0
    invoke-static {v14}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lf/l/a/r/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v41, v14

    :catch_0
    :cond_17
    sget v14, Lcom/otaliastudios/cameraview/R$c;->CameraView_cameraFilter:I

    invoke-virtual {v3, v14}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    :try_start_1
    invoke-static {v14}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lf/l/a/n/b;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_7

    :catch_1
    new-instance v14, Lf/l/a/n/c;

    invoke-direct {v14}, Lf/l/a/n/c;-><init>()V

    :goto_7
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v3, Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-direct {v3, v0}, Lcom/otaliastudios/cameraview/CameraView$b;-><init>(Lcom/otaliastudios/cameraview/CameraView;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->n:Lcom/otaliastudios/cameraview/CameraView$b;

    new-instance v3, Landroid/os/Handler;

    move-object/from16 v42, v14

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v14

    invoke-direct {v3, v14}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->l:Landroid/os/Handler;

    new-instance v3, Lf/l/a/p/e;

    iget-object v14, v0, Lcom/otaliastudios/cameraview/CameraView;->n:Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-direct {v3, v14}, Lf/l/a/p/e;-><init>(Lf/l/a/p/c$a;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->x:Lf/l/a/p/e;

    new-instance v3, Lf/l/a/p/g;

    iget-object v14, v0, Lcom/otaliastudios/cameraview/CameraView;->n:Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-direct {v3, v14}, Lf/l/a/p/g;-><init>(Lf/l/a/p/c$a;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->y:Lf/l/a/p/g;

    new-instance v3, Lf/l/a/p/f;

    iget-object v14, v0, Lcom/otaliastudios/cameraview/CameraView;->n:Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-direct {v3, v14}, Lf/l/a/p/f;-><init>(Lf/l/a/p/c$a;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->z:Lf/l/a/p/f;

    new-instance v3, Lf/l/a/q/c;

    invoke-direct {v3, v1}, Lf/l/a/q/c;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->A:Lf/l/a/q/c;

    new-instance v3, Lf/l/a/t/b;

    invoke-direct {v3, v1}, Lf/l/a/t/b;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    new-instance v3, Lf/l/a/r/c;

    invoke-direct {v3, v1}, Lf/l/a/r/c;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->B:Lf/l/a/r/c;

    iget-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->A:Lf/l/a/q/c;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->B:Lf/l/a/r/c;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Lcom/otaliastudios/cameraview/CameraView;->b()V

    invoke-virtual {v0, v15}, Lcom/otaliastudios/cameraview/CameraView;->setPlaySounds(Z)V

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setUseDeviceOrientation(Z)V

    invoke-static {v7}, Lf/l/a/l/g;->f(I)Lf/l/a/l/g;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setGrid(Lf/l/a/l/g;)V

    invoke-virtual {v0, v4}, Lcom/otaliastudios/cameraview/CameraView;->setGridColor(I)V

    invoke-static {v5}, Lf/l/a/l/e;->f(I)Lf/l/a/l/e;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFacing(Lf/l/a/l/e;)V

    invoke-static {v6}, Lf/l/a/l/f;->f(I)Lf/l/a/l/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFlash(Lf/l/a/l/f;)V

    invoke-static {v9}, Lf/l/a/l/i;->f(I)Lf/l/a/l/i;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setMode(Lf/l/a/l/i;)V

    invoke-static {v8}, Lf/l/a/l/m;->f(I)Lf/l/a/l/m;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setWhiteBalance(Lf/l/a/l/m;)V

    invoke-static {v10}, Lf/l/a/l/h;->f(I)Lf/l/a/l/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setHdr(Lf/l/a/l/h;)V

    invoke-static/range {v39 .. v39}, Lf/l/a/l/a;->f(I)Lf/l/a/l/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setAudio(Lf/l/a/l/a;)V

    move/from16 v2, v38

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setAudioBitRate(I)V

    invoke-static/range {v19 .. v19}, Lf/l/a/l/b;->f(I)Lf/l/a/l/b;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setAudioCodec(Lf/l/a/l/b;)V

    move-object/from16 v2, v37

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setPictureSize(Lf/l/a/w/c;)V

    move/from16 v2, v29

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setPictureMetering(Z)V

    move/from16 v2, v30

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setPictureSnapshotMetering(Z)V

    invoke-static/range {v16 .. v16}, Lf/l/a/l/j;->f(I)Lf/l/a/l/j;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setPictureFormat(Lf/l/a/l/j;)V

    invoke-virtual {v0, v11}, Lcom/otaliastudios/cameraview/CameraView;->setVideoSize(Lf/l/a/w/c;)V

    invoke-static/range {p2 .. p2}, Lf/l/a/l/l;->f(I)Lf/l/a/l/l;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setVideoCodec(Lf/l/a/l/l;)V

    move-wide/from16 v2, v21

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->setVideoMaxSize(J)V

    move/from16 v2, v24

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setVideoMaxDuration(I)V

    move/from16 v2, v23

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setVideoBitRate(I)V

    move-wide/from16 v2, v27

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->setAutoFocusResetDelay(J)V

    move/from16 v2, v26

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setPreviewFrameRateExact(Z)V

    move/from16 v2, v25

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setPreviewFrameRate(F)V

    move/from16 v2, v31

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setSnapshotMaxWidth(I)V

    move/from16 v2, v32

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setSnapshotMaxHeight(I)V

    move/from16 v2, v33

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingMaxWidth(I)V

    move/from16 v2, v34

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingMaxHeight(I)V

    move/from16 v2, v35

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingFormat(I)V

    move/from16 v2, v36

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingPoolSize(I)V

    move/from16 v2, v18

    invoke-virtual {v0, v2}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingExecutors(I)V

    sget-object v2, Lf/l/a/p/a;->e:Lf/l/a/p/a;

    invoke-static/range {v17 .. v17}, Lf/l/a/p/b;->f(I)Lf/l/a/p/b;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->e(Lf/l/a/p/a;Lf/l/a/p/b;)Z

    sget-object v2, Lf/l/a/p/a;->f:Lf/l/a/p/a;

    invoke-static/range {v20 .. v20}, Lf/l/a/p/b;->f(I)Lf/l/a/p/b;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->e(Lf/l/a/p/a;Lf/l/a/p/b;)Z

    sget-object v2, Lf/l/a/p/a;->d:Lf/l/a/p/a;

    invoke-static/range {v40 .. v40}, Lf/l/a/p/b;->f(I)Lf/l/a/p/b;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->e(Lf/l/a/p/a;Lf/l/a/p/b;)Z

    sget-object v2, Lf/l/a/p/a;->g:Lf/l/a/p/a;

    invoke-static {v12}, Lf/l/a/p/b;->f(I)Lf/l/a/p/b;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->e(Lf/l/a/p/a;Lf/l/a/p/b;)Z

    sget-object v2, Lf/l/a/p/a;->h:Lf/l/a/p/a;

    invoke-static {v13}, Lf/l/a/p/b;->f(I)Lf/l/a/p/b;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/otaliastudios/cameraview/CameraView;->e(Lf/l/a/p/a;Lf/l/a/p/b;)Z

    move-object/from16 v14, v41

    invoke-virtual {v0, v14}, Lcom/otaliastudios/cameraview/CameraView;->setAutoFocusMarker(Lf/l/a/r/a;)V

    move-object/from16 v14, v42

    invoke-virtual {v0, v14}, Lcom/otaliastudios/cameraview/CameraView;->setFilter(Lf/l/a/n/b;)V

    new-instance v2, Lf/l/a/q/d;

    iget-object v3, v0, Lcom/otaliastudios/cameraview/CameraView;->n:Lcom/otaliastudios/cameraview/CameraView$b;

    invoke-direct {v2, v1, v3}, Lf/l/a/q/d;-><init>(Landroid/content/Context;Lf/l/a/q/d$c;)V

    iput-object v2, v0, Lcom/otaliastudios/cameraview/CameraView;->p:Lf/l/a/q/d;

    :goto_8
    return-void
.end method


# virtual methods
.method public a(Lf/l/a/l/a;)Z
    .locals 10
    .param p1    # Lf/l/a/l/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    sget-object v0, Lf/l/a/l/a;->g:Lf/l/a/l/a;

    sget-object v1, Lf/l/a/l/a;->f:Lf/l/a/l/a;

    sget-object v2, Lf/l/a/l/a;->e:Lf/l/a/l/a;

    const-string v3, "android.permission.RECORD_AUDIO"

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eq p1, v2, :cond_0

    if-eq p1, v1, :cond_0

    if-ne p1, v0, :cond_3

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x1000

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    array-length v7, v6

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v7, :cond_2

    aget-object v9, v6, v8

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    sget-object v6, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    new-array v7, v4, [Ljava/lang/Object;

    const-string v8, "Permission error: when audio is enabled (Audio.ON) the RECORD_AUDIO permission should be added to the app manifest file."

    aput-object v8, v7, v5

    const/4 v8, 0x3

    invoke-virtual {v6, v8, v7}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/IllegalStateException;

    invoke-direct {v7, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_3
    :goto_1
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x17

    if-ge v6, v7, :cond_4

    return v4

    :cond_4
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    if-eq p1, v2, :cond_6

    if-eq p1, v1, :cond_6

    if-ne p1, v0, :cond_5

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    goto :goto_3

    :cond_6
    :goto_2
    const/4 p1, 0x1

    :goto_3
    const-string v0, "android.permission.CAMERA"

    invoke-virtual {v6, v0}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    goto :goto_4

    :cond_7
    const/4 v1, 0x0

    :goto_4
    if-eqz p1, :cond_8

    invoke-virtual {v6, v3}, Landroid/content/Context;->checkSelfPermission(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_8

    const/4 p1, 0x1

    goto :goto_5

    :cond_8
    const/4 p1, 0x0

    :goto_5
    if-nez v1, :cond_9

    if-nez p1, :cond_9

    return v4

    :cond_9
    iget-boolean v2, p0, Lcom/otaliastudios/cameraview/CameraView;->f:Z

    if-eqz v2, :cond_e

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v4, 0x0

    :goto_6
    instance-of v6, v2, Landroid/content/ContextWrapper;

    if-eqz v6, :cond_b

    instance-of v6, v2, Landroid/app/Activity;

    if-eqz v6, :cond_a

    move-object v4, v2

    check-cast v4, Landroid/app/Activity;

    :cond_a
    check-cast v2, Landroid/content/ContextWrapper;

    invoke-virtual {v2}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    goto :goto_6

    :cond_b
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz v1, :cond_c

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    if-eqz p1, :cond_d

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    if-eqz v4, :cond_e

    new-array p1, v5, [Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    const/16 v0, 0x10

    invoke-virtual {v4, p1, v0}, Landroid/app/Activity;->requestPermissions([Ljava/lang/String;I)V

    :cond_e
    return v5
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, p3, Lf/l/a/t/b$a;

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-virtual {p2, p1, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void
.end method

.method public final b()V
    .locals 9

    sget-object v0, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "doInstantiateEngine:"

    aput-object v4, v2, v3

    const-string v5, "instantiating. engine:"

    const/4 v6, 0x1

    aput-object v5, v2, v6

    iget-object v5, p0, Lcom/otaliastudios/cameraview/CameraView;->i:Lf/l/a/l/d;

    const/4 v7, 0x2

    aput-object v5, v2, v7

    invoke-virtual {v0, v7, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->i:Lf/l/a/l/d;

    iget-object v5, p0, Lcom/otaliastudios/cameraview/CameraView;->n:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-boolean v8, p0, Lcom/otaliastudios/cameraview/CameraView;->C:Z

    if-eqz v8, :cond_0

    sget-object v8, Lf/l/a/l/d;->e:Lf/l/a/l/d;

    if-ne v2, v8, :cond_0

    new-instance v2, Lf/l/a/m/d;

    invoke-direct {v2, v5}, Lf/l/a/m/d;-><init>(Lf/l/a/m/j$g;)V

    goto :goto_0

    :cond_0
    sget-object v2, Lf/l/a/l/d;->d:Lf/l/a/l/d;

    iput-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->i:Lf/l/a/l/d;

    new-instance v2, Lf/l/a/m/b;

    invoke-direct {v2, v5}, Lf/l/a/m/b;-><init>(Lf/l/a/m/j$g;)V

    :goto_0
    iput-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v3, "instantiated. engine:"

    aput-object v3, v1, v6

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v7, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-virtual {v0, v1}, Lf/l/a/m/j;->o0(Lf/l/a/t/a;)V

    return-void
.end method

.method public final c()Z
    .locals 3

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    iget-object v1, v0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v1, v1, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v2, Lf/l/a/m/v/b;->d:Lf/l/a/m/v/b;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lf/l/a/m/j;->O()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public close()V
    .locals 4
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_PAUSE:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->p:Lf/l/a/q/d;

    iget-boolean v1, v0, Lf/l/a/q/d;->h:Z

    const/4 v2, 0x0

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    iput-boolean v2, v0, Lf/l/a/q/d;->h:Z

    iget-object v1, v0, Lf/l/a/q/d;->d:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->disable()V

    iget-object v1, v0, Lf/l/a/q/d;->b:Landroid/content/Context;

    const-string v3, "display"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iget-object v3, v0, Lf/l/a/q/d;->f:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v1, v3}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    const/4 v1, -0x1

    iput v1, v0, Lf/l/a/q/d;->g:I

    iput v1, v0, Lf/l/a/q/d;->e:I

    :goto_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, v2}, Lf/l/a/m/j;->L0(Z)Lcom/google/android/gms/tasks/Task;

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lf/l/a/v/a;->n()V

    :cond_2
    return-void
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    iget-object v0, v0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v0, v0, Lf/l/a/m/v/c;->f:Lf/l/a/m/v/b;

    sget-object v1, Lf/l/a/m/v/b;->e:Lf/l/a/m/v/b;

    invoke-virtual {v0, v1}, Lf/l/a/m/v/b;->f(Lf/l/a/m/v/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    iget-object v0, v0, Lf/l/a/m/j;->g:Lf/l/a/m/v/c;

    iget-object v0, v0, Lf/l/a/m/v/c;->g:Lf/l/a/m/v/b;

    invoke-virtual {v0, v1}, Lf/l/a/m/v/b;->f(Lf/l/a/m/v/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public destroy()V
    .locals 4
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_DESTROY:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/otaliastudios/cameraview/CameraView;->v:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, v2}, Lf/l/a/m/j;->k0(Z)V

    :cond_2
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, v1, v2}, Lf/l/a/m/j;->d(ZI)V

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lf/l/a/v/a;->m()V

    :cond_3
    return-void
.end method

.method public e(Lf/l/a/p/a;Lf/l/a/p/b;)Z
    .locals 4
    .param p1    # Lf/l/a/p/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/p/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    sget-object v0, Lf/l/a/p/b;->d:Lf/l/a/p/b;

    invoke-virtual {p1, p2}, Lf/l/a/p/a;->f(Lf/l/a/p/b;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    if-eqz p1, :cond_6

    if-eq p1, p2, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->z:Lf/l/a/p/f;

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    sget-object v3, Lf/l/a/p/a;->g:Lf/l/a/p/a;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_1

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    sget-object v3, Lf/l/a/p/a;->h:Lf/l/a/p/a;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eq v1, v0, :cond_2

    :cond_1
    const/4 v2, 0x1

    :cond_2
    iput-boolean v2, p1, Lf/l/a/p/c;->a:Z

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->y:Lf/l/a/p/g;

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    sget-object v3, Lf/l/a/p/a;->e:Lf/l/a/p/a;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_4

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    sget-object v3, Lf/l/a/p/a;->f:Lf/l/a/p/a;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eq v1, v0, :cond_5

    :cond_4
    const/4 v2, 0x1

    :cond_5
    iput-boolean v2, p1, Lf/l/a/p/c;->a:Z

    goto :goto_0

    :cond_6
    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->x:Lf/l/a/p/e;

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    sget-object v3, Lf/l/a/p/a;->d:Lf/l/a/p/a;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eq v1, v0, :cond_7

    const/4 v2, 0x1

    :cond_7
    iput-boolean v2, p1, Lf/l/a/p/c;->a:Z

    :goto_0
    return p2

    :cond_8
    invoke-virtual {p0, p1, v0}, Lcom/otaliastudios/cameraview/CameraView;->e(Lf/l/a/p/a;Lf/l/a/p/b;)Z

    return v2
.end method

.method public final f(I)Ljava/lang/String;
    .locals 1

    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_2

    if-eqz p1, :cond_1

    const/high16 v0, 0x40000000    # 2.0f

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string p1, "EXACTLY"

    return-object p1

    :cond_1
    const-string p1, "UNSPECIFIED"

    return-object p1

    :cond_2
    const-string p1, "AT_MOST"

    return-object p1
.end method

.method public final g(Lf/l/a/p/c;Lf/l/a/c;)V
    .locals 13
    .param p1    # Lf/l/a/p/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lf/l/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p1, Lf/l/a/p/c;->b:Lf/l/a/p/a;

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->g:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/l/a/p/b;

    iget-object v2, p1, Lf/l/a/p/c;->c:[Landroid/graphics/PointF;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v3, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getFilter()Lf/l/a/n/b;

    move-result-object p2

    instance-of p2, p2, Lf/l/a/n/e;

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getFilter()Lf/l/a/n/b;

    move-result-object p2

    check-cast p2, Lf/l/a/n/e;

    invoke-interface {p2}, Lf/l/a/n/e;->f()F

    move-result v0

    invoke-virtual {p1, v0, v6, v4}, Lf/l/a/p/c;->a(FFF)F

    move-result p1

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    invoke-interface {p2, p1}, Lf/l/a/n/e;->a(F)V

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getFilter()Lf/l/a/n/b;

    move-result-object p2

    instance-of p2, p2, Lf/l/a/n/d;

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getFilter()Lf/l/a/n/b;

    move-result-object p2

    check-cast p2, Lf/l/a/n/d;

    invoke-interface {p2}, Lf/l/a/n/d;->h()F

    move-result v0

    invoke-virtual {p1, v0, v6, v4}, Lf/l/a/p/c;->a(FFF)F

    move-result p1

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    invoke-interface {p2, p1}, Lf/l/a/n/d;->c(F)V

    goto/16 :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->k()F

    move-result v0

    iget v1, p2, Lf/l/a/c;->m:F

    iget p2, p2, Lf/l/a/c;->n:F

    invoke-virtual {p1, v0, v1, p2}, Lf/l/a/p/c;->a(FFF)F

    move-result p1

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [F

    aput v1, v0, v5

    aput p2, v0, v3

    iget-object p2, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {p2, p1, v0, v2, v3}, Lf/l/a/m/j;->d0(F[F[Landroid/graphics/PointF;Z)V

    goto/16 :goto_1

    :pswitch_3
    iget-object p2, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {p2}, Lf/l/a/m/j;->N()F

    move-result p2

    invoke-virtual {p1, p2, v6, v4}, Lf/l/a/p/c;->a(FFF)F

    move-result p1

    cmpl-float p2, p1, p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {p2, p1, v2, v3}, Lf/l/a/m/j;->G0(F[Landroid/graphics/PointF;Z)V

    goto/16 :goto_1

    :pswitch_4
    new-instance p1, Lf/l/a/k$a;

    invoke-direct {p1}, Lf/l/a/k$a;-><init>()V

    iget-object p2, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {p2, p1}, Lf/l/a/m/j;->O0(Lf/l/a/k$a;)V

    goto/16 :goto_1

    :pswitch_5
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result p2

    aget-object v1, v2, v5

    int-to-float p1, p1

    const v3, 0x3d4ccccd    # 0.05f

    mul-float v4, p1, v3

    int-to-float p2, p2

    mul-float v3, v3, p2

    invoke-static {v1, v4, v3}, Lf/l/a/s/b;->a(Landroid/graphics/PointF;FF)Landroid/graphics/RectF;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    invoke-direct {v4, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v8

    new-instance v9, Lf/l/a/s/a;

    const/16 v10, 0x3e8

    invoke-direct {v9, v1, v10}, Lf/l/a/s/a;-><init>(Landroid/graphics/RectF;I)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float v7, v7, v1

    mul-float v8, v8, v1

    invoke-static {v4, v7, v8}, Lf/l/a/s/b;->a(Landroid/graphics/PointF;FF)Landroid/graphics/RectF;

    move-result-object v1

    new-instance v4, Lf/l/a/s/a;

    const v7, 0x3dcccccd    # 0.1f

    int-to-float v8, v10

    mul-float v8, v8, v7

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-direct {v4, v1, v7}, Lf/l/a/s/a;-><init>(Landroid/graphics/RectF;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf/l/a/s/a;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7, v6, v6, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    iget v9, v7, Landroid/graphics/RectF;->left:F

    iget-object v10, v4, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v9

    iget v10, v7, Landroid/graphics/RectF;->top:F

    iget-object v11, v4, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v10

    iget v11, v7, Landroid/graphics/RectF;->right:F

    iget-object v12, v4, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->right:F

    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    move-result v11

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget-object v12, v4, Lf/l/a/s/a;->d:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->bottom:F

    invoke-static {v7, v12}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-virtual {v8, v9, v10, v11, v7}, Landroid/graphics/RectF;->set(FFFF)V

    new-instance v7, Lf/l/a/s/a;

    iget v4, v4, Lf/l/a/s/a;->e:I

    invoke-direct {v7, v8, v4}, Lf/l/a/s/a;-><init>(Landroid/graphics/RectF;I)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lf/l/a/s/b;

    invoke-direct {p1, v1}, Lf/l/a/s/b;-><init>(Ljava/util/List;)V

    iget-object p2, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    aget-object v1, v2, v5

    invoke-virtual {p2, v0, p1, v1}, Lf/l/a/m/j;->I0(Lf/l/a/p/a;Lf/l/a/s/b;Landroid/graphics/PointF;)V

    :cond_1
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 3

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/otaliastudios/cameraview/R$c;->CameraView_Layout:[I

    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v2, Lcom/otaliastudios/cameraview/R$c;->CameraView_Layout_layout_drawOnPreview:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-nez v2, :cond_1

    sget v2, Lcom/otaliastudios/cameraview/R$c;->CameraView_Layout_layout_drawOnPictureSnapshot:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-nez v2, :cond_1

    sget v2, Lcom/otaliastudios/cameraview/R$c;->CameraView_Layout_layout_drawOnVideoSnapshot:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-virtual {v0, p1}, Lf/l/a/t/b;->a(Landroid/util/AttributeSet;)Lf/l/a/t/b$a;

    move-result-object p1

    return-object p1

    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public getAudio()Lf/l/a/l/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->f()Lf/l/a/l/a;

    move-result-object v0

    return-object v0
.end method

.method public getAudioBitRate()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->g()I

    move-result v0

    return v0
.end method

.method public getAudioCodec()Lf/l/a/l/b;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->h()Lf/l/a/l/b;

    move-result-object v0

    return-object v0
.end method

.method public getAutoFocusResetDelay()J
    .locals 2

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->i()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCameraOptions()Lf/l/a/c;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->j()Lf/l/a/c;

    move-result-object v0

    return-object v0
.end method

.method public getEngine()Lf/l/a/l/d;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->i:Lf/l/a/l/d;

    return-object v0
.end method

.method public getExposureCorrection()F
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->k()F

    move-result v0

    return v0
.end method

.method public getFacing()Lf/l/a/l/e;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->l()Lf/l/a/l/e;

    move-result-object v0

    return-object v0
.end method

.method public getFilter()Lf/l/a/n/b;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->j:Lf/l/a/n/b;

    return-object v0

    :cond_0
    instance-of v1, v0, Lf/l/a/v/b;

    if-eqz v1, :cond_1

    check-cast v0, Lf/l/a/v/b;

    invoke-interface {v0}, Lf/l/a/v/b;->b()Lf/l/a/n/b;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Filters are only supported by the GL_SURFACE preview. Current:"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFlash()Lf/l/a/l/f;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->m()Lf/l/a/l/f;

    move-result-object v0

    return-object v0
.end method

.method public getFrameProcessingExecutors()I
    .locals 1

    iget v0, p0, Lcom/otaliastudios/cameraview/CameraView;->k:I

    return v0
.end method

.method public getFrameProcessingFormat()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->n()I

    move-result v0

    return v0
.end method

.method public getFrameProcessingMaxHeight()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->o()I

    move-result v0

    return v0
.end method

.method public getFrameProcessingMaxWidth()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->p()I

    move-result v0

    return v0
.end method

.method public getFrameProcessingPoolSize()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->q()I

    move-result v0

    return v0
.end method

.method public getGrid()Lf/l/a/l/g;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->A:Lf/l/a/q/c;

    invoke-virtual {v0}, Lf/l/a/q/c;->getGridMode()Lf/l/a/l/g;

    move-result-object v0

    return-object v0
.end method

.method public getGridColor()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->A:Lf/l/a/q/c;

    invoke-virtual {v0}, Lf/l/a/q/c;->getGridColor()I

    move-result v0

    return v0
.end method

.method public getHdr()Lf/l/a/l/h;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->r()Lf/l/a/l/h;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->s()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public getMode()Lf/l/a/l/i;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->t()Lf/l/a/l/i;

    move-result-object v0

    return-object v0
.end method

.method public getPictureFormat()Lf/l/a/l/j;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->u()Lf/l/a/l/j;

    move-result-object v0

    return-object v0
.end method

.method public getPictureMetering()Z
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->v()Z

    move-result v0

    return v0
.end method

.method public getPictureSize()Lf/l/a/w/b;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    sget-object v1, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    invoke-virtual {v0, v1}, Lf/l/a/m/j;->w(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v0

    return-object v0
.end method

.method public getPictureSnapshotMetering()Z
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->y()Z

    move-result v0

    return v0
.end method

.method public getPlaySounds()Z
    .locals 1

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->d:Z

    return v0
.end method

.method public getPreview()Lf/l/a/l/k;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    return-object v0
.end method

.method public getPreviewFrameRate()F
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->A()F

    move-result v0

    return v0
.end method

.method public getPreviewFrameRateExact()Z
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->B()Z

    move-result v0

    return v0
.end method

.method public getSnapshotMaxHeight()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->D()I

    move-result v0

    return v0
.end method

.method public getSnapshotMaxWidth()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->E()I

    move-result v0

    return v0
.end method

.method public getSnapshotSize()Lf/l/a/w/b;
    .locals 8
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    sget-object v2, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v0, v2}, Lf/l/a/m/j;->F(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v3

    invoke-static {v1, v3}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v1

    iget v3, v0, Lf/l/a/w/b;->d:I

    iget v4, v0, Lf/l/a/w/b;->e:I

    const v5, 0x3a03126f    # 5.0E-4f

    invoke-virtual {v1}, Lf/l/a/w/a;->i()F

    move-result v6

    iget v7, v0, Lf/l/a/w/b;->d:I

    iget v0, v0, Lf/l/a/w/b;->e:I

    invoke-static {v7, v0}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/l/a/w/a;->i()F

    move-result v0

    sub-float/2addr v6, v0

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v6, 0x0

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v6, v6, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_2

    :cond_3
    invoke-static {v3, v4}, Lf/l/a/w/a;->f(II)Lf/l/a/w/a;

    move-result-object v0

    invoke-virtual {v0}, Lf/l/a/w/a;->i()F

    move-result v0

    invoke-virtual {v1}, Lf/l/a/w/a;->i()F

    move-result v5

    const/high16 v7, 0x40000000    # 2.0f

    cmpl-float v0, v0, v5

    if-lez v0, :cond_4

    int-to-float v0, v4

    invoke-virtual {v1}, Lf/l/a/w/a;->i()F

    move-result v1

    mul-float v1, v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    sub-int/2addr v3, v0

    int-to-float v1, v3

    div-float/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    move v3, v0

    move v6, v1

    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    int-to-float v0, v3

    invoke-virtual {v1}, Lf/l/a/w/a;->i()F

    move-result v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sub-int/2addr v4, v0

    int-to-float v1, v4

    div-float/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    move v4, v0

    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/2addr v3, v6

    add-int/2addr v4, v1

    invoke-direct {v0, v6, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_2
    new-instance v1, Lf/l/a/w/b;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-direct {v1, v3, v0}, Lf/l/a/w/b;-><init>(II)V

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->e()Lf/l/a/m/t/a;

    move-result-object v0

    sget-object v3, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    invoke-virtual {v0, v2, v3}, Lf/l/a/m/t/a;->b(Lf/l/a/m/t/c;Lf/l/a/m/t/c;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v1}, Lf/l/a/w/b;->f()Lf/l/a/w/b;

    move-result-object v0

    return-object v0

    :cond_5
    :goto_3
    return-object v1
.end method

.method public getUseDeviceOrientation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->e:Z

    return v0
.end method

.method public getVideoBitRate()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->G()I

    move-result v0

    return v0
.end method

.method public getVideoCodec()Lf/l/a/l/l;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->H()Lf/l/a/l/l;

    move-result-object v0

    return-object v0
.end method

.method public getVideoMaxDuration()I
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->I()I

    move-result v0

    return v0
.end method

.method public getVideoMaxSize()J
    .locals 2

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->J()J

    move-result-wide v0

    return-wide v0
.end method

.method public getVideoSize()Lf/l/a/w/b;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    sget-object v1, Lf/l/a/m/t/c;->g:Lf/l/a/m/t/c;

    invoke-virtual {v0, v1}, Lf/l/a/m/j;->K(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v0

    return-object v0
.end method

.method public getWhiteBalance()Lf/l/a/l/m;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->M()Lf/l/a/l/m;

    move-result-object v0

    return-object v0
.end method

.method public getZoom()F
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->N()F

    move-result v0

    return v0
.end method

.method public onAttachedToWindow()V
    .locals 8

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-nez v0, :cond_4

    sget-object v0, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    const/4 v1, 0x3

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "doInstantiateEngine:"

    aput-object v4, v2, v3

    const/4 v5, 0x1

    const-string v6, "instantiating. preview:"

    aput-object v6, v2, v5

    iget-object v6, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    const/4 v7, 0x2

    aput-object v6, v2, v7

    invoke-virtual {v0, v7, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    if-eqz v2, :cond_3

    if-eq v2, v5, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->isHardwareAccelerated()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lf/l/a/v/i;

    invoke-direct {v2, v6, p0}, Lf/l/a/v/i;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    goto :goto_1

    :cond_2
    :goto_0
    sget-object v2, Lf/l/a/l/k;->f:Lf/l/a/l/k;

    iput-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    new-instance v2, Lf/l/a/v/d;

    invoke-direct {v2, v6, p0}, Lf/l/a/v/d;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    goto :goto_1

    :cond_3
    new-instance v2, Lf/l/a/v/g;

    invoke-direct {v2, v6, p0}, Lf/l/a/v/g;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    :goto_1
    iput-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v4, v1, v3

    const-string v3, "instantiated. preview:"

    aput-object v3, v1, v5

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v7, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    invoke-virtual {v0, v1}, Lf/l/a/m/j;->u0(Lf/l/a/v/a;)V

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->j:Lf/l/a/n/b;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setFilter(Lf/l/a/n/b;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->j:Lf/l/a/n/b;

    :cond_4
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->r:Lf/l/a/w/b;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onMeasure(II)V
    .locals 16

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    const/high16 v2, 0x40000000    # 2.0f

    if-eqz v1, :cond_0

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-super {v0, v1, v2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_0
    iget-object v1, v0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    sget-object v3, Lf/l/a/m/t/c;->f:Lf/l/a/m/t/c;

    invoke-virtual {v1, v3}, Lf/l/a/m/j;->C(Lf/l/a/m/t/c;)Lf/l/a/w/b;

    move-result-object v1

    iput-object v1, v0, Lcom/otaliastudios/cameraview/CameraView;->r:Lf/l/a/w/b;

    const-string v3, "onMeasure:"

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-nez v1, :cond_1

    sget-object v1, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v3, v2, v5

    const-string v3, "surface is not ready. Calling default behavior."

    aput-object v3, v2, v6

    invoke-virtual {v1, v4, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-super/range {p0 .. p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_1
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    iget-object v10, v0, Lcom/otaliastudios/cameraview/CameraView;->r:Lf/l/a/w/b;

    iget v11, v10, Lf/l/a/w/b;->d:I

    int-to-float v11, v11

    iget v10, v10, Lf/l/a/w/b;->e:I

    int-to-float v10, v10

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    iget-object v13, v0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    invoke-virtual {v13}, Lf/l/a/v/a;->s()Z

    move-result v13

    const/high16 v14, -0x80000000

    if-nez v13, :cond_3

    if-ne v1, v2, :cond_2

    const/high16 v1, -0x80000000

    :cond_2
    if-ne v7, v2, :cond_5

    const/high16 v7, -0x80000000

    goto :goto_0

    :cond_3
    const/4 v2, -0x1

    if-ne v1, v14, :cond_4

    iget v13, v12, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v13, v2, :cond_4

    const/high16 v1, 0x40000000    # 2.0f

    :cond_4
    if-ne v7, v14, :cond_5

    iget v12, v12, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v12, v2, :cond_5

    const/high16 v7, 0x40000000    # 2.0f

    :cond_5
    :goto_0
    sget-object v2, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    new-array v12, v4, [Ljava/lang/Object;

    aput-object v3, v12, v5

    const-string v13, "requested dimensions are ("

    const-string v14, "["

    invoke-static {v13, v8, v14}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v0, v1}, Lcom/otaliastudios/cameraview/CameraView;->f(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v15, "]x"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Lcom/otaliastudios/cameraview/CameraView;->f(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "])"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v6

    invoke-virtual {v2, v6, v12}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v3, v12, v5

    const-string v5, "previewSize is"

    aput-object v5, v12, v6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "("

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string/jumbo v14, "x"

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v15, ")"

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v12, v4

    invoke-virtual {v2, v6, v12}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/4 v5, 0x4

    const/high16 v12, 0x40000000    # 2.0f

    if-ne v1, v12, :cond_6

    if-ne v7, v12, :cond_6

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v1, v5

    const-string v3, "both are MATCH_PARENT or fixed value. We adapt."

    aput-object v3, v1, v6

    const-string v3, "This means CROP_CENTER."

    aput-object v3, v1, v4

    invoke-static {v13, v8, v14, v9, v15}, Lf/e/c/a/a;->n(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v1, v4

    invoke-virtual {v2, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    invoke-super/range {p0 .. p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_6
    if-nez v1, :cond_7

    if-nez v7, :cond_7

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v1, v5

    const-string v3, "both are completely free."

    aput-object v3, v1, v6

    const-string v3, "We respect that and extend to the whole preview size."

    aput-object v3, v1, v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v1, v4

    invoke-virtual {v2, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    float-to-int v1, v11

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    float-to-int v3, v10

    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-super {v0, v1, v2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_7
    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    if-eqz v1, :cond_e

    if-nez v7, :cond_8

    goto/16 :goto_5

    :cond_8
    if-eq v1, v12, :cond_b

    if-ne v7, v12, :cond_9

    goto :goto_2

    :cond_9
    int-to-float v1, v9

    int-to-float v7, v8

    div-float v11, v1, v7

    cmpl-float v11, v11, v10

    if-ltz v11, :cond_a

    mul-float v7, v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v9

    goto :goto_1

    :cond_a
    div-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v8

    :goto_1
    new-array v1, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v1, v5

    const-string v3, "both dimension were AT_MOST."

    aput-object v3, v1, v6

    const-string v3, "We fit the preview aspect ratio."

    aput-object v3, v1, v4

    invoke-static {v13, v8, v14, v9, v15}, Lf/e/c/a/a;->n(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v1, v4

    invoke-virtual {v2, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v8, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v9, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {v0, v2, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_b
    :goto_2
    const/high16 v7, -0x80000000

    if-ne v1, v7, :cond_c

    const/4 v1, 0x1

    goto :goto_3

    :cond_c
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_d

    int-to-float v1, v9

    div-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto :goto_4

    :cond_d
    int-to-float v1, v8

    mul-float v1, v1, v10

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v1, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    :goto_4
    new-array v1, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v1, v5

    const-string v3, "one dimension was EXACTLY, another AT_MOST."

    aput-object v3, v1, v6

    const-string v3, "We have TRIED to fit the aspect ratio, but it\'s not guaranteed."

    aput-object v3, v1, v4

    invoke-static {v13, v8, v14, v9, v15}, Lf/e/c/a/a;->n(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v1, v4

    invoke-virtual {v2, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v8, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v9, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {v0, v2, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_e
    :goto_5
    if-nez v1, :cond_f

    const/4 v1, 0x1

    goto :goto_6

    :cond_f
    const/4 v1, 0x0

    :goto_6
    if-eqz v1, :cond_10

    int-to-float v1, v9

    div-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v8

    goto :goto_7

    :cond_10
    int-to-float v1, v8

    mul-float v1, v1, v10

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v9

    :goto_7
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v1, v5

    const-string v3, "one dimension was free, we adapted it to fit the ratio."

    aput-object v3, v1, v6

    invoke-static {v13, v8, v14, v9, v15}, Lf/e/c/a/a;->n(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-virtual {v2, v6, v1}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v8, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v9, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {v0, v2, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->d()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->j()Lf/l/a/c;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->x:Lf/l/a/p/e;

    iget-boolean v3, v2, Lf/l/a/p/c;->a:Z

    const/4 v4, 0x0

    if-nez v3, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v2, p1}, Lf/l/a/p/e;->c(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_0
    const-string v3, "onTouchEvent"

    const/4 v5, 0x2

    if-eqz v2, :cond_2

    sget-object p1, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v3, v2, v4

    const-string v3, "pinch!"

    aput-object v3, v2, v1

    invoke-virtual {p1, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->x:Lf/l/a/p/e;

    invoke-virtual {p0, p1, v0}, Lcom/otaliastudios/cameraview/CameraView;->g(Lf/l/a/p/c;Lf/l/a/c;)V

    goto :goto_3

    :cond_2
    iget-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->z:Lf/l/a/p/f;

    iget-boolean v6, v2, Lf/l/a/p/c;->a:Z

    if-nez v6, :cond_3

    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v2, p1}, Lf/l/a/p/f;->c(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_1
    if-eqz v2, :cond_4

    sget-object p1, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v3, v2, v4

    const-string v3, "scroll!"

    aput-object v3, v2, v1

    invoke-virtual {p1, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->z:Lf/l/a/p/f;

    invoke-virtual {p0, p1, v0}, Lcom/otaliastudios/cameraview/CameraView;->g(Lf/l/a/p/c;Lf/l/a/c;)V

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/otaliastudios/cameraview/CameraView;->y:Lf/l/a/p/g;

    iget-boolean v6, v2, Lf/l/a/p/c;->a:Z

    if-nez v6, :cond_5

    const/4 p1, 0x0

    goto :goto_2

    :cond_5
    invoke-virtual {v2, p1}, Lf/l/a/p/g;->c(Landroid/view/MotionEvent;)Z

    move-result p1

    :goto_2
    if-eqz p1, :cond_6

    sget-object p1, Lcom/otaliastudios/cameraview/CameraView;->G:Lf/l/a/b;

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v3, v2, v4

    const-string v3, "tap!"

    aput-object v3, v2, v1

    invoke-virtual {p1, v1, v2}, Lf/l/a/b;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->y:Lf/l/a/p/g;

    invoke-virtual {p0, p1, v0}, Lcom/otaliastudios/cameraview/CameraView;->g(Lf/l/a/p/c;Lf/l/a/c;)V

    :cond_6
    :goto_3
    return v1

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Options should not be null here."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public open()V
    .locals 4
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_RESUME:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/l/a/v/a;->o()V

    :cond_1
    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getAudio()Lf/l/a/l/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->a(Lf/l/a/l/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->p:Lf/l/a/q/d;

    iget-boolean v1, v0, Lf/l/a/q/d;->h:Z

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, v0, Lf/l/a/q/d;->h:Z

    invoke-virtual {v0}, Lf/l/a/q/d;->a()I

    move-result v1

    iput v1, v0, Lf/l/a/q/d;->g:I

    iget-object v1, v0, Lf/l/a/q/d;->b:Landroid/content/Context;

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iget-object v2, v0, Lf/l/a/q/d;->f:Landroid/hardware/display/DisplayManager$DisplayListener;

    iget-object v3, v0, Lf/l/a/q/d;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    iget-object v0, v0, Lf/l/a/q/d;->d:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    :goto_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->e()Lf/l/a/m/t/a;

    move-result-object v0

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->p:Lf/l/a/q/d;

    iget v1, v1, Lf/l/a/q/d;->g:I

    invoke-virtual {v0, v1}, Lf/l/a/m/t/a;->e(I)V

    iput v1, v0, Lf/l/a/m/t/a;->c:I

    invoke-virtual {v0}, Lf/l/a/m/t/a;->d()V

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0}, Lf/l/a/m/j;->H0()Lcom/google/android/gms/tasks/Task;

    :cond_3
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-boolean v1, p0, Lcom/otaliastudios/cameraview/CameraView;->D:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, v0, Lf/l/a/t/b$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->E:Lf/l/a/t/b;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public set(Lf/l/a/l/c;)V
    .locals 1
    .param p1    # Lf/l/a/l/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    instance-of v0, p1, Lf/l/a/l/a;

    if-eqz v0, :cond_0

    check-cast p1, Lf/l/a/l/a;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setAudio(Lf/l/a/l/a;)V

    goto/16 :goto_0

    :cond_0
    instance-of v0, p1, Lf/l/a/l/e;

    if-eqz v0, :cond_1

    check-cast p1, Lf/l/a/l/e;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setFacing(Lf/l/a/l/e;)V

    goto/16 :goto_0

    :cond_1
    instance-of v0, p1, Lf/l/a/l/f;

    if-eqz v0, :cond_2

    check-cast p1, Lf/l/a/l/f;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setFlash(Lf/l/a/l/f;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lf/l/a/l/g;

    if-eqz v0, :cond_3

    check-cast p1, Lf/l/a/l/g;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setGrid(Lf/l/a/l/g;)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lf/l/a/l/h;

    if-eqz v0, :cond_4

    check-cast p1, Lf/l/a/l/h;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setHdr(Lf/l/a/l/h;)V

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lf/l/a/l/i;

    if-eqz v0, :cond_5

    check-cast p1, Lf/l/a/l/i;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setMode(Lf/l/a/l/i;)V

    goto :goto_0

    :cond_5
    instance-of v0, p1, Lf/l/a/l/m;

    if-eqz v0, :cond_6

    check-cast p1, Lf/l/a/l/m;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setWhiteBalance(Lf/l/a/l/m;)V

    goto :goto_0

    :cond_6
    instance-of v0, p1, Lf/l/a/l/l;

    if-eqz v0, :cond_7

    check-cast p1, Lf/l/a/l/l;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setVideoCodec(Lf/l/a/l/l;)V

    goto :goto_0

    :cond_7
    instance-of v0, p1, Lf/l/a/l/b;

    if-eqz v0, :cond_8

    check-cast p1, Lf/l/a/l/b;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setAudioCodec(Lf/l/a/l/b;)V

    goto :goto_0

    :cond_8
    instance-of v0, p1, Lf/l/a/l/k;

    if-eqz v0, :cond_9

    check-cast p1, Lf/l/a/l/k;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setPreview(Lf/l/a/l/k;)V

    goto :goto_0

    :cond_9
    instance-of v0, p1, Lf/l/a/l/d;

    if-eqz v0, :cond_a

    check-cast p1, Lf/l/a/l/d;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setEngine(Lf/l/a/l/d;)V

    goto :goto_0

    :cond_a
    instance-of v0, p1, Lf/l/a/l/j;

    if-eqz v0, :cond_b

    check-cast p1, Lf/l/a/l/j;

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setPictureFormat(Lf/l/a/l/j;)V

    :cond_b
    :goto_0
    return-void
.end method

.method public setAudio(Lf/l/a/l/a;)V
    .locals 1
    .param p1    # Lf/l/a/l/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getAudio()Lf/l/a/l/a;

    move-result-object v0

    if-eq p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->a(Lf/l/a/l/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->Z(Lf/l/a/l/a;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->close()V

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->Z(Lf/l/a/l/a;)V

    :goto_1
    return-void
.end method

.method public setAudioBitRate(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->a0(I)V

    return-void
.end method

.method public setAudioCodec(Lf/l/a/l/b;)V
    .locals 1
    .param p1    # Lf/l/a/l/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->b0(Lf/l/a/l/b;)V

    return-void
.end method

.method public setAutoFocusMarker(Lf/l/a/r/a;)V
    .locals 4
    .param p1    # Lf/l/a/r/a;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->t:Lf/l/a/r/a;

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->B:Lf/l/a/r/c;

    iget-object v1, v0, Lf/l/a/r/c;->d:Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_0
    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lf/l/a/r/a;->b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object v1, v0, Lf/l/a/r/c;->d:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setAutoFocusResetDelay(J)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1, p2}, Lf/l/a/m/j;->c0(J)V

    return-void
.end method

.method public setEngine(Lf/l/a/l/d;)V
    .locals 2
    .param p1    # Lf/l/a/l/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->i:Lf/l/a/l/d;

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->b()V

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v1, v0}, Lf/l/a/m/j;->u0(Lf/l/a/v/a;)V

    :cond_1
    invoke-virtual {p1}, Lf/l/a/m/j;->l()Lf/l/a/l/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setFacing(Lf/l/a/l/e;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->m()Lf/l/a/l/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setFlash(Lf/l/a/l/f;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->t()Lf/l/a/l/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setMode(Lf/l/a/l/i;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->M()Lf/l/a/l/m;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setWhiteBalance(Lf/l/a/l/m;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->r()Lf/l/a/l/h;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setHdr(Lf/l/a/l/h;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->f()Lf/l/a/l/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setAudio(Lf/l/a/l/a;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->g()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setAudioBitRate(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->h()Lf/l/a/l/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setAudioCodec(Lf/l/a/l/b;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->x()Lf/l/a/w/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setPictureSize(Lf/l/a/w/c;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->u()Lf/l/a/l/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setPictureFormat(Lf/l/a/l/j;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->L()Lf/l/a/w/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setVideoSize(Lf/l/a/w/c;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->H()Lf/l/a/l/l;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setVideoCodec(Lf/l/a/l/l;)V

    invoke-virtual {p1}, Lf/l/a/m/j;->J()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/otaliastudios/cameraview/CameraView;->setVideoMaxSize(J)V

    invoke-virtual {p1}, Lf/l/a/m/j;->I()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setVideoMaxDuration(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->G()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setVideoBitRate(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/otaliastudios/cameraview/CameraView;->setAutoFocusResetDelay(J)V

    invoke-virtual {p1}, Lf/l/a/m/j;->A()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setPreviewFrameRate(F)V

    invoke-virtual {p1}, Lf/l/a/m/j;->B()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setPreviewFrameRateExact(Z)V

    invoke-virtual {p1}, Lf/l/a/m/j;->E()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setSnapshotMaxWidth(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->D()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setSnapshotMaxHeight(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->p()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingMaxWidth(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->o()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingMaxHeight(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingFormat(I)V

    invoke-virtual {p1}, Lf/l/a/m/j;->q()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/otaliastudios/cameraview/CameraView;->setFrameProcessingPoolSize(I)V

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lf/l/a/m/j;->k0(Z)V

    return-void
.end method

.method public setExperimental(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/otaliastudios/cameraview/CameraView;->C:Z

    return-void
.end method

.method public setExposureCorrection(F)V
    .locals 4

    invoke-virtual {p0}, Lcom/otaliastudios/cameraview/CameraView;->getCameraOptions()Lf/l/a/c;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v1, v0, Lf/l/a/c;->m:F

    iget v0, v0, Lf/l/a/c;->n:F

    cmpg-float v2, p1, v1

    if-gez v2, :cond_0

    move p1, v1

    :cond_0
    cmpl-float v2, p1, v0

    if-lez v2, :cond_1

    move p1, v0

    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v1, 0x1

    aput v0, v2, v1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v1, v3}, Lf/l/a/m/j;->d0(F[F[Landroid/graphics/PointF;Z)V

    :cond_2
    return-void
.end method

.method public setFacing(Lf/l/a/l/e;)V
    .locals 1
    .param p1    # Lf/l/a/l/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->e0(Lf/l/a/l/e;)V

    return-void
.end method

.method public setFilter(Lf/l/a/n/b;)V
    .locals 3
    .param p1    # Lf/l/a/n/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->j:Lf/l/a/n/b;

    goto :goto_1

    :cond_0
    instance-of v1, p1, Lf/l/a/n/c;

    instance-of v2, v0, Lf/l/a/v/b;

    if-nez v1, :cond_2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Filters are only supported by the GL_SURFACE preview. Current preview:"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    check-cast v0, Lf/l/a/v/b;

    invoke-interface {v0, p1}, Lf/l/a/v/b;->a(Lf/l/a/n/b;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public setFlash(Lf/l/a/l/f;)V
    .locals 1
    .param p1    # Lf/l/a/l/f;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->f0(Lf/l/a/l/f;)V

    return-void
.end method

.method public setFrameProcessingExecutors(I)V
    .locals 10

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    iput p1, p0, Lcom/otaliastudios/cameraview/CameraView;->k:I

    new-instance v9, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/otaliastudios/cameraview/CameraView$a;

    invoke-direct {v8, p0}, Lcom/otaliastudios/cameraview/CameraView$a;-><init>(Lcom/otaliastudios/cameraview/CameraView;)V

    move-object v1, v9

    move v2, p1

    move v3, p1

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    invoke-virtual {v9, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    iput-object v9, p0, Lcom/otaliastudios/cameraview/CameraView;->m:Ljava/util/concurrent/Executor;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Need at least 1 executor, got "

    invoke-static {v1, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setFrameProcessingFormat(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->g0(I)V

    return-void
.end method

.method public setFrameProcessingMaxHeight(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->h0(I)V

    return-void
.end method

.method public setFrameProcessingMaxWidth(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->i0(I)V

    return-void
.end method

.method public setFrameProcessingPoolSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->j0(I)V

    return-void
.end method

.method public setGrid(Lf/l/a/l/g;)V
    .locals 1
    .param p1    # Lf/l/a/l/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->A:Lf/l/a/q/c;

    invoke-virtual {v0, p1}, Lf/l/a/q/c;->setGridMode(Lf/l/a/l/g;)V

    return-void
.end method

.method public setGridColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->A:Lf/l/a/q/c;

    invoke-virtual {v0, p1}, Lf/l/a/q/c;->setGridColor(I)V

    return-void
.end method

.method public setHdr(Lf/l/a/l/h;)V
    .locals 1
    .param p1    # Lf/l/a/l/h;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->l0(Lf/l/a/l/h;)V

    return-void
.end method

.method public setLifecycleOwner(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 2
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->w:Landroidx/lifecycle/Lifecycle;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    iput-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->w:Landroidx/lifecycle/Lifecycle;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView;->w:Landroidx/lifecycle/Lifecycle;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    iput-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->w:Landroidx/lifecycle/Lifecycle;

    :cond_1
    invoke-interface {p1}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object p1

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->w:Landroidx/lifecycle/Lifecycle;

    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 1
    .param p1    # Landroid/location/Location;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->m0(Landroid/location/Location;)V

    return-void
.end method

.method public setMode(Lf/l/a/l/i;)V
    .locals 1
    .param p1    # Lf/l/a/l/i;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->n0(Lf/l/a/l/i;)V

    return-void
.end method

.method public setPictureFormat(Lf/l/a/l/j;)V
    .locals 1
    .param p1    # Lf/l/a/l/j;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->p0(Lf/l/a/l/j;)V

    return-void
.end method

.method public setPictureMetering(Z)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->q0(Z)V

    return-void
.end method

.method public setPictureSize(Lf/l/a/w/c;)V
    .locals 1
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->r0(Lf/l/a/w/c;)V

    return-void
.end method

.method public setPictureSnapshotMetering(Z)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->s0(Z)V

    return-void
.end method

.method public setPlaySounds(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView;->d:Z

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->t0(Z)V

    return-void
.end method

.method public setPreview(Lf/l/a/l/k;)V
    .locals 3
    .param p1    # Lf/l/a/l/k;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->h:Lf/l/a/l/k;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_2

    iget-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lf/l/a/v/a;->m()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView;->o:Lf/l/a/v/a;

    :cond_2
    return-void
.end method

.method public setPreviewFrameRate(F)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->v0(F)V

    return-void
.end method

.method public setPreviewFrameRateExact(Z)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->w0(Z)V

    return-void
.end method

.method public setPreviewStreamSize(Lf/l/a/w/c;)V
    .locals 1
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->x0(Lf/l/a/w/c;)V

    return-void
.end method

.method public setRequestPermissions(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/otaliastudios/cameraview/CameraView;->f:Z

    return-void
.end method

.method public setSnapshotMaxHeight(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->y0(I)V

    return-void
.end method

.method public setSnapshotMaxWidth(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->z0(I)V

    return-void
.end method

.method public setUseDeviceOrientation(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/otaliastudios/cameraview/CameraView;->e:Z

    return-void
.end method

.method public setVideoBitRate(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->A0(I)V

    return-void
.end method

.method public setVideoCodec(Lf/l/a/l/l;)V
    .locals 1
    .param p1    # Lf/l/a/l/l;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->B0(Lf/l/a/l/l;)V

    return-void
.end method

.method public setVideoMaxDuration(I)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->C0(I)V

    return-void
.end method

.method public setVideoMaxSize(J)V
    .locals 1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1, p2}, Lf/l/a/m/j;->D0(J)V

    return-void
.end method

.method public setVideoSize(Lf/l/a/w/c;)V
    .locals 1
    .param p1    # Lf/l/a/w/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->E0(Lf/l/a/w/c;)V

    return-void
.end method

.method public setWhiteBalance(Lf/l/a/l/m;)V
    .locals 1
    .param p1    # Lf/l/a/l/m;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    invoke-virtual {v0, p1}, Lf/l/a/m/j;->F0(Lf/l/a/l/m;)V

    return-void
.end method

.method public setZoom(F)V
    .locals 3

    const/4 v0, 0x0

    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v0

    if-lez v1, :cond_1

    const/high16 p1, 0x3f800000    # 1.0f

    :cond_1
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView;->q:Lf/l/a/m/j;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lf/l/a/m/j;->G0(F[Landroid/graphics/PointF;Z)V

    return-void
.end method
