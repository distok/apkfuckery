.class public Lcom/otaliastudios/cameraview/CameraView$b$f;
.super Ljava/lang/Object;
.source "CameraView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/otaliastudios/cameraview/CameraView$b;->d(Lf/l/a/p/a;ZLandroid/graphics/PointF;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Z

.field public final synthetic e:Lf/l/a/p/a;

.field public final synthetic f:Landroid/graphics/PointF;

.field public final synthetic g:Lcom/otaliastudios/cameraview/CameraView$b;


# direct methods
.method public constructor <init>(Lcom/otaliastudios/cameraview/CameraView$b;ZLf/l/a/p/a;Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->g:Lcom/otaliastudios/cameraview/CameraView$b;

    iput-boolean p2, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->d:Z

    iput-object p3, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->e:Lf/l/a/p/a;

    iput-object p4, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->f:Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-boolean v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->g:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-boolean v1, v0, Lcom/otaliastudios/cameraview/CameraView;->d:Z

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/otaliastudios/cameraview/CameraView;->s:Landroid/media/MediaActionSound;

    if-nez v1, :cond_0

    new-instance v1, Landroid/media/MediaActionSound;

    invoke-direct {v1}, Landroid/media/MediaActionSound;-><init>()V

    iput-object v1, v0, Lcom/otaliastudios/cameraview/CameraView;->s:Landroid/media/MediaActionSound;

    :cond_0
    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->s:Landroid/media/MediaActionSound;

    invoke-virtual {v0, v2}, Landroid/media/MediaActionSound;->play(I)V

    :cond_1
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->g:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->t:Lf/l/a/r/a;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->e:Lf/l/a/p/a;

    if-eqz v1, :cond_2

    sget-object v1, Lf/l/a/r/b;->d:Lf/l/a/r/b;

    goto :goto_0

    :cond_2
    sget-object v1, Lf/l/a/r/b;->e:Lf/l/a/r/b;

    :goto_0
    iget-boolean v2, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->d:Z

    iget-object v3, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->f:Landroid/graphics/PointF;

    invoke-interface {v0, v1, v2, v3}, Lf/l/a/r/a;->c(Lf/l/a/r/b;ZLandroid/graphics/PointF;)V

    :cond_3
    iget-object v0, p0, Lcom/otaliastudios/cameraview/CameraView$b$f;->g:Lcom/otaliastudios/cameraview/CameraView$b;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView$b;->c:Lcom/otaliastudios/cameraview/CameraView;

    iget-object v0, v0, Lcom/otaliastudios/cameraview/CameraView;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf/l/a/a;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    return-void
.end method
