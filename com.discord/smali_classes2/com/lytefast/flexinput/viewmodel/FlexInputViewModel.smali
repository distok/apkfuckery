.class public interface abstract Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;
.super Ljava/lang/Object;
.source "FlexInputViewModel.kt"


# virtual methods
.method public abstract hideExpressionTray()Z
.end method

.method public abstract observeEvents()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lf/b/a/f/a;",
            ">;"
        }
    .end annotation
.end method

.method public abstract observeState()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/lytefast/flexinput/viewmodel/FlexInputState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onAttachmentsUpdated(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "+",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract onCameraButtonClicked()V
.end method

.method public abstract onContentDialogDismissed()V
.end method

.method public abstract onContentDialogPageChanged(I)V
.end method

.method public abstract onExpandButtonClicked()V
.end method

.method public abstract onExpressionTrayButtonClicked()V
.end method

.method public abstract onFlexInputFragmentPause()V
.end method

.method public abstract onGalleryButtonClicked()V
.end method

.method public abstract onInputTextAppended(Ljava/lang/String;)V
.end method

.method public abstract onInputTextChanged(Ljava/lang/String;Ljava/lang/Boolean;)V
.end method

.method public abstract onInputTextClicked()Z
.end method

.method public abstract onSendButtonClicked(Lcom/lytefast/flexinput/InputListener;)V
.end method

.method public abstract onToolTipButtonLongPressed(Landroid/view/View;)Z
.end method
