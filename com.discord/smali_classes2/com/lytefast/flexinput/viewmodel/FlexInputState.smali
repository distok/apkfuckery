.class public final Lcom/lytefast/flexinput/viewmodel/FlexInputState;
.super Ljava/lang/Object;
.source "FlexInputState.kt"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Integer;

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z


# direct methods
.method public constructor <init>()V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;-><init>(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "+",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "+",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/Integer;",
            "ZZZZ)V"
        }
    .end annotation

    const-string v0, "inputText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachments"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    iput-object p3, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    iput-boolean p5, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    iput-boolean p6, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    iput-boolean p7, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    iput-boolean p8, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)V
    .locals 9

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v5, v0, 0x4

    if-eqz v5, :cond_2

    sget-object v2, Lx/h/l;->d:Lx/h/l;

    :cond_2
    and-int/lit8 v5, v0, 0x8

    const/4 v5, 0x0

    and-int/lit8 v6, v0, 0x10

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_2

    :cond_3
    move v6, p5

    :goto_2
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_4

    goto :goto_3

    :cond_4
    move v4, p6

    :goto_3
    and-int/lit8 v7, v0, 0x40

    const/4 v8, 0x0

    if-eqz v7, :cond_5

    const/4 v7, 0x0

    goto :goto_4

    :cond_5
    move/from16 v7, p7

    :goto_4
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    goto :goto_5

    :cond_6
    move/from16 v8, p8

    :goto_5
    move-object p1, p0

    move-object p2, v1

    move p3, v3

    move-object p4, v2

    move-object p5, v5

    move p6, v6

    move/from16 p7, v4

    move/from16 p8, v7

    move/from16 p9, v8

    invoke-direct/range {p1 .. p9}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;-><init>(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZ)V

    return-void
.end method

.method public static a(Lcom/lytefast/flexinput/viewmodel/FlexInputState;Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZI)Lcom/lytefast/flexinput/viewmodel/FlexInputState;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "inputText"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachments"

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    move-object p0, v0

    move-object p1, v2

    move p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move/from16 p8, v1

    invoke-direct/range {p0 .. p8}, Lcom/lytefast/flexinput/viewmodel/FlexInputState;-><init>(Ljava/lang/String;ZLjava/util/List;Ljava/lang/Integer;ZZZZ)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    iget-object v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    iget-boolean v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    iget-object v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    iget-boolean v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    iget-boolean v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    iget-boolean v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    iget-boolean p1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    if-eqz v1, :cond_7

    goto :goto_2

    :cond_7
    move v3, v1

    :goto_2
    add-int/2addr v0, v3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "FlexInputState(inputText="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", showExpandedButtons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showContentDialogIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ableToSendMessages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ableToAttachFiles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showExpressionTray="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showExpressionTrayButtonBadge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
