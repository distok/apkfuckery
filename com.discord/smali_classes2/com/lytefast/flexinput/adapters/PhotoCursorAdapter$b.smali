.class public final Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter$b;
.super Landroid/content/AsyncQueryHandler;
.source "PhotoCursorAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;->b(Landroid/content/ContentResolver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;Landroid/content/ContentResolver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter$b;->a:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method public onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 0

    const-string p1, "cookie"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p3, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter$b;->a:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    const-string p2, "_id"

    invoke-interface {p3, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    iput p2, p1, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;->c:I

    const-string p2, "_data"

    invoke-interface {p3, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    iput p2, p1, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;->d:I

    const-string p2, "_display_name"

    invoke-interface {p3, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p2

    iput p2, p1, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;->e:I

    iput-object p3, p1, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;->b:Landroid/database/Cursor;

    iget-object p1, p0, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter$b;->a:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method
