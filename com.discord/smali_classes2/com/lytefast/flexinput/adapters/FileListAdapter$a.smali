.class public final Lcom/lytefast/flexinput/adapters/FileListAdapter$a;
.super Landroid/os/AsyncTask;
.source "FileListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lytefast/flexinput/adapters/FileListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/io/File;",
        "Ljava/lang/Boolean;",
        "Ljava/util/List<",
        "+",
        "Lcom/lytefast/flexinput/model/Attachment<",
        "+",
        "Ljava/io/File;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/lytefast/flexinput/adapters/FileListAdapter;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/adapters/FileListAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/lytefast/flexinput/adapters/FileListAdapter$a;->a:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    return-void
.end method


# virtual methods
.method public doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, [Ljava/io/File;

    const-string v0, "rootFiles"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    aget-object p1, p1, v0

    sget-object v0, Lf/b/a/c/h;->d:Lf/b/a/c/h;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v0, p1}, Lf/b/a/c/h;->a(Ljava/io/File;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {v2, p1}, Lx/h/f;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    :goto_0
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {v2}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    const-string v3, "file"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->isHidden()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, p1}, Lf/b/a/c/h;->a(Ljava/io/File;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {v2, p1}, Lx/h/f;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lf/b/a/e/a;->a(Ljava/io/File;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance p1, Lf/b/a/c/g;

    invoke-direct {p1, p0}, Lf/b/a/c/g;-><init>(Lcom/lytefast/flexinput/adapters/FileListAdapter$a;)V

    new-instance v0, Lf/b/a/c/f;

    invoke-direct {v0}, Lf/b/a/c/f;-><init>()V

    const-string v2, "$this$then"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "comparator"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lx/i/b;

    invoke-direct {v2, p1, v0}, Lx/i/b;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->sortWith(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v1
.end method

.method public onPostExecute(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Ljava/util/List;

    const-string v0, "files"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/adapters/FileListAdapter$a;->a:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    iput-object p1, v0, Lcom/lytefast/flexinput/adapters/FileListAdapter;->b:Ljava/util/List;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method
