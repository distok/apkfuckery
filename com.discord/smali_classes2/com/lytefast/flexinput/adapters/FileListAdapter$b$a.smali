.class public final Lcom/lytefast/flexinput/adapters/FileListAdapter$b$a;
.super Ljava/lang/Object;
.source "FileListAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/adapters/FileListAdapter$b;-><init>(Lcom/lytefast/flexinput/adapters/FileListAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lcom/lytefast/flexinput/adapters/FileListAdapter$b;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/adapters/FileListAdapter$b;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/adapters/FileListAdapter$b$a;->d:Lcom/lytefast/flexinput/adapters/FileListAdapter$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lcom/lytefast/flexinput/adapters/FileListAdapter$b$a;->d:Lcom/lytefast/flexinput/adapters/FileListAdapter$b;

    iget-object v0, p1, Lcom/lytefast/flexinput/adapters/FileListAdapter$b;->h:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    iget-object v0, v0, Lcom/lytefast/flexinput/adapters/FileListAdapter;->a:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    iget-object v1, p1, Lcom/lytefast/flexinput/adapters/FileListAdapter$b;->g:Lcom/lytefast/flexinput/model/Attachment;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/utils/SelectionCoordinator;->c(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1, v2}, Lcom/lytefast/flexinput/utils/SelectionCoordinator;->b(Ljava/lang/Object;I)V

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v0, v3}, Lcom/lytefast/flexinput/adapters/FileListAdapter$b;->b(ZZ)V

    return-void
.end method
