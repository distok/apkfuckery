.class public final Lcom/lytefast/flexinput/fragment/PhotosFragment$b;
.super Ljava/lang/Object;
.source "PhotosFragment.kt"

# interfaces
.implements Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/PhotosFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

.field public final synthetic c:Lcom/lytefast/flexinput/fragment/PhotosFragment;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;Lcom/lytefast/flexinput/fragment/PhotosFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->a:Landroid/view/View;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->b:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    iput-object p3, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->c:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onRefresh()V
    .locals 3

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->c:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;->hasPermissions([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->b:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "context.contentResolver"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;->b(Landroid/content/ContentResolver;)V

    :cond_0
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;->c:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/PhotosFragment;->getSwipeRefreshLayout$flexinput_release()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    :cond_1
    return-void
.end method
