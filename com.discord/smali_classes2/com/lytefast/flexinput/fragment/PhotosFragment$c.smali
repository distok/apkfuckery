.class public final Lcom/lytefast/flexinput/fragment/PhotosFragment$c;
.super Ljava/lang/Object;
.source "PhotosFragment.kt"

# interfaces
.implements Lcom/lytefast/flexinput/fragment/PermissionsFragment$PermissionsResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/PhotosFragment;->requestPermissions(Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

.field public final synthetic b:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/PhotosFragment;Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->b:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/PhotosFragment;->getRecyclerView$flexinput_release()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    new-instance v1, Landroidx/recyclerview/widget/GridLayoutManager;

    iget-object v2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/PhotosFragment;->getRecyclerView$flexinput_release()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->b:Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/PhotosFragment;->getRecyclerView$flexinput_release()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->invalidateItemDecorations()V

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;->a:Lcom/lytefast/flexinput/fragment/PhotosFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/lytefast/flexinput/R$g;->files_permission_reason:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
