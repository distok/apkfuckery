.class public Lcom/lytefast/flexinput/fragment/CameraFragment;
.super Lcom/lytefast/flexinput/fragment/PermissionsFragment;
.source "CameraFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lytefast/flexinput/fragment/CameraFragment$b;
    }
.end annotation


# static fields
.field public static final l:[Ljava/lang/String;

.field public static final m:Lcom/lytefast/flexinput/fragment/CameraFragment$b;


# instance fields
.field public d:Landroid/view/View;

.field public e:Lcom/otaliastudios/cameraview/CameraView;

.field public f:Landroid/view/ViewStub;

.field public g:Landroid/widget/ImageView;

.field public h:Landroid/widget/ImageView;

.field public i:Landroid/widget/ImageView;

.field public j:Landroid/widget/ImageView;

.field public k:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/lytefast/flexinput/fragment/CameraFragment$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lytefast/flexinput/fragment/CameraFragment$b;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lytefast/flexinput/fragment/CameraFragment;->m:Lcom/lytefast/flexinput/fragment/CameraFragment$b;

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    const-string v2, "android.permission.CAMERA"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lytefast/flexinput/fragment/CameraFragment;->l:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;-><init>()V

    return-void
.end method

.method public static i(Lcom/lytefast/flexinput/fragment/CameraFragment;Ljava/lang/String;Ljava/lang/Exception;ZLjava/lang/String;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p4, p5, 0x2

    const/4 p6, 0x0

    if-eqz p4, :cond_0

    move-object p2, p6

    :cond_0
    and-int/lit8 p4, p5, 0x4

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p4, p5, 0x8

    if-eqz p4, :cond_2

    move-object p6, p1

    :cond_2
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p4, "Discord"

    invoke-static {p4, p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    if-eqz p3, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p6, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    :cond_3
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 3

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/otaliastudios/cameraview/CameraView;->getFacing()Lf/l/a/l/e;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    sget v1, Lcom/lytefast/flexinput/R$d;->ic_camera_front_white_24dp:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/lytefast/flexinput/R$d;->ic_camera_rear_white_24dp:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/lytefast/flexinput/R$d;->ic_camera_front_white_24dp:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_2
    const-string v0, "cameraView"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string v0, "cameraFacingBtn"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final g()V
    .locals 7

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->h:Landroid/widget/ImageView;

    const-string v1, "cameraFlashBtn"

    const/4 v2, 0x0

    if-eqz v0, :cond_a

    iget-object v3, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    const-string v4, "cameraView"

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/otaliastudios/cameraview/CameraView;->getFlash()Lf/l/a/l/f;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    const/4 v5, 0x3

    const/4 v6, 0x1

    if-eqz v3, :cond_2

    if-eq v3, v6, :cond_1

    if-eq v3, v5, :cond_0

    sget v3, Lcom/lytefast/flexinput/R$d;->ic_flash_auto_24dp:I

    goto :goto_0

    :cond_0
    sget v3, Lcom/lytefast/flexinput/R$d;->ic_flash_torch_24dp:I

    goto :goto_0

    :cond_1
    sget v3, Lcom/lytefast/flexinput/R$d;->ic_flash_on_24dp:I

    goto :goto_0

    :cond_2
    sget v3, Lcom/lytefast/flexinput/R$d;->ic_flash_off_24dp:I

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->h:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/otaliastudios/cameraview/CameraView;->getFlash()Lf/l/a/l/f;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_6

    if-eq v1, v6, :cond_5

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    if-eq v1, v5, :cond_3

    sget v1, Lcom/lytefast/flexinput/R$g;->flash_auto:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    sget v1, Lcom/lytefast/flexinput/R$g;->flash_torch:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    sget v1, Lcom/lytefast/flexinput/R$g;->flash_auto:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    sget v1, Lcom/lytefast/flexinput/R$g;->flash_on:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_6
    sget v1, Lcom/lytefast/flexinput/R$g;->flash_off:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_7
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_8
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_9
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_a
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public final h()Lf/b/a/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf/b/a/b<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lf/b/a/b;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    check-cast v1, Lf/b/a/b;

    return-object v1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p3, 0x11d7

    if-eq p3, p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->k:Ljava/io/File;

    if-eqz p1, :cond_3

    const/4 p3, -0x1

    if-eq p2, p3, :cond_1

    if-eqz p2, :cond_3

    sget p2, Lcom/lytefast/flexinput/R$g;->camera_intent_result_error:I

    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "getString(R.string.camera_intent_result_error)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    const/4 v0, 0x0

    invoke-static {p3, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/Toast;->show()V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    if-eqz p2, :cond_2

    new-instance p3, Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {p3, v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p2, p3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    invoke-virtual {p0}, Lcom/lytefast/flexinput/fragment/CameraFragment;->h()Lf/b/a/b;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-static {p1}, Lf/b/a/e/a;->a(Ljava/io/File;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object p1

    invoke-interface {p2, p1}, Lf/b/a/b;->e(Lcom/lytefast/flexinput/model/Attachment;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget p3, Lcom/lytefast/flexinput/R$f;->fragment_camera:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onResume()V
    .locals 8

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v4, "android.hardware.camera.any"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    sget-object v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->l:[Ljava/lang/String;

    array-length v4, v1

    invoke-static {v1, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;->hasPermissions([Ljava/lang/String;)Z

    move-result v1

    const-string v4, "cameraContainer"

    const/16 v5, 0x8

    const-string v6, "permissionsViewStub"

    const/4 v7, 0x0

    if-eqz v2, :cond_5

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->d:Landroid/view/View;

    if-eqz v1, :cond_4

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/ViewStub;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_3
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_4
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_5
    :goto_1
    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->d:Landroid/view/View;

    if-eqz v1, :cond_c

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Landroid/view/ViewStub;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    if-eqz v1, :cond_7

    invoke-virtual {v1, v0}, Landroid/view/ViewStub;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/lytefast/flexinput/R$e;->permissions_required_action_btn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/lytefast/flexinput/fragment/CameraFragment$c;

    invoke-direct {v2, p0}, Lcom/lytefast/flexinput/fragment/CameraFragment$c;-><init>(Lcom/lytefast/flexinput/fragment/CameraFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/lytefast/flexinput/R$e;->permissions_required_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById<TextVi\u2026ermissions_required_text)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/lytefast/flexinput/R$g;->system_permission_request_camera:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.syste\u2026ermission_request_camera)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lf/a/j/b/b/g;->b(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_7
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_8
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    if-eqz v0, :cond_a

    invoke-virtual {v0, v3}, Landroid/view/ViewStub;->setVisibility(I)V

    :cond_9
    :goto_2
    return-void

    :cond_a
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_b
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_c
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget p2, Lcom/lytefast/flexinput/R$e;->permissions_view_stub:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "view.findViewById(R.id.permissions_view_stub)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewStub;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->f:Landroid/view/ViewStub;

    sget p2, Lcom/lytefast/flexinput/R$e;->camera_container:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "view.findViewById(R.id.camera_container)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->d:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->camera_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "view.findViewById(R.id.camera_view)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/otaliastudios/cameraview/CameraView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/otaliastudios/cameraview/CameraView;->setLifecycleOwner(Landroidx/lifecycle/LifecycleOwner;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->e:Lcom/otaliastudios/cameraview/CameraView;

    const/4 v0, 0x0

    if-eqz p2, :cond_5

    new-instance v1, Lcom/lytefast/flexinput/fragment/CameraFragment$d;

    invoke-direct {v1, p0, p1}, Lcom/lytefast/flexinput/fragment/CameraFragment$d;-><init>(Lcom/lytefast/flexinput/fragment/CameraFragment;Landroid/view/View;)V

    iget-object p2, p2, Lcom/otaliastudios/cameraview/CameraView;->u:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget p2, Lcom/lytefast/flexinput/R$e;->take_photo_btn:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "view.findViewById(R.id.take_photo_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/lytefast/flexinput/fragment/CameraFragment$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p0}, Lcom/lytefast/flexinput/fragment/CameraFragment$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->i:Landroid/widget/ImageView;

    if-eqz p2, :cond_4

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    sget p2, Lcom/lytefast/flexinput/R$e;->launch_camera_btn:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "view.findViewById(R.id.launch_camera_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->j:Landroid/widget/ImageView;

    const-string v1, "launchCameraBtn"

    new-instance v2, Lcom/lytefast/flexinput/fragment/CameraFragment$a;

    const/4 v3, 0x1

    invoke-direct {v2, v3, p0}, Lcom/lytefast/flexinput/fragment/CameraFragment$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->j:Landroid/widget/ImageView;

    if-eqz p2, :cond_3

    const/4 v2, -0x1

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->j:Landroid/widget/ImageView;

    if-eqz p2, :cond_2

    sget v1, Lcom/lytefast/flexinput/R$d;->ic_launch_24dp:I

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    sget p2, Lcom/lytefast/flexinput/R$e;->camera_flash_btn:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "view.findViewById(R.id.camera_flash_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/lytefast/flexinput/fragment/CameraFragment$a;

    const/4 v3, 0x2

    invoke-direct {v1, v3, p0}, Lcom/lytefast/flexinput/fragment/CameraFragment$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->h:Landroid/widget/ImageView;

    if-eqz p2, :cond_1

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    sget p2, Lcom/lytefast/flexinput/R$e;->camera_facing_btn:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.camera_facing_btn)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->g:Landroid/widget/ImageView;

    new-instance p2, Lcom/lytefast/flexinput/fragment/CameraFragment$a;

    const/4 v1, 0x3

    invoke-direct {p2, v1, p0}, Lcom/lytefast/flexinput/fragment/CameraFragment$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment;->g:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    return-void

    :cond_0
    const-string p1, "cameraFacingBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string p1, "cameraFlashBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_4
    const-string p1, "takePhotoBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_5
    const-string p1, "cameraView"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
