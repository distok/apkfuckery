.class public Lcom/lytefast/flexinput/fragment/FilesFragment;
.super Lcom/lytefast/flexinput/fragment/PermissionsFragment;
.source "FilesFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lytefast/flexinput/fragment/FilesFragment$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/lytefast/flexinput/fragment/FilesFragment$Companion;

.field private static final REQUIRED_PERMISSION:Ljava/lang/String; = "android.permission.READ_EXTERNAL_STORAGE"


# instance fields
.field private adapter:Lcom/lytefast/flexinput/adapters/FileListAdapter;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lytefast/flexinput/utils/SelectionCoordinator<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation
.end field

.field private swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/lytefast/flexinput/fragment/FilesFragment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lytefast/flexinput/fragment/FilesFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lytefast/flexinput/fragment/FilesFragment;->Companion:Lcom/lytefast/flexinput/fragment/FilesFragment$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;-><init>()V

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/lytefast/flexinput/fragment/FilesFragment;)Lcom/lytefast/flexinput/adapters/FileListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->adapter:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    return-object p0
.end method

.method public static final synthetic access$getSelectionCoordinator$p(Lcom/lytefast/flexinput/fragment/FilesFragment;)Lcom/lytefast/flexinput/utils/SelectionCoordinator;
    .locals 0

    iget-object p0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    return-object p0
.end method

.method public static final synthetic access$loadDownloadFolder(Lcom/lytefast/flexinput/fragment/FilesFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/lytefast/flexinput/fragment/FilesFragment;->loadDownloadFolder()V

    return-void
.end method

.method public static final synthetic access$requestPermissions(Lcom/lytefast/flexinput/fragment/FilesFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/lytefast/flexinput/fragment/FilesFragment;->requestPermissions()V

    return-void
.end method

.method public static final synthetic access$setAdapter$p(Lcom/lytefast/flexinput/fragment/FilesFragment;Lcom/lytefast/flexinput/adapters/FileListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->adapter:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    return-void
.end method

.method public static final synthetic access$setSelectionCoordinator$p(Lcom/lytefast/flexinput/fragment/FilesFragment;Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    return-void
.end method

.method private final loadDownloadFolder()V
    .locals 4

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->adapter:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void

    :cond_0
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iget-object v2, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->adapter:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v3, "downloadFolder"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "root"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/lytefast/flexinput/adapters/FileListAdapter$a;

    invoke-direct {v3, v2}, Lcom/lytefast/flexinput/adapters/FileListAdapter$a;-><init>(Lcom/lytefast/flexinput/adapters/FileListAdapter;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/io/File;

    aput-object v0, v2, v1

    invoke-virtual {v3, v2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

.method private final requestPermissions()V
    .locals 2

    new-instance v0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;

    invoke-direct {v0, p0}, Lcom/lytefast/flexinput/fragment/FilesFragment$c;-><init>(Lcom/lytefast/flexinput/fragment/FilesFragment;)V

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;->requestPermissions(Lcom/lytefast/flexinput/fragment/PermissionsFragment$PermissionsResultCallback;[Ljava/lang/String;)Z

    return-void
.end method


# virtual methods
.method public final getRecyclerView$flexinput_release()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public final getSwipeRefreshLayout$flexinput_release()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method public newPermissionsRequestAdapter(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;
    .locals 3

    const-string v0, "onClickListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/lytefast/flexinput/adapters/EmptyListAdapter;

    sget v1, Lcom/lytefast/flexinput/R$f;->item_permission_storage:I

    sget v2, Lcom/lytefast/flexinput/R$e;->permissions_req_btn:I

    invoke-direct {v0, v1, v2, p1}, Lcom/lytefast/flexinput/adapters/EmptyListAdapter;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p3, v0, v0, v1}, Lcom/lytefast/flexinput/utils/SelectionCoordinator;-><init>(Landroidx/collection/ArrayMap;Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;I)V

    iput-object p3, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p3

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v0

    :goto_0
    instance-of v1, p3, Lf/b/a/b;

    if-nez v1, :cond_1

    move-object p3, v0

    :cond_1
    check-cast p3, Lf/b/a/b;

    if-eqz p3, :cond_2

    invoke-interface {p3}, Lf/b/a/b;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object p3

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p3, v1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->registerSelectionCoordinator(Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V

    :cond_2
    sget p3, Lcom/lytefast/flexinput/R$f;->fragment_recycler_view:I

    const/4 v1, 0x0

    invoke-virtual {p1, p3, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_6

    sget p2, Lcom/lytefast/flexinput/R$e;->list:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string p2, "android.permission.READ_EXTERNAL_STORAGE"

    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;->hasPermissions([Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_3

    new-instance p2, Lcom/lytefast/flexinput/adapters/FileListAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    const-string v0, "context"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p3

    const-string v0, "context.contentResolver"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-direct {p2, p3, v0}, Lcom/lytefast/flexinput/adapters/FileListAdapter;-><init>(Landroid/content/ContentResolver;Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->adapter:Lcom/lytefast/flexinput/adapters/FileListAdapter;

    iget-object p3, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p3, :cond_4

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_1

    :cond_3
    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p2, :cond_4

    new-instance p3, Lcom/lytefast/flexinput/fragment/FilesFragment$a;

    invoke-direct {p3, p0}, Lcom/lytefast/flexinput/fragment/FilesFragment$a;-><init>(Lcom/lytefast/flexinput/fragment/FilesFragment;)V

    invoke-virtual {p0, p3}, Lcom/lytefast/flexinput/fragment/FilesFragment;->newPermissionsRequestAdapter(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    :cond_4
    :goto_1
    sget p2, Lcom/lytefast/flexinput/R$e;->swipeRefreshLayout:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    if-eqz p2, :cond_5

    new-instance p3, Lcom/lytefast/flexinput/fragment/FilesFragment$b;

    invoke-direct {p3, p0}, Lcom/lytefast/flexinput/fragment/FilesFragment$b;-><init>(Lcom/lytefast/flexinput/fragment/FilesFragment;)V

    new-instance v0, Lf/b/a/a/e;

    invoke-direct {v0, p3}, Lf/b/a/a/e;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p2, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;)V

    :cond_5
    move-object v0, p1

    :cond_6
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/lytefast/flexinput/utils/SelectionCoordinator;->c:Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;

    invoke-interface {v0}, Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;->unregister()V

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    invoke-direct {p0}, Lcom/lytefast/flexinput/fragment/FilesFragment;->loadDownloadFolder()V

    return-void
.end method

.method public final setRecyclerView$flexinput_release(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public final setSwipeRefreshLayout$flexinput_release(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-void
.end method
