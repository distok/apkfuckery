.class public final Lcom/lytefast/flexinput/fragment/FilesFragment$c;
.super Ljava/lang/Object;
.source "FilesFragment.kt"

# interfaces
.implements Lcom/lytefast/flexinput/fragment/PermissionsFragment$PermissionsResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/FilesFragment;->requestPermissions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/lytefast/flexinput/fragment/FilesFragment;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FilesFragment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    new-instance v2, Lcom/lytefast/flexinput/adapters/FileListAdapter;

    invoke-static {v1}, Lcom/lytefast/flexinput/fragment/FilesFragment;->access$getSelectionCoordinator$p(Lcom/lytefast/flexinput/fragment/FilesFragment;)Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    move-result-object v3

    invoke-static {v3}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-direct {v2, v0, v3}, Lcom/lytefast/flexinput/adapters/FileListAdapter;-><init>(Landroid/content/ContentResolver;Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V

    invoke-static {v1, v2}, Lcom/lytefast/flexinput/fragment/FilesFragment;->access$setAdapter$p(Lcom/lytefast/flexinput/fragment/FilesFragment;Lcom/lytefast/flexinput/adapters/FileListAdapter;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/FilesFragment;->getRecyclerView$flexinput_release()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    invoke-static {v1}, Lcom/lytefast/flexinput/fragment/FilesFragment;->access$getAdapter$p(Lcom/lytefast/flexinput/fragment/FilesFragment;)Lcom/lytefast/flexinput/adapters/FileListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    :cond_0
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    invoke-static {v0}, Lcom/lytefast/flexinput/fragment/FilesFragment;->access$loadDownloadFolder(Lcom/lytefast/flexinput/fragment/FilesFragment;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FilesFragment$c;->a:Lcom/lytefast/flexinput/fragment/FilesFragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/lytefast/flexinput/R$g;->files_permission_reason:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
