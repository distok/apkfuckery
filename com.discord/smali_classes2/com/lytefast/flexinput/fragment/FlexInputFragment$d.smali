.class public final synthetic Lcom/lytefast/flexinput/fragment/FlexInputFragment$d;
.super Lx/m/c/i;
.source "FlexInputFragment.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/FlexInputFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/lytefast/flexinput/viewmodel/FlexInputState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V
    .locals 7

    const-class v3, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    const/4 v1, 0x1

    const-string v4, "configureUI"

    const-string v5, "configureUI(Lcom/lytefast/flexinput/viewmodel/FlexInputState;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    check-cast p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    sget-object v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->E:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lf/b/a/a/j;->d:Lf/b/a/a/j;

    new-instance v2, Lf/b/a/a/k;

    invoke-direct {v2, v0}, Lf/b/a/a/k;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    new-instance v3, Lf/b/a/a/l;

    invoke-direct {v3, v0}, Lf/b/a/a/l;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    new-instance v4, Lf/b/a/a/m;

    invoke-direct {v4, v0}, Lf/b/a/a/m;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m()Z

    move-result v5

    if-nez v5, :cond_0

    goto/16 :goto_e

    :cond_0
    iget-object v5, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    const-string v6, "inputEt"

    const/4 v7, 0x0

    if-eqz v5, :cond_21

    invoke-virtual {v5}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v8, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-static {v5, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    const/4 v8, 0x1

    xor-int/2addr v5, v8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    if-eqz v5, :cond_2

    iget-object v9, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-virtual {v5, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    if-eqz v5, :cond_1

    iget-object v6, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :cond_1
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_2
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_3
    :goto_0
    iget-boolean v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->b:Z

    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->t:Landroid/view/View;

    if-eqz v6, :cond_20

    const/16 v9, 0x8

    const/4 v10, 0x0

    if-nez v5, :cond_4

    const/4 v11, 0x0

    goto :goto_1

    :cond_4
    const/16 v11, 0x8

    :goto_1
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->s:Landroid/view/View;

    if-eqz v6, :cond_1f

    if-eqz v5, :cond_5

    const/4 v11, 0x0

    goto :goto_2

    :cond_5
    const/16 v11, 0x8

    :goto_2
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->r:Landroid/view/View;

    if-eqz v6, :cond_1e

    if-eqz v5, :cond_6

    const/4 v5, 0x0

    goto :goto_3

    :cond_6
    const/16 v5, 0x8

    :goto_3
    invoke-virtual {v6, v5}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    if-eqz v5, :cond_9

    iget-object v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    const-string v6, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-static {v5, v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {v5}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    const/4 v5, 0x1

    goto :goto_4

    :cond_7
    const/4 v5, 0x0

    :goto_4
    if-nez v5, :cond_8

    iget-object v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    if-lez v5, :cond_9

    :cond_8
    const/4 v5, 0x1

    goto :goto_5

    :cond_9
    const/4 v5, 0x0

    :goto_5
    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->o:Landroid/view/View;

    if-eqz v6, :cond_1d

    invoke-virtual {v6, v5}, Landroid/view/View;->setEnabled(Z)V

    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->p:Landroid/view/View;

    const-string v11, "sendBtn"

    if-eqz v6, :cond_1c

    invoke-virtual {v6, v5}, Landroid/view/View;->setEnabled(Z)V

    iget-object v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_a

    const/4 v5, 0x1

    goto :goto_6

    :cond_a
    const/4 v5, 0x0

    :goto_6
    if-nez v5, :cond_c

    iget-object v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    if-lez v5, :cond_b

    goto :goto_7

    :cond_b
    const/4 v5, 0x0

    goto :goto_8

    :cond_c
    :goto_7
    const/4 v5, 0x1

    :goto_8
    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->p:Landroid/view/View;

    if-eqz v6, :cond_1b

    if-eqz v5, :cond_d

    const/4 v11, 0x0

    goto :goto_9

    :cond_d
    const/16 v11, 0x8

    :goto_9
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h:Landroid/view/ViewGroup;

    const-string v11, "inputContainer"

    if-eqz v6, :cond_1a

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    const-string v12, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    invoke-static {v6, v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v5, :cond_e

    invoke-static {v9}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v5

    goto :goto_a

    :cond_e
    const/4 v5, 0x0

    :goto_a
    iput v5, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v5, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h:Landroid/view/ViewGroup;

    if-eqz v5, :cond_19

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->f:Landroid/view/View;

    if-eqz v5, :cond_18

    iget-object v6, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    if-lez v6, :cond_f

    iget-boolean v6, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    if-eqz v6, :cond_f

    const/4 v6, 0x0

    goto :goto_b

    :cond_f
    const/16 v6, 0x8

    :goto_b
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->e:Landroid/view/ViewGroup;

    if-eqz v5, :cond_17

    iget-boolean v6, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    if-eqz v6, :cond_10

    iget-boolean v6, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->f:Z

    if-eqz v6, :cond_10

    const/4 v6, 0x1

    goto :goto_c

    :cond_10
    const/4 v6, 0x0

    :goto_c
    invoke-virtual {v1, v5, v6}, Lf/b/a/a/j;->a(Landroid/view/ViewGroup;Z)V

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v5, "Add Content"

    invoke-virtual {v1, v5}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lf/b/a/a/a;

    iget-object v5, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->d:Ljava/lang/Integer;

    if-eqz v5, :cond_12

    if-nez v1, :cond_11

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :try_start_0
    iget-object v2, v2, Lf/b/a/a/k;->this$0:Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-static {v2, v1}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->f(Lcom/lytefast/flexinput/fragment/FlexInputFragment;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_d

    :catch_0
    move-exception v1

    sget-object v2, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->E:Ljava/lang/String;

    const-string v5, "Could not open AddContentDialogFragment"

    invoke-static {v2, v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_d

    :cond_11
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, v1, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v1, :cond_13

    invoke-virtual {v1, v2}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    goto :goto_d

    :cond_12
    if-eqz v1, :cond_13

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v2

    if-ne v2, v8, :cond_13

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->isDetached()Z

    move-result v2

    if-nez v2, :cond_13

    invoke-virtual {v1}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_13
    :goto_d
    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->l:Landroid/view/View;

    if-eqz v0, :cond_16

    iget-boolean v1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->e:Z

    if-nez v1, :cond_14

    const/4 v9, 0x0

    :cond_14
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->g:Z

    if-eqz v0, :cond_15

    invoke-virtual {v4}, Lf/b/a/a/m;->invoke()V

    goto :goto_e

    :cond_15
    iget-boolean p1, p1, Lcom/lytefast/flexinput/viewmodel/FlexInputState;->h:Z

    invoke-virtual {v3, p1}, Lf/b/a/a/l;->invoke(Z)Z

    :goto_e
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1

    :cond_16
    const-string p1, "cannotSendText"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_17
    const-string p1, "leftBtnsContainer"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_18
    const-string p1, "attachmentPreviewContainer"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_19
    invoke-static {v11}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_1a
    invoke-static {v11}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_1b
    invoke-static {v11}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_1c
    invoke-static {v11}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_1d
    const-string p1, "sendBtnImage"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_1e
    const-string p1, "galleryBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_1f
    const-string p1, "cameraBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_20
    const-string p1, "expandBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_21
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7
.end method
