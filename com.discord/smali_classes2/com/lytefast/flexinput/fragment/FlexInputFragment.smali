.class public Lcom/lytefast/flexinput/fragment/FlexInputFragment;
.super Landroidx/fragment/app/Fragment;
.source "FlexInputFragment.kt"

# interfaces
.implements Lf/b/a/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/fragment/app/Fragment;",
        "Lf/b/a/b<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final E:Ljava/lang/String;


# instance fields
.field public A:Lrx/Subscription;

.field public B:Lrx/Subscription;

.field public C:Lrx/Subscription;

.field public final D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Landroid/view/ViewGroup;

.field public e:Landroid/view/ViewGroup;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;

.field public h:Landroid/view/ViewGroup;

.field public i:Landroid/view/ViewGroup;

.field public j:Landroid/view/View;

.field public k:Landroidx/recyclerview/widget/RecyclerView;

.field public l:Landroid/view/View;

.field public m:Landroid/widget/ImageView;

.field public n:Landroid/widget/ImageView;

.field public o:Landroid/view/View;

.field public p:Landroid/view/View;

.field public q:Lcom/lytefast/flexinput/widget/FlexEditText;

.field public r:Landroid/view/View;

.field public s:Landroid/view/View;

.field public t:Landroid/view/View;

.field public u:Lf/b/a/d/a;

.field public v:Lcom/lytefast/flexinput/InputListener;

.field public w:Lcom/lytefast/flexinput/managers/FileManager;

.field public x:Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public y:[Lf/b/a/c/d$a;

.field public z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->E:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->D:Ljava/util/List;

    return-void
.end method

.method public static final f(Lcom/lytefast/flexinput/fragment/FlexInputFragment;I)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "childFragmentManager.beginTransaction()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lf/b/a/a/a;

    invoke-direct {v1}, Lf/b/a/a/a;-><init>()V

    const-string v2, "Add Content"

    invoke-virtual {v1, v0, v2}, Lf/b/a/a/a;->show(Landroidx/fragment/app/FragmentTransaction;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->executePendingTransactions()Z

    invoke-virtual {v1}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    :cond_1
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onContentDialogPageChanged(I)V

    :cond_2
    new-instance p1, Lf/b/a/a/f;

    invoke-direct {p1, p0}, Lf/b/a/a/f;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v1, Lf/b/a/a/a;->j:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_3

    iget-object v2, v1, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v2, :cond_3

    invoke-virtual {v2, v0}, Landroidx/viewpager/widget/ViewPager;->removeOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    :cond_3
    iput-object p1, v1, Lf/b/a/a/a;->j:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    iget-object v0, v1, Lf/b/a/a/a;->d:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    :cond_4
    invoke-virtual {v1}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object p1

    if-eqz p1, :cond_5

    new-instance v0, Lf/b/a/a/g;

    invoke-direct {v0, p0}, Lf/b/a/a/g;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_5
    new-instance p1, Lf/b/a/a/h;

    invoke-direct {p1, p0}, Lf/b/a/a/h;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    const-string p0, "onKeyboardSelectedListener"

    invoke-static {p1, p0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, v1, Lf/b/a/a/a;->i:Lkotlin/jvm/functions/Function1;

    :goto_0
    return-void
.end method

.method public static final synthetic g(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->j:Landroid/view/View;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "defaultWindowInsetsHandler"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic h(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m:Landroid/widget/ImageView;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "expressionBtn"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic i(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i:Landroid/view/ViewGroup;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "expressionContainer"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final j(Lcom/lytefast/flexinput/fragment/FlexInputFragment;Z)V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p0

    sget v0, Lcom/lytefast/flexinput/R$e;->flex_input_expression_tray_container:I

    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p0, Lf/b/a/e/b;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    :cond_0
    check-cast p0, Lf/b/a/e/b;

    if-eqz p0, :cond_1

    invoke-interface {p0, p1}, Lf/b/a/e/b;->isShown(Z)V

    :cond_1
    return-void
.end method


# virtual methods
.method public b()Lcom/lytefast/flexinput/utils/SelectionAggregator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/lytefast/flexinput/utils/SelectionAggregator<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->x:Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;->a:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    return-object v0

    :cond_0
    const-string v0, "attachmentPreviewAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public e(Lcom/lytefast/flexinput/model/Attachment;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "attachment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "Add Content"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Landroidx/fragment/app/DialogFragment;

    new-instance v1, Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-direct {v1, v2, v2, v3}, Lcom/lytefast/flexinput/utils/SelectionCoordinator;-><init>(Landroidx/collection/ArrayMap;Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;I)V

    invoke-virtual {p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->registerSelectionCoordinator(Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Lcom/lytefast/flexinput/utils/SelectionCoordinator;->b(Ljava/lang/Object;I)V

    iget-object p1, v1, Lcom/lytefast/flexinput/utils/SelectionCoordinator;->c:Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;

    invoke-interface {p1}, Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;->unregister()V

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->getAttachments()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onAttachmentsUpdated(Ljava/util/List;)V

    :cond_0
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_1

    new-instance v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment$b;

    invoke-direct {v1, v0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment$b;-><init>(Landroidx/fragment/app/DialogFragment;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    const-string p1, "attachmentPreviewList"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public getFileManager()Lcom/lytefast/flexinput/managers/FileManager;
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->w:Lcom/lytefast/flexinput/managers/FileManager;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "fileManager"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final k(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onViewCreatedUpdate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->D:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public final l()Lcom/lytefast/flexinput/widget/FlexEditText;
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "inputEt"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public final m()Z
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final n()V
    .locals 2

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->v:Lcom/lytefast/flexinput/InputListener;

    invoke-interface {v0, v1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onSendButtonClicked(Lcom/lytefast/flexinput/InputListener;)V

    :cond_0
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->x:Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;->a:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    invoke-virtual {v1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->clear()V

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void

    :cond_1
    const-string v0, "attachmentPreviewAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget p3, Lcom/lytefast/flexinput/R$f;->flex_input_widget:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type android.widget.LinearLayout"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Landroid/widget/LinearLayout;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_attachment_preview_container:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026chment_preview_container)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->f:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_attachment_clear_btn:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026put_attachment_clear_btn)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->g:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_main_input_container:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026put_main_input_container)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->h:Landroid/view/ViewGroup;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_expression_tray_container:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026xpression_tray_container)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i:Landroid/view/ViewGroup;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_default_window_insets_handler:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026lt_window_insets_handler)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->j:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_attachment_preview_list:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026_attachment_preview_list)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->k:Landroidx/recyclerview/widget/RecyclerView;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_container:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.flex_input_container)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->d:Landroid/view/ViewGroup;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_left_btns_container:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.f\u2026nput_left_btns_container)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->e:Landroid/view/ViewGroup;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_text_input:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "root.findViewById(R.id.flex_input_text_input)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/lytefast/flexinput/widget/FlexEditText;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    new-instance p3, Lcom/lytefast/flexinput/fragment/FlexInputFragment$c;

    invoke-direct {p3, p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment$c;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    invoke-static {p2, p0, p3}, Lcom/discord/utilities/view/text/TextWatcherKt;->addBindedTextWatcher(Landroid/widget/TextView;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    const/4 p3, 0x0

    if-eqz p2, :cond_10

    new-instance v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;

    invoke-direct {v1, v0, p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_cannot_send_text:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.f\u2026x_input_cannot_send_text)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->l:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_expression_btn:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.flex_input_expression_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m:Landroid/widget/ImageView;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_expression_btn_badge:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.f\u2026put_expression_btn_badge)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->n:Landroid/widget/ImageView;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_send_btn_image:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.flex_input_send_btn_image)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->o:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_send_btn_container:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.f\u2026input_send_btn_container)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->p:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_camera_btn:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.flex_input_camera_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->s:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_gallery_btn:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.flex_input_gallery_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->r:Landroid/view/View;

    sget p2, Lcom/lytefast/flexinput/R$e;->flex_input_expand_btn:I

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v1, "root.findViewById(R.id.flex_input_expand_btn)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->t:Landroid/view/View;

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->g:Landroid/view/View;

    const-string v1, "attachmentClearButton"

    if-eqz p2, :cond_f

    new-instance v2, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;

    const/4 v3, 0x1

    invoke-direct {v2, v3, p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment$a;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m:Landroid/widget/ImageView;

    const-string v2, "expressionBtn"

    if-eqz p2, :cond_e

    new-instance v4, Li;

    invoke-direct {v4, v0, p0}, Li;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->p:Landroid/view/View;

    const-string v4, "sendBtn"

    if-eqz p2, :cond_d

    new-instance v5, Li;

    invoke-direct {v5, v3, p0}, Li;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->s:Landroid/view/View;

    const-string v5, "cameraBtn"

    if-eqz p2, :cond_c

    new-instance v6, Li;

    const/4 v7, 0x2

    invoke-direct {v6, v7, p0}, Li;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->r:Landroid/view/View;

    const-string v6, "galleryBtn"

    if-eqz p2, :cond_b

    new-instance v8, Li;

    const/4 v9, 0x3

    invoke-direct {v8, v9, p0}, Li;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->t:Landroid/view/View;

    const-string v8, "expandBtn"

    if-eqz p2, :cond_a

    new-instance v10, Li;

    const/4 v11, 0x4

    invoke-direct {v10, v11, p0}, Li;-><init>(ILjava/lang/Object;)V

    invoke-virtual {p2, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x6

    new-array p2, p2, [Landroid/view/View;

    iget-object v10, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->g:Landroid/view/View;

    if-eqz v10, :cond_9

    aput-object v10, p2, v0

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_8

    aput-object v0, p2, v3

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->s:Landroid/view/View;

    if-eqz v0, :cond_7

    aput-object v0, p2, v7

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->m:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    aput-object v0, p2, v9

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->p:Landroid/view/View;

    if-eqz v0, :cond_5

    aput-object v0, p2, v11

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->t:Landroid/view/View;

    if-eqz v1, :cond_4

    aput-object v1, p2, v0

    invoke-static {p2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lf/b/a/a/i;

    invoke-direct {v1, p0}, Lf/b/a/a/i;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i:Landroid/view/ViewGroup;

    const-string v0, "expressionContainer"

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    const-string v1, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams"

    invoke-static {p2, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p2, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    const-string v3, "requireActivity()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    const/high16 v2, 0x3f000000    # 0.5f

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    mul-float v1, v1, v2

    float-to-int v1, v1

    iput v1, p2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_2
    :goto_1
    return-object p1

    :cond_3
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_4
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_5
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_6
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_7
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_8
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_9
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_a
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_b
    invoke-static {v6}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_c
    invoke-static {v5}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_d
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_e
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_f
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3

    :cond_10
    const-string p1, "inputEt"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p3
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroyView()V

    sget-object v0, Lcom/discord/utilities/view/text/TextWatcher;->Companion:Lcom/discord/utilities/view/text/TextWatcher$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/utilities/view/text/TextWatcher$Companion;->reset(Landroidx/fragment/app/Fragment;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onFlexInputFragmentPause()V

    :cond_0
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->A:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->B:Lrx/Subscription;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_2
    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->C:Lrx/Subscription;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_3
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->observeState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment$d;

    invoke-direct {v1, p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment$d;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    new-instance v2, Lf/b/a/a/o;

    invoke-direct {v2, v1}, Lf/b/a/a/o;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->Q(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->A:Lrx/Subscription;

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment$e;

    invoke-direct {v1, p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment$e;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    new-instance v2, Lf/b/a/a/o;

    invoke-direct {v2, v1}, Lf/b/a/a/o;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v2}, Lrx/Observable;->Q(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->B:Lrx/Subscription;

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->getAttachments()Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "FlexInput.ATTACHMENTS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FlexInput.TEXT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string p1, "inputEt"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;-><init>(Lkotlin/jvm/functions/Function1;)V

    iget-object v1, p1, Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;->a:Lcom/lytefast/flexinput/utils/SelectionAggregator;

    new-instance v2, Lf/b/a/a/n;

    invoke-direct {v2, p0}, Lf/b/a/a/n;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    invoke-virtual {v1, v2}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->addItemSelectionListener(Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;)Lcom/lytefast/flexinput/utils/SelectionAggregator;

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->x:Lcom/lytefast/flexinput/adapters/AttachmentPreviewAdapter;

    if-eqz p2, :cond_2

    const-string p1, "FlexInput.ATTACHMENTS"

    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->initFrom(Ljava/util/ArrayList;)Lcom/lytefast/flexinput/utils/SelectionAggregator;

    :cond_0
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->getAttachments()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;->onAttachmentsUpdated(Ljava/util/List;)V

    :cond_1
    const-string p1, "FlexInput.TEXT"

    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->z:Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;

    if-eqz p2, :cond_2

    const/4 v1, 0x2

    invoke-static {p2, p1, v0, v1, v0}, Lf/h/a/f/f/n/g;->U(Lcom/lytefast/flexinput/viewmodel/FlexInputViewModel;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    :cond_2
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->i:Landroid/view/ViewGroup;

    if-eqz p1, :cond_6

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->j:Landroid/view/View;

    if-eqz p1, :cond_5

    sget-object p2, Lf/b/a/a/p;->a:Lf/b/a/a/p;

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->d:Landroid/view/ViewGroup;

    if-eqz p1, :cond_4

    new-instance p2, Lf/b/a/a/q;

    invoke-direct {p2, p0}, Lf/b/a/a/q;-><init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->D:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->D:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void

    :cond_4
    const-string p1, "container"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_5
    const-string p1, "defaultWindowInsetsHandler"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_6
    const-string p1, "expressionContainer"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
