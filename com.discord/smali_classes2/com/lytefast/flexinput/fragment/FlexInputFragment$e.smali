.class public final synthetic Lcom/lytefast/flexinput/fragment/FlexInputFragment$e;
.super Lx/m/c/i;
.source "FlexInputFragment.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/FlexInputFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lf/b/a/f/a;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/FlexInputFragment;)V
    .locals 7

    const-class v3, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    const/4 v1, 0x1

    const-string v4, "handleEvent"

    const-string v5, "handleEvent(Lcom/lytefast/flexinput/viewmodel/FlexInputEvent;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Lf/b/a/f/a;

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;

    sget-object v1, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->E:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v1, p1, Lf/b/a/f/a$c;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    check-cast p1, Lf/b/a/f/a$c;

    iget-object p1, p1, Lf/b/a/f/a$c;->a:Ljava/lang/String;

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_0
    instance-of v1, p1, Lf/b/a/f/a$d;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    check-cast p1, Lf/b/a/f/a$d;

    iget p1, p1, Lf/b/a/f/a$d;->a:I

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    instance-of v1, p1, Lf/b/a/f/a$b;

    if-eqz v1, :cond_3

    iget-object p1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->u:Lf/b/a/d/a;

    if-eqz p1, :cond_4

    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->q:Lcom/lytefast/flexinput/widget/FlexEditText;

    if-eqz v0, :cond_2

    invoke-interface {p1, v0}, Lf/b/a/d/a;->requestDisplay(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    const-string p1, "inputEt"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_3
    instance-of p1, p1, Lf/b/a/f/a$a;

    if-eqz p1, :cond_4

    iget-object p1, v0, Lcom/lytefast/flexinput/fragment/FlexInputFragment;->u:Lf/b/a/d/a;

    if-eqz p1, :cond_4

    invoke-interface {p1}, Lf/b/a/d/a;->requestHide()V

    :cond_4
    :goto_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
