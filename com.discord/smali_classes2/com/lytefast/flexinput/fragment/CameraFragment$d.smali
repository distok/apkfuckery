.class public final Lcom/lytefast/flexinput/fragment/CameraFragment$d;
.super Lf/l/a/a;
.source "CameraFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lytefast/flexinput/fragment/CameraFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/lytefast/flexinput/fragment/CameraFragment;

.field public final synthetic b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/lytefast/flexinput/fragment/CameraFragment;Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->b:Landroid/view/View;

    invoke-direct {p0}, Lf/l/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/otaliastudios/cameraview/CameraException;)V
    .locals 8

    const-string v0, "exception"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    sget v0, Lcom/lytefast/flexinput/R$g;->camera_unknown_error:I

    invoke-virtual {v1, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "getString(R.string.camera_unknown_error)"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v3, p1

    invoke-static/range {v1 .. v7}, Lcom/lytefast/flexinput/fragment/CameraFragment;->i(Lcom/lytefast/flexinput/fragment/CameraFragment;Ljava/lang/String;Ljava/lang/Exception;ZLjava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public b(Lf/l/a/c;)V
    .locals 6

    const-string v0, "cameraOptions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    sget-object v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->l:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/CameraFragment;->g()V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/CameraFragment;->f()V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/CameraFragment;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lf/l/a/c;->a()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-le v2, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/16 v5, 0x8

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    iget-object v0, v0, Lcom/lytefast/flexinput/fragment/CameraFragment;->h:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lf/l/a/c;->b()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p1

    if-le p1, v4, :cond_2

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    const/16 v3, 0x8

    :goto_3
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_4
    const-string p1, "cameraFlashBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_5
    const-string p1, "cameraFacingBtn"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public c(Lf/l/a/k;)V
    .locals 4

    const-string v0, "result"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/CameraFragment$d;->a:Lcom/lytefast/flexinput/fragment/CameraFragment;

    sget-object v1, Lcom/lytefast/flexinput/fragment/CameraFragment;->l:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/lytefast/flexinput/fragment/CameraFragment;->h()Lf/b/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/b/a/b;->getFileManager()Lcom/lytefast/flexinput/managers/FileManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/lytefast/flexinput/managers/FileManager;->b()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/lytefast/flexinput/fragment/CameraFragment$d$a;

    invoke-direct {v1, p0}, Lcom/lytefast/flexinput/fragment/CameraFragment$d$a;-><init>(Lcom/lytefast/flexinput/fragment/CameraFragment$d;)V

    iget-object p1, p1, Lf/l/a/k;->a:[B

    sget v2, Lf/l/a/e;->a:I

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lf/l/a/d;

    invoke-direct {v3, p1, v0, v2, v1}, Lf/l/a/d;-><init>([BLjava/io/File;Landroid/os/Handler;Lf/l/a/j;)V

    const-string p1, "FallbackCameraThread"

    invoke-static {p1}, Lf/l/a/q/e;->a(Ljava/lang/String;)Lf/l/a/q/e;

    move-result-object p1

    sput-object p1, Lf/l/a/q/e;->g:Lf/l/a/q/e;

    iget-object p1, p1, Lf/l/a/q/e;->c:Landroid/os/Handler;

    invoke-virtual {p1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
