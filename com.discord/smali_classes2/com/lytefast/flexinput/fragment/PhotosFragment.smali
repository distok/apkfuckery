.class public Lcom/lytefast/flexinput/fragment/PhotosFragment;
.super Lcom/lytefast/flexinput/fragment/PermissionsFragment;
.source "PhotosFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lytefast/flexinput/fragment/PhotosFragment$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/lytefast/flexinput/fragment/PhotosFragment$Companion;

.field private static final GRID_LAYOUT_SPAN_COUNT:I = 0x3

.field private static final REQUIRED_PERMISSION:Ljava/lang/String; = "android.permission.READ_EXTERNAL_STORAGE"


# instance fields
.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/lytefast/flexinput/utils/SelectionCoordinator<",
            "Lcom/lytefast/flexinput/model/Attachment<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/lytefast/flexinput/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/lytefast/flexinput/fragment/PhotosFragment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/lytefast/flexinput/fragment/PhotosFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->Companion:Lcom/lytefast/flexinput/fragment/PhotosFragment$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;-><init>()V

    return-void
.end method

.method public static final synthetic access$requestPermissions(Lcom/lytefast/flexinput/fragment/PhotosFragment;Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/lytefast/flexinput/fragment/PhotosFragment;->requestPermissions(Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;)V

    return-void
.end method

.method private final requestPermissions(Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;)V
    .locals 1

    new-instance v0, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;

    invoke-direct {v0, p0, p1}, Lcom/lytefast/flexinput/fragment/PhotosFragment$c;-><init>(Lcom/lytefast/flexinput/fragment/PhotosFragment;Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;)V

    const-string p1, "android.permission.READ_EXTERNAL_STORAGE"

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;->requestPermissions(Lcom/lytefast/flexinput/fragment/PermissionsFragment$PermissionsResultCallback;[Ljava/lang/String;)Z

    return-void
.end method


# virtual methods
.method public final getRecyclerView$flexinput_release()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public final getSwipeRefreshLayout$flexinput_release()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method public newPermissionsRequestAdapter(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;
    .locals 3

    const-string v0, "onClickListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/lytefast/flexinput/adapters/EmptyListAdapter;

    sget v1, Lcom/lytefast/flexinput/R$f;->item_permission_storage:I

    sget v2, Lcom/lytefast/flexinput/R$e;->permissions_req_btn:I

    invoke-direct {v0, v1, v2, p1}, Lcom/lytefast/flexinput/adapters/EmptyListAdapter;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p3, v0, v0, v1}, Lcom/lytefast/flexinput/utils/SelectionCoordinator;-><init>(Landroidx/collection/ArrayMap;Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;I)V

    iput-object p3, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p3

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v0

    :goto_0
    instance-of v2, p3, Lf/b/a/b;

    if-nez v2, :cond_1

    move-object p3, v0

    :cond_1
    check-cast p3, Lf/b/a/b;

    if-eqz p3, :cond_2

    invoke-interface {p3}, Lf/b/a/b;->b()Lcom/lytefast/flexinput/utils/SelectionAggregator;

    move-result-object p3

    iget-object v2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-static {v2}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p3, v2}, Lcom/lytefast/flexinput/utils/SelectionAggregator;->registerSelectionCoordinator(Lcom/lytefast/flexinput/utils/SelectionCoordinator;)V

    :cond_2
    sget p3, Lcom/lytefast/flexinput/R$f;->fragment_recycler_view:I

    const/4 v2, 0x0

    invoke-virtual {p1, p3, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_9

    sget p2, Lcom/lytefast/flexinput/R$e;->list:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-interface {p2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    :cond_3
    new-instance p2, Landroid/graphics/Point;

    invoke-direct {p2}, Landroid/graphics/Point;-><init>()V

    if-eqz v0, :cond_4

    invoke-virtual {v0, p2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    :cond_4
    iget p2, p2, Landroid/graphics/Point;->x:I

    int-to-float p2, p2

    int-to-float p3, v1

    div-float/2addr p2, p3

    float-to-int p2, p2

    new-instance p3, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-direct {p3, v0, p2, p2}, Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;-><init>(Lcom/lytefast/flexinput/utils/SelectionCoordinator;II)V

    const-string p2, "android.permission.READ_EXTERNAL_STORAGE"

    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/lytefast/flexinput/fragment/PermissionsFragment;->hasPermissions([Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p2, :cond_5

    new-instance v0, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    :cond_5
    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p2, :cond_7

    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_1

    :cond_6
    iget-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p2, :cond_7

    new-instance v0, Lcom/lytefast/flexinput/fragment/PhotosFragment$a;

    invoke-direct {v0, p3, p0}, Lcom/lytefast/flexinput/fragment/PhotosFragment$a;-><init>(Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;Lcom/lytefast/flexinput/fragment/PhotosFragment;)V

    invoke-virtual {p0, v0}, Lcom/lytefast/flexinput/fragment/PhotosFragment;->newPermissionsRequestAdapter(Landroid/view/View$OnClickListener;)Lcom/lytefast/flexinput/adapters/EmptyListAdapter;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    :cond_7
    :goto_1
    sget p2, Lcom/lytefast/flexinput/R$e;->swipeRefreshLayout:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object p2, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    if-eqz p2, :cond_8

    new-instance v0, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;

    invoke-direct {v0, p1, p3, p0}, Lcom/lytefast/flexinput/fragment/PhotosFragment$b;-><init>(Landroid/view/View;Lcom/lytefast/flexinput/adapters/PhotoCursorAdapter;Lcom/lytefast/flexinput/fragment/PhotosFragment;)V

    invoke-virtual {p2, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$OnRefreshListener;)V

    :cond_8
    move-object v0, p1

    :cond_9
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->selectionCoordinator:Lcom/lytefast/flexinput/utils/SelectionCoordinator;

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/lytefast/flexinput/utils/SelectionCoordinator;->c:Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;

    invoke-interface {v0}, Lcom/lytefast/flexinput/utils/SelectionCoordinator$ItemSelectionListener;->unregister()V

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final setRecyclerView$flexinput_release(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public final setSwipeRefreshLayout$flexinput_release(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/lytefast/flexinput/fragment/PhotosFragment;->swipeRefreshLayout:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-void
.end method
