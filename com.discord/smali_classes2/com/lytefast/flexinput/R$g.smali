.class public final Lcom/lytefast/flexinput/R$g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lytefast/flexinput/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "g"
.end annotation


# static fields
.field public static final _continue:I = 0x7f120000

.field public static final _default:I = 0x7f120001

.field public static final _new:I = 0x7f120002

.field public static final a11y_role_checkbox:I = 0x7f120003

.field public static final a11y_role_radio_button:I = 0x7f120004

.field public static final a11y_role_switch:I = 0x7f120005

.field public static final abc_action_bar_home_description:I = 0x7f120006

.field public static final abc_action_bar_up_description:I = 0x7f120007

.field public static final abc_action_menu_overflow_description:I = 0x7f120008

.field public static final abc_action_mode_done:I = 0x7f120009

.field public static final abc_activity_chooser_view_see_all:I = 0x7f12000a

.field public static final abc_activitychooserview_choose_application:I = 0x7f12000b

.field public static final abc_capital_off:I = 0x7f12000c

.field public static final abc_capital_on:I = 0x7f12000d

.field public static final abc_menu_alt_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_delete_shortcut_label:I = 0x7f120010

.field public static final abc_menu_enter_shortcut_label:I = 0x7f120011

.field public static final abc_menu_function_shortcut_label:I = 0x7f120012

.field public static final abc_menu_meta_shortcut_label:I = 0x7f120013

.field public static final abc_menu_shift_shortcut_label:I = 0x7f120014

.field public static final abc_menu_space_shortcut_label:I = 0x7f120015

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120016

.field public static final abc_prepend_shortcut_label:I = 0x7f120017

.field public static final abc_search_hint:I = 0x7f120018

.field public static final abc_searchview_description_clear:I = 0x7f120019

.field public static final abc_searchview_description_query:I = 0x7f12001a

.field public static final abc_searchview_description_search:I = 0x7f12001b

.field public static final abc_searchview_description_submit:I = 0x7f12001c

.field public static final abc_searchview_description_voice:I = 0x7f12001d

.field public static final abc_shareactionprovider_share_with:I = 0x7f12001e

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f12001f

.field public static final abc_toolbar_collapse_description:I = 0x7f120020

.field public static final about_this_app:I = 0x7f120021

.field public static final accept_invite_modal_button:I = 0x7f120022

.field public static final accept_request_button_after:I = 0x7f120023

.field public static final accessibility:I = 0x7f120024

.field public static final accessibility_dark_sidebar:I = 0x7f120025

.field public static final accessibility_detection_modal_accept_label:I = 0x7f120026

.field public static final accessibility_detection_modal_body:I = 0x7f120027

.field public static final accessibility_detection_modal_decline_label:I = 0x7f120028

.field public static final accessibility_detection_modal_header:I = 0x7f120029

.field public static final accessibility_font_scaling_label:I = 0x7f12002a

.field public static final accessibility_font_scaling_use_app:I = 0x7f12002b

.field public static final accessibility_font_scaling_use_os:I = 0x7f12002c

.field public static final accessibility_message_group_spacing:I = 0x7f12002d

.field public static final accessibility_prefers_reduced_motion:I = 0x7f12002e

.field public static final accessibility_prefers_reduced_motion_auto:I = 0x7f12002f

.field public static final accessibility_prefers_reduced_motion_description:I = 0x7f120030

.field public static final accessibility_prefers_reduced_motion_enable:I = 0x7f120031

.field public static final accessibility_reduced_motion_settings_override:I = 0x7f120032

.field public static final accessibility_zoom_level_label:I = 0x7f120033

.field public static final account:I = 0x7f120034

.field public static final account_a11y_label:I = 0x7f120035

.field public static final account_click_to_copy:I = 0x7f120036

.field public static final account_disabled_description:I = 0x7f120037

.field public static final account_disabled_title:I = 0x7f120038

.field public static final account_management:I = 0x7f120039

.field public static final account_name:I = 0x7f12003a

.field public static final account_scheduled_for_deletion_cancel:I = 0x7f12003b

.field public static final account_scheduled_for_deletion_cancel_mobile:I = 0x7f12003c

.field public static final account_scheduled_for_deletion_description:I = 0x7f12003d

.field public static final account_scheduled_for_deletion_title:I = 0x7f12003e

.field public static final account_username_copy_success_1:I = 0x7f12003f

.field public static final account_username_copy_success_10:I = 0x7f120040

.field public static final account_username_copy_success_11:I = 0x7f120041

.field public static final account_username_copy_success_2:I = 0x7f120042

.field public static final account_username_copy_success_3:I = 0x7f120043

.field public static final account_username_copy_success_4:I = 0x7f120044

.field public static final account_username_copy_success_5:I = 0x7f120045

.field public static final account_username_copy_success_6:I = 0x7f120046

.field public static final account_username_copy_success_7:I = 0x7f120047

.field public static final account_username_copy_success_8:I = 0x7f120048

.field public static final account_username_copy_success_9:I = 0x7f120049

.field public static final acknowledgements:I = 0x7f12004a

.field public static final action_bar_scrolling_view_behavior:I = 0x7f12004b

.field public static final action_may_take_a_moment:I = 0x7f12004c

.field public static final actions:I = 0x7f12004d

.field public static final active_on_mobile:I = 0x7f12004e

.field public static final activity:I = 0x7f12004f

.field public static final activity_feed_card_gdpr_button_yes:I = 0x7f120050

.field public static final activity_feed_none_playing_body:I = 0x7f120051

.field public static final activity_feed_none_playing_header:I = 0x7f120052

.field public static final activity_feed_now_playing_action_go_to_server:I = 0x7f120053

.field public static final activity_feed_now_playing_action_join_channel:I = 0x7f120054

.field public static final activity_feed_now_playing_action_listen_along:I = 0x7f120055

.field public static final activity_feed_now_playing_action_play_on_spotify:I = 0x7f120056

.field public static final activity_feed_now_playing_header_two_known:I = 0x7f120057

.field public static final activity_feed_now_playing_header_two_known_only:I = 0x7f120058

.field public static final activity_feed_now_playing_in_a_voice_channel:I = 0x7f120059

.field public static final activity_feed_now_playing_multiple_games:I = 0x7f12005a

.field public static final activity_feed_now_playing_spotify:I = 0x7f12005b

.field public static final activity_feed_now_playing_watching:I = 0x7f12005c

.field public static final activity_feed_now_playing_xbox:I = 0x7f12005d

.field public static final activity_feed_now_streaming_twitch:I = 0x7f12005e

.field public static final activity_feed_other_member_list_header:I = 0x7f12005f

.field public static final activity_feed_popout_application_launching:I = 0x7f120060

.field public static final activity_feed_popout_application_running:I = 0x7f120061

.field public static final activity_feed_popout_desktop_app_required:I = 0x7f120062

.field public static final activity_feed_popout_not_friends_tooltip:I = 0x7f120063

.field public static final activity_feed_share_modal_search_placeholder:I = 0x7f120064

.field public static final activity_feed_single_member_list_header:I = 0x7f120065

.field public static final activity_feed_user_played_days_ago:I = 0x7f120066

.field public static final activity_feed_user_played_hours_ago:I = 0x7f120067

.field public static final activity_feed_user_played_minutes_ago:I = 0x7f120068

.field public static final activity_feed_user_played_seconds_ago:I = 0x7f120069

.field public static final activity_feed_user_playing_for_days:I = 0x7f12006a

.field public static final activity_feed_user_playing_for_hours:I = 0x7f12006b

.field public static final activity_feed_user_playing_for_minutes:I = 0x7f12006c

.field public static final activity_feed_user_playing_just_started:I = 0x7f12006d

.field public static final activity_invite_modal_header:I = 0x7f12006e

.field public static final activity_invite_modal_invite:I = 0x7f12006f

.field public static final activity_invite_modal_search_placeholder:I = 0x7f120070

.field public static final activity_invite_modal_sent:I = 0x7f120071

.field public static final activity_invite_private:I = 0x7f120072

.field public static final activity_panel_go_live:I = 0x7f120073

.field public static final activity_panel_go_live_change_screen:I = 0x7f120074

.field public static final activity_panel_go_live_stream_game:I = 0x7f120075

.field public static final activity_panel_go_live_tooltip_cant_stream_during_dm_call:I = 0x7f120076

.field public static final activity_panel_go_live_tooltip_no_permission_in_guild:I = 0x7f120077

.field public static final activity_panel_go_live_tooltip_no_permission_in_voice:I = 0x7f120078

.field public static final activity_panel_go_live_tooltip_not_in_guild:I = 0x7f120079

.field public static final activity_party_privacy:I = 0x7f12007a

.field public static final activity_party_privacy_friends:I = 0x7f12007b

.field public static final activity_party_privacy_friends_help:I = 0x7f12007c

.field public static final activity_party_privacy_voice_channel:I = 0x7f12007d

.field public static final activity_party_privacy_voice_channel_help:I = 0x7f12007e

.field public static final activity_report_game_body:I = 0x7f12007f

.field public static final activity_report_game_confusing:I = 0x7f120080

.field public static final activity_report_game_end_confusing:I = 0x7f120081

.field public static final activity_report_game_end_failed_load:I = 0x7f120082

.field public static final activity_report_game_end_lagging:I = 0x7f120083

.field public static final activity_report_game_end_not_fun:I = 0x7f120084

.field public static final activity_report_game_failed_load:I = 0x7f120085

.field public static final activity_report_game_lagging:I = 0x7f120086

.field public static final activity_report_game_not_fun:I = 0x7f120087

.field public static final activity_report_post_activity_header:I = 0x7f120088

.field public static final activity_report_post_activity_problem_title:I = 0x7f120089

.field public static final activity_reported_body:I = 0x7f12008a

.field public static final add:I = 0x7f12008b

.field public static final add_a_comment_optional:I = 0x7f12008c

.field public static final add_a_member:I = 0x7f12008d

.field public static final add_a_role:I = 0x7f12008e

.field public static final add_a_server:I = 0x7f12008f

.field public static final add_a_server_mobile:I = 0x7f120090

.field public static final add_by_id:I = 0x7f120091

.field public static final add_by_id_body:I = 0x7f120092

.field public static final add_by_id_title:I = 0x7f120093

.field public static final add_channel_or_category:I = 0x7f120094

.field public static final add_channel_to_override:I = 0x7f120095

.field public static final add_content:I = 0x7f120096

.field public static final add_email:I = 0x7f120097

.field public static final add_email_description:I = 0x7f120098

.field public static final add_email_short:I = 0x7f120099

.field public static final add_friend:I = 0x7f12009a

.field public static final add_friend_add_manually:I = 0x7f12009b

.field public static final add_friend_button:I = 0x7f12009c

.field public static final add_friend_button_after:I = 0x7f12009d

.field public static final add_friend_confirmation:I = 0x7f12009e

.field public static final add_friend_description:I = 0x7f12009f

.field public static final add_friend_error_discord_tag_username:I = 0x7f1200a0

.field public static final add_friend_error_invalid_discord_tag:I = 0x7f1200a1

.field public static final add_friend_error_numbers_only:I = 0x7f1200a2

.field public static final add_friend_error_other:I = 0x7f1200a3

.field public static final add_friend_error_too_many_friends:I = 0x7f1200a4

.field public static final add_friend_error_username_only:I = 0x7f1200a5

.field public static final add_friend_friend:I = 0x7f1200a6

.field public static final add_friend_input_hint:I = 0x7f1200a7

.field public static final add_friend_nearby:I = 0x7f1200a8

.field public static final add_friend_nearby_body:I = 0x7f1200a9

.field public static final add_friend_nearby_connection_error:I = 0x7f1200aa

.field public static final add_friend_nearby_connection_error_ios:I = 0x7f1200ab

.field public static final add_friend_nearby_disable_scanning:I = 0x7f1200ac

.field public static final add_friend_nearby_enable:I = 0x7f1200ad

.field public static final add_friend_nearby_enable_settings_android:I = 0x7f1200ae

.field public static final add_friend_nearby_found_body:I = 0x7f1200af

.field public static final add_friend_nearby_found_title:I = 0x7f1200b0

.field public static final add_friend_nearby_generic_error:I = 0x7f1200b1

.field public static final add_friend_nearby_info:I = 0x7f1200b2

.field public static final add_friend_nearby_learn_more:I = 0x7f1200b3

.field public static final add_friend_nearby_looking:I = 0x7f1200b4

.field public static final add_friend_nearby_stopped:I = 0x7f1200b5

.field public static final add_friend_nearby_title:I = 0x7f1200b6

.field public static final add_friend_placeholder:I = 0x7f1200b7

.field public static final add_friend_success:I = 0x7f1200b8

.field public static final add_keybind:I = 0x7f1200b9

.field public static final add_keybind_warning:I = 0x7f1200ba

.field public static final add_new_connection:I = 0x7f1200bb

.field public static final add_note:I = 0x7f1200bc

.field public static final add_override:I = 0x7f1200bd

.field public static final add_reaction:I = 0x7f1200be

.field public static final add_reaction_named:I = 0x7f1200bf

.field public static final add_reactions:I = 0x7f1200c0

.field public static final add_reactions_description:I = 0x7f1200c1

.field public static final add_role_a11y_label:I = 0x7f1200c2

.field public static final add_role_label:I = 0x7f1200c3

.field public static final add_role_placeholder:I = 0x7f1200c4

.field public static final add_role_subtitle:I = 0x7f1200c5

.field public static final add_role_title:I = 0x7f1200c6

.field public static final add_to_dictionary:I = 0x7f1200c7

.field public static final add_topic:I = 0x7f1200c8

.field public static final administrative:I = 0x7f1200c9

.field public static final administrator:I = 0x7f1200ca

.field public static final administrator_description:I = 0x7f1200cb

.field public static final advanced_settings:I = 0x7f1200cc

.field public static final advanced_voice_activity:I = 0x7f1200cd

.field public static final afk_settings:I = 0x7f1200ce

.field public static final age_gate_age_verified:I = 0x7f1200cf

.field public static final age_gate_age_verified_body:I = 0x7f1200d0

.field public static final age_gate_body:I = 0x7f1200d1

.field public static final age_gate_confirm_button:I = 0x7f1200d2

.field public static final age_gate_confirm_go_back:I = 0x7f1200d3

.field public static final age_gate_confirm_header:I = 0x7f1200d4

.field public static final age_gate_date_example:I = 0x7f1200d5

.field public static final age_gate_date_of_birth:I = 0x7f1200d6

.field public static final age_gate_dob_day:I = 0x7f1200d7

.field public static final age_gate_dob_month:I = 0x7f1200d8

.field public static final age_gate_dob_year:I = 0x7f1200d9

.field public static final age_gate_error_same_year:I = 0x7f1200da

.field public static final age_gate_existing_header:I = 0x7f1200db

.field public static final age_gate_failed_to_update_birthday:I = 0x7f1200dc

.field public static final age_gate_go_back:I = 0x7f1200dd

.field public static final age_gate_header:I = 0x7f1200de

.field public static final age_gate_invalid_birthday:I = 0x7f1200df

.field public static final age_gate_nsfw_body:I = 0x7f1200e0

.field public static final age_gate_nsfw_description:I = 0x7f1200e1

.field public static final age_gate_nsfw_underage_body:I = 0x7f1200e2

.field public static final age_gate_nsfw_underage_header:I = 0x7f1200e3

.field public static final age_gate_submit:I = 0x7f1200e4

.field public static final age_gate_underage_back_to_login:I = 0x7f1200e5

.field public static final age_gate_underage_body:I = 0x7f1200e6

.field public static final age_gate_underage_body_default_message:I = 0x7f1200e7

.field public static final age_gate_underage_body_post_register_message:I = 0x7f1200e8

.field public static final age_gate_underage_existing_body_deletion:I = 0x7f1200e9

.field public static final age_gate_underage_existing_body_deletion_with_days:I = 0x7f1200ea

.field public static final age_gate_underage_existing_header:I = 0x7f1200eb

.field public static final age_gate_underage_header:I = 0x7f1200ec

.field public static final age_gate_view_help_article:I = 0x7f1200ed

.field public static final age_gate_your_birthday:I = 0x7f1200ee

.field public static final aka:I = 0x7f1200ef

.field public static final all_servers:I = 0x7f1200f0

.field public static final allow_direct_messages:I = 0x7f1200f1

.field public static final allow_direct_messages_caption:I = 0x7f1200f2

.field public static final allow_game_detection_mobile:I = 0x7f1200f3

.field public static final allow_server_dms:I = 0x7f1200f4

.field public static final allow_tts_command:I = 0x7f1200f5

.field public static final already_have_account:I = 0x7f1200f6

.field public static final amount:I = 0x7f1200f7

.field public static final android_designate_other_channel:I = 0x7f1200f8

.field public static final android_unknown_view_holder:I = 0x7f1200f9

.field public static final android_welcome_message_subtitle_channel:I = 0x7f1200fa

.field public static final android_welcome_message_title_channel:I = 0x7f1200fb

.field public static final animate_emoji:I = 0x7f1200fc

.field public static final animate_emoji_note:I = 0x7f1200fd

.field public static final animated_emoji:I = 0x7f1200fe

.field public static final announcement_edit_rate_limit:I = 0x7f1200ff

.field public static final announcement_guild_description:I = 0x7f120100

.field public static final announcement_guild_here_to_help:I = 0x7f120101

.field public static final announcement_guild_popout_name:I = 0x7f120102

.field public static final answer:I = 0x7f120103

.field public static final app_information:I = 0x7f120104

.field public static final app_name:I = 0x7f120105

.field public static final app_not_opened:I = 0x7f120106

.field public static final app_opened_body:I = 0x7f120107

.field public static final app_opened_title:I = 0x7f120108

.field public static final app_opening:I = 0x7f120109

.field public static final app_permission_connect_desc:I = 0x7f12010a

.field public static final app_permission_connect_label:I = 0x7f12010b

.field public static final app_settings:I = 0x7f12010c

.field public static final appbar_scrolling_view_behavior:I = 0x7f12010d

.field public static final appearance:I = 0x7f12010e

.field public static final apple_billing_url:I = 0x7f12010f

.field public static final application_actions_menu_label:I = 0x7f120110

.field public static final application_addon_purchase_confirmation_blurb:I = 0x7f120111

.field public static final application_branch_name_master:I = 0x7f120112

.field public static final application_branch_name_unknown:I = 0x7f120113

.field public static final application_context_menu_application_id:I = 0x7f120114

.field public static final application_context_menu_branch_id:I = 0x7f120115

.field public static final application_context_menu_create_desktop_shortcut:I = 0x7f120116

.field public static final application_context_menu_hide:I = 0x7f120117

.field public static final application_context_menu_install:I = 0x7f120118

.field public static final application_context_menu_launch:I = 0x7f120119

.field public static final application_context_menu_launch_application_name:I = 0x7f12011a

.field public static final application_context_menu_launch_options:I = 0x7f12011b

.field public static final application_context_menu_private_status:I = 0x7f12011c

.field public static final application_context_menu_repair:I = 0x7f12011d

.field public static final application_context_menu_show:I = 0x7f12011e

.field public static final application_context_menu_show_in_folder:I = 0x7f12011f

.field public static final application_context_menu_sku_id:I = 0x7f120120

.field public static final application_context_menu_toggle_overlay_disable:I = 0x7f120121

.field public static final application_context_menu_uninstall:I = 0x7f120122

.field public static final application_entitlement_code_redemption_invalid:I = 0x7f120123

.field public static final application_entitlement_code_redemption_prompt:I = 0x7f120124

.field public static final application_entitlement_code_redemption_redeem:I = 0x7f120125

.field public static final application_iap_purchase_return_to_game:I = 0x7f120126

.field public static final application_installation_modal_directory_with_space:I = 0x7f120127

.field public static final application_installation_modal_location:I = 0x7f120128

.field public static final application_installation_modal_no_permission:I = 0x7f120129

.field public static final application_installation_modal_not_enough_space:I = 0x7f12012a

.field public static final application_installation_modal_select_directory:I = 0x7f12012b

.field public static final application_installation_modal_title:I = 0x7f12012c

.field public static final application_installation_space_used:I = 0x7f12012d

.field public static final application_library_empty_search_description:I = 0x7f12012e

.field public static final application_library_empty_state_description_no_import:I = 0x7f12012f

.field public static final application_library_empty_state_header:I = 0x7f120130

.field public static final application_library_filter_placeholder:I = 0x7f120131

.field public static final application_library_inventory:I = 0x7f120132

.field public static final application_library_my_games:I = 0x7f120133

.field public static final application_library_remove_confirm_body:I = 0x7f120134

.field public static final application_library_remove_confirm_confirm:I = 0x7f120135

.field public static final application_library_remove_confirm_header:I = 0x7f120136

.field public static final application_preorder_purchase_confirmation_button:I = 0x7f120137

.field public static final application_progress_indicator_installing:I = 0x7f120138

.field public static final application_progress_indicator_installing_hours:I = 0x7f120139

.field public static final application_progress_indicator_installing_minutes:I = 0x7f12013a

.field public static final application_progress_indicator_installing_seconds:I = 0x7f12013b

.field public static final application_progress_indicator_paused:I = 0x7f12013c

.field public static final application_progress_indicator_updating:I = 0x7f12013d

.field public static final application_progress_indicator_updating_hours:I = 0x7f12013e

.field public static final application_progress_indicator_updating_minutes:I = 0x7f12013f

.field public static final application_progress_indicator_updating_seconds:I = 0x7f120140

.field public static final application_store_about_header:I = 0x7f120141

.field public static final application_store_bundle_purchase_confirmation_blurb:I = 0x7f120142

.field public static final application_store_buy:I = 0x7f120143

.field public static final application_store_buy_as_gift:I = 0x7f120144

.field public static final application_store_buy_for_price:I = 0x7f120145

.field public static final application_store_buy_gift:I = 0x7f120146

.field public static final application_store_cloud_saves:I = 0x7f120147

.field public static final application_store_cloud_saves_tooltip:I = 0x7f120148

.field public static final application_store_coming_soon:I = 0x7f120149

.field public static final application_store_controller_support:I = 0x7f12014a

.field public static final application_store_controller_support_tooltip:I = 0x7f12014b

.field public static final application_store_countdown_days:I = 0x7f12014c

.field public static final application_store_countdown_hours:I = 0x7f12014d

.field public static final application_store_countdown_minutes:I = 0x7f12014e

.field public static final application_store_countdown_seconds:I = 0x7f12014f

.field public static final application_store_cross_platform:I = 0x7f120150

.field public static final application_store_cross_platform_tooltip:I = 0x7f120151

.field public static final application_store_description_read_less:I = 0x7f120152

.field public static final application_store_description_read_more:I = 0x7f120153

.field public static final application_store_details_developer:I = 0x7f120154

.field public static final application_store_details_genres:I = 0x7f120155

.field public static final application_store_details_publisher:I = 0x7f120156

.field public static final application_store_details_release_date:I = 0x7f120157

.field public static final application_store_discord_game_invites:I = 0x7f120158

.field public static final application_store_discord_game_invites_tooltip:I = 0x7f120159

.field public static final application_store_early_access:I = 0x7f12015a

.field public static final application_store_expand_downloadable_content:I = 0x7f12015b

.field public static final application_store_first_on_discord:I = 0x7f12015c

.field public static final application_store_free:I = 0x7f12015d

.field public static final application_store_free_premium_content:I = 0x7f12015e

.field public static final application_store_free_with_premium:I = 0x7f12015f

.field public static final application_store_genre_action:I = 0x7f120160

.field public static final application_store_genre_action_adventure:I = 0x7f120161

.field public static final application_store_genre_action_rpg:I = 0x7f120162

.field public static final application_store_genre_adventure:I = 0x7f120163

.field public static final application_store_genre_artillery:I = 0x7f120164

.field public static final application_store_genre_baseball:I = 0x7f120165

.field public static final application_store_genre_basketball:I = 0x7f120166

.field public static final application_store_genre_billiards:I = 0x7f120167

.field public static final application_store_genre_bowling:I = 0x7f120168

.field public static final application_store_genre_boxing:I = 0x7f120169

.field public static final application_store_genre_brawler:I = 0x7f12016a

.field public static final application_store_genre_card_game:I = 0x7f12016b

.field public static final application_store_genre_driving_racing:I = 0x7f12016c

.field public static final application_store_genre_dual_joystick_shooter:I = 0x7f12016d

.field public static final application_store_genre_dungeon_crawler:I = 0x7f12016e

.field public static final application_store_genre_education:I = 0x7f12016f

.field public static final application_store_genre_fighting:I = 0x7f120170

.field public static final application_store_genre_fishing:I = 0x7f120171

.field public static final application_store_genre_fitness:I = 0x7f120172

.field public static final application_store_genre_flight_simulator:I = 0x7f120173

.field public static final application_store_genre_football:I = 0x7f120174

.field public static final application_store_genre_four_x:I = 0x7f120175

.field public static final application_store_genre_fps:I = 0x7f120176

.field public static final application_store_genre_gambling:I = 0x7f120177

.field public static final application_store_genre_golf:I = 0x7f120178

.field public static final application_store_genre_hack_and_slash:I = 0x7f120179

.field public static final application_store_genre_hockey:I = 0x7f12017a

.field public static final application_store_genre_life_simulator:I = 0x7f12017b

.field public static final application_store_genre_light_gun:I = 0x7f12017c

.field public static final application_store_genre_massively_multiplayer:I = 0x7f12017d

.field public static final application_store_genre_metroidvania:I = 0x7f12017e

.field public static final application_store_genre_miscellaneous:I = 0x7f12017f

.field public static final application_store_genre_mmorpg:I = 0x7f120180

.field public static final application_store_genre_moba:I = 0x7f120181

.field public static final application_store_genre_music_rhythm:I = 0x7f120182

.field public static final application_store_genre_open_world:I = 0x7f120183

.field public static final application_store_genre_party_mini_game:I = 0x7f120184

.field public static final application_store_genre_pinball:I = 0x7f120185

.field public static final application_store_genre_platformer:I = 0x7f120186

.field public static final application_store_genre_psychological_horror:I = 0x7f120187

.field public static final application_store_genre_puzzle:I = 0x7f120188

.field public static final application_store_genre_roguelike:I = 0x7f120189

.field public static final application_store_genre_role_playing:I = 0x7f12018a

.field public static final application_store_genre_rts:I = 0x7f12018b

.field public static final application_store_genre_sandbox:I = 0x7f12018c

.field public static final application_store_genre_shoot_em_up:I = 0x7f12018d

.field public static final application_store_genre_shooter:I = 0x7f12018e

.field public static final application_store_genre_simulation:I = 0x7f12018f

.field public static final application_store_genre_skateboarding_skating:I = 0x7f120190

.field public static final application_store_genre_snowboarding_skiing:I = 0x7f120191

.field public static final application_store_genre_soccer:I = 0x7f120192

.field public static final application_store_genre_sports:I = 0x7f120193

.field public static final application_store_genre_stealth:I = 0x7f120194

.field public static final application_store_genre_strategy:I = 0x7f120195

.field public static final application_store_genre_surfing_wakeboarding:I = 0x7f120196

.field public static final application_store_genre_survival:I = 0x7f120197

.field public static final application_store_genre_survival_horror:I = 0x7f120198

.field public static final application_store_genre_tower_defense:I = 0x7f120199

.field public static final application_store_genre_track_field:I = 0x7f12019a

.field public static final application_store_genre_train_simulator:I = 0x7f12019b

.field public static final application_store_genre_trivia_board_game:I = 0x7f12019c

.field public static final application_store_genre_turn_based_strategy:I = 0x7f12019d

.field public static final application_store_genre_vehicular_combat:I = 0x7f12019e

.field public static final application_store_genre_visual_novel:I = 0x7f12019f

.field public static final application_store_genre_wargame:I = 0x7f1201a0

.field public static final application_store_genre_wrestling:I = 0x7f1201a1

.field public static final application_store_get_premium:I = 0x7f1201a2

.field public static final application_store_gift_purchase_confirm_monthly_mobile:I = 0x7f1201a3

.field public static final application_store_gift_purchase_confirm_subscription_monthly:I = 0x7f1201a4

.field public static final application_store_gift_purchase_confirm_subscription_yearly:I = 0x7f1201a5

.field public static final application_store_gift_purchase_confirm_yearly_mobile:I = 0x7f1201a6

.field public static final application_store_gift_purchase_confirmation_blurb:I = 0x7f1201a7

.field public static final application_store_gift_purchase_confirmation_subtext:I = 0x7f1201a8

.field public static final application_store_in_library:I = 0x7f1201a9

.field public static final application_store_link_copied:I = 0x7f1201aa

.field public static final application_store_listing_purchase_generic_error:I = 0x7f1201ab

.field public static final application_store_listing_purchase_generic_error_short:I = 0x7f1201ac

.field public static final application_store_listing_purchase_rate_limit_error:I = 0x7f1201ad

.field public static final application_store_listing_purchase_rate_limit_error_short:I = 0x7f1201ae

.field public static final application_store_listing_select_edition:I = 0x7f1201af

.field public static final application_store_local_coop:I = 0x7f1201b0

.field public static final application_store_local_coop_tooltip:I = 0x7f1201b1

.field public static final application_store_local_multiplayer:I = 0x7f1201b2

.field public static final application_store_local_multiplayer_tooltip:I = 0x7f1201b3

.field public static final application_store_navigation_browse_premium:I = 0x7f1201b4

.field public static final application_store_new_release:I = 0x7f1201b5

.field public static final application_store_online_coop:I = 0x7f1201b6

.field public static final application_store_online_coop_tooltip:I = 0x7f1201b7

.field public static final application_store_online_multiplayer:I = 0x7f1201b8

.field public static final application_store_online_multiplayer_tooltip:I = 0x7f1201b9

.field public static final application_store_preorder:I = 0x7f1201ba

.field public static final application_store_preorder_as_gift:I = 0x7f1201bb

.field public static final application_store_preorder_for_price:I = 0x7f1201bc

.field public static final application_store_preorder_purchase_confirmation_blurb:I = 0x7f1201bd

.field public static final application_store_purchase_application:I = 0x7f1201be

.field public static final application_store_purchase_available_date:I = 0x7f1201bf

.field public static final application_store_purchase_bundle:I = 0x7f1201c0

.field public static final application_store_purchase_confirmation_blurb:I = 0x7f1201c1

.field public static final application_store_purchase_confirmation_title_1:I = 0x7f1201c2

.field public static final application_store_purchase_confirmation_title_2:I = 0x7f1201c3

.field public static final application_store_purchase_confirmation_title_3:I = 0x7f1201c4

.field public static final application_store_purchase_confirmation_title_4:I = 0x7f1201c5

.field public static final application_store_purchase_confirmation_unsupported_os:I = 0x7f1201c6

.field public static final application_store_purchase_consumable:I = 0x7f1201c7

.field public static final application_store_purchase_dlc:I = 0x7f1201c8

.field public static final application_store_purchase_game_unsupported_os:I = 0x7f1201c9

.field public static final application_store_purchase_gift_confirmation_title:I = 0x7f1201ca

.field public static final application_store_purchase_gift_only:I = 0x7f1201cb

.field public static final application_store_purchase_gift_only_ungiftable:I = 0x7f1201cc

.field public static final application_store_purchase_header_bundles_different:I = 0x7f1201cd

.field public static final application_store_purchase_header_bundles_same:I = 0x7f1201ce

.field public static final application_store_purchase_header_consumables_different:I = 0x7f1201cf

.field public static final application_store_purchase_header_consumables_same:I = 0x7f1201d0

.field public static final application_store_purchase_header_distribution_application:I = 0x7f1201d1

.field public static final application_store_purchase_header_distribution_bundle:I = 0x7f1201d2

.field public static final application_store_purchase_header_distribution_consumable:I = 0x7f1201d3

.field public static final application_store_purchase_header_distribution_dlc:I = 0x7f1201d4

.field public static final application_store_purchase_header_dlc_different:I = 0x7f1201d5

.field public static final application_store_purchase_header_dlc_same:I = 0x7f1201d6

.field public static final application_store_purchase_header_first_on:I = 0x7f1201d7

.field public static final application_store_purchase_header_free_application:I = 0x7f1201d8

.field public static final application_store_purchase_header_free_bundle:I = 0x7f1201d9

.field public static final application_store_purchase_header_free_consumable:I = 0x7f1201da

.field public static final application_store_purchase_header_free_dlc:I = 0x7f1201db

.field public static final application_store_purchase_header_games_different:I = 0x7f1201dc

.field public static final application_store_purchase_header_games_same:I = 0x7f1201dd

.field public static final application_store_purchase_header_preorder_application:I = 0x7f1201de

.field public static final application_store_purchase_header_preorder_bundle:I = 0x7f1201df

.field public static final application_store_purchase_header_preorder_consumable:I = 0x7f1201e0

.field public static final application_store_purchase_header_preorder_dlc:I = 0x7f1201e1

.field public static final application_store_purchase_header_preorder_entitled:I = 0x7f1201e2

.field public static final application_store_purchase_header_time_left_until_release:I = 0x7f1201e3

.field public static final application_store_purchase_iap:I = 0x7f1201e4

.field public static final application_store_purchase_in_library:I = 0x7f1201e5

.field public static final application_store_purchase_in_library_hidden:I = 0x7f1201e6

.field public static final application_store_purchase_install_game:I = 0x7f1201e7

.field public static final application_store_purchase_test_mode:I = 0x7f1201e8

.field public static final application_store_purchase_warning_preorder:I = 0x7f1201e9

.field public static final application_store_pvp:I = 0x7f1201ea

.field public static final application_store_pvp_tooltip:I = 0x7f1201eb

.field public static final application_store_rating_descriptors_esrb_alcohol_reference:I = 0x7f1201ec

.field public static final application_store_rating_descriptors_esrb_animated_blood:I = 0x7f1201ed

.field public static final application_store_rating_descriptors_esrb_animated_violence:I = 0x7f1201ee

.field public static final application_store_rating_descriptors_esrb_blood:I = 0x7f1201ef

.field public static final application_store_rating_descriptors_esrb_blood_and_gore:I = 0x7f1201f0

.field public static final application_store_rating_descriptors_esrb_cartoon_violence:I = 0x7f1201f1

.field public static final application_store_rating_descriptors_esrb_comic_mischief:I = 0x7f1201f2

.field public static final application_store_rating_descriptors_esrb_crude_humor:I = 0x7f1201f3

.field public static final application_store_rating_descriptors_esrb_drug_reference:I = 0x7f1201f4

.field public static final application_store_rating_descriptors_esrb_fantasy_violence:I = 0x7f1201f5

.field public static final application_store_rating_descriptors_esrb_in_game_purchases:I = 0x7f1201f6

.field public static final application_store_rating_descriptors_esrb_intense_violence:I = 0x7f1201f7

.field public static final application_store_rating_descriptors_esrb_language:I = 0x7f1201f8

.field public static final application_store_rating_descriptors_esrb_lyrics:I = 0x7f1201f9

.field public static final application_store_rating_descriptors_esrb_mature_humor:I = 0x7f1201fa

.field public static final application_store_rating_descriptors_esrb_mild_blood:I = 0x7f1201fb

.field public static final application_store_rating_descriptors_esrb_mild_cartoon_violence:I = 0x7f1201fc

.field public static final application_store_rating_descriptors_esrb_mild_fantasy_violence:I = 0x7f1201fd

.field public static final application_store_rating_descriptors_esrb_mild_language:I = 0x7f1201fe

.field public static final application_store_rating_descriptors_esrb_mild_lyrics:I = 0x7f1201ff

.field public static final application_store_rating_descriptors_esrb_mild_sexual_themes:I = 0x7f120200

.field public static final application_store_rating_descriptors_esrb_mild_suggestive_themes:I = 0x7f120201

.field public static final application_store_rating_descriptors_esrb_mild_violence:I = 0x7f120202

.field public static final application_store_rating_descriptors_esrb_nudity:I = 0x7f120203

.field public static final application_store_rating_descriptors_esrb_partual_nudity:I = 0x7f120204

.field public static final application_store_rating_descriptors_esrb_real_gambling:I = 0x7f120205

.field public static final application_store_rating_descriptors_esrb_sexual_content:I = 0x7f120206

.field public static final application_store_rating_descriptors_esrb_sexual_themes:I = 0x7f120207

.field public static final application_store_rating_descriptors_esrb_sexual_violence:I = 0x7f120208

.field public static final application_store_rating_descriptors_esrb_shares_location:I = 0x7f120209

.field public static final application_store_rating_descriptors_esrb_simulated_gambling:I = 0x7f12020a

.field public static final application_store_rating_descriptors_esrb_strong_language:I = 0x7f12020b

.field public static final application_store_rating_descriptors_esrb_strong_lyrics:I = 0x7f12020c

.field public static final application_store_rating_descriptors_esrb_strong_sexual_content:I = 0x7f12020d

.field public static final application_store_rating_descriptors_esrb_suggestive_themes:I = 0x7f12020e

.field public static final application_store_rating_descriptors_esrb_tobacco_reference:I = 0x7f12020f

.field public static final application_store_rating_descriptors_esrb_unrestricted_internet:I = 0x7f120210

.field public static final application_store_rating_descriptors_esrb_use_of_alcohol:I = 0x7f120211

.field public static final application_store_rating_descriptors_esrb_use_of_drugs:I = 0x7f120212

.field public static final application_store_rating_descriptors_esrb_use_of_tobacco:I = 0x7f120213

.field public static final application_store_rating_descriptors_esrb_users_interact:I = 0x7f120214

.field public static final application_store_rating_descriptors_esrb_violence:I = 0x7f120215

.field public static final application_store_rating_descriptors_esrb_violent_references:I = 0x7f120216

.field public static final application_store_rating_descriptors_pegi_bad_language:I = 0x7f120217

.field public static final application_store_rating_descriptors_pegi_discrimination:I = 0x7f120218

.field public static final application_store_rating_descriptors_pegi_drugs:I = 0x7f120219

.field public static final application_store_rating_descriptors_pegi_fear:I = 0x7f12021a

.field public static final application_store_rating_descriptors_pegi_gambling:I = 0x7f12021b

.field public static final application_store_rating_descriptors_pegi_sex:I = 0x7f12021c

.field public static final application_store_rating_descriptors_pegi_violence:I = 0x7f12021d

.field public static final application_store_recommendation_ever_played_double:I = 0x7f12021e

.field public static final application_store_recommendation_ever_played_other:I = 0x7f12021f

.field public static final application_store_recommendation_ever_played_single:I = 0x7f120220

.field public static final application_store_recommendation_now_playing_double:I = 0x7f120221

.field public static final application_store_recommendation_now_playing_other:I = 0x7f120222

.field public static final application_store_recommendation_now_playing_single:I = 0x7f120223

.field public static final application_store_recommendation_recently_played_double:I = 0x7f120224

.field public static final application_store_recommendation_recently_played_other:I = 0x7f120225

.field public static final application_store_recommendation_recently_played_single:I = 0x7f120226

.field public static final application_store_restricted:I = 0x7f120227

.field public static final application_store_rich_presence:I = 0x7f120228

.field public static final application_store_rich_presence_tooltip:I = 0x7f120229

.field public static final application_store_search_empty:I = 0x7f12022a

.field public static final application_store_section_title_copyright:I = 0x7f12022b

.field public static final application_store_section_title_details:I = 0x7f12022c

.field public static final application_store_section_title_features:I = 0x7f12022d

.field public static final application_store_section_title_ratings:I = 0x7f12022e

.field public static final application_store_section_title_recommendation:I = 0x7f12022f

.field public static final application_store_section_title_system_requirements:I = 0x7f120230

.field public static final application_store_section_title_verified_guild:I = 0x7f120231

.field public static final application_store_secure_networking:I = 0x7f120232

.field public static final application_store_secure_networking_tooltip:I = 0x7f120233

.field public static final application_store_single_player:I = 0x7f120234

.field public static final application_store_single_player_tooltip:I = 0x7f120235

.field public static final application_store_specs_cpu:I = 0x7f120236

.field public static final application_store_specs_memory:I = 0x7f120237

.field public static final application_store_specs_memory_value:I = 0x7f120238

.field public static final application_store_specs_minimum:I = 0x7f120239

.field public static final application_store_specs_network:I = 0x7f12023a

.field public static final application_store_specs_notes:I = 0x7f12023b

.field public static final application_store_specs_os:I = 0x7f12023c

.field public static final application_store_specs_recommended:I = 0x7f12023d

.field public static final application_store_specs_sound:I = 0x7f12023e

.field public static final application_store_specs_storage:I = 0x7f12023f

.field public static final application_store_specs_video:I = 0x7f120240

.field public static final application_store_spectator_mode:I = 0x7f120241

.field public static final application_store_spectator_mode_tooltip:I = 0x7f120242

.field public static final application_store_staff_pick:I = 0x7f120243

.field public static final application_store_the_game_awards_winner:I = 0x7f120244

.field public static final application_store_warning_dlc_requires_base_application_description:I = 0x7f120245

.field public static final application_store_warning_dlc_requires_base_application_title:I = 0x7f120246

.field public static final application_store_warning_early_access_description:I = 0x7f120247

.field public static final application_store_warning_early_access_title:I = 0x7f120248

.field public static final application_store_warning_requires_desktop_app_description:I = 0x7f120249

.field public static final application_store_warning_requires_desktop_app_title:I = 0x7f12024a

.field public static final application_store_warning_restricted_in_region_description:I = 0x7f12024b

.field public static final application_store_warning_restricted_in_region_title:I = 0x7f12024c

.field public static final application_store_warning_unavailable_in_language_description:I = 0x7f12024d

.field public static final application_store_warning_unavailable_in_language_title:I = 0x7f12024e

.field public static final application_store_warning_unavailable_linux_description:I = 0x7f12024f

.field public static final application_store_warning_unavailable_linux_title:I = 0x7f120250

.field public static final application_store_warning_unavailable_mac_os_title:I = 0x7f120251

.field public static final application_store_warning_unavailable_os_description:I = 0x7f120252

.field public static final application_store_warning_unavailable_windows_title:I = 0x7f120253

.field public static final application_test_mode_view_other_listings:I = 0x7f120254

.field public static final application_uninstall_prompt_body:I = 0x7f120255

.field public static final application_uninstall_prompt_cancel:I = 0x7f120256

.field public static final application_uninstall_prompt_confirm:I = 0x7f120257

.field public static final application_uninstall_prompt_title:I = 0x7f120258

.field public static final applications_and_connections:I = 0x7f120259

.field public static final applications_and_connections_body:I = 0x7f12025a

.field public static final attach_files:I = 0x7f12025b

.field public static final attach_payment_source_optional_with_entitlements_warning:I = 0x7f12025c

.field public static final attach_payment_source_prompt_option:I = 0x7f12025d

.field public static final attach_payment_source_prompt_option_optional:I = 0x7f12025e

.field public static final attachment_compressing:I = 0x7f12025f

.field public static final attachment_filename_unknown:I = 0x7f120260

.field public static final attachment_files:I = 0x7f120261

.field public static final attachment_photos:I = 0x7f120262

.field public static final attachment_processing:I = 0x7f120263

.field public static final attenuate_while_speaking_others:I = 0x7f120264

.field public static final attenuate_while_speaking_self:I = 0x7f120265

.field public static final audio_device_actions:I = 0x7f120266

.field public static final audio_devices_bluetooth:I = 0x7f120267

.field public static final audio_devices_change_output:I = 0x7f120268

.field public static final audio_devices_earpiece:I = 0x7f120269

.field public static final audio_devices_output_selection_prompt:I = 0x7f12026a

.field public static final audio_devices_speaker:I = 0x7f12026b

.field public static final audio_devices_toggle_unavailable:I = 0x7f12026c

.field public static final audio_devices_unknown:I = 0x7f12026d

.field public static final audio_devices_wired:I = 0x7f12026e

.field public static final auth_banned_invite_body:I = 0x7f12026f

.field public static final auth_browser_handoff_detecting_description:I = 0x7f120270

.field public static final auth_disable_email_notifications_failure_body:I = 0x7f120271

.field public static final auth_disable_email_notifications_failure_header:I = 0x7f120272

.field public static final auth_disable_email_notifications_success_body:I = 0x7f120273

.field public static final auth_disable_email_notifications_success_header:I = 0x7f120274

.field public static final auth_expired_suggestion:I = 0x7f120275

.field public static final auth_invalid_invite_body:I = 0x7f120276

.field public static final auth_invalid_invite_tip:I = 0x7f120277

.field public static final auth_invalid_invite_title:I = 0x7f120278

.field public static final auth_ip_auth_succeeded_suggestion:I = 0x7f120279

.field public static final auth_login_body:I = 0x7f12027a

.field public static final auth_message_invited_by:I = 0x7f12027b

.field public static final auth_message_invited_to_play:I = 0x7f12027c

.field public static final auth_message_invited_to_play_username:I = 0x7f12027d

.field public static final auth_message_invited_to_stream:I = 0x7f12027e

.field public static final auth_username_tooltip:I = 0x7f12027f

.field public static final auth_verfication_expired_suggestion:I = 0x7f120280

.field public static final auth_view_password:I = 0x7f120281

.field public static final authorization:I = 0x7f120282

.field public static final authorization_expired:I = 0x7f120283

.field public static final authorize:I = 0x7f120284

.field public static final authorized:I = 0x7f120285

.field public static final authorized_apps:I = 0x7f120286

.field public static final authorizing:I = 0x7f120287

.field public static final auto_toggle_streamer_mode_description:I = 0x7f120288

.field public static final auto_toggle_streamer_mode_label:I = 0x7f120289

.field public static final autocomplete_no_results_body:I = 0x7f12028a

.field public static final autocomplete_no_results_header:I = 0x7f12028b

.field public static final automatic_gain_control:I = 0x7f12028c

.field public static final avatar_convert_failure_mobile:I = 0x7f12028d

.field public static final avatar_size_option_large:I = 0x7f12028e

.field public static final avatar_size_option_small:I = 0x7f12028f

.field public static final avatar_step_subtitle:I = 0x7f120290

.field public static final avatar_step_title:I = 0x7f120291

.field public static final avatar_upload_apply:I = 0x7f120292

.field public static final avatar_upload_cancel:I = 0x7f120293

.field public static final avatar_upload_edit_media:I = 0x7f120294

.field public static final b_plus_a_survey_button:I = 0x7f120295

.field public static final b_plus_a_survey_prompt:I = 0x7f120296

.field public static final back:I = 0x7f120297

.field public static final back_button_behavior_label_mobile:I = 0x7f120298

.field public static final back_button_behavior_mobile:I = 0x7f120299

.field public static final back_to_login:I = 0x7f12029a

.field public static final backspace:I = 0x7f12029b

.field public static final backup_codes_dash:I = 0x7f12029c

.field public static final ban:I = 0x7f12029d

.field public static final ban_confirm_title:I = 0x7f12029e

.field public static final ban_members:I = 0x7f12029f

.field public static final ban_reason:I = 0x7f1202a0

.field public static final ban_user:I = 0x7f1202a1

.field public static final ban_user_body:I = 0x7f1202a2

.field public static final ban_user_confirmed:I = 0x7f1202a3

.field public static final ban_user_error_generic:I = 0x7f1202a4

.field public static final ban_user_title:I = 0x7f1202a5

.field public static final bans:I = 0x7f1202a6

.field public static final bans_header:I = 0x7f1202a7

.field public static final bans_hint:I = 0x7f1202a8

.field public static final bans_no_results:I = 0x7f1202a9

.field public static final bans_no_users_banned:I = 0x7f1202aa

.field public static final bans_search_placeholder:I = 0x7f1202ab

.field public static final beep_boop:I = 0x7f1202ac

.field public static final beginning_channel:I = 0x7f1202ad

.field public static final beginning_channel_description:I = 0x7f1202ae

.field public static final beginning_channel_no_history:I = 0x7f1202af

.field public static final beginning_channel_welcome:I = 0x7f1202b0

.field public static final beginning_chat:I = 0x7f1202b1

.field public static final beginning_chat_dm_mobile:I = 0x7f1202b2

.field public static final beginning_chat_nickname_mobile:I = 0x7f1202b3

.field public static final beginning_dm:I = 0x7f1202b4

.field public static final beginning_group_dm:I = 0x7f1202b5

.field public static final beginning_group_dm_managed:I = 0x7f1202b6

.field public static final beginning_role_required_channel_description:I = 0x7f1202b7

.field public static final beta:I = 0x7f1202b8

.field public static final bg:I = 0x7f1202b9

.field public static final billing:I = 0x7f1202ba

.field public static final billing_accept_terms_tooltip:I = 0x7f1202bb

.field public static final billing_account_credit:I = 0x7f1202bc

.field public static final billing_account_credit_description:I = 0x7f1202bd

.field public static final billing_account_credit_description_ios_disclaimer:I = 0x7f1202be

.field public static final billing_add_payment_method:I = 0x7f1202bf

.field public static final billing_address:I = 0x7f1202c0

.field public static final billing_address_address:I = 0x7f1202c1

.field public static final billing_address_address2:I = 0x7f1202c2

.field public static final billing_address_address2_placeholder:I = 0x7f1202c3

.field public static final billing_address_address_error_required:I = 0x7f1202c4

.field public static final billing_address_address_placeholder:I = 0x7f1202c5

.field public static final billing_address_city:I = 0x7f1202c6

.field public static final billing_address_city_error_required:I = 0x7f1202c7

.field public static final billing_address_city_placeholder:I = 0x7f1202c8

.field public static final billing_address_country:I = 0x7f1202c9

.field public static final billing_address_country_error_required:I = 0x7f1202ca

.field public static final billing_address_name:I = 0x7f1202cb

.field public static final billing_address_name_error_required:I = 0x7f1202cc

.field public static final billing_address_postal_code:I = 0x7f1202cd

.field public static final billing_address_postal_code_canada_placeholder:I = 0x7f1202ce

.field public static final billing_address_postal_code_error_required:I = 0x7f1202cf

.field public static final billing_address_province:I = 0x7f1202d0

.field public static final billing_address_province_error_required:I = 0x7f1202d1

.field public static final billing_address_region:I = 0x7f1202d2

.field public static final billing_address_state:I = 0x7f1202d3

.field public static final billing_address_state_error_required:I = 0x7f1202d4

.field public static final billing_address_zip_code:I = 0x7f1202d5

.field public static final billing_address_zip_code_error_length:I = 0x7f1202d6

.field public static final billing_address_zip_code_error_required:I = 0x7f1202d7

.field public static final billing_address_zip_code_invalid:I = 0x7f1202d8

.field public static final billing_address_zip_code_placeholder:I = 0x7f1202d9

.field public static final billing_apple_description:I = 0x7f1202da

.field public static final billing_apple_header:I = 0x7f1202db

.field public static final billing_apple_manage_elsewhere:I = 0x7f1202dc

.field public static final billing_application_consumable_refund_text_unable:I = 0x7f1202dd

.field public static final billing_application_refund_text:I = 0x7f1202de

.field public static final billing_application_refund_text_unable:I = 0x7f1202df

.field public static final billing_applies_to_all_subscriptions:I = 0x7f1202e0

.field public static final billing_code_redemption_redirect:I = 0x7f1202e1

.field public static final billing_error_add_payment_source_streamer_mode:I = 0x7f1202e2

.field public static final billing_error_gateway:I = 0x7f1202e3

.field public static final billing_error_generic:I = 0x7f1202e4

.field public static final billing_error_negative_invoice_amount:I = 0x7f1202e5

.field public static final billing_error_purchase:I = 0x7f1202e6

.field public static final billing_error_purchase_details_not_found:I = 0x7f1202e7

.field public static final billing_error_rate_limit:I = 0x7f1202e8

.field public static final billing_error_section_address:I = 0x7f1202e9

.field public static final billing_error_section_card:I = 0x7f1202ea

.field public static final billing_error_unknown_payment_source:I = 0x7f1202eb

.field public static final billing_external_description:I = 0x7f1202ec

.field public static final billing_external_header:I = 0x7f1202ed

.field public static final billing_external_manage_elsewhere:I = 0x7f1202ee

.field public static final billing_gift_copied:I = 0x7f1202ef

.field public static final billing_gift_link:I = 0x7f1202f0

.field public static final billing_gift_purchase_tooltip:I = 0x7f1202f1

.field public static final billing_gift_refund_text:I = 0x7f1202f2

.field public static final billing_gift_refund_text_unable:I = 0x7f1202f3

.field public static final billing_history:I = 0x7f1202f4

.field public static final billing_invoice_gift_plan:I = 0x7f1202f5

.field public static final billing_invoice_subscription_credit_applied:I = 0x7f1202f6

.field public static final billing_invoice_tax:I = 0x7f1202f7

.field public static final billing_invoice_today_total:I = 0x7f1202f8

.field public static final billing_invoice_today_total_tax_inclusive:I = 0x7f1202f9

.field public static final billing_is_gift_purchase:I = 0x7f1202fa

.field public static final billing_legal_mumbo_jumbo:I = 0x7f1202fb

.field public static final billing_legal_mumbo_jumbo_label:I = 0x7f1202fc

.field public static final billing_legal_mumbo_jumbo_trial_label:I = 0x7f1202fd

.field public static final billing_manage_billing:I = 0x7f1202fe

.field public static final billing_manage_on_google_play:I = 0x7f1202ff

.field public static final billing_manage_subscription:I = 0x7f120300

.field public static final billing_managed_by_apple:I = 0x7f120301

.field public static final billing_managed_by_payment_gateway:I = 0x7f120302

.field public static final billing_no_payment_method:I = 0x7f120303

.field public static final billing_no_payment_method_description:I = 0x7f120304

.field public static final billing_pay_for_it_with:I = 0x7f120305

.field public static final billing_payment_breakdown_taxes:I = 0x7f120306

.field public static final billing_payment_breakdown_total:I = 0x7f120307

.field public static final billing_payment_history:I = 0x7f120308

.field public static final billing_payment_premium:I = 0x7f120309

.field public static final billing_payment_premium_description:I = 0x7f12030a

.field public static final billing_payment_premium_legalese_monthly:I = 0x7f12030b

.field public static final billing_payment_premium_legalese_yearly:I = 0x7f12030c

.field public static final billing_payment_source_invalid:I = 0x7f12030d

.field public static final billing_payment_sources:I = 0x7f12030e

.field public static final billing_payment_table_header_amount:I = 0x7f12030f

.field public static final billing_payment_table_header_date:I = 0x7f120310

.field public static final billing_payment_table_header_description:I = 0x7f120311

.field public static final billing_plan_selection_discount:I = 0x7f120312

.field public static final billing_post_purchase_join_guild_for_emoji:I = 0x7f120313

.field public static final billing_post_purchase_join_guild_for_emoji_cta:I = 0x7f120314

.field public static final billing_premium_and_premium_guild_plan_activated:I = 0x7f120315

.field public static final billing_premium_and_premium_guild_plans:I = 0x7f120316

.field public static final billing_premium_gift_month_mobile:I = 0x7f120317

.field public static final billing_premium_gift_year_mobile:I = 0x7f120318

.field public static final billing_premium_guild_plans:I = 0x7f120319

.field public static final billing_premium_plans:I = 0x7f12031a

.field public static final billing_premium_refund_text:I = 0x7f12031b

.field public static final billing_premium_refund_text_unable:I = 0x7f12031c

.field public static final billing_preorder_refund_text:I = 0x7f12031d

.field public static final billing_price_per_month:I = 0x7f12031e

.field public static final billing_price_per_month_bold:I = 0x7f12031f

.field public static final billing_price_per_month_current_plan:I = 0x7f120320

.field public static final billing_price_per_month_current_plan_mobile:I = 0x7f120321

.field public static final billing_price_per_month_each:I = 0x7f120322

.field public static final billing_price_per_month_mobile:I = 0x7f120323

.field public static final billing_price_per_year:I = 0x7f120324

.field public static final billing_price_per_year_bold:I = 0x7f120325

.field public static final billing_price_per_year_current_plan:I = 0x7f120326

.field public static final billing_price_per_year_current_plan_mobile:I = 0x7f120327

.field public static final billing_price_per_year_each:I = 0x7f120328

.field public static final billing_price_per_year_mobile:I = 0x7f120329

.field public static final billing_price_per_year_months_free:I = 0x7f12032a

.field public static final billing_purchase_details_header:I = 0x7f12032b

.field public static final billing_refund_header:I = 0x7f12032c

.field public static final billing_refund_play_time_never_played:I = 0x7f12032d

.field public static final billing_refund_play_time_subheader:I = 0x7f12032e

.field public static final billing_refund_purchase_date:I = 0x7f12032f

.field public static final billing_refund_purchase_date_subheader:I = 0x7f120330

.field public static final billing_refund_release_date_subheader:I = 0x7f120331

.field public static final billing_refund_report_a_problem:I = 0x7f120332

.field public static final billing_sales_tax_added:I = 0x7f120333

.field public static final billing_sales_tax_included:I = 0x7f120334

.field public static final billing_secure_tooltip:I = 0x7f120335

.field public static final billing_select_payment_source_tooltip:I = 0x7f120336

.field public static final billing_select_plan_premium_month_tier_1:I = 0x7f120337

.field public static final billing_select_plan_premium_month_tier_2:I = 0x7f120338

.field public static final billing_select_plan_premium_year_tier_1:I = 0x7f120339

.field public static final billing_select_plan_premium_year_tier_2:I = 0x7f12033a

.field public static final billing_standalone_add_payment_title:I = 0x7f12033b

.field public static final billing_standalone_game_pass_redemption_title:I = 0x7f12033c

.field public static final billing_standalone_guild_subscription_purchase_title:I = 0x7f12033d

.field public static final billing_standalone_payment_history_title:I = 0x7f12033e

.field public static final billing_standalone_premium_gift_purchase_title:I = 0x7f12033f

.field public static final billing_standalone_premium_purchase_title:I = 0x7f120340

.field public static final billing_standalone_premium_switch_plan_title:I = 0x7f120341

.field public static final billing_step_address:I = 0x7f120342

.field public static final billing_step_awaiting_authentication:I = 0x7f120343

.field public static final billing_step_credit_card_information:I = 0x7f120344

.field public static final billing_step_payment:I = 0x7f120345

.field public static final billing_step_payment_info:I = 0x7f120346

.field public static final billing_step_payment_type:I = 0x7f120347

.field public static final billing_step_paypal:I = 0x7f120348

.field public static final billing_step_review:I = 0x7f120349

.field public static final billing_step_select_a_plan:I = 0x7f12034a

.field public static final billing_step_select_plan:I = 0x7f12034b

.field public static final billing_subscription_credit:I = 0x7f12034c

.field public static final billing_switch_plan_change:I = 0x7f12034d

.field public static final billing_switch_plan_change_date:I = 0x7f12034e

.field public static final billing_switch_plan_change_date_with_charge:I = 0x7f12034f

.field public static final billing_switch_plan_choose_one:I = 0x7f120350

.field public static final billing_switch_plan_choose_one_trial_subtitle:I = 0x7f120351

.field public static final billing_switch_plan_confirm_tier_1:I = 0x7f120352

.field public static final billing_switch_plan_confirm_tier_1_year_to_month:I = 0x7f120353

.field public static final billing_switch_plan_confirm_tier_2:I = 0x7f120354

.field public static final billing_switch_plan_confirm_tier_2_to_tier_1:I = 0x7f120355

.field public static final billing_switch_plan_confirm_tier_2_year_to_month:I = 0x7f120356

.field public static final billing_switch_plan_confirm_upgrade_tier_1_year:I = 0x7f120357

.field public static final billing_switch_plan_confirm_upgrade_tier_2_month:I = 0x7f120358

.field public static final billing_switch_plan_confirm_upgrade_tier_2_year:I = 0x7f120359

.field public static final billing_switch_plan_current_plan:I = 0x7f12035a

.field public static final billing_switch_plan_downgrade_body_month:I = 0x7f12035b

.field public static final billing_switch_plan_downgrade_body_tier_1:I = 0x7f12035c

.field public static final billing_switch_plan_purchase_details:I = 0x7f12035d

.field public static final billing_switch_plan_select:I = 0x7f12035e

.field public static final billing_switch_plan_subscription_cost:I = 0x7f12035f

.field public static final billing_switch_plan_tier_1_description:I = 0x7f120360

.field public static final billing_switch_plan_tier_2_description:I = 0x7f120361

.field public static final billing_switch_plan_upgrade:I = 0x7f120362

.field public static final billing_switch_plan_upgrade_body_tier_1_year:I = 0x7f120363

.field public static final billing_switch_plan_upgrade_body_tier_2:I = 0x7f120364

.field public static final billing_switch_plan_upgrade_body_tier_2_year:I = 0x7f120365

.field public static final billing_switch_plan_yearly_free_months:I = 0x7f120366

.field public static final billing_switch_plan_you_selected:I = 0x7f120367

.field public static final billing_tag_failed:I = 0x7f120368

.field public static final billing_tag_pending:I = 0x7f120369

.field public static final billing_tag_refunded:I = 0x7f12036a

.field public static final billing_tag_reversed:I = 0x7f12036b

.field public static final billing_third_party_eula_label:I = 0x7f12036c

.field public static final black_friday_2020_banner_title:I = 0x7f12036d

.field public static final black_friday_2020_upsell_content:I = 0x7f12036e

.field public static final black_friday_modal_upsell_body:I = 0x7f12036f

.field public static final black_friday_modal_upsell_title:I = 0x7f120370

.field public static final black_friday_promotion_gift_inventory_title:I = 0x7f120371

.field public static final black_friday_promotion_notice_action:I = 0x7f120372

.field public static final black_friday_promotion_notice_body:I = 0x7f120373

.field public static final block:I = 0x7f120374

.field public static final blocked:I = 0x7f120375

.field public static final blocked_message_count:I = 0x7f120376

.field public static final blocked_messages:I = 0x7f120377

.field public static final blocked_messages_hide:I = 0x7f120378

.field public static final blocked_messages_show:I = 0x7f120379

.field public static final blue:I = 0x7f12037a

.field public static final bot_call_idle_disconnect:I = 0x7f12037b

.field public static final bot_dm_explicit_content:I = 0x7f12037c

.field public static final bot_dm_rate_limited:I = 0x7f12037d

.field public static final bot_dm_send_failed:I = 0x7f12037e

.field public static final bot_dm_send_message_temporarily_disabled:I = 0x7f12037f

.field public static final bot_gdm_explicit_content:I = 0x7f120380

.field public static final bot_guild_explicit_content:I = 0x7f120381

.field public static final bot_requires_email_verification:I = 0x7f120382

.field public static final bot_tag:I = 0x7f120383

.field public static final bot_tag_bot:I = 0x7f120384

.field public static final bot_tag_server:I = 0x7f120385

.field public static final bottom_sheet_behavior:I = 0x7f120386

.field public static final brown:I = 0x7f120387

.field public static final browser_chrome:I = 0x7f120388

.field public static final browser_firefox:I = 0x7f120389

.field public static final browser_handoff_authenticating_description:I = 0x7f12038a

.field public static final browser_handoff_authenticating_title:I = 0x7f12038b

.field public static final browser_handoff_detecting_title:I = 0x7f12038c

.field public static final browser_handoff_done_safe_to_close:I = 0x7f12038d

.field public static final browser_handoff_done_title:I = 0x7f12038e

.field public static final browser_handoff_failed_title:I = 0x7f12038f

.field public static final browser_handoff_success_action:I = 0x7f120390

.field public static final browser_handoff_success_body:I = 0x7f120391

.field public static final browser_handoff_success_cancel:I = 0x7f120392

.field public static final browser_handoff_success_title:I = 0x7f120393

.field public static final browser_input_device_warning:I = 0x7f120394

.field public static final browser_not_supported:I = 0x7f120395

.field public static final browser_notifications_enabled_body:I = 0x7f120396

.field public static final browser_output_device_warning:I = 0x7f120397

.field public static final browser_update_notice:I = 0x7f120398

.field public static final bug_hunter_badge_tooltip:I = 0x7f120399

.field public static final build_override:I = 0x7f12039a

.field public static final build_override_apply:I = 0x7f12039b

.field public static final build_override_clear:I = 0x7f12039c

.field public static final build_override_expired:I = 0x7f12039d

.field public static final build_override_for:I = 0x7f12039e

.field public static final build_override_id:I = 0x7f12039f

.field public static final build_override_incompatible_client:I = 0x7f1203a0

.field public static final build_override_incompatible_targets:I = 0x7f1203a1

.field public static final build_override_invalid:I = 0x7f1203a2

.field public static final build_override_invalid_user:I = 0x7f1203a3

.field public static final build_override_isnt_available:I = 0x7f1203a4

.field public static final build_override_link_copied:I = 0x7f1203a5

.field public static final build_override_link_copy:I = 0x7f1203a6

.field public static final build_override_modal_apply:I = 0x7f1203a7

.field public static final build_override_modal_expires_duration:I = 0x7f1203a8

.field public static final build_override_modal_invalid:I = 0x7f1203a9

.field public static final build_override_modal_invalid_button:I = 0x7f1203aa

.field public static final build_override_modal_invite:I = 0x7f1203ab

.field public static final bundle_ready_body:I = 0x7f1203ac

.field public static final bundle_ready_later:I = 0x7f1203ad

.field public static final bundle_ready_restart:I = 0x7f1203ae

.field public static final bundle_ready_title:I = 0x7f1203af

.field public static final burgundy:I = 0x7f1203b0

.field public static final call:I = 0x7f1203b1

.field public static final call_ended:I = 0x7f1203b2

.field public static final call_ended_description:I = 0x7f1203b3

.field public static final call_feedback_confirmation:I = 0x7f1203b4

.field public static final call_feedback_issue_section_header:I = 0x7f1203b5

.field public static final call_feedback_option_audio_cut:I = 0x7f1203b6

.field public static final call_feedback_option_audio_echos:I = 0x7f1203b7

.field public static final call_feedback_option_audio_robotic:I = 0x7f1203b8

.field public static final call_feedback_option_background_noise:I = 0x7f1203b9

.field public static final call_feedback_option_bad_volume:I = 0x7f1203ba

.field public static final call_feedback_option_could_not_hear_audio:I = 0x7f1203bb

.field public static final call_feedback_option_headset:I = 0x7f1203bc

.field public static final call_feedback_option_nobody_could_hear_me:I = 0x7f1203bd

.field public static final call_feedback_option_other:I = 0x7f1203be

.field public static final call_feedback_option_speakerphone:I = 0x7f1203bf

.field public static final call_feedback_prompt:I = 0x7f1203c0

.field public static final call_feedback_sentiment_negative:I = 0x7f1203c1

.field public static final call_feedback_sentiment_neutral:I = 0x7f1203c2

.field public static final call_feedback_sentiment_positive:I = 0x7f1203c3

.field public static final call_feedback_sheet_title:I = 0x7f1203c4

.field public static final call_invite_not_friends:I = 0x7f1203c5

.field public static final call_mobile_tap_to_return:I = 0x7f1203c6

.field public static final call_unavailable:I = 0x7f1203c7

.field public static final camera:I = 0x7f1203c8

.field public static final camera_a11y_turned_off:I = 0x7f1203c9

.field public static final camera_a11y_turned_on:I = 0x7f1203ca

.field public static final camera_disabled_limit_reached:I = 0x7f1203cb

.field public static final camera_intent_result_error:I = 0x7f1203cc

.field public static final camera_no_device:I = 0x7f1203cd

.field public static final camera_not_enabled:I = 0x7f1203ce

.field public static final camera_off:I = 0x7f1203cf

.field public static final camera_on:I = 0x7f1203d0

.field public static final camera_permission_denied:I = 0x7f1203d1

.field public static final camera_preview_menu_item:I = 0x7f1203d2

.field public static final camera_preview_modal_cta:I = 0x7f1203d3

.field public static final camera_preview_modal_header:I = 0x7f1203d4

.field public static final camera_preview_modal_subtitle:I = 0x7f1203d5

.field public static final camera_switch:I = 0x7f1203d6

.field public static final camera_switched:I = 0x7f1203d7

.field public static final camera_unavailable:I = 0x7f1203d8

.field public static final camera_unknown_error:I = 0x7f1203d9

.field public static final cameraview_default_autofocus_marker:I = 0x7f1203da

.field public static final cameraview_filter_autofix:I = 0x7f1203db

.field public static final cameraview_filter_black_and_white:I = 0x7f1203dc

.field public static final cameraview_filter_brightness:I = 0x7f1203dd

.field public static final cameraview_filter_contrast:I = 0x7f1203de

.field public static final cameraview_filter_cross_process:I = 0x7f1203df

.field public static final cameraview_filter_documentary:I = 0x7f1203e0

.field public static final cameraview_filter_duotone:I = 0x7f1203e1

.field public static final cameraview_filter_fill_light:I = 0x7f1203e2

.field public static final cameraview_filter_gamma:I = 0x7f1203e3

.field public static final cameraview_filter_grain:I = 0x7f1203e4

.field public static final cameraview_filter_grayscale:I = 0x7f1203e5

.field public static final cameraview_filter_hue:I = 0x7f1203e6

.field public static final cameraview_filter_invert_colors:I = 0x7f1203e7

.field public static final cameraview_filter_lomoish:I = 0x7f1203e8

.field public static final cameraview_filter_none:I = 0x7f1203e9

.field public static final cameraview_filter_posterize:I = 0x7f1203ea

.field public static final cameraview_filter_saturation:I = 0x7f1203eb

.field public static final cameraview_filter_sepia:I = 0x7f1203ec

.field public static final cameraview_filter_sharpness:I = 0x7f1203ed

.field public static final cameraview_filter_temperature:I = 0x7f1203ee

.field public static final cameraview_filter_tint:I = 0x7f1203ef

.field public static final cameraview_filter_vignette:I = 0x7f1203f0

.field public static final cancel:I = 0x7f1203f1

.field public static final cannot_attach_files:I = 0x7f1203f2

.field public static final cannot_delete_channel:I = 0x7f1203f3

.field public static final cannot_deny_missing_permission:I = 0x7f1203f4

.field public static final cannot_deny_self_simple:I = 0x7f1203f5

.field public static final cannot_deny_singular_permission:I = 0x7f1203f6

.field public static final cannot_manage_higher_rank:I = 0x7f1203f7

.field public static final cannot_manage_is_owner:I = 0x7f1203f8

.field public static final cannot_manage_same_rank:I = 0x7f1203f9

.field public static final captcha:I = 0x7f1203fa

.field public static final captcha_failed:I = 0x7f1203fb

.field public static final captcha_failed_play_services:I = 0x7f1203fc

.field public static final captcha_failed_unsupported:I = 0x7f1203fd

.field public static final captcha_issues:I = 0x7f1203fe

.field public static final captcha_open_browser:I = 0x7f1203ff

.field public static final captcha_problems:I = 0x7f120400

.field public static final captcha_problems_info:I = 0x7f120401

.field public static final categories:I = 0x7f120402

.field public static final category:I = 0x7f120403

.field public static final category_a11y_label:I = 0x7f120404

.field public static final category_a11y_label_with_expanded_state:I = 0x7f120405

.field public static final category_has_been_deleted:I = 0x7f120406

.field public static final category_name:I = 0x7f120407

.field public static final category_name_placeholder:I = 0x7f120408

.field public static final category_permissions_subtitle:I = 0x7f120409

.field public static final category_settings:I = 0x7f12040a

.field public static final category_settings_have_been_updated:I = 0x7f12040b

.field public static final certified:I = 0x7f12040c

.field public static final certified_device_recommendation_audio_input_and_output_body:I = 0x7f12040d

.field public static final certified_device_recommendation_audio_input_body:I = 0x7f12040e

.field public static final certified_device_recommendation_audio_output_body:I = 0x7f12040f

.field public static final certified_device_recommendation_title:I = 0x7f120410

.field public static final change:I = 0x7f120411

.field public static final change_avatar:I = 0x7f120412

.field public static final change_avatar_a11y_label:I = 0x7f120413

.field public static final change_banner:I = 0x7f120414

.field public static final change_camera:I = 0x7f120415

.field public static final change_category:I = 0x7f120416

.field public static final change_email:I = 0x7f120417

.field public static final change_email_short:I = 0x7f120418

.field public static final change_icon:I = 0x7f120419

.field public static final change_log:I = 0x7f12041a

.field public static final change_log_md_body:I = 0x7f12041b

.field public static final change_log_md_date:I = 0x7f12041c

.field public static final change_log_md_experiment_body:I = 0x7f12041d

.field public static final change_log_md_experiment_date:I = 0x7f12041e

.field public static final change_log_md_experiment_experiment_bucket:I = 0x7f12041f

.field public static final change_log_md_experiment_experiment_names:I = 0x7f120420

.field public static final change_log_md_experiment_locale:I = 0x7f120421

.field public static final change_log_md_experiment_revision:I = 0x7f120422

.field public static final change_log_md_experiment_template:I = 0x7f120423

.field public static final change_log_md_locale:I = 0x7f120424

.field public static final change_log_md_revision:I = 0x7f120425

.field public static final change_log_md_video:I = 0x7f120426

.field public static final change_nickname:I = 0x7f120427

.field public static final change_nickname_description:I = 0x7f120428

.field public static final change_nickname_warning:I = 0x7f120429

.field public static final change_password:I = 0x7f12042a

.field public static final change_password_link:I = 0x7f12042b

.field public static final change_phone_number:I = 0x7f12042c

.field public static final change_splash:I = 0x7f12042d

.field public static final change_vanity_url_error:I = 0x7f12042e

.field public static final changelog_special_cta:I = 0x7f12042f

.field public static final changelog_special_cta_body:I = 0x7f120430

.field public static final changelog_special_cta_desktop:I = 0x7f120431

.field public static final changelog_special_cta_desktop_fallback:I = 0x7f120432

.field public static final changelog_special_cta_title:I = 0x7f120433

.field public static final changelog_special_share_discord:I = 0x7f120434

.field public static final changelog_stickers_cta:I = 0x7f120435

.field public static final changelog_stickers_cta_body:I = 0x7f120436

.field public static final changelog_stickers_cta_title:I = 0x7f120437

.field public static final changelog_stickers_header:I = 0x7f120438

.field public static final channel:I = 0x7f120439

.field public static final channel_a11y_label:I = 0x7f12043a

.field public static final channel_actions_menu_label:I = 0x7f12043b

.field public static final channel_call_current_speaker:I = 0x7f12043c

.field public static final channel_call_members_popout_header:I = 0x7f12043d

.field public static final channel_call_overflow_menu_label:I = 0x7f12043e

.field public static final channel_call_participants:I = 0x7f12043f

.field public static final channel_has_been_deleted:I = 0x7f120440

.field public static final channel_header_bar_a11y_label:I = 0x7f120441

.field public static final channel_locked:I = 0x7f120442

.field public static final channel_locked_short:I = 0x7f120443

.field public static final channel_locked_to_category:I = 0x7f120444

.field public static final channel_members_a11y_label:I = 0x7f120445

.field public static final channel_mention_badge_a11y_label:I = 0x7f120446

.field public static final channel_message_a11y_label:I = 0x7f120447

.field public static final channel_message_a11y_role_description:I = 0x7f120448

.field public static final channel_messages_a11y_description:I = 0x7f120449

.field public static final channel_messages_a11y_label:I = 0x7f12044a

.field public static final channel_messages_a11y_role_description:I = 0x7f12044b

.field public static final channel_mute_label:I = 0x7f12044c

.field public static final channel_mute_tooltip:I = 0x7f12044d

.field public static final channel_name_placeholder:I = 0x7f12044e

.field public static final channel_or_category:I = 0x7f12044f

.field public static final channel_order_updated:I = 0x7f120450

.field public static final channel_permissions:I = 0x7f120451

.field public static final channel_permissions_add_members_title:I = 0x7f120452

.field public static final channel_permissions_add_members_tooltip:I = 0x7f120453

.field public static final channel_permissions_add_members_tooltip_administrator:I = 0x7f120454

.field public static final channel_permissions_add_members_tooltip_owner:I = 0x7f120455

.field public static final channel_permissions_advanced_permissions:I = 0x7f120456

.field public static final channel_permissions_advanced_view:I = 0x7f120457

.field public static final channel_permissions_basic_view:I = 0x7f120458

.field public static final channel_permissions_everyone_can_not_view_warning:I = 0x7f120459

.field public static final channel_permissions_everyone_is_admin_warning:I = 0x7f12045a

.field public static final channel_permissions_make_private_alert_subtitle:I = 0x7f12045b

.field public static final channel_permissions_make_private_alert_title:I = 0x7f12045c

.field public static final channel_permissions_make_public_alert_subtitle:I = 0x7f12045d

.field public static final channel_permissions_make_public_alert_title:I = 0x7f12045e

.field public static final channel_permissions_no_roles:I = 0x7f12045f

.field public static final channel_permissions_private_category_description:I = 0x7f120460

.field public static final channel_permissions_private_channel_description:I = 0x7f120461

.field public static final channel_permissions_private_channel_description_voice:I = 0x7f120462

.field public static final channel_permissions_read_only_description:I = 0x7f120463

.field public static final channel_permissions_read_only_title:I = 0x7f120464

.field public static final channel_permissions_subtitle:I = 0x7f120465

.field public static final channel_select:I = 0x7f120466

.field public static final channel_settings:I = 0x7f120467

.field public static final channel_settings_have_been_updated:I = 0x7f120468

.field public static final channel_slowmode_cooldown:I = 0x7f120469

.field public static final channel_slowmode_desc:I = 0x7f12046a

.field public static final channel_slowmode_desc_hours:I = 0x7f12046b

.field public static final channel_slowmode_desc_immune:I = 0x7f12046c

.field public static final channel_slowmode_desc_minutes:I = 0x7f12046d

.field public static final channel_slowmode_desc_short:I = 0x7f12046e

.field public static final channel_step_subtitle:I = 0x7f12046f

.field public static final channel_step_title:I = 0x7f120470

.field public static final channel_topic_empty:I = 0x7f120471

.field public static final channel_type:I = 0x7f120472

.field public static final channel_unmute_tooltip:I = 0x7f120473

.field public static final channels:I = 0x7f120474

.field public static final channels_unavailable_body:I = 0x7f120475

.field public static final channels_unavailable_title:I = 0x7f120476

.field public static final character_count_at_limit:I = 0x7f120477

.field public static final character_count_close_to_limit:I = 0x7f120478

.field public static final character_count_over_limit:I = 0x7f120479

.field public static final character_counter_content_description:I = 0x7f12047a

.field public static final character_counter_overflowed_content_description:I = 0x7f12047b

.field public static final character_counter_pattern:I = 0x7f12047c

.field public static final chat:I = 0x7f12047d

.field public static final chat_attach_invite_to_listen:I = 0x7f12047e

.field public static final chat_attach_invite_to_play_game:I = 0x7f12047f

.field public static final chat_attach_invite_to_watch:I = 0x7f120480

.field public static final chat_attach_upload_a_file:I = 0x7f120481

.field public static final chat_attach_upload_or_invite:I = 0x7f120482

.field public static final chat_behavior:I = 0x7f120483

.field public static final checking_for_updates:I = 0x7f120484

.field public static final chip_text:I = 0x7f120485

.field public static final choose_an_application:I = 0x7f120486

.field public static final claim_account:I = 0x7f120487

.field public static final claim_account_body:I = 0x7f120488

.field public static final claim_account_body_short:I = 0x7f120489

.field public static final claim_account_email_to:I = 0x7f12048a

.field public static final claim_account_get_app:I = 0x7f12048b

.field public static final claim_account_long:I = 0x7f12048c

.field public static final claim_account_promote_app_2020_06:I = 0x7f12048d

.field public static final claim_account_required_body:I = 0x7f12048e

.field public static final claim_account_required_email_to:I = 0x7f12048f

.field public static final claim_account_short:I = 0x7f120490

.field public static final claim_account_title:I = 0x7f120491

.field public static final clear_attachments:I = 0x7f120492

.field public static final clear_role_permissions:I = 0x7f120493

.field public static final clear_text_end_icon_content_description:I = 0x7f120494

.field public static final clone_channel:I = 0x7f120495

.field public static final clone_channel_help:I = 0x7f120496

.field public static final clone_server_button_cta:I = 0x7f120497

.field public static final close:I = 0x7f120498

.field public static final close_action_sheet:I = 0x7f120499

.field public static final close_dm:I = 0x7f12049a

.field public static final close_drawer:I = 0x7f12049b

.field public static final close_stream:I = 0x7f12049c

.field public static final close_window:I = 0x7f12049d

.field public static final cloud_sync_icon_tooltip_done:I = 0x7f12049e

.field public static final cloud_sync_icon_tooltip_planning:I = 0x7f12049f

.field public static final cloud_sync_icon_tooltip_preparing:I = 0x7f1204a0

.field public static final cloud_sync_icon_tooltip_pulling:I = 0x7f1204a1

.field public static final cloud_sync_icon_tooltip_pushing:I = 0x7f1204a2

.field public static final cloud_sync_icon_tooltip_supported:I = 0x7f1204a3

.field public static final cloud_sync_modal_conflict_choice_download:I = 0x7f1204a4

.field public static final cloud_sync_modal_conflict_choice_upload:I = 0x7f1204a5

.field public static final cloud_sync_modal_conflict_description:I = 0x7f1204a6

.field public static final cloud_sync_modal_conflict_header:I = 0x7f1204a7

.field public static final cloud_sync_modal_conflict_last_modified:I = 0x7f1204a8

.field public static final cloud_sync_modal_error_description:I = 0x7f1204a9

.field public static final cloud_sync_modal_error_header:I = 0x7f1204aa

.field public static final cloud_sync_modal_or:I = 0x7f1204ab

.field public static final collapse:I = 0x7f1204ac

.field public static final collapse_category:I = 0x7f1204ad

.field public static final collapsed:I = 0x7f1204ae

.field public static final color_picker_custom:I = 0x7f1204af

.field public static final color_picker_presets:I = 0x7f1204b0

.field public static final color_picker_title:I = 0x7f1204b1

.field public static final color_picker_transparency:I = 0x7f1204b2

.field public static final coming_soon:I = 0x7f1204b4

.field public static final command_giphy_description:I = 0x7f1204b5

.field public static final command_giphy_query_description:I = 0x7f1204b6

.field public static final command_me_description:I = 0x7f1204b7

.field public static final command_me_message_description:I = 0x7f1204b8

.field public static final command_nick_description:I = 0x7f1204b9

.field public static final command_nick_failure:I = 0x7f1204ba

.field public static final command_nick_failure_permission:I = 0x7f1204bb

.field public static final command_nick_newnick_description:I = 0x7f1204bc

.field public static final command_nick_reset:I = 0x7f1204bd

.field public static final command_nick_success:I = 0x7f1204be

.field public static final command_shrug_description:I = 0x7f1204bf

.field public static final command_shrug_message_description:I = 0x7f1204c0

.field public static final command_spoiler_description:I = 0x7f1204c1

.field public static final command_spoiler_message_description:I = 0x7f1204c2

.field public static final command_tableflip_description:I = 0x7f1204c3

.field public static final command_tableflip_message_description:I = 0x7f1204c4

.field public static final command_tableunflip_description:I = 0x7f1204c5

.field public static final command_tableunflip_message_description:I = 0x7f1204c6

.field public static final command_tts_description:I = 0x7f1204c7

.field public static final command_tts_message_description:I = 0x7f1204c8

.field public static final command_validation_boolean_error:I = 0x7f1204c9

.field public static final command_validation_channel_error:I = 0x7f1204ca

.field public static final command_validation_choice_error:I = 0x7f1204cb

.field public static final command_validation_integer_error:I = 0x7f1204cc

.field public static final command_validation_required_error:I = 0x7f1204cd

.field public static final command_validation_role_error:I = 0x7f1204ce

.field public static final command_validation_user_error:I = 0x7f1204cf

.field public static final commands:I = 0x7f1204d0

.field public static final commands_matching:I = 0x7f1204d1

.field public static final commands_optional_header:I = 0x7f1204d2

.field public static final common_google_play_services_enable_button:I = 0x7f1204d3

.field public static final common_google_play_services_enable_text:I = 0x7f1204d4

.field public static final common_google_play_services_enable_title:I = 0x7f1204d5

.field public static final common_google_play_services_install_button:I = 0x7f1204d6

.field public static final common_google_play_services_install_text:I = 0x7f1204d7

.field public static final common_google_play_services_install_title:I = 0x7f1204d8

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1204d9

.field public static final common_google_play_services_notification_ticker:I = 0x7f1204da

.field public static final common_google_play_services_unknown_issue:I = 0x7f1204db

.field public static final common_google_play_services_unsupported_text:I = 0x7f1204dc

.field public static final common_google_play_services_update_button:I = 0x7f1204dd

.field public static final common_google_play_services_update_text:I = 0x7f1204de

.field public static final common_google_play_services_update_title:I = 0x7f1204df

.field public static final common_google_play_services_updating_text:I = 0x7f1204e0

.field public static final common_google_play_services_wear_update_text:I = 0x7f1204e1

.field public static final common_open_on_phone:I = 0x7f1204e2

.field public static final common_signin_button_text:I = 0x7f1204e3

.field public static final common_signin_button_text_long:I = 0x7f1204e4

.field public static final communicators_info:I = 0x7f1204e5

.field public static final community:I = 0x7f1204e6

.field public static final community_policy_help:I = 0x7f1204e7

.field public static final community_policy_title:I = 0x7f1204e8

.field public static final compact_mode:I = 0x7f1204e9

.field public static final competing:I = 0x7f1204ea

.field public static final completed:I = 0x7f1204eb

.field public static final configure:I = 0x7f1204ec

.field public static final confirm:I = 0x7f1204ed

.field public static final confirm_channel_drag_title:I = 0x7f1204ee

.field public static final confirm_disable_silence_body:I = 0x7f1204ef

.field public static final confirm_disable_silence_dont_show:I = 0x7f1204f0

.field public static final confirm_disable_silence_title:I = 0x7f1204f1

.field public static final confirm_qr_check_your_phone:I = 0x7f1204f2

.field public static final confirm_qr_description:I = 0x7f1204f3

.field public static final confirm_qr_keep_me_signed_in:I = 0x7f1204f4

.field public static final confirm_qr_login_on_computer:I = 0x7f1204f5

.field public static final confirm_user_block_body:I = 0x7f1204f6

.field public static final confirm_user_block_title:I = 0x7f1204f7

.field public static final connect:I = 0x7f1204f8

.field public static final connect_account_description:I = 0x7f1204f9

.field public static final connect_account_title:I = 0x7f1204fa

.field public static final connect_to_video:I = 0x7f1204fb

.field public static final connect_to_voice:I = 0x7f1204fc

.field public static final connect_voice_only:I = 0x7f1204fd

.field public static final connected_account_revoked:I = 0x7f1204fe

.field public static final connected_account_verify_failure:I = 0x7f1204ff

.field public static final connected_account_verify_success:I = 0x7f120500

.field public static final connected_account_verifying:I = 0x7f120501

.field public static final connected_accounts:I = 0x7f120502

.field public static final connected_accounts_none:I = 0x7f120503

.field public static final connected_accounts_none_title:I = 0x7f120504

.field public static final connected_device_detected_cancel_button:I = 0x7f120505

.field public static final connected_device_detected_confirm_button:I = 0x7f120506

.field public static final connected_device_detected_input_and_output_body:I = 0x7f120507

.field public static final connected_device_detected_input_body:I = 0x7f120508

.field public static final connected_device_detected_option_input:I = 0x7f120509

.field public static final connected_device_detected_option_input_and_output:I = 0x7f12050a

.field public static final connected_device_detected_option_output:I = 0x7f12050b

.field public static final connected_device_detected_output_body:I = 0x7f12050c

.field public static final connected_device_detected_title:I = 0x7f12050d

.field public static final connected_on_another_client:I = 0x7f12050e

.field public static final connecting:I = 0x7f12050f

.field public static final connecting_problems_cta:I = 0x7f120510

.field public static final connection_enter_code:I = 0x7f120511

.field public static final connection_invalid_pin:I = 0x7f120512

.field public static final connection_login_microsoft:I = 0x7f120513

.field public static final connection_login_with:I = 0x7f120514

.field public static final connection_status_authenticating:I = 0x7f120515

.field public static final connection_status_awaiting_endpoint:I = 0x7f120516

.field public static final connection_status_connected:I = 0x7f120517

.field public static final connection_status_connecting:I = 0x7f120518

.field public static final connection_status_disconnected:I = 0x7f120519

.field public static final connection_status_ice_checking:I = 0x7f12051a

.field public static final connection_status_no_route:I = 0x7f12051b

.field public static final connection_status_rtc_connecting:I = 0x7f12051c

.field public static final connection_status_rtc_disconnected:I = 0x7f12051d

.field public static final connection_status_stream_connected:I = 0x7f12051e

.field public static final connection_status_stream_self_connected:I = 0x7f12051f

.field public static final connection_status_video_connected:I = 0x7f120520

.field public static final connection_status_voice_connected:I = 0x7f120521

.field public static final connection_verified:I = 0x7f120522

.field public static final connections:I = 0x7f120523

.field public static final contact_sync_cta_button_subtitle:I = 0x7f120524

.field public static final contact_sync_cta_button_title:I = 0x7f120525

.field public static final contact_sync_enter_phone_number_description:I = 0x7f120526

.field public static final contact_sync_enter_phone_number_title:I = 0x7f120527

.field public static final contact_sync_failed_alert_message:I = 0x7f120528

.field public static final contact_sync_failed_alert_title:I = 0x7f120529

.field public static final contact_sync_failed_no_phone_alert_message:I = 0x7f12052a

.field public static final contact_sync_failed_no_phone_alert_title:I = 0x7f12052b

.field public static final contact_sync_landing_screen_button:I = 0x7f12052c

.field public static final contact_sync_landing_screen_description:I = 0x7f12052d

.field public static final contact_sync_landing_screen_title:I = 0x7f12052e

.field public static final contact_sync_no_results_description:I = 0x7f12052f

.field public static final contact_sync_no_results_got_it:I = 0x7f120530

.field public static final contact_sync_no_results_title:I = 0x7f120531

.field public static final contact_sync_permission_denied_alert_message:I = 0x7f120532

.field public static final contact_sync_permission_denied_alert_title:I = 0x7f120533

.field public static final contact_sync_submit_phone_number:I = 0x7f120534

.field public static final contact_sync_syncing_description:I = 0x7f120535

.field public static final contact_sync_syncing_title:I = 0x7f120536

.field public static final contact_sync_toggle_label:I = 0x7f120537

.field public static final contact_sync_toggle_sub_label:I = 0x7f120538

.field public static final contact_sync_we_found_your_friends_body:I = 0x7f120539

.field public static final contact_sync_we_found_your_friends_title:I = 0x7f12053a

.field public static final content_matching:I = 0x7f12053b

.field public static final context_menu_hint:I = 0x7f12053c

.field public static final continue_in_browser:I = 0x7f12053d

.field public static final continue_to_webapp:I = 0x7f12053e

.field public static final control_volume:I = 0x7f12053f

.field public static final convert_emoticons:I = 0x7f120540

.field public static final convert_emoticons_help:I = 0x7f120541

.field public static final copied:I = 0x7f120542

.field public static final copied_text:I = 0x7f120543

.field public static final copy:I = 0x7f120544

.field public static final copy_channel_topic:I = 0x7f120545

.field public static final copy_code:I = 0x7f120546

.field public static final copy_id:I = 0x7f120547

.field public static final copy_image_preview:I = 0x7f120548

.field public static final copy_link:I = 0x7f120549

.field public static final copy_media_link:I = 0x7f12054a

.field public static final copy_message_link:I = 0x7f12054b

.field public static final copy_owner_id:I = 0x7f12054c

.field public static final copy_text:I = 0x7f12054d

.field public static final copy_username:I = 0x7f12054f

.field public static final countdown_units_days:I = 0x7f120550

.field public static final countdown_units_hours:I = 0x7f120551

.field public static final countdown_units_minutes:I = 0x7f120552

.field public static final countdown_units_seconds:I = 0x7f120553

.field public static final country_code:I = 0x7f120554

.field public static final country_codes:I = 0x7f120555

.field public static final country_no_results:I = 0x7f120556

.field public static final covid_new_user_survey_button:I = 0x7f120557

.field public static final covid_new_user_survey_prompt:I = 0x7f120558

.field public static final cozy_mode:I = 0x7f120559

.field public static final crash_app_version:I = 0x7f12055f

.field public static final crash_details:I = 0x7f120560

.field public static final crash_device:I = 0x7f120561

.field public static final crash_device_version:I = 0x7f120562

.field public static final crash_disclaimer:I = 0x7f120563

.field public static final crash_source:I = 0x7f120564

.field public static final crash_testers_invite:I = 0x7f120565

.field public static final crash_timestamp:I = 0x7f120566

.field public static final crash_unexpected:I = 0x7f120567

.field public static final create:I = 0x7f120568

.field public static final create_category:I = 0x7f120569

.field public static final create_channel:I = 0x7f12056a

.field public static final create_channel_error:I = 0x7f12056b

.field public static final create_channel_in_category:I = 0x7f12056c

.field public static final create_dm:I = 0x7f12056d

.field public static final create_group_dm:I = 0x7f12056e

.field public static final create_guild_description:I = 0x7f12056f

.field public static final create_guild_with_templates_title:I = 0x7f120570

.field public static final create_instant_invite:I = 0x7f120571

.field public static final create_link:I = 0x7f120572

.field public static final create_news_channel:I = 0x7f120573

.field public static final create_or_join_modal_header:I = 0x7f120574

.field public static final create_private_text_channel:I = 0x7f120575

.field public static final create_private_voice_channel:I = 0x7f120576

.field public static final create_server_button_action:I = 0x7f120577

.field public static final create_server_button_body:I = 0x7f120578

.field public static final create_server_button_cta:I = 0x7f120579

.field public static final create_server_button_cta_mobile:I = 0x7f12057a

.field public static final create_server_button_cta_mobile_desc:I = 0x7f12057b

.field public static final create_server_default_server_name_format:I = 0x7f12057c

.field public static final create_server_description_mobile:I = 0x7f12057d

.field public static final create_server_description_mobile_refresh:I = 0x7f12057e

.field public static final create_server_description_refresh:I = 0x7f12057f

.field public static final create_server_guidelines:I = 0x7f120580

.field public static final create_server_title:I = 0x7f120581

.field public static final create_server_title_mobile_refresh:I = 0x7f120582

.field public static final create_store_channel:I = 0x7f120583

.field public static final create_store_channel_application:I = 0x7f120584

.field public static final create_store_channel_beta_note:I = 0x7f120585

.field public static final create_store_channel_beta_title:I = 0x7f120586

.field public static final create_store_channel_branch:I = 0x7f120587

.field public static final create_store_channel_no_skus:I = 0x7f120588

.field public static final create_store_channel_no_skus_error:I = 0x7f120589

.field public static final create_store_channel_select_application:I = 0x7f12058a

.field public static final create_store_channel_select_branch:I = 0x7f12058b

.field public static final create_store_channel_select_sku:I = 0x7f12058c

.field public static final create_store_channel_sku:I = 0x7f12058d

.field public static final create_text_channel:I = 0x7f12058e

.field public static final create_voice_channel:I = 0x7f12058f

.field public static final creation_intent_checkbox_label:I = 0x7f120590

.field public static final creation_intent_description:I = 0x7f120591

.field public static final creation_intent_option_community:I = 0x7f120592

.field public static final creation_intent_option_friends:I = 0x7f120593

.field public static final creation_intent_skip:I = 0x7f120594

.field public static final creation_intent_title:I = 0x7f120595

.field public static final creator_journey_survey:I = 0x7f120596

.field public static final credit_card_cvc:I = 0x7f120597

.field public static final credit_card_error_expiration:I = 0x7f120598

.field public static final credit_card_error_number:I = 0x7f120599

.field public static final credit_card_error_required:I = 0x7f12059a

.field public static final credit_card_error_security_code:I = 0x7f12059b

.field public static final credit_card_expiration_date:I = 0x7f12059c

.field public static final credit_card_name:I = 0x7f12059d

.field public static final credit_card_name_on_card:I = 0x7f12059e

.field public static final credit_card_number:I = 0x7f12059f

.field public static final credit_card_security_code:I = 0x7f1205a0

.field public static final crop:I = 0x7f1205a1

.field public static final cs:I = 0x7f1205a2

.field public static final custom_color:I = 0x7f1205a3

.field public static final custom_status:I = 0x7f1205a4

.field public static final custom_status_clear_after:I = 0x7f1205a5

.field public static final custom_status_clear_custom_status:I = 0x7f1205a6

.field public static final custom_status_clear_in_hours:I = 0x7f1205a7

.field public static final custom_status_clear_in_minutes:I = 0x7f1205a8

.field public static final custom_status_clear_tomorrow:I = 0x7f1205a9

.field public static final custom_status_dont_clear:I = 0x7f1205aa

.field public static final custom_status_edit_custom_status_placeholder:I = 0x7f1205ab

.field public static final custom_status_hours:I = 0x7f1205ac

.field public static final custom_status_minutes:I = 0x7f1205ad

.field public static final custom_status_modal_body:I = 0x7f1205ae

.field public static final custom_status_modal_placeholder:I = 0x7f1205af

.field public static final custom_status_set_custom_status:I = 0x7f1205b0

.field public static final custom_status_today:I = 0x7f1205b1

.field public static final custom_status_update_success:I = 0x7f1205b2

.field public static final custom_tabs_mobile_body:I = 0x7f1205b3

.field public static final custom_tabs_mobile_header:I = 0x7f1205b4

.field public static final customize_group:I = 0x7f1205b5

.field public static final cut:I = 0x7f1205b6

.field public static final da:I = 0x7f1205b7

.field public static final dark_blue:I = 0x7f1205b8

.field public static final dark_grey:I = 0x7f1205b9

.field public static final dark_purple:I = 0x7f1205ba

.field public static final dark_teal:I = 0x7f1205bb

.field public static final data_download_requested_status_note:I = 0x7f1205bc

.field public static final data_privacy_controls:I = 0x7f1205bd

.field public static final data_privacy_controls_allow_accessibility_detection_note:I = 0x7f1205be

.field public static final data_privacy_controls_allow_accessibility_detection_title:I = 0x7f1205bf

.field public static final data_privacy_controls_basic_service_note:I = 0x7f1205c0

.field public static final data_privacy_controls_basic_service_title:I = 0x7f1205c1

.field public static final data_privacy_controls_personal_data_title:I = 0x7f1205c2

.field public static final data_privacy_controls_personalization_note_learn_more:I = 0x7f1205c3

.field public static final data_privacy_controls_personalization_title:I = 0x7f1205c4

.field public static final data_privacy_controls_request_data_download:I = 0x7f1205c5

.field public static final data_privacy_controls_request_data_failure_body:I = 0x7f1205c6

.field public static final data_privacy_controls_request_data_failure_title:I = 0x7f1205c7

.field public static final data_privacy_controls_request_data_modal_cancel:I = 0x7f1205c8

.field public static final data_privacy_controls_request_data_modal_confirm:I = 0x7f1205c9

.field public static final data_privacy_controls_request_data_modal_note:I = 0x7f1205ca

.field public static final data_privacy_controls_request_data_modal_success:I = 0x7f1205cb

.field public static final data_privacy_controls_request_data_modal_title:I = 0x7f1205cc

.field public static final data_privacy_controls_request_data_note:I = 0x7f1205cd

.field public static final data_privacy_controls_request_data_success_body:I = 0x7f1205ce

.field public static final data_privacy_controls_request_data_success_title:I = 0x7f1205cf

.field public static final data_privacy_controls_request_data_tooltip:I = 0x7f1205d0

.field public static final data_privacy_controls_usage_statistics_note:I = 0x7f1205d1

.field public static final data_privacy_controls_usage_statistics_title:I = 0x7f1205d2

.field public static final data_privacy_rate_limit_title:I = 0x7f1205d3

.field public static final data_privacy_update_consents_failed:I = 0x7f1205d4

.field public static final date:I = 0x7f1205d5

.field public static final de:I = 0x7f1205d6

.field public static final deafen:I = 0x7f1205d7

.field public static final deafen_members:I = 0x7f1205d8

.field public static final deauthorize:I = 0x7f1205d9

.field public static final deauthorize_app:I = 0x7f1205da

.field public static final deb:I = 0x7f1205db

.field public static final debug:I = 0x7f1205dc

.field public static final decline:I = 0x7f1205dd

.field public static final default_app_description:I = 0x7f1205de

.field public static final default_failure_to_perform_action_message:I = 0x7f1205df

.field public static final default_input_placeholder:I = 0x7f1205e0

.field public static final delete:I = 0x7f1205e2

.field public static final delete_account:I = 0x7f1205e3

.field public static final delete_account_body:I = 0x7f1205e4

.field public static final delete_account_error:I = 0x7f1205e5

.field public static final delete_account_transfer_ownership:I = 0x7f1205e6

.field public static final delete_account_transfer_ownership_body:I = 0x7f1205e7

.field public static final delete_app_confirm_msg:I = 0x7f1205e8

.field public static final delete_category:I = 0x7f1205e9

.field public static final delete_channel:I = 0x7f1205ea

.field public static final delete_channel_body:I = 0x7f1205eb

.field public static final delete_followed_news_body:I = 0x7f1205ec

.field public static final delete_message:I = 0x7f1205ed

.field public static final delete_message_body:I = 0x7f1205ee

.field public static final delete_message_context_menu_hint:I = 0x7f1205ef

.field public static final delete_message_history_option_24hr:I = 0x7f1205f0

.field public static final delete_message_history_option_7d:I = 0x7f1205f1

.field public static final delete_message_history_option_none:I = 0x7f1205f2

.field public static final delete_message_report:I = 0x7f1205f3

.field public static final delete_message_title:I = 0x7f1205f4

.field public static final delete_role:I = 0x7f1205f5

.field public static final delete_role_mobile:I = 0x7f1205f6

.field public static final delete_rules_channel_body:I = 0x7f1205f7

.field public static final delete_server:I = 0x7f1205f8

.field public static final delete_server_body:I = 0x7f1205f9

.field public static final delete_server_enter_name:I = 0x7f1205fa

.field public static final delete_server_incorrect_name:I = 0x7f1205fb

.field public static final delete_server_title:I = 0x7f1205fc

.field public static final delete_updates_channel_body:I = 0x7f1205fd

.field public static final description:I = 0x7f1205fe

.field public static final designate_other_channel:I = 0x7f1205ff

.field public static final desktop_app:I = 0x7f120600

.field public static final desktop_notifications_enable:I = 0x7f120601

.field public static final desktop_notifications_enable_body:I = 0x7f120602

.field public static final detect_platform_accounts:I = 0x7f120603

.field public static final developer_application_test_mode:I = 0x7f120604

.field public static final developer_application_test_mode_activate:I = 0x7f120605

.field public static final developer_application_test_mode_authorization_error:I = 0x7f120606

.field public static final developer_application_test_mode_clear:I = 0x7f120607

.field public static final developer_application_test_mode_invalid:I = 0x7f120608

.field public static final developer_application_test_mode_modal_body:I = 0x7f120609

.field public static final developer_application_test_mode_modal_header:I = 0x7f12060a

.field public static final developer_application_test_mode_note:I = 0x7f12060b

.field public static final developer_application_test_mode_placeholder:I = 0x7f12060c

.field public static final developer_mode:I = 0x7f12060d

.field public static final developer_mode_help_text:I = 0x7f12060e

.field public static final developer_options:I = 0x7f12060f

.field public static final device_settings:I = 0x7f120610

.field public static final direct_message:I = 0x7f120611

.field public static final direct_message_a11y_label:I = 0x7f120612

.field public static final direct_message_a11y_label_with_unreads:I = 0x7f120613

.field public static final direct_messages:I = 0x7f120614

.field public static final disable:I = 0x7f120615

.field public static final disable_account:I = 0x7f120616

.field public static final disable_account_body:I = 0x7f120617

.field public static final disable_email_notifications:I = 0x7f120618

.field public static final disable_integration_title:I = 0x7f120619

.field public static final disable_integration_twitch_body:I = 0x7f12061a

.field public static final disable_integration_youtube_body:I = 0x7f12061b

.field public static final disable_noise_cancellation:I = 0x7f12061c

.field public static final disable_notifications_description:I = 0x7f12061d

.field public static final disable_notifications_label:I = 0x7f12061e

.field public static final disable_sounds_description:I = 0x7f12061f

.field public static final disable_sounds_label:I = 0x7f120620

.field public static final disable_video:I = 0x7f120621

.field public static final discard_changes:I = 0x7f120622

.field public static final discard_changes_description:I = 0x7f120623

.field public static final discodo_disabled:I = 0x7f120624

.field public static final discodo_enabled:I = 0x7f120625

.field public static final disconnect:I = 0x7f120626

.field public static final disconnect_account:I = 0x7f120627

.field public static final disconnect_account_body:I = 0x7f120628

.field public static final disconnect_account_title:I = 0x7f120629

.field public static final disconnect_from_voice:I = 0x7f12062a

.field public static final disconnect_user_success:I = 0x7f12062b

.field public static final discord:I = 0x7f12062c

.field public static final discord_desc_long:I = 0x7f12062d

.field public static final discord_desc_short:I = 0x7f12062e

.field public static final discord_gg:I = 0x7f12062f

.field public static final discord_name:I = 0x7f120630

.field public static final discord_rich_presence:I = 0x7f120631

.field public static final discover:I = 0x7f120632

.field public static final discovery:I = 0x7f120633

.field public static final dismiss:I = 0x7f120634

.field public static final dispatch_error_modal_body:I = 0x7f120635

.field public static final dispatch_error_modal_description:I = 0x7f120636

.field public static final dispatch_error_modal_error_label:I = 0x7f120637

.field public static final dispatch_error_modal_header:I = 0x7f120638

.field public static final dispatch_error_modal_open_ticket:I = 0x7f120639

.field public static final dispatch_game_launch_failed_launch_target_not_found:I = 0x7f12063a

.field public static final display_activity:I = 0x7f12063b

.field public static final display_on_profile:I = 0x7f12063c

.field public static final display_option_always:I = 0x7f12063d

.field public static final display_option_never:I = 0x7f12063e

.field public static final display_option_only_while_speaking:I = 0x7f12063f

.field public static final display_silence_warning:I = 0x7f120640

.field public static final dm:I = 0x7f120641

.field public static final dm_search_placeholder:I = 0x7f120642

.field public static final dm_tooltip_a11y_label:I = 0x7f120643

.field public static final dm_verification_text_blocked:I = 0x7f120644

.field public static final done:I = 0x7f120645

.field public static final dont_show_again:I = 0x7f120646

.field public static final download:I = 0x7f120647

.field public static final download_app:I = 0x7f120648

.field public static final download_apps:I = 0x7f120649

.field public static final download_desktop_ptb_footer:I = 0x7f12064a

.field public static final download_desktop_stable_footer:I = 0x7f12064b

.field public static final download_desktop_title:I = 0x7f12064c

.field public static final download_failed:I = 0x7f12064d

.field public static final download_file_complete:I = 0x7f12064e

.field public static final download_mobile_title:I = 0x7f12064f

.field public static final duration_days:I = 0x7f120650

.field public static final duration_hours:I = 0x7f120651

.field public static final duration_hours_minutes_seconds:I = 0x7f120652

.field public static final duration_hours_short:I = 0x7f120653

.field public static final duration_mins:I = 0x7f120654

.field public static final duration_minutes:I = 0x7f120655

.field public static final duration_minutes_short:I = 0x7f120656

.field public static final duration_seconds:I = 0x7f120657

.field public static final duration_seconds_short:I = 0x7f120658

.field public static final early_supporter_tooltip:I = 0x7f120659

.field public static final echo_cancellation:I = 0x7f12065a

.field public static final edit:I = 0x7f12065b

.field public static final edit_category:I = 0x7f12065c

.field public static final edit_channel:I = 0x7f12065d

.field public static final edit_followed_news_body:I = 0x7f12065e

.field public static final edit_message:I = 0x7f12065f

.field public static final edit_note:I = 0x7f120660

.field public static final edit_overview:I = 0x7f120661

.field public static final edit_roles:I = 0x7f120662

.field public static final edit_textarea_help:I = 0x7f120663

.field public static final edit_user:I = 0x7f120664

.field public static final editing_message:I = 0x7f120665

.field public static final editing_with_attachment_error:I = 0x7f120666

.field public static final el:I = 0x7f120667

.field public static final elevated_permissions_sound_body:I = 0x7f120668

.field public static final elevated_permissions_sound_no_sound_button:I = 0x7f120669

.field public static final elevated_permissions_sound_request_access_button:I = 0x7f12066a

.field public static final elevated_permissions_sound_title:I = 0x7f12066b

.field public static final email:I = 0x7f12066c

.field public static final email_invalid:I = 0x7f12066d

.field public static final email_required:I = 0x7f12066e

.field public static final email_verification_instructions_body:I = 0x7f12066f

.field public static final email_verification_instructions_header:I = 0x7f120670

.field public static final embed_links:I = 0x7f120671

.field public static final embedded_activities_are_playing:I = 0x7f120672

.field public static final embedded_activities_channel_game_active:I = 0x7f120673

.field public static final embedded_activities_in_game:I = 0x7f120674

.field public static final embedded_activities_in_game_tooltip:I = 0x7f120675

.field public static final embedded_activities_invite_activity_ended:I = 0x7f120676

.field public static final embedded_activities_invite_game_footer_link_header:I = 0x7f120677

.field public static final embedded_activities_invite_in:I = 0x7f120678

.field public static final embedded_activities_invite_to_game:I = 0x7f120679

.field public static final embedded_activities_is_playing:I = 0x7f12067a

.field public static final embedded_activities_leave_game:I = 0x7f12067b

.field public static final embedded_activities_look_for_more:I = 0x7f12067c

.field public static final embedded_activities_marketing_tooltip_body:I = 0x7f12067d

.field public static final embedded_activities_marketing_tooltip_cta:I = 0x7f12067e

.field public static final embedded_activities_marketing_tooltip_header:I = 0x7f12067f

.field public static final embedded_activities_play_a_game:I = 0x7f120680

.field public static final embedded_activities_play_application:I = 0x7f120681

.field public static final embedded_activities_play_game:I = 0x7f120682

.field public static final embedded_activities_playing_application:I = 0x7f120683

.field public static final embedded_activities_playing_game:I = 0x7f120684

.field public static final embedded_activities_playing_in_guild:I = 0x7f120685

.field public static final embedded_activities_share_link_to_game:I = 0x7f120686

.field public static final emoji:I = 0x7f120687

.field public static final emoji_category_activity:I = 0x7f120688

.field public static final emoji_category_custom:I = 0x7f120689

.field public static final emoji_category_favorites:I = 0x7f12068a

.field public static final emoji_category_flags:I = 0x7f12068b

.field public static final emoji_category_food:I = 0x7f12068c

.field public static final emoji_category_nature:I = 0x7f12068d

.field public static final emoji_category_objects:I = 0x7f12068e

.field public static final emoji_category_people:I = 0x7f12068f

.field public static final emoji_category_recent:I = 0x7f120690

.field public static final emoji_category_symbols:I = 0x7f120691

.field public static final emoji_category_travel:I = 0x7f120692

.field public static final emoji_disabled_premium_tier_lost:I = 0x7f120693

.field public static final emoji_favorite_tooltip:I = 0x7f120694

.field public static final emoji_from_guild:I = 0x7f120695

.field public static final emoji_is_favorite_aria_label:I = 0x7f120696

.field public static final emoji_keyboard_toggle:I = 0x7f120697

.field public static final emoji_matching:I = 0x7f120698

.field public static final emoji_modifier_dark_skin_tone:I = 0x7f120699

.field public static final emoji_modifier_light_skin_tone:I = 0x7f12069a

.field public static final emoji_modifier_medium_dark_skin_tone:I = 0x7f12069b

.field public static final emoji_modifier_medium_light_skin_tone:I = 0x7f12069c

.field public static final emoji_modifier_medium_skin_tone:I = 0x7f12069d

.field public static final emoji_modifier_none:I = 0x7f12069e

.field public static final emoji_names_with_favorited:I = 0x7f12069f

.field public static final emoji_popout_current_guild_description:I = 0x7f1206a0

.field public static final emoji_popout_joined_guild_description:I = 0x7f1206a1

.field public static final emoji_popout_joined_guild_emoji_description:I = 0x7f1206a2

.field public static final emoji_popout_premium_cta:I = 0x7f1206a3

.field public static final emoji_popout_premium_current_guild_description:I = 0x7f1206a4

.field public static final emoji_popout_premium_joined_guild_description:I = 0x7f1206a5

.field public static final emoji_popout_premium_unjoined_discoverable_guild_description:I = 0x7f1206a6

.field public static final emoji_popout_premium_unjoined_private_guild_description:I = 0x7f1206a7

.field public static final emoji_popout_private_server:I = 0x7f1206a8

.field public static final emoji_popout_public_server:I = 0x7f1206a9

.field public static final emoji_popout_show_fewer:I = 0x7f1206aa

.field public static final emoji_popout_show_more_emojis:I = 0x7f1206ab

.field public static final emoji_popout_standard_emoji_description:I = 0x7f1206ac

.field public static final emoji_popout_unjoined_discoverable_guild_description:I = 0x7f1206ad

.field public static final emoji_popout_unjoined_guild_emoji_description:I = 0x7f1206ae

.field public static final emoji_popout_unjoined_private_guild_description:I = 0x7f1206af

.field public static final emoji_section:I = 0x7f1206b0

.field public static final emoji_slots_available:I = 0x7f1206b1

.field public static final emoji_too_big:I = 0x7f1206b2

.field public static final emoji_tooltip_click_cta:I = 0x7f1206b3

.field public static final emojis_title:I = 0x7f1206b4

.field public static final en:I = 0x7f1206b5

.field public static final en_gb:I = 0x7f1206b6

.field public static final en_us:I = 0x7f1206b7

.field public static final enable:I = 0x7f1206b8

.field public static final enable_community_button_text:I = 0x7f1206b9

.field public static final enable_community_modal_content_filter_label:I = 0x7f1206ba

.field public static final enable_community_modal_content_filter_met:I = 0x7f1206bb

.field public static final enable_community_modal_default_notifications_label:I = 0x7f1206bc

.field public static final enable_community_modal_default_notifications_label_mobile:I = 0x7f1206bd

.field public static final enable_community_modal_default_notifications_tooltip:I = 0x7f1206be

.field public static final enable_community_modal_everyone_role_permission_label:I = 0x7f1206bf

.field public static final enable_community_modal_everyone_role_permission_label_mobile:I = 0x7f1206c0

.field public static final enable_community_modal_everyone_role_permission_tooltip:I = 0x7f1206c1

.field public static final enable_community_modal_requirement_satisfied_tooltip:I = 0x7f1206c2

.field public static final enable_community_modal_step_1_body:I = 0x7f1206c3

.field public static final enable_community_modal_step_1_header:I = 0x7f1206c4

.field public static final enable_community_modal_step_1_title:I = 0x7f1206c5

.field public static final enable_community_modal_step_2_body:I = 0x7f1206c6

.field public static final enable_community_modal_step_2_header:I = 0x7f1206c7

.field public static final enable_community_modal_step_2_title:I = 0x7f1206c8

.field public static final enable_community_modal_step_3_body:I = 0x7f1206c9

.field public static final enable_community_modal_step_3_header:I = 0x7f1206ca

.field public static final enable_community_modal_step_3_title:I = 0x7f1206cb

.field public static final enable_community_modal_step_header:I = 0x7f1206cc

.field public static final enable_community_modal_title:I = 0x7f1206cd

.field public static final enable_community_modal_verification_level_help:I = 0x7f1206ce

.field public static final enable_community_modal_verification_level_label:I = 0x7f1206cf

.field public static final enable_community_modal_verification_level_met:I = 0x7f1206d0

.field public static final enable_ingame_overlay:I = 0x7f1206d1

.field public static final enable_noise_cancellation:I = 0x7f1206d2

.field public static final enable_notifications:I = 0x7f1206d3

.field public static final enable_permission:I = 0x7f1206d4

.field public static final enable_privacy_access:I = 0x7f1206d5

.field public static final enable_public_modal_content_filter_help:I = 0x7f1206d6

.field public static final enable_public_modal_create_channel:I = 0x7f1206d7

.field public static final enable_public_modal_rules_channel_help:I = 0x7f1206d8

.field public static final enable_public_modal_rules_channel_title:I = 0x7f1206d9

.field public static final enable_public_modal_verification_level_description:I = 0x7f1206da

.field public static final enable_streamer_mode_description:I = 0x7f1206db

.field public static final enable_streamer_mode_label:I = 0x7f1206dc

.field public static final enable_twitch_emoji_sync:I = 0x7f1206dd

.field public static final enter_email_body:I = 0x7f1206de

.field public static final enter_new_topic:I = 0x7f1206df

.field public static final enter_phone_description:I = 0x7f1206e0

.field public static final enter_phone_description_note:I = 0x7f1206e1

.field public static final enter_phone_or_email:I = 0x7f1206e2

.field public static final enter_phone_title:I = 0x7f1206e3

.field public static final error:I = 0x7f1206e4

.field public static final error_copying_image:I = 0x7f1206e5

.field public static final error_icon_content_description:I = 0x7f1206e6

.field public static final error_loading_sticker:I = 0x7f1206e7

.field public static final error_occurred_try_again:I = 0x7f1206e8

.field public static final error_saving_image:I = 0x7f1206e9

.field public static final errors_action_to_take:I = 0x7f1206ea

.field public static final errors_reload:I = 0x7f1206eb

.field public static final errors_restart_app:I = 0x7f1206ec

.field public static final errors_store_crash:I = 0x7f1206ed

.field public static final errors_unexpected_crash:I = 0x7f1206ee

.field public static final es_es:I = 0x7f1206ef

.field public static final everyone_popout_body:I = 0x7f1206f0

.field public static final everyone_popout_enter:I = 0x7f1206f1

.field public static final everyone_popout_esc:I = 0x7f1206f2

.field public static final everyone_popout_footer:I = 0x7f1206f3

.field public static final everyone_popout_send_now:I = 0x7f1206f4

.field public static final examples:I = 0x7f1206f5

.field public static final exit_full_screen:I = 0x7f1206f6

.field public static final expand_button_title:I = 0x7f12071f

.field public static final expand_buttons:I = 0x7f120720

.field public static final expanded:I = 0x7f120721

.field public static final experiment_feature_disabled:I = 0x7f120722

.field public static final experiment_mobile_only_user_header:I = 0x7f120723

.field public static final experiment_mobile_only_user_text1:I = 0x7f120724

.field public static final experimental_encoders:I = 0x7f120725

.field public static final expire_after:I = 0x7f120726

.field public static final expires_in:I = 0x7f120727

.field public static final explicit_content_filter_disabled:I = 0x7f120728

.field public static final explicit_content_filter_disabled_description:I = 0x7f120729

.field public static final explicit_content_filter_high:I = 0x7f12072a

.field public static final explicit_content_filter_high_description:I = 0x7f12072b

.field public static final explicit_content_filter_medium:I = 0x7f12072c

.field public static final explicit_content_filter_medium_description:I = 0x7f12072d

.field public static final exposed_dropdown_menu_content_description:I = 0x7f12072e

.field public static final expression_picker_emoji:I = 0x7f12072f

.field public static final expression_picker_gif:I = 0x7f120730

.field public static final expression_picker_intro_tooltip_action:I = 0x7f120731

.field public static final expression_picker_intro_tooltip_body:I = 0x7f120732

.field public static final expression_picker_intro_tooltip_header:I = 0x7f120733

.field public static final expression_picker_open_emoji_picker_a11y_label:I = 0x7f120734

.field public static final expression_picker_open_gif_picker_a11y_label:I = 0x7f120735

.field public static final expression_picker_open_sticker_picker_a11y_label:I = 0x7f120736

.field public static final expression_picker_sticker:I = 0x7f120737

.field public static final extra_emoji_count:I = 0x7f120738

.field public static final fab_transformation_scrim_behavior:I = 0x7f120739

.field public static final fab_transformation_sheet_behavior:I = 0x7f12073a

.field public static final facebook:I = 0x7f12073b

.field public static final failed:I = 0x7f12073c

.field public static final feedback_describe_issue:I = 0x7f120741

.field public static final feedback_issue_title:I = 0x7f120742

.field public static final feedback_modal_title:I = 0x7f120743

.field public static final feedback_need_more_help:I = 0x7f120744

.field public static final fi:I = 0x7f120745

.field public static final file_rate_kb:I = 0x7f120746

.field public static final file_rate_mb:I = 0x7f120747

.field public static final file_size_gb:I = 0x7f120748

.field public static final file_size_mb:I = 0x7f120749

.field public static final file_upload_limit_premium_tier_1:I = 0x7f12074a

.field public static final file_upload_limit_premium_tier_2:I = 0x7f12074b

.field public static final file_upload_limit_standard:I = 0x7f12074c

.field public static final files_permission_reason:I = 0x7f12074d

.field public static final filter:I = 0x7f12074e

.field public static final filter_mentions:I = 0x7f12074f

.field public static final filter_options:I = 0x7f120750

.field public static final first_week_survey_button:I = 0x7f120752

.field public static final first_week_survey_prompt:I = 0x7f120753

.field public static final flash_auto:I = 0x7f120754

.field public static final flash_off:I = 0x7f120755

.field public static final flash_on:I = 0x7f120756

.field public static final flash_torch:I = 0x7f120757

.field public static final focus_participant:I = 0x7f120758

.field public static final follow:I = 0x7f120759

.field public static final follow_modal_body:I = 0x7f12075a

.field public static final follow_modal_fail:I = 0x7f12075b

.field public static final follow_modal_hint:I = 0x7f12075c

.field public static final follow_modal_hint_no_perms:I = 0x7f12075d

.field public static final follow_modal_title:I = 0x7f12075e

.field public static final follow_modal_too_many_webhooks:I = 0x7f12075f

.field public static final follow_modal_warning:I = 0x7f120760

.field public static final follow_news_chat_input_message:I = 0x7f120761

.field public static final follow_news_chat_input_subtitle:I = 0x7f120762

.field public static final follow_success_modal_body:I = 0x7f120763

.field public static final follow_success_modal_header_01:I = 0x7f120764

.field public static final follow_success_modal_header_02:I = 0x7f120765

.field public static final follow_success_modal_header_03:I = 0x7f120766

.field public static final follow_success_modal_header_04:I = 0x7f120767

.field public static final follow_success_modal_header_05:I = 0x7f120768

.field public static final follow_success_modal_header_06:I = 0x7f120769

.field public static final follow_success_modal_header_07:I = 0x7f12076a

.field public static final follow_success_modal_header_08:I = 0x7f12076b

.field public static final follow_success_modal_header_09:I = 0x7f12076c

.field public static final follow_success_modal_header_10:I = 0x7f12076d

.field public static final follow_us_for_more_updates:I = 0x7f12076e

.field public static final follower_analytics:I = 0x7f12076f

.field public static final follower_analytics_header:I = 0x7f120770

.field public static final force_sync:I = 0x7f120771

.field public static final forgot_password:I = 0x7f120772

.field public static final form_checkbox_aec_dump:I = 0x7f120773

.field public static final form_checkbox_connection_log:I = 0x7f120774

.field public static final form_checkbox_debug_logging:I = 0x7f120775

.field public static final form_checkbox_qos:I = 0x7f120776

.field public static final form_description_audio_mode_android_call:I = 0x7f120777

.field public static final form_description_audio_mode_android_communication:I = 0x7f120778

.field public static final form_description_mobile_guild_notification_all_messages:I = 0x7f120779

.field public static final form_description_mobile_guild_notification_only_mentions:I = 0x7f12077a

.field public static final form_description_mobile_notification_muted:I = 0x7f12077b

.field public static final form_description_push_afk_timeout:I = 0x7f12077c

.field public static final form_description_tts:I = 0x7f12077d

.field public static final form_error_generic:I = 0x7f12077e

.field public static final form_help_aec_dump:I = 0x7f12077f

.field public static final form_help_afk_channel:I = 0x7f120780

.field public static final form_help_attenuation:I = 0x7f120781

.field public static final form_help_automatic_vad:I = 0x7f120782

.field public static final form_help_bitrate:I = 0x7f120783

.field public static final form_help_certified_voice_processing:I = 0x7f120784

.field public static final form_help_connection_log:I = 0x7f120785

.field public static final form_help_debug_logging:I = 0x7f120786

.field public static final form_help_default_notification_settings_mobile:I = 0x7f120787

.field public static final form_help_discoverable_change_time:I = 0x7f120788

.field public static final form_help_discovery_cover_image:I = 0x7f120789

.field public static final form_help_enable_discoverable:I = 0x7f12078a

.field public static final form_help_explicit_content_filter:I = 0x7f12078b

.field public static final form_help_hardware_h264:I = 0x7f12078c

.field public static final form_help_instant_invite_channel:I = 0x7f12078d

.field public static final form_help_last_seen:I = 0x7f12078e

.field public static final form_help_last_seen_with_roles:I = 0x7f12078f

.field public static final form_help_news:I = 0x7f120790

.field public static final form_help_nsfw:I = 0x7f120791

.field public static final form_help_open_h264:I = 0x7f120792

.field public static final form_help_qos:I = 0x7f120793

.field public static final form_help_server_banner:I = 0x7f120794

.field public static final form_help_server_description:I = 0x7f120795

.field public static final form_help_server_language:I = 0x7f120796

.field public static final form_help_slowmode:I = 0x7f120797

.field public static final form_help_system_channel:I = 0x7f120798

.field public static final form_help_system_channel_join_messages:I = 0x7f120799

.field public static final form_help_system_channel_premium_subscription_messages:I = 0x7f12079a

.field public static final form_help_temporary:I = 0x7f12079b

.field public static final form_help_user_limit:I = 0x7f12079c

.field public static final form_help_verification_level:I = 0x7f12079d

.field public static final form_help_voice_video_troubleshooting_guide:I = 0x7f12079e

.field public static final form_label_account_information:I = 0x7f12079f

.field public static final form_label_afk_channel:I = 0x7f1207a0

.field public static final form_label_afk_timeout:I = 0x7f1207a1

.field public static final form_label_all:I = 0x7f1207a2

.field public static final form_label_all_messages:I = 0x7f1207a3

.field public static final form_label_all_messages_short:I = 0x7f1207a4

.field public static final form_label_android_opensl:I = 0x7f1207a5

.field public static final form_label_android_opensl_default:I = 0x7f1207a6

.field public static final form_label_android_opensl_default_desc:I = 0x7f1207a7

.field public static final form_label_android_opensl_desc:I = 0x7f1207a8

.field public static final form_label_android_opensl_force_disabled:I = 0x7f1207a9

.field public static final form_label_android_opensl_force_disabled_desc:I = 0x7f1207aa

.field public static final form_label_android_opensl_force_enabled:I = 0x7f1207ab

.field public static final form_label_android_opensl_force_enabled_desc:I = 0x7f1207ac

.field public static final form_label_attenuation:I = 0x7f1207ad

.field public static final form_label_audio_mode_android:I = 0x7f1207ae

.field public static final form_label_audio_mode_android_call:I = 0x7f1207af

.field public static final form_label_audio_mode_android_communication:I = 0x7f1207b0

.field public static final form_label_automatic_vad:I = 0x7f1207b1

.field public static final form_label_automatic_vad_mobile:I = 0x7f1207b2

.field public static final form_label_avatar_size:I = 0x7f1207b3

.field public static final form_label_bitrate:I = 0x7f1207b4

.field public static final form_label_category_permissions:I = 0x7f1207b5

.field public static final form_label_channel_name:I = 0x7f1207b6

.field public static final form_label_channel_notifcation_settings:I = 0x7f1207b7

.field public static final form_label_channel_permissions:I = 0x7f1207b8

.field public static final form_label_channel_topic:I = 0x7f1207b9

.field public static final form_label_channel_voice_permissions:I = 0x7f1207ba

.field public static final form_label_current_password:I = 0x7f1207bb

.field public static final form_label_custom_twitch_emoticon:I = 0x7f1207bc

.field public static final form_label_debug:I = 0x7f1207bd

.field public static final form_label_default:I = 0x7f1207be

.field public static final form_label_default_notification_settings:I = 0x7f1207bf

.field public static final form_label_delete_message_history:I = 0x7f1207c0

.field public static final form_label_desktop_only:I = 0x7f1207c1

.field public static final form_label_disabled_for_everyone:I = 0x7f1207c2

.field public static final form_label_discovery_cover_image:I = 0x7f1207c3

.field public static final form_label_discovery_rules_channel:I = 0x7f1207c4

.field public static final form_label_display_names:I = 0x7f1207c5

.field public static final form_label_display_users:I = 0x7f1207c6

.field public static final form_label_email:I = 0x7f1207c7

.field public static final form_label_email_or_phone_number:I = 0x7f1207c8

.field public static final form_label_expire_grace_period:I = 0x7f1207c9

.field public static final form_label_explicit_content_filter:I = 0x7f1207ca

.field public static final form_label_hoist_description:I = 0x7f1207cb

.field public static final form_label_input:I = 0x7f1207cc

.field public static final form_label_input_device:I = 0x7f1207cd

.field public static final form_label_input_mode:I = 0x7f1207ce

.field public static final form_label_input_sensitivty:I = 0x7f1207cf

.field public static final form_label_input_volume:I = 0x7f1207d0

.field public static final form_label_instant_invite:I = 0x7f1207d1

.field public static final form_label_instant_invite_channel:I = 0x7f1207d2

.field public static final form_label_invite_link:I = 0x7f1207d3

.field public static final form_label_json_api:I = 0x7f1207d4

.field public static final form_label_last_seen:I = 0x7f1207d5

.field public static final form_label_max_age:I = 0x7f1207d6

.field public static final form_label_max_uses:I = 0x7f1207d7

.field public static final form_label_member_add:I = 0x7f1207d8

.field public static final form_label_membership_expire_behavior:I = 0x7f1207d9

.field public static final form_label_mentionable:I = 0x7f1207da

.field public static final form_label_mentionable_description:I = 0x7f1207db

.field public static final form_label_mentions:I = 0x7f1207dc

.field public static final form_label_mobile_category_muted:I = 0x7f1207dd

.field public static final form_label_mobile_category_muted_until:I = 0x7f1207de

.field public static final form_label_mobile_channel_muted:I = 0x7f1207df

.field public static final form_label_mobile_channel_muted_until:I = 0x7f1207e0

.field public static final form_label_mobile_channel_override_guild_message_notification:I = 0x7f1207e1

.field public static final form_label_mobile_channel_override_guild_muted:I = 0x7f1207e2

.field public static final form_label_mobile_channel_override_mute:I = 0x7f1207e3

.field public static final form_label_mobile_dm_muted:I = 0x7f1207e4

.field public static final form_label_mobile_dm_muted_until:I = 0x7f1207e5

.field public static final form_label_mobile_notifications_behavior:I = 0x7f1207e6

.field public static final form_label_mobile_notifications_blink:I = 0x7f1207e7

.field public static final form_label_mobile_notifications_call_desc:I = 0x7f1207e8

.field public static final form_label_mobile_notifications_inapp_desc:I = 0x7f1207e9

.field public static final form_label_mobile_notifications_inapp_label:I = 0x7f1207ea

.field public static final form_label_mobile_notifications_label:I = 0x7f1207eb

.field public static final form_label_mobile_notifications_os_control_jump:I = 0x7f1207ec

.field public static final form_label_mobile_notifications_os_control_jump_help:I = 0x7f1207ed

.field public static final form_label_mobile_notifications_sound_disable:I = 0x7f1207ee

.field public static final form_label_mobile_notifications_stream_desc:I = 0x7f1207ef

.field public static final form_label_mobile_notifications_system_desc:I = 0x7f1207f0

.field public static final form_label_mobile_notifications_system_hint:I = 0x7f1207f1

.field public static final form_label_mobile_notifications_system_label:I = 0x7f1207f2

.field public static final form_label_mobile_notifications_user_label:I = 0x7f1207f3

.field public static final form_label_mobile_notifications_vibrations:I = 0x7f1207f4

.field public static final form_label_mobile_notifications_wake_device:I = 0x7f1207f5

.field public static final form_label_mobile_push_notifications:I = 0x7f1207f6

.field public static final form_label_mobile_server_muted:I = 0x7f1207f7

.field public static final form_label_mobile_server_muted_until:I = 0x7f1207f8

.field public static final form_label_mobile_user_notifications_hint:I = 0x7f1207f9

.field public static final form_label_mobile_user_notifications_label:I = 0x7f1207fa

.field public static final form_label_mute_server:I = 0x7f1207fb

.field public static final form_label_mute_server_description:I = 0x7f1207fc

.field public static final form_label_muted:I = 0x7f1207fd

.field public static final form_label_new_password:I = 0x7f1207fe

.field public static final form_label_news_channel:I = 0x7f1207ff

.field public static final form_label_nothing:I = 0x7f120800

.field public static final form_label_notification_frequency:I = 0x7f120801

.field public static final form_label_notification_position:I = 0x7f120802

.field public static final form_label_nsfw_channel:I = 0x7f120803

.field public static final form_label_off:I = 0x7f120804

.field public static final form_label_only_mentions:I = 0x7f120805

.field public static final form_label_only_mentions_short:I = 0x7f120806

.field public static final form_label_output:I = 0x7f120807

.field public static final form_label_output_device:I = 0x7f120808

.field public static final form_label_output_volume:I = 0x7f120809

.field public static final form_label_overlay_chat_opacity:I = 0x7f12080a

.field public static final form_label_overlay_text_chat_notifications:I = 0x7f12080b

.field public static final form_label_password:I = 0x7f12080c

.field public static final form_label_phone_number:I = 0x7f12080d

.field public static final form_label_premade_widget:I = 0x7f12080e

.field public static final form_label_push_afk_timeout:I = 0x7f12080f

.field public static final form_label_qos:I = 0x7f120810

.field public static final form_label_reason_ban:I = 0x7f120811

.field public static final form_label_reason_kick:I = 0x7f120812

.field public static final form_label_report_reason:I = 0x7f120813

.field public static final form_label_role_add:I = 0x7f120814

.field public static final form_label_role_color:I = 0x7f120815

.field public static final form_label_role_enter_name:I = 0x7f120816

.field public static final form_label_role_name:I = 0x7f120817

.field public static final form_label_role_settings:I = 0x7f120818

.field public static final form_label_roles_pro_tip:I = 0x7f120819

.field public static final form_label_roles_pro_tip_description:I = 0x7f12081a

.field public static final form_label_select_channel:I = 0x7f12081b

.field public static final form_label_send_to:I = 0x7f12081c

.field public static final form_label_sensitivty:I = 0x7f12081d

.field public static final form_label_server_banner:I = 0x7f12081e

.field public static final form_label_server_description:I = 0x7f12081f

.field public static final form_label_server_id:I = 0x7f120820

.field public static final form_label_server_language:I = 0x7f120821

.field public static final form_label_server_name:I = 0x7f120822

.field public static final form_label_server_notification_settings:I = 0x7f120823

.field public static final form_label_server_region:I = 0x7f120824

.field public static final form_label_shortcut:I = 0x7f120825

.field public static final form_label_slowmode:I = 0x7f120826

.field public static final form_label_slowmode_cooldown:I = 0x7f120827

.field public static final form_label_slowmode_off:I = 0x7f120828

.field public static final form_label_stream_volume:I = 0x7f120829

.field public static final form_label_sub_expire_behavior:I = 0x7f12082a

.field public static final form_label_subsystem:I = 0x7f12082b

.field public static final form_label_suppress_everyone:I = 0x7f12082c

.field public static final form_label_suppress_roles:I = 0x7f12082d

.field public static final form_label_synced_members:I = 0x7f12082e

.field public static final form_label_synced_role:I = 0x7f12082f

.field public static final form_label_synced_subs:I = 0x7f120830

.field public static final form_label_system_channel:I = 0x7f120831

.field public static final form_label_system_channel_settings:I = 0x7f120832

.field public static final form_label_temporary:I = 0x7f120833

.field public static final form_label_test_microphone:I = 0x7f120834

.field public static final form_label_tts:I = 0x7f120835

.field public static final form_label_tts_notifications:I = 0x7f120836

.field public static final form_label_unmute_server:I = 0x7f120837

.field public static final form_label_use_rich_chat_box:I = 0x7f120838

.field public static final form_label_user_limit:I = 0x7f120839

.field public static final form_label_username:I = 0x7f12083a

.field public static final form_label_username_mobile:I = 0x7f12083b

.field public static final form_label_verification_level:I = 0x7f12083c

.field public static final form_label_video_device:I = 0x7f12083d

.field public static final form_label_video_preview:I = 0x7f12083e

.field public static final form_label_voice_diagnostics:I = 0x7f12083f

.field public static final form_label_voice_processing:I = 0x7f120840

.field public static final form_label_volume:I = 0x7f120841

.field public static final form_placeholder_server_name:I = 0x7f120842

.field public static final form_placeholder_username:I = 0x7f120843

.field public static final form_report_help_text:I = 0x7f120844

.field public static final form_warning_input_sensitivty:I = 0x7f120845

.field public static final form_warning_video_preview:I = 0x7f120846

.field public static final fr:I = 0x7f120847

.field public static final friend_has_been_deleted:I = 0x7f120848

.field public static final friend_permitted_source:I = 0x7f120849

.field public static final friend_permitted_source_all:I = 0x7f12084a

.field public static final friend_permitted_source_mutual_friends:I = 0x7f12084b

.field public static final friend_permitted_source_mutual_guilds:I = 0x7f12084c

.field public static final friend_request_accept:I = 0x7f12084d

.field public static final friend_request_cancel:I = 0x7f12084e

.field public static final friend_request_cancelled:I = 0x7f12084f

.field public static final friend_request_failed_header:I = 0x7f120850

.field public static final friend_request_ignore:I = 0x7f120851

.field public static final friend_request_ignored:I = 0x7f120852

.field public static final friend_request_rate_limited_body:I = 0x7f120853

.field public static final friend_request_rate_limited_button:I = 0x7f120854

.field public static final friend_request_rate_limited_header:I = 0x7f120855

.field public static final friend_request_requires_email_validation_body:I = 0x7f120856

.field public static final friend_request_requires_email_validation_button:I = 0x7f120857

.field public static final friend_request_requires_email_validation_header:I = 0x7f120858

.field public static final friend_request_sent:I = 0x7f120859

.field public static final friends:I = 0x7f12085a

.field public static final friends_all_header:I = 0x7f12085b

.field public static final friends_blocked_header:I = 0x7f12085c

.field public static final friends_column_name:I = 0x7f12085d

.field public static final friends_column_status:I = 0x7f12085e

.field public static final friends_empty_state_all:I = 0x7f12085f

.field public static final friends_empty_state_all_body:I = 0x7f120860

.field public static final friends_empty_state_all_cta:I = 0x7f120861

.field public static final friends_empty_state_all_header:I = 0x7f120862

.field public static final friends_empty_state_blocked:I = 0x7f120863

.field public static final friends_empty_state_copy:I = 0x7f120864

.field public static final friends_empty_state_online:I = 0x7f120865

.field public static final friends_empty_state_pending:I = 0x7f120866

.field public static final friends_empty_state_subtitle:I = 0x7f120867

.field public static final friends_offline_header:I = 0x7f120868

.field public static final friends_online_header:I = 0x7f120869

.field public static final friends_pending_header:I = 0x7f12086a

.field public static final friends_pending_request_expand:I = 0x7f12086b

.field public static final friends_pending_request_expand_collapse:I = 0x7f12086c

.field public static final friends_pending_request_header:I = 0x7f12086d

.field public static final friends_request_status_incoming:I = 0x7f12086e

.field public static final friends_request_status_outgoing:I = 0x7f12086f

.field public static final friends_row_action_button_accessibility_label_accept:I = 0x7f120870

.field public static final friends_row_action_button_accessibility_label_call:I = 0x7f120871

.field public static final friends_row_action_button_accessibility_label_cancel:I = 0x7f120872

.field public static final friends_row_action_button_accessibility_label_decline:I = 0x7f120873

.field public static final friends_row_action_button_accessibility_label_message:I = 0x7f120874

.field public static final friends_section_add_friend:I = 0x7f120875

.field public static final friends_section_all:I = 0x7f120876

.field public static final friends_section_online:I = 0x7f120877

.field public static final friends_section_pending:I = 0x7f120878

.field public static final friends_share:I = 0x7f120879

.field public static final friends_share_sheet_scanning_text_bottom:I = 0x7f12087a

.field public static final friends_share_sheet_scanning_text_top:I = 0x7f12087b

.field public static final friends_share_tabbar_title:I = 0x7f12087c

.field public static final full:I = 0x7f12087d

.field public static final full_screen:I = 0x7f12087e

.field public static final game_action_button_add_to_library:I = 0x7f12087f

.field public static final game_action_button_cannot_install:I = 0x7f120880

.field public static final game_action_button_downloading:I = 0x7f120881

.field public static final game_action_button_game_not_detected:I = 0x7f120882

.field public static final game_action_button_install:I = 0x7f120883

.field public static final game_action_button_locate:I = 0x7f120884

.field public static final game_action_button_login_to_buy:I = 0x7f120885

.field public static final game_action_button_now_playing:I = 0x7f120886

.field public static final game_action_button_paused:I = 0x7f120887

.field public static final game_action_button_play:I = 0x7f120888

.field public static final game_action_button_play_disabled_desktop_app:I = 0x7f120889

.field public static final game_action_button_preorder_wait:I = 0x7f12088a

.field public static final game_action_button_preorder_wait_tooltip:I = 0x7f12088b

.field public static final game_action_button_queued:I = 0x7f12088c

.field public static final game_action_button_restricted_in_region:I = 0x7f12088d

.field public static final game_action_button_unavailable:I = 0x7f12088e

.field public static final game_action_button_unavailable_tooltip:I = 0x7f12088f

.field public static final game_action_button_uninstalling_1:I = 0x7f120890

.field public static final game_action_button_uninstalling_10:I = 0x7f120891

.field public static final game_action_button_uninstalling_2:I = 0x7f120892

.field public static final game_action_button_uninstalling_3:I = 0x7f120893

.field public static final game_action_button_uninstalling_4:I = 0x7f120894

.field public static final game_action_button_uninstalling_5:I = 0x7f120895

.field public static final game_action_button_uninstalling_6:I = 0x7f120896

.field public static final game_action_button_uninstalling_7:I = 0x7f120897

.field public static final game_action_button_uninstalling_8:I = 0x7f120898

.field public static final game_action_button_uninstalling_9:I = 0x7f120899

.field public static final game_action_button_update:I = 0x7f12089a

.field public static final game_action_button_view_in_store:I = 0x7f12089b

.field public static final game_activity:I = 0x7f12089c

.field public static final game_detected:I = 0x7f12089d

.field public static final game_detection_modal_info_android:I = 0x7f12089e

.field public static final game_detection_open_settings:I = 0x7f12089f

.field public static final game_detection_service:I = 0x7f1208a0

.field public static final game_feed_activity_action_play:I = 0x7f1208a1

.field public static final game_feed_activity_playing_xbox:I = 0x7f1208a2

.field public static final game_feed_activity_streaming_twitch:I = 0x7f1208a3

.field public static final game_feed_current_header_title:I = 0x7f1208a4

.field public static final game_feed_unknown_player:I = 0x7f1208a5

.field public static final game_feed_user_played_days_ago:I = 0x7f1208a6

.field public static final game_feed_user_played_hours_ago:I = 0x7f1208a7

.field public static final game_feed_user_played_minutes_ago:I = 0x7f1208a8

.field public static final game_feed_user_played_months_ago:I = 0x7f1208a9

.field public static final game_feed_user_played_weeks_ago:I = 0x7f1208aa

.field public static final game_feed_user_played_years_ago:I = 0x7f1208ab

.field public static final game_feed_user_playing_for_days:I = 0x7f1208ac

.field public static final game_feed_user_playing_for_hours:I = 0x7f1208ad

.field public static final game_feed_user_playing_for_minutes:I = 0x7f1208ae

.field public static final game_feed_user_playing_just_ended:I = 0x7f1208af

.field public static final game_feed_user_playing_just_started:I = 0x7f1208b0

.field public static final game_launch_failed_launch_target_not_found:I = 0x7f1208b1

.field public static final game_library_last_played_days:I = 0x7f1208b2

.field public static final game_library_last_played_hours:I = 0x7f1208b3

.field public static final game_library_last_played_just_now:I = 0x7f1208b4

.field public static final game_library_last_played_minutes:I = 0x7f1208b5

.field public static final game_library_last_played_months:I = 0x7f1208b6

.field public static final game_library_last_played_none:I = 0x7f1208b7

.field public static final game_library_last_played_playing_now:I = 0x7f1208b8

.field public static final game_library_last_played_weeks:I = 0x7f1208b9

.field public static final game_library_last_played_years:I = 0x7f1208ba

.field public static final game_library_list_header_last_played:I = 0x7f1208bb

.field public static final game_library_list_header_name:I = 0x7f1208bc

.field public static final game_library_list_header_platform:I = 0x7f1208bd

.field public static final game_library_new:I = 0x7f1208be

.field public static final game_library_not_applicable:I = 0x7f1208bf

.field public static final game_library_notification_game_installed_body:I = 0x7f1208c0

.field public static final game_library_notification_game_installed_title:I = 0x7f1208c1

.field public static final game_library_overlay_disabled_tooltip:I = 0x7f1208c2

.field public static final game_library_private_tooltip:I = 0x7f1208c3

.field public static final game_library_time_played_hours:I = 0x7f1208c4

.field public static final game_library_time_played_minutes:I = 0x7f1208c5

.field public static final game_library_time_played_none:I = 0x7f1208c6

.field public static final game_library_time_played_seconds:I = 0x7f1208c7

.field public static final game_library_updates_action_move_up:I = 0x7f1208c8

.field public static final game_library_updates_action_pause:I = 0x7f1208c9

.field public static final game_library_updates_action_remove:I = 0x7f1208ca

.field public static final game_library_updates_action_resume:I = 0x7f1208cb

.field public static final game_library_updates_header_disk:I = 0x7f1208cc

.field public static final game_library_updates_header_network:I = 0x7f1208cd

.field public static final game_library_updates_installing:I = 0x7f1208ce

.field public static final game_library_updates_installing_hours:I = 0x7f1208cf

.field public static final game_library_updates_installing_minutes:I = 0x7f1208d0

.field public static final game_library_updates_installing_seconds:I = 0x7f1208d1

.field public static final game_library_updates_progress_allocating_disk:I = 0x7f1208d2

.field public static final game_library_updates_progress_finalizing:I = 0x7f1208d3

.field public static final game_library_updates_progress_finished:I = 0x7f1208d4

.field public static final game_library_updates_progress_paused:I = 0x7f1208d5

.field public static final game_library_updates_progress_paused_no_transition:I = 0x7f1208d6

.field public static final game_library_updates_progress_pausing:I = 0x7f1208d7

.field public static final game_library_updates_progress_planning:I = 0x7f1208d8

.field public static final game_library_updates_progress_post_install_scripts:I = 0x7f1208d9

.field public static final game_library_updates_progress_queued:I = 0x7f1208da

.field public static final game_library_updates_progress_queued_no_transition:I = 0x7f1208db

.field public static final game_library_updates_progress_repairing:I = 0x7f1208dc

.field public static final game_library_updates_progress_verifying:I = 0x7f1208dd

.field public static final game_library_updates_progress_waiting_for_another:I = 0x7f1208de

.field public static final game_library_updates_progress_waiting_for_another_no_transition:I = 0x7f1208df

.field public static final game_library_updates_updating:I = 0x7f1208e0

.field public static final game_library_updates_updating_updating_hours:I = 0x7f1208e1

.field public static final game_library_updates_updating_updating_minutes:I = 0x7f1208e2

.field public static final game_library_updates_updating_updating_seconds:I = 0x7f1208e3

.field public static final game_popout_follow:I = 0x7f1208e4

.field public static final game_popout_nitro_upsell:I = 0x7f1208e5

.field public static final game_popout_view_server:I = 0x7f1208e6

.field public static final general_permissions:I = 0x7f1208e8

.field public static final generate_a_new_link:I = 0x7f1208e9

.field public static final generic_actions_menu_label:I = 0x7f1208ea

.field public static final get_started:I = 0x7f1208eb

.field public static final gif:I = 0x7f1208ec

.field public static final gif_auto_play_label:I = 0x7f1208ed

.field public static final gif_auto_play_label_mobile:I = 0x7f1208ee

.field public static final gif_button_label:I = 0x7f1208ef

.field public static final gif_picker_enter_search:I = 0x7f1208f0

.field public static final gif_picker_favorites:I = 0x7f1208f1

.field public static final gif_picker_related_search:I = 0x7f1208f2

.field public static final gif_picker_result_type_trending_gifs:I = 0x7f1208f3

.field public static final gif_tooltip_add_to_favorites:I = 0x7f1208f4

.field public static final gif_tooltip_favorited_picker_button:I = 0x7f1208f5

.field public static final gif_tooltip_remove_from_favorites:I = 0x7f1208f6

.field public static final gift_code_auth_accept:I = 0x7f1208f7

.field public static final gift_code_auth_accepting:I = 0x7f1208f8

.field public static final gift_code_auth_check_verification_again:I = 0x7f1208f9

.field public static final gift_code_auth_continue_in_browser:I = 0x7f1208fa

.field public static final gift_code_auth_fetching_user:I = 0x7f1208fb

.field public static final gift_code_auth_gifted:I = 0x7f1208fc

.field public static final gift_code_auth_gifted_by:I = 0x7f1208fd

.field public static final gift_code_auth_gifted_subscription_monthly:I = 0x7f1208fe

.field public static final gift_code_auth_gifted_subscription_yearly:I = 0x7f1208ff

.field public static final gift_code_auth_help_text_claimed:I = 0x7f120900

.field public static final gift_code_auth_help_text_owned:I = 0x7f120901

.field public static final gift_code_auth_help_text_verification_required:I = 0x7f120902

.field public static final gift_code_auth_invalid_body:I = 0x7f120903

.field public static final gift_code_auth_invalid_tip:I = 0x7f120904

.field public static final gift_code_auth_invalid_title:I = 0x7f120905

.field public static final gift_code_auth_logged_in_as:I = 0x7f120906

.field public static final gift_code_auth_resolving:I = 0x7f120907

.field public static final gift_code_auth_verification_sent:I = 0x7f120908

.field public static final gift_code_auth_verification_title:I = 0x7f120909

.field public static final gift_code_cannot_accept_body_ios:I = 0x7f12090a

.field public static final gift_code_hint:I = 0x7f12090b

.field public static final gift_confirmation_body_claimed:I = 0x7f12090c

.field public static final gift_confirmation_body_confirm:I = 0x7f12090d

.field public static final gift_confirmation_body_confirm_nitro:I = 0x7f12090e

.field public static final gift_confirmation_body_confirm_nitro_disclaimer:I = 0x7f12090f

.field public static final gift_confirmation_body_error_invoice_open:I = 0x7f120910

.field public static final gift_confirmation_body_error_nitro_upgrade_downgrade:I = 0x7f120911

.field public static final gift_confirmation_body_error_subscription_managed:I = 0x7f120912

.field public static final gift_confirmation_body_invalid:I = 0x7f120913

.field public static final gift_confirmation_body_owned:I = 0x7f120914

.field public static final gift_confirmation_body_self_gift_no_payment:I = 0x7f120915

.field public static final gift_confirmation_body_subscription_monthly_confirm:I = 0x7f120916

.field public static final gift_confirmation_body_subscription_yearly_confirm:I = 0x7f120917

.field public static final gift_confirmation_body_success:I = 0x7f120918

.field public static final gift_confirmation_body_success_generic_subscription_monthly:I = 0x7f120919

.field public static final gift_confirmation_body_success_generic_subscription_yearly:I = 0x7f12091a

.field public static final gift_confirmation_body_success_mobile:I = 0x7f12091b

.field public static final gift_confirmation_body_success_nitro_classic_mobile:I = 0x7f12091c

.field public static final gift_confirmation_body_success_nitro_mobile:I = 0x7f12091d

.field public static final gift_confirmation_body_success_premium_tier_1_monthly:I = 0x7f12091e

.field public static final gift_confirmation_body_success_premium_tier_1_yearly:I = 0x7f12091f

.field public static final gift_confirmation_body_success_premium_tier_2_monthly:I = 0x7f120920

.field public static final gift_confirmation_body_success_premium_tier_2_yearly:I = 0x7f120921

.field public static final gift_confirmation_body_unknown_error:I = 0x7f120922

.field public static final gift_confirmation_button_confirm:I = 0x7f120923

.field public static final gift_confirmation_button_confirm_mobile:I = 0x7f120924

.field public static final gift_confirmation_button_confirm_subscription:I = 0x7f120925

.field public static final gift_confirmation_button_fail:I = 0x7f120926

.field public static final gift_confirmation_button_go_to_library:I = 0x7f120927

.field public static final gift_confirmation_button_noice:I = 0x7f120928

.field public static final gift_confirmation_button_subscription_success:I = 0x7f120929

.field public static final gift_confirmation_button_success_mobile:I = 0x7f12092a

.field public static final gift_confirmation_header_confirm:I = 0x7f12092b

.field public static final gift_confirmation_header_confirm_nitro:I = 0x7f12092c

.field public static final gift_confirmation_header_fail:I = 0x7f12092d

.field public static final gift_confirmation_header_success:I = 0x7f12092e

.field public static final gift_confirmation_header_success_nitro:I = 0x7f12092f

.field public static final gift_confirmation_nitro_time_frame_months:I = 0x7f120930

.field public static final gift_confirmation_nitro_time_frame_years:I = 0x7f120931

.field public static final gift_embed_body_claimed_other:I = 0x7f120932

.field public static final gift_embed_body_claimed_self:I = 0x7f120933

.field public static final gift_embed_body_claimed_self_mobile:I = 0x7f120934

.field public static final gift_embed_body_claimed_self_subscription:I = 0x7f120935

.field public static final gift_embed_body_default:I = 0x7f120936

.field public static final gift_embed_body_giveaway:I = 0x7f120937

.field public static final gift_embed_body_no_user_default:I = 0x7f120938

.field public static final gift_embed_body_no_user_giveaway:I = 0x7f120939

.field public static final gift_embed_body_owned:I = 0x7f12093a

.field public static final gift_embed_body_owned_mobile:I = 0x7f12093b

.field public static final gift_embed_body_requires_verification:I = 0x7f12093c

.field public static final gift_embed_body_requires_verification_mobile:I = 0x7f12093d

.field public static final gift_embed_body_self:I = 0x7f12093e

.field public static final gift_embed_body_self_not_redeemable:I = 0x7f12093f

.field public static final gift_embed_body_subscription_already_active:I = 0x7f120940

.field public static final gift_embed_body_subscription_default_months:I = 0x7f120941

.field public static final gift_embed_body_subscription_default_years:I = 0x7f120942

.field public static final gift_embed_body_subscription_giveaway:I = 0x7f120943

.field public static final gift_embed_body_subscription_mismatch:I = 0x7f120944

.field public static final gift_embed_body_subscription_no_user_default_months:I = 0x7f120945

.field public static final gift_embed_body_subscription_no_user_default_years:I = 0x7f120946

.field public static final gift_embed_body_subscription_no_user_giveaway:I = 0x7f120947

.field public static final gift_embed_button_accept:I = 0x7f120948

.field public static final gift_embed_button_cant_accept:I = 0x7f120949

.field public static final gift_embed_button_claimed:I = 0x7f12094a

.field public static final gift_embed_button_claiming:I = 0x7f12094b

.field public static final gift_embed_button_owned:I = 0x7f12094c

.field public static final gift_embed_copies_left:I = 0x7f12094d

.field public static final gift_embed_expiration:I = 0x7f12094e

.field public static final gift_embed_header_owner_invalid:I = 0x7f12094f

.field public static final gift_embed_header_receiver_invalid:I = 0x7f120950

.field public static final gift_embed_info_owner_invalid:I = 0x7f120951

.field public static final gift_embed_info_receiver_invalid:I = 0x7f120952

.field public static final gift_embed_invalid:I = 0x7f120953

.field public static final gift_embed_invalid_tagline_other:I = 0x7f120954

.field public static final gift_embed_invalid_tagline_self:I = 0x7f120955

.field public static final gift_embed_invalid_title_other:I = 0x7f120956

.field public static final gift_embed_invalid_title_self:I = 0x7f120957

.field public static final gift_embed_resolving:I = 0x7f120958

.field public static final gift_embed_subscriptions_left:I = 0x7f120959

.field public static final gift_embed_title:I = 0x7f12095a

.field public static final gift_embed_title_self:I = 0x7f12095b

.field public static final gift_embed_title_subscription:I = 0x7f12095c

.field public static final gift_embed_title_subscription_self:I = 0x7f12095d

.field public static final gift_inventory:I = 0x7f12095e

.field public static final gift_inventory_copies:I = 0x7f12095f

.field public static final gift_inventory_expires_in:I = 0x7f120960

.field public static final gift_inventory_expires_in_mobile:I = 0x7f120961

.field public static final gift_inventory_generate_help:I = 0x7f120962

.field public static final gift_inventory_generate_link:I = 0x7f120963

.field public static final gift_inventory_generate_link_ios:I = 0x7f120964

.field public static final gift_inventory_gifts_you_purchased:I = 0x7f120965

.field public static final gift_inventory_hidden:I = 0x7f120966

.field public static final gift_inventory_no_gifts:I = 0x7f120967

.field public static final gift_inventory_no_gifts_subtext:I = 0x7f120968

.field public static final gift_inventory_no_gifts_subtext_mobile:I = 0x7f120969

.field public static final gift_inventory_redeem_codes:I = 0x7f12096a

.field public static final gift_inventory_select_nitro_gift:I = 0x7f12096b

.field public static final gift_inventory_subscription_months:I = 0x7f12096c

.field public static final gift_inventory_subscription_years:I = 0x7f12096d

.field public static final gift_inventory_your_gifts:I = 0x7f12096e

.field public static final go_live_hardware_acceleration_unavailable:I = 0x7f12096f

.field public static final go_live_hey:I = 0x7f120970

.field public static final go_live_listen:I = 0x7f120971

.field public static final go_live_look:I = 0x7f120972

.field public static final go_live_modal_application_form_title:I = 0x7f120973

.field public static final go_live_modal_applications:I = 0x7f120974

.field public static final go_live_modal_cta:I = 0x7f120975

.field public static final go_live_modal_current_channel_form_title:I = 0x7f120976

.field public static final go_live_modal_description:I = 0x7f120977

.field public static final go_live_modal_description_generic:I = 0x7f120978

.field public static final go_live_modal_description_select_source:I = 0x7f120979

.field public static final go_live_modal_guild_form_title:I = 0x7f12097a

.field public static final go_live_modal_overflow_four_or_more_users:I = 0x7f12097b

.field public static final go_live_modal_overflow_three_users:I = 0x7f12097c

.field public static final go_live_modal_overflow_two_users:I = 0x7f12097d

.field public static final go_live_modal_screens:I = 0x7f12097e

.field public static final go_live_modal_select_channel_form_title:I = 0x7f12097f

.field public static final go_live_modal_select_guild_form_title:I = 0x7f120980

.field public static final go_live_modal_title:I = 0x7f120981

.field public static final go_live_private_channels_tooltip_body:I = 0x7f120982

.field public static final go_live_private_channels_tooltip_body_no_format:I = 0x7f120983

.field public static final go_live_private_channels_tooltip_cta:I = 0x7f120984

.field public static final go_live_screenshare_no_sound:I = 0x7f120985

.field public static final go_live_screenshare_update_for_soundshare:I = 0x7f120986

.field public static final go_live_share_screen:I = 0x7f120987

.field public static final go_live_tile_screen:I = 0x7f120988

.field public static final go_live_user_playing:I = 0x7f120989

.field public static final go_live_video_drivers_outdated:I = 0x7f12098a

.field public static final go_live_watching_user:I = 0x7f12098b

.field public static final grant_temporary_membership:I = 0x7f120990

.field public static final green:I = 0x7f120991

.field public static final grey:I = 0x7f120992

.field public static final group_dm:I = 0x7f120993

.field public static final group_dm_add_friends:I = 0x7f120994

.field public static final group_dm_header:I = 0x7f120995

.field public static final group_dm_invite_confirm:I = 0x7f120996

.field public static final group_dm_invite_confirm_button:I = 0x7f120997

.field public static final group_dm_invite_confirm_description:I = 0x7f120998

.field public static final group_dm_invite_empty:I = 0x7f120999

.field public static final group_dm_invite_full_main:I = 0x7f12099a

.field public static final group_dm_invite_full_sub:I = 0x7f12099b

.field public static final group_dm_invite_full_sub2:I = 0x7f12099c

.field public static final group_dm_invite_link_create:I = 0x7f12099d

.field public static final group_dm_invite_link_example:I = 0x7f12099e

.field public static final group_dm_invite_link_title:I = 0x7f12099f

.field public static final group_dm_invite_no_friends:I = 0x7f1209a0

.field public static final group_dm_invite_not_friends:I = 0x7f1209a1

.field public static final group_dm_invite_remaining:I = 0x7f1209a2

.field public static final group_dm_invite_select_existing:I = 0x7f1209a3

.field public static final group_dm_invite_to:I = 0x7f1209a4

.field public static final group_dm_invite_unselect_users:I = 0x7f1209a5

.field public static final group_dm_invite_will_fill_mobile:I = 0x7f1209a6

.field public static final group_dm_invite_with_name:I = 0x7f1209a7

.field public static final group_dm_search_placeholder:I = 0x7f1209a8

.field public static final group_dm_settings:I = 0x7f1209a9

.field public static final group_message_a11y_label:I = 0x7f1209aa

.field public static final group_message_a11y_label_with_unreads:I = 0x7f1209ab

.field public static final group_name:I = 0x7f1209ac

.field public static final group_owner:I = 0x7f1209ad

.field public static final groups:I = 0x7f1209ae

.field public static final guest_lurker_mode_chat_input_body:I = 0x7f1209af

.field public static final guest_lurker_mode_chat_input_header:I = 0x7f1209b0

.field public static final guild_actions_menu_label:I = 0x7f1209b1

.field public static final guild_analyics_developers_button:I = 0x7f1209b2

.field public static final guild_analytics_description:I = 0x7f1209b3

.field public static final guild_analytics_developers_cta:I = 0x7f1209b4

.field public static final guild_analytics_error_guild_size:I = 0x7f1209b5

.field public static final guild_analytics_error_message:I = 0x7f1209b6

.field public static final guild_analytics_metrics_last_week:I = 0x7f1209b7

.field public static final guild_create_description:I = 0x7f1209b8

.field public static final guild_create_title:I = 0x7f1209b9

.field public static final guild_create_upload_icon_button_text:I = 0x7f1209ba

.field public static final guild_create_upload_icon_label:I = 0x7f1209bb

.field public static final guild_create_upload_icon_recommended_size_label:I = 0x7f1209bc

.field public static final guild_discovery_all_category_filter:I = 0x7f1209bd

.field public static final guild_discovery_category_footer_title:I = 0x7f1209be

.field public static final guild_discovery_category_search_placeholder:I = 0x7f1209bf

.field public static final guild_discovery_category_title:I = 0x7f1209c0

.field public static final guild_discovery_covid_body:I = 0x7f1209c1

.field public static final guild_discovery_covid_button:I = 0x7f1209c2

.field public static final guild_discovery_covid_title:I = 0x7f1209c3

.field public static final guild_discovery_emojis_tooltip:I = 0x7f1209c4

.field public static final guild_discovery_featured_header:I = 0x7f1209c5

.field public static final guild_discovery_footer_body:I = 0x7f1209c6

.field public static final guild_discovery_header_games_you_play:I = 0x7f1209c7

.field public static final guild_discovery_header_popular_guilds:I = 0x7f1209c8

.field public static final guild_discovery_header_search_results:I = 0x7f1209c9

.field public static final guild_discovery_home_subtitle:I = 0x7f1209ca

.field public static final guild_discovery_home_title:I = 0x7f1209cb

.field public static final guild_discovery_popular_header:I = 0x7f1209cc

.field public static final guild_discovery_search_empty_body:I = 0x7f1209cd

.field public static final guild_discovery_search_empty_category_body:I = 0x7f1209ce

.field public static final guild_discovery_search_empty_category_header:I = 0x7f1209cf

.field public static final guild_discovery_search_empty_description:I = 0x7f1209d0

.field public static final guild_discovery_search_empty_header:I = 0x7f1209d1

.field public static final guild_discovery_search_empty_title:I = 0x7f1209d2

.field public static final guild_discovery_search_enter_cta:I = 0x7f1209d3

.field public static final guild_discovery_search_error:I = 0x7f1209d4

.field public static final guild_discovery_search_header:I = 0x7f1209d5

.field public static final guild_discovery_search_label:I = 0x7f1209d6

.field public static final guild_discovery_search_placeholder:I = 0x7f1209d7

.field public static final guild_discovery_search_press_enter:I = 0x7f1209d8

.field public static final guild_discovery_search_results_category_header:I = 0x7f1209d9

.field public static final guild_discovery_search_results_header:I = 0x7f1209da

.field public static final guild_discovery_tooltip:I = 0x7f1209db

.field public static final guild_discovery_view_button:I = 0x7f1209dc

.field public static final guild_folder_color:I = 0x7f1209dd

.field public static final guild_folder_name:I = 0x7f1209de

.field public static final guild_folder_tooltip_a11y_label:I = 0x7f1209df

.field public static final guild_folder_tooltip_a11y_label_with_expanded_state:I = 0x7f1209e0

.field public static final guild_members_header:I = 0x7f1209e1

.field public static final guild_members_search_no_result:I = 0x7f1209e2

.field public static final guild_owner:I = 0x7f1209e3

.field public static final guild_partner_application_button_pause:I = 0x7f1209e4

.field public static final guild_partner_application_category_description:I = 0x7f1209e5

.field public static final guild_partner_application_category_label:I = 0x7f1209e6

.field public static final guild_partner_application_connect:I = 0x7f1209e7

.field public static final guild_partner_application_content_platform:I = 0x7f1209e8

.field public static final guild_partner_application_country:I = 0x7f1209e9

.field public static final guild_partner_application_creator_connect:I = 0x7f1209ea

.field public static final guild_partner_application_creator_must_connect_primary:I = 0x7f1209eb

.field public static final guild_partner_application_description_description:I = 0x7f1209ec

.field public static final guild_partner_application_description_title:I = 0x7f1209ed

.field public static final guild_partner_application_first_name:I = 0x7f1209ee

.field public static final guild_partner_application_has_errors:I = 0x7f1209ef

.field public static final guild_partner_application_invite_link_description:I = 0x7f1209f0

.field public static final guild_partner_application_invite_link_title:I = 0x7f1209f1

.field public static final guild_partner_application_language:I = 0x7f1209f2

.field public static final guild_partner_application_last_name:I = 0x7f1209f3

.field public static final guild_partner_application_other_platform:I = 0x7f1209f4

.field public static final guild_partner_application_other_platform_description:I = 0x7f1209f5

.field public static final guild_partner_application_other_platform_optional:I = 0x7f1209f6

.field public static final guild_partner_application_pause:I = 0x7f1209f7

.field public static final guild_partner_application_server_step:I = 0x7f1209f8

.field public static final guild_partner_application_server_step_description:I = 0x7f1209f9

.field public static final guild_partner_application_server_step_title:I = 0x7f1209fa

.field public static final guild_partner_application_subcategory_description:I = 0x7f1209fb

.field public static final guild_partner_application_submit_step:I = 0x7f1209fc

.field public static final guild_partner_application_submit_step_description:I = 0x7f1209fd

.field public static final guild_partner_application_submit_step_description_2:I = 0x7f1209fe

.field public static final guild_partner_application_submit_step_title:I = 0x7f1209ff

.field public static final guild_partner_application_survey_community_team_access:I = 0x7f120a00

.field public static final guild_partner_application_survey_description:I = 0x7f120a01

.field public static final guild_partner_application_survey_description_other:I = 0x7f120a02

.field public static final guild_partner_application_survey_early_access_to_features:I = 0x7f120a03

.field public static final guild_partner_application_survey_new_ideas_suggestions:I = 0x7f120a04

.field public static final guild_partner_application_survey_other:I = 0x7f120a05

.field public static final guild_partner_application_survey_talk_with_other_partners:I = 0x7f120a06

.field public static final guild_partner_application_survey_title:I = 0x7f120a07

.field public static final guild_partner_application_title:I = 0x7f120a08

.field public static final guild_partner_application_type:I = 0x7f120a09

.field public static final guild_partner_application_type_brand:I = 0x7f120a0a

.field public static final guild_partner_application_type_content:I = 0x7f120a0b

.field public static final guild_partner_application_type_fan:I = 0x7f120a0c

.field public static final guild_partner_application_type_other:I = 0x7f120a0d

.field public static final guild_partner_application_type_software:I = 0x7f120a0e

.field public static final guild_partner_application_user_step:I = 0x7f120a0f

.field public static final guild_partner_application_user_step_description:I = 0x7f120a10

.field public static final guild_partner_application_user_step_description_content_creator:I = 0x7f120a11

.field public static final guild_partner_application_user_step_title:I = 0x7f120a12

.field public static final guild_partner_application_website:I = 0x7f120a13

.field public static final guild_partner_application_why_you_description:I = 0x7f120a14

.field public static final guild_partner_application_why_you_title:I = 0x7f120a15

.field public static final guild_partnered:I = 0x7f120a16

.field public static final guild_popout_unavailable_flavor:I = 0x7f120a17

.field public static final guild_popout_unavailable_header:I = 0x7f120a18

.field public static final guild_popout_view_server_button:I = 0x7f120a19

.field public static final guild_premium:I = 0x7f120a1a

.field public static final guild_profile_join_server_button:I = 0x7f120a1b

.field public static final guild_progress_cta:I = 0x7f120a1c

.field public static final guild_progress_current_step:I = 0x7f120a1d

.field public static final guild_progress_finish:I = 0x7f120a1e

.field public static final guild_progress_skip:I = 0x7f120a1f

.field public static final guild_progress_steps:I = 0x7f120a20

.field public static final guild_progress_title:I = 0x7f120a21

.field public static final guild_role_actions_menu_label:I = 0x7f120a22

.field public static final guild_rules_edit_link:I = 0x7f120a23

.field public static final guild_rules_header:I = 0x7f120a24

.field public static final guild_rules_subheader:I = 0x7f120a25

.field public static final guild_security_req_mfa_body:I = 0x7f120a26

.field public static final guild_security_req_mfa_enable:I = 0x7f120a27

.field public static final guild_security_req_mfa_guild_disable:I = 0x7f120a28

.field public static final guild_security_req_mfa_guild_enable:I = 0x7f120a29

.field public static final guild_security_req_mfa_label:I = 0x7f120a2a

.field public static final guild_security_req_mfa_turn_off:I = 0x7f120a2b

.field public static final guild_security_req_mfa_turn_on:I = 0x7f120a2c

.field public static final guild_security_req_owner_only:I = 0x7f120a2d

.field public static final guild_select:I = 0x7f120a2e

.field public static final guild_settings_action_filter_bot_add:I = 0x7f120a2f

.field public static final guild_settings_action_filter_channel_create:I = 0x7f120a30

.field public static final guild_settings_action_filter_channel_delete:I = 0x7f120a31

.field public static final guild_settings_action_filter_channel_overwrite_create:I = 0x7f120a32

.field public static final guild_settings_action_filter_channel_overwrite_delete:I = 0x7f120a33

.field public static final guild_settings_action_filter_channel_overwrite_update:I = 0x7f120a34

.field public static final guild_settings_action_filter_channel_update:I = 0x7f120a35

.field public static final guild_settings_action_filter_emoji_create:I = 0x7f120a36

.field public static final guild_settings_action_filter_emoji_delete:I = 0x7f120a37

.field public static final guild_settings_action_filter_emoji_update:I = 0x7f120a38

.field public static final guild_settings_action_filter_guild_update:I = 0x7f120a39

.field public static final guild_settings_action_filter_integration_create:I = 0x7f120a3a

.field public static final guild_settings_action_filter_integration_delete:I = 0x7f120a3b

.field public static final guild_settings_action_filter_integration_update:I = 0x7f120a3c

.field public static final guild_settings_action_filter_invite_create:I = 0x7f120a3d

.field public static final guild_settings_action_filter_invite_delete:I = 0x7f120a3e

.field public static final guild_settings_action_filter_invite_update:I = 0x7f120a3f

.field public static final guild_settings_action_filter_member_ban_add:I = 0x7f120a40

.field public static final guild_settings_action_filter_member_ban_remove:I = 0x7f120a41

.field public static final guild_settings_action_filter_member_disconnect:I = 0x7f120a42

.field public static final guild_settings_action_filter_member_kick:I = 0x7f120a43

.field public static final guild_settings_action_filter_member_move:I = 0x7f120a44

.field public static final guild_settings_action_filter_member_prune:I = 0x7f120a45

.field public static final guild_settings_action_filter_member_role_update:I = 0x7f120a46

.field public static final guild_settings_action_filter_member_update:I = 0x7f120a47

.field public static final guild_settings_action_filter_message_bulk_delete:I = 0x7f120a48

.field public static final guild_settings_action_filter_message_delete:I = 0x7f120a49

.field public static final guild_settings_action_filter_message_pin:I = 0x7f120a4a

.field public static final guild_settings_action_filter_message_unpin:I = 0x7f120a4b

.field public static final guild_settings_action_filter_role_create:I = 0x7f120a4c

.field public static final guild_settings_action_filter_role_delete:I = 0x7f120a4d

.field public static final guild_settings_action_filter_role_update:I = 0x7f120a4e

.field public static final guild_settings_action_filter_webhook_create:I = 0x7f120a4f

.field public static final guild_settings_action_filter_webhook_delete:I = 0x7f120a50

.field public static final guild_settings_action_filter_webhook_update:I = 0x7f120a51

.field public static final guild_settings_audit_log_bot_add:I = 0x7f120a52

.field public static final guild_settings_audit_log_channel_bitrate_change:I = 0x7f120a53

.field public static final guild_settings_audit_log_channel_bitrate_create:I = 0x7f120a54

.field public static final guild_settings_audit_log_channel_category_create:I = 0x7f120a55

.field public static final guild_settings_audit_log_channel_delete:I = 0x7f120a56

.field public static final guild_settings_audit_log_channel_name_change:I = 0x7f120a57

.field public static final guild_settings_audit_log_channel_name_create:I = 0x7f120a58

.field public static final guild_settings_audit_log_channel_nsfw_disabled:I = 0x7f120a59

.field public static final guild_settings_audit_log_channel_nsfw_enabled:I = 0x7f120a5a

.field public static final guild_settings_audit_log_channel_overwrite_create:I = 0x7f120a5b

.field public static final guild_settings_audit_log_channel_overwrite_delete:I = 0x7f120a5c

.field public static final guild_settings_audit_log_channel_overwrite_update:I = 0x7f120a5d

.field public static final guild_settings_audit_log_channel_permission_overrides_denied:I = 0x7f120a5e

.field public static final guild_settings_audit_log_channel_permission_overrides_granted:I = 0x7f120a5f

.field public static final guild_settings_audit_log_channel_position_change:I = 0x7f120a60

.field public static final guild_settings_audit_log_channel_position_create:I = 0x7f120a61

.field public static final guild_settings_audit_log_channel_rate_limit_per_user_change:I = 0x7f120a62

.field public static final guild_settings_audit_log_channel_rate_limit_per_user_create:I = 0x7f120a63

.field public static final guild_settings_audit_log_channel_text_create:I = 0x7f120a64

.field public static final guild_settings_audit_log_channel_topic_change:I = 0x7f120a65

.field public static final guild_settings_audit_log_channel_topic_create:I = 0x7f120a66

.field public static final guild_settings_audit_log_channel_type_change:I = 0x7f120a67

.field public static final guild_settings_audit_log_channel_type_create:I = 0x7f120a68

.field public static final guild_settings_audit_log_channel_update:I = 0x7f120a69

.field public static final guild_settings_audit_log_channel_voice_create:I = 0x7f120a6a

.field public static final guild_settings_audit_log_common_reason:I = 0x7f120a6b

.field public static final guild_settings_audit_log_emoji_create:I = 0x7f120a6c

.field public static final guild_settings_audit_log_emoji_delete:I = 0x7f120a6d

.field public static final guild_settings_audit_log_emoji_name_change:I = 0x7f120a6e

.field public static final guild_settings_audit_log_emoji_name_create:I = 0x7f120a6f

.field public static final guild_settings_audit_log_emoji_update:I = 0x7f120a70

.field public static final guild_settings_audit_log_guild_afk_channel_id_change:I = 0x7f120a71

.field public static final guild_settings_audit_log_guild_afk_channel_id_clear:I = 0x7f120a72

.field public static final guild_settings_audit_log_guild_afk_timeout_change:I = 0x7f120a73

.field public static final guild_settings_audit_log_guild_banner_hash_change:I = 0x7f120a74

.field public static final guild_settings_audit_log_guild_default_message_notifications_change_all_messages:I = 0x7f120a75

.field public static final guild_settings_audit_log_guild_default_message_notifications_change_only_mentions:I = 0x7f120a76

.field public static final guild_settings_audit_log_guild_description_change:I = 0x7f120a77

.field public static final guild_settings_audit_log_guild_description_clear:I = 0x7f120a78

.field public static final guild_settings_audit_log_guild_discovery_splash_hash_change:I = 0x7f120a79

.field public static final guild_settings_audit_log_guild_explicit_content_filter_all_members:I = 0x7f120a7a

.field public static final guild_settings_audit_log_guild_explicit_content_filter_disable:I = 0x7f120a7b

.field public static final guild_settings_audit_log_guild_explicit_content_filter_members_without_roles:I = 0x7f120a7c

.field public static final guild_settings_audit_log_guild_icon_hash_change:I = 0x7f120a7d

.field public static final guild_settings_audit_log_guild_mfa_level_disabled:I = 0x7f120a7e

.field public static final guild_settings_audit_log_guild_mfa_level_enabled:I = 0x7f120a7f

.field public static final guild_settings_audit_log_guild_name_change:I = 0x7f120a80

.field public static final guild_settings_audit_log_guild_owner_id_change:I = 0x7f120a81

.field public static final guild_settings_audit_log_guild_preferred_locale_change:I = 0x7f120a82

.field public static final guild_settings_audit_log_guild_region_change:I = 0x7f120a83

.field public static final guild_settings_audit_log_guild_rules_channel_id_change:I = 0x7f120a84

.field public static final guild_settings_audit_log_guild_rules_channel_id_clear:I = 0x7f120a85

.field public static final guild_settings_audit_log_guild_splash_hash_change:I = 0x7f120a86

.field public static final guild_settings_audit_log_guild_system_channel_id_change:I = 0x7f120a87

.field public static final guild_settings_audit_log_guild_system_channel_id_disable:I = 0x7f120a88

.field public static final guild_settings_audit_log_guild_update:I = 0x7f120a89

.field public static final guild_settings_audit_log_guild_updates_channel_id_change:I = 0x7f120a8a

.field public static final guild_settings_audit_log_guild_updates_channel_id_clear:I = 0x7f120a8b

.field public static final guild_settings_audit_log_guild_vanity_url_code_change:I = 0x7f120a8c

.field public static final guild_settings_audit_log_guild_vanity_url_code_delete:I = 0x7f120a8d

.field public static final guild_settings_audit_log_guild_verification_level_change_high:I = 0x7f120a8e

.field public static final guild_settings_audit_log_guild_verification_level_change_low:I = 0x7f120a8f

.field public static final guild_settings_audit_log_guild_verification_level_change_medium:I = 0x7f120a90

.field public static final guild_settings_audit_log_guild_verification_level_change_none:I = 0x7f120a91

.field public static final guild_settings_audit_log_guild_verification_level_change_very_high:I = 0x7f120a92

.field public static final guild_settings_audit_log_guild_widget_channel_id_change:I = 0x7f120a93

.field public static final guild_settings_audit_log_guild_widget_channel_id_delete:I = 0x7f120a94

.field public static final guild_settings_audit_log_guild_widget_disabled:I = 0x7f120a95

.field public static final guild_settings_audit_log_guild_widget_enabled:I = 0x7f120a96

.field public static final guild_settings_audit_log_integration_create:I = 0x7f120a97

.field public static final guild_settings_audit_log_integration_delete:I = 0x7f120a98

.field public static final guild_settings_audit_log_integration_enable_emoticons_off:I = 0x7f120a99

.field public static final guild_settings_audit_log_integration_enable_emoticons_on:I = 0x7f120a9a

.field public static final guild_settings_audit_log_integration_expire_behavior_kick_from_server:I = 0x7f120a9b

.field public static final guild_settings_audit_log_integration_expire_behavior_remove_synced_role:I = 0x7f120a9c

.field public static final guild_settings_audit_log_integration_expire_grace_period:I = 0x7f120a9d

.field public static final guild_settings_audit_log_integration_update:I = 0x7f120a9e

.field public static final guild_settings_audit_log_invite_channel_create:I = 0x7f120a9f

.field public static final guild_settings_audit_log_invite_code_create:I = 0x7f120aa0

.field public static final guild_settings_audit_log_invite_create:I = 0x7f120aa1

.field public static final guild_settings_audit_log_invite_delete:I = 0x7f120aa2

.field public static final guild_settings_audit_log_invite_max_age_create:I = 0x7f120aa3

.field public static final guild_settings_audit_log_invite_max_age_create_infinite:I = 0x7f120aa4

.field public static final guild_settings_audit_log_invite_max_uses_create:I = 0x7f120aa5

.field public static final guild_settings_audit_log_invite_max_uses_create_infinite:I = 0x7f120aa6

.field public static final guild_settings_audit_log_invite_temporary_off:I = 0x7f120aa7

.field public static final guild_settings_audit_log_invite_temporary_on:I = 0x7f120aa8

.field public static final guild_settings_audit_log_invite_update:I = 0x7f120aa9

.field public static final guild_settings_audit_log_load_more:I = 0x7f120aaa

.field public static final guild_settings_audit_log_member_ban_add:I = 0x7f120aab

.field public static final guild_settings_audit_log_member_ban_remove:I = 0x7f120aac

.field public static final guild_settings_audit_log_member_deaf_off:I = 0x7f120aad

.field public static final guild_settings_audit_log_member_deaf_on:I = 0x7f120aae

.field public static final guild_settings_audit_log_member_disconnect:I = 0x7f120aaf

.field public static final guild_settings_audit_log_member_kick:I = 0x7f120ab0

.field public static final guild_settings_audit_log_member_move:I = 0x7f120ab1

.field public static final guild_settings_audit_log_member_mute_off:I = 0x7f120ab2

.field public static final guild_settings_audit_log_member_mute_on:I = 0x7f120ab3

.field public static final guild_settings_audit_log_member_nick_change:I = 0x7f120ab4

.field public static final guild_settings_audit_log_member_nick_create:I = 0x7f120ab5

.field public static final guild_settings_audit_log_member_nick_delete:I = 0x7f120ab6

.field public static final guild_settings_audit_log_member_prune:I = 0x7f120ab7

.field public static final guild_settings_audit_log_member_prune_delete_days:I = 0x7f120ab8

.field public static final guild_settings_audit_log_member_role_update:I = 0x7f120ab9

.field public static final guild_settings_audit_log_member_roles_add:I = 0x7f120aba

.field public static final guild_settings_audit_log_member_roles_remove:I = 0x7f120abb

.field public static final guild_settings_audit_log_member_update:I = 0x7f120abc

.field public static final guild_settings_audit_log_message_bulk_delete:I = 0x7f120abd

.field public static final guild_settings_audit_log_message_delete:I = 0x7f120abe

.field public static final guild_settings_audit_log_message_pin:I = 0x7f120abf

.field public static final guild_settings_audit_log_message_unpin:I = 0x7f120ac0

.field public static final guild_settings_audit_log_role_color:I = 0x7f120ac1

.field public static final guild_settings_audit_log_role_color_ios:I = 0x7f120ac2

.field public static final guild_settings_audit_log_role_color_none:I = 0x7f120ac3

.field public static final guild_settings_audit_log_role_create:I = 0x7f120ac4

.field public static final guild_settings_audit_log_role_delete:I = 0x7f120ac5

.field public static final guild_settings_audit_log_role_hoist_off:I = 0x7f120ac6

.field public static final guild_settings_audit_log_role_hoist_on:I = 0x7f120ac7

.field public static final guild_settings_audit_log_role_mentionable_off:I = 0x7f120ac8

.field public static final guild_settings_audit_log_role_mentionable_on:I = 0x7f120ac9

.field public static final guild_settings_audit_log_role_name_change:I = 0x7f120aca

.field public static final guild_settings_audit_log_role_name_create:I = 0x7f120acb

.field public static final guild_settings_audit_log_role_permissions_denied:I = 0x7f120acc

.field public static final guild_settings_audit_log_role_permissions_granted:I = 0x7f120acd

.field public static final guild_settings_audit_log_role_update:I = 0x7f120ace

.field public static final guild_settings_audit_log_time_at_android:I = 0x7f120acf

.field public static final guild_settings_audit_log_unknown_action:I = 0x7f120ad0

.field public static final guild_settings_audit_log_webhook_avatar:I = 0x7f120ad1

.field public static final guild_settings_audit_log_webhook_channel_change:I = 0x7f120ad2

.field public static final guild_settings_audit_log_webhook_channel_create:I = 0x7f120ad3

.field public static final guild_settings_audit_log_webhook_create:I = 0x7f120ad4

.field public static final guild_settings_audit_log_webhook_delete:I = 0x7f120ad5

.field public static final guild_settings_audit_log_webhook_name_change:I = 0x7f120ad6

.field public static final guild_settings_audit_log_webhook_name_create:I = 0x7f120ad7

.field public static final guild_settings_audit_log_webhook_update:I = 0x7f120ad8

.field public static final guild_settings_banner_recommend:I = 0x7f120ad9

.field public static final guild_settings_community:I = 0x7f120ada

.field public static final guild_settings_community_administrator_only:I = 0x7f120adb

.field public static final guild_settings_community_disable_community:I = 0x7f120adc

.field public static final guild_settings_community_disable_community_description:I = 0x7f120add

.field public static final guild_settings_community_disable_community_dialog_message:I = 0x7f120ade

.field public static final guild_settings_community_disable_community_dialog_title:I = 0x7f120adf

.field public static final guild_settings_community_disable_public_confirm_default_title:I = 0x7f120ae0

.field public static final guild_settings_community_enable_community:I = 0x7f120ae1

.field public static final guild_settings_community_intro_body:I = 0x7f120ae2

.field public static final guild_settings_community_intro_details:I = 0x7f120ae3

.field public static final guild_settings_community_intro_details_disclaimer_mobile:I = 0x7f120ae4

.field public static final guild_settings_community_intro_details_mobile:I = 0x7f120ae5

.field public static final guild_settings_community_intro_header:I = 0x7f120ae6

.field public static final guild_settings_community_intro_learn_more:I = 0x7f120ae7

.field public static final guild_settings_community_intro_upsell_analytics_body:I = 0x7f120ae8

.field public static final guild_settings_community_intro_upsell_analytics_body_mobile:I = 0x7f120ae9

.field public static final guild_settings_community_intro_upsell_analytics_header:I = 0x7f120aea

.field public static final guild_settings_community_intro_upsell_analytics_tooltip:I = 0x7f120aeb

.field public static final guild_settings_community_intro_upsell_discovery_body:I = 0x7f120aec

.field public static final guild_settings_community_intro_upsell_discovery_header:I = 0x7f120aed

.field public static final guild_settings_community_intro_upsell_partner_body:I = 0x7f120aee

.field public static final guild_settings_community_intro_upsell_partner_header:I = 0x7f120aef

.field public static final guild_settings_community_intro_upsell_stay_informed_body:I = 0x7f120af0

.field public static final guild_settings_community_intro_upsell_stay_informed_header:I = 0x7f120af1

.field public static final guild_settings_community_locale_help:I = 0x7f120af2

.field public static final guild_settings_community_mod_channel_help:I = 0x7f120af3

.field public static final guild_settings_community_mod_channel_help_mobile:I = 0x7f120af4

.field public static final guild_settings_community_mod_channel_selector_title:I = 0x7f120af5

.field public static final guild_settings_community_mod_channel_title:I = 0x7f120af6

.field public static final guild_settings_community_rules_channel_help:I = 0x7f120af7

.field public static final guild_settings_community_rules_channel_help_mobile:I = 0x7f120af8

.field public static final guild_settings_community_upsell_body:I = 0x7f120af9

.field public static final guild_settings_community_upsell_button_analytics:I = 0x7f120afa

.field public static final guild_settings_community_upsell_button_discovery:I = 0x7f120afb

.field public static final guild_settings_community_upsell_button_welcome_screen:I = 0x7f120afc

.field public static final guild_settings_community_upsell_header:I = 0x7f120afd

.field public static final guild_settings_default_notification_settings_intro:I = 0x7f120afe

.field public static final guild_settings_default_notification_settings_protip:I = 0x7f120aff

.field public static final guild_settings_default_notifications_large_guild_notify_all:I = 0x7f120b00

.field public static final guild_settings_disable_discoverable:I = 0x7f120b01

.field public static final guild_settings_discovery_admin_only:I = 0x7f120b02

.field public static final guild_settings_discovery_checklist_2fa:I = 0x7f120b03

.field public static final guild_settings_discovery_checklist_2fa_description:I = 0x7f120b04

.field public static final guild_settings_discovery_checklist_2fa_description_failing:I = 0x7f120b05

.field public static final guild_settings_discovery_checklist_2fa_failing:I = 0x7f120b06

.field public static final guild_settings_discovery_checklist_abide_by_guidelines:I = 0x7f120b07

.field public static final guild_settings_discovery_checklist_age:I = 0x7f120b08

.field public static final guild_settings_discovery_checklist_age_description:I = 0x7f120b09

.field public static final guild_settings_discovery_checklist_age_description_failing:I = 0x7f120b0a

.field public static final guild_settings_discovery_checklist_age_failing:I = 0x7f120b0b

.field public static final guild_settings_discovery_checklist_bad_standing:I = 0x7f120b0c

.field public static final guild_settings_discovery_checklist_change_channel_categories:I = 0x7f120b0d

.field public static final guild_settings_discovery_checklist_change_channel_names:I = 0x7f120b0e

.field public static final guild_settings_discovery_checklist_change_description:I = 0x7f120b0f

.field public static final guild_settings_discovery_checklist_change_name:I = 0x7f120b10

.field public static final guild_settings_discovery_checklist_communicator_failing_action:I = 0x7f120b11

.field public static final guild_settings_discovery_checklist_communicator_failing_action_details:I = 0x7f120b12

.field public static final guild_settings_discovery_checklist_communicator_progress_label:I = 0x7f120b13

.field public static final guild_settings_discovery_checklist_engagement_failing:I = 0x7f120b14

.field public static final guild_settings_discovery_checklist_good_standing:I = 0x7f120b15

.field public static final guild_settings_discovery_checklist_health_definitions_intro:I = 0x7f120b16

.field public static final guild_settings_discovery_checklist_health_view_details:I = 0x7f120b17

.field public static final guild_settings_discovery_checklist_healthy:I = 0x7f120b18

.field public static final guild_settings_discovery_checklist_healthy_description:I = 0x7f120b19

.field public static final guild_settings_discovery_checklist_healthy_description_failing:I = 0x7f120b1a

.field public static final guild_settings_discovery_checklist_healthy_description_pending:I = 0x7f120b1b

.field public static final guild_settings_discovery_checklist_healthy_description_pending_size:I = 0x7f120b1c

.field public static final guild_settings_discovery_checklist_healthy_failing:I = 0x7f120b1d

.field public static final guild_settings_discovery_checklist_healthy_pending:I = 0x7f120b1e

.field public static final guild_settings_discovery_checklist_loading:I = 0x7f120b1f

.field public static final guild_settings_discovery_checklist_nsfw:I = 0x7f120b20

.field public static final guild_settings_discovery_checklist_nsfw_description:I = 0x7f120b21

.field public static final guild_settings_discovery_checklist_nsfw_failing:I = 0x7f120b22

.field public static final guild_settings_discovery_checklist_progress_requirement_label:I = 0x7f120b23

.field public static final guild_settings_discovery_checklist_retention_failing:I = 0x7f120b24

.field public static final guild_settings_discovery_checklist_retention_failing_action:I = 0x7f120b25

.field public static final guild_settings_discovery_checklist_retention_failing_action_details_updated:I = 0x7f120b26

.field public static final guild_settings_discovery_checklist_retention_progress_label:I = 0x7f120b27

.field public static final guild_settings_discovery_checklist_safe:I = 0x7f120b28

.field public static final guild_settings_discovery_checklist_safe_description:I = 0x7f120b29

.field public static final guild_settings_discovery_checklist_safe_description_failing:I = 0x7f120b2a

.field public static final guild_settings_discovery_checklist_safe_description_failing_guidelines:I = 0x7f120b2b

.field public static final guild_settings_discovery_checklist_safe_failing:I = 0x7f120b2c

.field public static final guild_settings_discovery_checklist_score_explain:I = 0x7f120b2d

.field public static final guild_settings_discovery_checklist_size:I = 0x7f120b2e

.field public static final guild_settings_discovery_checklist_size_description:I = 0x7f120b2f

.field public static final guild_settings_discovery_checklist_size_description_failing:I = 0x7f120b30

.field public static final guild_settings_discovery_checklist_size_description_failing_partners:I = 0x7f120b31

.field public static final guild_settings_discovery_checklist_size_failing:I = 0x7f120b32

.field public static final guild_settings_discovery_checklist_visitor_failing_action:I = 0x7f120b33

.field public static final guild_settings_discovery_checklist_visitor_failing_action_details:I = 0x7f120b34

.field public static final guild_settings_discovery_checklist_visitor_progress_label:I = 0x7f120b35

.field public static final guild_settings_discovery_description:I = 0x7f120b36

.field public static final guild_settings_discovery_disable_public_confirm_text:I = 0x7f120b37

.field public static final guild_settings_discovery_disqualified:I = 0x7f120b38

.field public static final guild_settings_discovery_disqualified_description:I = 0x7f120b39

.field public static final guild_settings_discovery_emoji_discoverability_description:I = 0x7f120b3a

.field public static final guild_settings_discovery_emoji_discoverability_disable:I = 0x7f120b3b

.field public static final guild_settings_discovery_emoji_discoverability_enable:I = 0x7f120b3c

.field public static final guild_settings_discovery_emoji_discoverability_title:I = 0x7f120b3d

.field public static final guild_settings_discovery_enabled_modal_body:I = 0x7f120b3e

.field public static final guild_settings_discovery_enabled_modal_dismiss:I = 0x7f120b3f

.field public static final guild_settings_discovery_enabled_modal_header:I = 0x7f120b40

.field public static final guild_settings_discovery_header:I = 0x7f120b41

.field public static final guild_settings_discovery_locale_help:I = 0x7f120b42

.field public static final guild_settings_discovery_pending_healthy:I = 0x7f120b43

.field public static final guild_settings_discovery_preview_description:I = 0x7f120b44

.field public static final guild_settings_discovery_primary_category_description:I = 0x7f120b45

.field public static final guild_settings_discovery_primary_category_title:I = 0x7f120b46

.field public static final guild_settings_discovery_requirements_not_met:I = 0x7f120b47

.field public static final guild_settings_discovery_search_keywords_description:I = 0x7f120b48

.field public static final guild_settings_discovery_search_keywords_title:I = 0x7f120b49

.field public static final guild_settings_discovery_subcategory_title:I = 0x7f120b4a

.field public static final guild_settings_emoji_alias:I = 0x7f120b4b

.field public static final guild_settings_emoji_alias_placeholder:I = 0x7f120b4c

.field public static final guild_settings_emoji_upload_to_server_message:I = 0x7f120b4d

.field public static final guild_settings_enable_discoverable:I = 0x7f120b4e

.field public static final guild_settings_example_tooltip:I = 0x7f120b4f

.field public static final guild_settings_filter_action:I = 0x7f120b50

.field public static final guild_settings_filter_all:I = 0x7f120b51

.field public static final guild_settings_filter_all_actions:I = 0x7f120b52

.field public static final guild_settings_filter_all_users:I = 0x7f120b53

.field public static final guild_settings_filter_user:I = 0x7f120b54

.field public static final guild_settings_filtered_action:I = 0x7f120b55

.field public static final guild_settings_filtered_user:I = 0x7f120b56

.field public static final guild_settings_follower_analytics:I = 0x7f120b57

.field public static final guild_settings_follower_analytics_description:I = 0x7f120b58

.field public static final guild_settings_follower_analytics_empty_body:I = 0x7f120b59

.field public static final guild_settings_follower_analytics_empty_header:I = 0x7f120b5a

.field public static final guild_settings_follower_analytics_last_updated:I = 0x7f120b5b

.field public static final guild_settings_follower_analytics_message_deleted:I = 0x7f120b5c

.field public static final guild_settings_follower_analytics_net_gain:I = 0x7f120b5d

.field public static final guild_settings_follower_analytics_net_gain_tooltip:I = 0x7f120b5e

.field public static final guild_settings_follower_analytics_net_servers:I = 0x7f120b5f

.field public static final guild_settings_follower_analytics_post:I = 0x7f120b60

.field public static final guild_settings_follower_analytics_reach:I = 0x7f120b61

.field public static final guild_settings_follower_analytics_reach_tooltip:I = 0x7f120b62

.field public static final guild_settings_follower_analytics_servers_following:I = 0x7f120b63

.field public static final guild_settings_follower_analytics_tooltip:I = 0x7f120b64

.field public static final guild_settings_guild_premium_perk_description_tier_1_animated_guild_icon:I = 0x7f120b65

.field public static final guild_settings_guild_premium_perk_description_tier_1_audio_quality:I = 0x7f120b66

.field public static final guild_settings_guild_premium_perk_description_tier_1_emoji:I = 0x7f120b67

.field public static final guild_settings_guild_premium_perk_description_tier_1_splash:I = 0x7f120b68

.field public static final guild_settings_guild_premium_perk_description_tier_1_streaming:I = 0x7f120b69

.field public static final guild_settings_guild_premium_perk_description_tier_2_audio_quality:I = 0x7f120b6a

.field public static final guild_settings_guild_premium_perk_description_tier_2_banner:I = 0x7f120b6b

.field public static final guild_settings_guild_premium_perk_description_tier_2_emoji:I = 0x7f120b6c

.field public static final guild_settings_guild_premium_perk_description_tier_2_streaming:I = 0x7f120b6d

.field public static final guild_settings_guild_premium_perk_description_tier_2_upload_limit:I = 0x7f120b6e

.field public static final guild_settings_guild_premium_perk_description_tier_3_audio_quality:I = 0x7f120b6f

.field public static final guild_settings_guild_premium_perk_description_tier_3_emoji:I = 0x7f120b70

.field public static final guild_settings_guild_premium_perk_description_tier_3_upload_limit:I = 0x7f120b71

.field public static final guild_settings_guild_premium_perk_description_tier_3_vanity_url:I = 0x7f120b72

.field public static final guild_settings_guild_premium_perk_title_tier_1_animated_guild_icon:I = 0x7f120b73

.field public static final guild_settings_guild_premium_perk_title_tier_1_splash:I = 0x7f120b74

.field public static final guild_settings_guild_premium_perk_title_tier_1_streaming:I = 0x7f120b75

.field public static final guild_settings_guild_premium_perk_title_tier_2_banner:I = 0x7f120b76

.field public static final guild_settings_guild_premium_perk_title_tier_2_streaming:I = 0x7f120b77

.field public static final guild_settings_guild_premium_perk_title_tier_3_vanity_url:I = 0x7f120b78

.field public static final guild_settings_guild_premium_perk_title_tier_any_audio_quality:I = 0x7f120b79

.field public static final guild_settings_guild_premium_perk_title_tier_any_emoji:I = 0x7f120b7a

.field public static final guild_settings_guild_premium_perk_title_tier_any_upload_limit:I = 0x7f120b7b

.field public static final guild_settings_guild_premium_perks_base_perks:I = 0x7f120b7c

.field public static final guild_settings_guild_premium_perks_description_none:I = 0x7f120b7d

.field public static final guild_settings_guild_premium_perks_previous_perks:I = 0x7f120b7e

.field public static final guild_settings_guild_premium_perks_title_none:I = 0x7f120b7f

.field public static final guild_settings_guild_premium_perks_title_tier_1:I = 0x7f120b80

.field public static final guild_settings_guild_premium_perks_title_tier_2:I = 0x7f120b81

.field public static final guild_settings_guild_premium_perks_title_tier_3:I = 0x7f120b82

.field public static final guild_settings_icon_recommend:I = 0x7f120b83

.field public static final guild_settings_label_audit_log:I = 0x7f120b84

.field public static final guild_settings_label_audit_log_empty_body:I = 0x7f120b85

.field public static final guild_settings_label_audit_log_empty_title:I = 0x7f120b86

.field public static final guild_settings_label_audit_log_error_body:I = 0x7f120b87

.field public static final guild_settings_label_audit_log_error_title:I = 0x7f120b88

.field public static final guild_settings_member_verification:I = 0x7f120b89

.field public static final guild_settings_member_verification_applications_title:I = 0x7f120b8a

.field public static final guild_settings_member_verification_approved:I = 0x7f120b8b

.field public static final guild_settings_member_verification_description:I = 0x7f120b8c

.field public static final guild_settings_member_verification_description_coming_soon:I = 0x7f120b8d

.field public static final guild_settings_member_verification_description_placeholder:I = 0x7f120b8e

.field public static final guild_settings_member_verification_description_title:I = 0x7f120b8f

.field public static final guild_settings_member_verification_enable_reminder:I = 0x7f120b90

.field public static final guild_settings_member_verification_enabled:I = 0x7f120b91

.field public static final guild_settings_member_verification_enabled_second_line:I = 0x7f120b92

.field public static final guild_settings_member_verification_example_guild_name:I = 0x7f120b93

.field public static final guild_settings_member_verification_intro_button:I = 0x7f120b94

.field public static final guild_settings_member_verification_pending:I = 0x7f120b95

.field public static final guild_settings_member_verification_preview:I = 0x7f120b96

.field public static final guild_settings_member_verification_preview_disabled_description:I = 0x7f120b97

.field public static final guild_settings_member_verification_preview_disabled_title:I = 0x7f120b98

.field public static final guild_settings_member_verification_preview_enabled_description:I = 0x7f120b99

.field public static final guild_settings_member_verification_preview_enabled_title:I = 0x7f120b9a

.field public static final guild_settings_member_verification_preview_title:I = 0x7f120b9b

.field public static final guild_settings_member_verification_progress_will_save:I = 0x7f120b9c

.field public static final guild_settings_member_verification_rejected:I = 0x7f120b9d

.field public static final guild_settings_member_verification_screen_title:I = 0x7f120b9e

.field public static final guild_settings_members_add_role:I = 0x7f120b9f

.field public static final guild_settings_members_display_role:I = 0x7f120ba0

.field public static final guild_settings_members_remove_role:I = 0x7f120ba1

.field public static final guild_settings_members_server_members:I = 0x7f120ba2

.field public static final guild_settings_overview_boost_unlock:I = 0x7f120ba3

.field public static final guild_settings_overview_boost_unlocked:I = 0x7f120ba4

.field public static final guild_settings_overview_tier_info:I = 0x7f120ba5

.field public static final guild_settings_partner_and_discovery_disable_public_confirm_text:I = 0x7f120ba6

.field public static final guild_settings_partner_checklist_age_description:I = 0x7f120ba7

.field public static final guild_settings_partner_checklist_age_description_failing:I = 0x7f120ba8

.field public static final guild_settings_partner_checklist_failing_header:I = 0x7f120ba9

.field public static final guild_settings_partner_checklist_failing_subheader:I = 0x7f120baa

.field public static final guild_settings_partner_checklist_health_view_details:I = 0x7f120bab

.field public static final guild_settings_partner_checklist_healthy_description:I = 0x7f120bac

.field public static final guild_settings_partner_checklist_healthy_description_failing:I = 0x7f120bad

.field public static final guild_settings_partner_checklist_healthy_description_pending:I = 0x7f120bae

.field public static final guild_settings_partner_checklist_healthy_description_pending_size:I = 0x7f120baf

.field public static final guild_settings_partner_checklist_passing_header:I = 0x7f120bb0

.field public static final guild_settings_partner_checklist_passing_subheader:I = 0x7f120bb1

.field public static final guild_settings_partner_checklist_size:I = 0x7f120bb2

.field public static final guild_settings_partner_checklist_size_description:I = 0x7f120bb3

.field public static final guild_settings_partner_checklist_size_description_failing:I = 0x7f120bb4

.field public static final guild_settings_partner_checklist_size_failing:I = 0x7f120bb5

.field public static final guild_settings_partner_disable_public_confirm_text:I = 0x7f120bb6

.field public static final guild_settings_partner_intro_apply_button:I = 0x7f120bb7

.field public static final guild_settings_partner_intro_apply_button_tooltip_only_owner:I = 0x7f120bb8

.field public static final guild_settings_partner_intro_apply_button_tooltip_pending:I = 0x7f120bb9

.field public static final guild_settings_partner_intro_apply_button_tooltip_rejected:I = 0x7f120bba

.field public static final guild_settings_partner_intro_apply_details:I = 0x7f120bbb

.field public static final guild_settings_partner_intro_body:I = 0x7f120bbc

.field public static final guild_settings_partner_intro_cooldown:I = 0x7f120bbd

.field public static final guild_settings_partner_intro_cooldown_counter:I = 0x7f120bbe

.field public static final guild_settings_partner_intro_header:I = 0x7f120bbf

.field public static final guild_settings_partner_intro_pending:I = 0x7f120bc0

.field public static final guild_settings_partner_intro_rejected:I = 0x7f120bc1

.field public static final guild_settings_partner_intro_upsell_branding_body:I = 0x7f120bc2

.field public static final guild_settings_partner_intro_upsell_branding_header:I = 0x7f120bc3

.field public static final guild_settings_partner_intro_upsell_perks_body:I = 0x7f120bc4

.field public static final guild_settings_partner_intro_upsell_perks_header:I = 0x7f120bc5

.field public static final guild_settings_partner_intro_upsell_recognition_body:I = 0x7f120bc6

.field public static final guild_settings_partner_intro_upsell_recognition_header:I = 0x7f120bc7

.field public static final guild_settings_partner_not_verified:I = 0x7f120bc8

.field public static final guild_settings_premium_guild_blurb:I = 0x7f120bc9

.field public static final guild_settings_premium_guild_close_hint:I = 0x7f120bca

.field public static final guild_settings_premium_guild_count_subscribers:I = 0x7f120bcb

.field public static final guild_settings_premium_guild_tier_requirement:I = 0x7f120bcc

.field public static final guild_settings_premium_guild_title:I = 0x7f120bcd

.field public static final guild_settings_premium_guild_unlocked:I = 0x7f120bce

.field public static final guild_settings_premium_upsell_body_perk_guild_subscription_discount:I = 0x7f120bcf

.field public static final guild_settings_premium_upsell_body_perk_no_free_guild_subscriptions:I = 0x7f120bd0

.field public static final guild_settings_premium_upsell_body_perk_num_guild_subscriptions:I = 0x7f120bd1

.field public static final guild_settings_premium_upsell_heading_primary:I = 0x7f120bd2

.field public static final guild_settings_premium_upsell_heading_secondary:I = 0x7f120bd3

.field public static final guild_settings_premium_upsell_heading_secondary_premium_user:I = 0x7f120bd4

.field public static final guild_settings_premium_upsell_heading_tertiary_premium_user:I = 0x7f120bd5

.field public static final guild_settings_premium_upsell_learn_more:I = 0x7f120bd6

.field public static final guild_settings_premium_upsell_subheading:I = 0x7f120bd7

.field public static final guild_settings_premium_upsell_subheading_extra_android:I = 0x7f120bd8

.field public static final guild_settings_premium_upsell_subheading_extra_ios:I = 0x7f120bd9

.field public static final guild_settings_premium_upsell_subheading_tier_1:I = 0x7f120bda

.field public static final guild_settings_premium_upsell_subheading_tier_1_mobile:I = 0x7f120bdb

.field public static final guild_settings_premium_upsell_subheading_tier_2:I = 0x7f120bdc

.field public static final guild_settings_premium_upsell_subheading_tier_2_mobile:I = 0x7f120bdd

.field public static final guild_settings_public_mod_channel_title:I = 0x7f120bde

.field public static final guild_settings_public_no_option_selected:I = 0x7f120bdf

.field public static final guild_settings_public_update_failed:I = 0x7f120be0

.field public static final guild_settings_public_welcome:I = 0x7f120be1

.field public static final guild_settings_public_welcome_add_recommended_channel:I = 0x7f120be2

.field public static final guild_settings_public_welcome_channel_delete:I = 0x7f120be3

.field public static final guild_settings_public_welcome_channel_description_placeholder:I = 0x7f120be4

.field public static final guild_settings_public_welcome_channel_edit:I = 0x7f120be5

.field public static final guild_settings_public_welcome_channel_move_down:I = 0x7f120be6

.field public static final guild_settings_public_welcome_channel_move_up:I = 0x7f120be7

.field public static final guild_settings_public_welcome_description_placeholder:I = 0x7f120be8

.field public static final guild_settings_public_welcome_enable_reminder:I = 0x7f120be9

.field public static final guild_settings_public_welcome_enabled:I = 0x7f120bea

.field public static final guild_settings_public_welcome_enabled_second_line:I = 0x7f120beb

.field public static final guild_settings_public_welcome_example_channel_description_1:I = 0x7f120bec

.field public static final guild_settings_public_welcome_example_channel_description_2:I = 0x7f120bed

.field public static final guild_settings_public_welcome_example_channel_description_3:I = 0x7f120bee

.field public static final guild_settings_public_welcome_example_channel_name_1:I = 0x7f120bef

.field public static final guild_settings_public_welcome_example_channel_name_2:I = 0x7f120bf0

.field public static final guild_settings_public_welcome_example_channel_name_3:I = 0x7f120bf1

.field public static final guild_settings_public_welcome_example_description:I = 0x7f120bf2

.field public static final guild_settings_public_welcome_example_title:I = 0x7f120bf3

.field public static final guild_settings_public_welcome_intro_button:I = 0x7f120bf4

.field public static final guild_settings_public_welcome_intro_text:I = 0x7f120bf5

.field public static final guild_settings_public_welcome_invalid_channel:I = 0x7f120bf6

.field public static final guild_settings_public_welcome_only_viewable_channels:I = 0x7f120bf7

.field public static final guild_settings_public_welcome_pick_channel:I = 0x7f120bf8

.field public static final guild_settings_public_welcome_preview:I = 0x7f120bf9

.field public static final guild_settings_public_welcome_progress_will_save:I = 0x7f120bfa

.field public static final guild_settings_public_welcome_recommended_channel_modal_add:I = 0x7f120bfb

.field public static final guild_settings_public_welcome_recommended_channel_modal_edit:I = 0x7f120bfc

.field public static final guild_settings_public_welcome_recommended_channels_description:I = 0x7f120bfd

.field public static final guild_settings_public_welcome_recommended_channels_title:I = 0x7f120bfe

.field public static final guild_settings_public_welcome_select_a_channel:I = 0x7f120bff

.field public static final guild_settings_public_welcome_set_description:I = 0x7f120c00

.field public static final guild_settings_public_welcome_settings_text:I = 0x7f120c01

.field public static final guild_settings_public_welcome_title:I = 0x7f120c02

.field public static final guild_settings_public_welcome_update_failure:I = 0x7f120c03

.field public static final guild_settings_server_banner:I = 0x7f120c04

.field public static final guild_settings_server_invite_background:I = 0x7f120c05

.field public static final guild_settings_splash_info:I = 0x7f120c06

.field public static final guild_settings_splash_recommend:I = 0x7f120c07

.field public static final guild_settings_title_server_widget:I = 0x7f120c08

.field public static final guild_settings_widget_embed_help:I = 0x7f120c09

.field public static final guild_settings_widget_enable_widget:I = 0x7f120c0a

.field public static final guild_sidebar_a11y_label:I = 0x7f120c0b

.field public static final guild_sidebar_announcement_channel_a11y_label:I = 0x7f120c0c

.field public static final guild_sidebar_announcement_channel_a11y_label_with_mentions:I = 0x7f120c0d

.field public static final guild_sidebar_announcement_channel_a11y_label_with_unreads:I = 0x7f120c0e

.field public static final guild_sidebar_default_channel_a11y_label:I = 0x7f120c0f

.field public static final guild_sidebar_default_channel_a11y_label_with_mentions:I = 0x7f120c10

.field public static final guild_sidebar_default_channel_a11y_label_with_unreads:I = 0x7f120c11

.field public static final guild_sidebar_store_channel_a11y_label:I = 0x7f120c12

.field public static final guild_sidebar_voice_channel_a11y_label:I = 0x7f120c13

.field public static final guild_sidebar_voice_channel_a11y_label_with_limit:I = 0x7f120c14

.field public static final guild_sidebar_voice_channel_a11y_label_with_users:I = 0x7f120c15

.field public static final guild_subscription_purchase_modal_activated_close_button:I = 0x7f120c16

.field public static final guild_subscription_purchase_modal_activated_description:I = 0x7f120c17

.field public static final guild_subscription_purchase_modal_activated_description_generic_guild:I = 0x7f120c18

.field public static final guild_subscription_purchase_modal_activated_description_mobile1:I = 0x7f120c19

.field public static final guild_subscription_purchase_modal_activated_description_mobile2:I = 0x7f120c1a

.field public static final guild_subscription_purchase_modal_activated_description_no_application:I = 0x7f120c1b

.field public static final guild_subscription_purchase_modal_apple:I = 0x7f120c1c

.field public static final guild_subscription_purchase_modal_counter:I = 0x7f120c1d

.field public static final guild_subscription_purchase_modal_external:I = 0x7f120c1e

.field public static final guild_subscription_purchase_modal_footer_discount:I = 0x7f120c1f

.field public static final guild_subscription_purchase_modal_footer_upsell:I = 0x7f120c20

.field public static final guild_subscription_purchase_modal_footer_upsell_trial:I = 0x7f120c21

.field public static final guild_subscription_purchase_modal_invoice_row_content:I = 0x7f120c22

.field public static final guild_subscription_purchase_modal_payment_source_tooltip:I = 0x7f120c23

.field public static final guild_subscription_purchase_modal_purchase_details_header:I = 0x7f120c24

.field public static final guild_subscription_purchase_modal_step_select_description:I = 0x7f120c25

.field public static final guild_subscription_purchase_modal_submit:I = 0x7f120c26

.field public static final guild_subscription_purchase_modal_subtotal:I = 0x7f120c27

.field public static final guild_subscription_purchase_modal_transferred_description:I = 0x7f120c28

.field public static final guild_subscription_purchase_modal_transferred_description_generic_guild:I = 0x7f120c29

.field public static final guild_subscription_purchase_modal_transferred_description_mobile1:I = 0x7f120c2a

.field public static final guild_subscription_purchase_modal_transferred_description_mobile2:I = 0x7f120c2b

.field public static final guild_subscription_purchase_modal_unused_slot_notice:I = 0x7f120c2c

.field public static final guild_template_based_on:I = 0x7f120c2d

.field public static final guild_template_create_discord:I = 0x7f120c2e

.field public static final guild_template_default_server_name_campus_clubs:I = 0x7f120c2f

.field public static final guild_template_default_server_name_classroom:I = 0x7f120c30

.field public static final guild_template_default_server_name_create_from_scratch:I = 0x7f120c31

.field public static final guild_template_default_server_name_creators_hobbies:I = 0x7f120c32

.field public static final guild_template_default_server_name_friends_family:I = 0x7f120c33

.field public static final guild_template_default_server_name_global_communities:I = 0x7f120c34

.field public static final guild_template_default_server_name_local_communities:I = 0x7f120c35

.field public static final guild_template_default_server_name_study_groups:I = 0x7f120c36

.field public static final guild_template_embed_view_in_app:I = 0x7f120c37

.field public static final guild_template_header_clubs:I = 0x7f120c38

.field public static final guild_template_header_community:I = 0x7f120c39

.field public static final guild_template_header_create:I = 0x7f120c3a

.field public static final guild_template_header_creator:I = 0x7f120c3b

.field public static final guild_template_header_friend:I = 0x7f120c3c

.field public static final guild_template_header_gaming:I = 0x7f120c3d

.field public static final guild_template_header_school_club:I = 0x7f120c3e

.field public static final guild_template_header_study:I = 0x7f120c3f

.field public static final guild_template_invalid_subtitle:I = 0x7f120c40

.field public static final guild_template_invalid_title:I = 0x7f120c41

.field public static final guild_template_mobile_invalid_cta:I = 0x7f120c42

.field public static final guild_template_mobile_invalid_error:I = 0x7f120c43

.field public static final guild_template_modal_channels_descriptions:I = 0x7f120c44

.field public static final guild_template_modal_channels_header:I = 0x7f120c45

.field public static final guild_template_modal_channels_tip:I = 0x7f120c46

.field public static final guild_template_modal_description_campus_clubs:I = 0x7f120c47

.field public static final guild_template_modal_description_classroom:I = 0x7f120c48

.field public static final guild_template_modal_description_create_from_scratch:I = 0x7f120c49

.field public static final guild_template_modal_description_creators_hobbies:I = 0x7f120c4a

.field public static final guild_template_modal_description_friends_family:I = 0x7f120c4b

.field public static final guild_template_modal_description_global_communities:I = 0x7f120c4c

.field public static final guild_template_modal_description_league_clubs:I = 0x7f120c4d

.field public static final guild_template_modal_description_local_communities:I = 0x7f120c4e

.field public static final guild_template_modal_description_study_groups:I = 0x7f120c4f

.field public static final guild_template_modal_error_title:I = 0x7f120c50

.field public static final guild_template_modal_roles_description:I = 0x7f120c51

.field public static final guild_template_modal_roles_header:I = 0x7f120c52

.field public static final guild_template_modal_roles_header2:I = 0x7f120c53

.field public static final guild_template_modal_title:I = 0x7f120c54

.field public static final guild_template_modal_title_campus_clubs:I = 0x7f120c55

.field public static final guild_template_modal_title_classroom:I = 0x7f120c56

.field public static final guild_template_modal_title_create_from_scratch:I = 0x7f120c57

.field public static final guild_template_modal_title_creators_hobbies:I = 0x7f120c58

.field public static final guild_template_modal_title_friends_family:I = 0x7f120c59

.field public static final guild_template_modal_title_global_communities:I = 0x7f120c5a

.field public static final guild_template_modal_title_league_clubs:I = 0x7f120c5b

.field public static final guild_template_modal_title_local_communities:I = 0x7f120c5c

.field public static final guild_template_modal_title_study_groups:I = 0x7f120c5d

.field public static final guild_template_name_announcements:I = 0x7f120c5e

.field public static final guild_template_name_category_information:I = 0x7f120c5f

.field public static final guild_template_name_category_text:I = 0x7f120c60

.field public static final guild_template_name_category_voice:I = 0x7f120c61

.field public static final guild_template_name_clips_and_highlights:I = 0x7f120c62

.field public static final guild_template_name_events:I = 0x7f120c63

.field public static final guild_template_name_game:I = 0x7f120c64

.field public static final guild_template_name_game_new:I = 0x7f120c65

.field public static final guild_template_name_game_room:I = 0x7f120c66

.field public static final guild_template_name_general:I = 0x7f120c67

.field public static final guild_template_name_giveaways:I = 0x7f120c68

.field public static final guild_template_name_homework:I = 0x7f120c69

.field public static final guild_template_name_homework_help:I = 0x7f120c6a

.field public static final guild_template_name_ideas_and_feedback:I = 0x7f120c6b

.field public static final guild_template_name_meeting_plans:I = 0x7f120c6c

.field public static final guild_template_name_meetups:I = 0x7f120c6d

.field public static final guild_template_name_memes:I = 0x7f120c6e

.field public static final guild_template_name_music:I = 0x7f120c6f

.field public static final guild_template_name_notes:I = 0x7f120c70

.field public static final guild_template_name_notes_resources:I = 0x7f120c71

.field public static final guild_template_name_off_topic:I = 0x7f120c72

.field public static final guild_template_name_random:I = 0x7f120c73

.field public static final guild_template_name_resources:I = 0x7f120c74

.field public static final guild_template_name_session_planning:I = 0x7f120c75

.field public static final guild_template_name_social:I = 0x7f120c76

.field public static final guild_template_name_strategy:I = 0x7f120c77

.field public static final guild_template_name_voice_community_hangout:I = 0x7f120c78

.field public static final guild_template_name_voice_gaming:I = 0x7f120c79

.field public static final guild_template_name_voice_general:I = 0x7f120c7a

.field public static final guild_template_name_voice_lobby:I = 0x7f120c7b

.field public static final guild_template_name_voice_lounge:I = 0x7f120c7c

.field public static final guild_template_name_voice_meeting_room:I = 0x7f120c7d

.field public static final guild_template_name_voice_meeting_room_1:I = 0x7f120c7e

.field public static final guild_template_name_voice_meeting_room_2:I = 0x7f120c7f

.field public static final guild_template_name_voice_stream_room:I = 0x7f120c80

.field public static final guild_template_name_voice_study_room:I = 0x7f120c81

.field public static final guild_template_name_welcome:I = 0x7f120c82

.field public static final guild_template_name_welcome_and_rules:I = 0x7f120c83

.field public static final guild_template_open:I = 0x7f120c84

.field public static final guild_template_preview_description:I = 0x7f120c85

.field public static final guild_template_preview_protip_body:I = 0x7f120c86

.field public static final guild_template_preview_title:I = 0x7f120c87

.field public static final guild_template_resolved_embed_title:I = 0x7f120c88

.field public static final guild_template_resolving_title:I = 0x7f120c89

.field public static final guild_template_selector_description:I = 0x7f120c8a

.field public static final guild_template_selector_option_header:I = 0x7f120c8b

.field public static final guild_template_selector_suggestion:I = 0x7f120c8c

.field public static final guild_template_selector_title:I = 0x7f120c8d

.field public static final guild_template_settings_description:I = 0x7f120c8e

.field public static final guild_template_setup_discord:I = 0x7f120c8f

.field public static final guild_template_usages:I = 0x7f120c90

.field public static final guild_template_usages_by_creator2:I = 0x7f120c91

.field public static final guild_template_usages_no_bold:I = 0x7f120c92

.field public static final guild_templates:I = 0x7f120c93

.field public static final guild_templates_create_link:I = 0x7f120c94

.field public static final guild_templates_default_template_name:I = 0x7f120c95

.field public static final guild_templates_delete_description:I = 0x7f120c96

.field public static final guild_templates_delete_template:I = 0x7f120c97

.field public static final guild_templates_delete_template_link:I = 0x7f120c98

.field public static final guild_templates_form_description_channels:I = 0x7f120c99

.field public static final guild_templates_form_description_members:I = 0x7f120c9a

.field public static final guild_templates_form_description_messages:I = 0x7f120c9b

.field public static final guild_templates_form_description_perks:I = 0x7f120c9c

.field public static final guild_templates_form_description_roles:I = 0x7f120c9d

.field public static final guild_templates_form_description_settings:I = 0x7f120c9e

.field public static final guild_templates_form_description_will_copy:I = 0x7f120c9f

.field public static final guild_templates_form_description_wont_copy:I = 0x7f120ca0

.field public static final guild_templates_form_error_name_required:I = 0x7f120ca1

.field public static final guild_templates_form_label_description:I = 0x7f120ca2

.field public static final guild_templates_form_label_icon:I = 0x7f120ca3

.field public static final guild_templates_form_label_link:I = 0x7f120ca4

.field public static final guild_templates_form_label_name:I = 0x7f120ca5

.field public static final guild_templates_form_label_server_name_classroom:I = 0x7f120ca6

.field public static final guild_templates_form_label_server_name_create_from_scratch:I = 0x7f120ca7

.field public static final guild_templates_form_label_server_name_creators_hobbies:I = 0x7f120ca8

.field public static final guild_templates_form_label_server_name_friends_family:I = 0x7f120ca9

.field public static final guild_templates_form_label_server_name_global_communities:I = 0x7f120caa

.field public static final guild_templates_form_label_server_name_local_communities:I = 0x7f120cab

.field public static final guild_templates_form_label_server_name_study_groups:I = 0x7f120cac

.field public static final guild_templates_form_placeholder_description:I = 0x7f120cad

.field public static final guild_templates_form_placeholder_name:I = 0x7f120cae

.field public static final guild_templates_last_sync:I = 0x7f120caf

.field public static final guild_templates_preview_template:I = 0x7f120cb0

.field public static final guild_templates_promotion_tooltip:I = 0x7f120cb1

.field public static final guild_templates_promotion_tooltip_aria_label:I = 0x7f120cb2

.field public static final guild_templates_sync_description:I = 0x7f120cb3

.field public static final guild_templates_sync_template:I = 0x7f120cb4

.field public static final guild_templates_template_sync:I = 0x7f120cb5

.field public static final guild_templates_unsynced_tooltip:I = 0x7f120cb6

.field public static final guild_templates_unsynced_warning:I = 0x7f120cb7

.field public static final guild_tooltip_a11y_label:I = 0x7f120cb8

.field public static final guild_unavailable_body:I = 0x7f120cb9

.field public static final guild_unavailable_header:I = 0x7f120cba

.field public static final guild_unavailable_title:I = 0x7f120cbb

.field public static final guild_verification_text_account_age:I = 0x7f120cbc

.field public static final guild_verification_text_member_age:I = 0x7f120cbd

.field public static final guild_verification_text_not_claimed:I = 0x7f120cbe

.field public static final guild_verification_text_not_phone_verified:I = 0x7f120cbf

.field public static final guild_verification_text_not_verified:I = 0x7f120cc0

.field public static final guild_verification_voice_account_age:I = 0x7f120cc1

.field public static final guild_verification_voice_header:I = 0x7f120cc2

.field public static final guild_verification_voice_member_age:I = 0x7f120cc3

.field public static final guild_verification_voice_not_claimed:I = 0x7f120cc4

.field public static final guild_verification_voice_not_phone_verified:I = 0x7f120cc5

.field public static final guild_verification_voice_not_verified:I = 0x7f120cc6

.field public static final guild_verified:I = 0x7f120cc7

.field public static final guild_verified_and_partnered:I = 0x7f120cc8

.field public static final guild_video_call_marketing_popout_body:I = 0x7f120cc9

.field public static final guild_video_call_marketing_popout_header:I = 0x7f120cca

.field public static final guild_voice_channel_empty_body_mobile:I = 0x7f120ccb

.field public static final guilds_bar_a11y_label:I = 0x7f120ccc

.field public static final hardware_acceleration:I = 0x7f120ccd

.field public static final hardware_acceleration_help_text:I = 0x7f120cce

.field public static final help:I = 0x7f120ccf

.field public static final help_clear_permissions:I = 0x7f120cd0

.field public static final help_desk:I = 0x7f120cd1

.field public static final help_missing_manage_roles_permission:I = 0x7f120cd2

.field public static final help_missing_permission:I = 0x7f120cd3

.field public static final help_role_locked:I = 0x7f120cd4

.field public static final help_role_locked_mine:I = 0x7f120cd5

.field public static final help_roles_description:I = 0x7f120cd6

.field public static final help_singular_permission:I = 0x7f120cd7

.field public static final hi:I = 0x7f120cd8

.field public static final hide:I = 0x7f120cd9

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120cda

.field public static final hide_chat:I = 0x7f120cdb

.field public static final hide_instant_invites_description:I = 0x7f120cdc

.field public static final hide_instant_invites_label:I = 0x7f120cdd

.field public static final hide_muted:I = 0x7f120cde

.field public static final hide_muted_channels:I = 0x7f120cdf

.field public static final hide_navigation:I = 0x7f120ce0

.field public static final hide_personal_information_description:I = 0x7f120ce1

.field public static final hide_personal_information_label:I = 0x7f120ce2

.field public static final hold_up:I = 0x7f120ce3

.field public static final home:I = 0x7f120ce4

.field public static final how_to_invite_others:I = 0x7f120ce5

.field public static final hr:I = 0x7f120ce6

.field public static final hu:I = 0x7f120ce7

.field public static final humanize_duration_a_few_seconds:I = 0x7f120ce8

.field public static final humanize_duration_days:I = 0x7f120ce9

.field public static final humanize_duration_hours:I = 0x7f120cea

.field public static final humanize_duration_minutes:I = 0x7f120ceb

.field public static final humanize_duration_seconds:I = 0x7f120cec

.field public static final hypesquad_attendee_cta:I = 0x7f120ced

.field public static final hypesquad_badge_tooltip:I = 0x7f120cee

.field public static final hypesquad_description_house_1:I = 0x7f120cef

.field public static final hypesquad_description_house_2:I = 0x7f120cf0

.field public static final hypesquad_description_house_3:I = 0x7f120cf1

.field public static final hypesquad_error_body:I = 0x7f120cf2

.field public static final hypesquad_error_heading:I = 0x7f120cf3

.field public static final hypesquad_heading:I = 0x7f120cf4

.field public static final hypesquad_heading_existing_member:I = 0x7f120cf5

.field public static final hypesquad_house_1:I = 0x7f120cf6

.field public static final hypesquad_house_2:I = 0x7f120cf7

.field public static final hypesquad_house_3:I = 0x7f120cf8

.field public static final hypesquad_join:I = 0x7f120cf9

.field public static final hypesquad_leave_action:I = 0x7f120cfa

.field public static final hypesquad_leave_error:I = 0x7f120cfb

.field public static final hypesquad_leave_prompt:I = 0x7f120cfc

.field public static final hypesquad_membership_heading:I = 0x7f120cfd

.field public static final hypesquad_newsletter_warning:I = 0x7f120cfe

.field public static final hypesquad_online_badge_tooltip:I = 0x7f120cff

.field public static final hypesquad_perk_description_newsletter:I = 0x7f120d00

.field public static final hypesquad_perk_description_profile_badge:I = 0x7f120d01

.field public static final hypesquad_perk_description_squad_challenges:I = 0x7f120d02

.field public static final hypesquad_perk_title_newsletter:I = 0x7f120d03

.field public static final hypesquad_perk_title_profile_badge:I = 0x7f120d04

.field public static final hypesquad_perk_title_squad_challenges:I = 0x7f120d05

.field public static final hypesquad_perks_heading:I = 0x7f120d06

.field public static final hypesquad_question_0_prompt:I = 0x7f120d07

.field public static final hypesquad_question_0_response_a:I = 0x7f120d08

.field public static final hypesquad_question_0_response_b:I = 0x7f120d09

.field public static final hypesquad_question_0_response_c:I = 0x7f120d0a

.field public static final hypesquad_question_0_response_d:I = 0x7f120d0b

.field public static final hypesquad_question_10_prompt:I = 0x7f120d0c

.field public static final hypesquad_question_10_response_a:I = 0x7f120d0d

.field public static final hypesquad_question_10_response_b:I = 0x7f120d0e

.field public static final hypesquad_question_10_response_c:I = 0x7f120d0f

.field public static final hypesquad_question_10_response_d:I = 0x7f120d10

.field public static final hypesquad_question_11_prompt:I = 0x7f120d11

.field public static final hypesquad_question_11_response_a:I = 0x7f120d12

.field public static final hypesquad_question_11_response_b:I = 0x7f120d13

.field public static final hypesquad_question_11_response_c:I = 0x7f120d14

.field public static final hypesquad_question_11_response_d:I = 0x7f120d15

.field public static final hypesquad_question_12_prompt:I = 0x7f120d16

.field public static final hypesquad_question_12_response_a:I = 0x7f120d17

.field public static final hypesquad_question_12_response_b:I = 0x7f120d18

.field public static final hypesquad_question_12_response_c:I = 0x7f120d19

.field public static final hypesquad_question_12_response_d:I = 0x7f120d1a

.field public static final hypesquad_question_13_prompt:I = 0x7f120d1b

.field public static final hypesquad_question_13_response_a:I = 0x7f120d1c

.field public static final hypesquad_question_13_response_b:I = 0x7f120d1d

.field public static final hypesquad_question_13_response_c:I = 0x7f120d1e

.field public static final hypesquad_question_13_response_d:I = 0x7f120d1f

.field public static final hypesquad_question_14_prompt:I = 0x7f120d20

.field public static final hypesquad_question_14_response_a:I = 0x7f120d21

.field public static final hypesquad_question_14_response_b:I = 0x7f120d22

.field public static final hypesquad_question_14_response_c:I = 0x7f120d23

.field public static final hypesquad_question_14_response_d:I = 0x7f120d24

.field public static final hypesquad_question_1_prompt:I = 0x7f120d25

.field public static final hypesquad_question_1_response_a:I = 0x7f120d26

.field public static final hypesquad_question_1_response_b:I = 0x7f120d27

.field public static final hypesquad_question_1_response_c:I = 0x7f120d28

.field public static final hypesquad_question_1_response_d:I = 0x7f120d29

.field public static final hypesquad_question_2_prompt:I = 0x7f120d2a

.field public static final hypesquad_question_2_response_a:I = 0x7f120d2b

.field public static final hypesquad_question_2_response_b:I = 0x7f120d2c

.field public static final hypesquad_question_2_response_c:I = 0x7f120d2d

.field public static final hypesquad_question_2_response_d:I = 0x7f120d2e

.field public static final hypesquad_question_3_prompt:I = 0x7f120d2f

.field public static final hypesquad_question_3_response_a:I = 0x7f120d30

.field public static final hypesquad_question_3_response_b:I = 0x7f120d31

.field public static final hypesquad_question_3_response_c:I = 0x7f120d32

.field public static final hypesquad_question_3_response_d:I = 0x7f120d33

.field public static final hypesquad_question_4_prompt:I = 0x7f120d34

.field public static final hypesquad_question_4_response_a:I = 0x7f120d35

.field public static final hypesquad_question_4_response_b:I = 0x7f120d36

.field public static final hypesquad_question_4_response_c:I = 0x7f120d37

.field public static final hypesquad_question_4_response_d:I = 0x7f120d38

.field public static final hypesquad_question_5_prompt:I = 0x7f120d39

.field public static final hypesquad_question_5_response_a:I = 0x7f120d3a

.field public static final hypesquad_question_5_response_b:I = 0x7f120d3b

.field public static final hypesquad_question_5_response_c:I = 0x7f120d3c

.field public static final hypesquad_question_5_response_d:I = 0x7f120d3d

.field public static final hypesquad_question_6_prompt:I = 0x7f120d3e

.field public static final hypesquad_question_6_response_a:I = 0x7f120d3f

.field public static final hypesquad_question_6_response_b:I = 0x7f120d40

.field public static final hypesquad_question_6_response_c:I = 0x7f120d41

.field public static final hypesquad_question_6_response_d:I = 0x7f120d42

.field public static final hypesquad_question_7_prompt:I = 0x7f120d43

.field public static final hypesquad_question_7_response_a:I = 0x7f120d44

.field public static final hypesquad_question_7_response_b:I = 0x7f120d45

.field public static final hypesquad_question_7_response_c:I = 0x7f120d46

.field public static final hypesquad_question_7_response_d:I = 0x7f120d47

.field public static final hypesquad_question_8_prompt:I = 0x7f120d48

.field public static final hypesquad_question_8_response_a:I = 0x7f120d49

.field public static final hypesquad_question_8_response_b:I = 0x7f120d4a

.field public static final hypesquad_question_8_response_c:I = 0x7f120d4b

.field public static final hypesquad_question_8_response_d:I = 0x7f120d4c

.field public static final hypesquad_question_9_prompt:I = 0x7f120d4d

.field public static final hypesquad_question_9_response_a:I = 0x7f120d4e

.field public static final hypesquad_question_9_response_b:I = 0x7f120d4f

.field public static final hypesquad_question_9_response_c:I = 0x7f120d50

.field public static final hypesquad_question_9_response_d:I = 0x7f120d51

.field public static final hypesquad_quiz_body_house_1:I = 0x7f120d52

.field public static final hypesquad_quiz_body_house_2:I = 0x7f120d53

.field public static final hypesquad_quiz_body_house_3:I = 0x7f120d54

.field public static final hypesquad_quiz_cancel:I = 0x7f120d55

.field public static final hypesquad_quiz_close:I = 0x7f120d56

.field public static final hypesquad_quiz_completed_modal_title:I = 0x7f120d57

.field public static final hypesquad_quiz_error_modal_title:I = 0x7f120d58

.field public static final hypesquad_quiz_next_question:I = 0x7f120d59

.field public static final hypesquad_quiz_ongoing_modal_title:I = 0x7f120d5a

.field public static final hypesquad_quiz_retry:I = 0x7f120d5b

.field public static final hypesquad_quiz_select_answer:I = 0x7f120d5c

.field public static final hypesquad_quiz_show_my_house:I = 0x7f120d5d

.field public static final hypesquad_quiz_title:I = 0x7f120d5e

.field public static final hypesquad_ready_to_rep:I = 0x7f120d5f

.field public static final hypesquad_subheading:I = 0x7f120d60

.field public static final hypesquad_switch_houses_action:I = 0x7f120d61

.field public static final hypesquad_switch_houses_prompt:I = 0x7f120d62

.field public static final hypesquad_unclaimed_account_notice:I = 0x7f120d63

.field public static final hypesquad_unverified_email_notice:I = 0x7f120d64

.field public static final hypesquad_your_house:I = 0x7f120d65

.field public static final i18n_locale_loading_error:I = 0x7f120d66

.field public static final icon_content_description:I = 0x7f120d67

.field public static final image:I = 0x7f120d68

.field public static final image_actions_menu_label:I = 0x7f120d69

.field public static final image_compression:I = 0x7f120d6a

.field public static final image_compression_label:I = 0x7f120d6b

.field public static final image_compression_nitro_upsell:I = 0x7f120d6c

.field public static final images:I = 0x7f120d6d

.field public static final img_alt_attachment_file_type:I = 0x7f120d6e

.field public static final img_alt_emoji:I = 0x7f120d6f

.field public static final img_alt_icon:I = 0x7f120d70

.field public static final img_alt_logo:I = 0x7f120d71

.field public static final in_category:I = 0x7f120d72

.field public static final in_game_voice_settings:I = 0x7f120d73

.field public static final in_the_voice_channel:I = 0x7f120d74

.field public static final inbox:I = 0x7f120d75

.field public static final incoming_call:I = 0x7f120d76

.field public static final incoming_call_ellipsis:I = 0x7f120d77

.field public static final incoming_call_preview_camera:I = 0x7f120d78

.field public static final incoming_friend_request:I = 0x7f120d79

.field public static final incoming_friend_requests_count:I = 0x7f120d7a

.field public static final incoming_video_call:I = 0x7f120d7b

.field public static final incoming_video_call_ellipsis:I = 0x7f120d7c

.field public static final incompatible_browser:I = 0x7f120d7d

.field public static final inivte_modal_no_thanks:I = 0x7f120d7e

.field public static final inline_attachment_media:I = 0x7f120d7f

.field public static final inline_attachment_media_help:I = 0x7f120d80

.field public static final inline_embed_media:I = 0x7f120d81

.field public static final inline_media_label:I = 0x7f120d82

.field public static final input_device:I = 0x7f120d83

.field public static final input_mode_ptt:I = 0x7f120d84

.field public static final input_mode_ptt_limited:I = 0x7f120d85

.field public static final input_mode_ptt_release_delay:I = 0x7f120d86

.field public static final input_mode_vad:I = 0x7f120d87

.field public static final instagram:I = 0x7f120d88

.field public static final install_location_main:I = 0x7f120d89

.field public static final instant_invite:I = 0x7f120d8a

.field public static final instant_invite_accept:I = 0x7f120d8b

.field public static final instant_invite_accepting:I = 0x7f120d8c

.field public static final instant_invite_ask_for_new_invite:I = 0x7f120d8d

.field public static final instant_invite_ask_user_for_new_invite:I = 0x7f120d8e

.field public static final instant_invite_banned:I = 0x7f120d8f

.field public static final instant_invite_banned_info:I = 0x7f120d90

.field public static final instant_invite_expired:I = 0x7f120d91

.field public static final instant_invite_expires:I = 0x7f120d92

.field public static final instant_invite_failed_to_generate:I = 0x7f120d93

.field public static final instant_invite_friends:I = 0x7f120d94

.field public static final instant_invite_friends_description:I = 0x7f120d95

.field public static final instant_invite_generated_by_widget:I = 0x7f120d96

.field public static final instant_invite_guild_by_user:I = 0x7f120d97

.field public static final instant_invite_guild_members_online:I = 0x7f120d98

.field public static final instant_invite_guild_members_total:I = 0x7f120d99

.field public static final instant_invite_hidden:I = 0x7f120d9a

.field public static final instant_invite_invalid_channel:I = 0x7f120d9b

.field public static final instant_invite_invite_code:I = 0x7f120d9c

.field public static final instant_invite_inviter:I = 0x7f120d9d

.field public static final instant_invite_looks_like:I = 0x7f120d9e

.field public static final instant_invite_not_allowed:I = 0x7f120d9f

.field public static final instant_invite_resolved_title:I = 0x7f120da0

.field public static final instant_invite_resolving:I = 0x7f120da1

.field public static final instant_invite_uses:I = 0x7f120da2

.field public static final instant_invite_you_are_already_a_member_of:I = 0x7f120da3

.field public static final instant_invite_you_have_been_invited_to_join:I = 0x7f120da4

.field public static final instant_invite_you_have_been_invited_to_join_by_user:I = 0x7f120da5

.field public static final instant_invite_you_have_been_invited_to_join_group_dm:I = 0x7f120da6

.field public static final instant_invite_you_have_joined:I = 0x7f120da7

.field public static final instant_invites:I = 0x7f120da8

.field public static final integration_added_date:I = 0x7f120da9

.field public static final integration_added_user:I = 0x7f120daa

.field public static final integration_added_user_date:I = 0x7f120dab

.field public static final integration_created_date:I = 0x7f120dac

.field public static final integration_created_user_date:I = 0x7f120dad

.field public static final integration_settings:I = 0x7f120dae

.field public static final integrations:I = 0x7f120daf

.field public static final integrations_account_description:I = 0x7f120db0

.field public static final integrations_application_added_by:I = 0x7f120db1

.field public static final integrations_application_bot:I = 0x7f120db2

.field public static final integrations_application_bot_name:I = 0x7f120db3

.field public static final integrations_application_button:I = 0x7f120db4

.field public static final integrations_application_denied_permissions:I = 0x7f120db5

.field public static final integrations_application_granted_permissions:I = 0x7f120db6

.field public static final integrations_application_no_bot:I = 0x7f120db7

.field public static final integrations_application_no_webhooks:I = 0x7f120db8

.field public static final integrations_application_remove:I = 0x7f120db9

.field public static final integrations_application_remove_body:I = 0x7f120dba

.field public static final integrations_application_remove_error:I = 0x7f120dbb

.field public static final integrations_application_remove_error_title:I = 0x7f120dbc

.field public static final integrations_application_remove_no_permissions:I = 0x7f120dbd

.field public static final integrations_application_remove_summary:I = 0x7f120dbe

.field public static final integrations_application_remove_title:I = 0x7f120dbf

.field public static final integrations_application_section:I = 0x7f120dc0

.field public static final integrations_application_verified_bot:I = 0x7f120dc1

.field public static final integrations_application_webhooks:I = 0x7f120dc2

.field public static final integrations_channel_following:I = 0x7f120dc3

.field public static final integrations_channel_following_button:I = 0x7f120dc4

.field public static final integrations_channel_following_description:I = 0x7f120dc5

.field public static final integrations_channel_following_empty:I = 0x7f120dc6

.field public static final integrations_channel_following_empty_button:I = 0x7f120dc7

.field public static final integrations_channel_following_section:I = 0x7f120dc8

.field public static final integrations_channel_following_summary:I = 0x7f120dc9

.field public static final integrations_channel_following_title:I = 0x7f120dca

.field public static final integrations_disable:I = 0x7f120dcb

.field public static final integrations_enable:I = 0x7f120dcc

.field public static final integrations_enabled:I = 0x7f120dcd

.field public static final integrations_followed_channel_delete:I = 0x7f120dce

.field public static final integrations_followed_channel_delete_body:I = 0x7f120dcf

.field public static final integrations_followed_channel_delete_title:I = 0x7f120dd0

.field public static final integrations_followed_channel_dest_channel:I = 0x7f120dd1

.field public static final integrations_followed_channel_error_deleting:I = 0x7f120dd2

.field public static final integrations_followed_channel_guild_source:I = 0x7f120dd3

.field public static final integrations_followed_channel_name:I = 0x7f120dd4

.field public static final integrations_last_sync:I = 0x7f120dd5

.field public static final integrations_overview:I = 0x7f120dd6

.field public static final integrations_overview_description_channel:I = 0x7f120dd7

.field public static final integrations_overview_description_guild:I = 0x7f120dd8

.field public static final integrations_overview_no_applications:I = 0x7f120dd9

.field public static final integrations_twitch:I = 0x7f120dda

.field public static final integrations_twitch_button:I = 0x7f120ddb

.field public static final integrations_twitch_description:I = 0x7f120ddc

.field public static final integrations_twitch_empty_button:I = 0x7f120ddd

.field public static final integrations_twitch_empty_summary:I = 0x7f120dde

.field public static final integrations_twitch_help:I = 0x7f120ddf

.field public static final integrations_twitch_summary:I = 0x7f120de0

.field public static final integrations_webhook_copied_url:I = 0x7f120de1

.field public static final integrations_webhook_copy_url:I = 0x7f120de2

.field public static final integrations_webhook_delete:I = 0x7f120de3

.field public static final integrations_webhooks:I = 0x7f120de4

.field public static final integrations_webhooks_button:I = 0x7f120de5

.field public static final integrations_webhooks_count:I = 0x7f120de6

.field public static final integrations_webhooks_create:I = 0x7f120de7

.field public static final integrations_webhooks_description:I = 0x7f120de8

.field public static final integrations_webhooks_empty:I = 0x7f120de9

.field public static final integrations_webhooks_empty_button:I = 0x7f120dea

.field public static final integrations_webhooks_summary:I = 0x7f120deb

.field public static final integrations_youtube:I = 0x7f120dec

.field public static final integrations_youtube_button:I = 0x7f120ded

.field public static final integrations_youtube_description:I = 0x7f120dee

.field public static final integrations_youtube_empty_button:I = 0x7f120def

.field public static final integrations_youtube_empty_summary:I = 0x7f120df0

.field public static final integrations_youtube_help:I = 0x7f120df1

.field public static final integrations_youtube_summary:I = 0x7f120df2

.field public static final interaction_required_body:I = 0x7f120df3

.field public static final interaction_required_title:I = 0x7f120df4

.field public static final internal_server_error:I = 0x7f120df5

.field public static final invalid_animated_emoji_body:I = 0x7f120df6

.field public static final invalid_animated_emoji_body_upgrade:I = 0x7f120df7

.field public static final invalid_attachments_failure:I = 0x7f120df8

.field public static final invalid_external_emoji_body:I = 0x7f120df9

.field public static final invalid_external_emoji_body_upgrade:I = 0x7f120dfa

.field public static final invalid_invite_link_error:I = 0x7f120dfb

.field public static final invalid_text_channel:I = 0x7f120dfc

.field public static final invalid_voice_channel:I = 0x7f120dfd

.field public static final invite_button_body_in_guild:I = 0x7f120dfe

.field public static final invite_button_expired:I = 0x7f120dff

.field public static final invite_button_invalid:I = 0x7f120e00

.field public static final invite_button_invalid_owner:I = 0x7f120e01

.field public static final invite_button_resolving:I = 0x7f120e02

.field public static final invite_button_stream_ended:I = 0x7f120e03

.field public static final invite_button_stream_ended_streamer:I = 0x7f120e04

.field public static final invite_button_stream_watching:I = 0x7f120e05

.field public static final invite_button_streamer:I = 0x7f120e06

.field public static final invite_button_streaming:I = 0x7f120e07

.field public static final invite_button_streaming_subtext:I = 0x7f120e08

.field public static final invite_button_title_invited:I = 0x7f120e09

.field public static final invite_button_title_invited_group_dm:I = 0x7f120e0a

.field public static final invite_button_title_invited_invalid:I = 0x7f120e0b

.field public static final invite_button_title_invited_stream:I = 0x7f120e0c

.field public static final invite_button_title_invited_to_play:I = 0x7f120e0d

.field public static final invite_button_title_invited_voice_channel:I = 0x7f120e0e

.field public static final invite_button_title_inviter:I = 0x7f120e0f

.field public static final invite_button_title_inviter_group_dm:I = 0x7f120e10

.field public static final invite_button_title_inviter_invalid:I = 0x7f120e11

.field public static final invite_button_title_inviter_stream:I = 0x7f120e12

.field public static final invite_button_title_inviter_voice_channel:I = 0x7f120e13

.field public static final invite_button_title_streaming:I = 0x7f120e14

.field public static final invite_copied:I = 0x7f120e15

.field public static final invite_copy_invite_link_header_mobile:I = 0x7f120e16

.field public static final invite_copy_share_link_header_mobile:I = 0x7f120e17

.field public static final invite_edit_link:I = 0x7f120e18

.field public static final invite_embed_ask_to_join:I = 0x7f120e19

.field public static final invite_embed_full_group:I = 0x7f120e1a

.field public static final invite_embed_game_has_ended:I = 0x7f120e1b

.field public static final invite_embed_game_invite:I = 0x7f120e1c

.field public static final invite_embed_in_group:I = 0x7f120e1d

.field public static final invite_embed_invite_to_join:I = 0x7f120e1e

.field public static final invite_embed_invite_to_join_group:I = 0x7f120e1f

.field public static final invite_embed_invite_to_listen:I = 0x7f120e20

.field public static final invite_embed_invite_to_watch:I = 0x7f120e21

.field public static final invite_embed_join_via_android:I = 0x7f120e22

.field public static final invite_embed_join_via_desktop_app:I = 0x7f120e23

.field public static final invite_embed_join_via_ios:I = 0x7f120e24

.field public static final invite_embed_join_via_xbox:I = 0x7f120e25

.field public static final invite_embed_joined:I = 0x7f120e26

.field public static final invite_embed_listen_has_ended:I = 0x7f120e27

.field public static final invite_embed_listening_invite:I = 0x7f120e28

.field public static final invite_embed_num_open_slots:I = 0x7f120e29

.field public static final invite_embed_send_invite:I = 0x7f120e2a

.field public static final invite_embed_watch_has_ended:I = 0x7f120e2b

.field public static final invite_empty_body:I = 0x7f120e2c

.field public static final invite_empty_title:I = 0x7f120e2d

.field public static final invite_expired_subtext_mobile:I = 0x7f120e2e

.field public static final invite_expires_days:I = 0x7f120e2f

.field public static final invite_expires_days_or_uses:I = 0x7f120e30

.field public static final invite_expires_hours:I = 0x7f120e31

.field public static final invite_expires_hours_or_uses:I = 0x7f120e32

.field public static final invite_expires_minutes:I = 0x7f120e33

.field public static final invite_expires_minutes_or_uses:I = 0x7f120e34

.field public static final invite_expires_never:I = 0x7f120e35

.field public static final invite_expires_uses:I = 0x7f120e36

.field public static final invite_footer_link_header:I = 0x7f120e37

.field public static final invite_friend_modal_invite:I = 0x7f120e38

.field public static final invite_friend_modal_loading:I = 0x7f120e39

.field public static final invite_friend_modal_no_results:I = 0x7f120e3a

.field public static final invite_friend_modal_retry:I = 0x7f120e3b

.field public static final invite_friend_modal_sent:I = 0x7f120e3c

.field public static final invite_friend_modal_title:I = 0x7f120e3d

.field public static final invite_friends:I = 0x7f120e3e

.field public static final invite_invalid_cta:I = 0x7f120e3f

.field public static final invite_invalid_error:I = 0x7f120e40

.field public static final invite_link_copied:I = 0x7f120e41

.field public static final invite_link_example_full:I = 0x7f120e42

.field public static final invite_link_example_simple:I = 0x7f120e43

.field public static final invite_link_example_verified:I = 0x7f120e44

.field public static final invite_links_expire_after_1_day:I = 0x7f120e45

.field public static final invite_members:I = 0x7f120e46

.field public static final invite_modal_button:I = 0x7f120e47

.field public static final invite_modal_error_default:I = 0x7f120e48

.field public static final invite_modal_error_title:I = 0x7f120e49

.field public static final invite_modal_open_button:I = 0x7f120e4a

.field public static final invite_never_expires_subtext_mobile:I = 0x7f120e4b

.field public static final invite_no_thanks:I = 0x7f120e4c

.field public static final invite_notice_message:I = 0x7f120e4d

.field public static final invite_notice_message_part_2:I = 0x7f120e4e

.field public static final invite_people:I = 0x7f120e4f

.field public static final invite_pokemon_go_announcements_label_long:I = 0x7f120e50

.field public static final invite_pokemon_go_announcements_label_short:I = 0x7f120e51

.field public static final invite_pokemon_go_friendship_label_long:I = 0x7f120e52

.field public static final invite_pokemon_go_friendship_label_short:I = 0x7f120e53

.field public static final invite_pokemon_go_strategy_label_long:I = 0x7f120e54

.field public static final invite_pokemon_go_strategy_label_short:I = 0x7f120e55

.field public static final invite_private_call_heads_up:I = 0x7f120e56

.field public static final invite_search_for_friends:I = 0x7f120e57

.field public static final invite_sent:I = 0x7f120e58

.field public static final invite_settings_description_no_expiration:I = 0x7f120e59

.field public static final invite_settings_expired_description:I = 0x7f120e5a

.field public static final invite_settings_title:I = 0x7f120e5b

.field public static final invite_share_link_header_mobile:I = 0x7f120e5c

.field public static final invite_share_link_own_server:I = 0x7f120e5d

.field public static final invite_share_link_to_stream:I = 0x7f120e5e

.field public static final invite_step_subtitle:I = 0x7f120e5f

.field public static final invite_step_title:I = 0x7f120e60

.field public static final invite_stream_footer_link_header:I = 0x7f120e61

.field public static final invite_stream_header:I = 0x7f120e62

.field public static final invite_to_server:I = 0x7f120e63

.field public static final invite_url:I = 0x7f120e64

.field public static final invite_voice_channel_join:I = 0x7f120e65

.field public static final invite_voice_empty_mobile:I = 0x7f120e66

.field public static final invite_welcome_heading:I = 0x7f120e67

.field public static final invite_welcome_subheading:I = 0x7f120e68

.field public static final invite_your_friends:I = 0x7f120e69

.field public static final invite_your_friends_channel_mobile:I = 0x7f120e6a

.field public static final invite_your_friends_header_mobile:I = 0x7f120e6b

.field public static final invites:I = 0x7f120e6c

.field public static final ios_app_preview_description:I = 0x7f120e6d

.field public static final ios_automatic_theme:I = 0x7f120e6e

.field public static final ios_automatic_theme_short:I = 0x7f120e6f

.field public static final ios_call_disconnected:I = 0x7f120e70

.field public static final ios_call_ended:I = 0x7f120e71

.field public static final ios_iap_generic_billing_error:I = 0x7f120e72

.field public static final ios_iap_itunes_error:I = 0x7f120e73

.field public static final ios_iap_manage_premium_guild_button:I = 0x7f120e74

.field public static final ios_iap_manage_subscription_button:I = 0x7f120e75

.field public static final ios_iap_manage_subscription_desktop:I = 0x7f120e76

.field public static final ios_iap_manage_subscription_google_play:I = 0x7f120e77

.field public static final ios_iap_restore_subscription_error:I = 0x7f120e78

.field public static final ios_iap_restore_subscription_none_description:I = 0x7f120e79

.field public static final ios_iap_restore_subscription_none_title:I = 0x7f120e7a

.field public static final ios_iap_restore_subscription_success:I = 0x7f120e7b

.field public static final ios_iap_testflight_error:I = 0x7f120e7c

.field public static final ios_media_keyboard_browse_photo:I = 0x7f120e7d

.field public static final ios_media_keyboard_enable_in_settings:I = 0x7f120e7e

.field public static final ios_media_keyboard_more_photos:I = 0x7f120e7f

.field public static final ios_media_keyboard_no_permission:I = 0x7f120e80

.field public static final ios_media_keyboard_no_photos:I = 0x7f120e81

.field public static final ios_notification_see_full:I = 0x7f120e82

.field public static final ios_profile_in_voice_call:I = 0x7f120e83

.field public static final ios_profile_open_voice_channel:I = 0x7f120e84

.field public static final ios_share_suggestions_hint:I = 0x7f120e85

.field public static final ios_share_suggestions_toggle:I = 0x7f120e86

.field public static final ios_stream_participants_hidden:I = 0x7f120e87

.field public static final ios_stream_show_non_video:I = 0x7f120e88

.field public static final ios_view_all:I = 0x7f120e89

.field public static final ip_address_secured:I = 0x7f120e8a

.field public static final ip_authorization_succeeded:I = 0x7f120e8b

.field public static final it:I = 0x7f120e8c

.field public static final item_view_role_description:I = 0x7f120e8d

.field public static final ja:I = 0x7f120e8e

.field public static final join:I = 0x7f120e8f

.field public static final join_afk_channel_body:I = 0x7f120e90

.field public static final join_call:I = 0x7f120e91

.field public static final join_guild:I = 0x7f120e92

.field public static final join_guild_connect:I = 0x7f120e93

.field public static final join_guild_connect_cta:I = 0x7f120e94

.field public static final join_guild_connect_text:I = 0x7f120e95

.field public static final join_guild_description:I = 0x7f120e96

.field public static final join_server_button_body:I = 0x7f120e97

.field public static final join_server_button_cta:I = 0x7f120e98

.field public static final join_server_button_cta_mobile:I = 0x7f120e99

.field public static final join_server_button_cta_mobile_desc:I = 0x7f120e9a

.field public static final join_server_description:I = 0x7f120e9b

.field public static final join_server_description_mobile:I = 0x7f120e9c

.field public static final join_server_description_mobile_refresh:I = 0x7f120e9d

.field public static final join_server_description_nuf:I = 0x7f120e9e

.field public static final join_server_examples:I = 0x7f120e9f

.field public static final join_server_invite_examples_header:I = 0x7f120ea0

.field public static final join_server_invite_examples_mobile_refresh:I = 0x7f120ea1

.field public static final join_server_title:I = 0x7f120ea2

.field public static final join_server_title_mobile_refresh:I = 0x7f120ea3

.field public static final join_stream:I = 0x7f120ea4

.field public static final join_video_call:I = 0x7f120ea5

.field public static final join_video_channel:I = 0x7f120ea6

.field public static final join_voice_call:I = 0x7f120ea7

.field public static final join_voice_channel:I = 0x7f120ea8

.field public static final join_voice_channel_cta:I = 0x7f120ea9

.field public static final joined_guild:I = 0x7f120eaa

.field public static final joining_guild:I = 0x7f120eab

.field public static final joining_voice_call_will_end_current_call_body:I = 0x7f120eac

.field public static final joining_voice_channel_will_end_current_call_body:I = 0x7f120ead

.field public static final joining_will_end_current_call_title:I = 0x7f120eae

.field public static final jump:I = 0x7f120eaf

.field public static final jump_bar_viewing_reply:I = 0x7f120eb0

.field public static final jump_to_channel:I = 0x7f120eb1

.field public static final jump_to_last_unread_message:I = 0x7f120eb2

.field public static final jump_to_message:I = 0x7f120eb3

.field public static final jump_to_original_message:I = 0x7f120eb4

.field public static final jump_to_present:I = 0x7f120eb5

.field public static final keep_permissions:I = 0x7f120eb6

.field public static final keybind_activate_overlay_chat:I = 0x7f120eb7

.field public static final keybind_conflict:I = 0x7f120eb8

.field public static final keybind_description_modal_call_accept:I = 0x7f120eb9

.field public static final keybind_description_modal_call_decline:I = 0x7f120eba

.field public static final keybind_description_modal_call_start:I = 0x7f120ebb

.field public static final keybind_description_modal_create_dm_group:I = 0x7f120ebc

.field public static final keybind_description_modal_create_guild:I = 0x7f120ebd

.field public static final keybind_description_modal_easter_egg:I = 0x7f120ebe

.field public static final keybind_description_modal_focus_text_area:I = 0x7f120ebf

.field public static final keybind_description_modal_jump_to_first_unread:I = 0x7f120ec0

.field public static final keybind_description_modal_mark_channel_read:I = 0x7f120ec1

.field public static final keybind_description_modal_mark_server_read:I = 0x7f120ec2

.field public static final keybind_description_modal_mark_top_inbox_channel_read:I = 0x7f120ec3

.field public static final keybind_description_modal_navigate_channels:I = 0x7f120ec4

.field public static final keybind_description_modal_navigate_servers:I = 0x7f120ec5

.field public static final keybind_description_modal_quickswitcher:I = 0x7f120ec6

.field public static final keybind_description_modal_scroll_chat:I = 0x7f120ec7

.field public static final keybind_description_modal_search:I = 0x7f120ec8

.field public static final keybind_description_modal_search_emojis:I = 0x7f120ec9

.field public static final keybind_description_modal_search_gifs:I = 0x7f120eca

.field public static final keybind_description_modal_subtitle:I = 0x7f120ecb

.field public static final keybind_description_modal_title:I = 0x7f120ecc

.field public static final keybind_description_modal_toggle_deafen:I = 0x7f120ecd

.field public static final keybind_description_modal_toggle_help:I = 0x7f120ece

.field public static final keybind_description_modal_toggle_inbox:I = 0x7f120ecf

.field public static final keybind_description_modal_toggle_mute:I = 0x7f120ed0

.field public static final keybind_description_modal_toggle_pins:I = 0x7f120ed1

.field public static final keybind_description_modal_toggle_previous_guild:I = 0x7f120ed2

.field public static final keybind_description_modal_toggle_users:I = 0x7f120ed3

.field public static final keybind_description_modal_unread_channels:I = 0x7f120ed4

.field public static final keybind_description_modal_unread_mention_channels:I = 0x7f120ed5

.field public static final keybind_description_modal_upload_file:I = 0x7f120ed6

.field public static final keybind_description_navigate_back:I = 0x7f120ed7

.field public static final keybind_description_navigate_forward:I = 0x7f120ed8

.field public static final keybind_description_push_to_mute:I = 0x7f120ed9

.field public static final keybind_description_push_to_talk:I = 0x7f120eda

.field public static final keybind_description_push_to_talk_priority:I = 0x7f120edb

.field public static final keybind_description_toggle_deafen:I = 0x7f120edc

.field public static final keybind_description_toggle_go_live_streaming:I = 0x7f120edd

.field public static final keybind_description_toggle_mute:I = 0x7f120ede

.field public static final keybind_description_toggle_overlay:I = 0x7f120edf

.field public static final keybind_description_toggle_overlay_input_lock:I = 0x7f120ee0

.field public static final keybind_description_toggle_streamer_mode:I = 0x7f120ee1

.field public static final keybind_description_toggle_voice_mode:I = 0x7f120ee2

.field public static final keybind_description_unassigned:I = 0x7f120ee3

.field public static final keybind_navigate_back:I = 0x7f120ee4

.field public static final keybind_navigate_forward:I = 0x7f120ee5

.field public static final keybind_push_to_mute:I = 0x7f120ee6

.field public static final keybind_push_to_talk:I = 0x7f120ee7

.field public static final keybind_push_to_talk_priority:I = 0x7f120ee8

.field public static final keybind_toggle_deafen:I = 0x7f120ee9

.field public static final keybind_toggle_go_live_streaming:I = 0x7f120eea

.field public static final keybind_toggle_mute:I = 0x7f120eeb

.field public static final keybind_toggle_overlay:I = 0x7f120eec

.field public static final keybind_toggle_overlay_input_lock:I = 0x7f120eed

.field public static final keybind_toggle_stream_mode:I = 0x7f120eee

.field public static final keybind_toggle_voice_mode:I = 0x7f120eef

.field public static final keybind_unassigned:I = 0x7f120ef0

.field public static final keybinds:I = 0x7f120ef1

.field public static final keyboard_behavior_mobile_header:I = 0x7f120ef2

.field public static final keyboard_behavior_mobile_shift_enter_to_send:I = 0x7f120ef3

.field public static final keyboard_behavior_mobile_shift_enter_to_send_hint:I = 0x7f120ef4

.field public static final kick:I = 0x7f120ef5

.field public static final kick_from_server:I = 0x7f120ef6

.field public static final kick_members:I = 0x7f120ef7

.field public static final kick_user:I = 0x7f120ef8

.field public static final kick_user_body:I = 0x7f120ef9

.field public static final kick_user_confirmed:I = 0x7f120efa

.field public static final kick_user_error_generic:I = 0x7f120efb

.field public static final kick_user_title:I = 0x7f120efc

.field public static final ko:I = 0x7f120efd

.field public static final label_with_online_status:I = 0x7f120eff

.field public static final language:I = 0x7f120f00

.field public static final language_not_found:I = 0x7f120f01

.field public static final language_select:I = 0x7f120f02

.field public static final language_updated:I = 0x7f120f03

.field public static final languages:I = 0x7f120f04

.field public static final large_guild_notify_all_messages_description:I = 0x7f120f05

.field public static final large_message_upload_subtitle:I = 0x7f120f06

.field public static final large_message_upload_title:I = 0x7f120f07

.field public static final last_seen:I = 0x7f120f08

.field public static final last_sync:I = 0x7f120f09

.field public static final launch_app:I = 0x7f120f0a

.field public static final learn_more:I = 0x7f120f0b

.field public static final learn_more_alt:I = 0x7f120f0c

.field public static final learn_more_link:I = 0x7f120f0d

.field public static final leave_call:I = 0x7f120f0e

.field public static final leave_group_dm:I = 0x7f120f0f

.field public static final leave_group_dm_body:I = 0x7f120f10

.field public static final leave_group_dm_managed_body:I = 0x7f120f11

.field public static final leave_group_dm_managed_title:I = 0x7f120f12

.field public static final leave_group_dm_title:I = 0x7f120f13

.field public static final leave_server:I = 0x7f120f14

.field public static final leave_server_body:I = 0x7f120f15

.field public static final leave_server_body_mobile:I = 0x7f120f16

.field public static final leave_server_title:I = 0x7f120f17

.field public static final lets_go:I = 0x7f120f18

.field public static final library:I = 0x7f120f1a

.field public static final light_blue:I = 0x7f120f1b

.field public static final light_green:I = 0x7f120f1c

.field public static final light_grey:I = 0x7f120f1d

.field public static final link_copied:I = 0x7f120f1e

.field public static final link_settings:I = 0x7f120f1f

.field public static final link_your_discord_account:I = 0x7f120f20

.field public static final link_your_xbox_account_1:I = 0x7f120f21

.field public static final link_your_xbox_account_2:I = 0x7f120f22

.field public static final linux:I = 0x7f120f23

.field public static final listen_on_spotify:I = 0x7f120f24

.field public static final listening_to:I = 0x7f120f25

.field public static final live:I = 0x7f120f26

.field public static final live_viewers:I = 0x7f120f27

.field public static final load_image_error:I = 0x7f120f28

.field public static final load_more_messages:I = 0x7f120f29

.field public static final load_reactions_error:I = 0x7f120f2a

.field public static final loading:I = 0x7f120f2b

.field public static final loading_did_you_know:I = 0x7f120f2c

.field public static final loading_keybind_tip_1:I = 0x7f120f2d

.field public static final loading_keybind_tip_2:I = 0x7f120f2e

.field public static final loading_keybind_tip_3:I = 0x7f120f2f

.field public static final loading_keybind_tip_4:I = 0x7f120f30

.field public static final loading_keybind_tip_5:I = 0x7f120f31

.field public static final loading_keybind_tip_6:I = 0x7f120f32

.field public static final loading_keybind_tip_7:I = 0x7f120f33

.field public static final loading_keybind_tip_8:I = 0x7f120f34

.field public static final loading_line_1:I = 0x7f120f35

.field public static final loading_line_10:I = 0x7f120f36

.field public static final loading_line_11:I = 0x7f120f37

.field public static final loading_line_12:I = 0x7f120f38

.field public static final loading_line_13:I = 0x7f120f39

.field public static final loading_line_14:I = 0x7f120f3a

.field public static final loading_line_15:I = 0x7f120f3b

.field public static final loading_line_2:I = 0x7f120f3c

.field public static final loading_line_3:I = 0x7f120f3d

.field public static final loading_line_4:I = 0x7f120f3e

.field public static final loading_line_5:I = 0x7f120f3f

.field public static final loading_line_6:I = 0x7f120f40

.field public static final loading_line_7:I = 0x7f120f41

.field public static final loading_line_8:I = 0x7f120f42

.field public static final loading_line_9:I = 0x7f120f43

.field public static final loading_messages_a11y_label:I = 0x7f120f44

.field public static final loading_note:I = 0x7f120f45

.field public static final loading_tip_1:I = 0x7f120f46

.field public static final loading_tip_10:I = 0x7f120f47

.field public static final loading_tip_11:I = 0x7f120f48

.field public static final loading_tip_12:I = 0x7f120f49

.field public static final loading_tip_13:I = 0x7f120f4a

.field public static final loading_tip_14:I = 0x7f120f4b

.field public static final loading_tip_15:I = 0x7f120f4c

.field public static final loading_tip_16:I = 0x7f120f4d

.field public static final loading_tip_17:I = 0x7f120f4e

.field public static final loading_tip_18:I = 0x7f120f4f

.field public static final loading_tip_19:I = 0x7f120f50

.field public static final loading_tip_2:I = 0x7f120f51

.field public static final loading_tip_20:I = 0x7f120f52

.field public static final loading_tip_21:I = 0x7f120f53

.field public static final loading_tip_22:I = 0x7f120f54

.field public static final loading_tip_23:I = 0x7f120f55

.field public static final loading_tip_24:I = 0x7f120f56

.field public static final loading_tip_25:I = 0x7f120f57

.field public static final loading_tip_3:I = 0x7f120f58

.field public static final loading_tip_4:I = 0x7f120f59

.field public static final loading_tip_5:I = 0x7f120f5a

.field public static final loading_tip_6:I = 0x7f120f5b

.field public static final loading_tip_7:I = 0x7f120f5c

.field public static final loading_tip_8:I = 0x7f120f5d

.field public static final loading_tip_9:I = 0x7f120f5e

.field public static final loading_your_pin:I = 0x7f120f5f

.field public static final lobby:I = 0x7f120f60

.field public static final local_muted:I = 0x7f120f61

.field public static final local_push_notification_guild_verification_body:I = 0x7f120f62

.field public static final local_push_notification_screenshare_not_supported:I = 0x7f120f63

.field public static final local_video_disabled:I = 0x7f120f64

.field public static final locale:I = 0x7f120f65

.field public static final login:I = 0x7f120f66

.field public static final login_as:I = 0x7f120f67

.field public static final login_body:I = 0x7f120f68

.field public static final login_required:I = 0x7f120f69

.field public static final login_title:I = 0x7f120f6a

.field public static final login_with_qr:I = 0x7f120f6b

.field public static final login_with_qr_description:I = 0x7f120f6c

.field public static final logout:I = 0x7f120f6d

.field public static final low_quality_image_mode:I = 0x7f120f6e

.field public static final low_quality_image_mode_help:I = 0x7f120f6f

.field public static final lt:I = 0x7f120f70

.field public static final lurker_mode_chat_input_button:I = 0x7f120f71

.field public static final lurker_mode_chat_input_message:I = 0x7f120f72

.field public static final lurker_mode_chat_input_message_ios:I = 0x7f120f73

.field public static final lurker_mode_nag_bar_button:I = 0x7f120f74

.field public static final lurker_mode_nag_bar_header:I = 0x7f120f75

.field public static final lurker_mode_popout_cancel:I = 0x7f120f76

.field public static final lurker_mode_popout_chat_header:I = 0x7f120f77

.field public static final lurker_mode_popout_join:I = 0x7f120f78

.field public static final lurker_mode_popout_reactions_header:I = 0x7f120f79

.field public static final lurker_mode_popout_success_body:I = 0x7f120f7a

.field public static final lurker_mode_popout_success_button:I = 0x7f120f7b

.field public static final lurker_mode_popout_success_header:I = 0x7f120f7c

.field public static final lurker_mode_popout_upsell_body:I = 0x7f120f7d

.field public static final lurker_mode_view_guild:I = 0x7f120f7e

.field public static final macos:I = 0x7f120f7f

.field public static final magenta:I = 0x7f120f80

.field public static final manage_channel:I = 0x7f120f81

.field public static final manage_channel_description:I = 0x7f120f82

.field public static final manage_channels:I = 0x7f120f83

.field public static final manage_channels_description:I = 0x7f120f84

.field public static final manage_emojis:I = 0x7f120f85

.field public static final manage_messages:I = 0x7f120f86

.field public static final manage_messages_description:I = 0x7f120f87

.field public static final manage_messages_description_in_announcement_channel:I = 0x7f120f88

.field public static final manage_nicknames:I = 0x7f120f89

.field public static final manage_nicknames_description:I = 0x7f120f8a

.field public static final manage_permissions:I = 0x7f120f8b

.field public static final manage_permissions_description:I = 0x7f120f8c

.field public static final manage_roles:I = 0x7f120f8d

.field public static final manage_roles_description:I = 0x7f120f8e

.field public static final manage_server:I = 0x7f120f8f

.field public static final manage_server_description:I = 0x7f120f90

.field public static final manage_user:I = 0x7f120f91

.field public static final manage_user_shorthand:I = 0x7f120f92

.field public static final manage_webhooks:I = 0x7f120f93

.field public static final manage_webhooks_description:I = 0x7f120f94

.field public static final managed_role_explaination:I = 0x7f120f95

.field public static final managed_role_integration_explanation:I = 0x7f120f96

.field public static final managed_role_premium_subscriber_explanation:I = 0x7f120f97

.field public static final mark_as_read:I = 0x7f120f98

.field public static final mark_unread:I = 0x7f120f99

.field public static final marked_as_read:I = 0x7f120f9a

.field public static final marketing_refresh_premium_features_badge_heading:I = 0x7f120f9b

.field public static final marketing_refresh_premium_features_badge_subheading:I = 0x7f120f9c

.field public static final marketing_refresh_premium_features_emoji_heading:I = 0x7f120f9d

.field public static final marketing_refresh_premium_features_emoji_subheading:I = 0x7f120f9e

.field public static final marketing_refresh_premium_features_file_upload_heading:I = 0x7f120f9f

.field public static final marketing_refresh_premium_features_file_upload_subheading:I = 0x7f120fa0

.field public static final marketing_refresh_premium_features_go_live_heading:I = 0x7f120fa1

.field public static final marketing_refresh_premium_features_go_live_info:I = 0x7f120fa2

.field public static final marketing_refresh_premium_features_go_live_subheading:I = 0x7f120fa3

.field public static final marketing_refresh_premium_features_included_guild_subscriptions_heading:I = 0x7f120fa4

.field public static final marketing_refresh_premium_features_included_guild_subscriptions_subheading:I = 0x7f120fa5

.field public static final marketing_refresh_premium_features_profile_heading:I = 0x7f120fa6

.field public static final marketing_refresh_premium_features_profile_subheading:I = 0x7f120fa7

.field public static final marketing_refresh_premium_tier_1_cta_description:I = 0x7f120fa8

.field public static final marketing_refresh_premium_tier_1_cta_description_perks_info_fps:I = 0x7f120fa9

.field public static final marketing_refresh_premium_tier_1_cta_description_perks_info_label:I = 0x7f120faa

.field public static final marketing_refresh_premium_tier_1_cta_description_perks_info_label_stickers:I = 0x7f120fab

.field public static final marketing_refresh_premium_tier_1_cta_description_perks_info_no_premium_guild_subscriptions:I = 0x7f120fac

.field public static final marketing_refresh_premium_tier_1_cta_description_perks_info_no_wumpus_tier_2:I = 0x7f120fad

.field public static final marketing_refresh_premium_tier_1_cta_description_perks_info_upload:I = 0x7f120fae

.field public static final marketing_refresh_premium_tier_1_cta_description_stickers:I = 0x7f120faf

.field public static final marketing_refresh_premium_tier_1_cta_description_with_price:I = 0x7f120fb0

.field public static final marketing_refresh_premium_tier_1_cta_description_with_price_stickers:I = 0x7f120fb1

.field public static final marketing_refresh_premium_tier_2_cta_subtitle:I = 0x7f120fb2

.field public static final marketing_refresh_premium_tier_2_cta_title:I = 0x7f120fb3

.field public static final marketing_refresh_premium_tier_2_subtitle:I = 0x7f120fb4

.field public static final marketing_refresh_premium_tier_2_subtitle_with_price:I = 0x7f120fb5

.field public static final marketing_refresh_premium_tier_2_title:I = 0x7f120fb6

.field public static final masked_link_body:I = 0x7f120fb7

.field public static final masked_link_cancel:I = 0x7f120fb8

.field public static final masked_link_confirm:I = 0x7f120fb9

.field public static final masked_link_trust_this_domain:I = 0x7f120fba

.field public static final masked_link_trust_this_protocol:I = 0x7f120fbb

.field public static final masked_protocol_link_body:I = 0x7f120fbc

.field public static final material_clock_display_divider:I = 0x7f120fbd

.field public static final material_clock_toggle_content_description:I = 0x7f120fbe

.field public static final material_hour_selection:I = 0x7f120fbf

.field public static final material_hour_suffix:I = 0x7f120fc0

.field public static final material_minute_selection:I = 0x7f120fc1

.field public static final material_minute_suffix:I = 0x7f120fc2

.field public static final material_slider_range_end:I = 0x7f120fc3

.field public static final material_slider_range_start:I = 0x7f120fc4

.field public static final material_timepicker_am:I = 0x7f120fc5

.field public static final material_timepicker_hour:I = 0x7f120fc6

.field public static final material_timepicker_minute:I = 0x7f120fc7

.field public static final material_timepicker_pm:I = 0x7f120fc8

.field public static final material_timepicker_select_time:I = 0x7f120fc9

.field public static final mature_listing_accept:I = 0x7f120fca

.field public static final mature_listing_decline:I = 0x7f120fcb

.field public static final mature_listing_description:I = 0x7f120fcc

.field public static final mature_listing_title:I = 0x7f120fcd

.field public static final max_age_never:I = 0x7f120fce

.field public static final max_age_never_description_mobile:I = 0x7f120fcf

.field public static final max_number_of_uses:I = 0x7f120fd0

.field public static final max_uses:I = 0x7f120fd1

.field public static final max_uses_description_mobile:I = 0x7f120fd2

.field public static final max_uses_description_unlimited_uses:I = 0x7f120fd3

.field public static final me:I = 0x7f120fd4

.field public static final media_keyboard_browse:I = 0x7f120fd5

.field public static final media_keyboard_gift:I = 0x7f120fd6

.field public static final member:I = 0x7f120fd7

.field public static final member_list:I = 0x7f120fd8

.field public static final member_list_server_owner_help:I = 0x7f120fd9

.field public static final member_verification_add_question:I = 0x7f120fda

.field public static final member_verification_add_rule:I = 0x7f120fdb

.field public static final member_verification_chat_blocker_text:I = 0x7f120fdc

.field public static final member_verification_chat_input_guard_message:I = 0x7f120fdd

.field public static final member_verification_choices_required_error:I = 0x7f120fde

.field public static final member_verification_claim_account_info:I = 0x7f120fdf

.field public static final member_verification_claim_account_subtitle:I = 0x7f120fe0

.field public static final member_verification_claim_account_title:I = 0x7f120fe1

.field public static final member_verification_delete_field_confirm_text:I = 0x7f120fe2

.field public static final member_verification_delete_field_confirm_title:I = 0x7f120fe3

.field public static final member_verification_email_confirmation_subtitle:I = 0x7f120fe4

.field public static final member_verification_email_confirmation_title:I = 0x7f120fe5

.field public static final member_verification_email_resend:I = 0x7f120fe6

.field public static final member_verification_email_verification_email_sent:I = 0x7f120fe7

.field public static final member_verification_email_verification_enabled:I = 0x7f120fe8

.field public static final member_verification_email_verification_resend_email:I = 0x7f120fe9

.field public static final member_verification_example_rules:I = 0x7f120fea

.field public static final member_verification_field_placeholder:I = 0x7f120feb

.field public static final member_verification_field_question:I = 0x7f120fec

.field public static final member_verification_field_question_required_error:I = 0x7f120fed

.field public static final member_verification_form_fields_limit:I = 0x7f120fee

.field public static final member_verification_form_incomplete:I = 0x7f120fef

.field public static final member_verification_form_item_coming_soon:I = 0x7f120ff0

.field public static final member_verification_form_item_email_verification_label:I = 0x7f120ff1

.field public static final member_verification_form_item_file_upload:I = 0x7f120ff2

.field public static final member_verification_form_item_multiple_choice:I = 0x7f120ff3

.field public static final member_verification_form_item_paragraph:I = 0x7f120ff4

.field public static final member_verification_form_item_phone_verification_label:I = 0x7f120ff5

.field public static final member_verification_form_item_rules:I = 0x7f120ff6

.field public static final member_verification_form_item_text_input:I = 0x7f120ff7

.field public static final member_verification_form_item_verification:I = 0x7f120ff8

.field public static final member_verification_form_required_item:I = 0x7f120ff9

.field public static final member_verification_form_rules_label:I = 0x7f120ffa

.field public static final member_verification_form_verification_settings:I = 0x7f120ffb

.field public static final member_verification_get_started_button:I = 0x7f120ffc

.field public static final member_verification_get_started_subtitle:I = 0x7f120ffd

.field public static final member_verification_get_started_title:I = 0x7f120ffe

.field public static final member_verification_modal_subtitle:I = 0x7f120fff

.field public static final member_verification_modal_title:I = 0x7f121000

.field public static final member_verification_multiple_choice_add:I = 0x7f121001

.field public static final member_verification_multiple_choice_limit:I = 0x7f121002

.field public static final member_verification_multiple_choice_placeholder:I = 0x7f121003

.field public static final member_verification_need_rules_channel_perms:I = 0x7f121004

.field public static final member_verification_notice_cta:I = 0x7f121005

.field public static final member_verification_notice_text:I = 0x7f121006

.field public static final member_verification_num_emojis:I = 0x7f121007

.field public static final member_verification_num_members:I = 0x7f121008

.field public static final member_verification_num_online:I = 0x7f121009

.field public static final member_verification_phone_verification_enabled:I = 0x7f12100a

.field public static final member_verification_preview_disabled_markdown_warning:I = 0x7f12100b

.field public static final member_verification_read_rules:I = 0x7f12100c

.field public static final member_verification_rule_be_respectful:I = 0x7f12100d

.field public static final member_verification_rule_be_respectful_full:I = 0x7f12100e

.field public static final member_verification_rule_index:I = 0x7f12100f

.field public static final member_verification_rule_limit:I = 0x7f121010

.field public static final member_verification_rule_no_nsfw:I = 0x7f121011

.field public static final member_verification_rule_no_nsfw_full:I = 0x7f121012

.field public static final member_verification_rule_no_spam:I = 0x7f121013

.field public static final member_verification_rule_no_spam_full:I = 0x7f121014

.field public static final member_verification_rule_placeholder:I = 0x7f121015

.field public static final member_verification_rule_safe:I = 0x7f121016

.field public static final member_verification_rule_safe_full:I = 0x7f121017

.field public static final member_verification_rule_tooltip:I = 0x7f121018

.field public static final member_verification_rules_required_error:I = 0x7f121019

.field public static final member_verification_verified:I = 0x7f12101a

.field public static final member_verification_version_mismatch_error:I = 0x7f12101b

.field public static final member_verification_warning:I = 0x7f12101c

.field public static final member_verification_warning_cancel:I = 0x7f12101d

.field public static final member_verification_warning_confirm:I = 0x7f12101e

.field public static final member_verification_warning_description:I = 0x7f12101f

.field public static final member_verification_warning_description_and_fields:I = 0x7f121020

.field public static final member_verification_warning_fields:I = 0x7f121021

.field public static final member_verification_warning_title:I = 0x7f121022

.field public static final members:I = 0x7f121023

.field public static final members_header:I = 0x7f121024

.field public static final members_matching:I = 0x7f121025

.field public static final members_search_placeholder:I = 0x7f121026

.field public static final mention:I = 0x7f121027

.field public static final mention_everyone:I = 0x7f121028

.field public static final mention_everyone_android:I = 0x7f121029

.field public static final mention_everyone_autocomplete_description:I = 0x7f12102a

.field public static final mention_everyone_autocomplete_description_mobile:I = 0x7f12102b

.field public static final mention_everyone_channel_description:I = 0x7f12102c

.field public static final mention_everyone_channel_description_android:I = 0x7f12102d

.field public static final mention_everyone_description:I = 0x7f12102e

.field public static final mention_everyone_description_android:I = 0x7f12102f

.field public static final mention_here_autocomplete_description:I = 0x7f121030

.field public static final mention_here_autocomplete_description_mobile:I = 0x7f121031

.field public static final mention_role_autocomplete_description_mobile:I = 0x7f121032

.field public static final mention_users_with_role:I = 0x7f121033

.field public static final mentions:I = 0x7f121034

.field public static final mentions_count:I = 0x7f121035

.field public static final message_action_reply:I = 0x7f121036

.field public static final message_actions_menu_label:I = 0x7f121037

.field public static final message_display_mode_label:I = 0x7f121038

.field public static final message_edited:I = 0x7f121039

.field public static final message_header_replied:I = 0x7f12103a

.field public static final message_header_replied_to:I = 0x7f12103b

.field public static final message_options:I = 0x7f12103c

.field public static final message_pinned:I = 0x7f12103d

.field public static final message_preview:I = 0x7f12103e

.field public static final message_published:I = 0x7f12103f

.field public static final message_rate_limited_body:I = 0x7f121040

.field public static final message_rate_limited_button:I = 0x7f121041

.field public static final message_rate_limited_header:I = 0x7f121042

.field public static final message_replied_to:I = 0x7f121043

.field public static final message_step_subtitle:I = 0x7f121044

.field public static final message_step_title:I = 0x7f121045

.field public static final message_too_long_body_text:I = 0x7f121046

.field public static final message_too_long_header:I = 0x7f121047

.field public static final message_tts:I = 0x7f121048

.field public static final message_tts_deleted_role:I = 0x7f121049

.field public static final message_tts_reply:I = 0x7f12104a

.field public static final message_unpinned:I = 0x7f12104b

.field public static final message_utilities_a11y_label:I = 0x7f12104c

.field public static final messages:I = 0x7f12104d

.field public static final messages_failed_to_load:I = 0x7f12104e

.field public static final messages_failed_to_load_try_again:I = 0x7f12104f

.field public static final mfa_sms_add_phone:I = 0x7f121050

.field public static final mfa_sms_already_enabled:I = 0x7f121051

.field public static final mfa_sms_auth:I = 0x7f121052

.field public static final mfa_sms_auth_current_phone:I = 0x7f121053

.field public static final mfa_sms_auth_sales_pitch:I = 0x7f121054

.field public static final mfa_sms_confirm_remove_action:I = 0x7f121055

.field public static final mfa_sms_confirm_remove_body:I = 0x7f121056

.field public static final mfa_sms_disabled_no_email:I = 0x7f121057

.field public static final mfa_sms_disabled_partner:I = 0x7f121058

.field public static final mfa_sms_enable:I = 0x7f121059

.field public static final mfa_sms_enable_should_do:I = 0x7f12105a

.field public static final mfa_sms_enable_subheader:I = 0x7f12105b

.field public static final mfa_sms_phone_number_hide:I = 0x7f12105c

.field public static final mfa_sms_phone_number_reveal:I = 0x7f12105d

.field public static final mfa_sms_receive_code:I = 0x7f12105e

.field public static final mfa_sms_remove:I = 0x7f12105f

.field public static final mfa_sms_resend:I = 0x7f121060

.field public static final mic_test_voice_channel_warning:I = 0x7f121061

.field public static final minimum_size:I = 0x7f121062

.field public static final missed_an_update:I = 0x7f121063

.field public static final missed_call:I = 0x7f121064

.field public static final missing_entitlement_modal_body:I = 0x7f121065

.field public static final missing_entitlement_modal_body_unknown_application:I = 0x7f121066

.field public static final missing_entitlement_modal_header:I = 0x7f121067

.field public static final missing_entitlement_modal_header_unknown_application:I = 0x7f121068

.field public static final mobile_advanced_voice_activity_cpu_overuse:I = 0x7f121069

.field public static final mobile_designate_other_channel:I = 0x7f12106a

.field public static final mobile_enable_hardware_scaling:I = 0x7f12106b

.field public static final mobile_enable_hardware_scaling_desc:I = 0x7f12106c

.field public static final mobile_noise_cancellation_cpu_overuse:I = 0x7f12106d

.field public static final mobile_noise_cancellation_failed:I = 0x7f12106e

.field public static final mobile_noise_cancellation_popout_description:I = 0x7f12106f

.field public static final mobile_noise_cancellation_popout_title:I = 0x7f121070

.field public static final mobile_replying_to:I = 0x7f121071

.field public static final mobile_stream_active_body:I = 0x7f121072

.field public static final mobile_stream_active_header:I = 0x7f121073

.field public static final mobile_stream_active_soundshare_warning_android:I = 0x7f121074

.field public static final mobile_stream_participants_hidden:I = 0x7f121075

.field public static final mobile_stream_screen_share:I = 0x7f121076

.field public static final mobile_stream_screen_sharing:I = 0x7f121077

.field public static final mobile_stream_stop_sharing:I = 0x7f121078

.field public static final moderation:I = 0x7f121079

.field public static final modify_followed_news_header:I = 0x7f12107a

.field public static final monthly:I = 0x7f12107b

.field public static final more:I = 0x7f12107c

.field public static final more_details:I = 0x7f12107d

.field public static final more_options:I = 0x7f12107e

.field public static final move_from_category_to:I = 0x7f12107f

.field public static final move_members:I = 0x7f121080

.field public static final move_members_description:I = 0x7f121081

.field public static final move_to:I = 0x7f121082

.field public static final move_to_success:I = 0x7f121083

.field public static final mtrl_badge_numberless_content_description:I = 0x7f121086

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f121087

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f121088

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f121089

.field public static final mtrl_picker_a11y_next_month:I = 0x7f12108a

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f12108b

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12108c

.field public static final mtrl_picker_cancel:I = 0x7f12108d

.field public static final mtrl_picker_confirm:I = 0x7f12108e

.field public static final mtrl_picker_date_header_selected:I = 0x7f12108f

.field public static final mtrl_picker_date_header_title:I = 0x7f121090

.field public static final mtrl_picker_date_header_unselected:I = 0x7f121091

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f121092

.field public static final mtrl_picker_invalid_format:I = 0x7f121093

.field public static final mtrl_picker_invalid_format_example:I = 0x7f121094

.field public static final mtrl_picker_invalid_format_use:I = 0x7f121095

.field public static final mtrl_picker_invalid_range:I = 0x7f121096

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f121097

.field public static final mtrl_picker_out_of_range:I = 0x7f121098

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f121099

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f12109a

.field public static final mtrl_picker_range_header_selected:I = 0x7f12109b

.field public static final mtrl_picker_range_header_title:I = 0x7f12109c

.field public static final mtrl_picker_range_header_unselected:I = 0x7f12109d

.field public static final mtrl_picker_save:I = 0x7f12109e

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f12109f

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f1210a0

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f1210a1

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f1210a2

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f1210a3

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f1210a4

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f1210a5

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f1210a6

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f1210a7

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f1210a8

.field public static final mute:I = 0x7f1210a9

.field public static final mute_category:I = 0x7f1210aa

.field public static final mute_channel:I = 0x7f1210ab

.field public static final mute_channel_generic:I = 0x7f1210ac

.field public static final mute_conversation:I = 0x7f1210ad

.field public static final mute_duration_15_minutes:I = 0x7f1210ae

.field public static final mute_duration_1_hour:I = 0x7f1210af

.field public static final mute_duration_24_hours:I = 0x7f1210b0

.field public static final mute_duration_8_hours:I = 0x7f1210b1

.field public static final mute_duration_always:I = 0x7f1210b2

.field public static final mute_group_dm:I = 0x7f1210b3

.field public static final mute_members:I = 0x7f1210b4

.field public static final mute_server:I = 0x7f1210b5

.field public static final mute_settings_mute_category:I = 0x7f1210b6

.field public static final mute_settings_mute_channel:I = 0x7f1210b7

.field public static final mute_settings_mute_server:I = 0x7f1210b8

.field public static final mute_settings_mute_this_conversation:I = 0x7f1210b9

.field public static final mute_until:I = 0x7f1210ba

.field public static final muted_until_time:I = 0x7f1210bb

.field public static final mutual_friends:I = 0x7f1210bc

.field public static final mutual_guilds:I = 0x7f1210bd

.field public static final n_days:I = 0x7f1210be

.field public static final nearby_scan:I = 0x7f1210bf

.field public static final need_account:I = 0x7f1210c0

.field public static final network_error_bad_request:I = 0x7f1210c1

.field public static final network_error_cloudflare_intermittent:I = 0x7f1210c2

.field public static final network_error_cloudflare_unauthorized:I = 0x7f1210c3

.field public static final network_error_connection:I = 0x7f1210c4

.field public static final network_error_forbidden:I = 0x7f1210c5

.field public static final network_error_request_too_large:I = 0x7f1210c6

.field public static final network_error_rest_request:I = 0x7f1210c7

.field public static final network_error_ssl:I = 0x7f1210c8

.field public static final network_error_unauthorized:I = 0x7f1210c9

.field public static final network_error_unknown:I = 0x7f1210ca

.field public static final network_offline:I = 0x7f1210cb

.field public static final network_offline_airplane_mode:I = 0x7f1210cc

.field public static final never_ask_again:I = 0x7f1210cd

.field public static final nevermind:I = 0x7f1210ce

.field public static final new_dm:I = 0x7f1210cf

.field public static final new_group_dm:I = 0x7f1210d0

.field public static final new_guilds_dm_allowed:I = 0x7f1210d1

.field public static final new_member_retention_info:I = 0x7f1210d2

.field public static final new_mentions:I = 0x7f1210d3

.field public static final new_messages:I = 0x7f1210d4

.field public static final new_messages_divider:I = 0x7f1210d5

.field public static final new_messages_estimated:I = 0x7f1210d6

.field public static final new_messages_estimated_with_date:I = 0x7f1210d7

.field public static final new_messages_with_date:I = 0x7f1210d8

.field public static final new_override:I = 0x7f1210d9

.field public static final new_permission:I = 0x7f1210da

.field public static final new_role:I = 0x7f1210db

.field public static final new_terms_ack:I = 0x7f1210dc

.field public static final new_terms_continue:I = 0x7f1210dd

.field public static final new_terms_description:I = 0x7f1210de

.field public static final new_terms_title:I = 0x7f1210df

.field public static final new_unreads:I = 0x7f1210e0

.field public static final news_channel:I = 0x7f1210e1

.field public static final news_channel_publish:I = 0x7f1210e2

.field public static final news_channel_publish_bump:I = 0x7f1210e3

.field public static final news_channel_publish_bump_hide_permanently:I = 0x7f1210e4

.field public static final news_channel_published:I = 0x7f1210e5

.field public static final next:I = 0x7f1210e6

.field public static final nickname:I = 0x7f1210e7

.field public static final nickname_changed:I = 0x7f1210e8

.field public static final nickname_cleared:I = 0x7f1210e9

.field public static final nl:I = 0x7f1210ea

.field public static final no:I = 0x7f1210eb

.field public static final no_afk_channel:I = 0x7f1210ec

.field public static final no_authorized_apps:I = 0x7f1210ed

.field public static final no_authorized_apps_note:I = 0x7f1210ee

.field public static final no_ban_reason:I = 0x7f1210ef

.field public static final no_bans:I = 0x7f1210f0

.field public static final no_camera_access:I = 0x7f1210f1

.field public static final no_camera_body:I = 0x7f1210f2

.field public static final no_camera_title:I = 0x7f1210f3

.field public static final no_category:I = 0x7f1210f4

.field public static final no_emoji:I = 0x7f1210f5

.field public static final no_emoji_body:I = 0x7f1210f6

.field public static final no_emoji_search_results:I = 0x7f1210f7

.field public static final no_emoji_title:I = 0x7f1210f8

.field public static final no_gif_favorites_flavor_favorite_please:I = 0x7f1210f9

.field public static final no_gif_favorites_flavor_still_here:I = 0x7f1210fa

.field public static final no_gif_favorites_go_favorite:I = 0x7f1210fb

.field public static final no_gif_favorites_how_to_favorite:I = 0x7f1210fc

.field public static final no_gif_favorites_where_to_favorite:I = 0x7f1210fd

.field public static final no_gif_search_results:I = 0x7f1210fe

.field public static final no_gif_search_results_with_related_search:I = 0x7f1210ff

.field public static final no_gif_search_results_without_related_search:I = 0x7f121100

.field public static final no_input_devices:I = 0x7f121101

.field public static final no_input_monitoring_access:I = 0x7f121102

.field public static final no_instant_invite:I = 0x7f121103

.field public static final no_integrations:I = 0x7f121104

.field public static final no_integrations_body:I = 0x7f121105

.field public static final no_integrations_label:I = 0x7f121106

.field public static final no_invites_body:I = 0x7f121107

.field public static final no_invites_caption:I = 0x7f121108

.field public static final no_invites_label:I = 0x7f121109

.field public static final no_mic_body:I = 0x7f12110a

.field public static final no_mic_title:I = 0x7f12110b

.field public static final no_micrphone_access:I = 0x7f12110c

.field public static final no_mutual_friends:I = 0x7f12110d

.field public static final no_mutual_guilds:I = 0x7f12110e

.field public static final no_output_devices:I = 0x7f12110f

.field public static final no_photos_access:I = 0x7f121110

.field public static final no_pins_in_channel:I = 0x7f121111

.field public static final no_pins_in_dm:I = 0x7f121112

.field public static final no_private_channels_description:I = 0x7f121113

.field public static final no_private_channels_title:I = 0x7f121114

.field public static final no_reactions_body:I = 0x7f121115

.field public static final no_reactions_header:I = 0x7f121116

.field public static final no_recent_mentions:I = 0x7f121117

.field public static final no_screenshare_permission_dialog_body:I = 0x7f121118

.field public static final no_screenshare_permission_dialog_title:I = 0x7f121119

.field public static final no_send_messages_permission_placeholder:I = 0x7f12111a

.field public static final no_sticker_search_results:I = 0x7f12111b

.field public static final no_system_channel:I = 0x7f12111c

.field public static final no_text:I = 0x7f12111d

.field public static final no_thanks:I = 0x7f12111e

.field public static final no_user_limit:I = 0x7f12111f

.field public static final no_video_devices:I = 0x7f121120

.field public static final no_video_permission_dialog_body:I = 0x7f121121

.field public static final no_video_permission_dialog_title:I = 0x7f121122

.field public static final noise_cancellation_cpu_overuse:I = 0x7f121123

.field public static final noise_cancellation_off:I = 0x7f121124

.field public static final noise_cancellation_on:I = 0x7f121125

.field public static final noise_cancellation_popout_description:I = 0x7f121126

.field public static final noise_cancellation_popout_footer:I = 0x7f121127

.field public static final noise_cancellation_popout_header:I = 0x7f121128

.field public static final noise_cancellation_popout_mic_test_title:I = 0x7f121129

.field public static final noise_cancellation_popout_stop:I = 0x7f12112a

.field public static final noise_cancellation_popout_test:I = 0x7f12112b

.field public static final noise_cancellation_tooltip:I = 0x7f12112c

.field public static final noise_suppression:I = 0x7f12112d

.field public static final none:I = 0x7f12112e

.field public static final not_available:I = 0x7f12112f

.field public static final not_in_the_voice_channel:I = 0x7f121130

.field public static final not_set:I = 0x7f121131

.field public static final note:I = 0x7f121132

.field public static final note_placeholder:I = 0x7f121133

.field public static final note_placeholder_mobile:I = 0x7f121134

.field public static final notice_application_test_mode:I = 0x7f121135

.field public static final notice_application_test_mode_go_to_listing:I = 0x7f121136

.field public static final notice_channel_max_members_cap_reached_message:I = 0x7f121137

.field public static final notice_connect_spotify:I = 0x7f121138

.field public static final notice_connection_conflict:I = 0x7f121139

.field public static final notice_corrupt_installation:I = 0x7f12113a

.field public static final notice_corrupt_installation_help_link_text:I = 0x7f12113b

.field public static final notice_detected_off_platform_no_premium_perk_message:I = 0x7f12113c

.field public static final notice_detected_off_platform_premium_perk_button:I = 0x7f12113d

.field public static final notice_detected_off_platform_premium_perk_message:I = 0x7f12113e

.field public static final notice_dispatch_api_error:I = 0x7f12113f

.field public static final notice_dispatch_application_lock_failed:I = 0x7f121140

.field public static final notice_dispatch_error:I = 0x7f121141

.field public static final notice_dispatch_error_disk_full:I = 0x7f121142

.field public static final notice_dispatch_error_disk_low:I = 0x7f121143

.field public static final notice_dispatch_error_file_name_too_long:I = 0x7f121144

.field public static final notice_dispatch_error_invalid_drive:I = 0x7f121145

.field public static final notice_dispatch_error_io_permission_denied:I = 0x7f121146

.field public static final notice_dispatch_error_no_manifests:I = 0x7f121147

.field public static final notice_dispatch_error_not_entitled:I = 0x7f121148

.field public static final notice_dispatch_error_post_install_cancelled:I = 0x7f121149

.field public static final notice_dispatch_error_post_install_failed:I = 0x7f12114a

.field public static final notice_dispatch_error_unwritable:I = 0x7f12114b

.field public static final notice_dispatch_error_with_code:I = 0x7f12114c

.field public static final notice_dispatch_install_script_progress:I = 0x7f12114d

.field public static final notice_dispatch_install_script_progress_with_name:I = 0x7f12114e

.field public static final notice_enable_public_guild_upsell_message:I = 0x7f12114f

.field public static final notice_guild_feedback_survey:I = 0x7f121150

.field public static final notice_hardware_mute:I = 0x7f121151

.field public static final notice_mfa_sms_backup:I = 0x7f121152

.field public static final notice_mfa_sms_backup_button:I = 0x7f121153

.field public static final notice_native_apps_2020_06:I = 0x7f121154

.field public static final notice_no_input_detected:I = 0x7f121155

.field public static final notice_no_input_detected_help_link_text:I = 0x7f121156

.field public static final notice_no_input_detected_settings:I = 0x7f121157

.field public static final notice_no_input_detected_settings_link_text:I = 0x7f121158

.field public static final notice_notification_message:I = 0x7f121159

.field public static final notice_notification_message2:I = 0x7f12115a

.field public static final notice_premium_grandfathered_ended:I = 0x7f12115b

.field public static final notice_premium_grandfathered_ending:I = 0x7f12115c

.field public static final notice_premium_grandfathered_monthly_ending:I = 0x7f12115d

.field public static final notice_premium_grandfathered_upgrade:I = 0x7f12115e

.field public static final notice_premium_promo_action:I = 0x7f12115f

.field public static final notice_premium_promo_message:I = 0x7f121160

.field public static final notice_premium_xbox_game_pass_promotion:I = 0x7f121161

.field public static final notice_premium_xbox_game_pass_promotion_redeem:I = 0x7f121162

.field public static final notice_product_feedback_survey:I = 0x7f121163

.field public static final notice_product_feedback_survey_cta:I = 0x7f121164

.field public static final notice_register_to_vote:I = 0x7f121165

.field public static final notice_register_to_vote_cta:I = 0x7f121166

.field public static final notice_scheduled_maintenance:I = 0x7f121167

.field public static final notice_spotify_auto_paused:I = 0x7f121168

.field public static final notice_streamer_mode_text:I = 0x7f121169

.field public static final notice_survey_body:I = 0x7f12116a

.field public static final notice_survey_button:I = 0x7f12116b

.field public static final notice_survey_improve_prompt:I = 0x7f12116c

.field public static final notice_survey_prompt:I = 0x7f12116d

.field public static final notice_unclaimed_account:I = 0x7f12116e

.field public static final notice_unverified_account:I = 0x7f12116f

.field public static final notice_whats_this:I = 0x7f121170

.field public static final notification_accepted_friend_request:I = 0x7f121171

.field public static final notification_body_attachment:I = 0x7f121172

.field public static final notification_body_start_game:I = 0x7f121173

.field public static final notification_dismiss:I = 0x7f121174

.field public static final notification_message_create_dm_activity_join:I = 0x7f121175

.field public static final notification_message_create_dm_activity_join_request:I = 0x7f121176

.field public static final notification_message_create_dm_activity_listen:I = 0x7f121177

.field public static final notification_message_create_group_dm_activity_join:I = 0x7f121178

.field public static final notification_message_create_group_dm_activity_listen:I = 0x7f121179

.field public static final notification_message_create_guild_activity_join:I = 0x7f12117a

.field public static final notification_message_create_guild_activity_listen:I = 0x7f12117b

.field public static final notification_mute_1_hour:I = 0x7f12117c

.field public static final notification_overrides:I = 0x7f12117d

.field public static final notification_pending_friend_request:I = 0x7f12117e

.field public static final notification_reply:I = 0x7f12117f

.field public static final notification_reply_failed:I = 0x7f121180

.field public static final notification_reply_success:I = 0x7f121181

.field public static final notification_settings:I = 0x7f121182

.field public static final notification_title_discord:I = 0x7f121183

.field public static final notification_title_start_game:I = 0x7f121184

.field public static final notifications:I = 0x7f121185

.field public static final notifications_muted:I = 0x7f121186

.field public static final notifications_nuf_body:I = 0x7f121187

.field public static final notifications_nuf_cta:I = 0x7f121188

.field public static final notifications_nuf_title:I = 0x7f121189

.field public static final now_playing_gdpr_body:I = 0x7f12118a

.field public static final now_playing_gdpr_header:I = 0x7f12118b

.field public static final nsfw_accept:I = 0x7f12118c

.field public static final nsfw_decline:I = 0x7f12118d

.field public static final nsfw_description:I = 0x7f12118e

.field public static final nsfw_title:I = 0x7f12118f

.field public static final nuf_age_gate_body:I = 0x7f121190

.field public static final nuf_body:I = 0x7f121191

.field public static final nuf_body_mobile:I = 0x7f121192

.field public static final nuf_body_refresh:I = 0x7f121193

.field public static final nuf_channel_prompt_channel_topic_template:I = 0x7f121194

.field public static final nuf_channel_prompt_cta:I = 0x7f121195

.field public static final nuf_channel_prompt_subtitle:I = 0x7f121196

.field public static final nuf_channel_prompt_title:I = 0x7f121197

.field public static final nuf_channel_prompt_topic:I = 0x7f121198

.field public static final nuf_channel_prompt_topic_placeholder:I = 0x7f121199

.field public static final nuf_channels_cta_label:I = 0x7f12119a

.field public static final nuf_channels_description:I = 0x7f12119b

.field public static final nuf_channels_title:I = 0x7f12119c

.field public static final nuf_chat_with_friends:I = 0x7f12119d

.field public static final nuf_club_or_interest_group:I = 0x7f12119e

.field public static final nuf_complete_cta:I = 0x7f12119f

.field public static final nuf_complete_subtitle:I = 0x7f1211a0

.field public static final nuf_complete_title:I = 0x7f1211a1

.field public static final nuf_create_server_button:I = 0x7f1211a2

.field public static final nuf_create_server_customize_header:I = 0x7f1211a3

.field public static final nuf_create_server_customize_subheader:I = 0x7f1211a4

.field public static final nuf_desktop_templates_subtitle:I = 0x7f1211a5

.field public static final nuf_desktop_templates_title:I = 0x7f1211a6

.field public static final nuf_download_app_button_platform:I = 0x7f1211a7

.field public static final nuf_gaming_community:I = 0x7f1211a8

.field public static final nuf_get_started:I = 0x7f1211a9

.field public static final nuf_have_an_invite_already:I = 0x7f1211aa

.field public static final nuf_join_a_friend:I = 0x7f1211ab

.field public static final nuf_join_server_button:I = 0x7f1211ac

.field public static final nuf_join_server_title_2:I = 0x7f1211ad

.field public static final nuf_just_look_around:I = 0x7f1211ae

.field public static final nuf_title_mobile:I = 0x7f1211af

.field public static final nuf_voice_channels_description:I = 0x7f1211b0

.field public static final nuf_voice_channels_title:I = 0x7f1211b1

.field public static final nuf_voice_chat_while_gaming:I = 0x7f1211b2

.field public static final nuf_welcome_body:I = 0x7f1211b3

.field public static final nuf_welcome_carousel_1_body_mobile:I = 0x7f1211b4

.field public static final nuf_welcome_carousel_1_title_mobile:I = 0x7f1211b5

.field public static final nuf_welcome_carousel_2_body_mobile:I = 0x7f1211b6

.field public static final nuf_welcome_carousel_2_title_mobile:I = 0x7f1211b7

.field public static final nuf_welcome_carousel_3_body_mobile:I = 0x7f1211b8

.field public static final nuf_welcome_carousel_3_title_mobile:I = 0x7f1211b9

.field public static final nuf_welcome_carousel_4_body_mobile:I = 0x7f1211ba

.field public static final nuf_welcome_carousel_4_title_mobile:I = 0x7f1211bb

.field public static final nuf_welcome_carousel_5_body_mobile:I = 0x7f1211bc

.field public static final nuf_welcome_carousel_5_title_mobile:I = 0x7f1211bd

.field public static final nuf_what_do_you_want:I = 0x7f1211be

.field public static final num_channels:I = 0x7f1211bf

.field public static final num_members:I = 0x7f1211c0

.field public static final num_subscribers:I = 0x7f1211c1

.field public static final num_users:I = 0x7f1211c2

.field public static final numbers_only:I = 0x7f1211c3

.field public static final nux_navigation_help_channels:I = 0x7f1211c4

.field public static final nux_navigation_help_dms:I = 0x7f1211c5

.field public static final nux_navigation_help_guild:I = 0x7f1211c6

.field public static final nux_overlay_description:I = 0x7f1211c7

.field public static final nux_overlay_enable:I = 0x7f1211c8

.field public static final nux_overlay_title:I = 0x7f1211c9

.field public static final nux_post_reg_join_server_description:I = 0x7f1211ca

.field public static final nux_post_reg_join_server_skip:I = 0x7f1211cb

.field public static final nux_post_reg_join_server_title:I = 0x7f1211cc

.field public static final nux_samsung_game_presence_link_description:I = 0x7f1211cd

.field public static final nux_samsung_game_presence_link_enable:I = 0x7f1211ce

.field public static final nux_samsung_game_presence_link_title:I = 0x7f1211cf

.field public static final oauth2_add_bot:I = 0x7f1211d0

.field public static final oauth2_add_to_guild:I = 0x7f1211d1

.field public static final oauth2_add_to_guild_description:I = 0x7f1211d2

.field public static final oauth2_add_to_guild_placeholder:I = 0x7f1211d3

.field public static final oauth2_add_webhook_incoming:I = 0x7f1211d4

.field public static final oauth2_can_read_notice:I = 0x7f1211d5

.field public static final oauth2_cannot_read_send_notice:I = 0x7f1211d6

.field public static final oauth2_confirm_bot_permissions:I = 0x7f1211d7

.field public static final oauth2_connect_to_discord:I = 0x7f1211d8

.field public static final oauth2_details_creation_date:I = 0x7f1211d9

.field public static final oauth2_details_guilds:I = 0x7f1211da

.field public static final oauth2_details_redirect:I = 0x7f1211db

.field public static final oauth2_disabled_permissions:I = 0x7f1211dc

.field public static final oauth2_fake_scope_1:I = 0x7f1211dd

.field public static final oauth2_fake_scope_2:I = 0x7f1211de

.field public static final oauth2_fake_scope_3:I = 0x7f1211df

.field public static final oauth2_fake_scope_4:I = 0x7f1211e0

.field public static final oauth2_fake_scope_5:I = 0x7f1211e1

.field public static final oauth2_fake_scope_6:I = 0x7f1211e2

.field public static final oauth2_fake_scope_7:I = 0x7f1211e3

.field public static final oauth2_fake_scope_8:I = 0x7f1211e4

.field public static final oauth2_logout:I = 0x7f1211e5

.field public static final oauth2_message_cta:I = 0x7f1211e6

.field public static final oauth2_request_invalid_scope:I = 0x7f1211e7

.field public static final oauth2_request_missing_param:I = 0x7f1211e8

.field public static final oauth2_requests_account_access:I = 0x7f1211e9

.field public static final oauth2_scopes_label:I = 0x7f1211ea

.field public static final oauth2_signed_in_as:I = 0x7f1211eb

.field public static final oauth2_title:I = 0x7f1211ec

.field public static final oauth2_unknown_error:I = 0x7f1211ed

.field public static final okay:I = 0x7f1211ee

.field public static final onboarding_video_bot_message:I = 0x7f1211ef

.field public static final onboarding_video_bot_message_new_user:I = 0x7f1211f0

.field public static final one_user_typing:I = 0x7f1211f1

.field public static final ongoing_call:I = 0x7f1211f2

.field public static final ongoing_call_connected_users:I = 0x7f1211f3

.field public static final ongoing_call_tap_to_join:I = 0x7f1211f4

.field public static final ongoing_call_voice_quality_low:I = 0x7f1211f5

.field public static final ongoing_call_you_are_all_alone:I = 0x7f1211f6

.field public static final only_you_can_see_and_delete_these:I = 0x7f1211f7

.field public static final open:I = 0x7f1211f8

.field public static final open_connection_replay:I = 0x7f1211f9

.field public static final open_in_browser:I = 0x7f1211fa

.field public static final open_in_theater:I = 0x7f1211fb

.field public static final open_link:I = 0x7f1211fc

.field public static final open_navigation:I = 0x7f1211fd

.field public static final open_original_image:I = 0x7f1211fe

.field public static final open_standard_keyboard_accessibility_label:I = 0x7f1211ff

.field public static final optional_application_command_indicator:I = 0x7f121200

.field public static final options:I = 0x7f121201

.field public static final options_matching:I = 0x7f121202

.field public static final orange:I = 0x7f121203

.field public static final os_min_screenshare_dialog_body:I = 0x7f121204

.field public static final os_min_screenshare_dialog_title:I = 0x7f121205

.field public static final other:I = 0x7f121206

.field public static final other_options:I = 0x7f121207

.field public static final other_reactions:I = 0x7f121208

.field public static final others_online:I = 0x7f121209

.field public static final out_of_date_action:I = 0x7f12120a

.field public static final out_of_date_description:I = 0x7f12120b

.field public static final out_of_date_title:I = 0x7f12120c

.field public static final outgoing_friend_request:I = 0x7f12120d

.field public static final outgoing_friend_request_delete_msg:I = 0x7f12120e

.field public static final output_device:I = 0x7f12120f

.field public static final overlay:I = 0x7f121210

.field public static final overlay_actions_menu_label:I = 0x7f121211

.field public static final overlay_channel_chat_hotkey:I = 0x7f121212

.field public static final overlay_click_to_jump_to_channel:I = 0x7f121213

.field public static final overlay_click_to_unlock:I = 0x7f121214

.field public static final overlay_crashed_title:I = 0x7f121215

.field public static final overlay_explanation:I = 0x7f121216

.field public static final overlay_friend_calling:I = 0x7f121217

.field public static final overlay_in_game_preview_header:I = 0x7f121218

.field public static final overlay_launch_open_tip:I = 0x7f121219

.field public static final overlay_launch_title:I = 0x7f12121a

.field public static final overlay_link_alert_body:I = 0x7f12121b

.field public static final overlay_link_alert_secondary:I = 0x7f12121c

.field public static final overlay_link_alert_title:I = 0x7f12121d

.field public static final overlay_menu_get_invite:I = 0x7f12121e

.field public static final overlay_menu_open_discord:I = 0x7f12121f

.field public static final overlay_menu_switch_channels:I = 0x7f121220

.field public static final overlay_mobile_required:I = 0x7f121221

.field public static final overlay_mobile_toggle_desc:I = 0x7f121222

.field public static final overlay_mobile_toggle_label:I = 0x7f121223

.field public static final overlay_mobile_unauthed:I = 0x7f121224

.field public static final overlay_news_go_live_body:I = 0x7f121225

.field public static final overlay_news_go_live_body_no_voice_channel:I = 0x7f121226

.field public static final overlay_news_go_live_cta:I = 0x7f121227

.field public static final overlay_no_results:I = 0x7f121228

.field public static final overlay_notification_incoming_call:I = 0x7f121229

.field public static final overlay_notification_settings_disabled:I = 0x7f12122a

.field public static final overlay_notification_settings_position:I = 0x7f12122b

.field public static final overlay_notifications_bottom_left:I = 0x7f12122c

.field public static final overlay_notifications_bottom_right:I = 0x7f12122d

.field public static final overlay_notifications_disabled:I = 0x7f12122e

.field public static final overlay_notifications_settings:I = 0x7f12122f

.field public static final overlay_notifications_top_left:I = 0x7f121230

.field public static final overlay_notifications_top_right:I = 0x7f121231

.field public static final overlay_reload:I = 0x7f121232

.field public static final overlay_reset_default_layout:I = 0x7f121233

.field public static final overlay_settings_general_tab:I = 0x7f121234

.field public static final overlay_settings_title:I = 0x7f121235

.field public static final overlay_settings_voice_tab:I = 0x7f121236

.field public static final overlay_too_small:I = 0x7f121237

.field public static final overlay_unlock_to_answer:I = 0x7f121238

.field public static final overlay_unlock_to_join:I = 0x7f121239

.field public static final overlay_user_chat_hotkey:I = 0x7f12123a

.field public static final overlay_widget_hide_in_game:I = 0x7f12123b

.field public static final overlay_widget_show_in_game:I = 0x7f12123c

.field public static final overview:I = 0x7f12123d

.field public static final overwrite_autocomplete_a11y_label:I = 0x7f12123e

.field public static final overwrite_autocomplete_label:I = 0x7f12123f

.field public static final overwrite_autocomplete_placeholder:I = 0x7f121240

.field public static final overwrite_no_role_to_add:I = 0x7f121241

.field public static final pagination_next:I = 0x7f121242

.field public static final pagination_page_label:I = 0x7f121243

.field public static final pagination_page_of:I = 0x7f121244

.field public static final pagination_previous:I = 0x7f121245

.field public static final paginator_current_page:I = 0x7f121246

.field public static final paginator_of_pages:I = 0x7f121247

.field public static final partial_outage:I = 0x7f121248

.field public static final partial_outage_a11y:I = 0x7f121249

.field public static final partner_badge_tooltip:I = 0x7f12124a

.field public static final partner_program:I = 0x7f12124b

.field public static final password_length_error:I = 0x7f12124c

.field public static final password_manager:I = 0x7f12124d

.field public static final password_manager_info_android:I = 0x7f12124e

.field public static final password_manager_open_settings:I = 0x7f12124f

.field public static final password_manager_open_settings_error:I = 0x7f121250

.field public static final password_manager_use:I = 0x7f121251

.field public static final password_recovery_external_link_description:I = 0x7f121252

.field public static final password_recovery_title:I = 0x7f121253

.field public static final password_recovery_verify_phone_subtitle:I = 0x7f121254

.field public static final password_recovery_verify_phone_title:I = 0x7f121255

.field public static final password_required:I = 0x7f121256

.field public static final password_toggle_content_description:I = 0x7f121257

.field public static final paste:I = 0x7f121258

.field public static final path_password_eye:I = 0x7f121259

.field public static final path_password_eye_mask_strike_through:I = 0x7f12125a

.field public static final path_password_eye_mask_visible:I = 0x7f12125b

.field public static final path_password_strike_through:I = 0x7f12125c

.field public static final pause:I = 0x7f12125d

.field public static final payment_authentication_modal_button:I = 0x7f12125e

.field public static final payment_authentication_modal_button_cancel_payment:I = 0x7f12125f

.field public static final payment_authentication_modal_canceled:I = 0x7f121260

.field public static final payment_authentication_modal_fail:I = 0x7f121261

.field public static final payment_authentication_modal_start:I = 0x7f121262

.field public static final payment_authentication_modal_success:I = 0x7f121263

.field public static final payment_authentication_modal_title:I = 0x7f121264

.field public static final payment_authentication_modal_title_canceled:I = 0x7f121265

.field public static final payment_authentication_modal_title_fail:I = 0x7f121266

.field public static final payment_authentication_modal_title_success:I = 0x7f121267

.field public static final payment_modal_button_premium:I = 0x7f121268

.field public static final payment_modal_button_premium_gift:I = 0x7f121269

.field public static final payment_modal_one_month:I = 0x7f12126a

.field public static final payment_modal_one_year:I = 0x7f12126b

.field public static final payment_modal_subtitle_premium_gift:I = 0x7f12126c

.field public static final payment_price_change_body:I = 0x7f12126d

.field public static final payment_price_change_title:I = 0x7f12126e

.field public static final payment_source_card_ending:I = 0x7f12126f

.field public static final payment_source_card_ending_in:I = 0x7f121270

.field public static final payment_source_card_expires:I = 0x7f121271

.field public static final payment_source_card_number:I = 0x7f121272

.field public static final payment_source_confirm_paypal_details:I = 0x7f121273

.field public static final payment_source_credit_card:I = 0x7f121274

.field public static final payment_source_delete:I = 0x7f121275

.field public static final payment_source_delete_disabled_tooltip:I = 0x7f121276

.field public static final payment_source_deleted:I = 0x7f121277

.field public static final payment_source_edit_help_card:I = 0x7f121278

.field public static final payment_source_edit_help_paypal:I = 0x7f121279

.field public static final payment_source_edit_saved:I = 0x7f12127a

.field public static final payment_source_edit_select_state:I = 0x7f12127b

.field public static final payment_source_edit_title:I = 0x7f12127c

.field public static final payment_source_expiration_date_placeholder:I = 0x7f12127d

.field public static final payment_source_information:I = 0x7f12127e

.field public static final payment_source_invalid:I = 0x7f12127f

.field public static final payment_source_invalid_help:I = 0x7f121280

.field public static final payment_source_make_default:I = 0x7f121281

.field public static final payment_source_name_error_required:I = 0x7f121282

.field public static final payment_source_payment_request_button_generic:I = 0x7f121283

.field public static final payment_source_payment_request_button_gpay:I = 0x7f121284

.field public static final payment_source_payment_request_info_creating:I = 0x7f121285

.field public static final payment_source_payment_request_info_loading:I = 0x7f121286

.field public static final payment_source_payment_request_info_title:I = 0x7f121287

.field public static final payment_source_payment_request_label:I = 0x7f121288

.field public static final payment_source_payment_request_unsupported:I = 0x7f121289

.field public static final payment_source_paypal:I = 0x7f12128a

.field public static final payment_source_paypal_account:I = 0x7f12128b

.field public static final payment_source_paypal_details:I = 0x7f12128c

.field public static final payment_source_paypal_loading:I = 0x7f12128d

.field public static final payment_source_paypal_reopen:I = 0x7f12128e

.field public static final payment_source_subscription:I = 0x7f12128f

.field public static final payment_source_type:I = 0x7f121290

.field public static final payment_source_type_select:I = 0x7f121291

.field public static final payment_sources_add:I = 0x7f121292

.field public static final payment_waiting_for_authentication:I = 0x7f121293

.field public static final paypal_account_verifying:I = 0x7f121294

.field public static final paypal_callback_error:I = 0x7f121295

.field public static final paypal_callback_success:I = 0x7f121296

.field public static final pending:I = 0x7f121297

.field public static final people:I = 0x7f121298

.field public static final percentage_since_last_week:I = 0x7f121299

.field public static final permission_helpdesk:I = 0x7f12129a

.field public static final permission_media_denied:I = 0x7f12129b

.field public static final permission_media_download_denied:I = 0x7f12129c

.field public static final permission_microphone_denied:I = 0x7f12129d

.field public static final permission_override_allow:I = 0x7f12129e

.field public static final permission_override_deny:I = 0x7f12129f

.field public static final permission_override_passthrough:I = 0x7f1212a0

.field public static final permission_overrides:I = 0x7f1212a1

.field public static final permission_qr_scanner_denied:I = 0x7f1212a2

.field public static final permissions:I = 0x7f1212a3

.field public static final permissions_unsynced:I = 0x7f1212a4

.field public static final permit_usage_android:I = 0x7f1212a5

.field public static final personalization_disable_modal_body:I = 0x7f1212a6

.field public static final personalization_disable_modal_cancel:I = 0x7f1212a7

.field public static final personalization_disable_modal_confirm:I = 0x7f1212a8

.field public static final personalization_disable_modal_title:I = 0x7f1212a9

.field public static final phone:I = 0x7f1212aa

.field public static final phone_ip_authorization_subtitle:I = 0x7f1212ab

.field public static final phone_ip_authorization_title:I = 0x7f1212ac

.field public static final phone_verification_add_title:I = 0x7f1212ad

.field public static final phone_verification_current_phone:I = 0x7f1212ae

.field public static final phone_verification_new_phone_label:I = 0x7f1212af

.field public static final phone_verification_phone_label:I = 0x7f1212b0

.field public static final phone_verification_receive_text:I = 0x7f1212b1

.field public static final phone_verification_update_title:I = 0x7f1212b2

.field public static final pick_a_color:I = 0x7f1212b3

.field public static final pin:I = 0x7f1212b4

.field public static final pin_confirm:I = 0x7f1212b5

.field public static final pin_message:I = 0x7f1212b6

.field public static final pin_message_body:I = 0x7f1212b7

.field public static final pin_message_body_mobile:I = 0x7f1212b8

.field public static final pin_message_body_private_channel:I = 0x7f1212b9

.field public static final pin_message_title:I = 0x7f1212ba

.field public static final pin_message_too_many_body:I = 0x7f1212bb

.field public static final pin_message_too_many_body_private_channel:I = 0x7f1212bc

.field public static final pin_message_too_many_title:I = 0x7f1212bd

.field public static final pinned_messages:I = 0x7f1212be

.field public static final pinned_messages_pro_tip:I = 0x7f1212bf

.field public static final pinned_messages_pro_tip_body_channel:I = 0x7f1212c0

.field public static final pinned_messages_pro_tip_body_dm:I = 0x7f1212c1

.field public static final pinned_messages_pro_tip_body_group_dm:I = 0x7f1212c2

.field public static final pins:I = 0x7f1212c3

.field public static final pins_disabled_nsfw:I = 0x7f1212c4

.field public static final pl:I = 0x7f1212c5

.field public static final platform_android:I = 0x7f1212c6

.field public static final platform_ios:I = 0x7f1212c7

.field public static final platform_linux:I = 0x7f1212c8

.field public static final platform_macos:I = 0x7f1212c9

.field public static final platform_windows:I = 0x7f1212ca

.field public static final play:I = 0x7f1212cb

.field public static final play_again:I = 0x7f1212cc

.field public static final play_full_video:I = 0x7f1212cd

.field public static final play_stream:I = 0x7f1212ce

.field public static final playing_game:I = 0x7f1212cf

.field public static final popout_player:I = 0x7f1212d0

.field public static final popout_player_opened:I = 0x7f1212d1

.field public static final popout_remove_from_top:I = 0x7f1212d2

.field public static final popout_return:I = 0x7f1212d3

.field public static final popout_stay_on_top:I = 0x7f1212d4

.field public static final preference_copied:I = 0x7f1212d5

.field public static final premium:I = 0x7f1212d6

.field public static final premium_activated:I = 0x7f1212d7

.field public static final premium_alert_error_title:I = 0x7f1212d8

.field public static final premium_and_premium_guild_subscription:I = 0x7f1212d9

.field public static final premium_badge_tooltip:I = 0x7f1212da

.field public static final premium_cancel_cancel_mobile:I = 0x7f1212db

.field public static final premium_cancel_confirm_body:I = 0x7f1212dc

.field public static final premium_cancel_confirm_body_tier_1:I = 0x7f1212dd

.field public static final premium_cancel_confirm_body_tier_1_mobile_part_1:I = 0x7f1212de

.field public static final premium_cancel_confirm_body_tier_1_mobile_part_2:I = 0x7f1212df

.field public static final premium_cancel_confirm_body_tier_2:I = 0x7f1212e0

.field public static final premium_cancel_confirm_body_tier_2_mobile_part_1:I = 0x7f1212e1

.field public static final premium_cancel_confirm_body_tier_2_mobile_part_2:I = 0x7f1212e2

.field public static final premium_cancel_confirm_button:I = 0x7f1212e3

.field public static final premium_cancel_confirm_header:I = 0x7f1212e4

.field public static final premium_cancel_confirm_mobile:I = 0x7f1212e5

.field public static final premium_cancel_confirm_new:I = 0x7f1212e6

.field public static final premium_cancel_confirm_title:I = 0x7f1212e7

.field public static final premium_cancel_failed_body:I = 0x7f1212e8

.field public static final premium_cancel_past_due_confirm_body:I = 0x7f1212e9

.field public static final premium_change_discriminator_length_error:I = 0x7f1212ea

.field public static final premium_change_discriminator_modal_body:I = 0x7f1212eb

.field public static final premium_change_discriminator_modal_confirm:I = 0x7f1212ec

.field public static final premium_change_discriminator_modal_header:I = 0x7f1212ed

.field public static final premium_change_discriminator_tooltip:I = 0x7f1212ee

.field public static final premium_change_discriminator_warning:I = 0x7f1212ef

.field public static final premium_change_discriminator_zero_error:I = 0x7f1212f0

.field public static final premium_chat_perks_animated_avatar_and_emoji_mobile:I = 0x7f1212f1

.field public static final premium_chat_perks_custom_emoji_mobile:I = 0x7f1212f2

.field public static final premium_chat_perks_description_premium_guild_discount:I = 0x7f1212f3

.field public static final premium_chat_perks_description_premium_guild_included:I = 0x7f1212f4

.field public static final premium_chat_perks_description_upload_limit:I = 0x7f1212f5

.field public static final premium_chat_perks_discriminator_mobile:I = 0x7f1212f6

.field public static final premium_chat_perks_premium_guild_subscription:I = 0x7f1212f7

.field public static final premium_chat_perks_rep_mobile:I = 0x7f1212f8

.field public static final premium_chat_perks_screenshare_mobile:I = 0x7f1212f9

.field public static final premium_chat_perks_simple_title:I = 0x7f1212fa

.field public static final premium_chat_perks_stickers:I = 0x7f1212fb

.field public static final premium_chat_perks_streaming_mobile:I = 0x7f1212fc

.field public static final premium_choose_plan_title:I = 0x7f1212fd

.field public static final premium_classic_payment_gift_blurb_mobile:I = 0x7f1212fe

.field public static final premium_current_active_subscription:I = 0x7f1212ff

.field public static final premium_downgrade_done_button:I = 0x7f121300

.field public static final premium_features_chat_perks:I = 0x7f121301

.field public static final premium_features_chat_perks_header:I = 0x7f121302

.field public static final premium_game:I = 0x7f121303

.field public static final premium_gift_button_label:I = 0x7f121304

.field public static final premium_gift_button_tooltip:I = 0x7f121305

.field public static final premium_gift_send:I = 0x7f121306

.field public static final premium_gift_share_link_ios:I = 0x7f121307

.field public static final premium_gifting_button:I = 0x7f121308

.field public static final premium_gifting_title:I = 0x7f121309

.field public static final premium_gifting_title_mobile:I = 0x7f12130a

.field public static final premium_grandfathered_monthly:I = 0x7f12130b

.field public static final premium_grandfathered_warning:I = 0x7f12130c

.field public static final premium_grandfathered_warning_confirm:I = 0x7f12130d

.field public static final premium_grandfathered_warning_mobile:I = 0x7f12130e

.field public static final premium_grandfathered_warning_title:I = 0x7f12130f

.field public static final premium_grandfathered_yearly:I = 0x7f121310

.field public static final premium_guild_cooldown_available_countdown:I = 0x7f121311

.field public static final premium_guild_emoji_promo_description:I = 0x7f121312

.field public static final premium_guild_emoji_promo_title:I = 0x7f121313

.field public static final premium_guild_features_custom_emoji_mobile:I = 0x7f121314

.field public static final premium_guild_features_upload_sizes_mobile:I = 0x7f121315

.field public static final premium_guild_features_upsell_banner_subscribe:I = 0x7f121316

.field public static final premium_guild_features_voice_quality:I = 0x7f121317

.field public static final premium_guild_full_feature_list:I = 0x7f121318

.field public static final premium_guild_grace_period_cooldown:I = 0x7f121319

.field public static final premium_guild_grace_period_cooldown_soon:I = 0x7f12131a

.field public static final premium_guild_grace_period_notice:I = 0x7f12131b

.field public static final premium_guild_grace_period_notice_button:I = 0x7f12131c

.field public static final premium_guild_grace_period_title:I = 0x7f12131d

.field public static final premium_guild_guild_feature_audio_quality:I = 0x7f12131e

.field public static final premium_guild_guild_feature_emoji:I = 0x7f12131f

.field public static final premium_guild_guild_feature_upload_size:I = 0x7f121320

.field public static final premium_guild_guild_features_header:I = 0x7f121321

.field public static final premium_guild_header_badge_no_tier:I = 0x7f121322

.field public static final premium_guild_header_dropdown_nitro_server_subscribe:I = 0x7f121323

.field public static final premium_guild_num_month_guild_subscriptions:I = 0x7f121324

.field public static final premium_guild_num_year_guild_subscriptions:I = 0x7f121325

.field public static final premium_guild_perks_modal_base_perks:I = 0x7f121326

.field public static final premium_guild_perks_modal_blurb:I = 0x7f121327

.field public static final premium_guild_perks_modal_blurb_mobile:I = 0x7f121328

.field public static final premium_guild_perks_modal_blurb_mobile_learn_more:I = 0x7f121329

.field public static final premium_guild_perks_modal_boost_alert_ios_body:I = 0x7f12132a

.field public static final premium_guild_perks_modal_boost_alert_ios_title:I = 0x7f12132b

.field public static final premium_guild_perks_modal_button_gift_premium:I = 0x7f12132c

.field public static final premium_guild_perks_modal_button_subscribe_this_server:I = 0x7f12132d

.field public static final premium_guild_perks_modal_button_upgrade_to_premium_external_error:I = 0x7f12132e

.field public static final premium_guild_perks_modal_button_upgrade_to_premium_ios_error:I = 0x7f12132f

.field public static final premium_guild_perks_modal_gift_nitro:I = 0x7f121330

.field public static final premium_guild_perks_modal_header:I = 0x7f121331

.field public static final premium_guild_perks_modal_header_subscription_count:I = 0x7f121332

.field public static final premium_guild_perks_modal_header_user_subscription_count:I = 0x7f121333

.field public static final premium_guild_perks_modal_level_subscriber_count_tooltip:I = 0x7f121334

.field public static final premium_guild_perks_modal_manage_your_subscriptions:I = 0x7f121335

.field public static final premium_guild_perks_modal_previous_perks:I = 0x7f121336

.field public static final premium_guild_perks_modal_protip:I = 0x7f121337

.field public static final premium_guild_perks_modal_protip_mobile:I = 0x7f121338

.field public static final premium_guild_perks_modal_subscribe_to_tier_tooltip:I = 0x7f121339

.field public static final premium_guild_perks_modal_subscriber_perks_header:I = 0x7f12133a

.field public static final premium_guild_plan_month:I = 0x7f12133b

.field public static final premium_guild_plan_year:I = 0x7f12133c

.field public static final premium_guild_settings_members_subscribed_needed:I = 0x7f12133d

.field public static final premium_guild_settings_x_of_y_subscriptions:I = 0x7f12133e

.field public static final premium_guild_subscribe_confirm_blurb:I = 0x7f12133f

.field public static final premium_guild_subscribe_confirm_cancel_warning:I = 0x7f121340

.field public static final premium_guild_subscribe_confirm_confirmation:I = 0x7f121341

.field public static final premium_guild_subscribe_confirm_confirmation_label:I = 0x7f121342

.field public static final premium_guild_subscribe_confirm_cooldown_warning:I = 0x7f121343

.field public static final premium_guild_subscribe_confirm_transfer_blurb:I = 0x7f121344

.field public static final premium_guild_subscribe_confirm_transfer_from_guild:I = 0x7f121345

.field public static final premium_guild_subscribe_confirm_transfer_header:I = 0x7f121346

.field public static final premium_guild_subscribe_confirm_transfer_to_guild:I = 0x7f121347

.field public static final premium_guild_subscribe_search_guild_placeholder:I = 0x7f121348

.field public static final premium_guild_subscribe_select_guild_header:I = 0x7f121349

.field public static final premium_guild_subscribe_select_guild_transfer_header:I = 0x7f12134a

.field public static final premium_guild_subscribe_success_okay:I = 0x7f12134b

.field public static final premium_guild_subscribe_success_thanks:I = 0x7f12134c

.field public static final premium_guild_subscribe_transfer_confirm_confirmation:I = 0x7f12134d

.field public static final premium_guild_subscribe_transfer_confirm_confirmation_label:I = 0x7f12134e

.field public static final premium_guild_subscribe_transfer_error_body:I = 0x7f12134f

.field public static final premium_guild_subscribe_transfer_error_header:I = 0x7f121350

.field public static final premium_guild_subscribe_unused_slots_cancel:I = 0x7f121351

.field public static final premium_guild_subscribe_unused_slots_counter:I = 0x7f121352

.field public static final premium_guild_subscribe_unused_slots_description:I = 0x7f121353

.field public static final premium_guild_subscribe_unused_slots_header:I = 0x7f121354

.field public static final premium_guild_subscribe_unused_slots_next:I = 0x7f121355

.field public static final premium_guild_subscriber_feature_member_badge:I = 0x7f121356

.field public static final premium_guild_subscriber_feature_profile_badge:I = 0x7f121357

.field public static final premium_guild_subscriber_feature_role:I = 0x7f121358

.field public static final premium_guild_subscription:I = 0x7f121359

.field public static final premium_guild_subscription_active_title:I = 0x7f12135a

.field public static final premium_guild_subscription_available:I = 0x7f12135b

.field public static final premium_guild_subscription_cancel_body_external:I = 0x7f12135c

.field public static final premium_guild_subscription_cancel_body_guild:I = 0x7f12135d

.field public static final premium_guild_subscription_cancel_body_inventory:I = 0x7f12135e

.field public static final premium_guild_subscription_cancel_button:I = 0x7f12135f

.field public static final premium_guild_subscription_cancel_button_disabled_past_due_tooltip:I = 0x7f121360

.field public static final premium_guild_subscription_cancel_button_disabled_tooltip:I = 0x7f121361

.field public static final premium_guild_subscription_cancel_button_mobile:I = 0x7f121362

.field public static final premium_guild_subscription_cancel_error_mobile:I = 0x7f121363

.field public static final premium_guild_subscription_cancel_invoice_subscription_cancelled:I = 0x7f121364

.field public static final premium_guild_subscription_cancel_preview:I = 0x7f121365

.field public static final premium_guild_subscription_cancel_title:I = 0x7f121366

.field public static final premium_guild_subscription_cancel_title_guild:I = 0x7f121367

.field public static final premium_guild_subscription_cancel_title_inventory:I = 0x7f121368

.field public static final premium_guild_subscription_cancel_title_pending_cancellation:I = 0x7f121369

.field public static final premium_guild_subscription_confirm_body:I = 0x7f12136a

.field public static final premium_guild_subscription_confirm_title:I = 0x7f12136b

.field public static final premium_guild_subscription_creation_date:I = 0x7f12136c

.field public static final premium_guild_subscription_duration:I = 0x7f12136d

.field public static final premium_guild_subscription_error_mobile:I = 0x7f12136e

.field public static final premium_guild_subscription_error_pending_mutation:I = 0x7f12136f

.field public static final premium_guild_subscription_error_rate_limit_days:I = 0x7f121370

.field public static final premium_guild_subscription_error_rate_limit_hours:I = 0x7f121371

.field public static final premium_guild_subscription_error_rate_limit_minutes:I = 0x7f121372

.field public static final premium_guild_subscription_guild_affinity_notice:I = 0x7f121373

.field public static final premium_guild_subscription_guild_affinity_notice_button:I = 0x7f121374

.field public static final premium_guild_subscription_guild_affinity_notice_small_guild:I = 0x7f121375

.field public static final premium_guild_subscription_guild_subsription_subtitle:I = 0x7f121376

.field public static final premium_guild_subscription_header_subscribe_tooltip_close:I = 0x7f121377

.field public static final premium_guild_subscription_header_subscribe_tooltip_header:I = 0x7f121378

.field public static final premium_guild_subscription_header_subscribe_tooltip_text:I = 0x7f121379

.field public static final premium_guild_subscription_inventory_uncancel_button:I = 0x7f12137a

.field public static final premium_guild_subscription_inventory_uncancel_button_mobile:I = 0x7f12137b

.field public static final premium_guild_subscription_inventory_uncancel_confirm_description:I = 0x7f12137c

.field public static final premium_guild_subscription_inventory_uncancel_confirm_title:I = 0x7f12137d

.field public static final premium_guild_subscription_inventory_uncancel_description:I = 0x7f12137e

.field public static final premium_guild_subscription_inventory_uncancel_error:I = 0x7f12137f

.field public static final premium_guild_subscription_inventory_uncancel_title:I = 0x7f121380

.field public static final premium_guild_subscription_inventory_uncancel_title_mobile:I = 0x7f121381

.field public static final premium_guild_subscription_marketing_blurb:I = 0x7f121382

.field public static final premium_guild_subscription_marketing_guilds_blurb:I = 0x7f121383

.field public static final premium_guild_subscription_marketing_guilds_blurb_prompt:I = 0x7f121384

.field public static final premium_guild_subscription_marketing_guilds_blurb_without_prompt:I = 0x7f121385

.field public static final premium_guild_subscription_marketing_guilds_empty_blurb_mobile:I = 0x7f121386

.field public static final premium_guild_subscription_marketing_guilds_empty_description_text:I = 0x7f121387

.field public static final premium_guild_subscription_marketing_guilds_empty_description_title:I = 0x7f121388

.field public static final premium_guild_subscription_marketing_header:I = 0x7f121389

.field public static final premium_guild_subscription_marketing_header_ios:I = 0x7f12138a

.field public static final premium_guild_subscription_notice:I = 0x7f12138b

.field public static final premium_guild_subscription_notice_button:I = 0x7f12138c

.field public static final premium_guild_subscription_notice_small_guild:I = 0x7f12138d

.field public static final premium_guild_subscription_num_subscriptions:I = 0x7f12138e

.field public static final premium_guild_subscription_out_of_slots_account_hold:I = 0x7f12138f

.field public static final premium_guild_subscription_out_of_slots_account_hold_title:I = 0x7f121390

.field public static final premium_guild_subscription_out_of_slots_canceled_subscription:I = 0x7f121391

.field public static final premium_guild_subscription_out_of_slots_pending_plan_change:I = 0x7f121392

.field public static final premium_guild_subscription_out_of_slots_purchase_on_desktop:I = 0x7f121393

.field public static final premium_guild_subscription_out_of_slots_title:I = 0x7f121394

.field public static final premium_guild_subscription_pending_cancelation:I = 0x7f121395

.field public static final premium_guild_subscription_purchase_button_disabled_pending_mutation_plan:I = 0x7f121396

.field public static final premium_guild_subscription_purchase_button_disabled_pending_mutation_premium_guild_subscription:I = 0x7f121397

.field public static final premium_guild_subscription_purchase_button_disabled_subscription_account_hold:I = 0x7f121398

.field public static final premium_guild_subscription_purchase_button_disabled_subscription_past_due:I = 0x7f121399

.field public static final premium_guild_subscription_select_server_button:I = 0x7f12139a

.field public static final premium_guild_subscription_slot_cancel_button:I = 0x7f12139b

.field public static final premium_guild_subscription_slot_uncancel_button:I = 0x7f12139c

.field public static final premium_guild_subscription_slot_uncancel_button_mobile:I = 0x7f12139d

.field public static final premium_guild_subscription_subscribe:I = 0x7f12139e

.field public static final premium_guild_subscription_subscriber_count_tooltip:I = 0x7f12139f

.field public static final premium_guild_subscription_subtitle:I = 0x7f1213a0

.field public static final premium_guild_subscription_subtitle_mobile_1:I = 0x7f1213a1

.field public static final premium_guild_subscription_subtitle_mobile_2:I = 0x7f1213a2

.field public static final premium_guild_subscription_title:I = 0x7f1213a3

.field public static final premium_guild_subscription_tooltip:I = 0x7f1213a4

.field public static final premium_guild_subscription_transfer_button:I = 0x7f1213a5

.field public static final premium_guild_subscription_transfer_button_disabled_tooltip:I = 0x7f1213a6

.field public static final premium_guild_subscription_unused_slot_description:I = 0x7f1213a7

.field public static final premium_guild_subscriptions_renewal_info:I = 0x7f1213a8

.field public static final premium_guild_subscriptions_renewal_info_account_hold:I = 0x7f1213a9

.field public static final premium_guild_subscriptions_renewal_info_account_hold_no_price:I = 0x7f1213aa

.field public static final premium_guild_subscriptions_renewal_info_android:I = 0x7f1213ab

.field public static final premium_guild_subscriptions_renewal_info_no_price:I = 0x7f1213ac

.field public static final premium_guild_subscriptions_renewal_info_pending_cancelation:I = 0x7f1213ad

.field public static final premium_guild_subscriptions_renewal_info_pending_cancelation_android:I = 0x7f1213ae

.field public static final premium_guild_subscriptions_renewal_info_pending_cancelation_no_price:I = 0x7f1213af

.field public static final premium_guild_tier_0:I = 0x7f1213b0

.field public static final premium_guild_tier_1:I = 0x7f1213b1

.field public static final premium_guild_tier_2:I = 0x7f1213b2

.field public static final premium_guild_tier_3:I = 0x7f1213b3

.field public static final premium_guild_unsubscribe_confirm_blurb:I = 0x7f1213b4

.field public static final premium_guild_unsubscribe_confirm_confirmation:I = 0x7f1213b5

.field public static final premium_guild_unsubscribe_confirm_cooldown_warning:I = 0x7f1213b6

.field public static final premium_guild_unsubscribe_confirm_cooldown_warning_days:I = 0x7f1213b7

.field public static final premium_guild_unsubscribe_confirm_cooldown_warning_hours:I = 0x7f1213b8

.field public static final premium_guild_unsubscribe_confirm_cooldown_warning_minutes:I = 0x7f1213b9

.field public static final premium_guild_unsubscribe_confirm_header:I = 0x7f1213ba

.field public static final premium_guild_user_feature_member_badge:I = 0x7f1213bb

.field public static final premium_guild_user_feature_profile_badge:I = 0x7f1213bc

.field public static final premium_guild_user_feature_role:I = 0x7f1213bd

.field public static final premium_guild_user_features_header:I = 0x7f1213be

.field public static final premium_included:I = 0x7f1213bf

.field public static final premium_legacy:I = 0x7f1213c0

.field public static final premium_manage_via_desktop:I = 0x7f1213c1

.field public static final premium_marketing_feature_emoji_description:I = 0x7f1213c2

.field public static final premium_marketing_feature_emoji_title:I = 0x7f1213c3

.field public static final premium_marketing_feature_header:I = 0x7f1213c4

.field public static final premium_marketing_feature_premium_badge_description:I = 0x7f1213c5

.field public static final premium_marketing_feature_premium_badge_title:I = 0x7f1213c6

.field public static final premium_marketing_feature_premium_go_live_description:I = 0x7f1213c7

.field public static final premium_marketing_feature_premium_go_live_title:I = 0x7f1213c8

.field public static final premium_marketing_feature_premium_guild_description:I = 0x7f1213c9

.field public static final premium_marketing_feature_premium_guild_title:I = 0x7f1213ca

.field public static final premium_marketing_feature_premium_stickers_description:I = 0x7f1213cb

.field public static final premium_marketing_feature_premium_stickers_title:I = 0x7f1213cc

.field public static final premium_marketing_feature_premium_upload_size_description:I = 0x7f1213cd

.field public static final premium_marketing_feature_premium_upload_size_title:I = 0x7f1213ce

.field public static final premium_marketing_feature_profile_description:I = 0x7f1213cf

.field public static final premium_marketing_feature_profile_title:I = 0x7f1213d0

.field public static final premium_marketing_hero_body:I = 0x7f1213d1

.field public static final premium_marketing_hero_body_stickers:I = 0x7f1213d2

.field public static final premium_marketing_hero_header:I = 0x7f1213d3

.field public static final premium_no_plans_body:I = 0x7f1213d4

.field public static final premium_no_plans_header:I = 0x7f1213d5

.field public static final premium_not_claimed:I = 0x7f1213d6

.field public static final premium_not_claimed_body:I = 0x7f1213d7

.field public static final premium_not_verified:I = 0x7f1213d8

.field public static final premium_not_verified_body:I = 0x7f1213d9

.field public static final premium_payment_confirmation_blurb_iap:I = 0x7f1213da

.field public static final premium_payment_confirmation_blurb_regular:I = 0x7f1213db

.field public static final premium_payment_confirmation_button_iap:I = 0x7f1213dc

.field public static final premium_payment_confirmation_button_regular:I = 0x7f1213dd

.field public static final premium_payment_gift_blurb_mobile:I = 0x7f1213de

.field public static final premium_payment_gift_subtext_monthly:I = 0x7f1213df

.field public static final premium_payment_gift_subtext_yearly:I = 0x7f1213e0

.field public static final premium_payment_is_gift:I = 0x7f1213e1

.field public static final premium_payment_select:I = 0x7f1213e2

.field public static final premium_pending_plan_change_cancel_body:I = 0x7f1213e3

.field public static final premium_pending_plan_change_cancel_button:I = 0x7f1213e4

.field public static final premium_pending_plan_change_cancel_header:I = 0x7f1213e5

.field public static final premium_pending_plan_change_notice:I = 0x7f1213e6

.field public static final premium_plan_month:I = 0x7f1213e7

.field public static final premium_plan_month_tier_1:I = 0x7f1213e8

.field public static final premium_plan_month_tier_2:I = 0x7f1213e9

.field public static final premium_plan_month_tier_2_trial:I = 0x7f1213ea

.field public static final premium_plan_year:I = 0x7f1213eb

.field public static final premium_plan_year_tier_1:I = 0x7f1213ec

.field public static final premium_plan_year_tier_2:I = 0x7f1213ed

.field public static final premium_plan_year_tier_2_trial:I = 0x7f1213ee

.field public static final premium_promo_description:I = 0x7f1213ef

.field public static final premium_promo_description_trial:I = 0x7f1213f0

.field public static final premium_promo_title:I = 0x7f1213f1

.field public static final premium_required:I = 0x7f1213f2

.field public static final premium_required_get_nitro:I = 0x7f1213f3

.field public static final premium_restore_subscription_ios:I = 0x7f1213f4

.field public static final premium_retention_emoji_picker_description:I = 0x7f1213f5

.field public static final premium_retention_emoji_picker_update_payment_information:I = 0x7f1213f6

.field public static final premium_settings:I = 0x7f1213f7

.field public static final premium_settings_account_hold_info:I = 0x7f1213f8

.field public static final premium_settings_account_hold_info_external:I = 0x7f1213f9

.field public static final premium_settings_account_hold_info_price:I = 0x7f1213fa

.field public static final premium_settings_account_hold_notice_change_payment_method_button:I = 0x7f1213fb

.field public static final premium_settings_account_hold_notice_info:I = 0x7f1213fc

.field public static final premium_settings_billing_info:I = 0x7f1213fd

.field public static final premium_settings_cancelled_info:I = 0x7f1213fe

.field public static final premium_settings_get:I = 0x7f1213ff

.field public static final premium_settings_manage:I = 0x7f121400

.field public static final premium_settings_past_due_info:I = 0x7f121401

.field public static final premium_settings_past_due_info_external:I = 0x7f121402

.field public static final premium_settings_premium_guild_subscriptions:I = 0x7f121403

.field public static final premium_settings_renewal_info:I = 0x7f121404

.field public static final premium_settings_renewal_info_external:I = 0x7f121405

.field public static final premium_settings_renewal_info_with_plan:I = 0x7f121406

.field public static final premium_settings_starting_at_per_month:I = 0x7f121407

.field public static final premium_settings_subscribe_today:I = 0x7f121408

.field public static final premium_settings_subscriptions_mobile_header:I = 0x7f121409

.field public static final premium_subscription_adjustment_tooltip:I = 0x7f12140a

.field public static final premium_subscription_billing_info_mobile:I = 0x7f12140b

.field public static final premium_subscription_cancelled:I = 0x7f12140c

.field public static final premium_subscription_credit:I = 0x7f12140d

.field public static final premium_subscription_credit_applied_mismatched_plan:I = 0x7f12140e

.field public static final premium_subscription_credit_applied_on:I = 0x7f12140f

.field public static final premium_subscription_credit_count_months:I = 0x7f121410

.field public static final premium_subscription_current_label:I = 0x7f121411

.field public static final premium_subscription_description_tier_1:I = 0x7f121412

.field public static final premium_subscription_description_tier_1_account_hold:I = 0x7f121413

.field public static final premium_subscription_description_tier_1_account_hold_no_price:I = 0x7f121414

.field public static final premium_subscription_description_tier_1_no_price:I = 0x7f121415

.field public static final premium_subscription_description_tier_1_pending_cancelation:I = 0x7f121416

.field public static final premium_subscription_description_tier_1_pending_cancelation_no_price:I = 0x7f121417

.field public static final premium_subscription_description_tier_2:I = 0x7f121418

.field public static final premium_subscription_description_tier_2_account_hold:I = 0x7f121419

.field public static final premium_subscription_description_tier_2_account_hold_no_price:I = 0x7f12141a

.field public static final premium_subscription_description_tier_2_no_price:I = 0x7f12141b

.field public static final premium_subscription_description_tier_2_pending_cancelation:I = 0x7f12141c

.field public static final premium_subscription_description_tier_2_pending_cancelation_no_price:I = 0x7f12141d

.field public static final premium_subscription_grandfathered_upgrade_note:I = 0x7f12141e

.field public static final premium_subscription_guild_subscription_adjustment:I = 0x7f12141f

.field public static final premium_subscription_guild_subscription_adjustment_text:I = 0x7f121420

.field public static final premium_subscription_guild_subscription_adjustment_tooltip:I = 0x7f121421

.field public static final premium_subscription_hide_details:I = 0x7f121422

.field public static final premium_subscription_new_label:I = 0x7f121423

.field public static final premium_subscription_new_total:I = 0x7f121424

.field public static final premium_subscription_period_reset_notice:I = 0x7f121425

.field public static final premium_subscription_plan_adjustment:I = 0x7f121426

.field public static final premium_subscription_policy_hint_ios:I = 0x7f121427

.field public static final premium_subscription_policy_ios:I = 0x7f121428

.field public static final premium_subscription_renewal_footer:I = 0x7f121429

.field public static final premium_subscription_renewal_footer_trial:I = 0x7f12142a

.field public static final premium_subscription_required_body:I = 0x7f12142b

.field public static final premium_subscription_show_details:I = 0x7f12142c

.field public static final premium_subscription_updates:I = 0x7f12142d

.field public static final premium_switch_plan_disabled_in_trial:I = 0x7f12142e

.field public static final premium_switch_plan_disabled_pending_mutation_plan:I = 0x7f12142f

.field public static final premium_switch_plan_disabled_pending_mutation_premium_guild_subscription:I = 0x7f121430

.field public static final premium_switch_plans:I = 0x7f121431

.field public static final premium_switch_review_header_premium_month_tier_1:I = 0x7f121432

.field public static final premium_switch_review_header_premium_month_tier_2:I = 0x7f121433

.field public static final premium_switch_review_header_premium_year_tier_1:I = 0x7f121434

.field public static final premium_switch_review_header_premium_year_tier_2:I = 0x7f121435

.field public static final premium_tier_1:I = 0x7f121436

.field public static final premium_tier_1_sticker_pack:I = 0x7f121437

.field public static final premium_tier_2:I = 0x7f121438

.field public static final premium_tier_2_subtitle:I = 0x7f121439

.field public static final premium_tier_2_title:I = 0x7f12143a

.field public static final premium_tier_2_trial_cta_note:I = 0x7f12143b

.field public static final premium_tier_2_trial_cta_subtitle:I = 0x7f12143c

.field public static final premium_tier_2_trial_cta_title:I = 0x7f12143d

.field public static final premium_title:I = 0x7f12143e

.field public static final premium_trial_cta_button:I = 0x7f12143f

.field public static final premium_trial_cta_descirption:I = 0x7f121440

.field public static final premium_upgrade_disabled_in_trial:I = 0x7f121441

.field public static final premium_upgrade_done_body_tier_1:I = 0x7f121442

.field public static final premium_upgrade_done_body_tier_2_guild_subscribe:I = 0x7f121443

.field public static final premium_upgrade_done_button:I = 0x7f121444

.field public static final premium_upgrade_required_body:I = 0x7f121445

.field public static final premium_upload_promo:I = 0x7f121446

.field public static final premium_upload_promo_trial:I = 0x7f121447

.field public static final premium_upsell_animated_avatar_active_mobile:I = 0x7f121448

.field public static final premium_upsell_animated_avatar_description_mobile:I = 0x7f121449

.field public static final premium_upsell_animated_avatar_passive_mobile:I = 0x7f12144a

.field public static final premium_upsell_animated_emojis_active_mobile:I = 0x7f12144b

.field public static final premium_upsell_animated_emojis_description_mobile:I = 0x7f12144c

.field public static final premium_upsell_animated_emojis_passive_mobile:I = 0x7f12144d

.field public static final premium_upsell_badge_active_mobile:I = 0x7f12144e

.field public static final premium_upsell_badge_description_mobile:I = 0x7f12144f

.field public static final premium_upsell_badge_passive_mobile:I = 0x7f121450

.field public static final premium_upsell_continue_to_boosts:I = 0x7f121451

.field public static final premium_upsell_continue_to_monthly:I = 0x7f121452

.field public static final premium_upsell_emoji_active_mobile:I = 0x7f121453

.field public static final premium_upsell_emoji_description_mobile:I = 0x7f121454

.field public static final premium_upsell_emoji_passive_mobile:I = 0x7f121455

.field public static final premium_upsell_feature_chat_perks:I = 0x7f121456

.field public static final premium_upsell_feature_chat_perks_mobile:I = 0x7f121457

.field public static final premium_upsell_feature_free_guild_subscription:I = 0x7f121458

.field public static final premium_upsell_feature_guild_subscription_discount:I = 0x7f121459

.field public static final premium_upsell_feature_pretext:I = 0x7f12145a

.field public static final premium_upsell_feature_pretext_trial:I = 0x7f12145b

.field public static final premium_upsell_feature_stream:I = 0x7f12145c

.field public static final premium_upsell_feature_upload:I = 0x7f12145d

.field public static final premium_upsell_tag_active_mobile:I = 0x7f12145e

.field public static final premium_upsell_tag_description_mobile:I = 0x7f12145f

.field public static final premium_upsell_tag_passive_mobile:I = 0x7f121460

.field public static final premium_upsell_upgrade:I = 0x7f121461

.field public static final premium_upsell_upload_active_mobile:I = 0x7f121462

.field public static final premium_upsell_upload_description_mobile:I = 0x7f121463

.field public static final premium_upsell_upload_passive_mobile:I = 0x7f121464

.field public static final premium_upsell_yearly_cta:I = 0x7f121465

.field public static final premium_upsell_yearly_description:I = 0x7f121466

.field public static final premium_upsell_yearly_title:I = 0x7f121467

.field public static final premium_with_price:I = 0x7f121468

.field public static final preorder_sku_name:I = 0x7f121469

.field public static final priority_speaker:I = 0x7f12146a

.field public static final priority_speaker_description:I = 0x7f12146b

.field public static final priority_speaker_description_app:I = 0x7f12146c

.field public static final privacy_and_safety:I = 0x7f12146d

.field public static final privacy_policy:I = 0x7f12146e

.field public static final privacy_policy_url:I = 0x7f12146f

.field public static final privacy_settings:I = 0x7f121470

.field public static final private_category:I = 0x7f121471

.field public static final private_category_note:I = 0x7f121472

.field public static final private_channel:I = 0x7f121473

.field public static final private_channel_access_info_members:I = 0x7f121474

.field public static final private_channel_access_info_members_overflow:I = 0x7f121475

.field public static final private_channel_access_info_roles:I = 0x7f121476

.field public static final private_channel_access_info_roles_overflow:I = 0x7f121477

.field public static final private_channel_access_info_title:I = 0x7f121478

.field public static final private_channel_access_info_title_with_colon:I = 0x7f121479

.field public static final private_channel_add_members_modal_no_result:I = 0x7f12147a

.field public static final private_channel_add_members_modal_no_roles:I = 0x7f12147b

.field public static final private_channel_add_members_modal_placeholder:I = 0x7f12147c

.field public static final private_channel_add_members_modal_row_administrator:I = 0x7f12147d

.field public static final private_channel_add_members_modal_row_member:I = 0x7f12147e

.field public static final private_channel_add_members_modal_row_owner:I = 0x7f12147f

.field public static final private_channel_add_members_modal_row_role:I = 0x7f121480

.field public static final private_channel_add_members_modal_search_placeholder_mobile:I = 0x7f121481

.field public static final private_channel_add_members_modal_skip:I = 0x7f121482

.field public static final private_channel_add_members_modal_subtitle:I = 0x7f121483

.field public static final private_channel_add_members_modal_subtitle_mobile:I = 0x7f121484

.field public static final private_channel_manage_channel_access_cta:I = 0x7f121485

.field public static final private_channel_members_added_toast_msg:I = 0x7f121486

.field public static final private_channel_members_remove_yes_confirmation:I = 0x7f121487

.field public static final private_channel_members_removed:I = 0x7f121488

.field public static final private_channel_members_roles_added_toast_msg:I = 0x7f121489

.field public static final private_channel_members_section_header:I = 0x7f12148a

.field public static final private_channel_modal_admin_role_force_enabled:I = 0x7f12148b

.field public static final private_channel_note:I = 0x7f12148c

.field public static final private_channel_roles_added_toast_msg:I = 0x7f12148d

.field public static final private_channels_a11y_label:I = 0x7f12148e

.field public static final profile:I = 0x7f12148f

.field public static final progress_completed:I = 0x7f121490

.field public static final progress_completed_description:I = 0x7f121491

.field public static final progress_steps:I = 0x7f121492

.field public static final progress_steps_finished:I = 0x7f121493

.field public static final promotion_card_action_claim:I = 0x7f121495

.field public static final promotion_card_body_claimed_hidden:I = 0x7f121496

.field public static final prune:I = 0x7f121497

.field public static final prune_members:I = 0x7f121498

.field public static final prune_with_roles:I = 0x7f121499

.field public static final pt_br:I = 0x7f12149a

.field public static final ptt_limited_body:I = 0x7f12149b

.field public static final ptt_limited_title:I = 0x7f12149c

.field public static final ptt_limited_warning:I = 0x7f12149d

.field public static final ptt_permission_body:I = 0x7f12149e

.field public static final ptt_permission_title:I = 0x7f12149f

.field public static final public_guild_policy_accept:I = 0x7f1214a0

.field public static final public_guild_policy_title:I = 0x7f1214a1

.field public static final public_locale_help:I = 0x7f1214a2

.field public static final public_locale_selector_title:I = 0x7f1214a3

.field public static final public_locale_title:I = 0x7f1214a4

.field public static final public_rules_channel_title:I = 0x7f1214a5

.field public static final public_rules_selector_title:I = 0x7f1214a6

.field public static final public_updates_channel_title:I = 0x7f1214a7

.field public static final publish_followed_news_body:I = 0x7f1214a8

.field public static final publish_followed_news_body_reach:I = 0x7f1214a9

.field public static final publish_followed_news_body_settings_insights:I = 0x7f1214aa

.field public static final publish_followed_news_fail_body:I = 0x7f1214ab

.field public static final publish_followed_news_fail_title:I = 0x7f1214ac

.field public static final publish_followed_news_generic_body:I = 0x7f1214ad

.field public static final purple:I = 0x7f1214ae

.field public static final qr_code_fail:I = 0x7f1214af

.field public static final qr_code_fail_description:I = 0x7f1214b0

.field public static final qr_code_in_app_scanner_only:I = 0x7f1214b1

.field public static final qr_code_invalid:I = 0x7f1214b2

.field public static final qr_code_login_confirm:I = 0x7f1214b3

.field public static final qr_code_login_finish_button:I = 0x7f1214b4

.field public static final qr_code_login_start_over:I = 0x7f1214b5

.field public static final qr_code_login_success:I = 0x7f1214b6

.field public static final qr_code_login_success_flavor:I = 0x7f1214b7

.field public static final qr_code_not_found:I = 0x7f1214b8

.field public static final qr_code_not_found_description:I = 0x7f1214b9

.field public static final quality_indicator:I = 0x7f1214ba

.field public static final quick_dm_blocked:I = 0x7f1214bb

.field public static final quick_dm_user:I = 0x7f1214bc

.field public static final quick_switcher:I = 0x7f1214bd

.field public static final quickswitcher_drafts:I = 0x7f1214be

.field public static final quickswitcher_empty_cta:I = 0x7f1214bf

.field public static final quickswitcher_empty_text:I = 0x7f1214c0

.field public static final quickswitcher_last_channel:I = 0x7f1214c1

.field public static final quickswitcher_mentions:I = 0x7f1214c2

.field public static final quickswitcher_notice:I = 0x7f1214c3

.field public static final quickswitcher_placeholder:I = 0x7f1214c4

.field public static final quickswitcher_protip:I = 0x7f1214c5

.field public static final quickswitcher_protip_guilds:I = 0x7f1214c6

.field public static final quickswitcher_protip_text_channels:I = 0x7f1214c7

.field public static final quickswitcher_protip_usernames:I = 0x7f1214c8

.field public static final quickswitcher_protip_voice_channels:I = 0x7f1214c9

.field public static final quickswitcher_querymode_applications:I = 0x7f1214ca

.field public static final quickswitcher_querymode_guilds:I = 0x7f1214cb

.field public static final quickswitcher_querymode_text_channels:I = 0x7f1214cc

.field public static final quickswitcher_querymode_users:I = 0x7f1214cd

.field public static final quickswitcher_querymode_users_in_guild:I = 0x7f1214ce

.field public static final quickswitcher_querymode_voice_channels:I = 0x7f1214cf

.field public static final quickswitcher_tip_nav:I = 0x7f1214d0

.field public static final quickswitcher_tip_select:I = 0x7f1214d1

.field public static final quickswitcher_tutorial_message_search:I = 0x7f1214d2

.field public static final quickswitcher_tutorial_message_select:I = 0x7f1214d3

.field public static final quickswitcher_unread_channels:I = 0x7f1214d4

.field public static final quote:I = 0x7f1214d5

.field public static final quote_attribution:I = 0x7f1214d6

.field public static final quote_attribution_facebook:I = 0x7f1214d7

.field public static final quote_attribution_instagram:I = 0x7f1214d8

.field public static final rate_limited:I = 0x7f1214d9

.field public static final rating_request_body_android:I = 0x7f1214da

.field public static final rating_request_title:I = 0x7f1214db

.field public static final react_with_label:I = 0x7f1214dc

.field public static final reaction_tooltip_1:I = 0x7f1214dd

.field public static final reaction_tooltip_1_n:I = 0x7f1214de

.field public static final reaction_tooltip_2:I = 0x7f1214df

.field public static final reaction_tooltip_2_n:I = 0x7f1214e0

.field public static final reaction_tooltip_3:I = 0x7f1214e1

.field public static final reaction_tooltip_3_n:I = 0x7f1214e2

.field public static final reaction_tooltip_n:I = 0x7f1214e3

.field public static final reactions:I = 0x7f1214e4

.field public static final reactions_matching:I = 0x7f1214e5

.field public static final read_message_history:I = 0x7f1214e6

.field public static final read_messages:I = 0x7f1214e7

.field public static final read_messages_view_channels:I = 0x7f1214e8

.field public static final read_only_channel:I = 0x7f1214e9

.field public static final ready:I = 0x7f1214ea

.field public static final recent_mentions:I = 0x7f1214eb

.field public static final recent_mentions_direct_only:I = 0x7f1214ec

.field public static final recent_mentions_empty_state_header:I = 0x7f1214ed

.field public static final recent_mentions_empty_state_tip:I = 0x7f1214ee

.field public static final recent_mentions_everyone_and_direct:I = 0x7f1214ef

.field public static final recent_mentions_filter_all_servers:I = 0x7f1214f0

.field public static final recent_mentions_filter_everyone:I = 0x7f1214f1

.field public static final recent_mentions_filter_explain_everything:I = 0x7f1214f2

.field public static final recent_mentions_filter_label:I = 0x7f1214f3

.field public static final recent_mentions_filter_roles:I = 0x7f1214f4

.field public static final recent_mentions_pro_tip:I = 0x7f1214f5

.field public static final recent_mentions_roles_and_direct:I = 0x7f1214f6

.field public static final recents_notifications_menu_label:I = 0x7f1214f7

.field public static final reconnect:I = 0x7f1214f8

.field public static final reconnecting:I = 0x7f1214f9

.field public static final refund:I = 0x7f1214fa

.field public static final region:I = 0x7f1214fb

.field public static final region_select_footer:I = 0x7f1214fc

.field public static final region_select_header:I = 0x7f1214fd

.field public static final register:I = 0x7f1214fe

.field public static final register_body:I = 0x7f1214ff

.field public static final register_login_privacy_notice:I = 0x7f121500

.field public static final register_title:I = 0x7f121501

.field public static final register_username_hint:I = 0x7f121502

.field public static final remaining_participants:I = 0x7f121503

.field public static final remind_me_later:I = 0x7f121504

.field public static final remove:I = 0x7f121505

.field public static final remove_all_reactions:I = 0x7f121506

.field public static final remove_all_reactions_confirm_body:I = 0x7f121507

.field public static final remove_all_reactions_confirm_title:I = 0x7f121508

.field public static final remove_friend:I = 0x7f121509

.field public static final remove_friend_body:I = 0x7f12150a

.field public static final remove_friend_title:I = 0x7f12150b

.field public static final remove_from_group:I = 0x7f12150c

.field public static final remove_icon:I = 0x7f12150d

.field public static final remove_keybind:I = 0x7f12150e

.field public static final remove_reaction:I = 0x7f12150f

.field public static final remove_role_or_user:I = 0x7f121510

.field public static final remove_synced_role:I = 0x7f121511

.field public static final remove_vanity_url:I = 0x7f121512

.field public static final render_embeds:I = 0x7f121513

.field public static final render_embeds_label:I = 0x7f121514

.field public static final render_reactions:I = 0x7f121515

.field public static final reply_mention_off:I = 0x7f121516

.field public static final reply_mention_off_tooltip:I = 0x7f121517

.field public static final reply_mention_on:I = 0x7f121518

.field public static final reply_mention_on_tooltip:I = 0x7f121519

.field public static final reply_quote_message_blocked:I = 0x7f12151a

.field public static final reply_quote_message_deleted:I = 0x7f12151b

.field public static final reply_quote_message_not_loaded:I = 0x7f12151c

.field public static final reply_quote_no_text_content:I = 0x7f12151d

.field public static final reply_quote_no_text_content_mobile:I = 0x7f12151e

.field public static final replying_to:I = 0x7f12151f

.field public static final report:I = 0x7f121520

.field public static final report_message:I = 0x7f121521

.field public static final report_message_menu_option:I = 0x7f121522

.field public static final report_modal_block_user:I = 0x7f121523

.field public static final report_modal_description:I = 0x7f121524

.field public static final report_modal_description_max_exceeded:I = 0x7f121525

.field public static final report_modal_description_min_max:I = 0x7f121526

.field public static final report_modal_error:I = 0x7f121527

.field public static final report_modal_message_selected:I = 0x7f121528

.field public static final report_modal_report_message:I = 0x7f121529

.field public static final report_modal_select_one:I = 0x7f12152a

.field public static final report_modal_should_block:I = 0x7f12152b

.field public static final report_modal_should_delete:I = 0x7f12152c

.field public static final report_modal_subject:I = 0x7f12152d

.field public static final report_modal_submit:I = 0x7f12152e

.field public static final report_modal_submitted:I = 0x7f12152f

.field public static final report_modal_submitted_email_confirmation:I = 0x7f121530

.field public static final report_modal_type_title:I = 0x7f121531

.field public static final report_server:I = 0x7f121532

.field public static final report_user:I = 0x7f121533

.field public static final resend:I = 0x7f121534

.field public static final resend_code:I = 0x7f121535

.field public static final resend_email:I = 0x7f121536

.field public static final resend_message:I = 0x7f121537

.field public static final resend_verification_email:I = 0x7f121538

.field public static final reset:I = 0x7f121539

.field public static final reset_nickname:I = 0x7f12153a

.field public static final reset_notification_override:I = 0x7f12153b

.field public static final reset_notification_settings:I = 0x7f12153c

.field public static final reset_password_title:I = 0x7f12153d

.field public static final reset_to_default:I = 0x7f12153e

.field public static final reset_voice_settings:I = 0x7f12153f

.field public static final reset_voice_settings_body:I = 0x7f121540

.field public static final resubscribe:I = 0x7f121541

.field public static final retry:I = 0x7f121542

.field public static final return_to_login:I = 0x7f121543

.field public static final reveal:I = 0x7f121544

.field public static final reversed:I = 0x7f121545

.field public static final revoke:I = 0x7f121546

.field public static final revoke_ban:I = 0x7f121547

.field public static final ring:I = 0x7f121548

.field public static final ring_username_a11y_label:I = 0x7f121549

.field public static final ro:I = 0x7f12154a

.field public static final role_add_members_search:I = 0x7f12154b

.field public static final role_add_members_subtitle:I = 0x7f12154c

.field public static final role_add_members_title:I = 0x7f12154d

.field public static final role_color:I = 0x7f12154e

.field public static final role_create:I = 0x7f12154f

.field public static final role_created_toast:I = 0x7f121550

.field public static final role_delete_cannot_be_undone:I = 0x7f121551

.field public static final role_edit_hoist_label:I = 0x7f121552

.field public static final role_edit_members_no_results:I = 0x7f121553

.field public static final role_edit_permissions_no_results:I = 0x7f121554

.field public static final role_edit_saved:I = 0x7f121555

.field public static final role_edit_tab_display:I = 0x7f121556

.field public static final role_edit_tab_members:I = 0x7f121557

.field public static final role_edit_tab_permissions:I = 0x7f121558

.field public static final role_everyone_sublabel:I = 0x7f121559

.field public static final role_id_copied:I = 0x7f12155a

.field public static final role_list_body:I = 0x7f12155b

.field public static final role_list_empty_sort:I = 0x7f12155c

.field public static final role_list_header:I = 0x7f12155d

.field public static final role_member_count:I = 0x7f12155e

.field public static final role_order_updated:I = 0x7f12155f

.field public static final role_overview_description:I = 0x7f121560

.field public static final role_overview_empty_subheader:I = 0x7f121561

.field public static final role_overview_header:I = 0x7f121562

.field public static final role_permission_help_body:I = 0x7f121563

.field public static final role_permission_template_name_manager:I = 0x7f121564

.field public static final role_permission_template_name_member:I = 0x7f121565

.field public static final role_permission_template_name_moderator:I = 0x7f121566

.field public static final role_permission_template_name_visual:I = 0x7f121567

.field public static final role_permission_template_select_cta:I = 0x7f121568

.field public static final role_permission_template_title:I = 0x7f121569

.field public static final role_remove_member_confirm_body:I = 0x7f12156a

.field public static final role_remove_member_confirm_title:I = 0x7f12156b

.field public static final role_required_edit_roles_modal_subtitle:I = 0x7f12156c

.field public static final role_required_edit_roles_modal_title:I = 0x7f12156d

.field public static final role_required_single_user_message:I = 0x7f12156e

.field public static final roles:I = 0x7f12156f

.field public static final roles_list:I = 0x7f121570

.field public static final rtc_connection:I = 0x7f121571

.field public static final rtc_connection_state_authenticating:I = 0x7f121572

.field public static final rtc_connection_state_awaiting_endpoint:I = 0x7f121573

.field public static final rtc_connection_state_connecting:I = 0x7f121574

.field public static final rtc_connection_state_disconnected:I = 0x7f121575

.field public static final rtc_connection_state_ice_checking:I = 0x7f121576

.field public static final rtc_connection_state_no_route:I = 0x7f121577

.field public static final rtc_connection_state_rtc_connected:I = 0x7f121578

.field public static final rtc_connection_state_rtc_connected_loss_rate:I = 0x7f121579

.field public static final rtc_connection_state_rtc_connecting:I = 0x7f12157a

.field public static final rtc_debug_context:I = 0x7f12157b

.field public static final rtc_debug_open:I = 0x7f12157c

.field public static final rtc_debug_rtp_inbound:I = 0x7f12157d

.field public static final rtc_debug_rtp_outbound:I = 0x7f12157e

.field public static final rtc_debug_screenshare:I = 0x7f12157f

.field public static final rtc_debug_transport:I = 0x7f121580

.field public static final ru:I = 0x7f121581

.field public static final salmon:I = 0x7f121582

.field public static final sample_channel_message:I = 0x7f121583

.field public static final sample_channel_name:I = 0x7f121584

.field public static final sample_channel_name_short:I = 0x7f121585

.field public static final sample_channel_search_filter:I = 0x7f121586

.field public static final sample_confirmation:I = 0x7f121587

.field public static final sample_connected_account:I = 0x7f121588

.field public static final sample_discrim:I = 0x7f121589

.field public static final sample_email:I = 0x7f12158a

.field public static final sample_empty_string:I = 0x7f12158b

.field public static final sample_language:I = 0x7f12158c

.field public static final sample_notification_frequency:I = 0x7f12158d

.field public static final sample_number_9:I = 0x7f12158e

.field public static final sample_number_99:I = 0x7f12158f

.field public static final sample_number_999:I = 0x7f121590

.field public static final sample_number_9999:I = 0x7f121591

.field public static final sample_number_bandwidth:I = 0x7f121592

.field public static final sample_number_ratio:I = 0x7f121593

.field public static final sample_online_members:I = 0x7f121594

.field public static final sample_phone:I = 0x7f121595

.field public static final sample_phone_country_code:I = 0x7f121596

.field public static final sample_server_name:I = 0x7f121597

.field public static final sample_server_name_short:I = 0x7f121598

.field public static final sample_server_role_name:I = 0x7f121599

.field public static final sample_time_duration_minutes:I = 0x7f12159a

.field public static final sample_time_duration_seconds:I = 0x7f12159b

.field public static final sample_time_pm:I = 0x7f12159c

.field public static final sample_time_stamp:I = 0x7f12159d

.field public static final sample_total_members:I = 0x7f12159e

.field public static final sample_user_generated_link:I = 0x7f12159f

.field public static final sample_user_name:I = 0x7f1215a0

.field public static final sample_user_name_discrim:I = 0x7f1215a1

.field public static final sample_user_nick:I = 0x7f1215a2

.field public static final sample_user_presence:I = 0x7f1215a3

.field public static final sample_user_presence_details:I = 0x7f1215a4

.field public static final save:I = 0x7f1215a5

.field public static final save_changes:I = 0x7f1215a6

.field public static final save_image:I = 0x7f1215a7

.field public static final save_image_preview:I = 0x7f1215a8

.field public static final save_media_failure_help_mobile:I = 0x7f1215a9

.field public static final save_media_failure_mobile:I = 0x7f1215aa

.field public static final save_media_success_mobile:I = 0x7f1215ab

.field public static final saved_settings:I = 0x7f1215ac

.field public static final scope_activities_read:I = 0x7f1215ad

.field public static final scope_activities_read_description:I = 0x7f1215ae

.field public static final scope_activities_write:I = 0x7f1215af

.field public static final scope_activities_write_description:I = 0x7f1215b0

.field public static final scope_applications_builds_read:I = 0x7f1215b1

.field public static final scope_applications_builds_read_description:I = 0x7f1215b2

.field public static final scope_applications_builds_upload:I = 0x7f1215b3

.field public static final scope_applications_builds_upload_description:I = 0x7f1215b4

.field public static final scope_applications_commands:I = 0x7f1215b5

.field public static final scope_applications_commands_description:I = 0x7f1215b6

.field public static final scope_applications_entitlements:I = 0x7f1215b7

.field public static final scope_applications_entitlements_description:I = 0x7f1215b8

.field public static final scope_applications_store_update:I = 0x7f1215b9

.field public static final scope_applications_store_update_description:I = 0x7f1215ba

.field public static final scope_bot:I = 0x7f1215bb

.field public static final scope_bot_permissions:I = 0x7f1215bc

.field public static final scope_bot_permissions_description:I = 0x7f1215bd

.field public static final scope_connections:I = 0x7f1215be

.field public static final scope_connections_empty:I = 0x7f1215bf

.field public static final scope_email:I = 0x7f1215c0

.field public static final scope_email_empty:I = 0x7f1215c1

.field public static final scope_gdm_join:I = 0x7f1215c2

.field public static final scope_gdm_join_description:I = 0x7f1215c3

.field public static final scope_guilds:I = 0x7f1215c4

.field public static final scope_guilds_empty:I = 0x7f1215c5

.field public static final scope_guilds_join:I = 0x7f1215c6

.field public static final scope_guilds_join_description:I = 0x7f1215c7

.field public static final scope_identify:I = 0x7f1215c8

.field public static final scope_messages_read:I = 0x7f1215c9

.field public static final scope_messages_read_description:I = 0x7f1215ca

.field public static final scope_relationships_read:I = 0x7f1215cb

.field public static final scope_relationships_read_description:I = 0x7f1215cc

.field public static final scope_rpc:I = 0x7f1215cd

.field public static final scope_rpc_description:I = 0x7f1215ce

.field public static final scope_rpc_notifications_read:I = 0x7f1215cf

.field public static final scope_rpc_notifications_read_description:I = 0x7f1215d0

.field public static final scope_unsupported_on_android:I = 0x7f1215d1

.field public static final scope_unsupported_on_android_description:I = 0x7f1215d2

.field public static final scope_webhook_incoming:I = 0x7f1215d3

.field public static final scope_webhook_incoming_channel_placeholder:I = 0x7f1215d4

.field public static final scope_webhook_incoming_description:I = 0x7f1215d5

.field public static final screen_share_nfx_body:I = 0x7f1215d6

.field public static final screen_share_nfx_skip:I = 0x7f1215d7

.field public static final screen_share_nfx_title:I = 0x7f1215d8

.field public static final screen_share_nfx_try:I = 0x7f1215d9

.field public static final screen_share_on:I = 0x7f1215da

.field public static final screen_share_options:I = 0x7f1215db

.field public static final screenshare_change_windows:I = 0x7f1215dc

.field public static final screenshare_description:I = 0x7f1215dd

.field public static final screenshare_frame_rate:I = 0x7f1215de

.field public static final screenshare_relaunch:I = 0x7f1215df

.field public static final screenshare_relaunch_body:I = 0x7f1215e0

.field public static final screenshare_screen:I = 0x7f1215e1

.field public static final screenshare_share_screen_or_window:I = 0x7f1215e2

.field public static final screenshare_sound_toggle_label:I = 0x7f1215e3

.field public static final screenshare_source:I = 0x7f1215e4

.field public static final screenshare_stop:I = 0x7f1215e5

.field public static final screenshare_stream_game:I = 0x7f1215e6

.field public static final screenshare_stream_quality:I = 0x7f1215e7

.field public static final screenshare_unavailable:I = 0x7f1215e8

.field public static final screenshare_unavailable_download_app:I = 0x7f1215e9

.field public static final screenshare_window:I = 0x7f1215ea

.field public static final search:I = 0x7f1215eb

.field public static final search_actions:I = 0x7f1215ec

.field public static final search_answer_date:I = 0x7f1215ed

.field public static final search_answer_file_name:I = 0x7f1215ee

.field public static final search_answer_file_type:I = 0x7f1215ef

.field public static final search_answer_from:I = 0x7f1215f0

.field public static final search_answer_has:I = 0x7f1215f1

.field public static final search_answer_has_attachment:I = 0x7f1215f2

.field public static final search_answer_has_embed:I = 0x7f1215f3

.field public static final search_answer_has_image:I = 0x7f1215f4

.field public static final search_answer_has_link:I = 0x7f1215f5

.field public static final search_answer_has_sound:I = 0x7f1215f6

.field public static final search_answer_has_video:I = 0x7f1215f7

.field public static final search_answer_in:I = 0x7f1215f8

.field public static final search_answer_link_from:I = 0x7f1215f9

.field public static final search_answer_mentions:I = 0x7f1215fa

.field public static final search_channels:I = 0x7f1215fb

.field public static final search_channels_no_result:I = 0x7f1215fc

.field public static final search_clear:I = 0x7f1215fd

.field public static final search_clear_history:I = 0x7f1215fe

.field public static final search_country:I = 0x7f1215ff

.field public static final search_date_picker_hint:I = 0x7f121600

.field public static final search_dm_still_indexing:I = 0x7f121601

.field public static final search_dm_with:I = 0x7f121602

.field public static final search_emojis:I = 0x7f121603

.field public static final search_error:I = 0x7f121604

.field public static final search_filter_after:I = 0x7f121605

.field public static final search_filter_before:I = 0x7f121606

.field public static final search_filter_during:I = 0x7f121607

.field public static final search_filter_file_name:I = 0x7f121608

.field public static final search_filter_file_type:I = 0x7f121609

.field public static final search_filter_from:I = 0x7f12160a

.field public static final search_filter_has:I = 0x7f12160b

.field public static final search_filter_in:I = 0x7f12160c

.field public static final search_filter_link_from:I = 0x7f12160d

.field public static final search_filter_mentions:I = 0x7f12160e

.field public static final search_filter_on:I = 0x7f12160f

.field public static final search_for_emoji:I = 0x7f121610

.field public static final search_for_sticker:I = 0x7f121611

.field public static final search_for_stickers:I = 0x7f121612

.field public static final search_for_value:I = 0x7f121613

.field public static final search_from_suggestions:I = 0x7f121614

.field public static final search_gifs:I = 0x7f121615

.field public static final search_group_header_channels:I = 0x7f121616

.field public static final search_group_header_dates:I = 0x7f121617

.field public static final search_group_header_file_type:I = 0x7f121618

.field public static final search_group_header_from:I = 0x7f121619

.field public static final search_group_header_has:I = 0x7f12161a

.field public static final search_group_header_history:I = 0x7f12161b

.field public static final search_group_header_link_from:I = 0x7f12161c

.field public static final search_group_header_mentions:I = 0x7f12161d

.field public static final search_group_header_search_options:I = 0x7f12161e

.field public static final search_guild_still_indexing:I = 0x7f12161f

.field public static final search_hide_blocked_messages:I = 0x7f121620

.field public static final search_in:I = 0x7f121621

.field public static final search_members:I = 0x7f121622

.field public static final search_members_no_result:I = 0x7f121623

.field public static final search_menu_title:I = 0x7f121624

.field public static final search_most_relevant:I = 0x7f121625

.field public static final search_newest:I = 0x7f121626

.field public static final search_no_results:I = 0x7f121627

.field public static final search_no_results_alt:I = 0x7f121628

.field public static final search_num_results_blocked_not_shown:I = 0x7f121629

.field public static final search_oldest:I = 0x7f12162a

.field public static final search_pagination_a11y_label:I = 0x7f12162b

.field public static final search_pick_date:I = 0x7f12162c

.field public static final search_results_section_label:I = 0x7f12162d

.field public static final search_roles:I = 0x7f12162e

.field public static final search_roles_no_result:I = 0x7f12162f

.field public static final search_shortcut_month:I = 0x7f121630

.field public static final search_shortcut_today:I = 0x7f121631

.field public static final search_shortcut_week:I = 0x7f121632

.field public static final search_shortcut_year:I = 0x7f121633

.field public static final search_shortcut_yesterday:I = 0x7f121634

.field public static final search_stickers:I = 0x7f121635

.field public static final search_still_indexing_hint:I = 0x7f121636

.field public static final search_tenor:I = 0x7f121637

.field public static final search_with_google:I = 0x7f121638

.field public static final searching:I = 0x7f121639

.field public static final security:I = 0x7f12163a

.field public static final select:I = 0x7f12163b

.field public static final select_channel_or_category:I = 0x7f12163c

.field public static final select_emoji:I = 0x7f12163d

.field public static final select_from_application_a11y_label:I = 0x7f12163e

.field public static final select_picture:I = 0x7f12163f

.field public static final select_sort_mode:I = 0x7f121640

.field public static final selection_shrink_scale:I = 0x7f121641

.field public static final self_deny_permission_body:I = 0x7f121642

.field public static final self_deny_permission_title:I = 0x7f121643

.field public static final self_username_indicator:I = 0x7f121644

.field public static final self_username_indicator_hook:I = 0x7f121645

.field public static final self_xss_header:I = 0x7f121646

.field public static final self_xss_line_1:I = 0x7f121647

.field public static final self_xss_line_2:I = 0x7f121648

.field public static final self_xss_line_3:I = 0x7f121649

.field public static final self_xss_line_4:I = 0x7f12164a

.field public static final send:I = 0x7f12164b

.field public static final send_a_message:I = 0x7f12164c

.field public static final send_compressed:I = 0x7f12164d

.field public static final send_dm:I = 0x7f12164e

.field public static final send_images_label:I = 0x7f12164f

.field public static final send_message:I = 0x7f121650

.field public static final send_message_failure:I = 0x7f121651

.field public static final send_messages:I = 0x7f121652

.field public static final send_messages_description:I = 0x7f121653

.field public static final send_tts_messages:I = 0x7f121654

.field public static final send_tts_messages_description:I = 0x7f121655

.field public static final server_deafen:I = 0x7f121656

.field public static final server_deafened:I = 0x7f121657

.field public static final server_deafened_dialog_body:I = 0x7f121658

.field public static final server_deafened_dialog_title:I = 0x7f121659

.field public static final server_desciption_empty:I = 0x7f12165a

.field public static final server_emoji:I = 0x7f12165b

.field public static final server_folder_mark_as_read:I = 0x7f12165c

.field public static final server_folder_placeholder:I = 0x7f12165d

.field public static final server_folder_settings:I = 0x7f12165e

.field public static final server_insights:I = 0x7f12165f

.field public static final server_mute:I = 0x7f121660

.field public static final server_muted:I = 0x7f121661

.field public static final server_muted_dialog_body:I = 0x7f121662

.field public static final server_muted_dialog_title:I = 0x7f121663

.field public static final server_name_required:I = 0x7f121664

.field public static final server_options:I = 0x7f121665

.field public static final server_overview:I = 0x7f121666

.field public static final server_quest:I = 0x7f121667

.field public static final server_region_unavailable:I = 0x7f121668

.field public static final server_settings:I = 0x7f121669

.field public static final server_settings_updated:I = 0x7f12166a

.field public static final server_status:I = 0x7f12166b

.field public static final server_undeafen:I = 0x7f12166c

.field public static final server_unmute:I = 0x7f12166d

.field public static final server_voice_mute:I = 0x7f12166e

.field public static final server_voice_unmute:I = 0x7f12166f

.field public static final servers:I = 0x7f121670

.field public static final service_connections_disconnect:I = 0x7f121671

.field public static final set_debug_logging:I = 0x7f121672

.field public static final set_debug_logging_body:I = 0x7f121673

.field public static final set_invite_link_never_expire:I = 0x7f121674

.field public static final set_status:I = 0x7f121675

.field public static final set_up_description:I = 0x7f121676

.field public static final set_up_your_server:I = 0x7f121677

.field public static final settings:I = 0x7f121678

.field public static final settings_advanced:I = 0x7f121679

.field public static final settings_games_add_game:I = 0x7f12167a

.field public static final settings_games_add_new_game:I = 0x7f12167b

.field public static final settings_games_added_games_label:I = 0x7f12167c

.field public static final settings_games_enable_overlay_label:I = 0x7f12167d

.field public static final settings_games_hidden_library_applications_label:I = 0x7f12167e

.field public static final settings_games_last_played:I = 0x7f12167f

.field public static final settings_games_no_game_detected:I = 0x7f121680

.field public static final settings_games_no_games_header:I = 0x7f121681

.field public static final settings_games_not_playing:I = 0x7f121682

.field public static final settings_games_not_seeing_game:I = 0x7f121683

.field public static final settings_games_now_playing_state:I = 0x7f121684

.field public static final settings_games_overlay:I = 0x7f121685

.field public static final settings_games_overlay_off:I = 0x7f121686

.field public static final settings_games_overlay_on:I = 0x7f121687

.field public static final settings_games_overlay_warning:I = 0x7f121688

.field public static final settings_games_toggle_overlay:I = 0x7f121689

.field public static final settings_games_verified_icon:I = 0x7f12168a

.field public static final settings_invite_tip:I = 0x7f12168b

.field public static final settings_invite_tip_without_create:I = 0x7f12168c

.field public static final settings_notice_message:I = 0x7f12168d

.field public static final settings_permissions_delete_body:I = 0x7f12168e

.field public static final settings_permissions_delete_title:I = 0x7f12168f

.field public static final settings_roles_delete_body:I = 0x7f121690

.field public static final settings_roles_delete_title:I = 0x7f121691

.field public static final settings_sync:I = 0x7f121692

.field public static final settings_webhooks_empty_body:I = 0x7f121693

.field public static final settings_webhooks_empty_body_ios:I = 0x7f121694

.field public static final settings_webhooks_empty_title:I = 0x7f121695

.field public static final settings_webhooks_intro:I = 0x7f121696

.field public static final setup_progress:I = 0x7f121697

.field public static final setup_vanity_url:I = 0x7f121698

.field public static final several_users_typing:I = 0x7f121699

.field public static final share:I = 0x7f12169a

.field public static final share_invite_link_for_access:I = 0x7f12169b

.field public static final share_invite_mobile:I = 0x7f12169c

.field public static final share_link:I = 0x7f12169d

.field public static final share_settings_title:I = 0x7f12169e

.field public static final share_to:I = 0x7f12169f

.field public static final share_your_screen:I = 0x7f1216a0

.field public static final sharing_screen:I = 0x7f1216a1

.field public static final shortcut_recorder_button:I = 0x7f1216a2

.field public static final shortcut_recorder_button_edit:I = 0x7f1216a3

.field public static final shortcut_recorder_button_recording:I = 0x7f1216a4

.field public static final shortcut_recorder_no_bind:I = 0x7f1216a5

.field public static final show_chat:I = 0x7f1216a6

.field public static final show_current_game:I = 0x7f1216a7

.field public static final show_current_game_desc:I = 0x7f1216a8

.field public static final show_folder:I = 0x7f1216a9

.field public static final show_keyboard:I = 0x7f1216aa

.field public static final show_muted:I = 0x7f1216ab

.field public static final show_muted_channels:I = 0x7f1216ac

.field public static final show_spoiler_always:I = 0x7f1216ad

.field public static final show_spoiler_content:I = 0x7f1216ae

.field public static final show_spoiler_content_help:I = 0x7f1216af

.field public static final show_spoiler_on_click:I = 0x7f1216b0

.field public static final show_spoiler_on_servers_i_mod:I = 0x7f1216b1

.field public static final skip:I = 0x7f1216b2

.field public static final skip_all_tips:I = 0x7f1216b3

.field public static final skip_to_content:I = 0x7f1216b4

.field public static final sky_blue:I = 0x7f1216b5

.field public static final sms_confirmation_description:I = 0x7f1216b6

.field public static final sms_confirmation_title:I = 0x7f1216b7

.field public static final sort:I = 0x7f1216b8

.field public static final sorting:I = 0x7f1216b9

.field public static final sorting_channels:I = 0x7f1216ba

.field public static final sound_deafen:I = 0x7f1216bb

.field public static final sound_incoming_ring:I = 0x7f1216bc

.field public static final sound_message:I = 0x7f1216bd

.field public static final sound_mute:I = 0x7f1216be

.field public static final sound_outgoing_ring:I = 0x7f1216bf

.field public static final sound_ptt_activate:I = 0x7f1216c0

.field public static final sound_ptt_deactivate:I = 0x7f1216c1

.field public static final sound_stream_started:I = 0x7f1216c2

.field public static final sound_stream_stopped:I = 0x7f1216c3

.field public static final sound_undeafen:I = 0x7f1216c4

.field public static final sound_unmute:I = 0x7f1216c5

.field public static final sound_user_join:I = 0x7f1216c6

.field public static final sound_user_leave:I = 0x7f1216c7

.field public static final sound_user_moved:I = 0x7f1216c8

.field public static final sound_viewer_join:I = 0x7f1216c9

.field public static final sound_viewer_leave:I = 0x7f1216ca

.field public static final sound_voice_disconnected:I = 0x7f1216cb

.field public static final sounds:I = 0x7f1216cc

.field public static final source_message_deleted:I = 0x7f1216cd

.field public static final speak:I = 0x7f1216ce

.field public static final speak_message:I = 0x7f1216cf

.field public static final spectate:I = 0x7f1216d0

.field public static final spectators:I = 0x7f1216d1

.field public static final spellcheck:I = 0x7f1216d2

.field public static final spoiler:I = 0x7f1216d3

.field public static final spoiler_hidden_a11y_label:I = 0x7f1216d4

.field public static final spoiler_mark_selected:I = 0x7f1216d5

.field public static final spoiler_reveal:I = 0x7f1216d6

.field public static final spotify_connection_info_android:I = 0x7f1216d7

.field public static final spotify_listen_along_host:I = 0x7f1216d8

.field public static final spotify_listen_along_info:I = 0x7f1216d9

.field public static final spotify_listen_along_listener:I = 0x7f1216da

.field public static final spotify_listen_along_listeners:I = 0x7f1216db

.field public static final spotify_listen_along_listening_along_count:I = 0x7f1216dc

.field public static final spotify_listen_along_stop:I = 0x7f1216dd

.field public static final spotify_listen_along_subtitle_listener:I = 0x7f1216de

.field public static final spotify_listen_along_title_host:I = 0x7f1216df

.field public static final spotify_listen_along_title_listener:I = 0x7f1216e0

.field public static final spotify_premium_upgrade_body:I = 0x7f1216e1

.field public static final spotify_premium_upgrade_button:I = 0x7f1216e2

.field public static final spotify_premium_upgrade_header:I = 0x7f1216e3

.field public static final staff_badge_tooltip:I = 0x7f1216e4

.field public static final start:I = 0x7f1216e5

.field public static final start_call:I = 0x7f1216e6

.field public static final start_video_call:I = 0x7f1216e7

.field public static final start_voice_call:I = 0x7f1216e8

.field public static final starting_at:I = 0x7f1216e9

.field public static final status_bar_notification_info_overflow:I = 0x7f1216ea

.field public static final status_dnd:I = 0x7f1216eb

.field public static final status_dnd_help:I = 0x7f1216ec

.field public static final status_idle:I = 0x7f1216ed

.field public static final status_invisible:I = 0x7f1216ee

.field public static final status_invisible_helper:I = 0x7f1216ef

.field public static final status_offline:I = 0x7f1216f0

.field public static final status_online:I = 0x7f1216f1

.field public static final status_online_mobile:I = 0x7f1216f2

.field public static final status_streaming:I = 0x7f1216f3

.field public static final status_unknown:I = 0x7f1216f4

.field public static final step_number:I = 0x7f1216f5

.field public static final sticker_a11y_label:I = 0x7f1216f6

.field public static final sticker_asset_load_error:I = 0x7f1216f7

.field public static final sticker_button_label:I = 0x7f1216f8

.field public static final sticker_category_a11y_label:I = 0x7f1216f9

.field public static final sticker_category_recent:I = 0x7f1216fa

.field public static final sticker_from_pack:I = 0x7f1216fb

.field public static final sticker_notification_body:I = 0x7f1216fc

.field public static final sticker_pack_price_free:I = 0x7f1216fd

.field public static final sticker_pack_price_free_with_premium_tier_1:I = 0x7f1216fe

.field public static final sticker_pack_price_free_with_premium_tier_2:I = 0x7f1216ff

.field public static final sticker_pack_price_percent_off:I = 0x7f121700

.field public static final sticker_pack_sticker_count:I = 0x7f121701

.field public static final sticker_pack_view:I = 0x7f121702

.field public static final sticker_picker_available_timer_tooltip:I = 0x7f121703

.field public static final sticker_picker_categories_recent:I = 0x7f121704

.field public static final sticker_picker_categories_shop:I = 0x7f121705

.field public static final sticker_picker_categories_shop_tooltip:I = 0x7f121706

.field public static final sticker_picker_category_shop_tooltip_new_feature_blurb:I = 0x7f121707

.field public static final sticker_picker_category_shop_tooltip_new_feature_button:I = 0x7f121708

.field public static final sticker_picker_category_shop_tooltip_new_feature_title:I = 0x7f121709

.field public static final sticker_picker_discounted_free:I = 0x7f12170a

.field public static final sticker_picker_discounted_free_android:I = 0x7f12170b

.field public static final sticker_picker_discounted_price:I = 0x7f12170c

.field public static final sticker_picker_discounted_price_android:I = 0x7f12170d

.field public static final sticker_picker_empty_state_subtitle:I = 0x7f12170e

.field public static final sticker_picker_empty_state_subtitle_browse:I = 0x7f12170f

.field public static final sticker_picker_empty_state_subtitle_mobile:I = 0x7f121710

.field public static final sticker_picker_empty_state_title:I = 0x7f121711

.field public static final sticker_picker_empty_state_with_results_subtitle_mobile:I = 0x7f121712

.field public static final sticker_picker_owned_pack:I = 0x7f121713

.field public static final sticker_picker_pack_details:I = 0x7f121714

.field public static final sticker_picker_pack_details_animated:I = 0x7f121715

.field public static final sticker_picker_pack_details_limited_time_left:I = 0x7f121716

.field public static final sticker_picker_pack_details_limited_time_left_hook:I = 0x7f121717

.field public static final sticker_picker_pack_details_premium:I = 0x7f121718

.field public static final sticker_picker_pack_expiring_soon:I = 0x7f121719

.field public static final sticker_picker_premium_exclusive_tooltip:I = 0x7f12171a

.field public static final sticker_picker_price:I = 0x7f12171b

.field public static final sticker_picker_price_unknown:I = 0x7f12171c

.field public static final sticker_picker_price_with_premium_tier_2:I = 0x7f12171d

.field public static final sticker_picker_view_all:I = 0x7f12171e

.field public static final sticker_picker_view_all_tooltip_mobile:I = 0x7f12171f

.field public static final sticker_popout_countdown_header:I = 0x7f121720

.field public static final sticker_popout_pack_info:I = 0x7f121721

.field public static final sticker_popout_pack_info_premium:I = 0x7f121722

.field public static final sticker_popout_pack_info_unavailable:I = 0x7f121723

.field public static final sticker_premium_tier_1_upsell_alert_description:I = 0x7f121724

.field public static final sticker_premium_tier_1_upsell_alert_perks_free_pack:I = 0x7f121725

.field public static final sticker_premium_tier_1_upsell_alert_upgrade_cta:I = 0x7f121726

.field public static final sticker_premium_tier_1_upsell_perk_premium_guild_subscription_discount:I = 0x7f121727

.field public static final sticker_premium_tier_1_upsell_perk_premium_subscription:I = 0x7f121728

.field public static final sticker_premium_tier_1_upsell_perk_streaming_quality:I = 0x7f121729

.field public static final sticker_premium_tier_2_upsell_alert_description:I = 0x7f12172a

.field public static final sticker_premium_tier_2_upsell_alert_disclaimer:I = 0x7f12172b

.field public static final sticker_premium_tier_2_upsell_alert_perks_discount:I = 0x7f12172c

.field public static final sticker_premium_tier_2_upsell_alert_perks_free_pack:I = 0x7f12172d

.field public static final sticker_premium_tier_2_upsell_alert_perks_guild_subscriptions:I = 0x7f12172e

.field public static final sticker_premium_tier_2_upsell_alert_perks_misc:I = 0x7f12172f

.field public static final sticker_premium_tier_2_upsell_alert_upgrade_cta:I = 0x7f121730

.field public static final sticker_premium_tier_2_upsell_continue_to_purchase:I = 0x7f121731

.field public static final sticker_premium_tier_2_upsell_perk_discount:I = 0x7f121732

.field public static final sticker_premium_tier_2_upsell_perk_exclusivity:I = 0x7f121733

.field public static final sticker_premium_tier_2_upsell_perk_guild_subscription:I = 0x7f121734

.field public static final sticker_premium_tier_2_upsell_perk_premium_pack:I = 0x7f121735

.field public static final sticker_premium_tier_2_upsell_perk_premium_subscription:I = 0x7f121736

.field public static final sticker_premium_upsell_alert_continue_cta:I = 0x7f121737

.field public static final sticker_premium_upsell_alert_upgrade_cta:I = 0x7f121738

.field public static final sticker_premium_upsell_modal_footer_free:I = 0x7f121739

.field public static final sticker_premium_upsell_modal_footer_pay_less:I = 0x7f12173a

.field public static final sticker_purchase_modal_bundled_pack_confirm_body_content:I = 0x7f12173b

.field public static final sticker_purchase_modal_confirm_acknowledge:I = 0x7f12173c

.field public static final sticker_purchase_modal_confirm_body_content:I = 0x7f12173d

.field public static final sticker_purchase_modal_confirm_body_header:I = 0x7f12173e

.field public static final sticker_purchase_modal_header:I = 0x7f12173f

.field public static final sticker_purchase_modal_header_claim_free_pack:I = 0x7f121740

.field public static final sticker_purchase_modal_legalese_fine_print:I = 0x7f121741

.field public static final sticker_purchase_modal_purchase:I = 0x7f121742

.field public static final sticker_purchase_modal_terms:I = 0x7f121743

.field public static final stickers_always_animate:I = 0x7f121744

.field public static final stickers_animate_on_interaction:I = 0x7f121745

.field public static final stickers_animate_on_interaction_description:I = 0x7f121746

.field public static final stickers_auto_play_heading:I = 0x7f121747

.field public static final stickers_auto_play_help:I = 0x7f121748

.field public static final stickers_auto_play_help_disabled:I = 0x7f121749

.field public static final stickers_matching:I = 0x7f12174a

.field public static final stickers_matching_ios:I = 0x7f12174b

.field public static final stickers_never_animate:I = 0x7f12174c

.field public static final stickers_you_might_like:I = 0x7f12174d

.field public static final stickers_you_might_like_a11y:I = 0x7f12174e

.field public static final still_indexing:I = 0x7f12174f

.field public static final stop:I = 0x7f121750

.field public static final stop_ringing:I = 0x7f121751

.field public static final stop_ringing_username_a11y_label:I = 0x7f121752

.field public static final stop_speaking_message:I = 0x7f121753

.field public static final stop_streaming:I = 0x7f121754

.field public static final stop_watching:I = 0x7f121755

.field public static final stop_watching_user:I = 0x7f121756

.field public static final storage_permission_denied:I = 0x7f121757

.field public static final store_channel:I = 0x7f121758

.field public static final stream_actions_menu_label:I = 0x7f121759

.field public static final stream_bad_spectator:I = 0x7f12175a

.field public static final stream_bad_streamer:I = 0x7f12175b

.field public static final stream_capture_paused:I = 0x7f12175c

.field public static final stream_capture_paused_details:I = 0x7f12175d

.field public static final stream_capture_paused_details_viewer:I = 0x7f12175e

.field public static final stream_channel_description:I = 0x7f12175f

.field public static final stream_description:I = 0x7f121760

.field public static final stream_ended:I = 0x7f121761

.field public static final stream_failed_description:I = 0x7f121762

.field public static final stream_failed_title:I = 0x7f121763

.field public static final stream_fps_option:I = 0x7f121764

.field public static final stream_full_modal_body:I = 0x7f121765

.field public static final stream_full_modal_header:I = 0x7f121766

.field public static final stream_issue_modal_header:I = 0x7f121767

.field public static final stream_network_quality_error:I = 0x7f121768

.field public static final stream_no_preview:I = 0x7f121769

.field public static final stream_participants_hidden:I = 0x7f12176a

.field public static final stream_playing:I = 0x7f12176b

.field public static final stream_premium_upsell_body:I = 0x7f12176c

.field public static final stream_premium_upsell_body_no_cta:I = 0x7f12176d

.field public static final stream_premium_upsell_cta:I = 0x7f12176e

.field public static final stream_premium_upsell_header:I = 0x7f12176f

.field public static final stream_preview_loading:I = 0x7f121770

.field public static final stream_preview_paused:I = 0x7f121771

.field public static final stream_preview_paused_subtext:I = 0x7f121772

.field public static final stream_quality_unlock:I = 0x7f121773

.field public static final stream_reconnecting_error:I = 0x7f121774

.field public static final stream_reconnecting_error_subtext:I = 0x7f121775

.field public static final stream_report_a_problem:I = 0x7f121776

.field public static final stream_report_a_problem_post_stream:I = 0x7f121777

.field public static final stream_report_audio_missing:I = 0x7f121778

.field public static final stream_report_audio_poor:I = 0x7f121779

.field public static final stream_report_black:I = 0x7f12177a

.field public static final stream_report_blurry:I = 0x7f12177b

.field public static final stream_report_ended_audio_missing:I = 0x7f12177c

.field public static final stream_report_ended_audio_poor:I = 0x7f12177d

.field public static final stream_report_ended_black:I = 0x7f12177e

.field public static final stream_report_ended_blurry:I = 0x7f12177f

.field public static final stream_report_ended_lagging:I = 0x7f121780

.field public static final stream_report_ended_out_of_sync:I = 0x7f121781

.field public static final stream_report_ended_stream_stopped_unexpectedly:I = 0x7f121782

.field public static final stream_report_game_issue:I = 0x7f121783

.field public static final stream_report_label:I = 0x7f121784

.field public static final stream_report_lagging:I = 0x7f121785

.field public static final stream_report_out_of_sync:I = 0x7f121786

.field public static final stream_report_placeholder:I = 0x7f121787

.field public static final stream_report_problem:I = 0x7f121788

.field public static final stream_report_problem_body:I = 0x7f121789

.field public static final stream_report_problem_header_mobile:I = 0x7f12178a

.field public static final stream_report_problem_menu_item:I = 0x7f12178b

.field public static final stream_report_problem_mobile:I = 0x7f12178c

.field public static final stream_report_rating_body:I = 0x7f12178d

.field public static final stream_report_rating_body_streamer:I = 0x7f12178e

.field public static final stream_report_submit:I = 0x7f12178f

.field public static final stream_reported:I = 0x7f121790

.field public static final stream_reported_body:I = 0x7f121791

.field public static final stream_resolution:I = 0x7f121792

.field public static final stream_show_all_participants:I = 0x7f121793

.field public static final stream_show_non_video:I = 0x7f121794

.field public static final stream_single_person_body:I = 0x7f121795

.field public static final stream_single_person_body_alt:I = 0x7f121796

.field public static final stream_single_person_invite:I = 0x7f121797

.field public static final stream_single_person_no_invite:I = 0x7f121798

.field public static final stream_soundshare_failed:I = 0x7f121799

.field public static final stream_volume:I = 0x7f12179a

.field public static final stream_watch_multiple_tooltip:I = 0x7f12179b

.field public static final streamer_mode:I = 0x7f12179c

.field public static final streamer_mode_enabled:I = 0x7f12179d

.field public static final streamer_playing:I = 0x7f12179e

.field public static final streamer_settings_title:I = 0x7f12179f

.field public static final streaming:I = 0x7f1217a0

.field public static final streaming_a_game:I = 0x7f1217a1

.field public static final sub_enabled_servers:I = 0x7f1217a2

.field public static final submit:I = 0x7f1217a3

.field public static final subscriber_information:I = 0x7f1217a4

.field public static final subscription_payment_legalese_monthly:I = 0x7f1217a5

.field public static final subscription_payment_legalese_yearly:I = 0x7f1217a6

.field public static final subscriptions_title:I = 0x7f1217a7

.field public static final suggestions:I = 0x7f1217a8

.field public static final summary_collapsed_preference_list:I = 0x7f1217a9

.field public static final support:I = 0x7f1217aa

.field public static final suppress_all_embeds:I = 0x7f1217ab

.field public static final suppress_embed_body:I = 0x7f1217ac

.field public static final suppress_embed_confirm:I = 0x7f1217ad

.field public static final suppress_embed_tip:I = 0x7f1217ae

.field public static final suppress_embed_title:I = 0x7f1217af

.field public static final suppressed:I = 0x7f1217b0

.field public static final suppressed_afk_body:I = 0x7f1217b1

.field public static final suppressed_afk_title:I = 0x7f1217b2

.field public static final suppressed_permission_body:I = 0x7f1217b3

.field public static final sv_se:I = 0x7f1217b4

.field public static final switch_audio_output:I = 0x7f1217b5

.field public static final switch_hardware_acceleration:I = 0x7f1217b6

.field public static final switch_hardware_acceleration_body:I = 0x7f1217b7

.field public static final switch_subsystem:I = 0x7f1217b8

.field public static final switch_subsystem_body:I = 0x7f1217b9

.field public static final switch_to_compact_mode:I = 0x7f1217ba

.field public static final switch_to_cozy_mode:I = 0x7f1217bb

.field public static final switch_to_dark_theme:I = 0x7f1217bc

.field public static final switch_to_light_theme:I = 0x7f1217bd

.field public static final switch_to_push_to_talk:I = 0x7f1217be

.field public static final switch_to_voice_activity:I = 0x7f1217bf

.field public static final sync:I = 0x7f1217c0

.field public static final sync_across_clients_appearance_help:I = 0x7f1217c1

.field public static final sync_across_clients_text:I = 0x7f1217c2

.field public static final sync_across_clients_text_help:I = 0x7f1217c3

.field public static final sync_friends:I = 0x7f1217c4

.field public static final sync_now:I = 0x7f1217c5

.field public static final sync_permissions:I = 0x7f1217c6

.field public static final sync_permissions_explanation:I = 0x7f1217c7

.field public static final sync_revoked:I = 0x7f1217c8

.field public static final sync_this_account:I = 0x7f1217c9

.field public static final system_dm_activity_text:I = 0x7f1217ca

.field public static final system_dm_channel_description:I = 0x7f1217cb

.field public static final system_dm_channel_description_subtext:I = 0x7f1217cc

.field public static final system_dm_empty_message:I = 0x7f1217cd

.field public static final system_dm_tag_system:I = 0x7f1217ce

.field public static final system_dm_urgent_message_modal_body:I = 0x7f1217cf

.field public static final system_dm_urgent_message_modal_header:I = 0x7f1217d0

.field public static final system_keyboard:I = 0x7f1217d1

.field public static final system_message_application_command_used:I = 0x7f1217d2

.field public static final system_message_application_command_used_mobile:I = 0x7f1217d3

.field public static final system_message_call_missed:I = 0x7f1217d4

.field public static final system_message_call_missed_with_duration:I = 0x7f1217d5

.field public static final system_message_call_started:I = 0x7f1217d6

.field public static final system_message_call_started_with_duration:I = 0x7f1217d7

.field public static final system_message_channel_follow_add:I = 0x7f1217d8

.field public static final system_message_channel_follow_add_ios:I = 0x7f1217d9

.field public static final system_message_channel_icon_change:I = 0x7f1217da

.field public static final system_message_channel_name_change:I = 0x7f1217db

.field public static final system_message_guild_bot_join:I = 0x7f1217dc

.field public static final system_message_guild_discovery_disqualified:I = 0x7f1217dd

.field public static final system_message_guild_discovery_disqualified_mobile:I = 0x7f1217de

.field public static final system_message_guild_discovery_grace_period_final_warning:I = 0x7f1217df

.field public static final system_message_guild_discovery_grace_period_initial_warning:I = 0x7f1217e0

.field public static final system_message_guild_discovery_requalified:I = 0x7f1217e1

.field public static final system_message_guild_member_join_001:I = 0x7f1217e2

.field public static final system_message_guild_member_join_002:I = 0x7f1217e3

.field public static final system_message_guild_member_join_003:I = 0x7f1217e4

.field public static final system_message_guild_member_join_004:I = 0x7f1217e5

.field public static final system_message_guild_member_join_005:I = 0x7f1217e6

.field public static final system_message_guild_member_join_006:I = 0x7f1217e7

.field public static final system_message_guild_member_join_007:I = 0x7f1217e8

.field public static final system_message_guild_member_join_008:I = 0x7f1217e9

.field public static final system_message_guild_member_join_009:I = 0x7f1217ea

.field public static final system_message_guild_member_join_010:I = 0x7f1217eb

.field public static final system_message_guild_member_join_011:I = 0x7f1217ec

.field public static final system_message_guild_member_join_012:I = 0x7f1217ed

.field public static final system_message_guild_member_join_013:I = 0x7f1217ee

.field public static final system_message_guild_member_subscribed:I = 0x7f1217ef

.field public static final system_message_guild_member_subscribed_achieved_tier:I = 0x7f1217f0

.field public static final system_message_guild_member_subscribed_many:I = 0x7f1217f1

.field public static final system_message_guild_member_subscribed_many_achieved_tier:I = 0x7f1217f2

.field public static final system_message_guild_stream_active:I = 0x7f1217f3

.field public static final system_message_guild_stream_active_android:I = 0x7f1217f4

.field public static final system_message_guild_stream_active_mobile:I = 0x7f1217f5

.field public static final system_message_guild_stream_active_no_activity:I = 0x7f1217f6

.field public static final system_message_guild_stream_ended:I = 0x7f1217f7

.field public static final system_message_guild_stream_ended_mobile:I = 0x7f1217f8

.field public static final system_message_join_call:I = 0x7f1217f9

.field public static final system_message_pinned_message:I = 0x7f1217fa

.field public static final system_message_pinned_message_mobile:I = 0x7f1217fb

.field public static final system_message_pinned_message_no_cta:I = 0x7f1217fc

.field public static final system_message_pinned_message_no_cta_formatted:I = 0x7f1217fd

.field public static final system_message_pinned_message_no_cta_formatted_with_message:I = 0x7f1217fe

.field public static final system_message_pinned_message_with_message:I = 0x7f1217ff

.field public static final system_message_recipient_add:I = 0x7f121800

.field public static final system_message_recipient_remove:I = 0x7f121801

.field public static final system_message_recipient_remove_self:I = 0x7f121802

.field public static final system_permission_grant:I = 0x7f121803

.field public static final system_permission_request_camera:I = 0x7f121804

.field public static final system_permission_request_files:I = 0x7f121805

.field public static final tab_bar:I = 0x7f121806

.field public static final tabs_friends_accessibility_label:I = 0x7f121807

.field public static final tabs_home_accessibility_label:I = 0x7f121808

.field public static final tabs_mentions_accessibility_label:I = 0x7f121809

.field public static final tabs_search_accessibility_label:I = 0x7f12180a

.field public static final tabs_settings_accessibility_label:I = 0x7f12180b

.field public static final take_a_photo:I = 0x7f12180c

.field public static final take_survey:I = 0x7f12180d

.field public static final tan:I = 0x7f12180e

.field public static final tap_add_nickname:I = 0x7f12180f

.field public static final tar_gz:I = 0x7f121810

.field public static final teal:I = 0x7f121811

.field public static final temporary_membership_explanation:I = 0x7f121812

.field public static final terms_of_service:I = 0x7f121813

.field public static final terms_of_service_url:I = 0x7f121814

.field public static final terms_privacy:I = 0x7f121815

.field public static final terms_privacy_opt_in:I = 0x7f121816

.field public static final terms_privacy_opt_in_tooltip:I = 0x7f121817

.field public static final terracotta:I = 0x7f121818

.field public static final test_newlines_key:I = 0x7f121819

.field public static final test_video:I = 0x7f12181a

.field public static final text:I = 0x7f12181b

.field public static final text_actions_menu_label:I = 0x7f12181c

.field public static final text_and_images:I = 0x7f12181d

.field public static final text_channel:I = 0x7f12181e

.field public static final text_channels:I = 0x7f12181f

.field public static final text_channels_matching:I = 0x7f121820

.field public static final text_permissions:I = 0x7f121821

.field public static final textarea_actions_menu_label:I = 0x7f121822

.field public static final textarea_placeholder:I = 0x7f121823

.field public static final th:I = 0x7f121824

.field public static final theme:I = 0x7f121825

.field public static final theme_dark:I = 0x7f121826

.field public static final theme_holy_light_reveal:I = 0x7f121827

.field public static final theme_light:I = 0x7f121828

.field public static final theme_pure_evil_easter_hint:I = 0x7f121829

.field public static final theme_pure_evil_easter_reveal:I = 0x7f12182a

.field public static final theme_pure_evil_switch_label:I = 0x7f12182b

.field public static final theme_pure_evil_updated:I = 0x7f12182c

.field public static final theme_updated:I = 0x7f12182d

.field public static final this_server:I = 0x7f12182e

.field public static final this_server_named:I = 0x7f12182f

.field public static final three_users_typing:I = 0x7f121830

.field public static final timeout_error:I = 0x7f121831

.field public static final tip_create_first_server_body3:I = 0x7f121832

.field public static final tip_create_first_server_title3:I = 0x7f121833

.field public static final tip_create_more_servers_body3:I = 0x7f121834

.field public static final tip_create_more_servers_title3:I = 0x7f121835

.field public static final tip_direct_messages_body3:I = 0x7f121836

.field public static final tip_direct_messages_title3:I = 0x7f121837

.field public static final tip_friends_list_body3:I = 0x7f121838

.field public static final tip_friends_list_title3:I = 0x7f121839

.field public static final tip_instant_invite_body3:I = 0x7f12183a

.field public static final tip_instant_invite_title3:I = 0x7f12183b

.field public static final tip_organize_by_topic_body3:I = 0x7f12183c

.field public static final tip_organize_by_topic_title3:I = 0x7f12183d

.field public static final tip_server_settings_body3:I = 0x7f12183e

.field public static final tip_server_settings_title3:I = 0x7f12183f

.field public static final tip_voice_conversations_body3:I = 0x7f121840

.field public static final tip_voice_conversations_title3:I = 0x7f121841

.field public static final tip_whos_online_body3:I = 0x7f121842

.field public static final tip_whos_online_title3:I = 0x7f121843

.field public static final tip_writing_messages_body3:I = 0x7f121844

.field public static final tip_writing_messages_title3:I = 0x7f121845

.field public static final title:I = 0x7f121846

.field public static final title_bar_close_window:I = 0x7f121847

.field public static final title_bar_fullscreen_window:I = 0x7f121848

.field public static final title_bar_maximize_window:I = 0x7f121849

.field public static final title_bar_minimize_window:I = 0x7f12184a

.field public static final toast_add_friend:I = 0x7f12184b

.field public static final toast_feedback_sent:I = 0x7f12184c

.field public static final toast_gif_saved:I = 0x7f12184d

.field public static final toast_id_copied:I = 0x7f12184e

.field public static final toast_image_saved:I = 0x7f12184f

.field public static final toast_message_copied:I = 0x7f121850

.field public static final toast_message_id_copied:I = 0x7f121851

.field public static final toast_username_saved:I = 0x7f121852

.field public static final toast_video_saved:I = 0x7f121853

.field public static final toggle_camera:I = 0x7f121854

.field public static final toggle_deafen:I = 0x7f121855

.field public static final toggle_drawer:I = 0x7f121856

.field public static final toggle_emoji_keyboard:I = 0x7f121857

.field public static final toggle_media_keyboard:I = 0x7f121858

.field public static final toggle_microphone:I = 0x7f121859

.field public static final toggle_mute:I = 0x7f12185a

.field public static final toggle_screenshare:I = 0x7f12185b

.field public static final too_many_animated_emoji:I = 0x7f12185c

.field public static final too_many_emoji:I = 0x7f12185d

.field public static final too_many_reactions_alert_body:I = 0x7f12185e

.field public static final too_many_reactions_alert_header:I = 0x7f12185f

.field public static final too_many_user_guilds_alert_description:I = 0x7f121860

.field public static final too_many_user_guilds_alert_title:I = 0x7f121861

.field public static final too_many_user_guilds_description:I = 0x7f121862

.field public static final too_many_user_guilds_title:I = 0x7f121863

.field public static final tooltip_community_feature_disabled:I = 0x7f121864

.field public static final tooltip_discoverable_guild_feature_disabled:I = 0x7f121865

.field public static final total_members:I = 0x7f121866

.field public static final total_results:I = 0x7f121867

.field public static final tr:I = 0x7f121868

.field public static final transfer:I = 0x7f121869

.field public static final transfer_ownership:I = 0x7f12186a

.field public static final transfer_ownership_acknowledge:I = 0x7f12186b

.field public static final transfer_ownership_protected_guild:I = 0x7f12186c

.field public static final transfer_ownership_to_user:I = 0x7f12186d

.field public static final trending_arrow_down:I = 0x7f12186e

.field public static final trending_arrow_up:I = 0x7f12186f

.field public static final try_again:I = 0x7f121870

.field public static final tts_alls:I = 0x7f121871

.field public static final tts_current:I = 0x7f121872

.field public static final tts_never:I = 0x7f121873

.field public static final tutorial_close:I = 0x7f121874

.field public static final tweet_us:I = 0x7f121875

.field public static final twitter:I = 0x7f121876

.field public static final twitter_page_url:I = 0x7f121877

.field public static final two_fa:I = 0x7f121878

.field public static final two_fa_activate:I = 0x7f121879

.field public static final two_fa_app_name_authy:I = 0x7f12187a

.field public static final two_fa_app_name_google_authenticator:I = 0x7f12187b

.field public static final two_fa_auth_code:I = 0x7f12187c

.field public static final two_fa_backup_code_enter:I = 0x7f12187d

.field public static final two_fa_backup_code_enter_wrong:I = 0x7f12187e

.field public static final two_fa_backup_code_hint:I = 0x7f12187f

.field public static final two_fa_backup_code_used:I = 0x7f121880

.field public static final two_fa_backup_codes_body:I = 0x7f121881

.field public static final two_fa_backup_codes_label:I = 0x7f121882

.field public static final two_fa_backup_codes_sales_pitch:I = 0x7f121883

.field public static final two_fa_backup_codes_warning:I = 0x7f121884

.field public static final two_fa_change_account:I = 0x7f121885

.field public static final two_fa_confirm_body:I = 0x7f121886

.field public static final two_fa_confirm_confirm:I = 0x7f121887

.field public static final two_fa_confirm_title:I = 0x7f121888

.field public static final two_fa_description:I = 0x7f121889

.field public static final two_fa_disable:I = 0x7f12188a

.field public static final two_fa_discord_backup_codes:I = 0x7f12188b

.field public static final two_fa_download_app_body:I = 0x7f12188c

.field public static final two_fa_download_app_label:I = 0x7f12188d

.field public static final two_fa_download_codes:I = 0x7f12188e

.field public static final two_fa_enable:I = 0x7f12188f

.field public static final two_fa_enable_subheader:I = 0x7f121890

.field public static final two_fa_enabled:I = 0x7f121891

.field public static final two_fa_enter_sms_token_label:I = 0x7f121892

.field public static final two_fa_enter_sms_token_sending:I = 0x7f121893

.field public static final two_fa_enter_sms_token_sent:I = 0x7f121894

.field public static final two_fa_enter_token_body:I = 0x7f121895

.field public static final two_fa_enter_token_label:I = 0x7f121896

.field public static final two_fa_generate_codes:I = 0x7f121897

.field public static final two_fa_generate_codes_confirm_text:I = 0x7f121898

.field public static final two_fa_guild_mfa_warning:I = 0x7f121899

.field public static final two_fa_guild_mfa_warning_ios:I = 0x7f12189a

.field public static final two_fa_guild_mfa_warning_message:I = 0x7f12189b

.field public static final two_fa_guild_mfa_warning_message_with_spacing:I = 0x7f12189c

.field public static final two_fa_guild_mfa_warning_resolve_button:I = 0x7f12189d

.field public static final two_fa_key:I = 0x7f12189e

.field public static final two_fa_login_body:I = 0x7f12189f

.field public static final two_fa_login_footer:I = 0x7f1218a0

.field public static final two_fa_login_label:I = 0x7f1218a1

.field public static final two_fa_not_verified:I = 0x7f1218a2

.field public static final two_fa_qr_body:I = 0x7f1218a3

.field public static final two_fa_qr_label:I = 0x7f1218a4

.field public static final two_fa_remove:I = 0x7f1218a5

.field public static final two_fa_sales_pitch:I = 0x7f1218a6

.field public static final two_fa_success_body_mobile:I = 0x7f1218a7

.field public static final two_fa_success_header:I = 0x7f1218a8

.field public static final two_fa_token_required:I = 0x7f1218a9

.field public static final two_fa_use_desktop_app:I = 0x7f1218aa

.field public static final two_fa_view_backup_codes:I = 0x7f1218ab

.field public static final two_fa_view_codes:I = 0x7f1218ac

.field public static final two_users_typing:I = 0x7f1218ad

.field public static final uk:I = 0x7f1218b3

.field public static final unable_to_join_channel_full:I = 0x7f1218b4

.field public static final unable_to_join_channel_full_modal_body:I = 0x7f1218b5

.field public static final unable_to_join_channel_full_modal_header:I = 0x7f1218b6

.field public static final unable_to_open_media_chooser:I = 0x7f1218b7

.field public static final unban:I = 0x7f1218b8

.field public static final unban_user_body:I = 0x7f1218b9

.field public static final unban_user_title:I = 0x7f1218ba

.field public static final unblock:I = 0x7f1218bb

.field public static final unblock_to_jump_body:I = 0x7f1218bc

.field public static final unblock_to_jump_title:I = 0x7f1218bd

.field public static final uncategorized:I = 0x7f1218be

.field public static final unclaimed_account_body:I = 0x7f1218bf

.field public static final unclaimed_account_body_2:I = 0x7f1218c0

.field public static final unclaimed_account_title:I = 0x7f1218c1

.field public static final undeafen:I = 0x7f1218c2

.field public static final unfocus_participant:I = 0x7f1218c3

.field public static final unhandled_link_body:I = 0x7f1218c4

.field public static final unhandled_link_title:I = 0x7f1218c5

.field public static final unicode_emoji_category_shortcut_a11y_label:I = 0x7f1218c6

.field public static final unknown_region:I = 0x7f1218c7

.field public static final unknown_user:I = 0x7f1218c8

.field public static final unmute:I = 0x7f1218c9

.field public static final unmute_category:I = 0x7f1218ca

.field public static final unmute_channel:I = 0x7f1218cb

.field public static final unmute_channel_generic:I = 0x7f1218cc

.field public static final unmute_conversation:I = 0x7f1218cd

.field public static final unmute_server:I = 0x7f1218ce

.field public static final unnamed:I = 0x7f1218cf

.field public static final unpin:I = 0x7f1218d0

.field public static final unpin_confirm:I = 0x7f1218d1

.field public static final unpin_message:I = 0x7f1218d2

.field public static final unpin_message_body:I = 0x7f1218d3

.field public static final unpin_message_context_menu_hint:I = 0x7f1218d4

.field public static final unpin_message_failed_body:I = 0x7f1218d5

.field public static final unpin_message_failed_title:I = 0x7f1218d6

.field public static final unpin_message_title:I = 0x7f1218d7

.field public static final unreads_confirm_mark_all_read_description:I = 0x7f1218d8

.field public static final unreads_confirm_mark_all_read_header:I = 0x7f1218d9

.field public static final unreads_empty_state_header:I = 0x7f1218da

.field public static final unreads_empty_state_tip:I = 0x7f1218db

.field public static final unreads_empty_state_tip_mac:I = 0x7f1218dc

.field public static final unreads_mark_read:I = 0x7f1218dd

.field public static final unreads_no_notifications_divider:I = 0x7f1218de

.field public static final unreads_old_channels_divider:I = 0x7f1218df

.field public static final unreads_tab_label:I = 0x7f1218e0

.field public static final unreads_tutorial_body:I = 0x7f1218e1

.field public static final unreads_tutorial_header:I = 0x7f1218e2

.field public static final unreads_view_channel:I = 0x7f1218e3

.field public static final unsupported_browser:I = 0x7f1218e4

.field public static final unsupported_browser_body:I = 0x7f1218e5

.field public static final unsupported_browser_details:I = 0x7f1218e6

.field public static final unsupported_browser_title:I = 0x7f1218e7

.field public static final unverified_account_title:I = 0x7f1218e8

.field public static final update_available:I = 0x7f1218e9

.field public static final update_badge_header:I = 0x7f1218ea

.field public static final update_downloaded:I = 0x7f1218eb

.field public static final update_manually:I = 0x7f1218ec

.field public static final upload:I = 0x7f1218ed

.field public static final upload_a_media_file:I = 0x7f1218ee

.field public static final upload_area_always_compress:I = 0x7f1218ef

.field public static final upload_area_always_compress_desc:I = 0x7f1218f0

.field public static final upload_area_cancel_all:I = 0x7f1218f1

.field public static final upload_area_help:I = 0x7f1218f2

.field public static final upload_area_invalid_file_type_help:I = 0x7f1218f3

.field public static final upload_area_invalid_file_type_title:I = 0x7f1218f4

.field public static final upload_area_leave_a_comment:I = 0x7f1218f5

.field public static final upload_area_optional:I = 0x7f1218f6

.field public static final upload_area_title:I = 0x7f1218f7

.field public static final upload_area_title_no_confirmation:I = 0x7f1218f8

.field public static final upload_area_too_large_help:I = 0x7f1218f9

.field public static final upload_area_too_large_help_mobile:I = 0x7f1218fa

.field public static final upload_area_too_large_title:I = 0x7f1218fb

.field public static final upload_area_upload_failed_help:I = 0x7f1218fc

.field public static final upload_area_upload_failed_title:I = 0x7f1218fd

.field public static final upload_background:I = 0x7f1218fe

.field public static final upload_debug_log_failure:I = 0x7f1218ff

.field public static final upload_debug_log_failure_header:I = 0x7f121900

.field public static final upload_debug_log_failure_no_file:I = 0x7f121901

.field public static final upload_debug_log_failure_progress:I = 0x7f121902

.field public static final upload_debug_log_failure_read:I = 0x7f121903

.field public static final upload_debug_log_failure_upload:I = 0x7f121904

.field public static final upload_debug_log_success:I = 0x7f121905

.field public static final upload_debug_log_success_header:I = 0x7f121906

.field public static final upload_debug_logs:I = 0x7f121907

.field public static final upload_emoji:I = 0x7f121908

.field public static final upload_emoji_subtitle:I = 0x7f121909

.field public static final upload_emoji_title:I = 0x7f12190a

.field public static final upload_image:I = 0x7f12190b

.field public static final upload_image_body:I = 0x7f12190c

.field public static final upload_open_file_failed:I = 0x7f12190d

.field public static final upload_queued:I = 0x7f12190e

.field public static final upload_uploads_too_large_help:I = 0x7f12190f

.field public static final upload_uploads_too_large_title:I = 0x7f121910

.field public static final uploaded_by:I = 0x7f121911

.field public static final uploading_files:I = 0x7f121912

.field public static final uploading_files_failed:I = 0x7f121913

.field public static final usage_access:I = 0x7f121914

.field public static final usage_statistics_disable_modal_body:I = 0x7f121915

.field public static final usage_statistics_disable_modal_cancel:I = 0x7f121916

.field public static final usage_statistics_disable_modal_confirm:I = 0x7f121917

.field public static final usage_statistics_disable_modal_title:I = 0x7f121918

.field public static final use_external_emojis:I = 0x7f121919

.field public static final use_external_emojis_description:I = 0x7f12191a

.field public static final use_rich_chat_box_description:I = 0x7f12191b

.field public static final use_speaker:I = 0x7f12191c

.field public static final use_vad:I = 0x7f12191d

.field public static final use_vad_description:I = 0x7f12191e

.field public static final user_actions_menu_label:I = 0x7f12191f

.field public static final user_activity_accept_invite:I = 0x7f121920

.field public static final user_activity_action_ask_to_join:I = 0x7f121921

.field public static final user_activity_action_ask_to_join_user:I = 0x7f121922

.field public static final user_activity_action_download_app:I = 0x7f121923

.field public static final user_activity_action_failed_to_launch:I = 0x7f121924

.field public static final user_activity_action_invite_to_join:I = 0x7f121925

.field public static final user_activity_action_invite_to_listen_along:I = 0x7f121926

.field public static final user_activity_action_notify_me:I = 0x7f121927

.field public static final user_activity_already_playing:I = 0x7f121928

.field public static final user_activity_already_syncing:I = 0x7f121929

.field public static final user_activity_cannot_join_self:I = 0x7f12192a

.field public static final user_activity_cannot_play_self:I = 0x7f12192b

.field public static final user_activity_cannot_sync_self:I = 0x7f12192c

.field public static final user_activity_chat_invite_education:I = 0x7f12192d

.field public static final user_activity_connect_platform:I = 0x7f12192e

.field public static final user_activity_header_competing:I = 0x7f12192f

.field public static final user_activity_header_listening:I = 0x7f121930

.field public static final user_activity_header_live_on_platform:I = 0x7f121931

.field public static final user_activity_header_playing:I = 0x7f121932

.field public static final user_activity_header_playing_on_platform:I = 0x7f121933

.field public static final user_activity_header_streaming_to_dm:I = 0x7f121934

.field public static final user_activity_header_streaming_to_guild:I = 0x7f121935

.field public static final user_activity_header_watching:I = 0x7f121936

.field public static final user_activity_invite_request_expired:I = 0x7f121937

.field public static final user_activity_invite_request_received:I = 0x7f121938

.field public static final user_activity_invite_request_requested:I = 0x7f121939

.field public static final user_activity_invite_request_sent:I = 0x7f12193a

.field public static final user_activity_invite_request_waiting:I = 0x7f12193b

.field public static final user_activity_invite_to_join:I = 0x7f12193c

.field public static final user_activity_joining:I = 0x7f12193d

.field public static final user_activity_listen_along:I = 0x7f12193e

.field public static final user_activity_listen_along_description:I = 0x7f12193f

.field public static final user_activity_listening_album:I = 0x7f121940

.field public static final user_activity_listening_artists:I = 0x7f121941

.field public static final user_activity_never_mind:I = 0x7f121942

.field public static final user_activity_not_detected:I = 0x7f121943

.field public static final user_activity_play_on_platform:I = 0x7f121944

.field public static final user_activity_respond_nope:I = 0x7f121945

.field public static final user_activity_respond_yeah:I = 0x7f121946

.field public static final user_activity_state_size:I = 0x7f121947

.field public static final user_activity_timestamp_end:I = 0x7f121948

.field public static final user_activity_timestamp_end_simple:I = 0x7f121949

.field public static final user_activity_timestamp_start:I = 0x7f12194a

.field public static final user_activity_timestamp_start_simple:I = 0x7f12194b

.field public static final user_activity_user_join:I = 0x7f12194c

.field public static final user_activity_user_join_hint:I = 0x7f12194d

.field public static final user_activity_user_playing_for_days:I = 0x7f12194e

.field public static final user_activity_user_playing_for_hours:I = 0x7f12194f

.field public static final user_activity_user_playing_for_minutes:I = 0x7f121950

.field public static final user_activity_watch_along:I = 0x7f121951

.field public static final user_dm_settings:I = 0x7f121952

.field public static final user_dm_settings_help:I = 0x7f121953

.field public static final user_dm_settings_question:I = 0x7f121954

.field public static final user_explicit_content_filter:I = 0x7f121955

.field public static final user_explicit_content_filter_disabled:I = 0x7f121956

.field public static final user_explicit_content_filter_disabled_help:I = 0x7f121957

.field public static final user_explicit_content_filter_friends_and_non_friends:I = 0x7f121958

.field public static final user_explicit_content_filter_friends_and_non_friends_help:I = 0x7f121959

.field public static final user_explicit_content_filter_help:I = 0x7f12195a

.field public static final user_explicit_content_filter_non_friends:I = 0x7f12195b

.field public static final user_explicit_content_filter_non_friends_help:I = 0x7f12195c

.field public static final user_has_been_blocked:I = 0x7f12195d

.field public static final user_has_been_unblocked:I = 0x7f12195e

.field public static final user_info:I = 0x7f12195f

.field public static final user_management:I = 0x7f121960

.field public static final user_popout_message:I = 0x7f121961

.field public static final user_popout_wumpus_tooltip:I = 0x7f121962

.field public static final user_profile_add_friend:I = 0x7f121963

.field public static final user_profile_audio:I = 0x7f121964

.field public static final user_profile_failure_to_open_message:I = 0x7f121965

.field public static final user_profile_guild_name_content_description:I = 0x7f121966

.field public static final user_profile_incoming_friend_request_dialog_body:I = 0x7f121967

.field public static final user_profile_message:I = 0x7f121968

.field public static final user_profile_mutual_friends:I = 0x7f121969

.field public static final user_profile_mutual_friends_placeholder:I = 0x7f12196a

.field public static final user_profile_mutual_guilds:I = 0x7f12196b

.field public static final user_profile_mutual_guilds_placeholder:I = 0x7f12196c

.field public static final user_profile_pending:I = 0x7f12196d

.field public static final user_profile_settings_setstatus:I = 0x7f12196e

.field public static final user_profile_video:I = 0x7f12196f

.field public static final user_profile_volume:I = 0x7f121970

.field public static final user_settings:I = 0x7f121971

.field public static final user_settings_account_change_email_prompt:I = 0x7f121972

.field public static final user_settings_account_change_email_prompt_desktop:I = 0x7f121973

.field public static final user_settings_account_change_email_title:I = 0x7f121974

.field public static final user_settings_account_change_email_title_desktop:I = 0x7f121975

.field public static final user_settings_account_change_password_prompt_desktop:I = 0x7f121976

.field public static final user_settings_account_change_password_title_desktop:I = 0x7f121977

.field public static final user_settings_account_change_username_prompt_desktop:I = 0x7f121978

.field public static final user_settings_account_change_username_title:I = 0x7f121979

.field public static final user_settings_account_change_username_title_desktop:I = 0x7f12197a

.field public static final user_settings_account_password_and_authentication:I = 0x7f12197b

.field public static final user_settings_account_removal_description:I = 0x7f12197c

.field public static final user_settings_account_removal_description_unclaimed:I = 0x7f12197d

.field public static final user_settings_account_removal_section:I = 0x7f12197e

.field public static final user_settings_account_remove_phone_number_button:I = 0x7f12197f

.field public static final user_settings_account_remove_phone_number_title:I = 0x7f121980

.field public static final user_settings_account_remove_phone_number_warning_body:I = 0x7f121981

.field public static final user_settings_account_remove_phone_number_warning_title:I = 0x7f121982

.field public static final user_settings_account_remove_phone_sms_backup_warning:I = 0x7f121983

.field public static final user_settings_account_verify_password_subtitle:I = 0x7f121984

.field public static final user_settings_account_verify_password_title:I = 0x7f121985

.field public static final user_settings_account_verify_password_title_in_title_case:I = 0x7f121986

.field public static final user_settings_actions_menu_label:I = 0x7f121987

.field public static final user_settings_add_email_claim:I = 0x7f121988

.field public static final user_settings_appearance_colorblind_mode_description:I = 0x7f121989

.field public static final user_settings_appearance_colorblind_mode_title:I = 0x7f12198a

.field public static final user_settings_appearance_colors:I = 0x7f12198b

.field public static final user_settings_appearance_preview_message_1:I = 0x7f12198c

.field public static final user_settings_appearance_preview_message_2_dark:I = 0x7f12198d

.field public static final user_settings_appearance_preview_message_2_light:I = 0x7f12198e

.field public static final user_settings_appearance_preview_message_3:I = 0x7f12198f

.field public static final user_settings_appearance_preview_message_4:I = 0x7f121990

.field public static final user_settings_appearance_preview_message_5:I = 0x7f121991

.field public static final user_settings_appearance_zoom_tip:I = 0x7f121992

.field public static final user_settings_available_codes:I = 0x7f121993

.field public static final user_settings_blocked_users:I = 0x7f121994

.field public static final user_settings_blocked_users_empty:I = 0x7f121995

.field public static final user_settings_blocked_users_header:I = 0x7f121996

.field public static final user_settings_blocked_users_unblockbutton:I = 0x7f121997

.field public static final user_settings_change_avatar:I = 0x7f121998

.field public static final user_settings_close_button:I = 0x7f121999

.field public static final user_settings_confirm_logout:I = 0x7f12199a

.field public static final user_settings_delete_avatar:I = 0x7f12199b

.field public static final user_settings_disable_advanced_voice_activity:I = 0x7f12199c

.field public static final user_settings_disable_noise_suppression:I = 0x7f12199d

.field public static final user_settings_edit_account:I = 0x7f12199e

.field public static final user_settings_edit_account_password_label:I = 0x7f12199f

.field public static final user_settings_edit_account_tag:I = 0x7f1219a0

.field public static final user_settings_enter_password_view_codes:I = 0x7f1219a1

.field public static final user_settings_game_activity:I = 0x7f1219a2

.field public static final user_settings_games_install_location:I = 0x7f1219a3

.field public static final user_settings_games_install_location_add:I = 0x7f1219a4

.field public static final user_settings_games_install_location_make_default:I = 0x7f1219a5

.field public static final user_settings_games_install_location_name:I = 0x7f1219a6

.field public static final user_settings_games_install_location_remove:I = 0x7f1219a7

.field public static final user_settings_games_install_location_space:I = 0x7f1219a8

.field public static final user_settings_games_install_locations:I = 0x7f1219a9

.field public static final user_settings_games_remove_location_body:I = 0x7f1219aa

.field public static final user_settings_games_shortcuts_desktop:I = 0x7f1219ab

.field public static final user_settings_games_shortcuts_desktop_note:I = 0x7f1219ac

.field public static final user_settings_games_shortcuts_start_menu:I = 0x7f1219ad

.field public static final user_settings_games_shortcuts_start_menu_note:I = 0x7f1219ae

.field public static final user_settings_hypesquad:I = 0x7f1219af

.field public static final user_settings_keybinds_action:I = 0x7f1219b0

.field public static final user_settings_keybinds_chat_section_title:I = 0x7f1219b1

.field public static final user_settings_keybinds_keybind:I = 0x7f1219b2

.field public static final user_settings_keybinds_miscellaneous_section_title:I = 0x7f1219b3

.field public static final user_settings_keybinds_navigation_section_title:I = 0x7f1219b4

.field public static final user_settings_keybinds_show_list_title:I = 0x7f1219b5

.field public static final user_settings_keybinds_voice_and_video_section_title:I = 0x7f1219b6

.field public static final user_settings_label_current_password:I = 0x7f1219b7

.field public static final user_settings_label_discriminator:I = 0x7f1219b8

.field public static final user_settings_label_email:I = 0x7f1219b9

.field public static final user_settings_label_new_password:I = 0x7f1219ba

.field public static final user_settings_label_phone_number:I = 0x7f1219bb

.field public static final user_settings_label_username:I = 0x7f1219bc

.field public static final user_settings_linux_settings:I = 0x7f1219bd

.field public static final user_settings_mfa_enable_code_body:I = 0x7f1219be

.field public static final user_settings_mfa_enable_code_label:I = 0x7f1219bf

.field public static final user_settings_mfa_enabled:I = 0x7f1219c0

.field public static final user_settings_mfa_removed:I = 0x7f1219c1

.field public static final user_settings_minimize_to_tray_body:I = 0x7f1219c2

.field public static final user_settings_minimize_to_tray_label:I = 0x7f1219c3

.field public static final user_settings_my_account:I = 0x7f1219c4

.field public static final user_settings_no_email_placeholder:I = 0x7f1219c5

.field public static final user_settings_no_phone_placeholder:I = 0x7f1219c6

.field public static final user_settings_noise_cancellation:I = 0x7f1219c7

.field public static final user_settings_noise_cancellation_description:I = 0x7f1219c8

.field public static final user_settings_noise_cancellation_model:I = 0x7f1219c9

.field public static final user_settings_notifications_preview_sound:I = 0x7f1219ca

.field public static final user_settings_notifications_show_badge_body:I = 0x7f1219cb

.field public static final user_settings_notifications_show_badge_label:I = 0x7f1219cc

.field public static final user_settings_notifications_show_flash_body:I = 0x7f1219cd

.field public static final user_settings_notifications_show_flash_label:I = 0x7f1219ce

.field public static final user_settings_open_on_startup_body:I = 0x7f1219cf

.field public static final user_settings_open_on_startup_label:I = 0x7f1219d0

.field public static final user_settings_privacy_terms:I = 0x7f1219d1

.field public static final user_settings_restart_app_mobile:I = 0x7f1219d2

.field public static final user_settings_save:I = 0x7f1219d3

.field public static final user_settings_scan_qr_code:I = 0x7f1219d4

.field public static final user_settings_show_library:I = 0x7f1219d5

.field public static final user_settings_show_library_note:I = 0x7f1219d6

.field public static final user_settings_start_minimized_body:I = 0x7f1219d7

.field public static final user_settings_start_minimized_label:I = 0x7f1219d8

.field public static final user_settings_startup_behavior:I = 0x7f1219d9

.field public static final user_settings_streamer_notice_body:I = 0x7f1219da

.field public static final user_settings_streamer_notice_title:I = 0x7f1219db

.field public static final user_settings_unverified_account_body:I = 0x7f1219dc

.field public static final user_settings_upload_avatar:I = 0x7f1219dd

.field public static final user_settings_used_backup_codes:I = 0x7f1219de

.field public static final user_settings_voice_add_multiple:I = 0x7f1219df

.field public static final user_settings_voice_codec_description:I = 0x7f1219e0

.field public static final user_settings_voice_codec_title:I = 0x7f1219e1

.field public static final user_settings_voice_experimental_soundshare_label:I = 0x7f1219e2

.field public static final user_settings_voice_hardware_h264:I = 0x7f1219e3

.field public static final user_settings_voice_mic_test_button_active:I = 0x7f1219e4

.field public static final user_settings_voice_mic_test_button_inactive:I = 0x7f1219e5

.field public static final user_settings_voice_mic_test_description:I = 0x7f1219e6

.field public static final user_settings_voice_mic_test_title:I = 0x7f1219e7

.field public static final user_settings_voice_mic_test_voice_caption:I = 0x7f1219e8

.field public static final user_settings_voice_mic_test_voice_no_input_notice:I = 0x7f1219e9

.field public static final user_settings_voice_open_h264:I = 0x7f1219ea

.field public static final user_settings_voice_video_codec_title:I = 0x7f1219eb

.field public static final user_settings_voice_video_hook_label:I = 0x7f1219ec

.field public static final user_settings_windows_settings:I = 0x7f1219ed

.field public static final user_settings_with_build_override:I = 0x7f1219ee

.field public static final user_volume:I = 0x7f1219ef

.field public static final username:I = 0x7f1219f0

.field public static final username_and_tag:I = 0x7f1219f1

.field public static final username_live:I = 0x7f1219f2

.field public static final username_required:I = 0x7f1219f3

.field public static final users:I = 0x7f1219f4

.field public static final v7_preference_off:I = 0x7f1219f5

.field public static final v7_preference_on:I = 0x7f1219f6

.field public static final vad_permission_body:I = 0x7f1219f7

.field public static final vad_permission_small:I = 0x7f1219f8

.field public static final vad_permission_title:I = 0x7f1219f9

.field public static final vanity_url:I = 0x7f1219fa

.field public static final vanity_url_help:I = 0x7f1219fb

.field public static final vanity_url_help_conflict:I = 0x7f1219fc

.field public static final vanity_url_help_extended:I = 0x7f1219fd

.field public static final vanity_url_help_extended_link:I = 0x7f1219fe

.field public static final vanity_url_hint_mobile:I = 0x7f1219ff

.field public static final vanity_url_uses:I = 0x7f121a00

.field public static final verfication_expired:I = 0x7f121a01

.field public static final verification_body:I = 0x7f121a02

.field public static final verification_body_alt:I = 0x7f121a03

.field public static final verification_email_body:I = 0x7f121a04

.field public static final verification_email_error_body:I = 0x7f121a05

.field public static final verification_email_error_title:I = 0x7f121a06

.field public static final verification_email_title:I = 0x7f121a07

.field public static final verification_footer:I = 0x7f121a08

.field public static final verification_footer_logout:I = 0x7f121a09

.field public static final verification_footer_support:I = 0x7f121a0a

.field public static final verification_level_high:I = 0x7f121a0b

.field public static final verification_level_high_criteria:I = 0x7f121a0c

.field public static final verification_level_low:I = 0x7f121a0d

.field public static final verification_level_low_criteria:I = 0x7f121a0e

.field public static final verification_level_medium:I = 0x7f121a0f

.field public static final verification_level_medium_criteria:I = 0x7f121a10

.field public static final verification_level_none:I = 0x7f121a11

.field public static final verification_level_none_criteria:I = 0x7f121a12

.field public static final verification_level_very_high:I = 0x7f121a13

.field public static final verification_level_very_high_criteria:I = 0x7f121a14

.field public static final verification_open_discord:I = 0x7f121a15

.field public static final verification_phone_description:I = 0x7f121a16

.field public static final verification_phone_title:I = 0x7f121a17

.field public static final verification_title:I = 0x7f121a18

.field public static final verification_verified:I = 0x7f121a19

.field public static final verification_verifying:I = 0x7f121a1a

.field public static final verified_bot_tooltip:I = 0x7f121a1b

.field public static final verified_developer_badge_tooltip:I = 0x7f121a1c

.field public static final verify:I = 0x7f121a1d

.field public static final verify_account:I = 0x7f121a1e

.field public static final verify_by:I = 0x7f121a1f

.field public static final verify_by_email:I = 0x7f121a20

.field public static final verify_by_email_formatted:I = 0x7f121a21

.field public static final verify_by_phone_formatted:I = 0x7f121a22

.field public static final verify_by_recaptcha:I = 0x7f121a23

.field public static final verify_by_recaptcha_description:I = 0x7f121a24

.field public static final verify_email_body:I = 0x7f121a25

.field public static final verify_email_body_resent:I = 0x7f121a26

.field public static final verify_phone:I = 0x7f121a27

.field public static final verifying:I = 0x7f121a28

.field public static final very_out_of_date_description:I = 0x7f121a29

.field public static final vi:I = 0x7f121a2a

.field public static final video:I = 0x7f121a2b

.field public static final video_call_auto_select:I = 0x7f121a2c

.field public static final video_call_hide_members:I = 0x7f121a2d

.field public static final video_call_return_to_grid:I = 0x7f121a2e

.field public static final video_call_return_to_list:I = 0x7f121a2f

.field public static final video_call_show_members:I = 0x7f121a30

.field public static final video_capacity_modal_body:I = 0x7f121a31

.field public static final video_capacity_modal_header:I = 0x7f121a32

.field public static final video_playback_mute_accessibility_label:I = 0x7f121a33

.field public static final video_playback_unmute_accessibility_label:I = 0x7f121a34

.field public static final video_poor_connection_body:I = 0x7f121a35

.field public static final video_poor_connection_title:I = 0x7f121a36

.field public static final video_settings:I = 0x7f121a37

.field public static final video_unavailable:I = 0x7f121a38

.field public static final video_unsupported_browser_body:I = 0x7f121a39

.field public static final video_unsupported_browser_title:I = 0x7f121a3a

.field public static final view_as_role:I = 0x7f121a3b

.field public static final view_as_role_description:I = 0x7f121a3c

.field public static final view_as_roles_hidden_vc_warning:I = 0x7f121a3d

.field public static final view_as_roles_mentions_warning:I = 0x7f121a3e

.field public static final view_as_roles_upsell_body:I = 0x7f121a3f

.field public static final view_as_roles_upsell_title:I = 0x7f121a40

.field public static final view_as_roles_voice_warning:I = 0x7f121a41

.field public static final view_audit_log:I = 0x7f121a42

.field public static final view_audit_log_description:I = 0x7f121a43

.field public static final view_channel:I = 0x7f121a44

.field public static final view_embed:I = 0x7f121a45

.field public static final view_guild_analytics:I = 0x7f121a46

.field public static final view_guild_analytics_description:I = 0x7f121a47

.field public static final view_profile:I = 0x7f121a48

.field public static final view_spectators:I = 0x7f121a49

.field public static final view_surrounding_messages:I = 0x7f121a4a

.field public static final viewing_as_roles:I = 0x7f121a4b

.field public static final viewing_as_roles_back:I = 0x7f121a4c

.field public static final viewing_as_roles_select:I = 0x7f121a4d

.field public static final visitors_info:I = 0x7f121a4e

.field public static final voice:I = 0x7f121a4f

.field public static final voice_and_video:I = 0x7f121a50

.field public static final voice_call_member_list_title:I = 0x7f121a51

.field public static final voice_channel:I = 0x7f121a52

.field public static final voice_channel_deafened:I = 0x7f121a53

.field public static final voice_channel_empty:I = 0x7f121a54

.field public static final voice_channel_hide_names:I = 0x7f121a55

.field public static final voice_channel_muted:I = 0x7f121a56

.field public static final voice_channel_show_names:I = 0x7f121a57

.field public static final voice_channel_subtitle:I = 0x7f121a58

.field public static final voice_channel_title:I = 0x7f121a59

.field public static final voice_channel_undeafened:I = 0x7f121a5a

.field public static final voice_channel_unmuted:I = 0x7f121a5b

.field public static final voice_channels:I = 0x7f121a5c

.field public static final voice_controls_sheet_tooltip_swipe_up:I = 0x7f121a5d

.field public static final voice_panel_introduction_close:I = 0x7f121a5e

.field public static final voice_panel_introduction_header:I = 0x7f121a5f

.field public static final voice_panel_introduction_text:I = 0x7f121a60

.field public static final voice_permissions:I = 0x7f121a61

.field public static final voice_settings:I = 0x7f121a62

.field public static final voice_status_connecting:I = 0x7f121a63

.field public static final voice_status_not_connected:I = 0x7f121a64

.field public static final voice_status_not_connected_mobile:I = 0x7f121a65

.field public static final voice_status_ringing:I = 0x7f121a66

.field public static final voice_unavailable:I = 0x7f121a67

.field public static final watch:I = 0x7f121a68

.field public static final watch_media_survey_button:I = 0x7f121a69

.field public static final watch_media_survey_prompt:I = 0x7f121a6a

.field public static final watch_stream:I = 0x7f121a6b

.field public static final watch_stream_in_app:I = 0x7f121a6c

.field public static final watch_stream_streaming:I = 0x7f121a6d

.field public static final watch_stream_tip:I = 0x7f121a6e

.field public static final watch_stream_watching:I = 0x7f121a6f

.field public static final watch_user_stream:I = 0x7f121a70

.field public static final watching:I = 0x7f121a71

.field public static final web_browser:I = 0x7f121a72

.field public static final web_browser_chrome:I = 0x7f121a73

.field public static final web_browser_in_app:I = 0x7f121a74

.field public static final web_browser_safari:I = 0x7f121a75

.field public static final webhook_cancel:I = 0x7f121a76

.field public static final webhook_create:I = 0x7f121a77

.field public static final webhook_created_on:I = 0x7f121a78

.field public static final webhook_delete:I = 0x7f121a79

.field public static final webhook_delete_body:I = 0x7f121a7a

.field public static final webhook_delete_title:I = 0x7f121a7b

.field public static final webhook_error_creating_webhook:I = 0x7f121a7c

.field public static final webhook_error_deleting_webhook:I = 0x7f121a7d

.field public static final webhook_error_internal_server_error:I = 0x7f121a7e

.field public static final webhook_error_max_webhooks_reached:I = 0x7f121a7f

.field public static final webhook_form_name:I = 0x7f121a80

.field public static final webhook_form_url:I = 0x7f121a81

.field public static final webhook_form_webhook_url_help:I = 0x7f121a82

.field public static final webhook_modal_icon_description:I = 0x7f121a83

.field public static final webhook_modal_icon_title:I = 0x7f121a84

.field public static final webhook_modal_title:I = 0x7f121a85

.field public static final webhooks:I = 0x7f121a86

.field public static final weekly_communicators:I = 0x7f121a87

.field public static final weekly_new_member_retention:I = 0x7f121a88

.field public static final weekly_new_members:I = 0x7f121a89

.field public static final weekly_visitors:I = 0x7f121a8a

.field public static final welcome_channel_delete_confirm_body:I = 0x7f121a8b

.field public static final welcome_channel_delete_confirm_body_generic:I = 0x7f121a8c

.field public static final welcome_channel_delete_confirm_title:I = 0x7f121a8d

.field public static final welcome_channel_emoji_picker_tooltip:I = 0x7f121a8e

.field public static final welcome_cta_create_channel:I = 0x7f121a8f

.field public static final welcome_cta_download_title:I = 0x7f121a90

.field public static final welcome_cta_invite_title:I = 0x7f121a91

.field public static final welcome_cta_message_title:I = 0x7f121a92

.field public static final welcome_cta_onboarding_video_title:I = 0x7f121a93

.field public static final welcome_cta_onboarding_video_title_new_user:I = 0x7f121a94

.field public static final welcome_cta_personalize_title:I = 0x7f121a95

.field public static final welcome_cta_personalize_title_mobile:I = 0x7f121a96

.field public static final welcome_cta_subtitle_action_with_guide:I = 0x7f121a97

.field public static final welcome_cta_subtitle_existing_server:I = 0x7f121a98

.field public static final welcome_cta_subtitle_member:I = 0x7f121a99

.field public static final welcome_cta_subtitle_owner:I = 0x7f121a9a

.field public static final welcome_cta_title:I = 0x7f121a9b

.field public static final welcome_cta_title_mobile:I = 0x7f121a9c

.field public static final welcome_message_desktop_apps:I = 0x7f121a9d

.field public static final welcome_message_edit_channel:I = 0x7f121a9e

.field public static final welcome_message_instant_invite:I = 0x7f121a9f

.field public static final welcome_message_mobile_apps:I = 0x7f121aa0

.field public static final welcome_message_mobile_explore_server:I = 0x7f121aa1

.field public static final welcome_message_mobile_explore_server_desc:I = 0x7f121aa2

.field public static final welcome_message_mobile_instant_invite:I = 0x7f121aa3

.field public static final welcome_message_mobile_instant_invite_desc:I = 0x7f121aa4

.field public static final welcome_message_mobile_owner_subtitle:I = 0x7f121aa5

.field public static final welcome_message_mobile_owner_title:I = 0x7f121aa6

.field public static final welcome_message_mobile_upload_icon:I = 0x7f121aa7

.field public static final welcome_message_mobile_upload_icon_desc:I = 0x7f121aa8

.field public static final welcome_message_owner_title:I = 0x7f121aa9

.field public static final welcome_message_setup_server:I = 0x7f121aaa

.field public static final welcome_message_subtitle_channel:I = 0x7f121aab

.field public static final welcome_message_support:I = 0x7f121aac

.field public static final welcome_message_title:I = 0x7f121aad

.field public static final welcome_message_title_channel:I = 0x7f121aae

.field public static final welcome_message_tutorial:I = 0x7f121aaf

.field public static final welcome_screen_choice_header:I = 0x7f121ab0

.field public static final welcome_screen_skip:I = 0x7f121ab1

.field public static final welcome_screen_title:I = 0x7f121ab2

.field public static final whats_new:I = 0x7f121ab3

.field public static final whats_new_date:I = 0x7f121ab4

.field public static final whitelist_failed:I = 0x7f121ab5

.field public static final whitelisted:I = 0x7f121ab6

.field public static final whitelisting:I = 0x7f121ab7

.field public static final widget:I = 0x7f121ab8

.field public static final windows:I = 0x7f121ab9

.field public static final working:I = 0x7f121aba

.field public static final wumpus:I = 0x7f121abb

.field public static final xbox_authorization_title:I = 0x7f121abc

.field public static final xbox_game_pass_card_body_claimed:I = 0x7f121abd

.field public static final xbox_game_pass_card_body_unclaimed_expanded:I = 0x7f121abe

.field public static final xbox_game_pass_card_body_unclaimed_unexpanded:I = 0x7f121abf

.field public static final xbox_game_pass_modal_code_description:I = 0x7f121ac0

.field public static final xbox_game_pass_modal_code_description_failed:I = 0x7f121ac1

.field public static final xbox_game_pass_modal_code_header:I = 0x7f121ac2

.field public static final xbox_game_pass_modal_code_header_failed:I = 0x7f121ac3

.field public static final xbox_game_pass_modal_header_description_success:I = 0x7f121ac4

.field public static final xbox_game_pass_modal_header_success:I = 0x7f121ac5

.field public static final xbox_game_pass_promotion_account_credit_body:I = 0x7f121ac6

.field public static final xbox_game_pass_promotion_account_credit_button:I = 0x7f121ac7

.field public static final xbox_game_pass_promotion_account_credit_confirm_body:I = 0x7f121ac8

.field public static final xbox_game_pass_promotion_account_credit_confirm_title:I = 0x7f121ac9

.field public static final xbox_game_pass_promotion_account_credit_title:I = 0x7f121aca

.field public static final xbox_game_pass_promotion_banner_description:I = 0x7f121acb

.field public static final xbox_game_pass_promotion_banner_description_expanded:I = 0x7f121acc

.field public static final xbox_game_pass_promotion_banner_header:I = 0x7f121acd

.field public static final xbox_game_pass_promotion_card_header:I = 0x7f121ace

.field public static final xbox_game_pass_promotion_existing_subscriber_body:I = 0x7f121acf

.field public static final xbox_game_pass_promotion_existing_subscriber_title:I = 0x7f121ad0

.field public static final xbox_game_pass_promotion_legalese:I = 0x7f121ad1

.field public static final xbox_game_pass_promotion_redeem_body:I = 0x7f121ad2

.field public static final xbox_link:I = 0x7f121ad3

.field public static final xbox_pin_step1:I = 0x7f121ad4

.field public static final xbox_pin_step2:I = 0x7f121ad5

.field public static final xbox_pin_step3:I = 0x7f121ad6

.field public static final yearly:I = 0x7f121ad7

.field public static final yellow:I = 0x7f121ad8

.field public static final yes_text:I = 0x7f121ad9

.field public static final your_pin_expires:I = 0x7f121ada

.field public static final your_pin_is_expired:I = 0x7f121adb

.field public static final youre_viewing_older_messages:I = 0x7f121adc

.field public static final zh_cn:I = 0x7f121add

.field public static final zh_tw:I = 0x7f121ade


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
