.class public Lcom/facebook/imagepipeline/request/ImageRequest;
.super Ljava/lang/Object;
.source "ImageRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/imagepipeline/request/ImageRequest$c;,
        Lcom/facebook/imagepipeline/request/ImageRequest$b;
    }
.end annotation


# static fields
.field public static final r:Lf/g/d/d/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/d/d/d<",
            "Lcom/facebook/imagepipeline/request/ImageRequest;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

.field public final b:Landroid/net/Uri;

.field public final c:I

.field public d:Ljava/io/File;

.field public final e:Z

.field public final f:Z

.field public final g:Lf/g/j/d/b;

.field public final h:Lf/g/j/d/e;

.field public final i:Lf/g/j/d/f;

.field public final j:Lf/g/j/d/a;

.field public final k:Lf/g/j/d/d;

.field public final l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

.field public final m:Z

.field public final n:Z

.field public final o:Ljava/lang/Boolean;

.field public final p:Lf/g/j/r/b;

.field public final q:Lf/g/j/l/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/facebook/imagepipeline/request/ImageRequest$a;

    invoke-direct {v0}, Lcom/facebook/imagepipeline/request/ImageRequest$a;-><init>()V

    sput-object v0, Lcom/facebook/imagepipeline/request/ImageRequest;->r:Lf/g/d/d/d;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/imagepipeline/request/ImageRequestBuilder;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->f:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-nez v0, :cond_0

    goto/16 :goto_5

    :cond_0
    invoke-static {v0}, Lf/g/d/l/b;->e(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_1
    invoke-static {v0}, Lf/g/d/l/b;->d(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lf/g/d/f/a;->a:Ljava/util/Map;

    const/16 v4, 0x2e

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    const/4 v5, 0x0

    if-ltz v4, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v3

    if-ne v4, v6, :cond_2

    goto :goto_0

    :cond_2
    add-int/2addr v4, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    :goto_0
    move-object v0, v5

    :goto_1
    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lf/g/d/f/b;->b:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_5

    goto :goto_2

    :cond_5
    sget-object v3, Lf/g/d/f/b;->a:Landroid/webkit/MimeTypeMap;

    invoke-virtual {v3, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    move-object v5, v3

    if-nez v5, :cond_6

    sget-object v3, Lf/g/d/f/a;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    :cond_6
    :goto_3
    if-eqz v5, :cond_7

    const-string v0, "video/"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_8

    const/4 v3, 0x2

    goto :goto_5

    :cond_8
    const/4 v3, 0x3

    goto :goto_5

    :cond_9
    invoke-static {v0}, Lf/g/d/l/b;->c(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v3, 0x4

    goto :goto_5

    :cond_a
    invoke-static {v0}, Lf/g/d/l/b;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "asset"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v3, 0x5

    goto :goto_5

    :cond_b
    invoke-static {v0}, Lf/g/d/l/b;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "res"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v3, 0x6

    goto :goto_5

    :cond_c
    invoke-static {v0}, Lf/g/d/l/b;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "data"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v3, 0x7

    goto :goto_5

    :cond_d
    invoke-static {v0}, Lf/g/d/l/b;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.resource"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v3, 0x8

    :cond_e
    :goto_5
    iput v3, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->c:I

    iget-boolean v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->g:Z

    iput-boolean v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->e:Z

    iget-boolean v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->h:Z

    iput-boolean v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->e:Lf/g/j/d/b;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->c:Lf/g/j/d/e;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->d:Lf/g/j/d/f;

    if-nez v0, :cond_f

    sget-object v0, Lf/g/j/d/f;->c:Lf/g/j/d/f;

    :cond_f
    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->o:Lf/g/j/d/a;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->i:Lf/g/j/d/d;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->b:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iget-boolean v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->k:Z

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->a:Landroid/net/Uri;

    invoke-static {v0}, Lf/g/d/l/b;->e(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_10

    goto :goto_6

    :cond_10
    const/4 v1, 0x0

    :goto_6
    iput-boolean v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    iget-boolean v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->l:Z

    iput-boolean v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->m:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->o:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->j:Lf/g/j/r/b;

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    iget-object p1, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->n:Lf/g/j/l/e;

    iput-object p1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->q:Lf/g/j/l/e;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()Ljava/io/File;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->d:Ljava/io/File;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->d:Ljava/io/File;

    :cond_0
    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->d:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    instance-of v0, p1, Lcom/facebook/imagepipeline/request/ImageRequest;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    check-cast p1, Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-boolean v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    iget-boolean v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    if-eq v0, v2, :cond_1

    return v1

    :cond_1
    iget-boolean v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    iget-boolean v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    if-eq v0, v2, :cond_2

    return v1

    :cond_2
    iget-boolean v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    iget-boolean v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    if-eq v0, v2, :cond_3

    return v1

    :cond_3
    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->d:Ljava/io/File;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->d:Ljava/io/File;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->o:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->o:Ljava/lang/Boolean;

    invoke-static {v0, v2}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    invoke-static {v0, v0}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    iget-object v3, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    invoke-static {v2, v3}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lf/g/j/r/b;->getPostprocessorCacheKey()Lcom/facebook/cache/common/CacheKey;

    move-result-object v1

    goto :goto_0

    :cond_5
    move-object v1, v0

    :goto_0
    iget-object p1, p1, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    if-eqz p1, :cond_6

    invoke-interface {p1}, Lf/g/j/r/b;->getPostprocessorCacheKey()Lcom/facebook/cache/common/CacheKey;

    move-result-object v0

    :cond_6
    invoke-static {v1, v0}, Ls/a/b/b/a;->r(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_7
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 5

    iget-object v0, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lf/g/j/r/b;->getPostprocessorCacheKey()Lcom/facebook/cache/common/CacheKey;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    iget-boolean v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->o:Ljava/lang/Boolean;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    iget-object v4, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    aput-object v0, v2, v3

    const/16 v0, 0xd

    aput-object v1, v2, v0

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Ls/a/b/b/a;->c0(Ljava/lang/Object;)Lf/g/d/d/i;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    const-string v2, "uri"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->a:Lcom/facebook/imagepipeline/request/ImageRequest$b;

    const-string v2, "cacheChoice"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->g:Lf/g/j/d/b;

    const-string v2, "decodeOptions"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->p:Lf/g/j/r/b;

    const-string v2, "postprocessor"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->k:Lf/g/j/d/d;

    const-string v2, "priority"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->h:Lf/g/j/d/e;

    const-string v2, "resizeOptions"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->i:Lf/g/j/d/f;

    const-string v2, "rotationOptions"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->j:Lf/g/j/d/a;

    const-string v2, "bytesRange"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    const/4 v1, 0x0

    const-string v2, "resizingAllowedOverride"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->e:Z

    const-string v2, "progressiveRenderingEnabled"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->f:Z

    const-string v2, "localThumbnailPreviewsEnabled"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->l:Lcom/facebook/imagepipeline/request/ImageRequest$c;

    const-string v2, "lowestPermittedRequestLevel"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->m:Z

    const-string v2, "isDiskCacheEnabled"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->n:Z

    const-string v2, "isMemoryCacheEnabled"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/imagepipeline/request/ImageRequest;->o:Ljava/lang/Boolean;

    const-string v2, "decodePrefetches"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    invoke-virtual {v0}, Lf/g/d/d/i;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
