.class public Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;
.super Ljava/lang/Object;
.source "NativeJpegTranscoder.java"

# interfaces
.implements Lf/g/j/t/b;


# annotations
.annotation build Lf/g/d/d/c;
.end annotation


# instance fields
.field public a:Z

.field public b:I

.field public c:Z


# direct methods
.method public constructor <init>(ZIZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->a:Z

    iput p2, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->b:I

    iput-boolean p3, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->c:Z

    if-eqz p4, :cond_0

    invoke-static {}, Lf/g/j/k/a;->S()V

    :cond_0
    return-void
.end method

.method private static native nativeTranscodeJpeg(Ljava/io/InputStream;Ljava/io/OutputStream;III)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private static native nativeTranscodeJpegWithExifOrientation(Ljava/io/InputStream;Ljava/io/OutputStream;III)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lf/g/d/d/c;
    .end annotation
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "NativeJpegTranscoder"

    return-object v0
.end method

.method public b(Lf/g/j/j/e;Lf/g/j/d/f;Lf/g/j/d/e;)Z
    .locals 1

    if-nez p2, :cond_0

    sget-object p2, Lf/g/j/d/f;->c:Lf/g/j/d/f;

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->a:Z

    invoke-static {p2, p3, p1, v0}, Lf/g/j/t/d;->c(Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/j/j/e;Z)I

    move-result p1

    const/16 p2, 0x8

    if-ge p1, p2, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public c(Lf/g/j/j/e;Ljava/io/OutputStream;Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/i/c;Ljava/lang/Integer;)Lf/g/j/t/a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p6, :cond_0

    const/16 p5, 0x55

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p6

    :cond_0
    if-nez p3, :cond_1

    sget-object p3, Lf/g/j/d/f;->c:Lf/g/j/d/f;

    :cond_1
    iget p5, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->b:I

    invoke-static {p3, p4, p1, p5}, Lf/g/j/k/a;->O(Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/j/j/e;I)I

    move-result p5

    const/4 v0, 0x0

    :try_start_0
    iget-boolean v1, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->a:Z

    invoke-static {p3, p4, p1, v1}, Lf/g/j/t/d;->c(Lf/g/j/d/f;Lf/g/j/d/e;Lf/g/j/j/e;Z)I

    move-result p4

    const/16 v1, 0x8

    div-int v2, v1, p5

    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-boolean v4, p0, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->c:Z

    if-eqz v4, :cond_2

    move p4, v2

    :cond_2
    invoke-virtual {p1}, Lf/g/j/j/e;->e()Ljava/io/InputStream;

    move-result-object v0

    sget-object v2, Lf/g/j/t/d;->a:Lf/g/d/d/e;

    invoke-virtual {p1}, Lf/g/j/j/e;->o()V

    iget v4, p1, Lf/g/j/j/e;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "no transformation requested"

    const/16 v5, 0x64

    const/16 v6, 0x10

    const/4 v7, 0x0

    if-eqz v2, :cond_9

    :try_start_1
    invoke-static {p3, p1}, Lf/g/j/t/d;->a(Lf/g/j/d/f;Lf/g/j/j/e;)I

    move-result p1

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-static {}, Lf/g/j/k/a;->S()V

    if-lt p4, v3, :cond_3

    const/4 p6, 0x1

    goto :goto_0

    :cond_3
    const/4 p6, 0x0

    :goto_0
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-gt p4, v6, :cond_4

    const/4 p6, 0x1

    goto :goto_1

    :cond_4
    const/4 p6, 0x0

    :goto_1
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-ltz p3, :cond_5

    const/4 p6, 0x1

    goto :goto_2

    :cond_5
    const/4 p6, 0x0

    :goto_2
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-gt p3, v5, :cond_6

    const/4 p6, 0x1

    goto :goto_3

    :cond_6
    const/4 p6, 0x0

    :goto_3
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    sget-object p6, Lf/g/j/t/d;->a:Lf/g/d/d/e;

    packed-switch p1, :pswitch_data_0

    const/4 p6, 0x0

    goto :goto_4

    :pswitch_0
    const/4 p6, 0x1

    :goto_4
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-ne p4, v1, :cond_8

    if-eq p1, v3, :cond_7

    goto :goto_5

    :cond_7
    const/4 p6, 0x0

    goto :goto_6

    :cond_8
    :goto_5
    const/4 p6, 0x1

    :goto_6
    invoke-static {p6, v4}, Ls/a/b/b/a;->h(ZLjava/lang/Object;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, p2, p1, p4, p3}, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->nativeTranscodeJpegWithExifOrientation(Ljava/io/InputStream;Ljava/io/OutputStream;III)V

    goto :goto_e

    :cond_9
    invoke-static {p3, p1}, Lf/g/j/t/d;->b(Lf/g/j/d/f;Lf/g/j/j/e;)I

    move-result p1

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p3

    invoke-static {}, Lf/g/j/k/a;->S()V

    if-lt p4, v3, :cond_a

    const/4 p6, 0x1

    goto :goto_7

    :cond_a
    const/4 p6, 0x0

    :goto_7
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-gt p4, v6, :cond_b

    const/4 p6, 0x1

    goto :goto_8

    :cond_b
    const/4 p6, 0x0

    :goto_8
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-ltz p3, :cond_c

    const/4 p6, 0x1

    goto :goto_9

    :cond_c
    const/4 p6, 0x0

    :goto_9
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-gt p3, v5, :cond_d

    const/4 p6, 0x1

    goto :goto_a

    :cond_d
    const/4 p6, 0x0

    :goto_a
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    sget-object p6, Lf/g/j/t/d;->a:Lf/g/d/d/e;

    if-ltz p1, :cond_e

    const/16 p6, 0x10e

    if-gt p1, p6, :cond_e

    rem-int/lit8 p6, p1, 0x5a

    if-nez p6, :cond_e

    const/4 p6, 0x1

    goto :goto_b

    :cond_e
    const/4 p6, 0x0

    :goto_b
    invoke-static {p6}, Ls/a/b/b/a;->g(Z)V

    if-ne p4, v1, :cond_10

    if-eqz p1, :cond_f

    goto :goto_c

    :cond_f
    const/4 p6, 0x0

    goto :goto_d

    :cond_10
    :goto_c
    const/4 p6, 0x1

    :goto_d
    invoke-static {p6, v4}, Ls/a/b/b/a;->h(ZLjava/lang/Object;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, p2, p1, p4, p3}, Lcom/facebook/imagepipeline/nativecode/NativeJpegTranscoder;->nativeTranscodeJpeg(Ljava/io/InputStream;Ljava/io/OutputStream;III)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_e
    invoke-static {v0}, Lf/g/d/d/a;->b(Ljava/io/InputStream;)V

    new-instance p1, Lf/g/j/t/a;

    if-ne p5, v3, :cond_11

    goto :goto_f

    :cond_11
    const/4 v3, 0x0

    :goto_f
    invoke-direct {p1, v3}, Lf/g/j/t/a;-><init>(I)V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {v0}, Lf/g/d/d/a;->b(Ljava/io/InputStream;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public d(Lf/g/i/c;)Z
    .locals 1

    sget-object v0, Lf/g/i/b;->a:Lf/g/i/c;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
