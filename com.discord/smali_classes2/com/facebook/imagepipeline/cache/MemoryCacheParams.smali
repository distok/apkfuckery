.class public Lcom/facebook/imagepipeline/cache/MemoryCacheParams;
.super Ljava/lang/Object;
.source "MemoryCacheParams.java"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:J


# direct methods
.method public constructor <init>(IIIIIJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->a:I

    iput p2, p0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->b:I

    iput p3, p0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->c:I

    iput p4, p0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->d:I

    iput p5, p0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->e:I

    iput-wide p6, p0, Lcom/facebook/imagepipeline/cache/MemoryCacheParams;->f:J

    return-void
.end method
