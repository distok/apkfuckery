.class public Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;
.super Lf/g/j/q/c1;
.source "LocalExifThumbnailProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;->b(Lf/g/j/q/l;Lf/g/j/q/v0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/j/q/c1<",
        "Lf/g/j/j/e;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic i:Lcom/facebook/imagepipeline/request/ImageRequest;

.field public final synthetic j:Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;Lcom/facebook/imagepipeline/request/ImageRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;->j:Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    iput-object p6, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;->i:Lcom/facebook/imagepipeline/request/ImageRequest;

    invoke-direct {p0, p2, p3, p4, p5}, Lf/g/j/q/c1;-><init>(Lf/g/j/q/l;Lf/g/j/q/x0;Lf/g/j/q/v0;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lf/g/j/j/e;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lf/g/j/j/e;->close()V

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Map;
    .locals 1

    check-cast p1, Lf/g/j/j/e;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    const-string v0, "createdThumbnail"

    invoke-static {v0, p1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public d()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;->i:Lcom/facebook/imagepipeline/request/ImageRequest;

    iget-object v0, v0, Lcom/facebook/imagepipeline/request/ImageRequest;->b:Landroid/net/Uri;

    iget-object v7, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;->j:Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    iget-object v1, v7, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;->c:Landroid/content/ContentResolver;

    invoke-static {v0}, Lf/g/d/l/b;->c(Landroid/net/Uri;)Z

    move-result v2

    const/4 v8, 0x0

    const/4 v9, -0x1

    if-eqz v2, :cond_2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, v0

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "_data"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v9, :cond_0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v8, v1

    goto :goto_1

    :cond_0
    move-object v2, v8

    :goto_0
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    :goto_1
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    :cond_2
    invoke-static {v0}, Lf/g/d/l/b;->d(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_3
    move-object v2, v8

    :cond_4
    :goto_2
    const/4 v1, 0x0

    const/4 v3, 0x1

    if-nez v2, :cond_5

    goto :goto_3

    :cond_5
    :try_start_2
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v4}, Ljava/io/File;->canRead()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v1, 0x1

    :cond_6
    :goto_3
    if-eqz v1, :cond_7

    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    iget-object v1, v7, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;->c:Landroid/content/ContentResolver;

    invoke-static {v0}, Lf/g/d/l/b;->c(Landroid/net/Uri;)Z

    move-result v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v2, :cond_8

    :try_start_3
    const-string v2, "r"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    :catch_0
    :cond_8
    move-object v0, v8

    :goto_4
    if-eqz v0, :cond_a

    :try_start_4
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_a

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    if-lt v1, v2, :cond_9

    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, v4}, Landroid/media/ExifInterface;-><init>(Ljava/io/FileDescriptor;)V

    goto :goto_5

    :cond_9
    move-object v1, v8

    :goto_5
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/StackOverflowError; {:try_start_4 .. :try_end_4} :catch_1

    move-object v0, v1

    goto :goto_6

    :catch_1
    const-class v0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    const-string v1, "StackOverflowError in ExifInterface constructor"

    invoke-static {v0, v1}, Lf/g/d/e/a;->a(Ljava/lang/Class;Ljava/lang/String;)V

    :catch_2
    :cond_a
    move-object v0, v8

    :goto_6
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Landroid/media/ExifInterface;->hasThumbnail()Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_9

    :cond_b
    invoke-virtual {v0}, Landroid/media/ExifInterface;->getThumbnail()[B

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;->j:Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    iget-object v2, v2, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;->b:Lf/g/d/g/g;

    invoke-interface {v2, v1}, Lf/g/d/g/g;->b([B)Lcom/facebook/common/memory/PooledByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer$a;->j:Lcom/facebook/imagepipeline/producers/LocalExifThumbnailProducer;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lf/g/d/g/h;

    invoke-direct {v2, v1}, Lf/g/d/g/h;-><init>(Lcom/facebook/common/memory/PooledByteBuffer;)V

    sget-object v4, Lf/g/k/a;->a:Landroidx/core/util/Pools$SynchronizedPool;

    sget-object v4, Lf/g/k/a;->a:Landroidx/core/util/Pools$SynchronizedPool;

    invoke-virtual {v4}, Landroidx/core/util/Pools$SynchronizedPool;->acquire()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/nio/ByteBuffer;

    if-nez v5, :cond_c

    const/16 v5, 0x4000

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    :cond_c
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v3, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    :try_start_5
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    iput-object v3, v6, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    invoke-static {v2, v8, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v2, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eq v2, v9, :cond_e

    iget v2, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v2, v9, :cond_d

    goto :goto_7

    :cond_d
    new-instance v8, Landroid/util/Pair;

    iget v2, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v8, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_e
    :goto_7
    invoke-virtual {v4, v5}, Landroidx/core/util/Pools$SynchronizedPool;->release(Ljava/lang/Object;)Z

    const-string v2, "Orientation"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lf/g/j/k/a;->b0(I)I

    move-result v0

    if-eqz v8, :cond_f

    iget-object v2, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_8

    :cond_f
    const/4 v2, -0x1

    :goto_8
    if-eqz v8, :cond_10

    iget-object v3, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    :cond_10
    invoke-static {v1}, Lcom/facebook/common/references/CloseableReference;->w(Ljava/io/Closeable;)Lcom/facebook/common/references/CloseableReference;

    move-result-object v1

    :try_start_6
    new-instance v8, Lf/g/j/j/e;

    invoke-direct {v8, v1}, Lf/g/j/j/e;-><init>(Lcom/facebook/common/references/CloseableReference;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_11
    sget-object v1, Lf/g/i/b;->a:Lf/g/i/c;

    iput-object v1, v8, Lf/g/j/j/e;->f:Lf/g/i/c;

    iput v0, v8, Lf/g/j/j/e;->g:I

    iput v2, v8, Lf/g/j/j/e;->i:I

    iput v9, v8, Lf/g/j/j/e;->j:I

    goto :goto_9

    :catchall_2
    move-exception v0

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/facebook/common/references/CloseableReference;->close()V

    :cond_12
    throw v0

    :catchall_3
    move-exception v0

    sget-object v1, Lf/g/k/a;->a:Landroidx/core/util/Pools$SynchronizedPool;

    invoke-virtual {v1, v5}, Landroidx/core/util/Pools$SynchronizedPool;->release(Ljava/lang/Object;)Z

    throw v0

    :cond_13
    :goto_9
    return-object v8
.end method
