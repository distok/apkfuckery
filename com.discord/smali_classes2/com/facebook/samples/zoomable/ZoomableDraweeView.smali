.class public Lcom/facebook/samples/zoomable/ZoomableDraweeView;
.super Lcom/facebook/drawee/view/DraweeView;
.source "ZoomableDraweeView.java"

# interfaces
.implements Landroidx/core/view/ScrollingView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/drawee/view/DraweeView<",
        "Lcom/facebook/drawee/generic/GenericDraweeHierarchy;",
        ">;",
        "Landroidx/core/view/ScrollingView;"
    }
.end annotation


# static fields
.field public static final n:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# instance fields
.field public d:Z

.field public final e:Landroid/graphics/RectF;

.field public final f:Landroid/graphics/RectF;

.field public g:Lcom/facebook/drawee/interfaces/DraweeController;

.field public h:Lf/g/l/b/e;

.field public i:Landroid/view/GestureDetector;

.field public j:Z

.field public final k:Lcom/facebook/drawee/controller/ControllerListener;

.field public final l:Lf/g/l/b/e$a;

.field public final m:Lf/g/l/b/d;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    sput-object v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->n:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->d:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->e:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->f:Landroid/graphics/RectF;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->j:Z

    new-instance v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$a;

    invoke-direct {v0, p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView$a;-><init>(Lcom/facebook/samples/zoomable/ZoomableDraweeView;)V

    iput-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->k:Lcom/facebook/drawee/controller/ControllerListener;

    new-instance v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$b;

    invoke-direct {v0, p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView$b;-><init>(Lcom/facebook/samples/zoomable/ZoomableDraweeView;)V

    iput-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->l:Lf/g/l/b/e$a;

    new-instance v1, Lf/g/l/b/d;

    invoke-direct {v1}, Lf/g/l/b/d;-><init>()V

    iput-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->m:Lf/g/l/b/d;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Lf/g/g/f/a;

    invoke-direct {v3, v2}, Lf/g/g/f/a;-><init>(Landroid/content/res/Resources;)V

    sget-object v2, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v2, Lf/g/g/e/v;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    iput-object v2, v3, Lf/g/g/f/a;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-static {v3, p1, p2}, Ls/a/b/b/a;->g0(Lf/g/g/f/a;Landroid/content/Context;Landroid/util/AttributeSet;)Lf/g/g/f/a;

    iget p1, v3, Lf/g/g/f/a;->c:F

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    invoke-virtual {v3}, Lf/g/g/f/a;->a()Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    new-instance p1, Lf/g/l/b/b;

    new-instance p2, Lf/g/l/a/b;

    new-instance v2, Lf/g/l/a/a;

    invoke-direct {v2}, Lf/g/l/a/a;-><init>()V

    invoke-direct {p2, v2}, Lf/g/l/a/b;-><init>(Lf/g/l/a/a;)V

    invoke-direct {p1, p2}, Lf/g/l/b/b;-><init>(Lf/g/l/a/b;)V

    iput-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    iput-object v0, p1, Lf/g/l/b/c;->b:Lf/g/l/b/e$a;

    new-instance p1, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->i:Landroid/view/GestureDetector;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/drawee/interfaces/DraweeController;Lcom/facebook/drawee/interfaces/DraweeController;)V
    .locals 2
    .param p1    # Lcom/facebook/drawee/interfaces/DraweeController;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/drawee/interfaces/DraweeController;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()Lcom/facebook/drawee/interfaces/DraweeController;

    move-result-object v0

    instance-of v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/drawee/controller/AbstractDraweeController;

    iget-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->k:Lcom/facebook/drawee/controller/ControllerListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->A(Lcom/facebook/drawee/controller/ControllerListener;)V

    :cond_0
    instance-of v0, p1, Lcom/facebook/drawee/controller/AbstractDraweeController;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/facebook/drawee/controller/AbstractDraweeController;

    iget-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->k:Lcom/facebook/drawee/controller/ControllerListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->f(Lcom/facebook/drawee/controller/ControllerListener;)V

    :cond_1
    iput-object p2, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->g:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    return-void
.end method

.method public b()V
    .locals 5

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->e:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    iget-object v1, v1, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->f:Lf/g/g/e/g;

    sget-object v2, Lf/g/g/e/g;->g:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lf/g/g/e/g;->n(Landroid/graphics/Matrix;)V

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->f:Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    iget-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->e:Landroid/graphics/RectF;

    check-cast v0, Lf/g/l/b/c;

    iget-object v2, v0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    invoke-virtual {v0}, Lf/g/l/b/c;->i()V

    :cond_0
    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    iget-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->f:Landroid/graphics/RectF;

    check-cast v0, Lf/g/l/b/c;

    iget-object v0, v0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->f:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->e:Landroid/graphics/RectF;

    const-string v4, "updateZoomableControllerBounds: view %x, view bounds: %s, image bounds: %s"

    invoke-static {v0, v4, v1, v2, v3}, Lf/g/d/e/a;->j(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public computeHorizontalScrollExtent()I
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    iget-object v0, v0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public computeHorizontalScrollOffset()I
    .locals 2

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    iget-object v1, v0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v0, v0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    float-to-int v0, v1

    return v0
.end method

.method public computeHorizontalScrollRange()I
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    iget-object v0, v0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public computeVerticalScrollExtent()I
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    iget-object v0, v0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public computeVerticalScrollOffset()I
    .locals 2

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    iget-object v1, v0, Lf/g/l/b/c;->d:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v0, v0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v0

    float-to-int v0, v1

    return v0
.end method

.method public computeVerticalScrollRange()I
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    iget-object v0, v0, Lf/g/l/b/c;->f:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public getLogTag()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    sget-object v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->n:Ljava/lang/Class;

    return-object v0
.end method

.method public getZoomableController()Lf/g/l/b/e;
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v1, Lf/g/l/b/c;

    iget-object v1, v1, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1

    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    sget v0, Lf/g/d/e/a;->a:I

    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->b()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    sget v0, Lf/g/d/e/a;->a:I

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->i:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    return v1

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    invoke-virtual {v0, p1}, Lf/g/l/b/c;->h(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    return v1

    :cond_1
    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    invoke-virtual {v0, p1}, Lf/g/l/b/c;->h(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->j:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    invoke-interface {p1}, Lf/g/l/b/e;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    iget-boolean p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->j:Z

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast p1, Lf/g/l/b/c;

    iget-boolean p1, p1, Lf/g/l/b/c;->l:Z

    if-nez p1, :cond_4

    :cond_3
    invoke-virtual {p0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_4
    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    return v1

    :cond_5
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    return v1

    :cond_6
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->i:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v0, Lf/g/l/b/c;

    invoke-virtual {v0, p1}, Lf/g/l/b/c;->h(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    const/4 p1, 0x0

    return p1
.end method

.method public setAllowTouchInterceptionWhileZoomed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->j:Z

    return-void
.end method

.method public setController(Lcom/facebook/drawee/interfaces/DraweeController;)V
    .locals 3
    .param p1    # Lcom/facebook/drawee/interfaces/DraweeController;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->a(Lcom/facebook/drawee/interfaces/DraweeController;Lcom/facebook/drawee/interfaces/DraweeController;)V

    iget-object v1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast v1, Lf/g/l/b/c;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lf/g/l/b/c;->k(Z)V

    invoke-virtual {p0, p1, v0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->a(Lcom/facebook/drawee/interfaces/DraweeController;Lcom/facebook/drawee/interfaces/DraweeController;)V

    return-void
.end method

.method public setExperimentalSimpleTouchHandlingEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->d:Z

    return-void
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->i:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    return-void
.end method

.method public setTapListener(Landroid/view/GestureDetector$SimpleOnGestureListener;)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->m:Lf/g/l/b/d;

    iput-object p1, v0, Lf/g/l/b/d;->d:Landroid/view/GestureDetector$SimpleOnGestureListener;

    return-void
.end method

.method public setZoomableController(Lf/g/l/b/e;)V
    .locals 2

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    const/4 v1, 0x0

    check-cast v0, Lf/g/l/b/c;

    iput-object v1, v0, Lf/g/l/b/c;->b:Lf/g/l/b/e$a;

    iput-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    iget-object v0, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->l:Lf/g/l/b/e$a;

    check-cast p1, Lf/g/l/b/c;

    iput-object v0, p1, Lf/g/l/b/c;->b:Lf/g/l/b/e$a;

    return-void
.end method
