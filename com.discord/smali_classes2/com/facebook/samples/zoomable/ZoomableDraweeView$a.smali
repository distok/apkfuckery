.class public Lcom/facebook/samples/zoomable/ZoomableDraweeView$a;
.super Lf/g/g/c/c;
.source "ZoomableDraweeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/samples/zoomable/ZoomableDraweeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/g/c/c<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/samples/zoomable/ZoomableDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/samples/zoomable/ZoomableDraweeView;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$a;->a:Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    invoke-direct {p0}, Lf/g/g/c/c;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$a;->a:Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    sget-object p2, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->n:Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    sget p2, Lf/g/d/e/a;->a:I

    iget-object p2, p1, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    check-cast p2, Lf/g/l/b/c;

    iget-boolean p2, p2, Lf/g/l/b/c;->c:Z

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->b()V

    iget-object p1, p1, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    const/4 p2, 0x1

    check-cast p1, Lf/g/l/b/c;

    invoke-virtual {p1, p2}, Lf/g/l/b/c;->k(Z)V

    :cond_0
    return-void
.end method

.method public onRelease(Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Lcom/facebook/samples/zoomable/ZoomableDraweeView$a;->a:Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    sget-object v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->n:Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getLogTag()Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    sget v0, Lf/g/d/e/a;->a:I

    iget-object p1, p1, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->h:Lf/g/l/b/e;

    const/4 v0, 0x0

    check-cast p1, Lf/g/l/b/c;

    invoke-virtual {p1, v0}, Lf/g/l/b/c;->k(Z)V

    return-void
.end method
