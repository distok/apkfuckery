.class public Lcom/facebook/drawee/view/DraweeHolder;
.super Ljava/lang/Object;
.source "DraweeHolder.java"

# interfaces
.implements Lf/g/g/e/g0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DH::",
        "Lcom/facebook/drawee/interfaces/DraweeHierarchy;",
        ">",
        "Ljava/lang/Object;",
        "Lf/g/g/e/g0;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Lcom/facebook/drawee/interfaces/DraweeHierarchy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDH;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/drawee/interfaces/DraweeController;

.field public final f:Lf/g/g/b/c;


# direct methods
.method public constructor <init>(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDH;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    sget-boolean v0, Lf/g/g/b/c;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Lf/g/g/b/c;

    invoke-direct {v0}, Lf/g/g/b/c;-><init>()V

    goto :goto_0

    :cond_0
    sget-object v0, Lf/g/g/b/c;->b:Lf/g/g/b/c;

    :goto_0
    iput-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/view/DraweeHolder;->h(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->j:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeController;->b()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeController;->d()V

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->a()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->c()V

    :goto_0
    return-void
.end method

.method public final c()V
    .locals 2

    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->k:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeController;->a()V

    :cond_1
    return-void
.end method

.method public d()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->d:Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeHierarchy;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public e()Z
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/facebook/drawee/interfaces/DraweeController;->b()Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->d:Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->c:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    if-eqz p1, :cond_1

    sget-object v1, Lf/g/g/b/c$a;->t:Lf/g/g/b/c$a;

    goto :goto_0

    :cond_1
    sget-object v1, Lf/g/g/b/c$a;->u:Lf/g/g/b/c$a;

    :goto_0
    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iput-boolean p1, p0, Lcom/facebook/drawee/view/DraweeHolder;->c:Z

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->b()V

    return-void
.end method

.method public g(Lcom/facebook/drawee/interfaces/DraweeController;)V
    .locals 3

    iget-boolean v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v2, Lf/g/g/b/c$a;->g:Lf/g/g/b/c$a;

    invoke-virtual {v1, v2}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-object v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/facebook/drawee/interfaces/DraweeController;->e(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    :cond_1
    iput-object p1, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->f:Lf/g/g/b/c$a;

    invoke-virtual {p1, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-object p1, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    iget-object v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->d:Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    invoke-interface {p1, v1}, Lcom/facebook/drawee/interfaces/DraweeController;->e(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->h:Lf/g/g/b/c$a;

    invoke-virtual {p1, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->a()V

    :cond_3
    return-void
.end method

.method public h(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TDH;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->d:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->e()Z

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v2, v1, Lf/g/g/e/f0;

    if-eqz v2, :cond_0

    check-cast v1, Lf/g/g/e/f0;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lf/g/g/e/f0;->k(Lf/g/g/e/g0;)V

    :cond_0
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/facebook/drawee/view/DraweeHolder;->d:Lcom/facebook/drawee/interfaces/DraweeHierarchy;

    invoke-interface {p1}, Lcom/facebook/drawee/interfaces/DraweeHierarchy;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/facebook/drawee/view/DraweeHolder;->f(Z)V

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeHolder;->d()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v2, v1, Lf/g/g/e/f0;

    if-eqz v2, :cond_3

    check-cast v1, Lf/g/g/e/f0;

    invoke-interface {v1, p0}, Lf/g/g/e/f0;->k(Lf/g/g/e/g0;)V

    :cond_3
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/drawee/view/DraweeHolder;->e:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-interface {v0, p1}, Lcom/facebook/drawee/interfaces/DraweeController;->e(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V

    :cond_4
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Ls/a/b/b/a;->c0(Ljava/lang/Object;)Lf/g/d/d/i;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->a:Z

    const-string v2, "controllerAttached"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->b:Z

    const-string v2, "holderAttached"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->c:Z

    const-string v2, "drawableVisible"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    invoke-virtual {v1}, Lf/g/g/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "events"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    invoke-virtual {v0}, Lf/g/d/d/i;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
