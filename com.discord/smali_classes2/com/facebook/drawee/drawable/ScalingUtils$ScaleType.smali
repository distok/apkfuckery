.class public interface abstract Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;
.super Ljava/lang/Object;
.source "ScalingUtils.java"


# static fields
.field public static final a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final b:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final c:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final d:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final e:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final f:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final g:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final h:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final i:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final j:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

.field public static final k:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lf/g/g/e/z;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/y;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->b:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/a0;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->c:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/x;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->d:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/v;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->e:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/w;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->f:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/r;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->g:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/t;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->h:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/s;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->i:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/b0;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->j:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/u;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sput-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->k:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFF)Landroid/graphics/Matrix;
.end method
