.class public Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
.super Landroid/text/SpannableStringBuilder;
.source "DraweeSpanStringBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;,
        Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;
    }
.end annotation


# static fields
.field public static final synthetic g:I


# instance fields
.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lf/g/g/i/a;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;

.field public f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->d:Ljava/util/Set;

    new-instance v0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;-><init>(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Lcom/facebook/drawee/span/DraweeSpanStringBuilder$a;)V

    iput-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->e:Lcom/facebook/drawee/span/DraweeSpanStringBuilder$b;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    if-eq v0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->f:Landroid/view/View;

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->f:Landroid/view/View;

    iget-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->d:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/g/i/a;

    iget-object v0, v0, Lf/g/g/i/a;->j:Lcom/facebook/drawee/view/DraweeHolder;

    iget-object v1, v0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v2, Lf/g/g/b/c$a;->r:Lf/g/g/b/c$a;

    invoke-virtual {v1, v2}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/drawee/view/DraweeHolder;->b:Z

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeHolder;->b()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->f:Landroid/view/View;

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->f:Landroid/view/View;

    :goto_0
    iget-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->d:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf/g/g/i/a;

    iget-object v0, v0, Lf/g/g/i/a;->j:Lcom/facebook/drawee/view/DraweeHolder;

    iget-object v1, v0, Lcom/facebook/drawee/view/DraweeHolder;->f:Lf/g/g/b/c;

    sget-object v2, Lf/g/g/b/c$a;->s:Lf/g/g/b/c$a;

    invoke-virtual {v1, v2}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/drawee/view/DraweeHolder;->b:Z

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeHolder;->b()V

    goto :goto_1

    :cond_1
    return-void
.end method
