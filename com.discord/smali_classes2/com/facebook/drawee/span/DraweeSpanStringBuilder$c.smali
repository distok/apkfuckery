.class public Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;
.super Lf/g/g/c/c;
.source "DraweeSpanStringBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/g/c/c<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lf/g/g/i/a;

.field public final b:Z

.field public final c:I

.field public final synthetic d:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;


# direct methods
.method public constructor <init>(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Lf/g/g/i/a;ZI)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->d:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-direct {p0}, Lf/g/g/c/c;-><init>()V

    iput-object p2, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->a:Lf/g/g/i/a;

    iput-boolean p3, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->b:Z

    iput p4, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->c:I

    return-void
.end method


# virtual methods
.method public onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 3

    check-cast p2, Lcom/facebook/imagepipeline/image/ImageInfo;

    iget-boolean p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->b:Z

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    iget-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->a:Lf/g/g/i/a;

    iget-object p1, p1, Lf/g/g/i/a;->j:Lcom/facebook/drawee/view/DraweeHolder;

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeHolder;->d()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->a:Lf/g/g/i/a;

    iget-object p1, p1, Lf/g/g/i/a;->j:Lcom/facebook/drawee/view/DraweeHolder;

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeHolder;->d()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object p3

    iget v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->c:I

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    int-to-float v0, v0

    invoke-interface {p2}, Lcom/facebook/imagepipeline/image/ImageInfo;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-interface {p2}, Lcom/facebook/imagepipeline/image/ImageInfo;->getWidth()I

    move-result p2

    int-to-float p2, p2

    mul-float v0, v0, p2

    float-to-int p2, v0

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-ne v0, p2, :cond_0

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    iget v0, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->c:I

    if-eq p3, v0, :cond_3

    :cond_0
    iget p3, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->c:I

    invoke-virtual {p1, v2, v2, p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->d:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    sget p2, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->g:I

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-interface {p2}, Lcom/facebook/imagepipeline/image/ImageInfo;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    invoke-interface {p2}, Lcom/facebook/imagepipeline/image/ImageInfo;->getHeight()I

    move-result v0

    if-eq p3, v0, :cond_3

    :cond_2
    invoke-interface {p2}, Lcom/facebook/imagepipeline/image/ImageInfo;->getWidth()I

    move-result p3

    invoke-interface {p2}, Lcom/facebook/imagepipeline/image/ImageInfo;->getHeight()I

    move-result p2

    invoke-virtual {p1, v2, v2, p3, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object p1, p0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder$c;->d:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    sget p2, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;->g:I

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_0
    return-void
.end method
