.class public Lcom/facebook/drawee/generic/GenericDraweeHierarchy;
.super Ljava/lang/Object;
.source "GenericDraweeHierarchy.java"

# interfaces
.implements Lf/g/g/h/a;


# instance fields
.field public final a:Landroid/graphics/drawable/Drawable;

.field public final b:Landroid/content/res/Resources;

.field public c:Lf/g/g/f/c;

.field public final d:Lf/g/g/f/b;

.field public final e:Lf/g/g/e/f;

.field public final f:Lf/g/g/e/g;


# direct methods
.method public constructor <init>(Lf/g/g/f/a;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v2, p1, Lf/g/g/f/a;->a:Landroid/content/res/Resources;

    iput-object v2, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    iget-object v2, p1, Lf/g/g/f/a;->p:Lf/g/g/f/c;

    iput-object v2, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    new-instance v2, Lf/g/g/e/g;

    invoke-direct {v2, v0}, Lf/g/g/e/g;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->f:Lf/g/g/e/g;

    iget-object v0, p1, Lf/g/g/f/a;->n:Ljava/util/List;

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v4, p1, Lf/g/g/f/a;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    add-int/2addr v0, v4

    add-int/lit8 v4, v0, 0x6

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    iget-object v5, p1, Lf/g/g/f/a;->m:Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v5, p1, Lf/g/g/f/a;->d:Landroid/graphics/drawable/Drawable;

    iget-object v7, p1, Lf/g/g/f/a;->e:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {p0, v5, v7}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v5, 0x2

    iget-object v7, p1, Lf/g/g/f/a;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {v2, v6}, Lf/g/g/e/g;->setColorFilter(Landroid/graphics/ColorFilter;)V

    invoke-static {v2, v7, v6}, Lf/g/g/f/d;->e(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x3

    iget-object v5, p1, Lf/g/g/f/a;->j:Landroid/graphics/drawable/Drawable;

    iget-object v7, p1, Lf/g/g/f/a;->k:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {p0, v5, v7}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x4

    iget-object v5, p1, Lf/g/g/f/a;->f:Landroid/graphics/drawable/Drawable;

    iget-object v7, p1, Lf/g/g/f/a;->g:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {p0, v5, v7}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x5

    iget-object v5, p1, Lf/g/g/f/a;->h:Landroid/graphics/drawable/Drawable;

    iget-object v7, p1, Lf/g/g/f/a;->i:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    invoke-virtual {p0, v5, v7}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v2

    if-lez v0, :cond_5

    iget-object v0, p1, Lf/g/g/f/a;->n:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/Drawable;

    add-int/lit8 v7, v2, 0x1

    add-int/lit8 v2, v2, 0x6

    invoke-virtual {p0, v5, v6}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v4, v2

    move v2, v7

    goto :goto_2

    :cond_3
    const/4 v2, 0x1

    :cond_4
    iget-object v0, p1, Lf/g/g/f/a;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    add-int/lit8 v2, v2, 0x6

    invoke-virtual {p0, v0, v6}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aput-object v0, v4, v2

    :cond_5
    new-instance v0, Lf/g/g/e/f;

    invoke-direct {v0, v4}, Lf/g/g/e/f;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    iget p1, p1, Lf/g/g/f/a;->b:I

    iput p1, v0, Lf/g/g/e/f;->n:I

    iget p1, v0, Lf/g/g/e/f;->m:I

    if-ne p1, v3, :cond_6

    iput v1, v0, Lf/g/g/e/f;->m:I

    :cond_6
    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    invoke-static {v0, p1}, Lf/g/g/f/d;->d(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    new-instance v0, Lf/g/g/f/b;

    invoke-direct {v0, p1}, Lf/g/g/f/b;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->d:Lf/g/g/f/b;

    invoke-virtual {v0}, Lf/g/g/e/g;->mutate()Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->n()V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->d:Lf/g/g/f/b;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->d:Lf/g/g/f/b;

    iput-object p1, v0, Lf/g/g/f/b;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public c(Ljava/lang/Throwable;)V
    .locals 1

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->e()V

    invoke-virtual {p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->j()V

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lf/g/g/e/b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    :goto_0
    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->f()V

    return-void
.end method

.method public d(Ljava/lang/Throwable;)V
    .locals 1

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->e()V

    invoke-virtual {p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->j()V

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lf/g/g/e/b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    :goto_0
    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->f()V

    return-void
.end method

.method public e(FZ)V
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lf/g/g/e/b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {v0}, Lf/g/g/e/f;->e()V

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->q(F)V

    if-eqz p2, :cond_1

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->g()V

    :cond_1
    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->f()V

    return-void
.end method

.method public f()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->d:Lf/g/g/f/b;

    return-object v0
.end method

.method public g(Landroid/graphics/drawable/Drawable;FZ)V
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    iget-object v1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1}, Lf/g/g/f/d;->c(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->f:Lf/g/g/e/g;

    invoke-virtual {v0, p1}, Lf/g/g/e/g;->o(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->e()V

    invoke-virtual {p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->j()V

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->q(F)V

    if-eqz p3, :cond_0

    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->g()V

    :cond_0
    iget-object p1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {p1}, Lf/g/g/e/f;->f()V

    return-void
.end method

.method public final h(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    iget-object v1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    invoke-static {p1, v0, v1}, Lf/g/g/f/d;->c(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lf/g/g/f/d;->e(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public final i(I)V
    .locals 4

    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lf/g/g/e/f;->v:Z

    iput v2, v0, Lf/g/g/e/f;->m:I

    iget-object v1, v0, Lf/g/g/e/f;->s:[Z

    aput-boolean v3, v1, p1

    invoke-virtual {v0}, Lf/g/g/e/f;->invalidateSelf()V

    :cond_1
    return-void
.end method

.method public final j()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->k(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->k(I)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->k(I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->k(I)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->k(I)V

    return-void
.end method

.method public final k(I)V
    .locals 3

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v1, 0x0

    iput v1, v0, Lf/g/g/e/f;->m:I

    iget-object v2, v0, Lf/g/g/e/f;->s:[Z

    aput-boolean v1, v2, p1

    invoke-virtual {v0}, Lf/g/g/e/f;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public final l(I)Lf/g/g/e/d;
    .locals 4

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ltz p1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static {v3}, Ls/a/b/b/a;->g(Z)V

    iget-object v3, v0, Lf/g/g/e/b;->g:[Lf/g/g/e/d;

    array-length v3, v3

    if-ge p1, v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, Ls/a/b/b/a;->g(Z)V

    iget-object v1, v0, Lf/g/g/e/b;->g:[Lf/g/g/e/d;

    aget-object v2, v1, p1

    if-nez v2, :cond_2

    new-instance v2, Lf/g/g/e/a;

    invoke-direct {v2, v0, p1}, Lf/g/g/e/a;-><init>(Lf/g/g/e/b;I)V

    aput-object v2, v1, p1

    :cond_2
    aget-object p1, v1, p1

    invoke-interface {p1}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Lf/g/g/e/h;

    if-eqz v0, :cond_3

    invoke-interface {p1}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Lf/g/g/e/h;

    :cond_3
    invoke-interface {p1}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Lf/g/g/e/p;

    if-eqz v0, :cond_4

    invoke-interface {p1}, Lf/g/g/e/d;->l()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Lf/g/g/e/p;

    :cond_4
    return-object p1
.end method

.method public final m(I)Lf/g/g/e/p;
    .locals 3

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->l(I)Lf/g/g/e/d;

    move-result-object p1

    instance-of v0, p1, Lf/g/g/e/p;

    if-eqz v0, :cond_0

    check-cast p1, Lf/g/g/e/p;

    return-object p1

    :cond_0
    sget-object v0, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v0, Lf/g/g/e/z;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v1, Lf/g/g/f/d;->a:Landroid/graphics/drawable/Drawable;

    invoke-interface {p1, v1}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lf/g/g/f/d;->e(Landroid/graphics/drawable/Drawable;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;Landroid/graphics/PointF;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-interface {p1, v0}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    const-string p1, "Parent has no child drawable!"

    invoke-static {v0, p1}, Ls/a/b/b/a;->i(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lf/g/g/e/p;

    return-object v0
.end method

.method public final n()V
    .locals 3

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/g/g/e/f;->e()V

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v1, 0x0

    iput v1, v0, Lf/g/g/e/f;->m:I

    iget-object v1, v0, Lf/g/g/e/f;->s:[Z

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([ZZ)V

    invoke-virtual {v0}, Lf/g/g/e/f;->invalidateSelf()V

    invoke-virtual {p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->j()V

    invoke-virtual {p0, v2}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {v0}, Lf/g/g/e/f;->g()V

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    invoke-virtual {v0}, Lf/g/g/e/f;->f()V

    :cond_0
    return-void
.end method

.method public final o(ILandroid/graphics/drawable/Drawable;)V
    .locals 2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lf/g/g/e/b;->c(ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->c:Lf/g/g/f/c;

    iget-object v1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    invoke-static {p2, v0, v1}, Lf/g/g/f/d;->c(Landroid/graphics/drawable/Drawable;Lf/g/g/f/c;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->l(I)Lf/g/g/e/d;

    move-result-object p1

    invoke-interface {p1, p2}, Lf/g/g/e/d;->g(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public p(I)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->o(ILandroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final q(F)V
    .locals 3

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->e:Lf/g/g/e/f;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lf/g/g/e/b;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v2, 0x3f7fbe77    # 0.999f

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_2

    instance-of v2, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Landroid/graphics/drawable/Animatable;

    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->stop()V

    :cond_1
    invoke-virtual {p0, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->k(I)V

    goto :goto_0

    :cond_2
    instance-of v2, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v2, :cond_3

    move-object v2, v0

    check-cast v2, Landroid/graphics/drawable/Animatable;

    invoke-interface {v2}, Landroid/graphics/drawable/Animatable;->start()V

    :cond_3
    invoke-virtual {p0, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->i(I)V

    :goto_0
    const v1, 0x461c4000    # 10000.0f

    mul-float p1, p1, v1

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    return-void
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->f:Lf/g/g/e/g;

    iget-object v1, p0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lf/g/g/e/g;->o(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->n()V

    return-void
.end method
