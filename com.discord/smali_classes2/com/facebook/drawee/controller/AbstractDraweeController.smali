.class public abstract Lcom/facebook/drawee/controller/AbstractDraweeController;
.super Ljava/lang/Object;
.source "AbstractDraweeController.java"

# interfaces
.implements Lcom/facebook/drawee/interfaces/DraweeController;
.implements Lf/g/g/b/a$a;
.implements Lf/g/g/g/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/drawee/controller/AbstractDraweeController$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "INFO:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/drawee/interfaces/DraweeController;",
        "Lf/g/g/b/a$a;",
        "Lf/g/g/g/a$a;"
    }
.end annotation


# static fields
.field public static final u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lf/g/g/b/c;

.field public final b:Lf/g/g/b/a;

.field public final c:Ljava/util/concurrent/Executor;

.field public d:Lf/g/g/b/d;

.field public e:Lf/g/g/g/a;

.field public f:Lcom/facebook/drawee/controller/ControllerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "TINFO;>;"
        }
    .end annotation
.end field

.field public g:Lf/g/h/b/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/h/b/a/c<",
            "TINFO;>;"
        }
    .end annotation
.end field

.field public h:Lf/g/g/h/a;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/Object;

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Lcom/facebook/datasource/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;"
        }
    .end annotation
.end field

.field public r:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public s:Z

.field public t:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const-string v0, "component_tag"

    const-string v1, "drawee"

    invoke-static {v0, v1}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->u:Ljava/util/Map;

    const-string v0, "origin"

    const-string v1, "memory_bitmap"

    const-string v2, "origin_sub"

    const-string v3, "shortcut"

    invoke-static {v0, v1, v2, v3}, Lf/g/d/d/f;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->v:Ljava/util/Map;

    const-class v0, Lcom/facebook/drawee/controller/AbstractDraweeController;

    sput-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->w:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lf/g/g/b/a;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean p3, Lf/g/g/b/c;->c:Z

    if-eqz p3, :cond_0

    new-instance p3, Lf/g/g/b/c;

    invoke-direct {p3}, Lf/g/g/b/c;-><init>()V

    goto :goto_0

    :cond_0
    sget-object p3, Lf/g/g/b/c;->b:Lf/g/g/b/c;

    :goto_0
    iput-object p3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    new-instance p3, Lf/g/h/b/a/c;

    invoke-direct {p3}, Lf/g/h/b/a/c;-><init>()V

    iput-object p3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    const/4 p3, 0x1

    iput-boolean p3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->s:Z

    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->b:Lf/g/g/b/a;

    iput-object p2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->c:Ljava/util/concurrent/Executor;

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->n(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public A(Lcom/facebook/drawee/controller/ControllerListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "-TINFO;>;)V"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    instance-of v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast v0, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    monitor-enter v0

    :try_start_0
    iget-object v1, v0, Lf/g/g/c/d;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    iget-object v1, v0, Lf/g/g/c/d;->a:Ljava/util/List;

    invoke-interface {v1, p1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_1
    if-ne v0, p1, :cond_2

    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    :cond_2
    return-void
.end method

.method public B(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;TINFO;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->i()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->k:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/facebook/drawee/controller/ControllerListener;->onSubmit(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->k:Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->m()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, p1, p2, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->r(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;Landroid/net/Uri;)Lf/g/h/b/a/b$a;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lf/g/h/b/a/c;->a(Ljava/lang/String;Ljava/lang/Object;Lf/g/h/b/a/b$a;)V

    return-void
.end method

.method public final C(Ljava/lang/String;Ljava/lang/Object;Lcom/facebook/datasource/DataSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->l(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->i()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->c()Landroid/graphics/drawable/Animatable;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/drawee/controller/ControllerListener;->onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    const/4 v1, 0x0

    invoke-virtual {p0, p3, p2, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->r(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;Landroid/net/Uri;)Lf/g/h/b/a/b$a;

    move-result-object p3

    invoke-virtual {v0, p1, p2, p3}, Lf/g/h/b/a/c;->d(Ljava/lang/String;Ljava/lang/Object;Lf/g/h/b/a/b$a;)V

    return-void
.end method

.method public final D()Z
    .locals 4

    iget-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->n:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->d:Lf/g/g/b/d;

    if-eqz v0, :cond_1

    iget-boolean v3, v0, Lf/g/g/b/d;->a:Z

    if-eqz v3, :cond_0

    iget v3, v0, Lf/g/g/b/d;->c:I

    iget v0, v0, Lf/g/g/b/d;->b:I

    if-ge v3, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public E()V
    .locals 8

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->h()Ljava/lang/Object;

    move-result-object v3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v3, :cond_0

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iput-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    iput-boolean v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    iput-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->n:Z

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->A:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    invoke-virtual {p0, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->l(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->B(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->v(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/drawee/controller/AbstractDraweeController;->w(Ljava/lang/String;Lcom/facebook/datasource/DataSource;Ljava/lang/Object;FZZZ)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_0
    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    sget-object v4, Lf/g/g/b/c$a;->m:Lf/g/g/b/c$a;

    invoke-virtual {v3, v4}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2}, Lf/g/g/h/a;->e(FZ)V

    iput-boolean v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    iput-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->n:Z

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->j()Lcom/facebook/datasource/DataSource;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->B(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;)V

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->w:Ljava/lang/Class;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "controller %x %s: submitRequest: dataSource: %x"

    invoke-static {v0, v4, v1, v2, v3}, Lf/g/d/e/a;->j(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    invoke-interface {v1}, Lcom/facebook/datasource/DataSource;->b()Z

    move-result v1

    new-instance v2, Lcom/facebook/drawee/controller/AbstractDraweeController$a;

    invoke-direct {v2, p0, v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController$a;-><init>(Lcom/facebook/drawee/controller/AbstractDraweeController;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v2, v1}, Lcom/facebook/datasource/DataSource;->f(Lf/g/e/f;Ljava/util/concurrent/Executor;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method

.method public a()V
    .locals 5

    invoke-static {}, Lf/g/j/s/b;->b()Z

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->k:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->l:Z

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->b:Lf/g/g/b/a;

    check-cast v1, Lf/g/g/b/b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->release()V

    goto :goto_1

    :cond_2
    iget-object v2, v1, Lf/g/g/b/b;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v1, Lf/g/g/b/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    monitor-exit v2

    goto :goto_1

    :cond_3
    iget-object v3, v1, Lf/g/g/b/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v1, Lf/g/g/b/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_4

    const/4 v0, 0x1

    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lf/g/g/b/b;->c:Landroid/os/Handler;

    iget-object v1, v1, Lf/g/g/b/b;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_5
    :goto_1
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Lcom/facebook/drawee/interfaces/DraweeHierarchy;
    .locals 1

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    return-object v0
.end method

.method public c()Landroid/graphics/drawable/Animatable;
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->t:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public d()V
    .locals 5

    invoke-static {}, Lf/g/j/s/b;->b()Z

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->w:Ljava/lang/Class;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    if-eqz v3, :cond_0

    const-string v3, "request already submitted"

    goto :goto_0

    :cond_0
    const-string v3, "request needs submit"

    :goto_0
    const-string v4, "controller %x %s: onAttach: %s"

    invoke-static {v0, v4, v1, v2, v3}, Lf/g/d/e/a;->j(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->j:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->b:Lf/g/g/b/a;

    invoke-virtual {v0, p0}, Lf/g/g/b/a;->a(Lf/g/g/b/a$a;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->l:Z

    iget-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->E()V

    :cond_2
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method

.method public e(Lcom/facebook/drawee/interfaces/DraweeHierarchy;)V
    .locals 4

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->w:Ljava/lang/Class;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    const-string v3, "controller %x %s: setHierarchy: %s"

    invoke-static {v0, v3, v1, v2, p1}, Lf/g/d/e/a;->j(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    if-eqz p1, :cond_1

    sget-object v1, Lf/g/g/b/c$a;->d:Lf/g/g/b/c$a;

    goto :goto_0

    :cond_1
    sget-object v1, Lf/g/g/b/c$a;->e:Lf/g/g/b/c$a;

    :goto_0
    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->b:Lf/g/g/b/a;

    invoke-virtual {v0, p0}, Lf/g/g/b/a;->a(Lf/g/g/b/a$a;)V

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->release()V

    :cond_2
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lf/g/g/h/a;->b(Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    :cond_3
    if-eqz p1, :cond_4

    instance-of v0, p1, Lf/g/g/h/a;

    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    check-cast p1, Lf/g/g/h/a;

    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->i:Landroid/graphics/drawable/Drawable;

    invoke-interface {p1, v0}, Lf/g/g/h/a;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    return-void
.end method

.method public f(Lcom/facebook/drawee/controller/ControllerListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "-TINFO;>;)V"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    instance-of v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    invoke-virtual {v0, p1}, Lf/g/g/c/d;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    return-void

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    new-instance v1, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    invoke-direct {v1}, Lcom/facebook/drawee/controller/AbstractDraweeController$b;-><init>()V

    invoke-virtual {v1, v0}, Lf/g/g/c/d;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    invoke-virtual {v1, p1}, Lf/g/g/c/d;->a(Lcom/facebook/drawee/controller/ControllerListener;)V

    invoke-static {}, Lf/g/j/s/b;->b()Z

    iput-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    return-void

    :cond_1
    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    return-void
.end method

.method public abstract g(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation
.end method

.method public h()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public i()Lcom/facebook/drawee/controller/ControllerListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/drawee/controller/ControllerListener<",
            "TINFO;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    if-nez v0, :cond_0

    invoke-static {}, Lf/g/g/c/c;->getNoOpListener()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public abstract j()Lcom/facebook/datasource/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;"
        }
    .end annotation
.end method

.method public k(Ljava/lang/Object;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public abstract l(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TINFO;"
        }
    .end annotation
.end method

.method public m()Landroid/net/Uri;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final declared-synchronized n(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->i:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->b:Lf/g/g/b/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lf/g/g/b/a;->a(Lf/g/g/b/a$a;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->l:Z

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->y()V

    iput-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->o:Z

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->d:Lf/g/g/b/d;

    if-eqz v1, :cond_1

    iput-boolean v0, v1, Lf/g/g/b/d;->a:Z

    const/4 v2, 0x4

    iput v2, v1, Lf/g/g/b/d;->b:I

    iput v0, v1, Lf/g/g/b/d;->c:I

    :cond_1
    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->e:Lf/g/g/g/a;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iput-object v2, v1, Lf/g/g/g/a;->a:Lf/g/g/g/a$a;

    iput-boolean v0, v1, Lf/g/g/g/a;->c:Z

    iput-boolean v0, v1, Lf/g/g/g/a;->d:Z

    iput-object p0, v1, Lf/g/g/g/a;->a:Lf/g/g/g/a$a;

    :cond_2
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    instance-of v1, v0, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/facebook/drawee/controller/AbstractDraweeController$b;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, v0, Lf/g/g/c/d;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_3
    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->f:Lcom/facebook/drawee/controller/ControllerListener;

    :goto_0
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lf/g/g/h/a;->reset()V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {v0, v2}, Lf/g/g/h/a;->b(Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    :cond_4
    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->i:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/facebook/drawee/controller/AbstractDraweeController;->w:Ljava/lang/Class;

    const-string v1, "controller %x %s -> %s: initialize"

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, p1}, Lf/g/d/e/a;->j(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_5
    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    iput-object p2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->k:Ljava/lang/Object;

    invoke-static {}, Lf/g/j/s/b;->b()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final o(Ljava/lang/String;Lcom/facebook/datasource/DataSource;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;)Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    if-nez v1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    if-ne p2, p1, :cond_1

    iget-boolean p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v0, 0x2

    invoke-static {v0}, Lf/g/d/e/a;->h(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/facebook/drawee/controller/AbstractDraweeController;->w:Ljava/lang/Class;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    const-string v5, "controller %x %s: onTouchEvent %s"

    invoke-static {v2, v5, v3, v4, p1}, Lf/g/d/e/a;->j(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->e:Lf/g/g/g/a;

    const/4 v3, 0x0

    if-nez v2, :cond_1

    return v3

    :cond_1
    iget-boolean v2, v2, Lf/g/g/g/a;->c:Z

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->D()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    return v3

    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->e:Lf/g/g/g/a;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_c

    if-eq v4, v5, :cond_7

    if-eq v4, v0, :cond_5

    const/4 p1, 0x3

    if-eq v4, p1, :cond_4

    goto/16 :goto_1

    :cond_4
    iput-boolean v3, v2, Lf/g/g/g/a;->c:Z

    iput-boolean v3, v2, Lf/g/g/g/a;->d:Z

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, v2, Lf/g/g/g/a;->f:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, v2, Lf/g/g/g/a;->b:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iget v0, v2, Lf/g/g/g/a;->g:F

    sub-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    iget v0, v2, Lf/g/g/g/a;->b:F

    cmpl-float p1, p1, v0

    if-lez p1, :cond_d

    :cond_6
    iput-boolean v3, v2, Lf/g/g/g/a;->d:Z

    goto :goto_1

    :cond_7
    iput-boolean v3, v2, Lf/g/g/g/a;->c:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v4, v2, Lf/g/g/g/a;->f:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, v2, Lf/g/g/g/a;->b:F

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v4, v2, Lf/g/g/g/a;->g:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, v2, Lf/g/g/g/a;->b:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_9

    :cond_8
    iput-boolean v3, v2, Lf/g/g/g/a;->d:Z

    :cond_9
    iget-boolean v0, v2, Lf/g/g/g/a;->d:Z

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iget-wide v8, v2, Lf/g/g/g/a;->e:J

    sub-long/2addr v6, v8

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    int-to-long v8, p1

    cmp-long p1, v6, v8

    if-gtz p1, :cond_b

    iget-object p1, v2, Lf/g/g/g/a;->a:Lf/g/g/g/a$a;

    if-eqz p1, :cond_b

    check-cast p1, Lcom/facebook/drawee/controller/AbstractDraweeController;

    if-eqz v1, :cond_a

    invoke-static {p1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    :cond_a
    invoke-virtual {p1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->D()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/facebook/drawee/controller/AbstractDraweeController;->d:Lf/g/g/b/d;

    iget v1, v0, Lf/g/g/b/d;->c:I

    add-int/2addr v1, v5

    iput v1, v0, Lf/g/g/b/d;->c:I

    iget-object v0, p1, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {v0}, Lf/g/g/h/a;->reset()V

    invoke-virtual {p1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->E()V

    :cond_b
    iput-boolean v3, v2, Lf/g/g/g/a;->d:Z

    goto :goto_1

    :cond_c
    iput-boolean v5, v2, Lf/g/g/g/a;->c:Z

    iput-boolean v5, v2, Lf/g/g/g/a;->d:Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, v2, Lf/g/g/g/a;->e:J

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, v2, Lf/g/g/g/a;->f:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, v2, Lf/g/g/g/a;->g:F

    :cond_d
    :goto_1
    return v5
.end method

.method public final p(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    const/4 p1, 0x2

    invoke-static {p1}, Lf/g/d/e/a;->h(I)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    :cond_0
    return-void
.end method

.method public final q(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    const/4 p1, 0x2

    invoke-static {p1}, Lf/g/d/e/a;->h(I)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->k(Ljava/lang/Object;)I

    :cond_1
    return-void
.end method

.method public final r(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;Landroid/net/Uri;)Lf/g/h/b/a/b$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;TINFO;",
            "Landroid/net/Uri;",
            ")",
            "Lf/g/h/b/a/b$a;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lcom/facebook/datasource/DataSource;->a()Ljava/util/Map;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->t(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->s(Ljava/util/Map;Ljava/util/Map;Landroid/net/Uri;)Lf/g/h/b/a/b$a;

    move-result-object p1

    return-object p1
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    sget-object v1, Lf/g/g/b/c$a;->l:Lf/g/g/b/c$a;

    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->d:Lf/g/g/b/d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iput v1, v0, Lf/g/g/b/d;->c:I

    :cond_0
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->e:Lf/g/g/g/a;

    if-eqz v0, :cond_1

    iput-boolean v1, v0, Lf/g/g/g/a;->c:Z

    iput-boolean v1, v0, Lf/g/g/g/a;->d:Z

    :cond_1
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lf/g/g/h/a;->reset()V

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->y()V

    return-void
.end method

.method public final s(Ljava/util/Map;Ljava/util/Map;Landroid/net/Uri;)Lf/g/h/b/a/b$a;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Landroid/net/Uri;",
            ")",
            "Lf/g/h/b/a/b$a;"
        }
    .end annotation

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    instance-of v1, v0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->l(I)Lf/g/g/e/d;

    move-result-object v3

    instance-of v3, v3, Lf/g/g/e/p;

    if-nez v3, :cond_0

    move-object v0, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->m(I)Lf/g/g/e/p;

    move-result-object v0

    iget-object v0, v0, Lf/g/g/e/p;->h:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    check-cast v3, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;

    invoke-virtual {v3, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->l(I)Lf/g/g/e/d;

    move-result-object v4

    instance-of v4, v4, Lf/g/g/e/p;

    if-nez v4, :cond_1

    move-object v1, v2

    goto :goto_1

    :cond_1
    invoke-virtual {v3, v1}, Lcom/facebook/drawee/generic/GenericDraweeHierarchy;->m(I)Lf/g/g/e/p;

    move-result-object v1

    iget-object v1, v1, Lf/g/g/e/p;->j:Landroid/graphics/PointF;

    goto :goto_1

    :cond_2
    move-object v0, v2

    move-object v1, v0

    :goto_1
    sget-object v3, Lcom/facebook/drawee/controller/AbstractDraweeController;->u:Ljava/util/Map;

    sget-object v4, Lcom/facebook/drawee/controller/AbstractDraweeController;->v:Ljava/util/Map;

    iget-object v5, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    if-nez v5, :cond_3

    goto :goto_2

    :cond_3
    invoke-interface {v5}, Lcom/facebook/drawee/interfaces/DraweeHierarchy;->a()Landroid/graphics/Rect;

    move-result-object v2

    :goto_2
    iget-object v5, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->k:Ljava/lang/Object;

    new-instance v6, Lf/g/h/b/a/b$a;

    invoke-direct {v6}, Lf/g/h/b/a/b$a;-><init>()V

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    const-string v3, "viewport_height"

    const-string v7, "viewport_width"

    if-eqz v2, :cond_4

    iget-object v8, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v7, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    iget-object v2, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    const/4 v8, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v2, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    iget-object v2, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    const-string v3, "scale_type"

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_5

    iget-object v0, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const-string v3, "focus_point_x"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const-string v2, "focus_point_y"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget-object v0, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    const-string v1, "caller_context"

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_6

    iget-object v0, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    const-string v1, "uri_main"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    if-eqz p1, :cond_7

    iput-object p1, v6, Lf/g/h/b/a/b$a;->a:Ljava/util/Map;

    if-eqz p2, :cond_8

    invoke-interface {p1, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_4

    :cond_7
    iput-object p2, v6, Lf/g/h/b/a/b$a;->a:Ljava/util/Map;

    iget-object p1, v6, Lf/g/h/b/a/b$a;->b:Ljava/util/Map;

    invoke-interface {p1, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_8
    :goto_4
    return-object v6
.end method

.method public abstract t(Ljava/lang/Object;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TINFO;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Ls/a/b/b/a;->c0(Ljava/lang/Object;)Lf/g/d/d/i;

    move-result-object v0

    iget-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->l:Z

    const-string v2, "isAttached"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    const-string v2, "isRequestSubmitted"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->n:Z

    const-string v2, "hasFetchFailed"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->b(Ljava/lang/String;Z)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->k(Ljava/lang/Object;)I

    move-result v1

    const-string v2, "fetchedImage"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->a(Ljava/lang/String;I)Lf/g/d/d/i;

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    invoke-virtual {v1}, Lf/g/g/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "events"

    invoke-virtual {v0, v2, v1}, Lf/g/d/d/i;->c(Ljava/lang/String;Ljava/lang/Object;)Lf/g/d/d/i;

    invoke-virtual {v0}, Lf/g/d/d/i;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(Ljava/lang/String;Lcom/facebook/datasource/DataSource;Ljava/lang/Throwable;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            "Z)V"
        }
    .end annotation

    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->o(Ljava/lang/String;Lcom/facebook/datasource/DataSource;)Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "ignore_old_datasource @ onFailure"

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->p(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2}, Lcom/facebook/datasource/DataSource;->close()Z

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_0
    iget-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    if-eqz p4, :cond_1

    sget-object v0, Lf/g/g/b/c$a;->p:Lf/g/g/b/c$a;

    goto :goto_0

    :cond_1
    sget-object v0, Lf/g/g/b/c$a;->q:Lf/g/g/b/c$a;

    :goto_0
    invoke-virtual {p1, v0}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V

    if-eqz p4, :cond_4

    const-string p1, "final_failed @ onFailure"

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->p(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    const/4 p4, 0x1

    iput-boolean p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->n:Z

    iget-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v0, v2, p4}, Lf/g/g/h/a;->g(Landroid/graphics/drawable/Drawable;FZ)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->D()Z

    move-result p4

    if-eqz p4, :cond_3

    iget-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {p4, p3}, Lf/g/g/h/a;->c(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    iget-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {p4, p3}, Lf/g/g/h/a;->d(Ljava/lang/Throwable;)V

    :goto_1
    invoke-virtual {p0, p2, p1, p1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->r(Lcom/facebook/datasource/DataSource;Ljava/lang/Object;Landroid/net/Uri;)Lf/g/h/b/a/b$a;

    move-result-object p1

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->i()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object p2

    iget-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-interface {p2, p4, p3}, Lcom/facebook/drawee/controller/ControllerListener;->onFailure(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object p2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    iget-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-virtual {p2, p4, p3, p1}, Lf/g/h/b/a/c;->b(Ljava/lang/String;Ljava/lang/Throwable;Lf/g/h/b/a/b$a;)V

    goto :goto_2

    :cond_4
    const-string p1, "intermediate_failed @ onFailure"

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->p(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->i()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object p1

    iget-object p2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-interface {p1, p2, p3}, Lcom/facebook/drawee/controller/ControllerListener;->onIntermediateImageFailed(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TT;)V"
        }
    .end annotation

    return-void
.end method

.method public final w(Ljava/lang/String;Lcom/facebook/datasource/DataSource;Ljava/lang/Object;FZZZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/datasource/DataSource<",
            "TT;>;TT;FZZZ)V"
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lf/g/j/s/b;->b()Z

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->o(Ljava/lang/String;Lcom/facebook/datasource/DataSource;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "ignore_old_datasource @ onNewResult"

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->z(Ljava/lang/Object;)V

    invoke-interface {p2}, Lcom/facebook/datasource/DataSource;->close()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->a:Lf/g/g/b/c;

    if-eqz p5, :cond_1

    sget-object v1, Lf/g/g/b/c$a;->n:Lf/g/g/b/c$a;

    goto :goto_0

    :cond_1
    sget-object v1, Lf/g/g/b/c$a;->o:Lf/g/g/b/c$a;

    :goto_0
    invoke-virtual {v0, v1}, Lf/g/g/b/c;->a(Lf/g/g/b/c$a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {p0, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->g(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->t:Landroid/graphics/drawable/Drawable;

    iput-object p3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    iput-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->t:Landroid/graphics/drawable/Drawable;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const-string v3, "release_previous_result @ onNewResult"

    const/high16 v4, 0x3f800000    # 1.0f

    if-eqz p5, :cond_2

    :try_start_4
    const-string p4, "set_final_result @ onNewResult"

    invoke-virtual {p0, p4, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 p4, 0x0

    iput-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    iget-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {p4, v0, v4, p6}, Lf/g/g/h/a;->g(Landroid/graphics/drawable/Drawable;FZ)V

    invoke-virtual {p0, p1, p3, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->C(Ljava/lang/String;Ljava/lang/Object;Lcom/facebook/datasource/DataSource;)V

    goto :goto_1

    :cond_2
    if-eqz p7, :cond_3

    const-string p4, "set_temporary_result @ onNewResult"

    invoke-virtual {p0, p4, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object p4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {p4, v0, v4, p6}, Lf/g/g/h/a;->g(Landroid/graphics/drawable/Drawable;FZ)V

    invoke-virtual {p0, p1, p3, p2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->C(Ljava/lang/String;Ljava/lang/Object;Lcom/facebook/datasource/DataSource;)V

    goto :goto_1

    :cond_3
    const-string p2, "set_intermediate_result @ onNewResult"

    invoke-virtual {p0, p2, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object p2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->h:Lf/g/g/h/a;

    invoke-interface {p2, v0, p4, p6}, Lf/g/g/h/a;->g(Landroid/graphics/drawable/Drawable;FZ)V

    invoke-virtual {p0, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->l(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->i()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object p4

    invoke-interface {p4, p1, p2}, Lcom/facebook/drawee/controller/ControllerListener;->onIntermediateImageSet(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    if-eqz v2, :cond_4

    if-eq v2, v0, :cond_4

    :try_start_5
    invoke-virtual {p0, v2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->x(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    if-eqz v1, :cond_5

    if-eq v1, p3, :cond_5

    invoke-virtual {p0, v3, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->z(Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_5
    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_0
    move-exception p1

    if-eqz v2, :cond_6

    if-eq v2, v0, :cond_6

    :try_start_6
    invoke-virtual {p0, v2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->x(Landroid/graphics/drawable/Drawable;)V

    :cond_6
    if-eqz v1, :cond_7

    if-eq v1, p3, :cond_7

    invoke-virtual {p0, v3, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->z(Ljava/lang/Object;)V

    :cond_7
    throw p1

    :catch_0
    move-exception p4

    const-string p6, "drawable_failed @ onNewResult"

    invoke-virtual {p0, p6, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0, p3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->z(Ljava/lang/Object;)V

    invoke-virtual {p0, p1, p2, p4, p5}, Lcom/facebook/drawee/controller/AbstractDraweeController;->u(Ljava/lang/String;Lcom/facebook/datasource/DataSource;Ljava/lang/Throwable;Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    return-void

    :catchall_1
    move-exception p1

    invoke-static {}, Lf/g/j/s/b;->b()Z

    throw p1
.end method

.method public abstract x(Landroid/graphics/drawable/Drawable;)V
.end method

.method public final y()V
    .locals 6

    iget-boolean v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->m:Z

    iput-boolean v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->n:Z

    iget-object v1, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/facebook/datasource/DataSource;->a()Ljava/util/Map;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    invoke-interface {v3}, Lcom/facebook/datasource/DataSource;->close()Z

    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->q:Lcom/facebook/datasource/DataSource;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->t:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    invoke-virtual {p0, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->x(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->p:Ljava/lang/String;

    if-eqz v3, :cond_2

    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->p:Ljava/lang/String;

    :cond_2
    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->t:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    if-eqz v3, :cond_3

    invoke-virtual {p0, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->l(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/facebook/drawee/controller/AbstractDraweeController;->t(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    const-string v5, "release"

    invoke-virtual {p0, v5, v4}, Lcom/facebook/drawee/controller/AbstractDraweeController;->q(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    invoke-virtual {p0, v4}, Lcom/facebook/drawee/controller/AbstractDraweeController;->z(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->r:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v3, v2

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/facebook/drawee/controller/AbstractDraweeController;->i()Lcom/facebook/drawee/controller/ControllerListener;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-interface {v0, v4}, Lcom/facebook/drawee/controller/ControllerListener;->onRelease(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->g:Lf/g/h/b/a/c;

    iget-object v4, p0, Lcom/facebook/drawee/controller/AbstractDraweeController;->j:Ljava/lang/String;

    invoke-virtual {p0, v1, v3, v2}, Lcom/facebook/drawee/controller/AbstractDraweeController;->s(Ljava/util/Map;Ljava/util/Map;Landroid/net/Uri;)Lf/g/h/b/a/b$a;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lf/g/h/b/a/c;->c(Ljava/lang/String;Lf/g/h/b/a/b$a;)V

    :cond_4
    return-void
.end method

.method public abstract z(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method
