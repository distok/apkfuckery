.class public Lcom/facebook/cache/disk/DiskCacheConfig;
.super Ljava/lang/Object;
.source "DiskCacheConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/cache/disk/DiskCacheConfig$b;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/common/internal/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/internal/Supplier<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:Lf/g/b/b/g;

.field public final h:Lf/g/b/a/a;

.field public final i:Lf/g/b/a/b;

.field public final j:Lf/g/d/a/a;

.field public final k:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/cache/disk/DiskCacheConfig$b;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->e:Landroid/content/Context;

    iput-object v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->k:Landroid/content/Context;

    iget-object v1, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->b:Lcom/facebook/common/internal/Supplier;

    const/4 v2, 0x1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const-string v3, "Either a non-null context or a base directory path or supplier must be provided."

    invoke-static {v1, v3}, Ls/a/b/b/a;->k(ZLjava/lang/Object;)V

    iget-object v1, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->b:Lcom/facebook/common/internal/Supplier;

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    new-instance v0, Lcom/facebook/cache/disk/DiskCacheConfig$a;

    invoke-direct {v0, p0}, Lcom/facebook/cache/disk/DiskCacheConfig$a;-><init>(Lcom/facebook/cache/disk/DiskCacheConfig;)V

    iput-object v0, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->b:Lcom/facebook/common/internal/Supplier;

    :cond_2
    iput v2, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->a:I

    iget-object v0, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->b:Ljava/lang/String;

    iget-object v0, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->b:Lcom/facebook/common/internal/Supplier;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->c:Lcom/facebook/common/internal/Supplier;

    iget-wide v0, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->c:J

    iput-wide v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->d:J

    const-wide/32 v0, 0xa00000

    iput-wide v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->e:J

    const-wide/32 v0, 0x200000

    iput-wide v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->f:J

    iget-object p1, p1, Lcom/facebook/cache/disk/DiskCacheConfig$b;->d:Lf/g/b/b/g;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->g:Lf/g/b/b/g;

    const-class p1, Lf/g/b/a/d;

    monitor-enter p1

    :try_start_0
    sget-object v0, Lf/g/b/a/d;->a:Lf/g/b/a/d;

    if-nez v0, :cond_3

    new-instance v0, Lf/g/b/a/d;

    invoke-direct {v0}, Lf/g/b/a/d;-><init>()V

    sput-object v0, Lf/g/b/a/d;->a:Lf/g/b/a/d;

    :cond_3
    sget-object v0, Lf/g/b/a/d;->a:Lf/g/b/a/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    monitor-exit p1

    iput-object v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->h:Lf/g/b/a/a;

    const-class p1, Lf/g/b/a/e;

    monitor-enter p1

    :try_start_1
    sget-object v0, Lf/g/b/a/e;->a:Lf/g/b/a/e;

    if-nez v0, :cond_4

    new-instance v0, Lf/g/b/a/e;

    invoke-direct {v0}, Lf/g/b/a/e;-><init>()V

    sput-object v0, Lf/g/b/a/e;->a:Lf/g/b/a/e;

    :cond_4
    sget-object v0, Lf/g/b/a/e;->a:Lf/g/b/a/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p1

    iput-object v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->i:Lf/g/b/a/b;

    const-class p1, Lf/g/d/a/b;

    monitor-enter p1

    :try_start_2
    sget-object v0, Lf/g/d/a/b;->a:Lf/g/d/a/b;

    if-nez v0, :cond_5

    new-instance v0, Lf/g/d/a/b;

    invoke-direct {v0}, Lf/g/d/a/b;-><init>()V

    sput-object v0, Lf/g/d/a/b;->a:Lf/g/d/a/b;

    :cond_5
    sget-object v0, Lf/g/d/a/b;->a:Lf/g/d/a/b;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p1

    iput-object v0, p0, Lcom/facebook/cache/disk/DiskCacheConfig;->j:Lf/g/d/a/a;

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p1

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit p1

    throw v0
.end method
