.class public Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;
.super Ljava/lang/Object;
.source "AnimatedFactoryV2Impl.java"

# interfaces
.implements Lf/g/j/a/b/a;


# annotations
.annotation build Lf/g/d/d/c;
.end annotation


# instance fields
.field public final a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

.field public final b:Lf/g/j/e/f;

.field public final c:Lf/g/j/c/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public e:Lf/g/j/a/b/d;

.field public f:Lf/g/j/a/c/b;

.field public g:Lf/g/j/a/d/a;

.field public h:Lf/g/j/i/a;


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/j/e/f;Lf/g/j/c/m;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;",
            "Lf/g/j/e/f;",
            "Lf/g/j/c/m<",
            "Lcom/facebook/cache/common/CacheKey;",
            "Lf/g/j/j/c;",
            ">;Z)V"
        }
    .end annotation

    .annotation build Lf/g/d/d/c;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iput-object p2, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->b:Lf/g/j/e/f;

    iput-object p3, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->c:Lf/g/j/c/m;

    iput-boolean p4, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->d:Z

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lf/g/j/i/a;
    .locals 9

    iget-object p1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->h:Lf/g/j/i/a;

    if-nez p1, :cond_2

    new-instance v7, Lf/g/h/a/d/a;

    invoke-direct {v7, p0}, Lf/g/h/a/d/a;-><init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;)V

    new-instance v3, Lf/g/d/b/c;

    iget-object p1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->b:Lf/g/j/e/f;

    invoke-interface {p1}, Lf/g/j/e/f;->a()Ljava/util/concurrent/Executor;

    move-result-object p1

    invoke-direct {v3, p1}, Lf/g/d/b/c;-><init>(Ljava/util/concurrent/Executor;)V

    new-instance v8, Lf/g/h/a/d/b;

    invoke-direct {v8, p0}, Lf/g/h/a/d/b;-><init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;)V

    new-instance p1, Lf/g/h/a/d/e;

    iget-object v0, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->f:Lf/g/j/a/c/b;

    if-nez v0, :cond_0

    new-instance v0, Lf/g/h/a/d/c;

    invoke-direct {v0, p0}, Lf/g/h/a/d/c;-><init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;)V

    iput-object v0, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->f:Lf/g/j/a/c/b;

    :cond_0
    iget-object v1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->f:Lf/g/j/a/c/b;

    sget-object v0, Lf/g/d/b/f;->e:Lf/g/d/b/f;

    if-nez v0, :cond_1

    new-instance v0, Lf/g/d/b/f;

    invoke-direct {v0}, Lf/g/d/b/f;-><init>()V

    sput-object v0, Lf/g/d/b/f;->e:Lf/g/d/b/f;

    :cond_1
    sget-object v2, Lf/g/d/b/f;->e:Lf/g/d/b/f;

    invoke-static {}, Lcom/facebook/common/time/RealtimeSinceBootClock;->get()Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    iget-object v6, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->c:Lf/g/j/c/m;

    move-object v0, p1

    invoke-direct/range {v0 .. v8}, Lf/g/h/a/d/e;-><init>(Lf/g/j/a/c/b;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/ExecutorService;Lf/g/d/k/b;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;Lf/g/j/c/m;Lcom/facebook/common/internal/Supplier;Lcom/facebook/common/internal/Supplier;)V

    iput-object p1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->h:Lf/g/j/i/a;

    :cond_2
    iget-object p1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->h:Lf/g/j/i/a;

    return-object p1
.end method

.method public b(Landroid/graphics/Bitmap$Config;)Lf/g/j/h/b;
    .locals 1

    new-instance v0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;

    invoke-direct {v0, p0, p1}, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;-><init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;Landroid/graphics/Bitmap$Config;)V

    return-object v0
.end method

.method public c(Landroid/graphics/Bitmap$Config;)Lf/g/j/h/b;
    .locals 1

    new-instance v0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$b;

    invoke-direct {v0, p0, p1}, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$b;-><init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;Landroid/graphics/Bitmap$Config;)V

    return-object v0
.end method
