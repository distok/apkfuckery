.class public Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;
.super Ljava/lang/Object;
.source "AnimatedFactoryV2Impl.java"

# interfaces
.implements Lf/g/j/h/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->b(Landroid/graphics/Bitmap$Config;)Lf/g/j/h/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/graphics/Bitmap$Config;

.field public final synthetic b:Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;


# direct methods
.method public constructor <init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;Landroid/graphics/Bitmap$Config;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;->b:Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;

    iput-object p2, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;->a:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf/g/j/j/e;ILf/g/j/j/i;Lf/g/j/d/b;)Lf/g/j/j/c;
    .locals 4

    iget-object p2, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;->b:Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;

    iget-object p3, p2, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->e:Lf/g/j/a/b/d;

    if-nez p3, :cond_0

    new-instance p3, Lf/g/h/a/d/d;

    invoke-direct {p3, p2}, Lf/g/h/a/d/d;-><init>(Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;)V

    new-instance v0, Lf/g/j/a/b/e;

    iget-object v1, p2, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->a:Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;

    invoke-direct {v0, p3, v1}, Lf/g/j/a/b/e;-><init>(Lf/g/j/a/c/b;Lcom/facebook/imagepipeline/bitmaps/PlatformBitmapFactory;)V

    iput-object v0, p2, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->e:Lf/g/j/a/b/d;

    :cond_0
    iget-object p2, p2, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl;->e:Lf/g/j/a/b/d;

    iget-object p3, p0, Lcom/facebook/fresco/animation/factory/AnimatedFactoryV2Impl$a;->a:Landroid/graphics/Bitmap$Config;

    check-cast p2, Lf/g/j/a/b/e;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lf/g/j/a/b/e;->c:Lf/g/j/a/b/c;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lf/g/j/j/e;->c()Lcom/facebook/common/references/CloseableReference;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->n()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/memory/PooledByteBuffer;

    invoke-interface {v0}, Lcom/facebook/common/memory/PooledByteBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v1, Lf/g/j/a/b/e;->c:Lf/g/j/a/b/c;

    invoke-interface {v0}, Lcom/facebook/common/memory/PooledByteBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v1, v0, p4}, Lf/g/j/a/b/c;->c(Ljava/nio/ByteBuffer;Lf/g/j/d/b;)Lf/g/j/a/a/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v1, Lf/g/j/a/b/e;->c:Lf/g/j/a/b/c;

    invoke-interface {v0}, Lcom/facebook/common/memory/PooledByteBuffer;->p()J

    move-result-wide v2

    invoke-interface {v0}, Lcom/facebook/common/memory/PooledByteBuffer;->size()I

    move-result v0

    invoke-interface {v1, v2, v3, v0, p4}, Lf/g/j/a/b/c;->g(JILf/g/j/d/b;)Lf/g/j/a/a/c;

    move-result-object v0

    :goto_0
    invoke-virtual {p2, p4, v0, p3}, Lf/g/j/a/b/e;->a(Lf/g/j/d/b;Lf/g/j/a/a/c;Landroid/graphics/Bitmap$Config;)Lf/g/j/j/c;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Lcom/facebook/common/references/CloseableReference;->close()V

    throw p2

    :cond_2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "To encode animated gif please add the dependency to the animated-gif module"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
