.class public Lcom/facebook/animated/gif/GifImage;
.super Ljava/lang/Object;
.source "GifImage.java"

# interfaces
.implements Lf/g/j/a/a/c;
.implements Lf/g/j/a/b/c;


# annotations
.annotation build Lf/g/d/d/c;
.end annotation


# static fields
.field public static volatile a:Z


# instance fields
.field private mNativeContext:J
    .annotation build Lf/g/d/d/c;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lf/g/d/d/c;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .annotation build Lf/g/d/d/c;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/facebook/animated/gif/GifImage;->mNativeContext:J

    return-void
.end method

.method public static declared-synchronized j()V
    .locals 2

    const-class v0, Lcom/facebook/animated/gif/GifImage;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/facebook/animated/gif/GifImage;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/facebook/animated/gif/GifImage;->a:Z

    const-string v1, "gifimage"

    invoke-static {v1}, Lf/g/m/n/a;->c(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static native nativeCreateFromDirectByteBuffer(Ljava/nio/ByteBuffer;IZ)Lcom/facebook/animated/gif/GifImage;
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private static native nativeCreateFromFileDescriptor(IIZ)Lcom/facebook/animated/gif/GifImage;
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private static native nativeCreateFromNativeMemory(JIIZ)Lcom/facebook/animated/gif/GifImage;
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeDispose()V
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeFinalize()V
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetDuration()I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetFrame(I)Lcom/facebook/animated/gif/GifFrame;
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetFrameCount()I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetFrameDurations()[I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetHeight()I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetLoopCount()I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetSizeInBytes()I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeGetWidth()I
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method

.method private native nativeIsAnimated()Z
    .annotation build Lf/g/d/d/c;
    .end annotation
.end method


# virtual methods
.method public a()I
    .locals 1

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrameCount()I

    move-result v0

    return v0
.end method

.method public b()I
    .locals 3

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetLoopCount()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_0

    add-int/2addr v0, v2

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    return v2
.end method

.method public c(Ljava/nio/ByteBuffer;Lf/g/j/d/b;)Lf/g/j/a/a/c;
    .locals 1

    invoke-static {}, Lcom/facebook/animated/gif/GifImage;->j()V

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget p2, p2, Lf/g/j/d/b;->b:I

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/facebook/animated/gif/GifImage;->nativeCreateFromDirectByteBuffer(Ljava/nio/ByteBuffer;IZ)Lcom/facebook/animated/gif/GifImage;

    move-result-object p1

    return-object p1
.end method

.method public d(I)Lf/g/j/a/a/d;
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrame(I)Lcom/facebook/animated/gif/GifFrame;

    move-result-object p1

    return-object p1
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f(I)Lf/g/j/a/a/b;
    .locals 10

    invoke-direct {p0, p1}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrame(I)Lcom/facebook/animated/gif/GifFrame;

    move-result-object v0

    :try_start_0
    new-instance v9, Lf/g/j/a/a/b;

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->b()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->c()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->getHeight()I

    move-result v6

    sget-object v7, Lf/g/j/a/a/b$a;->d:Lf/g/j/a/a/b$a;

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->d()I

    move-result v1

    sget-object v2, Lf/g/j/a/a/b$b;->d:Lf/g/j/a/a/b$b;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v8, 0x1

    if-ne v1, v8, :cond_1

    goto :goto_1

    :cond_1
    const/4 v8, 0x2

    if-ne v1, v8, :cond_2

    sget-object v1, Lf/g/j/a/a/b$b;->e:Lf/g/j/a/a/b$b;

    goto :goto_0

    :cond_2
    const/4 v8, 0x3

    if-ne v1, v8, :cond_3

    sget-object v1, Lf/g/j/a/a/b$b;->f:Lf/g/j/a/a/b$b;

    :goto_0
    move-object v8, v1

    goto :goto_2

    :cond_3
    :goto_1
    move-object v8, v2

    :goto_2
    move-object v1, v9

    move v2, p1

    invoke-direct/range {v1 .. v8}, Lf/g/j/a/a/b;-><init>(IIIIILf/g/j/a/a/b$a;Lf/g/j/a/a/b$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->dispose()V

    return-object v9

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Lcom/facebook/animated/gif/GifFrame;->dispose()V

    throw p1
.end method

.method public finalize()V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeFinalize()V

    return-void
.end method

.method public g(JILf/g/j/d/b;)Lf/g/j/a/a/c;
    .locals 4

    invoke-static {}, Lcom/facebook/animated/gif/GifImage;->j()V

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    cmp-long v3, p1, v0

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ls/a/b/b/a;->g(Z)V

    iget p4, p4, Lf/g/j/d/b;->b:I

    invoke-static {p1, p2, p3, p4, v2}, Lcom/facebook/animated/gif/GifImage;->nativeCreateFromNativeMemory(JIIZ)Lcom/facebook/animated/gif/GifImage;

    move-result-object p1

    return-object p1
.end method

.method public getHeight()I
    .locals 1

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetHeight()I

    move-result v0

    return v0
.end method

.method public getWidth()I
    .locals 1

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetWidth()I

    move-result v0

    return v0
.end method

.method public h()[I
    .locals 1

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetFrameDurations()[I

    move-result-object v0

    return-object v0
.end method

.method public i()I
    .locals 1

    invoke-direct {p0}, Lcom/facebook/animated/gif/GifImage;->nativeGetSizeInBytes()I

    move-result v0

    return v0
.end method
