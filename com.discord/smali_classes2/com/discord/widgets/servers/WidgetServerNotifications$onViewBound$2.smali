.class public final Lcom/discord/widgets/servers/WidgetServerNotifications$onViewBound$2;
.super Ljava/lang/Object;
.source "WidgetServerNotifications.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/WidgetServerNotifications;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $guildId:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/servers/WidgetServerNotifications$onViewBound$2;->$guildId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    sget-object v0, Lcom/discord/widgets/servers/WidgetServerNotificationsOverrideSelector;->Companion:Lcom/discord/widgets/servers/WidgetServerNotificationsOverrideSelector$Companion;

    const-string v1, "it"

    const-string v2, "it.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerNotifications$onViewBound$2;->$guildId:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/widgets/servers/WidgetServerNotificationsOverrideSelector$Companion;->launch(Landroid/content/Context;J)V

    return-void
.end method
