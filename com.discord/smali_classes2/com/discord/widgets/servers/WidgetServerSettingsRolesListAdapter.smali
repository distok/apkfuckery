.class public Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;
.super Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;
.source "WidgetServerSettingsRolesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleListHelpItem;,
        Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleListItem;,
        Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$HelpItem;,
        Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter<",
        "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;",
        ">;"
    }
.end annotation


# static fields
.field public static final TYPE_HELP:I = 0x0

.field public static final TYPE_ROLE:I = 0x1


# instance fields
.field private roleDropListener:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private roleSelectedListener:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method


# virtual methods
.method public computeChangedPositions()Ljava/util/Map;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;

    invoke-interface {v3}, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;->getType()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    goto :goto_1

    :cond_0
    check-cast v3, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;

    invoke-virtual {v3}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;->isEveryoneRole()Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v1, -0x1

    sub-int/2addr v4, v2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;->getOrigPositions()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v3}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v4, v5, :cond_2

    invoke-virtual {v3}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public configure(Ljava/util/List;Lrx/functions/Action1;Lrx/functions/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;",
            ">;",
            "Lrx/functions/Action1<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Lrx/functions/Action1<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    iput-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;->roleSelectedListener:Lrx/functions/Action1;

    iput-object p3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;->roleDropListener:Lrx/functions/Action1;

    invoke-virtual {p0, p1}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;->setData(Ljava/util/List;)V

    return-void
.end method

.method public getRoleSelectedListener()Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/functions/Action1<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;->roleSelectedListener:Lrx/functions/Action1;

    return-object v0
.end method

.method public isValidMove(II)Z
    .locals 4

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    :goto_0
    const/4 p2, 0x1

    if-gt v0, p1, :cond_2

    invoke-virtual {p0}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;

    invoke-interface {v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;->getType()I

    move-result v2

    const/4 v3, 0x0

    if-eq v2, p2, :cond_0

    return v3

    :cond_0
    check-cast v1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;->canReorder()Z

    move-result p2

    if-nez p2, :cond_1

    return v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return p2
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;",
            ">;"
        }
    .end annotation

    const/4 p1, 0x1

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleListItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleListItem;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;)V

    return-object p1

    :cond_0
    if-nez p2, :cond_1

    new-instance p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleListHelpItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleListHelpItem;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;)V

    return-object p1

    :cond_1
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public onNewPositions(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter;->roleDropListener:Lrx/functions/Action1;

    invoke-interface {v0, p1}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    return-void
.end method
