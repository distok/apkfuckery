.class public final synthetic Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->values()[Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const/4 v0, 0x6

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_CONDITIONAL:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const/4 v2, 0x4

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v4, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_ADMIN:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const/4 v4, 0x5

    const/4 v5, 0x2

    aput v5, v1, v4

    invoke-static {}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->values()[Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->NO_MANAGE_ROLES_PERMISSION:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const/4 v1, 0x0

    aput v3, v0, v1

    sget-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->LOCKED_HIGHER:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput v5, v0, v3

    sget-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->LOCKED_HIGHEST:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const/4 v1, 0x3

    aput v1, v0, v5

    sget-object v3, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->USER_NOT_ELEVATED:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput v2, v0, v1

    return-void
.end method
