.class public final Lcom/discord/widgets/servers/WidgetServerNotifications$configureRadio$1;
.super Ljava/lang/Object;
.source "WidgetServerNotifications.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/WidgetServerNotifications;->configureRadio(Lcom/discord/views/CheckedSetting;ILcom/discord/widgets/servers/WidgetServerNotifications$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $model:Lcom/discord/widgets/servers/WidgetServerNotifications$Model;

.field public final synthetic $type:I


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/WidgetServerNotifications$Model;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerNotifications$configureRadio$1;->$model:Lcom/discord/widgets/servers/WidgetServerNotifications$Model;

    iput p2, p0, Lcom/discord/widgets/servers/WidgetServerNotifications$configureRadio$1;->$type:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v0

    const-string v1, "view"

    const-string v2, "view.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerNotifications$configureRadio$1;->$model:Lcom/discord/widgets/servers/WidgetServerNotifications$Model;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/WidgetServerNotifications$Model;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    iget v2, p0, Lcom/discord/widgets/servers/WidgetServerNotifications$configureRadio$1;->$type:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/stores/StoreUserGuildSettings;->setGuildFrequency(Landroid/content/Context;Lcom/discord/models/domain/ModelGuild;I)V

    return-void
.end method
