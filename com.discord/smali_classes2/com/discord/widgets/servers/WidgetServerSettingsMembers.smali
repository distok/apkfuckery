.class public Lcom/discord/widgets/servers/WidgetServerSettingsMembers;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsMembers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter;,
        Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;
    }
.end annotation


# static fields
.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "GUILD_ID"

.field private static final VIEW_INDEX_MEMBER_LIST:I = 0x0

.field private static final VIEW_INDEX_NO_RESULTS:I = 0x1

.field public static final synthetic d:I


# instance fields
.field private adapter:Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;

.field private guildId:J

.field private guildRoles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFilterPublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private recycler:Landroidx/recyclerview/widget/RecyclerView;

.field private final roleFilterPublisher:Lrx/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/Subject<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private rolesSpinner:Landroid/widget/Spinner;

.field private searchBox:Lcom/google/android/material/textfield/TextInputLayout;

.field private viewFlipper:Landroid/widget/ViewFlipper;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const-string v0, ""

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->nameFilterPublisher:Lrx/subjects/Subject;

    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->roleFilterPublisher:Lrx/subjects/Subject;

    return-void
.end method

.method public static synthetic access$100(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;)Lrx/subjects/Subject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->roleFilterPublisher:Lrx/subjects/Subject;

    return-object p0
.end method

.method public static synthetic access$200(Ljava/lang/String;)Ljava/lang/Long;
    .locals 0

    invoke-static {p0}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->parseUserId(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p0

    return-object p0
.end method

.method private configureMenu(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V
    .locals 2

    iget-boolean v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->canKick:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0e0016

    goto :goto_0

    :cond_0
    const v0, 0x7f0e000c

    :goto_0
    new-instance v1, Lf/a/o/e/g1;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/g1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V

    invoke-virtual {p0, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method private configureToolbar(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f120fd8

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V
    .locals 2

    if-eqz p1, :cond_5

    iget-boolean v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->canManageMembers:Z

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->configureMenu(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V

    iget-object v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->configureToolbar(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->viewFlipper:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_2

    iget-object v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->memberItems:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildRoles:Ljava/util/Map;

    if-eqz v0, :cond_3

    iget-object v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->roles:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->roles:Ljava/util/Map;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildRoles:Ljava/util/Map;

    iget-object v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->setupRolesSpinner(J)V

    :cond_4
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->adapter:Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;

    new-instance v1, Lf/a/o/e/i1;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/i1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V

    invoke-virtual {v0, p1, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;->configure(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;Lrx/functions/Action2;)V

    return-void

    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-virtual {p1}, Lf/a/b/f;->onBackPressed()V

    :cond_6
    return-void
.end method

.method public static create(Landroid/content/Context;J)V
    .locals 2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    const-string v1, "MEMBERS"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/stores/StoreAnalytics;->onGuildSettingsPaneViewed(Ljava/lang/String;J)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p1

    const-class p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;

    invoke-static {p0, p2, p1}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;)V

    return-void
.end method

.method private static parseUserId(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private setupRolesSpinner(J)V
    .locals 8

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildRoles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter$RoleSpinnerItem;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildRoles:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelGuildRole;

    new-instance v4, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter$RoleSpinnerItem;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/discord/utilities/guilds/RoleUtils;->getRoleColor(Lcom/discord/models/domain/ModelGuildRole;Landroid/content/Context;)I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter$RoleSpinnerItem;-><init>(Ljava/lang/Long;Ljava/lang/String;I)V

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v5

    cmp-long v3, v5, p1

    if-nez v3, :cond_0

    const/4 v3, 0x0

    aput-object v4, v0, v3

    goto :goto_0

    :cond_0
    aput-object v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    const v1, 0x7f0d00e3

    invoke-direct {p1, p2, v1, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter;-><init>(Landroid/content/Context;I[Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter$RoleSpinnerItem;)V

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->rolesSpinner:Landroid/widget/Spinner;

    invoke-virtual {p2, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->rolesSpinner:Landroid/widget/Spinner;

    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$RolesSpinnerAdapter;)V

    invoke-virtual {p2, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method


# virtual methods
.method public synthetic g(Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->parseUserId(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/discord/stores/StoreStream;->getGatewaySocket()Lcom/discord/stores/StoreGatewayConnection;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2, p1, v0}, Lcom/discord/stores/StoreGatewayConnection;->requestGuildMembers(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Z

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0282

    return v0
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->searchBox:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->nameFilterPublisher:Lrx/subjects/Subject;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lf/a/o/e/b;

    invoke-direct {v2, v1}, Lf/a/o/e/b;-><init>(Lrx/subjects/Subject;)V

    invoke-static {p0, v0, v2}, Lcom/discord/utilities/view/text/TextWatcher;->addBindedTextWatcher(Landroidx/fragment/app/Fragment;Landroid/widget/TextView;Lrx/functions/Action1;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->nameFilterPublisher:Lrx/subjects/Subject;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->searchBox:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lg0/g;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->nameFilterPublisher:Lrx/subjects/Subject;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v2, v3, v1}, Lrx/Observable;->o(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/o/e/c1;->d:Lf/a/o/e/c1;

    invoke-virtual {v0, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/b1;

    invoke-direct {v1, p0}, Lf/a/o/e/b1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildId:J

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->nameFilterPublisher:Lrx/subjects/Subject;

    iget-object v3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->roleFilterPublisher:Lrx/subjects/Subject;

    invoke-static {v0, v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model;->access$000(JLrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "observable"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/h1;

    invoke-direct {v1, p0}, Lf/a/o/e/h1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembers;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a08d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->searchBox:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f0a08d6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->recycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a08d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->rolesSpinner:Landroid/widget/Spinner;

    const v0, 0x7f0a08d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ViewFlipper;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled()Landroidx/appcompat/widget/Toolbar;

    new-instance p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->recycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p1, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-static {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->adapter:Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "GUILD_ID"

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildId:J

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->isRecreated()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->roleFilterPublisher:Lrx/subjects/Subject;

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembers;->guildId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Lg0/g;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
