.class public final Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions$Companion$show$$inlined$apply$lambda$1;
.super Lx/m/c/k;
.source "WidgetServerSettingsChannelsSortActions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions$Companion;->show(Landroidx/fragment/app/FragmentManager;Lrx/functions/Action1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $fragmentManager$inlined:Landroidx/fragment/app/FragmentManager;

.field public final synthetic $sortTypeSelectedListener$inlined:Lrx/functions/Action1;


# direct methods
.method public constructor <init>(Lrx/functions/Action1;Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions$Companion$show$$inlined$apply$lambda$1;->$sortTypeSelectedListener$inlined:Lrx/functions/Action1;

    iput-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions$Companion$show$$inlined$apply$lambda$1;->$fragmentManager$inlined:Landroidx/fragment/app/FragmentManager;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions$Companion$show$$inlined$apply$lambda$1;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions$Companion$show$$inlined$apply$lambda$1;->$sortTypeSelectedListener$inlined:Lrx/functions/Action1;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
