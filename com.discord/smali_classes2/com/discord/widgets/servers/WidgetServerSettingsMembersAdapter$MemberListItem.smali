.class public Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetServerSettingsMembersAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MemberListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;",
        "Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;",
        ">;"
    }
.end annotation


# instance fields
.field private avatar:Landroid/widget/ImageView;

.field private containerView:Landroid/view/View;

.field private lockIndicator:Landroid/widget/ImageView;

.field private overflow:Landroid/widget/ImageView;

.field private rolesListView:Lcom/discord/widgets/roles/RolesListView;

.field private userName:Lcom/discord/views/UsernameView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter;)V
    .locals 1

    const v0, 0x7f0d0281

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0673

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->containerView:Landroid/view/View;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0675

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/UsernameView;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->userName:Lcom/discord/views/UsernameView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0672

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->avatar:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0677

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/roles/RolesListView;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->rolesListView:Lcom/discord/widgets/roles/RolesListView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0674

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->lockIndicator:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0676

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->overflow:Landroid/widget/ImageView;

    return-void
.end method

.method private setupLockStatus(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->lockIndicator:Landroid/widget/ImageView;

    iget-boolean v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->isManagable:Z

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->overflow:Landroid/widget/ImageView;

    iget-boolean p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->isManagable:Z

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private setupRoles(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->rolesListView:Lcom/discord/widgets/roles/RolesListView;

    if-eqz p1, :cond_0

    move-object v1, p1

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->rolesListView:Lcom/discord/widgets/roles/RolesListView;

    const v3, 0x7f06021d

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/roles/RolesListView;->updateView(Ljava/util/List;I)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->rolesListView:Lcom/discord/widgets/roles/RolesListView;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-static {v0, p1}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->userName:Lcom/discord/views/UsernameView;

    iget-object v0, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->userDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/discord/views/UsernameView;->setUsernameText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->userName:Lcom/discord/views/UsernameView;

    iget-boolean v0, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->isBot:Z

    iget-boolean v1, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->isVerifiedBot:Z

    const v2, 0x7f120383

    invoke-virtual {p1, v0, v2, v1}, Lcom/discord/views/UsernameView;->a(ZIZ)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->avatar:Landroid/widget/ImageView;

    iget-object v0, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->avatarUrl:Ljava/lang/String;

    const v1, 0x7f07006b

    invoke-static {p1, v0, v1}, Lcom/discord/utilities/icon/IconUtils;->setIcon(Landroid/widget/ImageView;Ljava/lang/String;I)V

    iget-object p1, p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;->roles:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->setupRoles(Ljava/util/List;)V

    invoke-direct {p0, p2}, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->setupLockStatus(Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->containerView:Landroid/view/View;

    new-instance v0, Lf/a/o/e/j1;

    invoke-direct {v0, p0, p2}, Lf/a/o/e/j1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/WidgetServerSettingsMembersAdapter$MemberListItem;->onConfigure(ILcom/discord/widgets/servers/WidgetServerSettingsMembers$Model$MemberItem;)V

    return-void
.end method
