.class public Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;
.super Lcom/discord/app/AppDialog;
.source "WidgetServerSettingsTransferOwnership.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;
    }
.end annotation


# static fields
.field private static final ARG_GUILD_ID:Ljava/lang/String; = "ARG_GUILD_ID"

.field private static final ARG_USER_ID:Ljava/lang/String; = "ARG_USER_ID"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private acknowledgeCheck:Lcom/discord/views/CheckedSetting;

.field private cancel:Landroid/view/View;

.field private confirm:Landroid/view/View;

.field private mfaInput:Lcom/google/android/material/textfield/TextInputLayout;

.field private mfaWrap:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;)V
    .locals 5

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->dismiss()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->mfaWrap:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-boolean v2, p1, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;->mfaEnabled:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->cancel:Landroid/view/View;

    if-eqz v0, :cond_3

    new-instance v2, Lf/a/o/e/n2;

    invoke-direct {v2, p0}, Lf/a/o/e/n2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->confirm:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->acknowledgeCheck:Lcom/discord/views/CheckedSetting;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/views/CheckedSetting;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f12186b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/textprocessing/Parsers;->parseBoldMarkdown(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->acknowledgeCheck:Lcom/discord/views/CheckedSetting;

    invoke-virtual {v1, v0}, Lcom/discord/views/CheckedSetting;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->acknowledgeCheck:Lcom/discord/views/CheckedSetting;

    new-instance v1, Lf/a/o/e/o2;

    invoke-direct {v1, p0}, Lf/a/o/e/o2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->confirm:Landroid/view/View;

    new-instance v1, Lf/a/o/e/p2;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/p2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void
.end method

.method public static create(JJLandroidx/fragment/app/FragmentActivity;)V
    .locals 3
    .param p4    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p4, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;

    invoke-direct {v0}, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ARG_GUILD_ID"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string p0, "ARG_USER_ID"

    invoke-virtual {v1, p0, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p4}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p0

    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->TAG:Ljava/lang/String;

    invoke-virtual {v0, p0, p1}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;)V

    return-void
.end method

.method private getMfaCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->mfaInput:Lcom/google/android/material/textfield/TextInputLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->mfaInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->mfaInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public synthetic g(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->acknowledgeCheck:Lcom/discord/views/CheckedSetting;

    invoke-virtual {p1}, Lcom/discord/views/CheckedSetting;->toggle()V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->confirm:Landroid/view/View;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->acknowledgeCheck:Lcom/discord/views/CheckedSetting;

    invoke-virtual {v0}, Lcom/discord/views/CheckedSetting;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0218

    return v0
.end method

.method public synthetic h(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;Landroid/view/View;)V
    .locals 5

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p2

    iget-wide v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;->guildId:J

    new-instance v2, Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;

    iget-object p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-direct {p0}, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->getMfaCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v3, v4, p1}, Lcom/discord/restapi/RestAPIParams$TransferGuildOwnership;-><init>(JLjava/lang/String;)V

    invoke-virtual {p2, v0, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->transferGuildOwnership(JLcom/discord/restapi/RestAPIParams$TransferGuildOwnership;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lf/a/o/e/q2;

    invoke-direct {p2, p0}, Lf/a/o/e/q2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;)V

    invoke-static {p2, p0}, Lf/a/b/r;->l(Lrx/functions/Action1;Lcom/discord/app/AppDialog;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a0536

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->confirm:Landroid/view/View;

    const v0, 0x7f0a0535

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->cancel:Landroid/view/View;

    const v0, 0x7f0a0538

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->mfaWrap:Landroid/view/View;

    const v0, 0x7f0a0537

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->mfaInput:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f0a0534

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;->acknowledgeCheck:Lcom/discord/views/CheckedSetting;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 6

    invoke-super {p0}, Lcom/discord/app/AppDialog;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "ARG_USER_ID"

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership$Model;->access$000(JJ)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/s2;

    invoke-direct {v1, p0}, Lf/a/o/e/s2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsTransferOwnership;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
