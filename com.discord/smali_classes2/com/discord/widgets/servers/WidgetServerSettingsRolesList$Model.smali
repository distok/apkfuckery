.class public Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;
.super Ljava/lang/Object;
.source "WidgetServerSettingsRolesList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsRolesList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Model"
.end annotation


# instance fields
.field public final canManageRoles:Z

.field public final elevated:Z

.field public final guildId:J

.field public final guildName:Ljava/lang/String;

.field public final roleItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;ZZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/DragAndDropAdapter$Payload;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildName:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->canManageRoles:Z

    iput-boolean p5, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->elevated:Z

    iput-object p6, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->roleItems:Ljava/util/List;

    return-void
.end method

.method public static synthetic a(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/lang/Long;Ljava/util/Map;)Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;
    .locals 0

    invoke-static {p1, p0, p2, p3, p4}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->create(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/lang/Long;Ljava/util/Map;)Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic access$000(J)Lrx/Observable;
    .locals 0

    invoke-static {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->get(J)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private static create(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/lang/Long;Ljava/util/Map;)Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;)",
            "Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    if-eqz p0, :cond_5

    if-eqz p1, :cond_5

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    if-nez v2, :cond_0

    goto/16 :goto_4

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v3

    invoke-virtual/range {p0 .. p0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v5

    const-wide/32 v6, 0x10000000

    invoke-static {v6, v7, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v7

    invoke-static {v1, v7}, Lcom/discord/utilities/permissions/PermissionUtils;->isElevated(ZI)Z

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v10

    const/4 v14, 0x1

    cmp-long v12, v8, v10

    if-nez v12, :cond_1

    const/4 v15, 0x1

    goto :goto_0

    :cond_1
    const/4 v15, 0x0

    :goto_0
    invoke-static {v2, v0}, Lcom/discord/models/domain/ModelGuildRole;->getHighestRole(Ljava/util/Map;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v0

    new-instance v8, Ljava/util/ArrayList;

    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v8}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/2addr v9, v14

    invoke-direct {v2, v9}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v9, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$HelpItem;

    invoke-direct {v9, v3, v4}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$HelpItem;-><init>(J)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/discord/models/domain/ModelGuildRole;

    new-instance v13, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v10

    cmp-long v8, v3, v10

    if-nez v8, :cond_2

    const/4 v10, 0x1

    goto :goto_2

    :cond_2
    const/4 v10, 0x0

    :goto_2
    if-nez v15, :cond_3

    invoke-static {v0, v9}, Lcom/discord/models/domain/ModelGuildRole;->rankIsHigher(Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Z

    move-result v8

    if-nez v8, :cond_3

    const/4 v11, 0x1

    goto :goto_3

    :cond_3
    const/4 v11, 0x0

    :goto_3
    move-object v8, v13

    move v12, v6

    move-object v1, v13

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesListAdapter$RoleItem;-><init>(Lcom/discord/models/domain/ModelGuildRole;ZZZZ)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    new-instance v8, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;

    move-object v0, v8

    move-object v9, v2

    move-wide v1, v3

    move-object v3, v5

    move v4, v6

    move v5, v7

    move-object v6, v9

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;-><init>(JLjava/lang/String;ZZLjava/util/List;)V

    return-object v8

    :cond_5
    :goto_4
    const/4 v0, 0x0

    return-object v0
.end method

.method private static get(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/s1;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/s1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    iget-wide v3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildId:J

    iget-wide v5, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildId:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_3

    return v2

    :cond_3
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildName:Ljava/lang/String;

    iget-object v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildName:Ljava/lang/String;

    if-nez v1, :cond_4

    if-eqz v3, :cond_5

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    return v2

    :cond_5
    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->canManageRoles:Z

    iget-boolean v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->canManageRoles:Z

    if-eq v1, v3, :cond_6

    return v2

    :cond_6
    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->elevated:Z

    iget-boolean v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->elevated:Z

    if-eq v1, v3, :cond_7

    return v2

    :cond_7
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->roleItems:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->roleItems:Ljava/util/List;

    if-nez v1, :cond_8

    if-eqz p1, :cond_9

    goto :goto_1

    :cond_8
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    :goto_1
    return v2

    :cond_9
    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildId:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    const/16 v0, 0x3b

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildName:Ljava/lang/String;

    mul-int/lit8 v1, v1, 0x3b

    const/16 v3, 0x2b

    if-nez v2, :cond_0

    const/16 v2, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_0
    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x3b

    iget-boolean v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->canManageRoles:Z

    const/16 v4, 0x4f

    const/16 v5, 0x61

    if-eqz v2, :cond_1

    const/16 v2, 0x4f

    goto :goto_1

    :cond_1
    const/16 v2, 0x61

    :goto_1
    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x3b

    iget-boolean v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->elevated:Z

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/16 v4, 0x61

    :goto_2
    add-int/2addr v1, v4

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->roleItems:Ljava/util/List;

    mul-int/lit8 v1, v1, 0x3b

    if-nez v2, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_3
    add-int/2addr v1, v3

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "WidgetServerSettingsRolesList.Model(guildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->guildName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", canManageRoles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->canManageRoles:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", elevated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->elevated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", roleItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsRolesList$Model;->roleItems:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
