.class public Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;
.super Ljava/lang/Object;
.source "WidgetServerSettingsChannels.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsChannels;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Model"
.end annotation


# instance fields
.field private final canManageGuildChannels:Z

.field private final channelPermissions:Ljava/util/Map;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final channels:Ljava/util/Map;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;"
        }
    .end annotation
.end field

.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final isSorting:Z

.field public final items:Ljava/util/List;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/discord/models/domain/ModelGuild;Ljava/util/List;ZLjava/util/Map;Ljava/util/Map;)V
    .locals 1
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Ljava/util/Map;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/CategoricalDragAndDropAdapter$Payload;",
            ">;Z",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "items is marked non-null but is null"

    invoke-static {p3, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "channels is marked non-null but is null"

    invoke-static {p5, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "channelPermissions is marked non-null but is null"

    invoke-static {p6, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-boolean p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canManageGuildChannels:Z

    iput-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->items:Ljava/util/List;

    iput-boolean p4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->isSorting:Z

    iput-object p5, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channels:Ljava/util/Map;

    iput-object p6, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channelPermissions:Ljava/util/Map;

    return-void
.end method

.method public static synthetic a(Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Map;I)Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;
    .locals 0

    invoke-static/range {p0 .. p6}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->create(Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Map;I)Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic access$000(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Lcom/discord/models/domain/ModelGuild;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object p0
.end method

.method public static synthetic access$100(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->isSorting:Z

    return p0
.end method

.method public static synthetic access$200(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canManageGuildChannels:Z

    return p0
.end method

.method public static synthetic access$300(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channels:Ljava/util/Map;

    return-object p0
.end method

.method public static synthetic access$400(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channelPermissions:Ljava/util/Map;

    return-object p0
.end method

.method private static create(Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Lcom/discord/models/domain/ModelGuild;Ljava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Map;I)Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;I)",
            "Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v6, p5

    move/from16 v1, p6

    const/4 v2, 0x0

    if-eqz p0, :cond_15

    if-eqz p2, :cond_15

    if-eqz v0, :cond_15

    if-eqz p3, :cond_15

    if-nez v6, :cond_0

    goto/16 :goto_a

    :cond_0
    const/4 v3, -0x1

    const/4 v5, 0x0

    if-eq v1, v3, :cond_1

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v8

    const-wide/16 v9, 0x10

    invoke-static {v9, v10, v0, v3, v8}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    const-wide/16 v18, 0x0

    if-eqz v7, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v15, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v8, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    const/16 v20, 0x0

    const-wide/16 v21, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x1

    invoke-static/range {v20 .. v26}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->createCategoryItem(Ljava/lang/String;JIZZZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-static {}, Lcom/discord/models/domain/ModelChannel;->getSortByNameAndType()Ljava/util/Comparator;

    move-result-object v11

    move-object/from16 v12, p4

    invoke-static {v12, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    move-object v14, v11

    check-cast v14, Lcom/discord/models/domain/ModelChannel;

    invoke-static {v14, v6}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z

    move-result v11

    if-nez v11, :cond_5

    invoke-virtual {v14}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v11

    cmp-long v13, v11, v18

    if-nez v13, :cond_4

    goto :goto_2

    :cond_4
    move-object/from16 p0, v15

    goto :goto_4

    :cond_5
    :goto_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v14}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v6, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-static {v9, v10, v11}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v17

    if-nez v17, :cond_6

    if-eqz v0, :cond_7

    :cond_6
    invoke-virtual {v14}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v15, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz v7, :cond_8

    if-eqz v17, :cond_4

    :cond_8
    invoke-virtual {v14}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v14}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v21

    invoke-virtual {v14}, Lcom/discord/models/domain/ModelChannel;->getPosition()I

    move-result v16

    const/4 v13, 0x4

    if-ne v1, v13, :cond_9

    const/16 v23, 0x1

    goto :goto_3

    :cond_9
    const/16 v23, 0x0

    :goto_3
    move-object v4, v12

    move-wide/from16 v12, v21

    move-object/from16 v21, v14

    move/from16 v14, v16

    move-object/from16 p0, v15

    move/from16 v15, v23

    move/from16 v16, v0

    invoke-static/range {v11 .. v17}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->createCategoryItem(Ljava/lang/String;JIZZZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {v21 .. v21}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v8, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4
    move-object/from16 v15, p0

    goto :goto_1

    :cond_a
    move-object/from16 p0, v15

    invoke-virtual/range {p0 .. p0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    return-object v2

    :cond_b
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p3 .. p3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v9, p0

    invoke-virtual {v9, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v4, v7, v5}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->createChannelItem(Lcom/discord/models/domain/ModelChannel;ZZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;

    move-result-object v5

    if-eqz v7, :cond_c

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    :cond_c
    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyChannel()Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-static {v4, v6}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_d
    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_e

    invoke-static {v4, v6}, Lcom/discord/utilities/permissions/PermissionUtils;->hasAccess(Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;)Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    :goto_6
    move-object/from16 p0, v9

    goto :goto_5

    :cond_f
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_10
    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;

    invoke-virtual {v2}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v8, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-virtual {v2}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_11
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_12
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {v1}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_13
    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_9

    :cond_14
    new-instance v8, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    const/4 v1, 0x1

    move-object v0, v8

    move-object/from16 v2, p2

    move v4, v7

    move-object/from16 v5, p3

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;-><init>(ZLcom/discord/models/domain/ModelGuild;Ljava/util/List;ZLjava/util/Map;Ljava/util/Map;)V

    return-object v8

    :cond_15
    :goto_a
    return-object v2
.end method

.method private static createCategoryItem(Ljava/lang/String;JIZZZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;
    .locals 9

    new-instance v8, Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;

    move-object v0, v8

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$CategoryItem;-><init>(Ljava/lang/String;JIZZZ)V

    return-object v8
.end method

.method private static createChannelItem(Lcom/discord/models/domain/ModelChannel;ZZ)Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;
    .locals 7

    new-instance v6, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v3

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$ChannelItem;-><init>(Lcom/discord/models/domain/ModelChannel;ZJZ)V

    return-object v6
.end method

.method public static get(JLrx/Observable;)Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lcom/discord/stores/StoreStream;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StorePermissions;->observePermissionsForGuild(J)Lrx/Observable;

    move-result-object v2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v3

    new-instance v0, Lf/a/o/e/h;

    invoke-direct {v0, p0, p1}, Lf/a/o/e/h;-><init>(J)V

    invoke-virtual {p2, v0}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v9, Lf/a/b/q;->d:Lf/a/b/q;

    invoke-virtual {v0, v9}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v4

    invoke-static {}, Lcom/discord/stores/StoreStream;->getStoreChannelCategories()Lcom/discord/stores/StoreChannelCategories;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StoreChannelCategories;->getChannelCategories(J)Lrx/Observable;

    move-result-object v5

    invoke-static {}, Lcom/discord/stores/StoreStream;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StorePermissions;->observeChannelPermissionsForGuild(J)Lrx/Observable;

    move-result-object v6

    sget-object v8, Lf/a/o/e/i;->a:Lf/a/o/e/i;

    move-object v7, p2

    invoke-static/range {v1 .. v8}, Lrx/Observable;->e(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0, v9}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canManageGuildChannels:Z

    iget-boolean v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canManageGuildChannels:Z

    if-eq v1, v3, :cond_3

    return v2

    :cond_3
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    if-nez v1, :cond_4

    if-eqz v3, :cond_5

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v3}, Lcom/discord/models/domain/ModelGuild;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    return v2

    :cond_5
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->items:Ljava/util/List;

    if-nez v1, :cond_6

    if-eqz v3, :cond_7

    goto :goto_1

    :cond_6
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    :goto_1
    return v2

    :cond_7
    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->isSorting:Z

    iget-boolean v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->isSorting:Z

    if-eq v1, v3, :cond_8

    return v2

    :cond_8
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channels:Ljava/util/Map;

    iget-object v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channels:Ljava/util/Map;

    if-nez v1, :cond_9

    if-eqz v3, :cond_a

    goto :goto_2

    :cond_9
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    :goto_2
    return v2

    :cond_a
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channelPermissions:Ljava/util/Map;

    iget-object p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channelPermissions:Ljava/util/Map;

    if-nez v1, :cond_b

    if-eqz p1, :cond_c

    goto :goto_3

    :cond_b
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_c

    :goto_3
    return v2

    :cond_c
    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canManageGuildChannels:Z

    const/16 v1, 0x4f

    const/16 v2, 0x61

    if-eqz v0, :cond_0

    const/16 v0, 0x4f

    goto :goto_0

    :cond_0
    const/16 v0, 0x61

    :goto_0
    const/16 v3, 0x3b

    add-int/2addr v0, v3

    iget-object v4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    mul-int/lit8 v0, v0, 0x3b

    const/16 v5, 0x2b

    if-nez v4, :cond_1

    const/16 v4, 0x2b

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v4

    :goto_1
    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->items:Ljava/util/List;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v4, :cond_2

    const/16 v4, 0x2b

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x3b

    iget-boolean v4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->isSorting:Z

    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    const/16 v1, 0x61

    :goto_3
    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channels:Ljava/util/Map;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v1, :cond_4

    const/16 v1, 0x2b

    goto :goto_4

    :cond_4
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_4
    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channelPermissions:Ljava/util/Map;

    mul-int/lit8 v0, v0, 0x3b

    if-nez v1, :cond_5

    goto :goto_5

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    :goto_5
    add-int/2addr v0, v5

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "WidgetServerSettingsChannels.Model(canManageGuildChannels="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->canManageGuildChannels:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isSorting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->isSorting:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", channels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channels:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelPermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->channelPermissions:Ljava/util/Map;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->B(Ljava/lang/StringBuilder;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
