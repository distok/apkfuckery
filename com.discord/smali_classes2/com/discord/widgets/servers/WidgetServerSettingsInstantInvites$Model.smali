.class public Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;
.super Ljava/lang/Object;
.source "WidgetServerSettingsInstantInvites.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;
    }
.end annotation


# instance fields
.field public final guildId:J

.field public final guildName:Ljava/lang/String;

.field public final inviteItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildName:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->inviteItems:Ljava/util/List;

    return-void
.end method

.method public static synthetic a(Lcom/discord/models/domain/ModelGuild;Ljava/util/List;)Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;
    .locals 0

    invoke-static {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->create(Lcom/discord/models/domain/ModelGuild;Ljava/util/List;)Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic access$000(J)Lrx/Observable;
    .locals 0

    invoke-static {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->get(J)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private static create(Lcom/discord/models/domain/ModelGuild;Ljava/util/List;)Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model$InviteItem;",
            ">;)",
            "Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;-><init>(JLjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method private static get(J)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/stores/StoreStream;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getInstantInvites()Lcom/discord/stores/StoreInstantInvites;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/discord/stores/StoreInstantInvites;->get(J)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/o/e/v0;

    invoke-direct {v2, p0, p1}, Lf/a/o/e/v0;-><init>(J)V

    invoke-virtual {v1, v2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lf/a/o/e/w0;->d:Lf/a/o/e/w0;

    invoke-static {v0, p0, p1}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public canEqual(Ljava/lang/Object;)Z
    .locals 0

    instance-of p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->canEqual(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    :cond_2
    iget-wide v3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildId:J

    iget-wide v5, p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildId:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_3

    return v2

    :cond_3
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildName:Ljava/lang/String;

    iget-object v3, p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildName:Ljava/lang/String;

    if-nez v1, :cond_4

    if-eqz v3, :cond_5

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_0
    return v2

    :cond_5
    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->inviteItems:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->inviteItems:Ljava/util/List;

    if-nez v1, :cond_6

    if-eqz p1, :cond_7

    goto :goto_1

    :cond_6
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    :goto_1
    return v2

    :cond_7
    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildId:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    const/16 v0, 0x3b

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildName:Ljava/lang/String;

    mul-int/lit8 v1, v1, 0x3b

    const/16 v3, 0x2b

    if-nez v2, :cond_0

    const/16 v2, 0x2b

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_0
    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->inviteItems:Ljava/util/List;

    mul-int/lit8 v1, v1, 0x3b

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :goto_1
    add-int/2addr v1, v3

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "WidgetServerSettingsInstantInvites.Model(guildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->guildName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", inviteItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsInstantInvites$Model;->inviteItems:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
