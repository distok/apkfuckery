.class public final Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEditRole.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

.field public final synthetic this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    iput-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->$data:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;->access$getRoleName$p(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-gt v3, v0, :cond_5

    if-nez v4, :cond_0

    move v5, v3

    goto :goto_1

    :cond_0
    move v5, v0

    :goto_1
    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    invoke-static {v5, v6}, Lx/m/c/j;->compare(II)I

    move-result v5

    if-gtz v5, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    :goto_2
    if-nez v4, :cond_3

    if-nez v5, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    if-nez v5, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_5
    :goto_3
    add-int/2addr v0, v1

    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    invoke-static {v0}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;->access$getRoleName$p(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    sget-object v0, Lcom/discord/restapi/RestAPIParams$Role;->Companion:Lcom/discord/restapi/RestAPIParams$Role$Companion;

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->$data:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    invoke-virtual {v2}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->getRole()Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/discord/restapi/RestAPIParams$Role$Companion;->createWithRole(Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/restapi/RestAPIParams$Role;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/restapi/RestAPIParams$Role;->setName(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->$data:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    invoke-virtual {v2}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->getGuildId()J

    move-result-wide v2

    invoke-static {p1, v2, v3, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;->access$patchRole(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;JLcom/discord/restapi/RestAPIParams$Role;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    const/4 v0, 0x0

    invoke-static {p1, v0, v1, v0}, Lcom/discord/app/AppFragment;->hideKeyboard$default(Lcom/discord/app/AppFragment;Landroid/view/View;ILjava/lang/Object;)V

    goto :goto_5

    :cond_7
    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;

    const v0, 0x7f120816

    const/4 v1, 0x4

    invoke-static {p1, v0, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :goto_5
    return-void
.end method
