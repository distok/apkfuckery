.class public Lcom/discord/widgets/servers/WidgetServerSettingsModeration;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsModeration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;
    }
.end annotation


# static fields
.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field private static final VERIFICATION_LEVEL_HIGH_MINUTES:Ljava/lang/String; = "10"

.field private static final VERIFICATION_LEVEL_MEDIUM_MINUTES:Ljava/lang/String; = "5"


# instance fields
.field private explicitContentViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/views/CheckedSetting;",
            ">;"
        }
    .end annotation
.end field

.field private guild:Lcom/discord/models/domain/ModelGuild;

.field private radioManagerExplicit:Lcom/discord/views/RadioManager;

.field private radioManagerVerification:Lcom/discord/views/RadioManager;

.field private verificationViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/views/CheckedSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method

.method private configureExplicitContentRadio(Lcom/discord/views/CheckedSetting;I)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->isCommunityServer()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    const v0, 0x7f121864

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->b(I)V

    goto :goto_0

    :cond_0
    new-instance v0, Lf/a/o/e/m1;

    invoke-direct {v0, p0, p2}, Lf/a/o/e/m1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsModeration;I)V

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->radioManagerExplicit:Lcom/discord/views/RadioManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getExplicitContentFilter()I

    move-result v0

    if-ne v0, p2, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->radioManagerExplicit:Lcom/discord/views/RadioManager;

    invoke-virtual {p2, p1}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    :cond_1
    return-void
.end method

.method private configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;)V
    .locals 4

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;->access$000(Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;)Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureVerificationLevelRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureVerificationLevelRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureVerificationLevelRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    const/4 v3, 0x3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureVerificationLevelRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    const/4 v3, 0x4

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureVerificationLevelRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->explicitContentViews:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureExplicitContentRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->explicitContentViews:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureExplicitContentRadio(Lcom/discord/views/CheckedSetting;I)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->explicitContentViews:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureExplicitContentRadio(Lcom/discord/views/CheckedSetting;I)V

    return-void
.end method

.method private configureVerificationLevelRadio(Lcom/discord/views/CheckedSetting;I)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->isCommunityServer()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const v0, 0x7f121864

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->b(I)V

    goto :goto_0

    :cond_0
    new-instance v0, Lf/a/o/e/k1;

    invoke-direct {v0, p0, p2}, Lf/a/o/e/k1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsModeration;I)V

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->radioManagerVerification:Lcom/discord/views/RadioManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v0

    if-ne v0, p2, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->radioManagerVerification:Lcom/discord/views/RadioManager;

    invoke-virtual {p2, p1}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    :cond_1
    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/servers/WidgetServerSettingsModeration;Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;)V

    return-void
.end method

.method public static launch(Landroid/content/Context;J)V
    .locals 2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    const-string v1, "MODERATION"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/stores/StoreAnalytics;->onGuildSettingsPaneViewed(Ljava/lang/String;J)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p1

    const-class p2, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;

    invoke-static {p0, p2, p1}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method private updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)V
    .locals 1

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/utilities/rest/RestAPI;->updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lf/a/o/e/l1;

    invoke-direct {p2, p0}, Lf/a/o/e/l1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsModeration;)V

    invoke-static {p2, p0}, Lf/a/b/r;->m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method


# virtual methods
.method public synthetic g(ILandroid/view/View;)V
    .locals 2

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-static {p1}, Lcom/discord/restapi/RestAPIParams$UpdateGuild;->createForExplicitContentFilter(I)Lcom/discord/restapi/RestAPIParams$UpdateGuild;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0283

    return v0
.end method

.method public synthetic h(ILandroid/view/View;)V
    .locals 2

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-static {p1}, Lcom/discord/restapi/RestAPIParams$UpdateGuild;->createForVerificationLevel(I)Lcom/discord/restapi/RestAPIParams$UpdateGuild;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->updateGuild(JLcom/discord/restapi/RestAPIParams$UpdateGuild;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled()Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f121079

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    const v0, 0x7f0a08e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const v3, 0x7f12079d

    invoke-static {v0, v3, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;I[Ljava/lang/Object;)V

    const v0, 0x7f0a08dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-array v2, v1, [Ljava/lang/Object;

    const v3, 0x7f12078b

    invoke-static {v0, v3, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;I[Ljava/lang/Object;)V

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/discord/views/CheckedSetting;

    const v2, 0x7f0a08dd

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    aput-object v2, v0, v1

    const v2, 0x7f0a08de

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const v2, 0x7f0a08df

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    const/4 v4, 0x2

    aput-object v2, v0, v4

    const v2, 0x7f0a08e0

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    const/4 v5, 0x3

    aput-object v2, v0, v5

    const v2, 0x7f0a08e1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    const/4 v6, 0x4

    aput-object v2, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    new-array v0, v5, [Lcom/discord/views/CheckedSetting;

    const v2, 0x7f0a08d9

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    aput-object v2, v0, v1

    const v2, 0x7f0a08da

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    aput-object v2, v0, v3

    const v2, 0x7f0a08db

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    aput-object p1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->explicitContentViews:Ljava/util/List;

    new-instance p1, Lcom/discord/views/RadioManager;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    invoke-direct {p1, v0}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->radioManagerVerification:Lcom/discord/views/RadioManager;

    new-instance p1, Lcom/discord/views/RadioManager;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->explicitContentViews:Ljava/util/List;

    invoke-direct {p1, v0}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->radioManagerExplicit:Lcom/discord/views/RadioManager;

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    new-array v0, v3, [Ljava/lang/Object;

    const-string v2, "5"

    aput-object v2, v0, v1

    const v2, 0x7f121a10

    invoke-virtual {p0, v2, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->setSubtext(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsModeration;->verificationViews:Ljava/util/List;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    new-array v0, v3, [Ljava/lang/Object;

    const-string v2, "10"

    aput-object v2, v0, v1

    const v1, 0x7f121a0c

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->setSubtext(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsModeration$Model;->get(J)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/n1;

    invoke-direct {v1, p0}, Lf/a/o/e/n1;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsModeration;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
