.class public Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;
.super Landroidx/fragment/app/Fragment;
.source "WidgetServerSettingsChannelsFabMenuFragment.java"


# static fields
.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field public static final synthetic d:I


# instance fields
.field private dismissHandler:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Landroidx/fragment/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public floatingButtonMenuInitializer:Lcom/discord/utilities/views/FloatingButtonMenuInitializer;

.field private guildId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method

.method private createListener(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lf/a/o/e/s;

    invoke-direct {v0, p0, p1}, Lf/a/o/e/s;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public static show(JLandroidx/fragment/app/FragmentManager;Lrx/functions/Action0;)V
    .locals 3

    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;

    invoke-direct {v0}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    new-instance p0, Lf/a/o/e/x;

    invoke-direct {p0, p2, p3}, Lf/a/o/e/x;-><init>(Landroidx/fragment/app/FragmentManager;Lrx/functions/Action0;)V

    iput-object p0, v0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->dismissHandler:Lrx/functions/Action1;

    invoke-virtual {p2}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p0

    const p1, 0x7f01002b

    const p2, 0x7f01002c

    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/FragmentTransaction;->setCustomAnimations(II)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p0

    const p1, 0x7f0a0bd7

    const-string p2, "fab menu"

    invoke-virtual {p0, p1, v0, p2}, Landroidx/fragment/app/FragmentTransaction;->add(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p0

    invoke-virtual {p0, p2}, Landroidx/fragment/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p0

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void
.end method


# virtual methods
.method public synthetic f(Landroid/view/View$OnClickListener;Landroid/view/View;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->dismissHandler:Lrx/functions/Action1;

    invoke-interface {p1, p0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic g(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->guildId:J

    const/4 v2, 0x4

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/channels/WidgetCreateChannel;->show(Landroid/content/Context;JI)V

    return-void
.end method

.method public getFabMenuItems()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const v1, 0x7f0a0409

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lf/a/o/e/y;

    invoke-direct {v2, p0}, Lf/a/o/e/y;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;)V

    invoke-direct {p0, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->createListener(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f0a040b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lf/a/o/e/t;

    invoke-direct {v2, p0}, Lf/a/o/e/t;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;)V

    invoke-direct {p0, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->createListener(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f0a040a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lf/a/o/e/v;

    invoke-direct {v2, p0}, Lf/a/o/e/v;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;)V

    invoke-direct {p0, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->createListener(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7f0a040c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lf/a/o/e/u;

    invoke-direct {v2, p0}, Lf/a/o/e/u;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public synthetic h(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->guildId:J

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/channels/WidgetCreateChannel;->show(Landroid/content/Context;JI)V

    return-void
.end method

.method public synthetic i(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->guildId:J

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/channels/WidgetCreateChannel;->show(Landroid/content/Context;JI)V

    return-void
.end method

.method public synthetic j(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->dismissHandler:Lrx/functions/Action1;

    invoke-interface {p1, p0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic k(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->dismissHandler:Lrx/functions/Action1;

    invoke-interface {p1, p0}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Lcom/discord/utilities/views/FloatingButtonMenuInitializer;

    invoke-virtual {p0}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->getFabMenuItems()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/discord/utilities/views/FloatingButtonMenuInitializer;-><init>(Landroid/content/Context;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->floatingButtonMenuInitializer:Lcom/discord/utilities/views/FloatingButtonMenuInitializer;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const p3, 0x7f0d026c

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-wide/16 v0, 0x0

    const-string p3, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {p2, p3, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide p2

    iput-wide p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->guildId:J

    :cond_0
    new-instance p2, Lf/a/o/e/w;

    invoke-direct {p2, p0}, Lf/a/o/e/w;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p3, 0x7f0a040e

    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/widget/TableLayout;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->floatingButtonMenuInitializer:Lcom/discord/utilities/views/FloatingButtonMenuInitializer;

    invoke-virtual {v0, p3, p2}, Lcom/discord/utilities/views/FloatingButtonMenuInitializer;->initialize(Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)Lcom/discord/utilities/views/FloatingButtonMenuInitializer;

    return-object p1
.end method

.method public setDismissHandler(Lrx/functions/Action1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action1<",
            "Landroidx/fragment/app/Fragment;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->dismissHandler:Lrx/functions/Action1;

    return-void
.end method
