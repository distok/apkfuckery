.class public final Lcom/discord/widgets/servers/WidgetServerSettingsBans$configureUI$1;
.super Lx/m/c/k;
.source "WidgetServerSettingsBans.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsBans;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelBan;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $model:Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;

.field public final synthetic this$0:Lcom/discord/widgets/servers/WidgetServerSettingsBans;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsBans;Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsBans$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsBans;

    iput-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsBans$configureUI$1;->$model:Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelBan;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsBans$configureUI$1;->invoke(Lcom/discord/models/domain/ModelBan;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelBan;)V
    .locals 3

    const-string v0, "ban"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsBans$configureUI$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsBans;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsBans$configureUI$1;->$model:Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/WidgetServerSettingsBans$Model;->getGuildId()J

    move-result-wide v1

    invoke-static {v0, v1, v2, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsBans;->access$showConfirmUnbanDialog(Lcom/discord/widgets/servers/WidgetServerSettingsBans;JLcom/discord/models/domain/ModelBan;)V

    return-void
.end method
