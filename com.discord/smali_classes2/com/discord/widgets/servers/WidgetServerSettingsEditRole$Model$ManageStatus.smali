.class public final enum Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
.super Ljava/lang/Enum;
.source "WidgetServerSettingsEditRole.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ManageStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field public static final enum CAN_MANAGE_ADMIN:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field public static final enum CAN_MANAGE_CONDITIONAL:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field public static final enum LOCKED_HIGHER:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field public static final enum LOCKED_HIGHEST:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field public static final enum NO_MANAGE_ROLES_PERMISSION:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field public static final enum USER_NOT_ELEVATED:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const-string v2, "NO_MANAGE_ROLES_PERMISSION"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->NO_MANAGE_ROLES_PERMISSION:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const-string v2, "LOCKED_HIGHER"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->LOCKED_HIGHER:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const-string v2, "LOCKED_HIGHEST"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->LOCKED_HIGHEST:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const-string v2, "USER_NOT_ELEVATED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->USER_NOT_ELEVATED:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const-string v2, "CAN_MANAGE_CONDITIONAL"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_CONDITIONAL:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    const-string v2, "CAN_MANAGE_ADMIN"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_ADMIN:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->$VALUES:[Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
    .locals 1

    const-class v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->$VALUES:[Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    invoke-virtual {v0}, [Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    return-object v0
.end method
