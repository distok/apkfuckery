.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;
.super Lx/m/c/k;
.source "WidgetServerSettingsEnableCommunitySteps.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->invoke(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCurrentPage()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    invoke-static {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->access$getStepsView$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)Lcom/discord/views/steps/StepsView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/views/steps/StepsView;->b(I)V

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->access$getStepsView$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)Lcom/discord/views/steps/StepsView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getExplicitContentFilter()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getVerificationLevel()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcom/discord/views/steps/StepsView;->setIsNextButtonEnabled(Z)V

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->access$getStepsView$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)Lcom/discord/views/steps/StepsView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->isPrivacyPolicyAccepted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/steps/StepsView;->setIsDoneButtonEnabled(Z)V

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->access$getStepsView$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)Lcom/discord/views/steps/StepsView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->isLoading()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/views/steps/StepsView;->setIsLoading(Z)V

    return-void
.end method
