.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1$1;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEnableCommunityViewModel.kt"

# interfaces
.implements Lrx/functions/Func5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelGuild;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func5<",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/lang/Long;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildRole;",
        ">;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guild:Lcom/discord/models/domain/ModelGuild;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1$1;->$guild:Lcom/discord/models/domain/ModelGuild;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelChannel;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/lang/Long;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;"
        }
    .end annotation

    new-instance v7, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;

    iget-object v1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1$1;->$guild:Lcom/discord/models/domain/ModelGuild;

    const-string v0, "me"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelChannel;)V

    return-object v7
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Ljava/lang/Long;

    check-cast p3, Ljava/util/Map;

    check-cast p4, Lcom/discord/models/domain/ModelChannel;

    check-cast p5, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual/range {p0 .. p5}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1$1;->call(Lcom/discord/models/domain/ModelUser;Ljava/lang/Long;Ljava/util/Map;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelChannel;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Valid;

    move-result-object p1

    return-object p1
.end method
