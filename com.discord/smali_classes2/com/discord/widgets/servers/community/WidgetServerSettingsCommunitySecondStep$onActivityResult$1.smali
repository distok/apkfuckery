.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep$onActivityResult$1;
.super Lx/m/c/k;
.source "WidgetServerSettingsCommunitySecondStep.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Long;",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep$onActivityResult$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, v0, v1, p2, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep$onActivityResult$1;->invoke(JLjava/lang/String;I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(JLjava/lang/String;I)V
    .locals 1

    const-string p4, "channelName"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p4, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep$onActivityResult$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;

    invoke-static {p4}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;->access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    move-result-object p4

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep$onActivityResult$1$1;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep$onActivityResult$1$1;-><init>(JLjava/lang/String;)V

    invoke-virtual {p4, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;->modifyGuildConfig(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
