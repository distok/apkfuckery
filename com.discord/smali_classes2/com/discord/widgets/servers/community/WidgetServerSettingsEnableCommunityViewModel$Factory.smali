.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEnableCommunityViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J

.field private final storeChannels:Lcom/discord/stores/StoreChannels;

.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storePermissions:Lcom/discord/stores/StorePermissions;

.field private final storeUsers:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;)V
    .locals 1

    const-string v0, "storeGuilds"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeChannels"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storePermissions"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeUsers"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object p4, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storeChannels:Lcom/discord/stores/StoreChannels;

    iput-object p5, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storePermissions:Lcom/discord/stores/StorePermissions;

    iput-object p6, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storeUsers:Lcom/discord/stores/StoreUser;

    return-void
.end method

.method public synthetic constructor <init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p3

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p7, 0x4

    if-eqz p3, :cond_1

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object p4

    :cond_1
    move-object v4, p4

    and-int/lit8 p3, p7, 0x8

    if-eqz p3, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object p5

    :cond_2
    move-object v5, p5

    and-int/lit8 p3, p7, 0x10

    if-eqz p3, :cond_3

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p6

    :cond_3
    move-object v6, p6

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;)V

    return-void
.end method

.method public static final synthetic access$getGuildId$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->guildId:J

    return-wide v0
.end method

.method public static final synthetic access$getStoreChannels$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StoreChannels;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storeChannels:Lcom/discord/stores/StoreChannels;

    return-object p0
.end method

.method public static final synthetic access$getStorePermissions$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StorePermissions;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storePermissions:Lcom/discord/stores/StorePermissions;

    return-object p0
.end method

.method public static final synthetic access$getStoreUsers$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StoreUser;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storeUsers:Lcom/discord/stores/StoreUser;

    return-object p0
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iget-wide v1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "storeGuilds.observeGuild\u2026      }\n        }\n      }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    iget-wide v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->guildId:J

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;-><init>(JLrx/Observable;)V

    return-object p1
.end method
