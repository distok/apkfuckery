.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "WidgetServerSettingsCommunityOverviewViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelGuild;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelGuild;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$StoreState;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$StoreState$Invalid;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;->access$getStoreUsers$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;)Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-static {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;->access$getStorePermissions$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;)Lcom/discord/stores/StorePermissions;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-static {v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;->access$getGuildId$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StorePermissions;->observePermissionsForGuild(J)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-static {v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;->access$getStoreChannels$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;)Lcom/discord/stores/StoreChannels;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object v3

    const-wide/16 v4, 0x0

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :goto_0
    const-string v6, "guild.rulesChannelId ?: 0L"

    invoke-static {v3, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-static {v3}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;->access$getStoreChannels$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;)Lcom/discord/stores/StoreChannels;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object v6

    if-eqz v6, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :goto_1
    const-string v4, "guild.publicUpdatesChannelId ?: 0L"

    invoke-static {v6, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1$1;

    invoke-direct {v4, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory$observeStoreState$1$1;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    invoke-static {v0, v1, v2, v3, v4}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v0

    :goto_2
    return-object v0
.end method
