.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;
.super Lx/m/c/k;
.source "WidgetServerSettingsCommunityOverviewViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->saveCommunityGuildSettings(Lcom/discord/restapi/RestAPIParams$UpdateGuild;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $updateGuild:Lcom/discord/restapi/RestAPIParams$UpdateGuild;

.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;Lcom/discord/restapi/RestAPIParams$UpdateGuild;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    iput-object p2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->$updateGuild:Lcom/discord/restapi/RestAPIParams$UpdateGuild;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->invoke(Lcom/discord/models/domain/ModelGuild;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelGuild;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->$updateGuild:Lcom/discord/restapi/RestAPIParams$UpdateGuild;

    invoke-static {p1, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->access$isDisableCommunityTapped(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;Lcom/discord/restapi/RestAPIParams$UpdateGuild;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->access$handleDisableCommunityButtonState(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;Z)V

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    invoke-static {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->access$getEventSubject$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;)Lrx/subjects/PublishSubject;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$DisableCommunitySuccess;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$DisableCommunitySuccess;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$saveCommunityGuildSettings$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    invoke-static {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->access$getEventSubject$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;)Lrx/subjects/PublishSubject;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$SaveSuccess;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$SaveSuccess;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
