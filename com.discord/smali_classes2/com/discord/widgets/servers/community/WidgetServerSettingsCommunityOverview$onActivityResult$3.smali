.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$3;
.super Lx/m/c/k;
.source "WidgetServerSettingsCommunityOverview.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$3;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$3;->invoke(Ljava/lang/String;I)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;I)V
    .locals 0

    const-string p2, "locale"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$3;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    invoke-static {p2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->saveLocale(Ljava/lang/String;)V

    return-void
.end method
