.class public final Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;
.super Lcom/discord/app/AppDialog;
.source "WidgetConfirmRemoveCommunityDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"


# instance fields
.field private final cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final ok$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;

    const-string v3, "ok"

    const-string v4, "getOk()Landroid/widget/Button;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;

    const-string v6, "cancel"

    const-string v7, "getCancel()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->Companion:Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a02e1

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->ok$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02de

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;)Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    return-void
.end method

.method private final getCancel()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->cancel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getOk()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->ok$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01e0

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 11

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v10, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, v0, v10}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026iewViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->getOk()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$1;-><init>(Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->getCancel()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$2;-><init>(Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
