.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsEnableCommunitySteps.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"


# instance fields
.field private final stepsView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    const-string v3, "stepsView"

    const-string v4, "getStepsView()Lcom/discord/views/steps/StepsView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a03b6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->stepsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getStepsView$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)Lcom/discord/views/steps/StepsView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->getStepsView()Lcom/discord/views/steps/StepsView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public static final create(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;->create(Landroid/content/Context;J)V

    return-void
.end method

.method private final getStepsView()Lcom/discord/views/steps/StepsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->stepsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/steps/StepsView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01e9

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 27

    move-object/from16 v0, p0

    const-string v1, "view"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/discord/views/steps/StepsView$b$a;

    new-instance v14, Lcom/discord/views/steps/StepsView$b$a;

    const-class v3, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunity;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x7e

    move-object v2, v14

    invoke-direct/range {v2 .. v13}, Lcom/discord/views/steps/StepsView$b$a;-><init>(Ljava/lang/Class;IIILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZZZI)V

    const/4 v2, 0x0

    aput-object v14, v1, v2

    new-instance v3, Lcom/discord/views/steps/StepsView$b$a;

    const-class v16, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityFirstStep;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0xfe

    move-object v15, v3

    invoke-direct/range {v15 .. v26}, Lcom/discord/views/steps/StepsView$b$a;-><init>(Ljava/lang/Class;IIILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZZZI)V

    const/4 v4, 0x1

    aput-object v3, v1, v4

    new-instance v3, Lcom/discord/views/steps/StepsView$b$a;

    const-class v6, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunitySecondStep;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xfe

    move-object v5, v3

    invoke-direct/range {v5 .. v16}, Lcom/discord/views/steps/StepsView$b$a;-><init>(Ljava/lang/Class;IIILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZZZI)V

    const/4 v4, 0x2

    aput-object v3, v1, v4

    new-instance v3, Lcom/discord/views/steps/StepsView$b$a;

    const-class v6, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    new-instance v12, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$steps$1;

    invoke-direct {v12, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$steps$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V

    const v9, 0x7f1206b9

    const/16 v16, 0xb6

    move-object v5, v3

    invoke-direct/range {v5 .. v16}, Lcom/discord/views/steps/StepsView$b$a;-><init>(Ljava/lang/Class;IIILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZZZI)V

    const/4 v5, 0x3

    aput-object v3, v1, v5

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v3, Lcom/discord/views/steps/StepsView$d;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v5

    const-string v6, "parentFragmentManager"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v5}, Lcom/discord/views/steps/StepsView$d;-><init>(Landroidx/fragment/app/FragmentManager;)V

    iput-object v1, v3, Lcom/discord/views/steps/StepsView$d;->a:Ljava/util/List;

    invoke-virtual {v3}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->getStepsView()Lcom/discord/views/steps/StepsView;

    move-result-object v1

    new-instance v5, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$1;

    invoke-direct {v5, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V

    new-instance v6, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$2;

    invoke-direct {v6, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$2;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V

    invoke-virtual {v1, v3, v5, v6}, Lcom/discord/views/steps/StepsView;->a(Lcom/discord/views/steps/StepsView$d;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    new-instance v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$3;

    invoke-direct {v1, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBound$3;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v4, v3}, Lcom/discord/app/AppFragment;->setOnBackPressed$default(Lcom/discord/app/AppFragment;Lrx/functions/Func0;IILjava/lang/Object;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 15

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance v2, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    new-instance v13, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    move-object v4, v13

    move-wide v5, v0

    invoke-direct/range {v4 .. v12}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v2, v3, v13}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v3, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    invoke-virtual {v2, v3}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v2

    const-string v3, "ViewModelProvider(requir\u2026ityViewModel::class.java)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    iput-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    const/4 v3, 0x0

    const-string v4, "viewModel"

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v2

    const-string v5, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v6

    const-class v7, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v12, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$1;

    invoke-direct {v12, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V

    const/16 v13, 0x1e

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    sget-object v5, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$$inlined$filterIs$1;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$$inlined$filterIs$1;

    invoke-virtual {v2, v5}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    sget-object v5, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$$inlined$filterIs$2;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$$inlined$filterIs$2;

    invoke-virtual {v2, v5}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v6

    const-string v2, "filter { it is T }.map { it as T }"

    invoke-static {v6, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v7, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v12, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;

    invoke-direct {v12, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;)V

    const/16 v13, 0x1e

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;->observeEvents()Lrx/Observable;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$3;

    invoke-direct {v9, p0, v0, v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$onViewBoundOrOnResume$3;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;J)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method
