.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEnableCommunityViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelGuild;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelGuild;)Lrx/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$StoreState$Invalid;

    new-instance v0, Lg0/l/e/j;

    invoke-direct {v0, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->access$getStoreUsers$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->access$getStorePermissions$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StorePermissions;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-static {v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->access$getGuildId$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/discord/stores/StorePermissions;->observePermissionsForGuild(J)Lrx/Observable;

    move-result-object v2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-static {v3}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->access$getGuildId$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/discord/stores/StoreGuilds;->observeRoles(J)Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->access$getStoreChannels$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getRulesChannelId()Ljava/lang/Long;

    move-result-object v4

    const-wide/16 v5, 0x0

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    :goto_0
    const-string v7, "guild.rulesChannelId ?: 0L"

    invoke-static {v4, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;->access$getStoreChannels$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory;)Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPublicUpdatesChannelId()Ljava/lang/Long;

    move-result-object v7

    if-eqz v7, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    :goto_1
    const-string v5, "guild.publicUpdatesChannelId ?: 0L"

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1$1;

    invoke-direct {v6, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$Factory$observeStoreState$1$1;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    invoke-static/range {v1 .. v6}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object v0

    :goto_2
    return-object v0
.end method
