.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;
.super Ljava/lang/Object;
.source "WidgetServerSettingsCommunityThirdStep.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $guildDefaultMessageNotifications:Z

.field public final synthetic $viewState:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;

.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    iput-object p2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->$viewState:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;

    iput-boolean p3, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->$guildDefaultMessageNotifications:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->$viewState:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getDefaultMessageNotifications()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->$guildDefaultMessageNotifications:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1206c2

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    invoke-static {v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->access$getToastManager$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/utilities/view/ToastManager;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lf/a/b/p;->e(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    invoke-static {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;->modifyGuildConfig(Lkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method
