.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsCommunityOverview.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field private static final REQUEST_CODE_LOCALE:I = 0xfa4

.field private static final REQUEST_CODE_RULES_CHANNEL:I = 0xfa2

.field private static final REQUEST_CODE_UPDATES_CHANNEL:I = 0xfa3


# instance fields
.field private final disableCommunityButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final localeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rulesChannelContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final updatesChannelContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const-string v3, "rulesChannelContainer"

    const-string v4, "getRulesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const-string v6, "updatesChannelContainer"

    const-string v7, "getUpdatesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const-string v6, "localeContainer"

    const-string v7, "getLocaleContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const-string v6, "disableCommunityButton"

    const-string v7, "getDisableCommunityButton()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a02d2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->rulesChannelContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02db

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->updatesChannelContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a08b2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->localeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a08c3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->disableCommunityButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->configureValidUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;)V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$DisableCommunityLoading;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getDisableCommunityButton()Lcom/discord/views/LoadingButton;

    move-result-object v0

    check-cast p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$DisableCommunityLoading;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$DisableCommunityLoading;->isLoading()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final configureValidUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;)V
    .locals 12

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getDisableCommunityButton()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;->getRulesChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const-string v2, "requireContext().getStri\u2026ublic_no_option_selected)"

    const-string v3, "requireActivity()"

    const v4, 0x7f120bdf

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;->getRulesChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils;Landroid/content/Context;Ljava/lang/String;IZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    const-string/jumbo v5, "when (viewState.rulesCha\u2026no_option_selected)\n    }"

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;->getUpdatesChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    sget-object v1, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;->getUpdatesChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils;Landroid/content/Context;Ljava/lang/String;IZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    const-string/jumbo v2, "when (viewState.updatesC\u2026no_option_selected)\n    }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getRulesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/discord/widgets/servers/community/CommunitySelectorView;->setSubtitle(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getUpdatesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/discord/widgets/servers/community/CommunitySelectorView;->setSubtitle(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getLocaleContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getPreferredLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->getAsStringInLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "WidgetSettingsLanguage.g\u2026te.guild.preferredLocale)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/servers/community/CommunitySelectorView;->setSubtitle(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getRulesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getUpdatesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$2;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getLocaleContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$3;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->getDisableCommunityButton()Lcom/discord/views/LoadingButton;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$4;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$configureValidUI$4;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static final create(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$Companion;->create(Landroid/content/Context;J)V

    return-void
.end method

.method private final getDisableCommunityButton()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->disableCommunityButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getLocaleContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->localeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/community/CommunitySelectorView;

    return-object v0
.end method

.method private final getRulesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->rulesChannelContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/community/CommunitySelectorView;

    return-object v0
.end method

.method private final getUpdatesChannelContainer()Lcom/discord/widgets/servers/community/CommunitySelectorView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->updatesChannelContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/community/CommunitySelectorView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d026d

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;->Companion:Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Companion;

    new-instance v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$3;

    invoke-direct {v2, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$3;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)V

    invoke-virtual {v0, p1, p3, v2}, Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect$Companion;->handleResult(ILandroid/content/Intent;Lkotlin/jvm/functions/Function2;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    new-instance v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$2;

    invoke-direct {v4, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$2;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move v1, p1

    move-object v2, p3

    move-object v3, v4

    move v4, v5

    move v5, v6

    move-object v6, v7

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->handleResult$default(Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;ILandroid/content/Intent;Lkotlin/jvm/functions/Function3;ZILjava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    new-instance v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$1;

    invoke-direct {v4, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onActivityResult$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move v1, p1

    move-object v2, p3

    move-object v3, v4

    move v4, v5

    move v5, v6

    move-object v6, v7

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->handleResult$default(Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;ILandroid/content/Intent;Lkotlin/jvm/functions/Function3;ZILjava/lang/Object;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0xfa2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onViewBoundOrOnResume()V
    .locals 15

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    const v0, 0x7f120ada

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance v2, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    new-instance v13, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    move-object v4, v13

    move-wide v5, v0

    invoke-direct/range {v4 .. v12}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Factory;-><init>(JLcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v2, v3, v13}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v3, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    invoke-virtual {v2, v3}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v2

    const-string v3, "ViewModelProvider(\n     \u2026iewViewModel::class.java)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    iput-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    const-string v3, "viewModel"

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v2

    const-string v5, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x2

    invoke-static {v2, p0, v4, v5, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v6

    const-class v7, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v12, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$1;

    invoke-direct {v12, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;)V

    const/16 v13, 0x1e

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->observeEvents()Lrx/Observable;

    move-result-object v2

    invoke-static {v2, p0, v4, v5, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v6

    const-class v7, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v12, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;

    invoke-direct {v12, p0, v0, v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;J)V

    const/16 v13, 0x1e

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4
.end method
