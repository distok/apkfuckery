.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;
.super Lx/m/c/k;
.source "WidgetServerSettingsCommunityOverview.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    iput-wide p2, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->$guildId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->invoke(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$DisableCommunitySuccess;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$DisableCommunitySuccess;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    sget-object p1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "requireActivity()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->$guildId:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunitySteps$Companion;->create(Landroid/content/Context;J)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$SaveSuccess;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$SaveSuccess;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const v0, 0x7f12166a

    invoke-static {p1, v0, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$Error;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel$Event$Error;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverview;

    const v0, 0x7f120be0

    invoke-static {p1, v0, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_2
    :goto_0
    return-void
.end method
