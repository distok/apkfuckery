.class public final Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsCommunityThirdStep.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;


# instance fields
.field private final guidelinesSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guidelinesTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationsSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final permissionsSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private toastManager:Lcom/discord/utilities/view/ToastManager;

.field private viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    const-string v3, "notificationsSwitch"

    const-string v4, "getNotificationsSwitch()Lcom/discord/views/CheckedSetting;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    const-string v6, "permissionsSwitch"

    const-string v7, "getPermissionsSwitch()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    const-string v6, "guidelinesSwitch"

    const-string v7, "getGuidelinesSwitch()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    const-string v6, "guidelinesTextView"

    const-string v7, "getGuidelinesTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a02d7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->notificationsSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02d6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->permissionsSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02d3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->guidelinesSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02d5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->guidelinesTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/utilities/view/ToastManager;

    invoke-direct {v0}, Lcom/discord/utilities/view/ToastManager;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->toastManager:Lcom/discord/utilities/view/ToastManager;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V

    return-void
.end method

.method public static final synthetic access$getGuidelinesSwitch$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getGuidelinesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getNotificationsSwitch$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getNotificationsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPermissionsSwitch$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getPermissionsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getToastManager$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/utilities/view/ToastManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->toastManager:Lcom/discord/utilities/view/ToastManager;

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setToastManager$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;Lcom/discord/utilities/view/ToastManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->toastManager:Lcom/discord/utilities/view/ToastManager;

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;)V
    .locals 9

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getNotificationsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getDefaultMessageNotifications()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getPermissionsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getEveryonePermissions()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getGuidelinesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->isPrivacyPolicyAccepted()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getDefaultMessageNotifications()I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-wide/32 v3, -0x1002203f

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getRoles()Ljava/util/Map;

    move-result-object v5

    const/4 v6, 0x0

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_1

    :cond_1
    move-object v7, v6

    :goto_1
    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelGuildRole;

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v7

    goto :goto_2

    :cond_2
    const-wide/16 v7, 0x0

    :goto_2
    and-long/2addr v3, v7

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getRoles()Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;->getCommunityGuildConfig()Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$CommunityGuildConfig;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    :cond_3
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelGuildRole;

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v5

    cmp-long v7, v3, v5

    if-nez v7, :cond_4

    const/4 v1, 0x1

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getNotificationsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;Z)V

    invoke-virtual {v2, v3}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getPermissionsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$2;

    invoke-direct {v2, p0, p1, v1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$2;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel$ViewState$Loaded;Z)V

    invoke-virtual {v0, v2}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getGuidelinesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$configureUI$3;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)V

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static final create(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->Companion:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$Companion;->create(Landroid/content/Context;)V

    return-void
.end method

.method private final getCommunityGuidelinesText()Ljava/lang/CharSequence;
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Lf/a/b/g;->a:Lf/a/b/g;

    const/4 v2, 0x0

    const-wide v3, 0x53d3d0e920L

    invoke-virtual {v1, v3, v4, v2}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const v1, 0x7f1204e7

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "getString(R.string.commu\u2026UNITY_SERVER_GUIDELINES))"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "requireContext()"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private final getGuidelinesSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->guidelinesSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getGuidelinesTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->guidelinesTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNotificationsSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->notificationsSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getPermissionsSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->permissionsSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0270

    return v0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->toastManager:Lcom/discord/utilities/view/ToastManager;

    invoke-virtual {v0}, Lcom/discord/utilities/view/ToastManager;->close()V

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    const-class v1, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(requir\u2026ityViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    iput-object v0, p0, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->viewModel:Lcom/discord/widgets/servers/community/WidgetServerSettingsEnableCommunityViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$$inlined$filterIs$1;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$$inlined$filterIs$1;

    invoke-virtual {v0, v2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$$inlined$filterIs$2;->INSTANCE:Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$$inlined$filterIs$2;

    invoke-virtual {v0, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    const-string v0, "filter { it is T }.map { it as T }"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v4, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getNotificationsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v10, 0x7f06004a

    const-string v11, "it"

    if-eqz v2, :cond_0

    invoke-static {v2, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f1206bd

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getString(R.string.enabl\u2026tifications_label_mobile)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v10}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x38

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {v0, v2}, Lcom/discord/views/CheckedSetting;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getPermissionsSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f1206c0

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v1, "getString(R.string.enabl\u2026_permission_label_mobile)"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v10}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x38

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_1
    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getGuidelinesTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityThirdStep;->getCommunityGuidelinesText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_2
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
