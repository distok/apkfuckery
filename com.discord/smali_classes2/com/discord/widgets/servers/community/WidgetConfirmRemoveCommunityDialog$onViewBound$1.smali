.class public final Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$1;
.super Ljava/lang/Object;
.source "WidgetConfirmRemoveCommunityDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$1;->this$0:Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$1;->this$0:Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;

    invoke-virtual {p1}, Lcom/discord/app/AppDialog;->dismiss()V

    iget-object p1, p0, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog$onViewBound$1;->this$0:Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;

    invoke-static {p1}, Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;->access$getViewModel$p(Lcom/discord/widgets/servers/community/WidgetConfirmRemoveCommunityDialog;)Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/servers/community/WidgetServerSettingsCommunityOverviewViewModel;->disableCommunity()V

    return-void
.end method
