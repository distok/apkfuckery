.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;
.source "WidgetCommunityGatingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

.field private final enableConfirmButton:Z

.field private final isConfirmButtonLoading:Z

.field private final isGatingAccepted:Z

.field private final isRulesListVisible:Z

.field private final ruleItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;",
            ">;"
        }
    .end annotation
.end field

.field private final verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

.field private final verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;


# direct methods
.method public constructor <init>(ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ",
            "Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;",
            "Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;",
            ">;",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;",
            ")V"
        }
    .end annotation

    const-string v0, "verificationState"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ruleItems"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    iput-boolean p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    iput-boolean p3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    iput-boolean p4, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    iput-object p5, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    iput-object p6, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    iput-object p7, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    iput-object p8, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move p1, v2

    move p2, v3

    move p3, v4

    move p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->copy(ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    return v0
.end method

.method public final component5()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    return-object v0
.end method

.method public final component6()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    return-object v0
.end method

.method public final component7()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    return-object v0
.end method

.method public final component8()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    return-object v0
.end method

.method public final copy(ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZZ",
            "Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;",
            "Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;",
            ">;",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;",
            ")",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;"
        }
    .end annotation

    const-string v0, "verificationState"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ruleItems"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;-><init>(ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    iget-boolean v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    iget-boolean v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    iget-boolean v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    iget-object p1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCommunityGatingConfig()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    return-object v0
.end method

.method public final getEnableConfirmButton()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    return v0
.end method

.method public final getRuleItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    return-object v0
.end method

.method public final getVerificationState()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    return-object v0
.end method

.method public final getVerificationType()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    return v0
.end method

.method public final isConfirmButtonLoading()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    return v0
.end method

.method public final isGatingAccepted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    return v0
.end method

.method public final isRulesListVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Loaded(isRulesListVisible="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isConfirmButtonLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isGatingAccepted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", enableConfirmButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->enableConfirmButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", verificationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationType:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", verificationState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->verificationState:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ruleItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->ruleItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", communityGatingConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->communityGatingConfig:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
