.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;
.super Ljava/lang/Object;
.source "WidgetCommunityGatingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommunityGatingConfig"
.end annotation


# instance fields
.field private final formFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;"
        }
    .end annotation
.end field

.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final guildIcon:Ljava/lang/String;

.field private final guildId:Ljava/lang/Long;

.field private final version:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    iput-object p3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    iput-object p5, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->copy(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;"
        }
    .end annotation

    new-instance v6, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;-><init>(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFormFields()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    return-object v0
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getGuildIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "CommunityGatingConfig(guild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->guildIcon:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", formFields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->formFields:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->version:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
