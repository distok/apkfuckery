.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;
.super Lf/a/b/l0;
.source "WidgetCommunityGatingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;,
        Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;,
        Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;,
        Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;,
        Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final guildId:J

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeGuildGating:Lcom/discord/stores/StoreGuildGating;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuildGating;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;J)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreGuildGating;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;",
            ">;J)V"
        }
    .end annotation

    const-string v0, "storeGuildGating"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeObservable"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->storeGuildGating:Lcom/discord/stores/StoreGuildGating;

    iput-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object p3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->storeObservable:Lrx/Observable;

    iput-wide p4, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->guildId:J

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    const/4 p3, 0x0

    const/4 v0, 0x2

    invoke-static {p2, p0, p3, v0, p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    new-instance v7, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$1;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p1, p4, p5}, Lcom/discord/stores/StoreGuildGating;->fetchGating(J)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreGuildGating;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;JILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGuildGating()Lcom/discord/stores/StoreGuildGating;

    move-result-object p1

    :cond_0
    move-object v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p2

    :cond_1
    move-object v2, p2

    move-object v0, p0

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;-><init>(Lcom/discord/stores/StoreGuildGating;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;J)V

    return-void
.end method

.method public static final synthetic access$getEventSubject$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$handleGuildUpdateError(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->handleGuildUpdateError()V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->handleStoreState(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;)V

    return-void
.end method

.method public static final synthetic access$updateViewState(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;)V
    .locals 0

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final createRuleItems(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMemberVerificationForm$FormField;->getValues()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    :goto_1
    if-ge v0, v2, :cond_2

    new-instance v3, Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v4, v0}, Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter$CommunityGatingRulesItem;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v4

    goto :goto_1

    :cond_2
    return-object v1

    :cond_3
    :goto_2
    sget-object p1, Lx/h/l;->d:Lx/h/l;

    return-object p1
.end method

.method private final handleGuildUpdateError()V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xfd

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Error;->INSTANCE:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Error;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;)V
    .locals 19
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->getGuildGatingData()Lcom/discord/models/domain/guild/ModelGatingData;

    move-result-object v2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/models/domain/guild/ModelGatingData;->getFetchState()Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    move-result-object v5

    goto :goto_0

    :cond_0
    move-object v5, v4

    :goto_0
    if-nez v5, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    if-eqz v5, :cond_11

    const/4 v6, 0x2

    if-eq v5, v6, :cond_2

    :goto_1
    sget-object v7, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    new-instance v9, Ljava/lang/IllegalStateException;

    const-string v1, "unsupported state while fetching data for guildId: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->guildId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v9, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    const-string v8, "unsupported state while fetching store state for community gating"

    invoke-static/range {v7 .. v12}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Invalid;

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto/16 :goto_e

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/models/domain/guild/ModelGatingData;->getData()Lcom/discord/models/domain/ModelMemberVerificationForm;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelMemberVerificationForm;->getFormFields()Ljava/util/List;

    move-result-object v5

    move-object v8, v5

    goto :goto_2

    :cond_3
    move-object v8, v4

    :goto_2
    const/4 v5, 0x4

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v6

    if-eq v6, v5, :cond_4

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v6, 0x1

    :goto_4
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getVerificationLevel()I

    move-result v7

    if-ne v7, v5, :cond_6

    const/4 v5, 0x1

    goto :goto_5

    :cond_6
    const/4 v5, 0x0

    :goto_5
    sget-object v7, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->NONE:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    if-eqz v8, :cond_8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_7

    goto :goto_6

    :cond_7
    const/4 v9, 0x0

    const/4 v13, 0x0

    goto :goto_7

    :cond_8
    :goto_6
    const/4 v9, 0x1

    const/4 v13, 0x1

    :goto_7
    if-eqz v6, :cond_9

    sget-object v5, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;->EMAIL:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isVerified()Z

    move-result v1

    if-nez v1, :cond_c

    sget-object v1, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->PENDING:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    goto :goto_8

    :cond_9
    if-eqz v5, :cond_b

    sget-object v5, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;->PHONE:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->hasPhone()Z

    move-result v1

    if-nez v1, :cond_c

    :cond_a
    sget-object v1, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->PENDING:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    :goto_8
    move-object v15, v5

    goto :goto_9

    :cond_b
    move-object v5, v4

    :cond_c
    move-object v15, v5

    move-object v1, v7

    :goto_9
    if-eqz v13, :cond_d

    if-ne v1, v7, :cond_d

    const/4 v5, 0x1

    const/4 v14, 0x1

    goto :goto_a

    :cond_d
    const/4 v5, 0x0

    const/4 v14, 0x0

    :goto_a
    invoke-direct {v0, v8}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->createRuleItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v17

    new-instance v18, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    iget-wide v5, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->guildId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v6

    goto :goto_b

    :cond_e
    move-object v6, v4

    :goto_b
    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/discord/models/domain/guild/ModelGatingData;->getData()Lcom/discord/models/domain/ModelMemberVerificationForm;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelMemberVerificationForm;->getVersion()Ljava/lang/String;

    move-result-object v2

    move-object v7, v2

    goto :goto_c

    :cond_f
    move-object v7, v4

    :goto_c
    move-object/from16 v2, v18

    move-object v4, v5

    move-object v5, v6

    move-object v6, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;-><init>(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v8, :cond_10

    invoke-interface {v8}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_10

    const/4 v3, 0x1

    const/4 v11, 0x1

    goto :goto_d

    :cond_10
    const/4 v3, 0x0

    const/4 v11, 0x0

    :goto_d
    const/4 v12, 0x0

    move-object v10, v2

    move-object/from16 v16, v1

    invoke-direct/range {v10 .. v18}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;-><init>(ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;)V

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_e

    :cond_11
    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loading;

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_e
    return-void
.end method


# virtual methods
.method public final confirmCommunityGating()V
    .locals 14

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xfd

    const/4 v13, 0x0

    move-object v3, v0

    invoke-static/range {v3 .. v13}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-result-object v1

    invoke-virtual {p0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->guildId:J

    sget-object v5, Lcom/discord/restapi/RestAPIParams$CommunityGating;->Companion:Lcom/discord/restapi/RestAPIParams$CommunityGating$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getCommunityGatingConfig()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->getVersion()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_1
    move-object v6, v2

    :goto_0
    invoke-virtual {v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getCommunityGatingConfig()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->getFormFields()Ljava/util/List;

    move-result-object v7

    goto :goto_1

    :cond_2
    move-object v7, v2

    :goto_1
    invoke-virtual {v5, v6, v7}, Lcom/discord/restapi/RestAPIParams$CommunityGating$Companion;->createFromMemberVerificationForm(Ljava/lang/String;Ljava/util/List;)Lcom/discord/restapi/RestAPIParams$CommunityGating;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, Lcom/discord/utilities/rest/RestAPI;->confirmCommunityGating(JLcom/discord/restapi/RestAPIParams$CommunityGating;)Lrx/Observable;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v1, v3, v4, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const/4 v3, 0x2

    invoke-static {v1, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$1;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;)V

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;

    invoke-direct {v10, p0, v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;)V

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public final confirmVerified()V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->VERIFIED:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xdf

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final updateRulesApproval(Z)V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getVerificationState()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    move-result-object v0

    sget-object v4, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->NONE:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    if-ne v0, v4, :cond_1

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf3

    const/4 v11, 0x0

    move v4, p1

    invoke-static/range {v1 .. v11}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method
