.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;
.super Ljava/lang/Object;
.source "WidgetCommunityGatingSuccessDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final enqueue(Ljava/lang/String;)V
    .locals 16

    move-object/from16 v1, p1

    const-string v0, "guildName"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    const-class v0, Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    new-instance v12, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion$enqueue$communityGatingSuccessDialogNotice$1;

    invoke-direct {v12, v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion$enqueue$communityGatingSuccessDialogNotice$1;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const/16 v13, 0x16

    const/4 v14, 0x0

    move-object v0, v15

    invoke-direct/range {v0 .. v14}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    invoke-virtual {v0, v15}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    return-void
.end method
