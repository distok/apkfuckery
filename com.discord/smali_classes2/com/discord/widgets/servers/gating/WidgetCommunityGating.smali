.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGating;
.super Lcom/discord/app/AppFragment;
.source "WidgetCommunityGating.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"

.field private static final VERIFICATION_REQUEST_CODE:I = 0xfa2


# instance fields
.field private final avatarView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final bannerImage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final confirm$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rootView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private rulesAdapter:Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter;

.field private final rulesContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rulesHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rulesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rulesSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final verificationContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xa

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v3, "rulesRecycler"

    const-string v4, "getRulesRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "rootView"

    const-string v7, "getRootView()Landroidx/core/widget/NestedScrollView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "dimmer"

    const-string v7, "getDimmer()Lcom/discord/utilities/dimmer/DimmerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "confirm"

    const-string v7, "getConfirm()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "rulesSwitch"

    const-string v7, "getRulesSwitch()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "bannerImage"

    const-string v7, "getBannerImage()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "avatarView"

    const-string v7, "getAvatarView()Lcom/discord/views/CommunityGatingAvatarView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "verificationContainer"

    const-string v7, "getVerificationContainer()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "rulesHeader"

    const-string v7, "getRulesHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const-string v6, "rulesContainer"

    const-string v7, "getRulesContainer()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->Companion:Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a02bd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02be

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rootView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a035a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02ba

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->confirm$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02b9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02b3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->bannerImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02b5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->avatarView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02c1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->verificationContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02bc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02bb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->configureUI(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getRulesSwitch$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    return-void
.end method

.method private final configureLoadedUI(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;)V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRootView()Landroidx/core/widget/NestedScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isGatingAccepted()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getConfirm()Lcom/discord/views/LoadingButton;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isConfirmButtonLoading()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v1, v2, v3}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getConfirm()Lcom/discord/views/LoadingButton;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getEnableConfirmButton()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/discord/views/LoadingButton;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible()Z

    move-result v2

    const/16 v4, 0x8

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesContainer()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->isRulesListVisible()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getBannerImage()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getCommunityGatingConfig()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v2

    goto :goto_3

    :cond_3
    move-object v2, v3

    :goto_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "resources"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    if-eqz v4, :cond_4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_4

    :cond_4
    move-object v4, v3

    :goto_4
    invoke-virtual {v1, v2, v4}, Lcom/discord/utilities/icon/IconUtils;->getGuildSplashUrl(Lcom/discord/models/domain/ModelGuild;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getAvatarView()Lcom/discord/views/CommunityGatingAvatarView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getCommunityGatingConfig()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    goto :goto_5

    :cond_5
    move-object v1, v3

    :goto_5
    invoke-virtual {v0, v1}, Lcom/discord/views/CommunityGatingAvatarView;->a(Lcom/discord/models/domain/ModelGuild;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesAdapter:Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getRuleItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getVerificationState()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getVerificationType()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->configureVerificationBanner(Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;)V

    return-void

    :cond_6
    const-string p1, "rulesAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method

.method private final configureUI(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;)V
    .locals 4

    instance-of v0, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Invalid;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->configureLoadedUI(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loading;

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRootView()Landroidx/core/widget/NestedScrollView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final configureVerificationBanner(Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationType;)V
    .locals 5

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getVerificationContainer()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->NONE:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-nez p2, :cond_2

    goto :goto_4

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    if-eqz p2, :cond_5

    if-eq p2, v2, :cond_3

    goto :goto_4

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getVerificationContainer()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;

    move-result-object p2

    const v0, 0x7f0803d1

    const v1, 0x7f120ff5

    sget-object v4, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->VERIFIED:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    if-ne p1, v4, :cond_4

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    new-instance p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$configureVerificationBanner$2;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$configureVerificationBanner$2;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V

    invoke-virtual {p2, v0, v1, v2, p1}, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;->configure(IIZLkotlin/jvm/functions/Function0;)V

    goto :goto_4

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getVerificationContainer()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;

    move-result-object p2

    const v0, 0x7f0802e4

    const v1, 0x7f120ff1

    sget-object v4, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;->VERIFIED:Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;

    if-ne p1, v4, :cond_6

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    new-instance p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$configureVerificationBanner$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$configureVerificationBanner$1;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V

    invoke-virtual {p2, v0, v1, v2, p1}, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;->configure(IIZLkotlin/jvm/functions/Function0;)V

    :goto_4
    return-void
.end method

.method public static final create(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->Companion:Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$Companion;->create(Landroid/content/Context;J)V

    return-void
.end method

.method private final getAvatarView()Lcom/discord/views/CommunityGatingAvatarView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->avatarView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CommunityGatingAvatarView;

    return-object v0
.end method

.method private final getBannerImage()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->bannerImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getConfirm()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->confirm$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getDimmer()Lcom/discord/utilities/dimmer/DimmerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    return-object v0
.end method

.method private final getRootView()Landroidx/core/widget/NestedScrollView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rootView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/core/widget/NestedScrollView;

    return-object v0
.end method

.method private final getRulesContainer()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getRulesHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getRulesRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getRulesSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getVerificationContainer()Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->verificationContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/gating/CommunityGatingVerificationView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01dd

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 p3, -0x1

    if-ne p2, p3, :cond_2

    const/16 p2, 0xfa2

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->confirmVerified()V

    goto :goto_0

    :cond_1
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_2
    :goto_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v2, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;

    invoke-direct {v2, v0, v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;-><init>(J)V

    invoke-direct {p1, p0, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(this, \u2026ewModel::class.java\n    )"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    new-instance p1, Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesAdapter:Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->rulesAdapter:Lcom/discord/widgets/servers/gating/CommunityGatingRulesAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_0
    const-string p1, "rulesAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    const/4 v1, 0x0

    const-string v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->viewModel:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getConfirm()Lcom/discord/views/LoadingButton;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$3;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->getRulesSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$4;

    invoke-direct {v1, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$4;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
