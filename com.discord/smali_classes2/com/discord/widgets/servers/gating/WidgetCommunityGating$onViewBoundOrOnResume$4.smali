.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$4;
.super Ljava/lang/Object;
.source "WidgetCommunityGating.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$4;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$4;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    invoke-static {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->access$getViewModel$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$4;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    invoke-static {v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->access$getRulesSwitch$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/views/CheckedSetting;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->updateRulesApproval(Z)V

    return-void
.end method
