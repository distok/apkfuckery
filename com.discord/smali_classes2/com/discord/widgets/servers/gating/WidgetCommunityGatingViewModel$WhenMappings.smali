.class public final synthetic Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->values()[Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const/4 v0, 0x3

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->FETCHING:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v1, Lcom/discord/models/domain/guild/CommunityGatingFetchStates;->SUCCEEDED:Lcom/discord/models/domain/guild/CommunityGatingFetchStates;

    const/4 v1, 0x2

    aput v1, v0, v1

    return-void
.end method
