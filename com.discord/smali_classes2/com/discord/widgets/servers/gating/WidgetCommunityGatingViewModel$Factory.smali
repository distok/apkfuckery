.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetCommunityGatingViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;->guildId:J

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    invoke-virtual {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v4

    iget-wide v5, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;->guildId:J

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;-><init>(Lcom/discord/stores/StoreGuildGating;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public final observeStores()Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreUser;->observeMe(Z)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    iget-wide v3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;->guildId:J

    invoke-virtual {v2, v3, v4}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildGating()Lcom/discord/stores/StoreGuildGating;

    move-result-object v0

    iget-wide v3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory;->guildId:J

    invoke-virtual {v0, v3, v4}, Lcom/discord/stores/StoreGuildGating;->observeMemberVerificationForm(J)Lrx/Observable;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Factory$observeStores$1;

    invoke-static {v1, v2, v0, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n            .\u2026          )\n            }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
