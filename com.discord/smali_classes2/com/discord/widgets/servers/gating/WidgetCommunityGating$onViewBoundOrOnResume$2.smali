.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;
.super Lx/m/c/k;
.source "WidgetCommunityGating.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGating;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGating;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;->invoke(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Success;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Success;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Success;->getGuildName()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    sget-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->Companion:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;->enqueue(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    instance-of p1, p1, Ljava/lang/Error;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGating$onViewBoundOrOnResume$2;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGating;

    const v0, 0x7f120be0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {p1, v0, v1, v2}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_1
    :goto_0
    return-void
.end method
