.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;
.super Ljava/lang/Object;
.source "WidgetCommunityGatingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

.field private final me:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/guild/ModelGatingData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/guild/ModelGatingData;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->copy(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/guild/ModelGatingData;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/guild/ModelGatingData;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/guild/ModelGatingData;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;
    .locals 1

    new-instance v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/guild/ModelGatingData;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    iget-object p1, p1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getGuildGatingData()Lcom/discord/models/domain/guild/ModelGatingData;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    return-object v0
.end method

.method public final getMe()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/guild/ModelGatingData;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(me="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->me:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildGatingData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$StoreState;->guildGatingData:Lcom/discord/models/domain/guild/ModelGatingData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
