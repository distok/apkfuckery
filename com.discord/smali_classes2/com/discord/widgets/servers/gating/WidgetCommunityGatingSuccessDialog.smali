.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;
.super Lcom/discord/app/AppDialog;
.source "WidgetCommunityGatingSuccessDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;

.field private static final INTENT_EXTRA_GUILD_NAME:Ljava/lang/String; = "INTENT_EXTRA_GUILD_NAME"


# instance fields
.field private final successButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final successHeader$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;

    const-string v3, "successHeader"

    const-string v4, "getSuccessHeader()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;

    const-string v6, "successButton"

    const-string v7, "getSuccessButton()Lcom/google/android/material/button/MaterialButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->Companion:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a02b6

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->successHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02b4

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->successButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method private final getSuccessButton()Lcom/google/android/material/button/MaterialButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->successButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    return-object v0
.end method

.method private final getSuccessHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->successHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01df

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/discord/app/AppDialog;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "INTENT_EXTRA_GUILD_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->getSuccessHeader()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const p1, 0x7f120f7c

    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;->getSuccessButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog$onViewBound$1;-><init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingSuccessDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
