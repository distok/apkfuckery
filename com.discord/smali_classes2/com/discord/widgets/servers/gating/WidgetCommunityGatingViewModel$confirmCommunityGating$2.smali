.class public final Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;
.super Lx/m/c/k;
.source "WidgetCommunityGatingViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->confirmCommunityGating()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Void;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $currentViewState:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

.field public final synthetic this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    iput-object p2, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->$currentViewState:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->invoke(Ljava/lang/Void;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Void;)V
    .locals 11

    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    iget-object v0, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->$currentViewState:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xfd

    const/4 v10, 0x0

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;ZZZZLcom/discord/widgets/servers/gating/CommunityGatingVerificationType;Lcom/discord/widgets/servers/gating/CommunityGatingVerificationState;Ljava/util/List;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;ILjava/lang/Object;)Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->access$updateViewState(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->this$0:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;

    invoke-static {p1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;->access$getEventSubject$p(Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel;)Lrx/subjects/PublishSubject;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Success;

    iget-object v1, p0, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$confirmCommunityGating$2;->$currentViewState:Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$ViewState$Loaded;->getCommunityGatingConfig()Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$CommunityGatingConfig;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/gating/WidgetCommunityGatingViewModel$Event$Success;-><init>(Ljava/lang/String;)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method
