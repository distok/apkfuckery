.class public final Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;
.super Ljava/lang/Object;
.source "WidgetServerSettingsOverview.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsOverview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$VoiceRegion;,
        Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$Companion;


# instance fields
.field private final afkChannelModel:Lcom/discord/models/domain/ModelChannel;

.field private final approximateMemberCount:Ljava/lang/Integer;

.field private final canManage:Z

.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final isAboveNotifyAllSize:Z

.field private final isOwner:Z

.field private final selectedVoiceRegionName:Ljava/lang/String;

.field private final systemChannelModel:Lcom/discord/models/domain/ModelChannel;

.field private final voiceRegions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$VoiceRegion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->Companion:Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Ljava/util/List;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelVoiceRegion;",
            ">;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;",
            ")V"
        }
    .end annotation

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "me"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "regions"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    iput-object p4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->afkChannelModel:Lcom/discord/models/domain/ModelChannel;

    iput-object p5, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->systemChannelModel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide p4

    invoke-virtual {p1, p4, p5}, Lcom/discord/models/domain/ModelGuild;->isOwner(J)Z

    move-result p4

    iput-boolean p4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->isOwner:Z

    const/4 p5, 0x0

    if-eqz p7, :cond_0

    invoke-virtual {p7}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getData()Lcom/discord/models/domain/ModelGuildPreview;

    move-result-object p7

    if-eqz p7, :cond_0

    invoke-virtual {p7}, Lcom/discord/models/domain/ModelGuildPreview;->getApproximateMemberCount()Ljava/lang/Integer;

    move-result-object p7

    goto :goto_0

    :cond_0
    move-object p7, p5

    :goto_0
    iput-object p7, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->approximateMemberCount:Ljava/lang/Integer;

    const/4 p7, 0x0

    const/4 v0, 0x1

    if-nez p4, :cond_2

    const-wide/16 v1, 0x20

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result p2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result p1

    invoke-static {v1, v2, p6, p2, p1}, Lcom/discord/utilities/permissions/PermissionUtils;->canAndIsElevated(JLjava/lang/Long;ZI)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    iput-boolean p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->canManage:Z

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    move-object p4, p2

    check-cast p4, Lcom/discord/models/domain/ModelVoiceRegion;

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelVoiceRegion;->getId()Ljava/lang/String;

    move-result-object p4

    iget-object p6, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {p6}, Lcom/discord/models/domain/ModelGuild;->getRegion()Ljava/lang/String;

    move-result-object p6

    invoke-static {p4, p6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p4

    if-eqz p4, :cond_3

    goto :goto_3

    :cond_4
    move-object p2, p5

    :goto_3
    check-cast p2, Lcom/discord/models/domain/ModelVoiceRegion;

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoiceRegion;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    :cond_5
    move-object p1, p5

    :goto_4
    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->selectedVoiceRegionName:Ljava/lang/String;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_6
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/models/domain/ModelVoiceRegion;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelVoiceRegion;->isDeprecated()Z

    move-result p4

    if-nez p4, :cond_7

    new-instance p4, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$VoiceRegion;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelVoiceRegion;->getId()Ljava/lang/String;

    move-result-object p6

    const-string v1, "it.id"

    invoke-static {p6, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelVoiceRegion;->getName()Ljava/lang/String;

    move-result-object p3

    const-string v1, "it.name"

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p4, p6, p3}, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$VoiceRegion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    move-object p4, p5

    :goto_6
    if-eqz p4, :cond_6

    invoke-interface {p1, p4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_8
    sget-object p2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    const-string p3, "java.lang.String.CASE_INSENSITIVE_ORDER"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$$special$$inlined$compareBy$1;

    invoke-direct {p3, p2}, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$$special$$inlined$compareBy$1;-><init>(Ljava/util/Comparator;)V

    invoke-static {p1, p3}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->voiceRegions:Ljava/util/List;

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->approximateMemberCount:Ljava/lang/Integer;

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 p2, 0x9c4

    if-le p1, p2, :cond_9

    const/4 p7, 0x1

    :cond_9
    iput-boolean p7, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->isAboveNotifyAllSize:Z

    return-void
.end method


# virtual methods
.method public final getAfkChannelModel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->afkChannelModel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getCanManage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->canManage:Z

    return v0
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getSystemChannelModel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->systemChannelModel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getVoiceRegion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->selectedVoiceRegionName:Ljava/lang/String;

    return-object v0
.end method

.method public final getVoiceRegions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model$VoiceRegion;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->voiceRegions:Ljava/util/List;

    return-object v0
.end method

.method public final isAboveNotifyAllSize()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->isAboveNotifyAllSize:Z

    return v0
.end method

.method public final isOwner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsOverview$Model;->isOwner:Z

    return v0
.end method
