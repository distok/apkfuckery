.class public final Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEditRole.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$computeManageStatus(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;ZZLjava/lang/Long;Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;->computeManageStatus(ZZLjava/lang/Long;Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$computeMyOtherPermissions(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;Ljava/util/Collection;Ljava/util/Map;JJ)J
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;->computeMyOtherPermissions(Ljava/util/Collection;Ljava/util/Map;JJ)J

    move-result-wide p0

    return-wide p0
.end method

.method private final computeManageStatus(ZZLjava/lang/Long;Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
    .locals 2

    const-wide/16 v0, 0x8

    invoke-static {v0, v1, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v0

    invoke-static {p4, p5}, Lcom/discord/models/domain/ModelGuildRole;->rankIsHigher(Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Z

    move-result v1

    if-nez p1, :cond_5

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p5, p4}, Lcom/discord/models/domain/ModelGuildRole;->rankIsHigher(Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->LOCKED_HIGHER:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    goto :goto_1

    :cond_1
    invoke-static {p4, p5}, Lcom/discord/models/domain/ModelGuildRole;->rankEquals(Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->LOCKED_HIGHEST:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    goto :goto_1

    :cond_2
    const-wide/32 p4, 0x10000000

    invoke-static {p4, p5, p3}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_CONDITIONAL:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    goto :goto_1

    :cond_3
    if-nez p2, :cond_4

    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->USER_NOT_ELEVATED:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    goto :goto_1

    :cond_4
    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->NO_MANAGE_ROLES_PERMISSION:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    goto :goto_1

    :cond_5
    :goto_0
    sget-object p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_ADMIN:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    :goto_1
    return-object p1
.end method

.method private final computeMyOtherPermissions(Ljava/util/Collection;Ljava/util/Map;JJ)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;JJ)J"
        }
    .end annotation

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuildRole;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    cmp-long v3, p3, p5

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide p5

    or-long/2addr p5, v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide v0

    or-long v1, p5, v0

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    if-eqz p5, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/lang/Number;

    invoke-virtual {p5}, Ljava/lang/Number;->longValue()J

    move-result-wide p5

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuildRole;

    if-eqz v0, :cond_1

    cmp-long v3, p5, p3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getPermissions()J

    move-result-wide p5

    or-long/2addr p5, v1

    move-wide v1, p5

    goto :goto_0

    :cond_2
    return-wide v1
.end method


# virtual methods
.method public final get(JJ)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;-><init>(JJ)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "getUsers()\n            .\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
