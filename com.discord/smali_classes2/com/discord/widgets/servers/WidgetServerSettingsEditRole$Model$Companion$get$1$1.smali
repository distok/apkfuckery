.class public final Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEditRole.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;->call(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildRole;",
        ">;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $meUser:Lcom/discord/models/domain/ModelUser;

.field public final synthetic this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;

    iput-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;",
            "Ljava/lang/Long;",
            ")",
            "Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v3, p3

    iget-object v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    move-object/from16 v2, p2

    invoke-static {v1, v2}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuildMember$Computed;

    const/4 v2, 0x0

    if-eqz v3, :cond_0

    iget-object v4, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;

    iget-wide v4, v4, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;->$roleId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelGuildRole;

    move-object v11, v4

    goto :goto_0

    :cond_0
    move-object v11, v2

    :goto_0
    if-eqz v11, :cond_3

    if-eqz p1, :cond_3

    if-nez v1, :cond_1

    goto :goto_2

    :cond_1
    invoke-static {v3, v1}, Lcom/discord/models/domain/ModelGuildRole;->getHighestRole(Ljava/util/Map;Lcom/discord/models/domain/ModelGuildMember$Computed;)Lcom/discord/models/domain/ModelGuildRole;

    move-result-object v9

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getOwnerId()J

    move-result-wide v4

    iget-object v2, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    const/4 v2, 0x1

    const/4 v12, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    const/4 v12, 0x0

    :goto_1
    iget-object v2, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->isMfaEnabled()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getMfaLevel()I

    move-result v4

    invoke-static {v2, v4}, Lcom/discord/utilities/permissions/PermissionUtils;->isElevated(ZI)Z

    move-result v7

    sget-object v2, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->Companion:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;

    move-object v5, v2

    move v6, v12

    move-object/from16 v8, p4

    move-object v10, v11

    invoke-static/range {v5 .. v10}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;->access$computeManageStatus(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;ZZLjava/lang/Long;Lcom/discord/models/domain/ModelGuildRole;Lcom/discord/models/domain/ModelGuildRole;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    move-result-object v10

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v4

    const-string v1, "meComputed.roles"

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->this$0:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;

    iget-wide v5, v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1;->$roleId:J

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    move-object v1, v2

    move-object v2, v4

    move-object/from16 v3, p3

    move-wide v4, v5

    move-wide v6, v7

    invoke-static/range {v1 .. v7}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;->access$computeMyOtherPermissions(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;Ljava/util/Collection;Ljava/util/Map;JJ)J

    move-result-wide v1

    new-instance v3, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    move-object v5, v3

    move v6, v12

    move-object v9, v11

    move-object/from16 v11, p4

    move-wide v12, v1

    invoke-direct/range {v5 .. v13}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;-><init>(ZJLcom/discord/models/domain/ModelGuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;J)V

    move-object v2, v3

    :cond_3
    :goto_2
    return-object v2
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Ljava/util/Map;

    check-cast p3, Ljava/util/Map;

    check-cast p4, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion$get$1$1;->call(Lcom/discord/models/domain/ModelGuild;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    move-result-object p1

    return-object p1
.end method
