.class public Lcom/discord/widgets/servers/WidgetServerSettingsChannels;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsChannels.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;
    }
.end annotation


# static fields
.field public static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"


# instance fields
.field private adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

.field private final channelSortTypeSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private dimmer:Lcom/discord/utilities/dimmer/DimmerView;

.field private textChannelsRecycler:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->channelSortTypeSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method private configureToolbar(Lcom/discord/models/domain/ModelGuild;Z)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NonConstantResourceId"
        }
    .end annotation

    xor-int/lit8 v0, p2, 0x1

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled(Z)Landroidx/appcompat/widget/Toolbar;

    if-nez p2, :cond_0

    const v0, 0x7f120474

    goto :goto_0

    :cond_0
    const v0, 0x7f1216b9

    :goto_0
    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    const p1, 0x7f0e0005

    new-instance v0, Lf/a/o/e/c;

    invoke-direct {v0, p0}, Lf/a/o/e/c;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;)V

    new-instance v1, Lf/a/o/e/d;

    invoke-direct {v1, p2}, Lf/a/o/e/d;-><init>(Z)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;Lrx/functions/Action1;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V
    .locals 2

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_0
    return-void

    :cond_1
    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$000(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$100(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->configureToolbar(Lcom/discord/models/domain/ModelGuild;Z)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->setFabVisibility(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lf/a/o/e/p;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/p;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    iget-object v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/DragAndDropAdapter;->setData(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    new-instance v1, Lf/a/o/e/e;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/e;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/servers/SettingsChannelListAdapter;->setPositionUpdateListener(Lrx/functions/Action1;)V

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$100(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    sget-object v0, Lf/a/o/e/m;->d:Lf/a/o/e/m;

    invoke-virtual {p1, v0}, Lcom/discord/widgets/servers/SettingsChannelListAdapter;->setOnClickListener(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    new-instance v1, Lf/a/o/e/l;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/l;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/servers/SettingsChannelListAdapter;->setOnClickListener(Lkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method

.method private getRestCall(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Ljava/util/Map;)Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    new-instance v3, Lcom/discord/restapi/RestAPIParams$ChannelPosition;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;

    invoke-virtual {v6}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;->getPosition()I

    move-result v6

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;

    invoke-virtual {v2}, Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;->getParentId()Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v3, v4, v5, v6, v2}, Lcom/discord/restapi/RestAPIParams$ChannelPosition;-><init>(JILjava/lang/Long;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApiSerializeNulls()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p2

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$000(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v1

    invoke-virtual {p2, v1, v2, v0}, Lcom/discord/utilities/rest/RestAPI;->reorderChannels(JLjava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic i(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    return-void
.end method

.method private reorderChannels(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/widgets/servers/SettingsChannelListAdapter$UpdatedPosition;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->getRestCall(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Ljava/util/Map;)Lrx/Observable;

    move-result-object p2

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p2

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->dimmer:Lcom/discord/utilities/dimmer/DimmerView;

    invoke-static {v0}, Lf/a/b/r;->q(Lcom/discord/utilities/dimmer/DimmerView;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p2

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p2, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lf/a/o/e/q;

    invoke-direct {v0, p0}, Lf/a/o/e/q;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lf/a/o/e/r;

    invoke-direct {v2, p0, p1}, Lf/a/o/e/r;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    invoke-static {v0, v1, v2}, Lf/a/b/r;->k(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p1

    invoke-virtual {p2, p1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method private setFabVisibility(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$200(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$100(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->show()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->hide()V

    :cond_1
    :goto_0
    return-void
.end method

.method public static show(Landroid/content/Context;J)V
    .locals 2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    const-string v1, "CHANNELS"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/stores/StoreAnalytics;->onGuildSettingsPaneViewed(Ljava/lang/String;J)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p1

    const-class p2, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;

    invoke-static {p0, p2, p1}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public synthetic f(Landroid/view/MenuItem;Landroid/content/Context;)V
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->channelSortTypeSubject:Lrx/subjects/BehaviorSubject;

    const/4 p2, -0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->channelSortTypeSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lf/a/o/e/a;

    invoke-direct {v0, p2}, Lf/a/o/e/a;-><init>(Lrx/subjects/BehaviorSubject;)V

    invoke-static {p1, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsSortActions;->show(Landroidx/fragment/app/FragmentManager;Lrx/functions/Action1;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a06a6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic g(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Landroid/view/View;)V
    .locals 3

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->access$000(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)Lcom/discord/models/domain/ModelGuild;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    new-instance v2, Lf/a/o/e/n;

    invoke-direct {v2, p0, p1}, Lf/a/o/e/n;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    invoke-static {v0, v1, p2, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannelsFabMenuFragment;->show(JLandroidx/fragment/app/FragmentManager;Lrx/functions/Action0;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->hide()V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d026a

    return v0
.end method

.method public synthetic h(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->reorderChannels(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Ljava/util/Map;)V

    return-void
.end method

.method public synthetic j(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->setFabVisibility(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    return-void
.end method

.method public synthetic k(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;Lcom/discord/utilities/error/Error;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a08b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->textChannelsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a08ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->createFab:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const v0, 0x7f0a035a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/dimmer/DimmerView;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->dimmer:Lcom/discord/utilities/dimmer/DimmerView;

    new-instance p1, Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->textChannelsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/servers/SettingsChannelListAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Z)V

    invoke-static {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->adapter:Lcom/discord/widgets/servers/SettingsChannelListAdapter;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsChannels;->channelSortTypeSubject:Lrx/subjects/BehaviorSubject;

    invoke-static {v0, v1, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsChannels$Model;->get(JLrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/o;

    invoke-direct {v1, p0}, Lf/a/o/e/o;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsChannels;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
