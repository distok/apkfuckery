.class public final Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;
.super Ljava/lang/Object;
.source "NotificationMuteSettingsView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/NotificationMuteSettingsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final isMuted:Z

.field private final muteDescriptionText:Ljava/lang/String;

.field private final muteEndTime:Ljava/lang/String;

.field private final rawMuteText:Ljava/lang/String;

.field private final rawMutedStatusText:Ljava/lang/String;

.field private final rawMutedUntilStatusResId:I

.field private final rawUnmuteText:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p6    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const-string v0, "rawMuteText"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rawUnmuteText"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rawMutedStatusText"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    iput-object p2, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    iput p6, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    iput-object p7, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move-object v8, v0

    goto :goto_0

    :cond_0
    move-object/from16 v8, p7

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->copy(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    return v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;
    .locals 9
    .param p6    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const-string v0, "rawMuteText"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rawUnmuteText"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rawMutedStatusText"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;

    iget-boolean v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    iget v1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMuteDescriptionText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    return-object v0
.end method

.method public final getMuteEndTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public final getRawMuteText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    return-object v0
.end method

.method public final getRawMutedStatusText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    return-object v0
.end method

.method public final getRawMutedUntilStatusResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    return v0
.end method

.method public final getRawUnmuteText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    return v0
.end method

.method public final isMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(isMuted="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->isMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", muteEndTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteEndTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", rawMuteText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMuteText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", rawUnmuteText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawUnmuteText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", rawMutedStatusText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedStatusText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", rawMutedUntilStatusResId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->rawMutedUntilStatusResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", muteDescriptionText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/NotificationMuteSettingsView$ViewState;->muteDescriptionText:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
