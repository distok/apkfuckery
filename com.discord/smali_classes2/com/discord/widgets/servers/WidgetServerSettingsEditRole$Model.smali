.class public final Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;
.super Ljava/lang/Object;
.source "WidgetServerSettingsEditRole.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/WidgetServerSettingsEditRole;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;,
        Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;


# instance fields
.field private final guildId:J

.field private final manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

.field private final myPermissions:Ljava/lang/Long;

.field private final myPermissionsFromOtherRoles:J

.field private final owner:Z

.field private final role:Lcom/discord/models/domain/ModelGuildRole;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->Companion:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$Companion;

    return-void
.end method

.method public constructor <init>(ZJLcom/discord/models/domain/ModelGuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;J)V
    .locals 1

    const-string v0, "role"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    iput-wide p2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    iput-object p4, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    iput-object p5, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    iput-object p6, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    iput-wide p7, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;ZJLcom/discord/models/domain/ModelGuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;JILjava/lang/Object;)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;
    .locals 9

    move-object v0, p0

    and-int/lit8 v1, p9, 0x1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    and-int/lit8 v2, p9, 0x2

    if-eqz v2, :cond_1

    iget-wide v2, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    goto :goto_1

    :cond_1
    move-wide v2, p2

    :goto_1
    and-int/lit8 v4, p9, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    goto :goto_2

    :cond_2
    move-object v4, p4

    :goto_2
    and-int/lit8 v5, p9, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    goto :goto_3

    :cond_3
    move-object v5, p5

    :goto_3
    and-int/lit8 v6, p9, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    goto :goto_4

    :cond_4
    move-object v6, p6

    :goto_4
    and-int/lit8 v7, p9, 0x20

    if-eqz v7, :cond_5

    iget-wide v7, v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    goto :goto_5

    :cond_5
    move-wide/from16 v7, p7

    :goto_5
    move p1, v1

    move-wide p2, v2

    move-object p4, v4

    move-object p5, v5

    move-object p6, v6

    move-wide/from16 p7, v7

    invoke-virtual/range {p0 .. p8}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->copy(ZJLcom/discord/models/domain/ModelGuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;J)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final canManage()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    sget-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_CONDITIONAL:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;->CAN_MANAGE_ADMIN:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    return v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    return-wide v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelGuildRole;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    return-object v0
.end method

.method public final component4()Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    return-object v0
.end method

.method public final component5()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    return-wide v0
.end method

.method public final copy(ZJLcom/discord/models/domain/ModelGuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;J)Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;
    .locals 10

    const-string v0, "role"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    move-object v1, v0

    move v2, p1

    move-wide v3, p2

    move-object v6, p5

    move-object/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;-><init>(ZJLcom/discord/models/domain/ModelGuildRole;Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;Ljava/lang/Long;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    iget-boolean v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    iget-object v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    iget-object v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    return-wide v0
.end method

.method public final getManageStatus()Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    return-object v0
.end method

.method public final getMyPermissions()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final getMyPermissionsFromOtherRoles()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    return-wide v0
.end method

.method public final getOwner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    return v0
.end method

.method public final getRole()Lcom/discord/models/domain/ModelGuildRole;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildRole;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final isEveryoneRole()Z
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildRole;->getId()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isSingular(J)Z
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    and-long/2addr v0, p1

    cmp-long v2, v0, p1

    if-eqz v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Model(owner="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->owner:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->role:Lcom/discord/models/domain/ModelGuildRole;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", manageStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->manageStatus:Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model$ManageStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myPermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissions:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myPermissionsFromOtherRoles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsEditRole$Model;->myPermissionsFromOtherRoles:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
