.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;
.super Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.source "PremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LaunchPurchaseSubscription"
.end annotation


# instance fields
.field private final guildId:J

.field private final oldSkuName:Ljava/lang/String;

.field private final section:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    const-string v0, "section"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    iput-wide p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    iput-object p4, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;JLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;Ljava/lang/String;JLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-wide p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-object p4, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->copy(Ljava/lang/String;JLjava/lang/String;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    return-wide v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;JLjava/lang/String;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;
    .locals 1

    const-string v0, "section"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    return-wide v0
.end method

.method public final getOldSkuName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSection()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "LaunchPurchaseSubscription(section="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->section:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", oldSkuName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->oldSkuName:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
