.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;
.super Ljava/lang/Object;
.source "PremiumGuildTransferInProgressViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->transferPremiumGuildSubscription()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Void;",
        "Lrx/Observable<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;->this$0:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;->call(Ljava/lang/Void;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Void;)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Void;",
            ")",
            "Lrx/Observable<",
            "+",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscription;",
            ">;>;"
        }
    .end annotation

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;->this$0:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->getRestAPI()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;->this$0:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->getTargetGuildId()J

    move-result-wide v0

    new-instance v2, Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;

    iget-object v3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;->this$0:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    invoke-virtual {v3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->getSlotId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->subscribeToGuild(JLcom/discord/restapi/RestAPIParams$PremiumGuildSubscribe;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
