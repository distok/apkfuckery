.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;
.super Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.source "PremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LaunchSubscriptionConfirmation"
.end annotation


# instance fields
.field private final guildId:J

.field private final slotId:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    iput-wide p3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;JJILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    :cond_0
    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_1

    iget-wide p3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->copy(JJ)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    return-wide v0
.end method

.method public final copy(JJ)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;
    .locals 1

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;-><init>(JJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    return-wide v0
.end method

.method public final getSlotId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "LaunchSubscriptionConfirmation(guildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", slotId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->slotId:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
