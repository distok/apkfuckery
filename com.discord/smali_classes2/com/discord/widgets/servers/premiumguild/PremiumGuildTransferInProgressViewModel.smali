.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;
.super Lf/a/b/l0;
.source "PremiumGuildTransferInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private premiumGuildSubscribingSubscription:Lrx/Subscription;

.field private final previousGuildId:J

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final slotId:J

.field private final storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

.field private final subscriptionId:J

.field private final targetGuildId:J


# direct methods
.method public constructor <init>(Lrx/Observable;JJJJLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StorePremiumGuildSubscription;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;",
            ">;JJJJ",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p10

    move-object/from16 v2, p11

    const-string v3, "storeObservable"

    move-object v4, p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "restAPI"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "storePremiumGuildSubscription"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$Loading;

    invoke-direct {p0, v3}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    move-wide v5, p2

    iput-wide v5, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->slotId:J

    move-wide v5, p4

    iput-wide v5, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->subscriptionId:J

    move-wide/from16 v5, p6

    iput-wide v5, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->previousGuildId:J

    move-wide/from16 v5, p8

    iput-wide v5, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->targetGuildId:J

    iput-object v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object v2, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v1, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    new-instance v3, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$1;-><init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object p1, v1

    move-object p2, v2

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v3

    move/from16 p8, v8

    move-object/from16 p9, v9

    invoke-static/range {p1 .. p9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handlePremiumGuildSubscriptionCompleted(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->handlePremiumGuildSubscriptionCompleted()V

    return-void
.end method

.method public static final synthetic access$handlePremiumGuildSubscriptionError(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->handlePremiumGuildSubscriptionError()V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->handleStoreState(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;)V

    return-void
.end method

.method private final handlePremiumGuildSubscriptionCompleted()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription;->fetchUserGuildPremiumState()V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->getTargetGuildSubscriptionCount()I

    move-result v2

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->getTargetGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-direct {v1, v0, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;-><init>(Lcom/discord/models/domain/ModelGuild;I)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handlePremiumGuildSubscriptionError()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorTransfer;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorTransfer;

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handlePremiumGuildSubscriptionStarted()V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;

    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    if-eqz v1, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;)V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;->getPreviousGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;->getTargetGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_3

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;->getPremiumGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;->getPremiumGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->targetGuildId:J

    cmp-long v8, v4, v6

    if-nez v8, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_1

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;->getPreviousGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;->getTargetGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-direct {v2, v3, p1, v0, v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZ)V

    invoke-virtual {p0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_5
    :goto_3
    sget-object p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorLoading;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorLoading;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getPreviousGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->previousGuildId:J

    return-wide v0
.end method

.method public final getRestAPI()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final getSlotId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->slotId:J

    return-wide v0
.end method

.method public final getStorePremiumGuildSubscription()Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    return-object v0
.end method

.method public final getSubscriptionId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->subscriptionId:J

    return-wide v0
.end method

.method public final getTargetGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->targetGuildId:J

    return-wide v0
.end method

.method public onCleared()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->premiumGuildSubscribingSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    return-void
.end method

.method public final transferPremiumGuildSubscription()V
    .locals 13
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->handlePremiumGuildSubscriptionStarted()V

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->previousGuildId:J

    iget-wide v3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;->subscriptionId:J

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/utilities/rest/RestAPI;->unsubscribeToGuild(JJ)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$1;-><init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "restAPI\n        .unsubsc\u2026              )\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v3, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    new-instance v8, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$2;-><init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V

    new-instance v10, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$3;

    invoke-direct {v10, p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$transferPremiumGuildSubscription$3;-><init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
