.class public abstract Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.super Ljava/lang/Object;
.source "PremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowDesktopAlertDialog;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$UnacknowledgedPurchase;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;-><init>()V

    return-void
.end method
