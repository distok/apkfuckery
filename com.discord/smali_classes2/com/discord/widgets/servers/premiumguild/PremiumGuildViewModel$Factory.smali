.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;
.super Ljava/lang/Object;
.source "PremiumGuildViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;->guildId:J

    return-void
.end method

.method private final observeStores()Lrx/Observable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v2}, Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState$default(Lcom/discord/stores/StorePremiumGuildSubscription;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreSubscriptions;->getSubscriptions()Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;->guildId:J

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases;->getState()Lrx/Observable;

    move-result-object v8

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$sam$rx_functions_Func5$0;

    invoke-direct {v1, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$sam$rx_functions_Func5$0;-><init>(Lkotlin/jvm/functions/Function5;)V

    move-object v0, v1

    :cond_0
    move-object v9, v0

    check-cast v9, Lrx/functions/Func5;

    invoke-static/range {v4 .. v9}, Lrx/Observable;->g(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func5;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026     ::StoreState\n      )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v0

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v2

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v3

    invoke-direct {p1, v1, v0, v2, v3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V

    return-object p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;->guildId:J

    return-wide v0
.end method
