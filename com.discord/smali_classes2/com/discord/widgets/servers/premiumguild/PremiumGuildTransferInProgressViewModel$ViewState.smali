.class public abstract Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;
.super Ljava/lang/Object;
.source "PremiumGuildTransferInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$Loading;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorTransfer;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorLoading;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;-><init>()V

    return-void
.end method
