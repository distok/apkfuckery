.class public final synthetic Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;
.super Lx/m/c/i;
.source "PremiumGuildViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function5<",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/stores/StoreGooglePlayPurchases$State;",
        "Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;

    invoke-direct {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;

    const/4 v1, 0x5

    const-string v3, "<init>"

    const-string v4, "<init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGooglePlayPurchases$State;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;
    .locals 7

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p5"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGooglePlayPurchases$State;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    check-cast p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    check-cast p3, Lcom/discord/models/domain/ModelUser;

    check-cast p4, Lcom/discord/models/domain/ModelGuild;

    check-cast p5, Lcom/discord/stores/StoreGooglePlayPurchases$State;

    invoke-virtual/range {p0 .. p5}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory$observeStores$1;->invoke(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
