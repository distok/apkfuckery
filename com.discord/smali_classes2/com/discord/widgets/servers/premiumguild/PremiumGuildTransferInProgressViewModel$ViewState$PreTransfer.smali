.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;
.super Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;
.source "PremiumGuildTransferInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreTransfer"
.end annotation


# instance fields
.field private final isTransferInProgress:Z

.field private final previousGuild:Lcom/discord/models/domain/ModelGuild;

.field private final targetGuild:Lcom/discord/models/domain/ModelGuild;

.field private final targetGuildSubscriptionCount:I


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZ)V
    .locals 1

    const-string v0, "previousGuild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "targetGuild"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    iput-object p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    iput p3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    iput-boolean p4, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->copy(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZ)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZ)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;
    .locals 1

    const-string v0, "previousGuild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "targetGuild"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;IZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    iget v1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    iget-boolean p1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPreviousGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getTargetGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getTargetGuildSubscriptionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isTransferInProgress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "PreTransfer(previousGuild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->previousGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", targetGuild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", targetGuildSubscriptionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->targetGuildSubscriptionCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isTransferInProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
