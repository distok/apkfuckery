.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;
.super Lf/a/b/l0;
.source "PremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;,
        Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

.field private final storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/utilities/time/Clock;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            "Lcom/discord/stores/StoreSubscriptions;",
            "Lcom/discord/utilities/time/Clock;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "storePremiumGuildSubscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeSubscriptions"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeObservable"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    iput-object p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    iput-object p3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->fetchData()V

    invoke-static {p4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    new-instance v6, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$1;-><init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->handleStoreState(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;)V

    return-void
.end method

.method private final fetchData()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription;->fetchUserGuildPremiumState()V

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;)V
    .locals 8
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;->getPremiumGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;->getSubscriptionState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;->getPurchasesState()Lcom/discord/stores/StoreGooglePlayPurchases$State;

    move-result-object v2

    instance-of v3, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    if-nez v3, :cond_5

    instance-of v3, v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    if-nez v3, :cond_5

    instance-of v3, v2, Lcom/discord/stores/StoreGooglePlayPurchases$State$Uninitialized;

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    instance-of v3, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    if-nez v3, :cond_4

    instance-of v3, v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    instance-of v3, v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v3, :cond_3

    instance-of v3, v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v3, :cond_3

    instance-of v3, v2, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    if-nez v3, :cond_2

    sget-object p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;

    return-void

    :cond_2
    new-instance v6, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;

    check-cast v0, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v3

    check-cast v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v4

    check-cast v2, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    invoke-virtual {v2}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->getPurchases()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$StoreState;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    move-object v0, v6

    move-object v1, v3

    move-object v2, v4

    move-object v3, v5

    move-object v4, v7

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;-><init>(Ljava/util/Map;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuild;)V

    goto :goto_2

    :cond_3
    sget-object v6, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;

    goto :goto_2

    :cond_4
    :goto_0
    sget-object v6, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;

    goto :goto_2

    :cond_5
    :goto_1
    sget-object v6, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loading;

    :goto_2
    invoke-virtual {p0, v6}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getClock()Lcom/discord/utilities/time/Clock;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->clock:Lcom/discord/utilities/time/Clock;

    return-object v0
.end method

.method public final getStorePremiumGuildSubscription()Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    return-object v0
.end method

.method public final getStoreSubscriptions()Lcom/discord/stores/StoreSubscriptions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final retryClicked()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->fetchData()V

    return-void
.end method

.method public final subscribeClicked(Ljava/lang/String;)V
    .locals 13
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v1

    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCooldownExpiresAtTimestamp()J

    move-result-wide v8

    iget-object v10, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v10}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v12, v8, v10

    if-gez v12, :cond_3

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_1

    :cond_2
    move-object v7, v2

    :goto_1
    if-nez v7, :cond_3

    const/4 v5, 0x1

    :cond_3
    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v6

    if-eqz v1, :cond_5

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v4

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getId()J

    move-result-wide v2

    invoke-direct {v1, v4, v5, v2, v3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;-><init>(JJ)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_7

    :cond_5
    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelSubscription;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_2

    :cond_6
    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowDesktopAlertDialog;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowDesktopAlertDialog;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_7

    :cond_7
    :goto_2
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v3

    goto :goto_3

    :cond_8
    move-object v3, v2

    :goto_3
    const v4, 0x7f121394

    if-eqz v3, :cond_9

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    const v1, 0x7f121392

    invoke-direct {v0, v4, v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;-><init>(II)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_7

    :cond_9
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v3

    goto :goto_4

    :cond_a
    move-object v3, v2

    :goto_4
    sget-object v7, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v3, v7, :cond_b

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    const v1, 0x7f121391

    invoke-direct {v0, v4, v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;-><init>(II)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_7

    :cond_b
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v1

    goto :goto_5

    :cond_c
    move-object v1, v2

    :goto_5
    sget-object v3, Lcom/discord/models/domain/ModelSubscription$Status;->ACCOUNT_HOLD:Lcom/discord/models/domain/ModelSubscription$Status;

    if-ne v1, v3, :cond_d

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    const v1, 0x7f121390

    const v2, 0x7f12138f

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;-><init>(II)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_7

    :cond_d
    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getPurchases()Ljava/util/List;

    move-result-object v1

    instance-of v3, v1, Ljava/util/Collection;

    if-eqz v3, :cond_e

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_e

    goto :goto_6

    :cond_e
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/billingclient/api/Purchase;

    invoke-virtual {v3}, Lcom/android/billingclient/api/Purchase;->c()Z

    move-result v3

    xor-int/2addr v3, v6

    if-eqz v3, :cond_f

    const/4 v5, 0x1

    :cond_10
    :goto_6
    if-eqz v5, :cond_11

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$UnacknowledgedPurchase;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$UnacknowledgedPurchase;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_7

    :cond_11
    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v3, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelSubscription;

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v2

    :cond_12
    invoke-direct {v3, p1, v4, v5, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    iget-object p1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v3}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_13
    :goto_7
    return-void
.end method
