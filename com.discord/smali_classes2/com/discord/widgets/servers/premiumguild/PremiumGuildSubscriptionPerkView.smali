.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;
.super Landroid/widget/RelativeLayout;
.source "PremiumGuildSubscriptionPerkView.kt"


# instance fields
.field private adapter:Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;

.field private container:Landroid/widget/LinearLayout;

.field private contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field private contentText:Landroid/widget/TextView;

.field private header:Landroid/view/View;

.field private headerBoostText:Landroid/widget/TextView;

.field private headerText:Landroid/widget/TextView;

.field private headerUnlocked:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->initialize()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final initialize()V
    .locals 5

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d013c

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0a077d

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->header:Landroid/view/View;

    const v0, 0x7f0a0780

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header_text)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerText:Landroid/widget/TextView;

    const v0, 0x7f0a077e

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header_boosts)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerBoostText:Landroid/widget/TextView;

    const v0, 0x7f0a0781

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_header_unlocked)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerUnlocked:Landroid/view/View;

    const v0, 0x7f0a077b

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_contents_header)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->contentText:Landroid/widget/TextView;

    const v0, 0x7f0a077c

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.perks_level_contents_recycler)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a02f2

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.container)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->container:Landroid/widget/LinearLayout;

    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;

    iget-object v2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v3, 0x0

    const-string v4, "contentRecycler"

    if-eqz v2, :cond_1

    invoke-direct {v1, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->adapter:Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->contentRecycler:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$initialize$1;

    invoke-direct {v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$initialize$1;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;)V

    return-void

    :cond_0
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public final configure(II)V
    .locals 20

    move-object/from16 v0, p0

    move/from16 v1, p1

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sget-object v3, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move/from16 v6, p2

    if-lt v6, v1, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    const-string v7, "header"

    const/4 v8, 0x0

    if-eqz v6, :cond_2

    iget-object v9, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->header:Landroid/view/View;

    if-eqz v9, :cond_1

    const v7, 0x7f080127

    invoke-virtual {v9, v7}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_1
    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v8

    :cond_2
    iget-object v9, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->header:Landroid/view/View;

    if-eqz v9, :cond_1e

    const v7, 0x7f0404a8

    invoke-static {v0, v7}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v7

    invoke-virtual {v9, v7}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_1
    const/4 v7, 0x3

    const/4 v9, 0x2

    if-eq v1, v4, :cond_5

    if-eq v1, v9, :cond_4

    if-eq v1, v7, :cond_3

    const/4 v10, 0x0

    goto :goto_2

    :cond_3
    const/16 v10, 0x1e

    goto :goto_2

    :cond_4
    const/16 v10, 0xf

    goto :goto_2

    :cond_5
    const/4 v10, 0x2

    :goto_2
    iget-object v11, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerText:Landroid/widget/TextView;

    const-string v12, "headerText"

    if-eqz v11, :cond_1d

    if-eq v1, v4, :cond_8

    if-eq v1, v9, :cond_7

    if-eq v1, v7, :cond_6

    const/4 v13, 0x0

    goto :goto_3

    :cond_6
    const v13, 0x7f120b82

    goto :goto_3

    :cond_7
    const v13, 0x7f120b81

    goto :goto_3

    :cond_8
    const v13, 0x7f120b80

    :goto_3
    invoke-static {v0, v13}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eq v1, v4, :cond_d

    if-eq v1, v9, :cond_b

    if-eq v1, v7, :cond_9

    const/4 v14, 0x0

    goto :goto_4

    :cond_9
    if-eqz v6, :cond_a

    const v11, 0x7f0803c8

    const v14, 0x7f0803c8

    goto :goto_4

    :cond_a
    const v11, 0x7f0803c9

    const v14, 0x7f0803c9

    goto :goto_4

    :cond_b
    if-eqz v6, :cond_c

    const v11, 0x7f0803c6

    const v14, 0x7f0803c6

    goto :goto_4

    :cond_c
    const v11, 0x7f0803c7

    const v14, 0x7f0803c7

    goto :goto_4

    :cond_d
    if-eqz v6, :cond_e

    const v11, 0x7f0803c4

    const v14, 0x7f0803c4

    goto :goto_4

    :cond_e
    const v11, 0x7f0803c5

    const v14, 0x7f0803c5

    :goto_4
    iget-object v13, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerText:Landroid/widget/TextView;

    if-eqz v13, :cond_1c

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xe

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/drawable/DrawableCompat;->setCompoundDrawablesCompat$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    iget-object v11, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerText:Landroid/widget/TextView;

    if-eqz v11, :cond_1b

    if-eqz v6, :cond_f

    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f060292

    invoke-static {v12, v13}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v12

    goto :goto_5

    :cond_f
    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v12

    const v13, 0x7f040495

    invoke-static {v12, v13}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v12

    :goto_5
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v11, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerBoostText:Landroid/widget/TextView;

    const-string v12, "headerBoostText"

    if-eqz v11, :cond_1a

    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const-string v14, "resources"

    invoke-static {v13, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "context"

    invoke-static {v14, v15}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v15, 0x7f10008c

    new-array v8, v4, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v8, v5

    invoke-static {v13, v14, v15, v10, v8}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerBoostText:Landroid/widget/TextView;

    if-eqz v8, :cond_19

    xor-int/lit8 v10, v6, 0x1

    const/16 v11, 0x8

    if-eqz v10, :cond_10

    const/4 v10, 0x0

    goto :goto_6

    :cond_10
    const/16 v10, 0x8

    :goto_6
    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->headerUnlocked:Landroid/view/View;

    if-eqz v8, :cond_18

    if-eqz v6, :cond_11

    const/4 v11, 0x0

    :cond_11
    invoke-virtual {v8, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->contentText:Landroid/widget/TextView;

    if-eqz v8, :cond_17

    if-eq v1, v4, :cond_12

    const v10, 0x7f120b7e

    invoke-static {v0, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v10

    goto :goto_7

    :cond_12
    const v10, 0x7f120b7c

    invoke-static {v0, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v10

    :goto_7
    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v13, 0x4

    const v14, 0x7f120b79

    const v15, 0x7f080344

    const v8, 0x7f120b7a

    const v10, 0x7f080430

    if-eq v1, v4, :cond_15

    const v12, 0x7f08046a

    if-eq v1, v9, :cond_14

    if-eq v1, v7, :cond_13

    sget-object v1, Lx/h/l;->d:Lx/h/l;

    goto/16 :goto_8

    :cond_13
    new-array v1, v13, [Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    new-instance v13, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v10, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v10

    new-array v11, v9, [Ljava/lang/Object;

    const/16 v16, 0x64

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v11, v5

    invoke-static {v7, v5}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v11, v4

    invoke-static {v0, v8, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v13, v10, v8}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v13, v1, v5

    new-instance v8, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v15, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v10

    new-array v11, v4, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v13, v2}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v11, v5

    invoke-static {v0, v14, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v10, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v4

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v12, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v8

    new-array v10, v4, [Ljava/lang/Object;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v11}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB(Ljava/lang/Integer;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v4, v5

    const v11, 0x7f120749

    invoke-static {v0, v11, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v5

    const v4, 0x7f120b7b

    invoke-static {v0, v4, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v8, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    const v4, 0x7f08043b

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v3

    const v4, 0x7f120b78

    invoke-static {v0, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v7

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_8

    :cond_14
    const/4 v1, 0x5

    new-array v1, v1, [Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    new-instance v11, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v10, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v10

    new-array v13, v9, [Ljava/lang/Object;

    const/16 v18, 0x32

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v13, v5

    invoke-static {v9, v5}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v13, v4

    invoke-static {v0, v8, v13}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v11, v10, v8}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v11, v1, v5

    new-instance v8, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v15, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v10

    new-array v11, v4, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static {v13, v2}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v11, v5

    invoke-static {v0, v14, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v10, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v4

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v12, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v8

    new-array v10, v4, [Ljava/lang/Object;

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v11}, Lcom/discord/models/domain/ModelGuild;->getMaxFileSizeMB(Ljava/lang/Integer;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v4, v5

    const v11, 0x7f120749

    invoke-static {v0, v11, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v5

    const v4, 0x7f120b7b

    invoke-static {v0, v4, v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v8, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    const v4, 0x7f08035d

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v4

    const v5, 0x7f120b76

    invoke-static {v0, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    const v4, 0x7f08033a

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v3

    const v4, 0x7f120b77

    invoke-static {v0, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    const/4 v3, 0x4

    aput-object v2, v1, v3

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_8

    :cond_15
    const/4 v1, 0x5

    new-array v1, v1, [Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    new-instance v11, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v10, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v10

    new-array v12, v9, [Ljava/lang/Object;

    const/16 v13, 0x32

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v5

    invoke-static {v4, v5}, Lcom/discord/models/domain/ModelGuild;->getEmojiMaxCount(IZ)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v12, v4

    invoke-static {v0, v8, v12}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v11, v10, v8}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v11, v1, v5

    new-instance v8, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    invoke-virtual {v3, v15, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v10

    new-array v11, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v12, v2}, Lcom/discord/models/domain/ModelGuild;->getMaxVoiceBitrateKbps(Ljava/lang/Integer;Ljava/lang/Boolean;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v11, v5

    invoke-static {v0, v14, v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v10, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v8, v1, v4

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    const v4, 0x7f0803a2

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v4

    const v5, 0x7f120b73

    invoke-static {v0, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    const v4, 0x7f08035d

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v4

    const v5, 0x7f120b74

    invoke-static {v0, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;

    const v4, 0x7f08033a

    invoke-virtual {v3, v4, v6}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView$configure$1;->invoke(IZ)I

    move-result v3

    const v4, 0x7f120b75

    invoke-static {v0, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter$PremiumGuildSubscriptionPerkViewListItem;-><init>(ILjava/lang/String;)V

    const/4 v3, 0x4

    aput-object v2, v1, v3

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    :goto_8
    iget-object v2, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->adapter:Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;

    if-eqz v2, :cond_16

    invoke-virtual {v2, v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkViewAdapter;->configure(Ljava/util/List;)V

    return-void

    :cond_16
    const-string v1, "adapter"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    throw v1

    :cond_17
    const/4 v1, 0x0

    const-string v2, "contentText"

    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_18
    const/4 v1, 0x0

    const-string v2, "headerUnlocked"

    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_19
    const/4 v1, 0x0

    invoke-static {v12}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1a
    move-object v1, v8

    invoke-static {v12}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1b
    move-object v1, v8

    invoke-static {v12}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1c
    move-object v1, v8

    invoke-static {v12}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1d
    move-object v1, v8

    invoke-static {v12}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1e
    move-object v1, v8

    invoke-static {v7}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
