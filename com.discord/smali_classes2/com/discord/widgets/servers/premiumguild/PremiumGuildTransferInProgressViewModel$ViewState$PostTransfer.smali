.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;
.super Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;
.source "PremiumGuildTransferInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PostTransfer"
.end annotation


# instance fields
.field private final targetGuild:Lcom/discord/models/domain/ModelGuild;

.field private final targetGuildSubscriptionCount:I


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;I)V
    .locals 1

    const-string v0, "targetGuild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    iput p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;Lcom/discord/models/domain/ModelGuild;IILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->copy(Lcom/discord/models/domain/ModelGuild;I)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;I)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;
    .locals 1

    const-string v0, "targetGuild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;-><init>(Lcom/discord/models/domain/ModelGuild;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    iget p1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getTargetGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getTargetGuildSubscriptionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "PostTransfer(targetGuild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", targetGuildSubscriptionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->targetGuildSubscriptionCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
