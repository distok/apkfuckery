.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;
.super Ljava/lang/Object;
.source "PremiumGuildTransferInProgressViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;)V
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    invoke-virtual {v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->getPreviousGuildId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    invoke-virtual {v3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->getTargetGuildId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v3, v4, v5, v4}, Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState$default(Lcom/discord/stores/StorePremiumGuildSubscription;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;

    invoke-static {v1, v2, v3, v4}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v2

    const-string v1, "Observable.combineLatest\u2026            )\n          }"

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->getSubscriptionId()J

    move-result-wide v5

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->getSlotId()J

    move-result-wide v3

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->getPreviousGuildId()J

    move-result-wide v7

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->config:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    invoke-virtual {v1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->getTargetGuildId()J

    move-result-wide v9

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v11

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v12

    move-object v1, p1

    invoke-direct/range {v1 .. v12}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;-><init>(Lrx/Observable;JJJJLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StorePremiumGuildSubscription;)V

    return-object p1
.end method
