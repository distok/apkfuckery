.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;
.super Ljava/lang/Object;
.source "PremiumGuildTransferInProgressViewModel.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;->create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        "Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;

    invoke-direct {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StorePremiumGuildSubscription$State;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;
    .locals 2

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;

    const-string v1, "premiumGuildSubscriptionState"

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StorePremiumGuildSubscription$State;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Lcom/discord/models/domain/ModelGuild;

    check-cast p3, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory$create$1;->call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StorePremiumGuildSubscription$State;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
