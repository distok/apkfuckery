.class public final Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;
.super Lcom/discord/app/AppFragment;
.source "WidgetPremiumGuildSubscriptionTransfer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;

.field private static final INTENT_EXTRA_PREVIOUS_GUILD_ID:Ljava/lang/String; = "PREVIOUS_GUILD_ID"

.field private static final INTENT_EXTRA_SLOT_ID:Ljava/lang/String; = "SLOT_ID"

.field private static final INTENT_EXTRA_SUBSCRIPTION_ID:Ljava/lang/String; = "SUBSCRIPTION_ID"

.field private static final INTENT_EXTRA_TARGET_GUILD_ID:Ljava/lang/String; = "TARGET_GUILD_ID"


# instance fields
.field private final dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final errorTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final previousGuildConfirmationView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final previousGuildHeaderTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final targetGuildConfirmationView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final targetGuildHeaderTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final transferButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final transferConfirmationTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v3, "transferConfirmationTextView"

    const-string v4, "getTransferConfirmationTextView()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "previousGuildHeaderTextView"

    const-string v7, "getPreviousGuildHeaderTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "previousGuildConfirmationView"

    const-string v7, "getPreviousGuildConfirmationView()Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "targetGuildHeaderTextView"

    const-string v7, "getTargetGuildHeaderTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "targetGuildConfirmationView"

    const-string v7, "getTargetGuildConfirmationView()Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "transferButton"

    const-string v7, "getTransferButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "errorTextView"

    const-string v7, "getErrorTextView()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const-string v6, "dimmer"

    const-string v7, "getDimmer()Lcom/discord/utilities/dimmer/DimmerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a07b6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->transferConfirmationTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07b9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->previousGuildHeaderTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07b8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->previousGuildConfirmationView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->targetGuildHeaderTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->targetGuildConfirmationView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07ba

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->transferButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07b7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->errorTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a035a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->configureUI(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState;)V
    .locals 12

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorLoading;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$Loading;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v4, v5, v3, v2}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getErrorTextView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$ErrorTransfer;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v5, v5, v3, v2}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getErrorTextView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object v0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->isTransferInProgress()Z

    move-result v6

    invoke-static {v0, v6, v5, v3, v2}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getErrorTextView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getPreviousGuildConfirmationView()Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->getPreviousGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->a(Lcom/discord/models/domain/ModelGuild;I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getTargetGuildConfirmationView()Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PreTransfer;->getTargetGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-virtual {v0, p1, v4}, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;->a(Lcom/discord/models/domain/ModelGuild;I)V

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    if-eqz v0, :cond_5

    sget-object v5, Lf/a/a/b/c;->k:Lf/a/a/b/c$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v6

    const-string v0, "parentFragmentManager"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const-string v0, "resources"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->getTargetGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$ViewState$PostTransfer;->getTargetGuildSubscriptionCount()I

    move-result p1

    add-int/lit8 v9, p1, 0x1

    const/4 v10, 0x1

    new-instance v11, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$configureUI$1;

    invoke-direct {v11, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$configureUI$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;)V

    invoke-virtual/range {v5 .. v11}, Lf/a/a/b/c$a;->a(Landroidx/fragment/app/FragmentManager;Landroid/content/res/Resources;Ljava/lang/String;IZLkotlin/jvm/functions/Function0;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public static final create(Landroid/content/Context;JJLcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;->create(Landroid/content/Context;JJLcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V

    return-void
.end method

.method private final getDimmer()Lcom/discord/utilities/dimmer/DimmerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    return-object v0
.end method

.method private final getErrorTextView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->errorTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPreviousGuildConfirmationView()Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->previousGuildConfirmationView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;

    return-object v0
.end method

.method private final getPreviousGuildHeaderTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->previousGuildHeaderTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTargetGuildConfirmationView()Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->targetGuildConfirmationView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumGuildConfirmationView;

    return-object v0
.end method

.method private final getTargetGuildHeaderTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->targetGuildHeaderTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTransferButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->transferButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getTransferConfirmationTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->transferConfirmationTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0249

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 8

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getTransferConfirmationTextView()Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const v7, 0x7f1000da

    invoke-virtual {v1, v7, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v4, v5, v6

    const v7, 0x7f1000d9

    invoke-virtual {v1, v7, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f121344

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getPreviousGuildHeaderTextView()Landroid/widget/TextView;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v4, v5, v6

    const v7, 0x7f1000db

    invoke-virtual {v1, v7, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f121345

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getTargetGuildHeaderTextView()Landroid/widget/TextView;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v4, v5, v6

    const v7, 0x7f1000dc

    invoke-virtual {v1, v7, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f121347

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getTransferButton()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$onViewBound$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->getTransferButton()Landroid/widget/Button;

    move-result-object p1

    new-array v0, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v4, v2, v6

    const v4, 0x7f1000de

    invoke-virtual {v1, v4, v3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f12134d

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PREVIOUS_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "TARGET_GUILD_ID"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SLOT_ID"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SUBSCRIPTION_ID"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    move-object v4, v2

    invoke-direct/range {v4 .. v12}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;-><init>(JJJJ)V

    invoke-direct {v1, v2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Factory;-><init>(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;)V

    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(\n     \u2026essViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    const-string v0, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v2, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
