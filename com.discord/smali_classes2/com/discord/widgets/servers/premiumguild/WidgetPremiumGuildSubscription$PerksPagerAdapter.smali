.class public final Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "WidgetPremiumGuildSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PerksPagerAdapter"
.end annotation


# instance fields
.field private premiumTier:I

.field private subscriptionCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final configureViews(Landroidx/viewpager/widget/ViewPager;)V
    .locals 5

    const-string v0, "viewPager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "getChildAt(index)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type kotlin.Int"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    check-cast v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;

    iget v4, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->premiumTier:I

    invoke-virtual {v2, v3, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->configure(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    const-string p2, "container"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "view"

    invoke-static {p3, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final getPremiumTier()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->premiumTier:I

    return v0
.end method

.method public final getSubscriptionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->subscriptionCount:I

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7

    const-string v0, "container"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/lit8 p2, p2, 0x1

    new-instance v6, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "container.context"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->premiumTier:I

    invoke-virtual {v6, p2, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildSubscriptionPerkView;->configure(II)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v6, p2}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v6
.end method

.method public bridge synthetic instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "any"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final setPremiumTier(I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->premiumTier:I

    return-void
.end method

.method public final setSubscriptionCount(I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->subscriptionCount:I

    return-void
.end method
