.class public final Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;
.super Lcom/discord/app/AppFragment;
.source "WidgetPremiumGuildSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;,
        Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "GUILD_ID"

.field private static final VIEW_INDEX_LOADED:I = 0x2

.field private static final VIEW_INDEX_LOADING:I = 0x0

.field private static final VIEW_INDEX_LOAD_FAILED:I = 0x1


# instance fields
.field private final boostNumber$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostProgressBar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostProtipTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostStatusLearnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostSubscribeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostSubscribeButton2$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier0Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier1Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier1Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier2Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier2Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier3Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostTier3Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildId$delegate:Lkotlin/Lazy;

.field private levelBackgrounds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private levelText:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private pagerAdapter:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

.field private final premiumGuildSubscriptionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final premiumSubscriptionMarketingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final retry$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

.field private final viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private wasPagerPageSet:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x12

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v3, "boostNumber"

    const-string v4, "getBoostNumber()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostStatusLearnMore"

    const-string v7, "getBoostStatusLearnMore()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostProtipTv"

    const-string v7, "getBoostProtipTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostProgressBar"

    const-string v7, "getBoostProgressBar()Landroid/widget/ProgressBar;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier0Iv"

    const-string v7, "getBoostTier0Iv()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier1Iv"

    const-string v7, "getBoostTier1Iv()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier1Tv"

    const-string v7, "getBoostTier1Tv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier2Iv"

    const-string v7, "getBoostTier2Iv()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier2Tv"

    const-string v7, "getBoostTier2Tv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier3Iv"

    const-string v7, "getBoostTier3Iv()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostTier3Tv"

    const-string v7, "getBoostTier3Tv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostSubscribeButton"

    const-string v7, "getBoostSubscribeButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "boostSubscribeButton2"

    const-string v7, "getBoostSubscribeButton2()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "premiumGuildSubscriptionUpsellView"

    const-string v7, "getPremiumGuildSubscriptionUpsellView()Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "premiumSubscriptionMarketingView"

    const-string v7, "getPremiumSubscriptionMarketingView()Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "flipper"

    const-string v7, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "retry"

    const-string v7, "getRetry()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x11

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const-string v6, "viewPager"

    const-string v7, "getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0113

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostNumber$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0112

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostStatusLearnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0116

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostProtipTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0100

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostProgressBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0102

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier0Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0104

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier1Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0105

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier1Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0107

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier2Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0108

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier2Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a010a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier3Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a010b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier3Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0118

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostSubscribeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0119

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostSubscribeButton2$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0115

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->premiumGuildSubscriptionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0114

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->premiumSubscriptionMarketingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0111

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0117

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a011a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$guildId$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$guildId$2;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->guildId$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$configureLevelBubbles(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->configureLevelBubbles(I)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->configureUI(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getMostRecentIntent$p(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)Landroid/content/Intent;
    .locals 0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->handleEvent(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    return-void
.end method

.method private final configureLevelBubbles(I)V
    .locals 7

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-gt v0, v1, :cond_6

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->levelBackgrounds:Ljava/util/List;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/16 v4, 0x20

    const/16 v5, 0x14

    if-ne p1, v0, :cond_0

    const/16 v6, 0x20

    goto :goto_1

    :cond_0
    const/16 v6, 0x14

    :goto_1
    invoke-static {v6}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v6

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne p1, v0, :cond_1

    goto :goto_2

    :cond_1
    const/16 v4, 0x14

    :goto_2
    invoke-static {v4}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->levelText:Ljava/util/List;

    if-eqz v1, :cond_4

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_3

    if-ne p1, v0, :cond_2

    const v2, 0x7f04048d

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    goto :goto_3

    :cond_2
    const v2, 0x7f040498

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v2

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    const-string p1, "levelText"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    const-string p1, "levelBackgrounds"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_6
    return-void
.end method

.method private final configureProgressBar(II)V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;

    invoke-virtual {v1, p1, p2}, Lcom/discord/utilities/premium/PremiumGuildSubscriptionUtils;->calculateTotalProgress(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz p1, :cond_3

    if-eq p1, v4, :cond_2

    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    const v5, 0x7f1213b3

    goto :goto_0

    :cond_1
    const v5, 0x7f1213b2

    goto :goto_0

    :cond_2
    const v5, 0x7f1213b1

    goto :goto_0

    :cond_3
    const v5, 0x7f121322

    :goto_0
    invoke-virtual {p0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier0Iv()Landroid/widget/ImageView;

    move-result-object v0

    if-lez p2, :cond_4

    const/4 p2, 0x1

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier1Iv()Landroid/widget/ImageView;

    move-result-object p2

    if-lt p1, v4, :cond_5

    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier2Iv()Landroid/widget/ImageView;

    move-result-object p2

    if-lt p1, v2, :cond_6

    const/4 v0, 0x1

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier3Iv()Landroid/widget/ImageView;

    move-result-object p2

    if-lt p1, v1, :cond_7

    const/4 v3, 0x1

    :cond_7
    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void
.end method

.method private final configureToolbar(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f121331

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState;)V
    .locals 7

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loading;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Failure;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getRetry()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$configureUI$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$configureUI$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    :cond_2
    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_0
    const-string v4, "viewState.guild.premiumSubscriptionCount ?: 0"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->configureToolbar(Ljava/lang/String;)V

    invoke-direct {p0, v0, v3}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->configureProgressBar(II)V

    invoke-direct {p0, v0, v3}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->configureViewpager(II)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getPremiumSubscriptionMarketingView()Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v4

    if-eqz v4, :cond_4

    goto :goto_1

    :cond_4
    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_1
    const-string v5, "viewState.meUser.premium\u2026tionPlan.PremiumTier.NONE"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$configureUI$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$configureUI$2;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    invoke-virtual {v0, v4, v6}, Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;->a(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getPremiumGuildSubscriptionUpsellView()Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$ViewState$Loaded;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object p1

    if-eqz p1, :cond_5

    goto :goto_2

    :cond_5
    sget-object p1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_2
    invoke-static {p1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;->a(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostNumber()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v4, "resources"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "requireContext()"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1000ce

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v1

    invoke-static {v0, v4, v5, v3, v2}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final configureViewpager(II)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->pagerAdapter:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

    const/4 v1, 0x0

    const-string v2, "pagerAdapter"

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->setPremiumTier(I)V

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->pagerAdapter:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->setSubscriptionCount(I)V

    iget-object p2, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->pagerAdapter:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;->configureViews(Landroidx/viewpager/widget/ViewPager;)V

    iget-boolean p2, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->wasPagerPageSet:Z

    if-nez p2, :cond_0

    add-int/lit8 p2, p1, -0x1

    const/4 v0, 0x0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    const/4 p2, 0x1

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->configureLevelBubbles(I)V

    iput-boolean p2, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->wasPagerPageSet:Z

    :cond_0
    return-void

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public static final create(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$Companion;->create(Landroid/content/Context;J)V

    return-void
.end method

.method private final fetchData()V
    .locals 2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StorePremiumGuildSubscription;->fetchUserGuildPremiumState()V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    return-void
.end method

.method private final getBoostNumber()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostNumber$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostProgressBar()Landroid/widget/ProgressBar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostProgressBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private final getBoostProtipTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostProtipTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostStatusLearnMore()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostStatusLearnMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostSubscribeButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostSubscribeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBoostSubscribeButton2()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostSubscribeButton2$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBoostTier0Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier0Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier1Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier1Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier1Tv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier1Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostTier2Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier2Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier2Tv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier2Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostTier3Iv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier3Iv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostTier3Tv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->boostTier3Tv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getGuildId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->guildId$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private final getPremiumGuildSubscriptionUpsellView()Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->premiumGuildSubscriptionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;

    return-object v0
.end method

.method private final getPremiumSubscriptionMarketingView()Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->premiumSubscriptionMarketingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;

    return-object v0
.end method

.method private final getRetry()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->viewPager$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/simple_pager/SimplePager;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;)V
    .locals 25

    move-object/from16 v8, p0

    move-object/from16 v0, p1

    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    if-eqz v1, :cond_0

    sget-object v2, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionConfirmation;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionConfirmation$Companion;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    const-string v1, "requireContext()"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->getGuildId()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchSubscriptionConfirmation;->getSlotId()J

    move-result-wide v6

    invoke-virtual/range {v2 .. v7}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionConfirmation$Companion;->create(Landroid/content/Context;JJ)V

    goto/16 :goto_0

    :cond_0
    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;

    sget-object v2, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;->BUY_PREMIUM_GUILD:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->getOldSkuName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/discord/utilities/analytics/Traits$Location;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$LaunchPurchaseSubscription;->getSection()Ljava/lang/String;

    move-result-object v11

    const/4 v14, 0x0

    const/16 v15, 0x10

    const/16 v16, 0x0

    const-string v10, "User-Facing Premium Guild Subscription Fullscreen Modal"

    const-string v12, "Button CTA"

    const-string v13, "buy"

    move-object v9, v4

    invoke-direct/range {v9 .. v16}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v1

    move-object/from16 v1, p0

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;->launch$default(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;Lcom/discord/app/AppFragment;Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_1
    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowDesktopAlertDialog;

    const/4 v2, 0x0

    const-string v3, "parentFragmentManager"

    if-eqz v1, :cond_2

    sget-object v9, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v10

    invoke-static {v10, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f121394

    invoke-virtual {v8, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    const v0, 0x7f121393

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v3, Lf/a/b/g;->a:Lf/a/b/g;

    const-wide v4, 0x53d4f93245L

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v8, v0, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const-string v0, "getString(\n             \u2026OOGLE_PLAY)\n            )"

    invoke-static {v12, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f121377

    invoke-virtual {v8, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1ff0

    const/16 v24, 0x0

    invoke-static/range {v9 .. v24}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$UnacknowledgedPurchase;

    if-eqz v1, :cond_3

    const v0, 0x7f1202e6

    const/4 v1, 0x4

    invoke-static {v8, v0, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    goto :goto_0

    :cond_3
    instance-of v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    if-eqz v1, :cond_4

    sget-object v9, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v10

    invoke-static {v10, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->getHeaderStringRes()I

    move-result v1

    invoke-virtual {v8, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->getBodyStringRes()I

    move-result v0

    invoke-virtual {v8, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v0, "getString(event.bodyStringRes)"

    invoke-static {v12, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f120300

    invoke-virtual {v8, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    const v0, 0x7f1203f1

    invoke-virtual {v8, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    const v0, 0x7f0a06fc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$handleEvent$1;

    invoke-direct {v1, v8}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$handleEvent$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v2}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1fc0

    const/16 v24, 0x0

    invoke-static/range {v9 .. v24}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d025e

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/discord/utilities/simple_pager/SimplePager;->setWrapHeight(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->pagerAdapter:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/simple_pager/SimplePager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getViewPager()Lcom/discord/utilities/simple_pager/SimplePager;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onActivityCreated$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onActivityCreated$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void

    :cond_0
    const-string p1, "pagerAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p3, 0xfa0

    if-ne p1, p3, :cond_0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->fetchData()V

    :cond_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 16

    move-object/from16 v6, p0

    const-string v0, "view"

    move-object/from16 v7, p1

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object v2, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$1;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$1;

    const v1, 0x7f0e0012

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu$default(Lcom/discord/app/AppFragment;ILrx/functions/Action2;Lrx/functions/Action1;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostStatusLearnMore()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v8

    const-string v1, "requireContext()"

    invoke-static {v8, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "https://www.discord.com"

    aput-object v4, v2, v3

    const v5, 0x7f121329

    invoke-virtual {v6, v5, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v2, "getString(\n            R\u2026ww.discord.com\"\n        )"

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v10, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$2;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$2;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x18

    const/4 v14, 0x0

    invoke-static/range {v8 .. v14}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v3

    const v9, 0x7f1000d0

    invoke-virtual {v0, v9, v8, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "resources.getQuantityStr\u2026PTIONS_WITH_PREMIUM\n    )"

    invoke-static {v0, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v3

    aput-object v4, v5, v1

    const v0, 0x7f121338

    invoke-virtual {v6, v0, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostProtipTv()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v2, "view.context"

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v11, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$3;->INSTANCE:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$3;

    const v2, 0x7f060200

    invoke-static {v6, v2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/16 v14, 0x8

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x4

    new-array v2, v0, [Landroid/widget/ImageView;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier0Iv()Landroid/widget/ImageView;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier1Iv()Landroid/widget/ImageView;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier2Iv()Landroid/widget/ImageView;

    move-result-object v4

    aput-object v4, v2, v8

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier3Iv()Landroid/widget/ImageView;

    move-result-object v4

    const/4 v5, 0x3

    aput-object v4, v2, v5

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v6, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->levelBackgrounds:Ljava/util/List;

    new-array v0, v0, [Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v2, v0, v3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier1Tv()Landroid/widget/TextView;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier2Tv()Landroid/widget/TextView;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostTier3Tv()Landroid/widget/TextView;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v6, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->levelText:Ljava/util/List;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostSubscribeButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$4;

    invoke-direct {v1, v6}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$4;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getBoostSubscribeButton2()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$5;

    invoke-direct {v1, v6}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBound$5;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

    invoke-direct {v0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;-><init>()V

    iput-object v0, v6, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->pagerAdapter:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$PerksPagerAdapter;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->fetchData()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getGuildId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getGuildId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->getGuildId()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Factory;-><init>(J)V

    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(requir\u2026ildViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    iput-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    const/4 v1, 0x0

    const-string v2, "viewModel"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v3, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBoundOrOnResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscription;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
