.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;
.super Ljava/lang/Object;
.source "PremiumGuildTransferInProgressViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation


# instance fields
.field private final previousGuildId:J

.field private final slotId:J

.field private final subscriptionId:J

.field private final targetGuildId:J


# direct methods
.method public constructor <init>(JJJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    iput-wide p3, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    iput-wide p5, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    iput-wide p7, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;JJJJILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;
    .locals 9

    move-object v0, p0

    and-int/lit8 v1, p9, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p9, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    goto :goto_1

    :cond_1
    move-wide v3, p3

    :goto_1
    and-int/lit8 v5, p9, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    goto :goto_2

    :cond_2
    move-wide v5, p5

    :goto_2
    and-int/lit8 v7, p9, 0x8

    if-eqz v7, :cond_3

    iget-wide v7, v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    goto :goto_3

    :cond_3
    move-wide/from16 v7, p7

    :goto_3
    move-wide p1, v1

    move-wide p3, v3

    move-wide p5, v5

    move-wide/from16 p7, v7

    invoke-virtual/range {p0 .. p8}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->copy(JJJJ)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    return-wide v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    return-wide v0
.end method

.method public final copy(JJJJ)Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;
    .locals 10

    new-instance v9, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    move-object v0, v9

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;-><init>(JJJJ)V

    return-object v9
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    iget-wide v2, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPreviousGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    return-wide v0
.end method

.method public final getSlotId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    return-wide v0
.end method

.method public final getSubscriptionId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    return-wide v0
.end method

.method public final getTargetGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Config(previousGuildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->previousGuildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", targetGuildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->targetGuildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", slotId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->slotId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildTransferInProgressViewModel$Config;->subscriptionId:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
