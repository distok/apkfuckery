.class public final Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;
.super Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.source "PremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowBlockedPlanSwitchAlertDialog"
.end annotation


# instance fields
.field private final bodyStringRes:I

.field private final headerStringRes:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    iput p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;IIILjava/lang/Object;)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->copy(II)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    return v0
.end method

.method public final copy(II)Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    iget v1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    iget p1, p1, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBodyStringRes()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    return v0
.end method

.method public final getHeaderStringRes()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ShowBlockedPlanSwitchAlertDialog(headerStringRes="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->headerStringRes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bodyStringRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/servers/premiumguild/PremiumGuildViewModel$Event$ShowBlockedPlanSwitchAlertDialog;->bodyStringRes:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
