.class public final Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;
.super Ljava/lang/Object;
.source "WidgetPremiumGuildSubscriptionTransfer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;-><init>()V

    return-void
.end method

.method public static synthetic create$default(Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;Landroid/content/Context;JJLcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;ILjava/lang/Object;)V
    .locals 7

    and-int/lit8 p7, p7, 0x8

    if-eqz p7, :cond_0

    const/4 p6, 0x0

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;->create(Landroid/content/Context;JJLcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V

    return-void
.end method


# virtual methods
.method public final create(Landroid/content/Context;JJLcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "PREVIOUS_GUILD_ID"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p2

    const-string p3, "TARGET_GUILD_ID"

    invoke-virtual {p2, p3, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p2

    const/4 p3, 0x0

    if-eqz p6, :cond_0

    invoke-virtual {p6}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getId()J

    move-result-wide p4

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    goto :goto_0

    :cond_0
    move-object p4, p3

    :goto_0
    const-string p5, "SLOT_ID"

    invoke-virtual {p2, p5, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object p2

    if-eqz p6, :cond_1

    invoke-virtual {p6}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object p4

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getId()J

    move-result-wide p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    :cond_1
    const-string p4, "SUBSCRIPTION_ID"

    invoke-virtual {p2, p4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object p2

    const-string p3, "Intent()\n          .putE\u2026iumGuildSubscription?.id)"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class p3, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;

    invoke-static {p1, p3, p2}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method
