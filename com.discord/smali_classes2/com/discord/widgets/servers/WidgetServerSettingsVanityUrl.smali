.class public Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;
.super Lcom/discord/app/AppFragment;
.source "WidgetServerSettingsVanityUrl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION:J = 0xfaL

.field private static final INTENT_EXTRA_GUILD_ID:Ljava/lang/String; = "INTENT_EXTRA_GUILD_ID"


# instance fields
.field private currentUrl:Landroid/widget/TextView;

.field private errorText:Landroid/widget/TextView;

.field private loadingIndicator:Landroid/widget/ProgressBar;

.field private remove:Landroid/widget/TextView;

.field private save:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private state:Lcom/discord/utilities/stateful/StatefulViews;

.field private urlPrefix:Landroid/widget/TextView;

.field private vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

.field private vanityInputContainer:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    new-instance v0, Lcom/discord/utilities/stateful/StatefulViews;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0a0929

    aput v3, v1, v2

    invoke-direct {v0, v1}, Lcom/discord/utilities/stateful/StatefulViews;-><init>([I)V

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->state:Lcom/discord/utilities/stateful/StatefulViews;

    return-void
.end method

.method private configureInviteCode(Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v2

    invoke-virtual {v1, v2, p1}, Lcom/discord/utilities/stateful/StatefulViews;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearFocus()V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->currentUrl:Landroid/widget/TextView;

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "https://discord.gg/"

    invoke-static {v0, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->currentUrl:Landroid/widget/TextView;

    const v4, 0x7f1219fd

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v3, v4, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;I[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->currentUrl:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->currentUrl:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->remove:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    return-void
.end method

.method private configureToolbar(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f121698

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;)V
    .locals 2

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->showLoadingUI(Z)V

    iget-object v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;->guildName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->configureToolbar(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;->vanityUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->configureInviteCode(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->state:Lcom/discord/utilities/stateful/StatefulViews;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->save:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {v0, v1}, Lcom/discord/utilities/stateful/StatefulViews;->configureSaveActionView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->save:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    new-instance v1, Lf/a/o/e/w2;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/w2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->remove:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    new-instance v1, Lf/a/o/e/y2;

    invoke-direct {v1, p0, p1}, Lf/a/o/e/y2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void
.end method

.method public static create(Landroid/content/Context;J)V
    .locals 2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    const-string v1, "VANITY_URL"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/stores/StoreAnalytics;->onGuildSettingsPaneViewed(Ljava/lang/String;J)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p1

    const-class p2, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;

    invoke-static {p0, p2, p1}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic j(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->configureUI(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;)V

    return-void
.end method

.method private onUpdatedVanityUrl(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->showLoadingUI(Z)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->hideKeyboard()V

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->configureInviteCode(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {p1}, Lcom/discord/utilities/stateful/StatefulViews;->clear()V

    return-void
.end method

.method private showLoadingUI(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->loadingIndicator:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v1, 0x4

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->errorText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->remove:Landroid/widget/TextView;

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method private showUrlPrefix(Z)Lkotlin/Unit;
    .locals 2

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->urlPrefix:Landroid/widget/TextView;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {v1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->translateToOriginX(Landroid/view/View;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->translateToOriginX(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result p1

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->urlPrefix:Landroid/widget/TextView;

    invoke-static {v1, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->translateLeft(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v1, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->translateLeft(Landroid/view/View;I)V

    :goto_0
    return-object v0
.end method

.method private static translateLeft(Landroid/view/View;I)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    neg-int p1, p1

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->translationXBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const-wide/16 v0, 0xfa

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    new-instance p1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private static translateToOriginX(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const-wide/16 v0, 0xfa

    invoke-virtual {p0, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v0}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private updateVanityUrl(JLjava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->showLoadingUI(Z)V

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v1, Lcom/discord/restapi/RestAPIParams$VanityUrl;

    invoke-direct {v1, p3}, Lcom/discord/restapi/RestAPIParams$VanityUrl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/utilities/rest/RestAPI;->updateVanityUrl(JLcom/discord/restapi/RestAPIParams$VanityUrl;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lf/a/o/e/a3;

    invoke-direct {p2, p0}, Lf/a/o/e/a3;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p3

    new-instance v0, Lf/a/o/e/u2;

    invoke-direct {v0, p0}, Lf/a/o/e/u2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;)V

    invoke-static {p2, p3, v0}, Lf/a/b/r;->k(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method


# virtual methods
.method public synthetic f(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;Landroid/view/View;)V
    .locals 1

    iget-wide p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;->guildId:J

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->updateVanityUrl(JLjava/lang/String;)V

    return-void
.end method

.method public synthetic g(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;Landroid/view/View;)V
    .locals 1

    iget-wide p1, p1, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;->guildId:J

    const-string v0, ""

    invoke-direct {p0, p1, p2, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->updateVanityUrl(JLjava/lang/String;)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d028d

    return v0
.end method

.method public synthetic h(Landroid/view/View;Z)V
    .locals 0

    xor-int/lit8 p1, p2, 0x1

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->showUrlPrefix(Z)Lkotlin/Unit;

    return-void
.end method

.method public synthetic i(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->showKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic k(Lcom/discord/models/domain/ModelGuild$VanityUrlResponse;)V
    .locals 0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild$VanityUrlResponse;->getCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->onUpdatedVanityUrl(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic l(Lcom/discord/utilities/error/Error;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->showLoadingUI(Z)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/error/Error$Response;->getCode()I

    move-result v1

    const v2, 0xc364

    if-ne v1, v2, :cond_0

    invoke-virtual {p1, v0}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->errorText:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_GUILD_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->showLoadingUI(Z)V

    invoke-static {v0, v1}, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl$Model;->access$000(J)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/e/z2;

    invoke-direct {v1, p0}, Lf/a/o/e/z2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a0929

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f0a092c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInputContainer:Landroid/view/View;

    const v0, 0x7f0a092d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->loadingIndicator:Landroid/widget/ProgressBar;

    const v0, 0x7f0a092e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->urlPrefix:Landroid/widget/TextView;

    const v0, 0x7f0a092b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->errorText:Landroid/widget/TextView;

    const v0, 0x7f0a092a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->currentUrl:Landroid/widget/TextView;

    const v0, 0x7f0a092f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->remove:Landroid/widget/TextView;

    const v0, 0x7f0a0930

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    iput-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->save:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled()Landroidx/appcompat/widget/Toolbar;

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v0, Lf/a/o/e/t2;

    invoke-direct {v0, p0}, Lf/a/o/e/t2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditTextFocusChangeListener(Lcom/google/android/material/textfield/TextInputLayout;Landroid/view/View$OnFocusChangeListener;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInputContainer:Landroid/view/View;

    new-instance v0, Lf/a/o/e/x2;

    invoke-direct {v0, p0}, Lf/a/o/e/x2;-><init>(Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->state:Lcom/discord/utilities/stateful/StatefulViews;

    invoke-virtual {p1, p0}, Lcom/discord/utilities/stateful/StatefulViews;->setupUnsavedChangesConfirmation(Lcom/discord/app/AppFragment;)V

    iget-object p1, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->state:Lcom/discord/utilities/stateful/StatefulViews;

    iget-object v0, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->save:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, p0, Lcom/discord/widgets/servers/WidgetServerSettingsVanityUrl;->vanityInput:Lcom/google/android/material/textfield/TextInputLayout;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, p0, v0, v1}, Lcom/discord/utilities/stateful/StatefulViews;->setupTextWatcherWithSaveAction(Lcom/discord/app/AppFragment;Landroid/view/View;[Landroid/view/View;)V

    return-void
.end method
