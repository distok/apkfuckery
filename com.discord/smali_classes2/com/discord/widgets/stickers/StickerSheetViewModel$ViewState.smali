.class public final Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;
.super Ljava/lang/Object;
.source "StickerSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/stickers/StickerSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final isBeingPurchased:Z

.field private final isPackOwned:Ljava/lang/Boolean;

.field private final meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field private final sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field private final stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;


# direct methods
.method public constructor <init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/sticker/dto/ModelSticker;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/Boolean;)V
    .locals 1

    const-string v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sticker"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUserPremiumTier"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iput-object p2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iput-boolean p3, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    iput-object p4, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iput-object p5, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/sticker/dto/ModelSticker;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->copy(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/sticker/dto/ModelSticker;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/Boolean;)Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public final component2()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    return v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final component5()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/sticker/dto/ModelSticker;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/Boolean;)Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;
    .locals 7

    const-string v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sticker"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUserPremiumTier"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/sticker/dto/ModelSticker;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iget-object v1, p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iget-object v1, p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    iget-boolean v1, p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object v1, p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMeUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final getSticker()Lcom/discord/models/sticker/dto/ModelSticker;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    return-object v0
.end method

.method public final getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/sticker/dto/ModelStickerPack;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/sticker/dto/ModelSticker;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isBeingPurchased()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    return v0
.end method

.method public final isPackOwned()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final isUserPremiumTier2()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ViewState(stickerPack="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sticker="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isBeingPurchased="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isBeingPurchased:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", meUserPremiumTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isPackOwned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isPackOwned:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
