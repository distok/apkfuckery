.class public final Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;
.super Ljava/lang/Object;
.source "WidgetStickerSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/stickers/WidgetStickerSheet;->configureUI(Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $location:Ljava/lang/String;

.field public final synthetic $meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

.field public final synthetic $stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

.field public final synthetic this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/stickers/WidgetStickerSheet;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    iput-object p2, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->$stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    iput-object p3, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->$meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iput-object p4, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->$location:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 14

    :try_start_0
    sget-object v0, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const-string p1, "requireActivity()"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string p1, "parentFragmentManager"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->$stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object v5, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->$meUserPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    new-instance p1, Lcom/discord/utilities/analytics/Traits$Location;

    iget-object v7, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->$location:Ljava/lang/String;

    sget-object v6, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v6}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Sticker Popout Purchase Button"

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x18

    const/4 v13, 0x0

    move-object v6, p1

    invoke-direct/range {v6 .. v13}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/discord/utilities/dsti/StickerUtils;->claimOrPurchaseStickerPack(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lcom/discord/utilities/analytics/Traits$Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const v0, 0x7f1206e8

    invoke-virtual {p1, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {p1, v0, v1, v2}, Lf/a/b/p;->l(Landroidx/fragment/app/Fragment;Ljava/lang/CharSequence;II)V

    :goto_0
    return-void
.end method
