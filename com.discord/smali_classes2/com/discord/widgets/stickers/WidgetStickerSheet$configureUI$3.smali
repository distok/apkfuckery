.class public final Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;
.super Ljava/lang/Object;
.source "WidgetStickerSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/stickers/WidgetStickerSheet;->configureUI(Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $isPackOwned:Ljava/lang/Boolean;

.field public final synthetic $location:Ljava/lang/String;

.field public final synthetic $sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field public final synthetic $stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

.field public final synthetic this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/stickers/WidgetStickerSheet;Ljava/lang/Boolean;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPack;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    iput-object p2, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$isPackOwned:Ljava/lang/Boolean;

    iput-object p3, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iput-object p4, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$location:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$isPackOwned:Ljava/lang/Boolean;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getExpressionPickerNavigation()Lcom/discord/stores/StoreExpressionPickerNavigation;

    move-result-object p1

    new-instance v0, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$OpenStickerPicker;

    iget-object v1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;->OWNED_PACKS:Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;

    invoke-direct {v0, v1, v2}, Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent$OpenStickerPicker;-><init>(Ljava/lang/Long;Lcom/discord/widgets/chat/input/sticker/StickerPickerScreen;)V

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreExpressionPickerNavigation;->createExpressionPickerEvent(Lcom/discord/stores/StoreExpressionPickerNavigation$ExpressionPickerEvent;)V

    goto :goto_0

    :cond_0
    sget-object v3, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet;->Companion:Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;

    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v4

    const-string p1, "parentFragmentManager"

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iget-object v7, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$location:Ljava/lang/String;

    sget-object p1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->Companion:Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$isPackOwned:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->$stickerPack:Lcom/discord/models/sticker/dto/ModelStickerPack;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelStickerPack;->canBePurchased()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;->getPopoutPurchaseLocation(Ljava/lang/Boolean;Z)Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    move-result-object v8

    sget-object v6, Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;->STICKER_POPOUT_VIEW_ALL:Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;

    invoke-virtual/range {v3 .. v8}, Lcom/discord/widgets/chat/input/sticker/WidgetStickerPackStoreSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/widgets/chat/input/sticker/StickerPackStoreSheetViewType;Ljava/lang/String;Lcom/discord/widgets/stickers/StickerPurchaseLocation;)V

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;->this$0:Lcom/discord/widgets/stickers/WidgetStickerSheet;

    invoke-virtual {p1}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method
