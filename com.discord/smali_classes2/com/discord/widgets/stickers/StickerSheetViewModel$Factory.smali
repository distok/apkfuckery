.class public final Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "StickerSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/stickers/StickerSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final location:Ljava/lang/String;

.field private final sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field private final storeStickers:Lcom/discord/stores/StoreStickers;

.field private final storeUser:Lcom/discord/stores/StoreUser;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUser;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;)V
    .locals 1

    const-string v0, "storeStickers"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeUser"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sticker"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iput-object p2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p3, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iput-object p4, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->location:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUser;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getStickers()Lcom/discord/stores/StoreStickers;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p2

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUser;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreStickers;->observeStickerPack(J)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStickers;->observeOwnedStickerPacks()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStickers;->observePurchasingStickerPacks()Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v3}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;

    invoke-static {v0, v1, v2, v3, v4}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026cks\n          )\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/stickers/StickerSheetViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->storeStickers:Lcom/discord/stores/StoreStickers;

    iget-object v2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iget-object v3, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->location:Ljava/lang/String;

    invoke-direct {p1, v0, v2, v1, v3}, Lcom/discord/widgets/stickers/StickerSheetViewModel;-><init>(Lrx/Observable;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/stores/StoreStickers;Ljava/lang/String;)V

    return-object p1
.end method
