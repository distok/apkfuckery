.class public final Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "StickerSheetViewModel.kt"

# interfaces
.implements Lrx/functions/Func4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func4<",
        "Lcom/discord/stores/StoreStickers$StickerPackState;",
        "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/Long;",
        ">;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Lcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreStickers$StickerPackState;",
            "Lcom/discord/stores/StoreStickers$OwnedStickerPackState;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;"
        }
    .end annotation

    const-string v0, "stickerPack"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ownedStickerPackState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasingPacks"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "meUser"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;-><init>(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Lcom/discord/models/domain/ModelUser;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreStickers$StickerPackState;

    check-cast p2, Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    check-cast p3, Ljava/util/Set;

    check-cast p4, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory$observeStoreState$1;->call(Lcom/discord/stores/StoreStickers$StickerPackState;Lcom/discord/stores/StoreStickers$OwnedStickerPackState;Ljava/util/Set;Lcom/discord/models/domain/ModelUser;)Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
