.class public final Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;
.super Ljava/lang/Object;
.source "StickerPurchaseLocation.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/stickers/StickerPurchaseLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getPopoutPurchaseLocation(Ljava/lang/Boolean;Z)Lcom/discord/widgets/stickers/StickerPurchaseLocation;
    .locals 1

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    sget-object p1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_UPSELL_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    :goto_0
    return-object p1
.end method

.method public final getSimplifiedLocation(Lcom/discord/widgets/stickers/StickerPurchaseLocation;)Lcom/discord/widgets/stickers/StickerPurchaseLocation;
    .locals 2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    :goto_0
    return-object p1
.end method
