.class public final Lcom/discord/widgets/stickers/WidgetStickerSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetStickerSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ANALYTICS_LOCATION:Ljava/lang/String; = "widget_sticker_sheet_analytics_location"

.field public static final Companion:Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;


# instance fields
.field private final buyButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final infoTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final limitedChip$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final limitedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final nameTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerView1$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerView2$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerView3$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickerView4$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/stickers/StickerSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xa

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v3, "nameTv"

    const-string v4, "getNameTv()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "infoTv"

    const-string v7, "getInfoTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "buyButton"

    const-string v7, "getBuyButton()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "viewButton"

    const-string v7, "getViewButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "limitedContainer"

    const-string v7, "getLimitedContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "limitedChip"

    const-string v7, "getLimitedChip()Lcom/google/android/material/chip/Chip;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "stickerView1"

    const-string v7, "getStickerView1()Lcom/discord/views/sticker/StickerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "stickerView2"

    const-string v7, "getStickerView2()Lcom/discord/views/sticker/StickerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "stickerView3"

    const-string v7, "getStickerView3()Lcom/discord/views/sticker/StickerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const-string v6, "stickerView4"

    const-string v7, "getStickerView4()Lcom/discord/views/sticker/StickerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->Companion:Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0a4e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->nameTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a4d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->infoTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a4c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->buyButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a51

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->viewButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a53

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->limitedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a52

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->limitedChip$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a63

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView1$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a64

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView2$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a65

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView3$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a66

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView4$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/stickers/WidgetStickerSheet;Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->configureUI(Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;)V
    .locals 20

    move-object/from16 v6, p0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->component1()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->component2()Lcom/discord/models/sticker/dto/ModelSticker;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->component3()Z

    move-result v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->component4()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->component5()Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getNameTv()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->canBePurchased()Z

    move-result v5

    const/4 v8, 0x2

    const-string v9, "requireContext()"

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    if-nez v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f121723

    new-array v14, v10, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, v11

    invoke-virtual {v6, v5, v14}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const-string v5, "getString(R.string.stick\u2026ilable, stickerPack.name)"

    invoke-static {v14, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v15, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$infoText$1;->INSTANCE:Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$infoText$1;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x18

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    goto/16 :goto_1

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isUserPremiumTier2()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v4, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isPremiumPack()Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f121721

    new-array v14, v8, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, v11

    sget-object v15, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual {v15}, Lcom/discord/utilities/dsti/StickerUtils;->calculatePremiumStickerPackDiscount()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v14, v10

    invoke-virtual {v6, v5, v14}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v14, "getString(\n             \u2026.toString()\n            )"

    invoke-static {v5, v14}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v14, 0x4

    const-string v15, "onClick"

    const-string v8, "https://discord.com"

    invoke-static {v5, v15, v8, v11, v14}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v14

    sget-object v15, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$infoText$3;->INSTANCE:Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$infoText$3;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x18

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f121722

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v8, v11

    invoke-virtual {v6, v5, v8}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const-string v5, "getString(R.string.stick\u2026remium, stickerPack.name)"

    invoke-static {v14, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v15, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$infoText$2;->INSTANCE:Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$infoText$2;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x18

    const/16 v19, 0x0

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;->isUserPremiumTier2()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getInfoTv()Landroid/widget/TextView;

    move-result-object v8

    new-instance v13, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$1;

    invoke-direct {v13, v6}, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$1;-><init>(Lcom/discord/widgets/stickers/WidgetStickerSheet;)V

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getInfoTv()Landroid/widget/TextView;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getStickerView1()Lcom/discord/views/sticker/StickerView;

    move-result-object v5

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v5, v8, v12}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getStickerView2()Lcom/discord/views/sticker/StickerView;

    move-result-object v5

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v5, v8, v12}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getStickerView3()Lcom/discord/views/sticker/StickerView;

    move-result-object v5

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v8

    const/4 v13, 0x2

    invoke-interface {v8, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v5, v8, v12}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getStickerView4()Lcom/discord/views/sticker/StickerView;

    move-result-object v5

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStickers()Ljava/util/List;

    move-result-object v8

    const/4 v13, 0x3

    invoke-interface {v8, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v5, v8, v12}, Lcom/discord/views/sticker/StickerView;->h(Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    nop

    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_4

    const-string/jumbo v8, "widget_sticker_sheet_analytics_location"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    :goto_3
    sget-object v8, Lcom/discord/utilities/dsti/StickerUtils;->INSTANCE:Lcom/discord/utilities/dsti/StickerUtils;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8, v12, v7, v2}, Lcom/discord/utilities/dsti/StickerUtils;->getStickerPackPremiumPriceLabel(Landroid/content/Context;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getBuyButton()Lcom/discord/views/LoadingButton;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/discord/views/LoadingButton;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getBuyButton()Lcom/discord/views/LoadingButton;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getBuyButton()Lcom/discord/views/LoadingButton;

    move-result-object v1

    new-instance v9, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;

    invoke-direct {v9, v6, v7, v2, v5}, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$2;-><init>(Lcom/discord/widgets/stickers/WidgetStickerSheet;Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getBuyButton()Lcom/discord/views/LoadingButton;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v4, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->canBePurchased()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    const/16 v9, 0x8

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    goto :goto_5

    :cond_6
    const/16 v2, 0x8

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getViewButton()Landroid/view/View;

    move-result-object v1

    invoke-static {v4, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->canBePurchased()Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_6

    :cond_7
    const/4 v10, 0x0

    :cond_8
    :goto_6
    if-eqz v10, :cond_9

    const/4 v0, 0x0

    goto :goto_7

    :cond_9
    const/16 v0, 0x8

    :goto_7
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getViewButton()Landroid/view/View;

    move-result-object v10

    new-instance v12, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;

    move-object v0, v12

    move-object/from16 v1, p0

    move-object v2, v4

    move-object v4, v5

    move-object v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/stickers/WidgetStickerSheet$configureUI$3;-><init>(Lcom/discord/widgets/stickers/WidgetStickerSheet;Ljava/lang/Boolean;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;Lcom/discord/models/sticker/dto/ModelStickerPack;)V

    invoke-virtual {v10, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getLimitedContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->isLimitedPack()Z

    move-result v1

    if-eqz v1, :cond_a

    goto :goto_8

    :cond_a
    const/16 v11, 0x8

    :goto_8
    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet;->getLimitedChip()Lcom/google/android/material/chip/Chip;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/discord/models/sticker/dto/ModelStickerPack;->getStoreListing()Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lcom/discord/utilities/dsti/StickerUtils;->getLimitedTimeLeftString(Landroid/content/res/Resources;Lcom/discord/models/sticker/dto/ModelStickerPackStoreListing;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final getBuyButton()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->buyButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getInfoTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->infoTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getLimitedChip()Lcom/google/android/material/chip/Chip;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->limitedChip$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/chip/Chip;

    return-object v0
.end method

.method private final getLimitedContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->limitedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNameTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->nameTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getStickerView1()Lcom/discord/views/sticker/StickerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView1$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    return-object v0
.end method

.method private final getStickerView2()Lcom/discord/views/sticker/StickerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView2$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    return-object v0
.end method

.method private final getStickerView3()Lcom/discord/views/sticker/StickerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView3$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    return-object v0
.end method

.method private final getStickerView4()Lcom/discord/views/sticker/StickerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->stickerView4$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/sticker/StickerView;

    return-object v0
.end method

.method private final getViewButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->viewButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->Companion:Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/discord/widgets/stickers/WidgetStickerSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;Lcom/discord/models/sticker/dto/ModelSticker;J)V

    return-void
.end method


# virtual methods
.method public bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
    .locals 9

    const-string v0, "compositeSubscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->viewModel:Lcom/discord/widgets/stickers/StickerSheetViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string p1, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v1, Lcom/discord/widgets/stickers/WidgetStickerSheet;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/stickers/WidgetStickerSheet$bindSubscriptions$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/stickers/WidgetStickerSheet$bindSubscriptions$1;-><init>(Lcom/discord/widgets/stickers/WidgetStickerSheet;)V

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02b7

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    const-string v0, "inflater"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "com.discord.intent.extra.EXTRA_STICKER"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lcom/discord/models/sticker/dto/ModelSticker;

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    move-object v5, v0

    check-cast v5, Lcom/discord/models/sticker/dto/ModelSticker;

    if-nez v5, :cond_2

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-object v1

    :cond_2
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v9, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string/jumbo v1, "widget_sticker_sheet_analytics_location"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    move-object v6, v1

    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;-><init>(Lcom/discord/stores/StoreStickers;Lcom/discord/stores/StoreUser;Lcom/discord/models/sticker/dto/ModelSticker;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, p0, v9}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/stickers/StickerSheetViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;

    iput-object v0, p0, Lcom/discord/widgets/stickers/WidgetStickerSheet;->viewModel:Lcom/discord/widgets/stickers/StickerSheetViewModel;

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppBottomSheet;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
