.class public final enum Lcom/discord/widgets/stickers/StickerPurchaseLocation;
.super Ljava/lang/Enum;
.source "StickerPurchaseLocation.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/stickers/StickerPurchaseLocation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/stickers/StickerPurchaseLocation;

.field public static final Companion:Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;

.field public static final enum EXPRESSION_PICKER:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

.field public static final enum STICKER_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

.field public static final enum STICKER_UPSELL_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;


# instance fields
.field private final analyticsValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    new-instance v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    const-string v2, "EXPRESSION_PICKER"

    const/4 v3, 0x0

    const-string v4, "Expression Picker"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->EXPRESSION_PICKER:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    const-string v2, "STICKER_POPOUT"

    const/4 v3, 0x1

    const-string v4, "Sticker Popout"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    const-string v2, "STICKER_UPSELL_POPOUT"

    const/4 v3, 0x2

    const-string v4, "Sticker Upsell Popout"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->STICKER_UPSELL_POPOUT:Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->$VALUES:[Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    new-instance v0, Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->Companion:Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->analyticsValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/stickers/StickerPurchaseLocation;
    .locals 1

    const-class v0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/stickers/StickerPurchaseLocation;
    .locals 1

    sget-object v0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->$VALUES:[Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    invoke-virtual {v0}, [Lcom/discord/widgets/stickers/StickerPurchaseLocation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->analyticsValue:Ljava/lang/String;

    return-object v0
.end method
