.class public final Lcom/discord/widgets/stickers/StickerSheetViewModel;
.super Lf/a/b/l0;
.source "StickerSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;,
        Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;,
        Lcom/discord/widgets/stickers/StickerSheetViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private hasFiredAnalytics:Z

.field private final location:Ljava/lang/String;

.field private final sticker:Lcom/discord/models/sticker/dto/ModelSticker;

.field private final stickersStore:Lcom/discord/stores/StoreStickers;


# direct methods
.method public constructor <init>(Lrx/Observable;Lcom/discord/models/sticker/dto/ModelSticker;Lcom/discord/stores/StoreStickers;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;",
            ">;",
            "Lcom/discord/models/sticker/dto/ModelSticker;",
            "Lcom/discord/stores/StoreStickers;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sticker"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stickersStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    iput-object p3, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    iput-object p4, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->location:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/widgets/stickers/StickerSheetViewModel;->fetchStickersData()V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/stickers/StickerSheetViewModel;

    new-instance v7, Lcom/discord/widgets/stickers/StickerSheetViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/stickers/StickerSheetViewModel$1;-><init>(Lcom/discord/widgets/stickers/StickerSheetViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/stickers/StickerSheetViewModel;Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel;->handleStoreState(Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V

    return-void
.end method

.method private final fetchStickersData()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStickers;->fetchOwnedStickerPacks()V

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    iget-object v1, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v1}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreStickers;->fetchStickerPack(J)V

    iget-object v0, p0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->stickersStore:Lcom/discord/stores/StoreStickers;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreStickers;->fetchStickerStoreDirectory(Z)V

    return-void
.end method

.method private final handleLoadedStoreState(Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V
    .locals 20

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;->getOwnedStickerPackState()Lcom/discord/stores/StoreStickers$OwnedStickerPackState;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;->getStickerPack()Lcom/discord/stores/StoreStickers$StickerPackState;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type com.discord.stores.StoreStickers.StickerPackState.Loaded"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v3, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;->getStickerPack()Lcom/discord/models/sticker/dto/ModelStickerPack;

    move-result-object v5

    instance-of v3, v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    if-eqz v3, :cond_0

    check-cast v1, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStickers$OwnedStickerPackState$Loaded;->getOwnedStickerPacks()Ljava/util/Map;

    move-result-object v1

    iget-object v3, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v9, v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;->getPurchasingPacks()Ljava/util/Set;

    move-result-object v1

    iget-object v3, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v3}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    iget-boolean v1, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->hasFiredAnalytics:Z

    if-nez v1, :cond_1

    if-eqz v9, :cond_1

    iget-object v1, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->location:Ljava/lang/String;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->Companion:Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;

    invoke-virtual {v5}, Lcom/discord/models/sticker/dto/ModelStickerPack;->canBePurchased()Z

    move-result v3

    invoke-virtual {v1, v9, v3}, Lcom/discord/widgets/stickers/StickerPurchaseLocation$Companion;->getPopoutPurchaseLocation(Ljava/lang/Boolean;Z)Lcom/discord/widgets/stickers/StickerPurchaseLocation;

    move-result-object v1

    sget-object v3, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v4, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v4}, Lcom/discord/models/sticker/dto/ModelSticker;->getPackId()J

    move-result-wide v10

    iget-object v4, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->location:Ljava/lang/String;

    new-instance v6, Lcom/discord/utilities/analytics/Traits$Location;

    const/4 v13, 0x0

    invoke-virtual {v1}, Lcom/discord/widgets/stickers/StickerPurchaseLocation;->getAnalyticsValue()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1d

    const/16 v19, 0x0

    move-object v12, v6

    invoke-direct/range {v12 .. v19}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v10, v11, v4, v6}, Lcom/discord/utilities/analytics/AnalyticsTracker;->stickerPopoutOpened(JLjava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->hasFiredAnalytics:Z

    :cond_1
    new-instance v1, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;

    iget-object v6, v0, Lcom/discord/widgets/stickers/StickerSheetViewModel;->sticker:Lcom/discord/models/sticker/dto/ModelSticker;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_1
    move-object v8, v2

    const-string v2, "meUser.premiumTier ?: Mo\u2026tionPlan.PremiumTier.NONE"

    invoke-static {v8, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v1

    invoke-direct/range {v4 .. v9}, Lcom/discord/widgets/stickers/StickerSheetViewModel$ViewState;-><init>(Lcom/discord/models/sticker/dto/ModelStickerPack;Lcom/discord/models/sticker/dto/ModelSticker;ZLcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Ljava/lang/Boolean;)V

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;->getStickerPack()Lcom/discord/stores/StoreStickers$StickerPackState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreStickers$StickerPackState$Loaded;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/discord/widgets/stickers/StickerSheetViewModel;->handleLoadedStoreState(Lcom/discord/widgets/stickers/StickerSheetViewModel$StoreState;)V

    :cond_0
    return-void
.end method
