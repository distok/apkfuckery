.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;
.super Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.source "WidgetVoiceBottomSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowCameraCapacityDialog"
.end annotation


# instance fields
.field private final guildMaxVideoChannelUsers:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;IILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->copy(I)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    return v0
.end method

.method public final copy(I)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    invoke-direct {v0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    iget p1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildMaxVideoChannelUsers()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ShowCameraCapacityDialog(guildMaxVideoChannelUsers="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->guildMaxVideoChannelUsers:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
