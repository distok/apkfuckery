.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetVoiceBottomSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final callsStore:Lcom/discord/stores/StoreCalls;

.field private final channelId:J

.field private final experimentStore:Lcom/discord/stores/StoreExperiments;

.field private final forwardToFullscreenIfVideoActivated:Z

.field private final mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

.field private final mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

.field private final permissionsStore:Lcom/discord/stores/StorePermissions;

.field private final selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private final userSettingsStore:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public constructor <init>(JZLcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreCalls;)V
    .locals 1

    const-string v0, "experimentStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaSettingsStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedVoiceChannelStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaEngineStore"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userSettingsStore"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionsStore"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callsStore"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->channelId:J

    iput-boolean p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->forwardToFullscreenIfVideoActivated:Z

    iput-object p4, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->experimentStore:Lcom/discord/stores/StoreExperiments;

    iput-object p5, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    iput-object p6, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-object p7, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    iput-object p8, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->userSettingsStore:Lcom/discord/stores/StoreUserSettings;

    iput-object p9, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iput-object p10, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->callsStore:Lcom/discord/stores/StoreCalls;

    return-void
.end method

.method public synthetic constructor <init>(JZLcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreCalls;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 13

    move/from16 v0, p11

    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object/from16 v6, p4

    :goto_0
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v1

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object/from16 v7, p5

    :goto_1
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v1

    move-object v8, v1

    goto :goto_2

    :cond_2
    move-object/from16 v8, p6

    :goto_2
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v1

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p7

    :goto_3
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_4

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    move-object v10, v1

    goto :goto_4

    :cond_4
    move-object/from16 v10, p8

    :goto_4
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_5

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v1

    move-object v11, v1

    goto :goto_5

    :cond_5
    move-object/from16 v11, p9

    :goto_5
    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_6

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getCalls()Lcom/discord/stores/StoreCalls;

    move-result-object v0

    move-object v12, v0

    goto :goto_6

    :cond_6
    move-object/from16 v12, p10

    :goto_6
    move-object v2, p0

    move-wide v3, p1

    move/from16 v5, p3

    invoke-direct/range {v2 .. v12}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;-><init>(JZLcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreCalls;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/voice/model/CallModel;->Companion:Lcom/discord/widgets/voice/model/CallModel$Companion;

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->channelId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/voice/model/CallModel$Companion;->get(J)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->experimentStore:Lcom/discord/stores/StoreExperiments;

    const-string v2, "2020-08_android_screenshare"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iget-wide v3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->channelId:J

    invoke-virtual {v2, v3, v4}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026Invalid\n        }\n      }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "modelClass"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    move-object v2, v1

    iget-wide v3, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->channelId:J

    iget-boolean v5, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->forwardToFullscreenIfVideoActivated:Z

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v6

    iget-object v7, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iget-object v8, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    iget-object v9, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    iget-object v10, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->userSettingsStore:Lcom/discord/stores/StoreUserSettings;

    iget-object v11, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->callsStore:Lcom/discord/stores/StoreCalls;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1f00

    const/16 v18, 0x0

    invoke-direct/range {v2 .. v18}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;-><init>(JZLrx/Observable;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreCalls;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v1
.end method
