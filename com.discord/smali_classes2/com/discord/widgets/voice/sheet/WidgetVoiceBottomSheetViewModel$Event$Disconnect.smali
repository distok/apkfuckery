.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;
.super Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.source "WidgetVoiceBottomSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Disconnect"
.end annotation


# instance fields
.field private final callDuration:J

.field private final channelId:J

.field private final mediaSessionId:Ljava/lang/String;

.field private final rtcConnectionId:Ljava/lang/String;

.field private final sampleRateDenominator:I


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JI)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    iput-wide p5, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    iput p7, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;JLjava/lang/String;Ljava/lang/String;JIILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;
    .locals 8

    move-object v0, p0

    and-int/lit8 v1, p8, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p8, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p3

    :goto_1
    and-int/lit8 v4, p8, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v4, p4

    :goto_2
    and-int/lit8 v5, p8, 0x8

    if-eqz v5, :cond_3

    iget-wide v5, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    goto :goto_3

    :cond_3
    move-wide v5, p5

    :goto_3
    and-int/lit8 v7, p8, 0x10

    if-eqz v7, :cond_4

    iget v7, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    goto :goto_4

    :cond_4
    move v7, p7

    :goto_4
    move-wide p1, v1

    move-object p3, v3

    move-object p4, v4

    move-wide p5, v5

    move p7, v7

    invoke-virtual/range {p0 .. p7}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->copy(JLjava/lang/String;Ljava/lang/String;JI)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    return-wide v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    return v0
.end method

.method public final copy(JLjava/lang/String;Ljava/lang/String;JI)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;
    .locals 9

    new-instance v8, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    move-object v0, v8

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;-><init>(JLjava/lang/String;Ljava/lang/String;JI)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    iget p1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCallDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    return-wide v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    return-wide v0
.end method

.method public final getMediaSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRtcConnectionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSampleRateDenominator()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Disconnect(channelId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", rtcConnectionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->rtcConnectionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mediaSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", callDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->callDuration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", sampleRateDenominator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->sampleRateDenominator:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
