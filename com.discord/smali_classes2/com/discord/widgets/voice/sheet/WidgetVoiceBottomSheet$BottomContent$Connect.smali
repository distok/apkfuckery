.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;
.super Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
.source "WidgetVoiceBottomSheet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Connect"
.end annotation


# instance fields
.field private final isConnectEnabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;ZILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->copy(Z)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    return v0
.end method

.method public final copy(Z)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    invoke-direct {v0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;-><init>(Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    iget-boolean p1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final isConnectEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Connect(isConnectEnabled="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
