.class public final Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$1;
.super Lx/m/c/k;
.source "CallParticipantsAdapter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/graphics/Bitmap;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $voiceUser:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$1;->$voiceUser:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$1;->invoke(Landroid/graphics/Bitmap;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/graphics/Bitmap;)V
    .locals 3

    const-string v0, "bitmap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$1;->$voiceUser:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->handleBitmap(JLandroid/graphics/Bitmap;)V

    return-void
.end method
