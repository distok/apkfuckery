.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;
.super Lf/a/b/l0;
.source "WidgetVoiceBottomSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final callsStore:Lcom/discord/stores/StoreCalls;

.field private final channelId:J

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private fetchedPreviews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final forwardToFullscreenIfVideoActivated:Z

.field private final mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

.field private final mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

.field private mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

.field private final selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private final storeApplicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;

.field private final userSettingsStore:Lcom/discord/stores/StoreUserSettings;

.field private final videoPermissionsManager:Lcom/discord/utilities/permissions/VideoPermissionsManager;

.field private final voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

.field private wasConnected:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(JZLrx/Observable;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreCalls;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreVoiceChannelSelected;",
            "Lcom/discord/stores/StoreMediaSettings;",
            "Lcom/discord/stores/StoreMediaEngine;",
            "Lcom/discord/stores/StoreUserSettings;",
            "Lcom/discord/stores/StoreCalls;",
            "Lcom/discord/utilities/permissions/VideoPermissionsManager;",
            "Lcom/discord/stores/StoreApplicationStreamPreviews;",
            "Lcom/discord/utilities/voice/VoiceEngineServiceController;",
            "Lcom/discord/utilities/time/Clock;",
            "Lcom/discord/tooltips/TooltipManager;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    move-object/from16 v11, p14

    const-string v12, "storeStateObservable"

    invoke-static {v1, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "selectedVoiceChannelStore"

    invoke-static {v2, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "mediaSettingsStore"

    invoke-static {v3, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "mediaEngineStore"

    invoke-static {v4, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "userSettingsStore"

    invoke-static {v5, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "callsStore"

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "videoPermissionsManager"

    invoke-static {v7, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "storeApplicationStreamPreviews"

    invoke-static {v8, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "voiceEngineServiceController"

    invoke-static {v9, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "clock"

    invoke-static {v10, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "tooltipManager"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-direct {p0, v12, v13, v12}, Lf/a/b/l0;-><init>(Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-wide/from16 v13, p1

    iput-wide v13, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    move/from16 v13, p3

    iput-boolean v13, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->forwardToFullscreenIfVideoActivated:Z

    iput-object v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-object v3, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    iput-object v4, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    iput-object v5, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->userSettingsStore:Lcom/discord/stores/StoreUserSettings;

    iput-object v6, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->callsStore:Lcom/discord/stores/StoreCalls;

    iput-object v7, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->videoPermissionsManager:Lcom/discord/utilities/permissions/VideoPermissionsManager;

    iput-object v8, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->storeApplicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    iput-object v9, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    iput-object v10, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    iput-object v11, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->fetchedPreviews:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v1, p0, v12, v2, v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    new-instance v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    move-object/from16 p1, v1

    move-object/from16 p2, v2

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v3

    move/from16 p8, v8

    move-object/from16 p9, v9

    invoke-static/range {p1 .. p9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(JZLrx/Observable;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreCalls;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 19

    move/from16 v0, p15

    and-int/lit16 v1, v0, 0x100

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-instance v1, Lcom/discord/utilities/permissions/VideoPermissionsManager;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v2}, Lcom/discord/utilities/permissions/VideoPermissionsManager;-><init>(Lcom/discord/utilities/permissions/PermissionsManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v14, v1

    goto :goto_0

    :cond_0
    move-object/from16 v14, p10

    :goto_0
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreamPreviews()Lcom/discord/stores/StoreApplicationStreamPreviews;

    move-result-object v1

    move-object v15, v1

    goto :goto_1

    :cond_1
    move-object/from16 v15, p11

    :goto_1
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/utilities/voice/VoiceEngineServiceController;->Companion:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;->getINSTANCE()Lcom/discord/utilities/voice/VoiceEngineServiceController;

    move-result-object v1

    move-object/from16 v16, v1

    goto :goto_2

    :cond_2
    move-object/from16 v16, p12

    :goto_2
    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v1

    move-object/from16 v17, v1

    goto :goto_3

    :cond_3
    move-object/from16 v17, p13

    :goto_3
    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_8

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "logger"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_4

    :cond_4
    move-object v1, v2

    :goto_4
    if-nez v1, :cond_5

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v1, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_5
    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string v0, "floatingViewManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/tooltips/TooltipManager;

    :cond_6
    if-nez v2, :cond_7

    new-instance v0, Lcom/discord/tooltips/TooltipManager;

    sget-object v2, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {v2}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf/a/l/a;

    sget-object v3, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {v3}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object/from16 p10, v0

    move-object/from16 p11, v2

    move-object/from16 p12, v3

    move/from16 p13, v4

    move-object/from16 p14, v1

    move/from16 p15, v5

    invoke-direct/range {p10 .. p15}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    goto :goto_5

    :cond_7
    move-object v0, v2

    :goto_5
    move-object/from16 v18, v0

    goto :goto_6

    :cond_8
    move-object/from16 v18, p14

    :goto_6
    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    invoke-direct/range {v4 .. v18}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;-><init>(JZLrx/Observable;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreCalls;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/utilities/time/Clock;Lcom/discord/tooltips/TooltipManager;)V

    return-void
.end method

.method public static final synthetic access$getChannelId$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    return-wide v0
.end method

.method public static final synthetic access$getEventSubject$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->handleStoreState(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;)V

    return-void
.end method

.method public static final synthetic access$joinVoiceChannel(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->joinVoiceChannel(J)V

    return-void
.end method

.method private final createConnectedListItems(Ljava/util/Map;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    const/4 v2, 0x1

    xor-int/2addr p1, v2

    const/4 v3, 0x0

    if-eqz p2, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_1
    if-eqz p1, :cond_7

    invoke-direct {p0, p2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->createUserItemsComparator(Ljava/lang/String;)Ljava/util/Comparator;

    move-result-object p1

    invoke-static {v1, p1}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    new-instance v1, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {p1, v5}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    new-instance v6, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v5}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v7

    if-nez v7, :cond_4

    const/4 v7, 0x1

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    :goto_3
    if-eqz v4, :cond_5

    invoke-virtual {v5}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getWatchingStream()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v8, 0x1

    goto :goto_4

    :cond_5
    const/4 v8, 0x0

    :goto_4
    invoke-direct {v6, v5, v7, v8}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZZ)V

    invoke-interface {v1, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_7
    return-object v0
.end method

.method private final createUserItemsComparator(Ljava/lang/String;)Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Comparator<",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$createUserItemsComparator$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$createUserItemsComparator$1;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic createUserItemsComparator$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;Ljava/lang/String;ILjava/lang/Object;)Ljava/util/Comparator;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->createUserItemsComparator(Ljava/lang/String;)Ljava/util/Comparator;

    move-result-object p0

    return-object p0
.end method

.method private final emitServerDeafenedDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitServerMutedDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowNoScreenSharePermissionDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoScreenSharePermissionDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoScreenSharePermissionDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowNoVideoPermissionDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitSuppressedDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;)V
    .locals 26
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iput-object v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v2, v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v2, :cond_0

    return-void

    :cond_0
    check-cast v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isConnected()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->forwardToFullscreenIfVideoActivated:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    const/4 v5, 0x0

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v6

    invoke-direct {v3, v6, v7, v5}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;-><init>(JLjava/lang/String;)V

    iget-object v1, v2, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v3}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_2
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/widgets/voice/model/CallModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v6

    move-object v8, v6

    goto :goto_1

    :cond_3
    move-object v8, v5

    :goto_1
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_4
    move-object v6, v5

    :goto_2
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/widgets/voice/model/CallModel;->getParticipants()Ljava/util/Map;

    move-result-object v9

    invoke-direct {v0, v9, v6, v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->createConnectedListItems(Ljava/util/Map;Ljava/lang/String;Lcom/discord/models/domain/ModelChannel;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;

    instance-of v10, v9, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;

    if-eqz v10, :cond_5

    check-cast v9, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;

    invoke-virtual {v9}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;->getParticipant()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-virtual {v9}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-virtual {v10}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v10

    goto :goto_4

    :cond_6
    move-object v10, v5

    :goto_4
    if-eqz v10, :cond_5

    iget-object v11, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->fetchedPreviews:Ljava/util/Set;

    invoke-interface {v11, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    iget-object v11, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->storeApplicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    invoke-virtual {v11, v9}, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewIfNotFetching(Lcom/discord/utilities/streams/StreamContext;)V

    iget-object v9, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->fetchedPreviews:Ljava/util/Set;

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    xor-int/2addr v6, v4

    if-eqz v6, :cond_8

    new-instance v6, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;

    invoke-direct {v6, v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;-><init>(Ljava/util/List;)V

    move-object/from16 v16, v6

    goto :goto_5

    :cond_8
    sget-object v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;

    move-object/from16 v16, v2

    :goto_5
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isConnected()Z

    move-result v2

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/widgets/voice/model/CallModel;->getVoiceChannelJoinability()Lcom/discord/utilities/voice/VoiceChannelJoinability;

    move-result-object v6

    sget-object v9, Lcom/discord/utilities/voice/VoiceChannelJoinability;->PERMISSIONS_MISSING:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    if-eq v6, v9, :cond_9

    const/4 v6, 0x1

    goto :goto_6

    :cond_9
    const/4 v6, 0x0

    :goto_6
    iget-object v9, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->wasConnected:Ljava/lang/Boolean;

    if-eqz v9, :cond_a

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-nez v9, :cond_a

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/widgets/voice/model/CallModel;->isSuppressed()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitSuppressedDialogEvent()V

    :cond_a
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iput-object v9, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->wasConnected:Ljava/lang/Boolean;

    if-nez v2, :cond_b

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    invoke-direct {v2, v6}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;-><init>(Z)V

    move-object/from16 v17, v2

    goto :goto_8

    :cond_b
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getMobileScreenshareExperimentEnabled()Z

    move-result v23

    new-instance v6, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isMeMutedByAnySource()Z

    move-result v20

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isDeafenedByAnySource()Z

    move-result v21

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    move-result-object v18

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v19

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v22

    if-eqz v23, :cond_c

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->hasVideoPermission()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v24, 0x1

    goto :goto_7

    :cond_c
    const/16 v24, 0x0

    :goto_7
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v25

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v25}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;-><init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZLcom/discord/widgets/voice/model/CameraState;ZZZ)V

    :goto_8
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v2

    if-eqz v2, :cond_d

    goto :goto_9

    :cond_d
    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v5

    :goto_9
    move-object v11, v5

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    const-string v3, "title"

    invoke-static {v7, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v9, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/voice/model/CallModel;->canInvite()Z

    move-result v12

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getNoiseCancellation()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getMobileScreenshareExperimentEnabled()Z

    move-result v14

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isDeafenedByAnySource()Z

    move-result v15

    move-object v6, v2

    invoke-direct/range {v6 .. v17}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLjava/lang/Boolean;ZZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)V

    invoke-virtual {v0, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final hasVideoPermission()Z
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->videoPermissionsManager:Lcom/discord/utilities/permissions/VideoPermissionsManager;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/voice/model/CallModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuild;->getAfkChannelId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getMyPermissions()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v3, v2, v0}, Lcom/discord/utilities/permissions/VideoPermissionsManager;->hasVideoPermission(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Ljava/lang/Long;)Z

    move-result v0

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private final joinVoiceChannel(J)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreVoiceChannelSelected;->selectVoiceChannel(J)Lrx/Observable;

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object p2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, p2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onCameraButtonPressed()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->hasVideoPermission()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitShowNoVideoPermissionDialogEvent()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    move-object v0, v2

    :cond_1
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    :goto_0
    sget-object v1, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_DISABLED:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v0, v1, :cond_3

    return-void

    :cond_3
    sget-object v1, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreMediaEngine;->selectVideoInputDevice(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v1, :cond_6

    goto :goto_2

    :cond_6
    move-object v2, v0

    :goto_2
    check-cast v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getNumUsersConnected()I

    move-result v0

    invoke-virtual {v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getGuildMaxVideoChannelMembers()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v0, v2, :cond_7

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v2, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_8
    :goto_3
    return-void
.end method

.method public final onCameraPermissionsGranted()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/discord/stores/StoreMediaEngine;->selectDefaultVideoDevice$default(Lcom/discord/stores/StoreMediaEngine;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final onDeafenPressed()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isServerDeafened()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitServerDeafenedDialogEvent()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfDeafened()V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f121a5a

    goto :goto_0

    :cond_2
    const v0, 0x7f121a53

    :goto_0
    invoke-direct {v2, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;-><init>(I)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public final onDisconnectPressed()V
    .locals 13
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->clear()V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v12, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    iget-wide v5, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getRtcConnectionId()Ljava/lang/String;

    move-result-object v4

    move-object v7, v4

    goto :goto_0

    :cond_2
    move-object v7, v2

    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getMediaSessionId()Ljava/lang/String;

    move-result-object v2

    :cond_3
    move-object v8, v2

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/voice/model/CallModel;->getCallDurationMs(Lcom/discord/utilities/time/Clock;)J

    move-result-wide v9

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getCallFeedbackSampleRateDenominator()I

    move-result v11

    move-object v4, v12

    invoke-direct/range {v4 .. v11}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;-><init>(JLjava/lang/String;Ljava/lang/String;JI)V

    iget-object v0, v3, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v12}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public final onMutePressed()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isSuppressed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitSuppressedDialogEvent()V

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitServerMutedDialogEvent()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfMuted()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;

    const v2, 0x7f1219f8

    invoke-direct {v1, v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f121a5b

    goto :goto_0

    :cond_4
    const v0, 0x7f121a56

    :goto_0
    invoke-direct {v2, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;-><init>(I)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_5
    :goto_1
    return-void
.end method

.method public final onNoiseCancellationPressed()V
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v2, Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;->INSTANCE:Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/discord/tooltips/TooltipManager;->b(Lcom/discord/tooltips/TooltipManager$Tooltip;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    invoke-virtual {v0, v2}, Lcom/discord/tooltips/TooltipManager;->a(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoiseCancellationBottomSheet;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoiseCancellationBottomSheet;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings;->toggleNoiseCancellation()V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;

    const v2, 0x7f121124

    invoke-direct {v1, v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;

    const v2, 0x7f121125

    invoke-direct {v1, v2}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final onPttPressed(Z)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMediaEngine;->setPttActive(Z)V

    return-void
.end method

.method public final onScreenSharePressed()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    instance-of v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    check-cast v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-virtual {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->stopStream()V

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->hasVideoPermission()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitShowNoScreenSharePermissionDialogEvent()V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_4
    :goto_2
    return-void
.end method

.method public final onStreamPreviewClicked(Ljava/lang/String;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getVoiceChannelJoinability()Lcom/discord/utilities/voice/VoiceChannelJoinability;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->GUILD_VIDEO_AT_CAPACITY:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    if-ne v0, v1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v3

    invoke-direct {v2, v3, v4, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;-><init>(JLjava/lang/String;)V

    iget-object p1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final onToggleRingingPressed(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "voiceUser"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->callsStore:Lcom/discord/stores/StoreCalls;

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/stores/StoreCalls;->stopRinging(JLjava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->callsStore:Lcom/discord/stores/StoreCalls;

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/stores/StoreCalls;->ring(JLjava/util/List;)V

    :goto_0
    return-void
.end method

.method public final startStream(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->startStream(Landroid/content/Intent;)V

    return-void
.end method

.method public final tryConnectToVoice(Z)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getVoiceChannelJoinability()Lcom/discord/utilities/voice/VoiceChannelJoinability;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/voice/VoiceChannelJoinability;->GUILD_VIDEO_AT_CAPACITY:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    if-ne v0, v1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->userSettingsStore:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getMobileOverlay()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_2
    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->hasVideoPermission()Z

    move-result p1

    if-nez p1, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->emitShowNoVideoPermissionDialogEvent()V

    return-void

    :cond_3
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreMediaEngine;->selectDefaultVideoDevice(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_4
    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->channelId:J

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->joinVoiceChannel(J)V

    :cond_5
    :goto_0
    return-void
.end method
