.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;
.super Lx/m/c/k;
.source "WidgetVoiceBottomSheetViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->tryConnectToVoice(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    invoke-static {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->access$getChannelId$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->access$joinVoiceChannel(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;J)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$tryConnectToVoice$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    invoke-static {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->access$getEventSubject$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)Lrx/subjects/PublishSubject;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
