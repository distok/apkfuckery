.class public final Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "CallParticipantsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem;,
        Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderDivider;,
        Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderHeader;,
        Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;,
        Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$Companion;

.field private static final VIEW_TYPE_DIVIDER:I = 0x3

.field private static final VIEW_TYPE_HEADER:I = 0x1

.field private static final VIEW_TYPE_SPECTATORS_HEADER:I = 0x2

.field private static final VIEW_TYPE_VOICE_USER:I


# instance fields
.field private onStreamPreviewClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onToggleRingingClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onVoiceUserClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final quantizeUserAvatars:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->Companion:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Z)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-boolean p2, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->quantizeUserAvatars:Z

    sget-object p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$onVoiceUserClicked$1;->INSTANCE:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$onVoiceUserClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onVoiceUserClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$onStreamPreviewClicked$1;->INSTANCE:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$onStreamPreviewClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onStreamPreviewClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$onToggleRingingClicked$1;->INSTANCE:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$onToggleRingingClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onToggleRingingClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/recyclerview/widget/RecyclerView;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Z)V

    return-void
.end method


# virtual methods
.method public final getOnStreamPreviewClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onStreamPreviewClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnToggleRingingClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onToggleRingingClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnVoiceUserClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onVoiceUserClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_3

    const/4 p1, 0x1

    if-eq p2, p1, :cond_2

    const/4 p1, 0x2

    if-eq p2, p1, :cond_1

    const/4 p1, 0x3

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderDivider;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderDivider;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderHeader;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderHeader;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderHeader;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderHeader;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;)V

    goto :goto_0

    :cond_3
    new-instance p1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;

    iget-boolean p2, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->quantizeUserAvatars:Z

    invoke-direct {p1, p0, p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;Z)V

    :goto_0
    return-object p1
.end method

.method public final setOnStreamPreviewClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onStreamPreviewClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnToggleRingingClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onToggleRingingClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnVoiceUserClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->onVoiceUserClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method
