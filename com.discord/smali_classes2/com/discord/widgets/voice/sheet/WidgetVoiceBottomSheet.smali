.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetVoiceBottomSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$ViewState;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_FEATURE_CONTEXT:Ljava/lang/String; = "ARG_FEATURE_CONTEXT"

.field private static final ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED:Ljava/lang/String; = "ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED"

.field public static final Companion:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;


# instance fields
.field private final connectBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final connectContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final connectVideoBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final controls$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptyStateContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

.field private final headerDeafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final headerInviteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final headerNoiseCancellationButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final headerSettingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onStreamPreviewClickedListener:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

.field private final participantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final root$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final title$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

.field private viewModelEventSubscription:Lrx/Subscription;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xd

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v3, "root"

    const-string v4, "getRoot()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "title"

    const-string v7, "getTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "subtitle"

    const-string v7, "getSubtitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "participantsRecycler"

    const-string v7, "getParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "emptyStateContainer"

    const-string v7, "getEmptyStateContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "connectContainer"

    const-string v7, "getConnectContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "connectBtn"

    const-string v7, "getConnectBtn()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "connectVideoBtn"

    const-string v7, "getConnectVideoBtn()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "controls"

    const-string v7, "getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "headerDeafenButton"

    const-string v7, "getHeaderDeafenButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "headerInviteButton"

    const-string v7, "getHeaderInviteButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "headerNoiseCancellationButton"

    const-string v7, "getHeaderNoiseCancellationButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const-string v6, "headerSettingsButton"

    const-string v7, "getHeaderSettingsButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->Companion:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0b88

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->root$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b85

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->title$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b84

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->subtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b87

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b7d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->emptyStateContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b7b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->connectContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b7a

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->connectBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b86

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->connectVideoBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b7c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->controls$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b7f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerDeafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b81

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerInviteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b82

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerNoiseCancellationButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b83

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerSettingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClickedListener$1;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClickedListener$1;

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->onStreamPreviewClickedListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getViewModelEventSubscription$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModelEventSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->handleEvent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$onStreamPreviewClicked(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    return-void
.end method

.method public static final synthetic access$setViewModelEventSubscription$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModelEventSubscription:Lrx/Subscription;

    return-void
.end method

.method private final configureBottomContent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)V
    .locals 24

    move-object/from16 v15, p0

    move-object/from16 v0, p1

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v20

    const v1, 0x7f04013a

    const/16 v2, 0x8

    if-nez v0, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getRoot()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getRoot()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    move-object v4, v15

    goto/16 :goto_2

    :cond_0
    instance-of v3, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    const/4 v4, 0x0

    if-eqz v3, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    move-result-object v1

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted()Z

    move-result v4

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened()Z

    move-result v5

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing()Z

    move-result v6

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v8

    new-instance v9, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$1;

    iget-object v7, v15, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    const-string v10, "viewModel"

    if-eqz v7, :cond_6

    invoke-direct {v9, v7}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    new-instance v12, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$2;

    iget-object v7, v15, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz v7, :cond_5

    invoke-direct {v12, v7}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$2;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    new-instance v13, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$3;

    invoke-direct {v13, v15}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$3;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    new-instance v14, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$4;

    iget-object v7, v15, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz v7, :cond_4

    invoke-direct {v14, v7}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$4;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    new-instance v7, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$5;

    iget-object v11, v15, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz v11, :cond_3

    invoke-direct {v7, v11}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$5;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->getShowScreenshare()Z

    move-result v11

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->getShowScreenShareSparkle()Z

    move-result v16

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$6;

    move-object/from16 v17, v7

    iget-object v7, v15, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz v7, :cond_2

    invoke-direct {v0, v7}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$6;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v10

    const-string v7, "parentFragmentManager"

    invoke-static {v10, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$7;

    invoke-direct {v7, v15}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$7;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    move-object/from16 v21, v14

    iget-object v14, v15, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    if-eqz v14, :cond_1

    move-object/from16 v22, v0

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v11

    move-object/from16 v23, v7

    move/from16 v7, v16

    move-object/from16 v16, v10

    move-object v10, v12

    move-object v11, v13

    move-object/from16 v12, v21

    move-object/from16 v13, v17

    move-object/from16 v21, v14

    move-object/from16 v14, v22

    move-object/from16 v15, p0

    move-object/from16 v17, v23

    invoke-virtual/range {v0 .. v21}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->configureUI(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZZZZLcom/discord/widgets/voice/model/CameraState;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getRoot()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getRoot()Landroid/view/ViewGroup;

    move-result-object v1

    const v2, 0x7f04013b

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    goto/16 :goto_1

    :cond_1
    const-string v0, "featureContext"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0

    :cond_2
    const/4 v0, 0x0

    invoke-static {v10}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v0, 0x0

    invoke-static {v10}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    invoke-static {v10}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v0, 0x0

    invoke-static {v10}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 v0, 0x0

    invoke-static {v10}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_7
    instance-of v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    if-eqz v2, :cond_9

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->hidePtt()V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectContainer()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectBtn()Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f120ea9

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectBtn()Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectBtn()Landroid/widget/Button;

    move-result-object v2

    const v3, 0x7f120443

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectBtn()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectBtn()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$8;

    move-object/from16 v4, p0

    invoke-direct {v3, v4}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$8;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectVideoBtn()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Connect;->isConnectEnabled()Z

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getConnectVideoBtn()Landroid/widget/Button;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$9;

    invoke-direct {v2, v4}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureBottomContent$9;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getRoot()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getRoot()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    goto :goto_2

    :cond_9
    :goto_1
    move-object/from16 v4, p0

    :goto_2
    return-void
.end method

.method private final configureCenterContent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;JLjava/lang/Long;)V
    .locals 5

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "participantsAdapter"

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getEmptyStateContainer()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$ListItems;->getItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    if-eqz p1, :cond_2

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureCenterContent$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureCenterContent$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->setOnStreamPreviewClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    if-eqz p1, :cond_1

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureCenterContent$2;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureCenterContent$2;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;JLjava/lang/Long;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->setOnVoiceUserClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    if-eqz p1, :cond_0

    new-instance p2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureCenterContent$3;

    invoke-direct {p2, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureCenterContent$3;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;->setOnToggleRingingClicked(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_0
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    sget-object p2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent$Empty;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getEmptyStateContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    if-eqz p1, :cond_5

    sget-object p2, Lx/h/l;->d:Lx/h/l;

    invoke-virtual {p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_5
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderDeafenButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderDeafenButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderDeafenButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f1218c2

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const v1, 0x7f1205d7

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderDeafenButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderInviteButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderInviteButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$2;

    invoke-direct {v1, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$2;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderSettingsButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$3;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderNoiseCancellationButton()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$4;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$4;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderNoiseCancellationButton()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0803a9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderNoiseCancellationButton()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f0803aa

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderNoiseCancellationButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getHeaderNoiseCancellationButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->configureCenterContent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;JLjava/lang/Long;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->configureBottomContent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)V

    return-void
.end method

.method private final getConnectBtn()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->connectBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getConnectContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->connectContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getConnectVideoBtn()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->connectVideoBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->controls$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    return-object v0
.end method

.method private final getEmptyStateContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->emptyStateContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getHeaderDeafenButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerDeafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getHeaderInviteButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerInviteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getHeaderNoiseCancellationButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerNoiseCancellationButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getHeaderSettingsButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->headerSettingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getRoot()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->root$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getSubtitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->subtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->title$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;)V
    .locals 11

    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->showSuppressedDialog()V

    goto/16 :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->showServerMutedDialog()V

    goto/16 :goto_0

    :cond_1
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->showServerDeafenedDialog()V

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->showNoVideoPermissionDialog()V

    goto/16 :goto_0

    :cond_3
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->showNoVideoDevicesToast()V

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$handleEvent$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$handleEvent$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-virtual {p0, p1}, Lcom/discord/app/AppBottomSheet;->requestVideoCallPermissions(Lkotlin/jvm/functions/Function0;)V

    goto/16 :goto_0

    :cond_5
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    const-string v1, "parentFragmentManager"

    if-eqz v0, :cond_6

    sget-object v0, Lf/a/a/i;->g:Lf/a/a/i$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;->getGuildMaxVideoChannelUsers()I

    move-result p1

    invoke-virtual {v0, v2, p1}, Lf/a/a/i$a;->a(Landroidx/fragment/app/FragmentManager;I)V

    goto/16 :goto_0

    :cond_6
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoiseCancellationBottomSheet;

    if-eqz v0, :cond_7

    sget-object p1, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheet;->Companion:Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheet$Companion;

    invoke-virtual {p1, p0}, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheet$Companion;->show(Landroidx/fragment/app/Fragment;)V

    goto/16 :goto_0

    :cond_7
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    const-string v2, "requireContext()"

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModelEventSubscription:Lrx/Subscription;

    if-eqz v0, :cond_8

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_8
    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    sget-object v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->getChannelId()J

    move-result-wide v5

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->getAutoTargetStreamKey()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object p1, Lf/a/a/n;->f:Lf/a/a/n$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lf/a/a/n$a;->a(Landroidx/fragment/app/FragmentManager;)V

    goto/16 :goto_0

    :cond_a
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object p1, Lcom/discord/widgets/notice/WidgetNoticeNuxOverlay;->Companion:Lcom/discord/widgets/notice/WidgetNoticeNuxOverlay$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/notice/WidgetNoticeNuxOverlay$Companion;->enqueue()V

    goto/16 :goto_0

    :cond_b
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;->getToastResId()I

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {v0, p1, v1, v2, v3}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    goto/16 :goto_0

    :cond_c
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    sget-object v3, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->INSTANCE:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->getChannelId()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->getRtcConnectionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->getMediaSessionId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->getCallDuration()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;->getSampleRateDenominator()I

    move-result v9

    invoke-virtual/range {v3 .. v9}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->enqueueNotice(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;I)V

    goto :goto_0

    :cond_d
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    goto :goto_0

    :cond_e
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/discord/utilities/accessibility/AccessibilityUtils;->INSTANCE:Lcom/discord/utilities/accessibility/AccessibilityUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;->getMessageResId()I

    move-result p1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "getString(event.messageResId)"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/accessibility/AccessibilityUtils;->sendAnnouncement(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_f
    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoScreenSharePermissionDialog;

    if-eqz v0, :cond_10

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->showNoScreenSharePermissionDialog()V

    goto :goto_0

    :cond_10
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {p0}, Lcom/discord/widgets/voice/stream/StreamNavigator;->requestStartStream(Landroidx/fragment/app/Fragment;)V

    goto :goto_0

    :cond_11
    sget-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_12

    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Lcom/discord/app/AppBottomSheet;->setBottomSheetState(I)V

    :goto_0
    return-void

    :cond_12
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->onStreamPreviewClickedListener:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual {p0, v0}, Lcom/discord/app/AppBottomSheet;->requestMicrophone(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void

    :cond_2
    const-string p1, "featureContext"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final showNoScreenSharePermissionDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121122

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121118

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026e_permission_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showNoVideoDevicesToast()V
    .locals 3

    const v0, 0x7f121120

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {p0, v0, v1, v2}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    return-void
.end method

.method private final showNoVideoPermissionDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121122

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121121

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026o_permission_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showServerDeafenedDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121659

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121658

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026ver_deafened_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showServerMutedDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121663

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121662

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026server_muted_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showSuppressedDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f1217b0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f1217b3

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026ppressed_permission_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02d2

    return v0
.end method

.method public final getOnStreamPreviewClickedListener()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->onStreamPreviewClickedListener:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onActivityResult$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onActivityResult$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    invoke-static {p1, p2, p3, v0}, Lcom/discord/widgets/voice/stream/StreamNavigator;->handleActivityResult(IILandroid/content/Intent;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17

    move-object/from16 v0, p0

    const-string v1, "inflater"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "ARG_FEATURE_CONTEXT"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    const-string v3, "null cannot be cast to non-null type com.discord.widgets.voice.sheet.WidgetVoiceBottomSheet.FeatureContext"

    invoke-static {v1, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    iput-object v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "ARG_FORWARD_TO_FULLSCREEN_IF_VIDEO_ACTIVATED"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1fc

    const/16 v16, 0x0

    move-object v4, v3

    invoke-direct/range {v4 .. v16}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;-><init>(JZLcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreCalls;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v1, v0, v3}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    invoke-virtual {v1, v3}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v1

    const-string v3, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    iput-object v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    invoke-super/range {p0 .. p3}, Lcom/discord/app/AppBottomSheet;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Z)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->participantsAdapter:Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    const/4 v1, 0x0

    const-string v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onResume$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    const/4 v3, 0x0

    new-instance v7, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onResume$2;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onResume$3;

    invoke-direct {v4, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onResume$3;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)V

    const/16 v8, 0x1a

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Lcom/discord/app/AppBottomSheet;->setBottomSheetState(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->getControls()Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onViewCreated$1;

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    if-eqz v0, :cond_0

    invoke-direct {p2, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->setOnPttPressedListener(Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public final setOnStreamPreviewClickedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->onStreamPreviewClickedListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method
