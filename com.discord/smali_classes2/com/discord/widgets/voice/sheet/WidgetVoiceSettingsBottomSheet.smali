.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetVoiceSettingsBottomSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_CHANNEL_ID:Ljava/lang/String; = "ARG_CHANNEL_ID"

.field public static final Companion:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$Companion;


# instance fields
.field private final inviteItem$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noiseSuppressionToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

.field private final voiceParticipantsToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final voiceSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    const-string v3, "voiceParticipantsToggle"

    const-string v4, "getVoiceParticipantsToggle()Landroidx/appcompat/widget/SwitchCompat;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    const-string v6, "noiseSuppressionToggle"

    const-string v7, "getNoiseSuppressionToggle()Landroidx/appcompat/widget/SwitchCompat;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    const-string v6, "inviteItem"

    const-string v7, "getInviteItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    const-string v6, "voiceSettingsItem"

    const-string v7, "getVoiceSettingsItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->Companion:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0b96

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->voiceParticipantsToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b94

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->noiseSuppressionToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b93

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->inviteItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b97

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->voiceSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;)Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$getVoiceSettingsItem$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getVoiceSettingsItem()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getInviteItem()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getInviteItem()Landroid/view/View;

    move-result-object v0

    invoke-interface {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;->getShowInviteItem()Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getVoiceParticipantsToggle()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$2;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getVoiceParticipantsToggle()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    invoke-interface {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;->getShowVoiceParticipants()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getVoiceParticipantsToggle()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    invoke-interface {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;->getShowVoiceParticipantsToggle()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getVoiceSettingsItem()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$3;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getNoiseSuppressionToggle()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$4;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$4;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->getNoiseSuppressionToggle()Landroidx/appcompat/widget/SwitchCompat;

    move-result-object v0

    invoke-interface {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;->getNoiseCancellationEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    return-void
.end method

.method private final getInviteItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->inviteItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNoiseSuppressionToggle()Landroidx/appcompat/widget/SwitchCompat;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->noiseSuppressionToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    return-object v0
.end method

.method private final getVoiceParticipantsToggle()Landroidx/appcompat/widget/SwitchCompat;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->voiceParticipantsToggle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SwitchCompat;

    return-object v0
.end method

.method private final getVoiceSettingsItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->voiceSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02d7

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const-string v0, "inflater"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CHANNEL_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    new-instance v2, Landroidx/lifecycle/ViewModelProvider;

    new-instance v3, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel$Factory;

    invoke-direct {v3, v0, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel$Factory;-><init>(J)V

    invoke-direct {v2, p0, v3}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

    invoke-virtual {v2, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(this, \u2026del::class.java\n        )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

    iput-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppBottomSheet;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->viewModel:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$onResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$onResume$1;-><init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Lcom/discord/app/AppBottomSheet;->setBottomSheetState(I)V

    return-void
.end method
