.class public abstract Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.super Ljava/lang/Object;
.source "WidgetVoiceBottomSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowSuppressedDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerMutedDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowServerDeafenedDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoPermissionDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoVideoDevicesAvailableToast;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoScreenSharePermissionDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowRequestCameraPermissionsDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowGuildVideoAtCapacityDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowOverlayNux;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowNoiseCancellationBottomSheet;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowCameraCapacityDialog;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ShowToast;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Dismiss;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$Disconnect;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$AccessibilityAnnouncement;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$RequestStartStream;,
        Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$ExpandSheet;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;-><init>()V

    return-void
.end method
