.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;
.super Ljava/lang/Object;
.source "WidgetVoiceBottomSheetViewModel.kt"

# interfaces
.implements Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$ViewState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewState"
.end annotation


# instance fields
.field private final bottomContent:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

.field private final canInvite:Z

.field private final centerContent:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

.field private final channelId:J

.field private final guildId:Ljava/lang/Long;

.field private final isDeafened:Z

.field private final isNoiseCancellationActive:Ljava/lang/Boolean;

.field private final showDeafenButtonInHeader:Z

.field private final subtitle:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLjava/lang/Boolean;ZZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)V
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "centerContent"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->subtitle:Ljava/lang/String;

    iput-wide p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->channelId:J

    iput-object p5, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->guildId:Ljava/lang/Long;

    iput-boolean p6, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->canInvite:Z

    iput-object p7, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive:Ljava/lang/Boolean;

    iput-boolean p8, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->showDeafenButtonInHeader:Z

    iput-boolean p9, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened:Z

    iput-object p10, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->centerContent:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    iput-object p11, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->bottomContent:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLjava/lang/Boolean;ZZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;ILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;
    .locals 11

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, p2

    :goto_1
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v3

    goto :goto_2

    :cond_2
    move-wide v3, p3

    :goto_2
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v5

    goto :goto_3

    :cond_3
    move-object/from16 v5, p5

    :goto_3
    and-int/lit8 v6, v0, 0x10

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v6

    goto :goto_4

    :cond_4
    move/from16 v6, p6

    :goto_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v7

    goto :goto_5

    :cond_5
    move-object/from16 v7, p7

    :goto_5
    and-int/lit8 v8, v0, 0x40

    if-eqz v8, :cond_6

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v8

    goto :goto_6

    :cond_6
    move/from16 v8, p8

    :goto_6
    and-int/lit16 v9, v0, 0x80

    if-eqz v9, :cond_7

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v9

    goto :goto_7

    :cond_7
    move/from16 v9, p9

    :goto_7
    and-int/lit16 v10, v0, 0x100

    if-eqz v10, :cond_8

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v10

    goto :goto_8

    :cond_8
    move-object/from16 v10, p10

    :goto_8
    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    goto :goto_9

    :cond_9
    move-object/from16 v0, p11

    :goto_9
    move-object p1, v1

    move-object p2, v2

    move-wide p3, v3

    move-object/from16 p5, v5

    move/from16 p6, v6

    move-object/from16 p7, v7

    move/from16 p8, v8

    move/from16 p9, v9

    move-object/from16 p10, v10

    move-object/from16 p11, v0

    invoke-virtual/range {p0 .. p11}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->copy(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLjava/lang/Boolean;ZZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component10()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()J
    .locals 2

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v0

    return-wide v0
.end method

.method public final component4()Ljava/lang/Long;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v0

    return v0
.end method

.method public final component6()Ljava/lang/Boolean;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v0

    return v0
.end method

.method public final component8()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v0

    return v0
.end method

.method public final component9()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLjava/lang/Boolean;ZZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;
    .locals 13

    const-string v0, "title"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "centerContent"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    move-object v1, v0

    move-object v3, p2

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v12, p11

    invoke-direct/range {v1 .. v12}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Long;ZLjava/lang/Boolean;ZZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v1

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object p1

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->bottomContent:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    return-object v0
.end method

.method public getCanInvite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->canInvite:Z

    return v0
.end method

.method public getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->centerContent:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    return-object v0
.end method

.method public getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->channelId:J

    return-wide v0
.end method

.method public getGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->guildId:Ljava/lang/Long;

    return-object v0
.end method

.method public getShowDeafenButtonInHeader()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->showDeafenButtonInHeader:Z

    return v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v2

    if-eqz v2, :cond_6

    goto :goto_4

    :cond_6
    move v3, v2

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    return v0
.end method

.method public isDeafened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened:Z

    return v0
.end method

.method public isNoiseCancellationActive()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive:Ljava/lang/Boolean;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "ViewState(title="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", subtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", channelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canInvite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCanInvite()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isNoiseCancellationActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isNoiseCancellationActive()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showDeafenButtonInHeader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getShowDeafenButtonInHeader()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDeafened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->isDeafened()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", centerContent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bottomContent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
