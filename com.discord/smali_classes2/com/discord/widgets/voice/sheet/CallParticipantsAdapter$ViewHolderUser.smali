.class public final Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "CallParticipantsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolderUser"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# instance fields
.field private final avatar:Lcom/discord/views/VoiceUserView;

.field private final deafenIndicator:Landroid/widget/ImageView;

.field private final liveIndicator:Landroid/widget/TextView;

.field private final muteIndicator:Landroid/widget/ImageView;

.field private final name:Landroid/widget/TextView;

.field private final quantizeUserAvatar:Z

.field private final spectatingIndicator:Landroid/widget/ImageView;

.field private final streamPreview:Lcom/discord/views/StreamPreviewView;

.field private final toggleRingingButton:Landroid/widget/Button;

.field private final videoIndicator:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;Z)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d0170

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iput-boolean p2, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->quantizeUserAvatar:Z

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0ba1

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026er_list_item_user_avatar)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/VoiceUserView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->avatar:Lcom/discord/views/VoiceUserView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0ba2

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026user_list_item_user_name)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->name:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0b9d

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026list_item_live_indicator)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->liveIndicator:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0b9f

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026tem_spectating_indicator)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->spectatingIndicator:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0b9b

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026st_item_deafen_indicator)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->deafenIndicator:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0b9e

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026list_item_mute_indicator)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->muteIndicator:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0ba4

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026ist_item_video_indicator)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->videoIndicator:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0ba3

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026item_user_stream_preview)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/StreamPreviewView;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->streamPreview:Lcom/discord/views/StreamPreviewView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const p2, 0x7f0a0ba0

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "itemView.findViewById(R.\u2026list_item_toggle_ringing)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;)Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter;

    return-object p0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 8

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    check-cast p2, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;

    invoke-virtual {p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;->getParticipant()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-result-object p1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->quantizeUserAvatar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->avatar:Lcom/discord/views/VoiceUserView;

    new-instance v1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$1;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/VoiceUserView;->setOnBitmapLoadedListener(Lkotlin/jvm/functions/Function1;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->avatar:Lcom/discord/views/VoiceUserView;

    const v1, 0x7f07006b

    invoke-virtual {v0, p1, v1}, Lcom/discord/views/VoiceUserView;->a(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->name:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$2;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    invoke-virtual {p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;->getCanRing()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    const v4, 0x7f121751

    invoke-static {v1, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    const v4, 0x7f121548

    invoke-static {v1, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    new-instance v1, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$3;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$3;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result v1

    const/4 v4, 0x1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    const v5, 0x7f121752

    new-array v6, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v1, v5, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->toggleRingingButton:Landroid/widget/Button;

    const v5, 0x7f121549

    new-array v6, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v1, v5, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->deafenIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->muteIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSelfDeaf()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isDeaf()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v1, 0x1

    :goto_4
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSelfMute()Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isMute()Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSuppress()Z

    move-result v5

    if-eqz v5, :cond_6

    goto :goto_5

    :cond_6
    const/4 v5, 0x0

    goto :goto_6

    :cond_7
    :goto_5
    const/4 v5, 0x1

    :goto_6
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v0

    iget-object v6, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->spectatingIndicator:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;->isSpectatingSameStream()Z

    move-result v7

    if-eqz v7, :cond_8

    const/4 v7, 0x0

    goto :goto_7

    :cond_8
    const/16 v7, 0x8

    :goto_7
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v6, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->deafenIndicator:Landroid/widget/ImageView;

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    goto :goto_8

    :cond_9
    const/16 v1, 0x8

    :goto_8
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->muteIndicator:Landroid/widget/ImageView;

    if-eqz v5, :cond_a

    const/4 v5, 0x0

    goto :goto_9

    :cond_a
    const/16 v5, 0x8

    :goto_9
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->videoIndicator:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    goto :goto_a

    :cond_b
    const/16 v0, 0x8

    :goto_a
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->liveIndicator:Landroid/widget/TextView;

    if-eqz v0, :cond_c

    const/4 v5, 0x1

    goto :goto_b

    :cond_c
    const/4 v5, 0x0

    :goto_b
    if-eqz v5, :cond_d

    const/4 v5, 0x0

    goto :goto_c

    :cond_d
    const/16 v5, 0x8

    :goto_c
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/discord/utilities/streams/StreamContext;->getPreview()Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    move-result-object v1

    goto :goto_d

    :cond_e
    const/4 v1, 0x0

    :goto_d
    if-eqz v1, :cond_11

    invoke-virtual {v0}, Lcom/discord/utilities/streams/StreamContext;->getJoinability()Lcom/discord/utilities/streams/StreamContext$Joinability;

    move-result-object v0

    sget-object v5, Lcom/discord/utilities/streams/StreamContext$Joinability;->CAN_CONNECT:Lcom/discord/utilities/streams/StreamContext$Joinability;

    if-eq v0, v5, :cond_f

    goto :goto_e

    :cond_f
    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->streamPreview:Lcom/discord/views/StreamPreviewView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->streamPreview:Lcom/discord/views/StreamPreviewView;

    new-instance v2, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$4;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser$onConfigure$4;-><init>(Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ListItem$VoiceUser;->getParticipant()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object p1

    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating()Z

    move-result p1

    if-ne p1, v4, :cond_10

    const/4 v3, 0x1

    :cond_10
    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->streamPreview:Lcom/discord/views/StreamPreviewView;

    invoke-virtual {p1, v1, v5, v3}, Lcom/discord/views/StreamPreviewView;->a(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Z)V

    goto :goto_f

    :cond_11
    :goto_e
    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->streamPreview:Lcom/discord/views/StreamPreviewView;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_12
    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->deafenIndicator:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->muteIndicator:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->streamPreview:Lcom/discord/views/StreamPreviewView;

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->videoIndicator:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->spectatingIndicator:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->liveIndicator:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_f
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/sheet/CallParticipantsAdapter$ViewHolderUser;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
