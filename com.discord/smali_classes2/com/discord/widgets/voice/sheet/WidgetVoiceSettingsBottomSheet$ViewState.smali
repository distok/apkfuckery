.class public interface abstract Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;
.super Ljava/lang/Object;
.source "WidgetVoiceSettingsBottomSheet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewState"
.end annotation


# virtual methods
.method public abstract getChannel()Lcom/discord/models/domain/ModelChannel;
.end method

.method public abstract getNoiseCancellationEnabled()Z
.end method

.method public abstract getShowInviteItem()Z
.end method

.method public abstract getShowVoiceParticipants()Z
.end method

.method public abstract getShowVoiceParticipantsToggle()Z
.end method
