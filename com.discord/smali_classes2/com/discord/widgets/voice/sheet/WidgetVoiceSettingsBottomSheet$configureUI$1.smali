.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;
.super Ljava/lang/Object;
.source "WidgetVoiceSettingsBottomSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;->configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;

.field public final synthetic this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    iput-object p2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;->$viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet;

    invoke-virtual {v0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v0

    const-string v1, "view"

    const-string v2, "view.context"

    invoke-static {p1, v1, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$configureUI$1;->$viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;

    invoke-interface {v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceSettingsBottomSheet$ViewState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    const-string v2, "Voice Call"

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/utilities/channel/ChannelUtils;->inviteToChannel(Landroid/content/Context;Lcom/discord/models/domain/ModelChannel;Ljava/lang/String;)V

    return-void
.end method
