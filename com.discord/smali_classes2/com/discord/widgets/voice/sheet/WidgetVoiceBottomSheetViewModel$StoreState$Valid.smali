.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;
.super Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;
.source "WidgetVoiceBottomSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Valid"
.end annotation


# instance fields
.field private final callModel:Lcom/discord/widgets/voice/model/CallModel;

.field private final mobileScreenshareExperimentEnabled:Z

.field private final myPermissions:Ljava/lang/Long;

.field private final noiseCancellation:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)V
    .locals 1

    const-string v0, "callModel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    iput-object p2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    iput-object p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    iput-boolean p4, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;ZILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->copy(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/voice/model/CallModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    return-object v0
.end method

.method public final component2()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    return v0
.end method

.method public final copy(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;
    .locals 1

    const-string v0, "callModel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;-><init>(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    iget-boolean p1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCallModel()Lcom/discord/widgets/voice/model/CallModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    return-object v0
.end method

.method public final getMobileScreenshareExperimentEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    return v0
.end method

.method public final getMyPermissions()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final getNoiseCancellation()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Valid(callModel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->callModel:Lcom/discord/widgets/voice/model/CallModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", noiseCancellation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->noiseCancellation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myPermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->myPermissions:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mobileScreenshareExperimentEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;->mobileScreenshareExperimentEnabled:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
