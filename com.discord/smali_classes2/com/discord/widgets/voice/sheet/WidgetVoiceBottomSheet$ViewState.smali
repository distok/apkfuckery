.class public interface abstract Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$ViewState;
.super Ljava/lang/Object;
.source "WidgetVoiceBottomSheet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ViewState"
.end annotation


# virtual methods
.method public abstract getBottomContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
.end method

.method public abstract getCanInvite()Z
.end method

.method public abstract getCenterContent()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$CenterContent;
.end method

.method public abstract getChannelId()J
.end method

.method public abstract getGuildId()Ljava/lang/Long;
.end method

.method public abstract getShowDeafenButtonInHeader()Z
.end method

.method public abstract getSubtitle()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract isDeafened()Z
.end method

.method public abstract isNoiseCancellationActive()Ljava/lang/Boolean;
.end method
