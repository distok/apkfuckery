.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;
.super Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
.source "WidgetVoiceBottomSheet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Controls"
.end annotation


# instance fields
.field private final audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

.field private final cameraState:Lcom/discord/widgets/voice/model/CameraState;

.field private final inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

.field private final isDeafened:Z

.field private final isMuted:Z

.field private final isScreensharing:Z

.field private final showScreenShareSparkle:Z

.field private final showScreenshare:Z


# direct methods
.method public constructor <init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZLcom/discord/widgets/voice/model/CameraState;ZZZ)V
    .locals 1

    const-string v0, "inputMode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioDevicesState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cameraState"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    iput-object p2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iput-boolean p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    iput-boolean p4, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    iput-object p5, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    iput-boolean p6, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    iput-boolean p7, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    iput-boolean p8, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZLcom/discord/widgets/voice/model/CameraState;ZZZILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move p3, v4

    move p4, v5

    move-object p5, v6

    move p6, v7

    move/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->copy(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZLcom/discord/widgets/voice/model/CameraState;ZZZ)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    return v0
.end method

.method public final component5()Lcom/discord/widgets/voice/model/CameraState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    return v0
.end method

.method public final copy(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZLcom/discord/widgets/voice/model/CameraState;ZZZ)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;
    .locals 10

    const-string v0, "inputMode"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioDevicesState"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cameraState"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    move-object v1, v0

    move v4, p3

    move v5, p4

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;-><init>(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZLcom/discord/widgets/voice/model/CameraState;ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    iget-object v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    iget-boolean p1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-object v0
.end method

.method public final getCameraState()Lcom/discord/widgets/voice/model/CameraState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    return-object v0
.end method

.method public final getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    return-object v0
.end method

.method public final getShowScreenShareSparkle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    return v0
.end method

.method public final getShowScreenshare()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    if-eqz v1, :cond_7

    goto :goto_2

    :cond_7
    move v3, v1

    :goto_2
    add-int/2addr v0, v3

    return v0
.end method

.method public final isDeafened()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    return v0
.end method

.method public final isMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    return v0
.end method

.method public final isScreensharing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Controls(inputMode="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->inputMode:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioDevicesState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDeafened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isDeafened:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cameraState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showScreenshare="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenshare:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showScreenShareSparkle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->showScreenShareSparkle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isScreensharing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$BottomContent$Controls;->isScreensharing:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
