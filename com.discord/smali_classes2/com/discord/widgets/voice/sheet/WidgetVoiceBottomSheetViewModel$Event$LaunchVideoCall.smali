.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;
.super Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.source "WidgetVoiceBottomSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LaunchVideoCall"
.end annotation


# instance fields
.field private final autoTargetStreamKey:Ljava/lang/String;

.field private final channelId:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;JLjava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-object p3, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->copy(JLjava/lang/String;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;-><init>(JLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAutoTargetStreamKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "LaunchVideoCall(channelId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", autoTargetStreamKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Event$LaunchVideoCall;->autoTargetStreamKey:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
