.class public final Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;
.super Lcom/discord/tooltips/TooltipManager$Tooltip;
.source "NoiseCancellationTooltip.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;

    invoke-direct {v0}, Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;->INSTANCE:Lcom/discord/widgets/voice/sheet/NoiseCancellationTooltip;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "CACHE_KEY_NOISE_CANCELLATION_TOOLTIP_ACKNOWLEDGED"

    const-string v1, "NOISE_CANCELLATION_TOOLTIP"

    invoke-direct {p0, v0, v1}, Lcom/discord/tooltips/TooltipManager$Tooltip;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
