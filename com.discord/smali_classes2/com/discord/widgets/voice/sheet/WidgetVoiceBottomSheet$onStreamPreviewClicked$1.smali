.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;
.super Lx/m/c/k;
.source "WidgetVoiceBottomSheet.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $streamContext:Lcom/discord/utilities/streams/StreamContext;

.field public final synthetic this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    iput-object p2, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;->$streamContext:Lcom/discord/utilities/streams/StreamContext;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;->this$0:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    invoke-static {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->access$getViewModel$p(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$onStreamPreviewClicked$1;->$streamContext:Lcom/discord/utilities/streams/StreamContext;

    invoke-virtual {v1}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel;->onStreamPreviewClicked(Ljava/lang/String;)V

    return-void
.end method
