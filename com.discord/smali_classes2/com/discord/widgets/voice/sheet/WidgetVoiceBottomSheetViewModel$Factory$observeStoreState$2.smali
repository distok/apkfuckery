.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;
.super Ljava/lang/Object;
.source "WidgetVoiceBottomSheetViewModel.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "Lcom/discord/widgets/voice/model/CallModel;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;

    invoke-direct {v0}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;
    .locals 6

    if-eqz p1, :cond_3

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v1, v4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getVoiceSettings()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getNoiseProcessing()Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;->Cancellation:Lcom/discord/stores/StoreMediaSettings$NoiseProcessing;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v2, "mobileScreenshareExperimentEnabled"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result p2

    if-eqz p2, :cond_2

    :cond_1
    const/4 v4, 0x1

    :cond_2
    invoke-direct {v1, p1, v0, p3, v4}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Valid;-><init>(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;Z)V

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState$Invalid;

    :goto_1
    return-object v1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/voice/model/CallModel;

    check-cast p2, Ljava/lang/Boolean;

    check-cast p3, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$Factory$observeStoreState$2;->call(Lcom/discord/widgets/voice/model/CallModel;Ljava/lang/Boolean;Ljava/lang/Long;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
