.class public final Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$2;
.super Ljava/lang/Object;
.source "WidgetVoiceBottomSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->configureUI(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$2;->$viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v1

    const-string v0, "view"

    const-string v2, "view.context"

    invoke-static {p1, v0, v2}, Lf/e/c/a/a;->Z(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Context;

    move-result-object v2

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$2;->$viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getChannelId()J

    move-result-wide v3

    iget-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$configureUI$2;->$viewState:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheetViewModel$ViewState;->getGuildId()Ljava/lang/Long;

    move-result-object v5

    const-string v6, "Voice Channel Bottom Sheet"

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/channel/ChannelUtils;->inviteToChannel(Landroid/content/Context;JLjava/lang/Long;Ljava/lang/String;)V

    return-void
.end method
