.class public final Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetNoiseCancellationBottomSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Factory;-><init>(Lcom/discord/stores/StoreMediaSettings;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreMediaSettings;)V
    .locals 1

    const-string v0, "mediaSettingsStore"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Factory;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreMediaSettings;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Factory;-><init>(Lcom/discord/stores/StoreMediaSettings;)V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel;

    iget-object v0, p0, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel$Factory;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-direct {p1, v0}, Lcom/discord/widgets/voice/sheet/WidgetNoiseCancellationBottomSheetViewModel;-><init>(Lcom/discord/stores/StoreMediaSettings;)V

    return-object p1
.end method
