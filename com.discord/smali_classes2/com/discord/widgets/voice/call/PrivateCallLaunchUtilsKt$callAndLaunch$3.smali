.class public final Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;
.super Lx/m/c/k;
.source "PrivateCallLaunchUtils.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt;->callAndLaunch(JZLcom/discord/app/AppPermissions$Requests;Landroid/content/Context;Lcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $appComponent:Lcom/discord/app/AppComponent;

.field public final synthetic $channelId:J

.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $fragmentManager:Landroidx/fragment/app/FragmentManager;

.field public final synthetic $isVideo:Z

.field public final synthetic $weakContext:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(JLjava/lang/ref/WeakReference;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Z)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$channelId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$weakContext:Ljava/lang/ref/WeakReference;

    iput-object p4, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$appComponent:Lcom/discord/app/AppComponent;

    iput-object p5, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$context:Landroid/content/Context;

    iput-object p6, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$fragmentManager:Landroidx/fragment/app/FragmentManager;

    iput-boolean p7, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$isVideo:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 12

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreRtcConnection;->getConnectionState()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;->INSTANCE:Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$2;->INSTANCE:Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$2;

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$3;->INSTANCE:Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$3;

    invoke-static {v1, v2, v3}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$4;

    invoke-direct {v2, p0}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$4;-><init>(Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;)V

    const-wide/16 v3, -0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0xfa

    invoke-static {v2, v3, v5, v6, v4}, Lf/a/b/r;->c(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v1

    const-string v2, "Observable\n        .comb\u2026       )\n        .take(1)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    new-instance v9, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$5;

    invoke-direct {v9, p0}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$5;-><init>(Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    new-instance v1, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$6;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$6;-><init>(Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;)V

    iget-boolean v2, p0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->$isVideo:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$7;

    invoke-direct {v2, p0, v1}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$7;-><init>(Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$6;)V

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreMediaEngine;->selectDefaultVideoDevice(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$6;->invoke()V

    :goto_0
    return-void
.end method
