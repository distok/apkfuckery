.class public final Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;
.super Ljava/lang/Object;
.source "PrivateCallLaunchUtils.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/rtcconnection/RtcConnection$State;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;

    invoke-direct {v0}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;->INSTANCE:Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/rtcconnection/RtcConnection$State;)Ljava/lang/Boolean;
    .locals 1

    sget-object v0, Lcom/discord/rtcconnection/RtcConnection$State$f;->a:Lcom/discord/rtcconnection/RtcConnection$State$f;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/rtcconnection/RtcConnection$State;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3$1;->call(Lcom/discord/rtcconnection/RtcConnection$State;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
