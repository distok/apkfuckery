.class public final Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;
.super Ljava/lang/Object;
.source "WidgetVoiceCallIncoming.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;->configureUI(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $callModel:Lcom/discord/widgets/voice/model/CallModel;

.field public final synthetic $model:Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;

.field public final synthetic this$0:Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;Lcom/discord/widgets/voice/model/CallModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;->this$0:Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;

    iput-object p2, p0, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;->$model:Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;

    iput-object p3, p0, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;->$callModel:Lcom/discord/widgets/voice/model/CallModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;->this$0:Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;

    iget-object v0, p0, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;->$model:Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;

    iget-object v1, p0, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$configureUI$3;->$callModel:Lcom/discord/widgets/voice/model/CallModel;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming;->onConnect(Lcom/discord/widgets/voice/call/WidgetVoiceCallIncoming$Model;Z)V

    return-void
.end method
