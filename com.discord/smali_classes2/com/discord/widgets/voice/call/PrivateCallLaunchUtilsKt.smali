.class public final Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt;
.super Ljava/lang/Object;
.source "PrivateCallLaunchUtils.kt"


# direct methods
.method public static final callAndLaunch(JZLcom/discord/app/AppPermissions$Requests;Landroid/content/Context;Lcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;)V
    .locals 9

    const-string v0, "appPermissionsRequests"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appComponent"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragmentManager"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    new-instance v0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$1;

    invoke-direct {v0, p3}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$1;-><init>(Lcom/discord/app/AppPermissions$Requests;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$2;

    invoke-direct {v0, p3}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$2;-><init>(Lcom/discord/app/AppPermissions$Requests;)V

    :goto_0
    new-instance p3, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;

    move-object v1, p3

    move-wide v2, p0

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    move v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/voice/call/PrivateCallLaunchUtilsKt$callAndLaunch$3;-><init>(JLjava/lang/ref/WeakReference;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;Z)V

    invoke-interface {v0, p3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
