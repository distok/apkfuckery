.class public final Lcom/discord/widgets/voice/model/CallModel$Companion$observeChunk$1;
.super Ljava/lang/Object;
.source "CallModel.kt"

# interfaces
.implements Lrx/functions/Func9;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/model/CallModel$Companion;->observeChunk(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "T8:",
        "Ljava/lang/Object;",
        "T9:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func9<",
        "Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
        "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;",
        "Ljava/lang/Long;",
        "Ljava/util/List<",
        "+",
        "Lco/discord/media_engine/VideoInputDeviceDescription;",
        ">;",
        "Lco/discord/media_engine/VideoInputDeviceDescription;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$observeChunk$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Long;Ljava/lang/Long;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/Boolean;)Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;"
        }
    .end annotation

    new-instance v13, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    move-object v14, p0

    iget-object v1, v14, Lcom/discord/widgets/voice/model/CallModel$Companion$observeChunk$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    const-string v0, "myUserId"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-string v0, "timeConnectedMs"

    move-object/from16 v4, p2

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-string v0, "voiceConfig"

    move-object/from16 v6, p3

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p4 .. p4}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v7

    const-string v0, "voiceParticipants"

    move-object/from16 v8, p5

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "videoDevices"

    move-object/from16 v10, p7

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isVideoSupported"

    move-object/from16 v9, p9

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p9 .. p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    move-object v0, v13

    move-object/from16 v9, p6

    move-object/from16 v11, p8

    invoke-direct/range {v0 .. v12}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;-><init>(Lcom/discord/models/domain/ModelChannel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Z)V

    return-object v13
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    check-cast p3, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    check-cast p4, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    check-cast p5, Ljava/util/Map;

    check-cast p6, Ljava/lang/Long;

    check-cast p7, Ljava/util/List;

    check-cast p8, Lco/discord/media_engine/VideoInputDeviceDescription;

    check-cast p9, Ljava/lang/Boolean;

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/voice/model/CallModel$Companion$observeChunk$1;->call(Ljava/lang/Long;Ljava/lang/Long;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Ljava/lang/Boolean;)Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    move-result-object p1

    return-object p1
.end method
