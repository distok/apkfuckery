.class public final Lcom/discord/widgets/voice/model/CallModel$Companion$get$1$3;
.super Ljava/lang/Object;
.source "CallModel.kt"

# interfaces
.implements Lrx/functions/Func9;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/model/CallModel$Companion$get$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "T8:",
        "Ljava/lang/Object;",
        "T9:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func9<",
        "Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;",
        "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
        "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
        "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Ljava/lang/Long;",
        "Lcom/discord/widgets/voice/model/CallModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$get$1$3;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/voice/VoiceChannelJoinability;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;)Lcom/discord/widgets/voice/model/CallModel;
    .locals 22

    move-object/from16 v0, p0

    sget-object v1, Lcom/discord/widgets/voice/model/CallModel;->Companion:Lcom/discord/widgets/voice/model/CallModel$Companion;

    iget-object v2, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$get$1$3;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getMyUserId()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getTimeConnectedMs()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getVoiceConfig()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getVoiceParticipants()Ljava/util/Map;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getChannelPermissions()Ljava/lang/Long;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getVideoDevices()Ljava/util/List;

    move-result-object v12

    iget-object v3, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$get$1$3;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v13

    if-nez p9, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p9 .. p9}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    cmp-long v3, v13, v15

    if-nez v3, :cond_1

    const/4 v3, 0x1

    const/4 v13, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x0

    const/4 v13, 0x0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported()Z

    move-result v16

    const-string v3, "callFeedbackSampleRateDenominator"

    move-object/from16 v15, p4

    invoke-static {v15, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Integer;->intValue()I

    move-result v18

    const-string v3, "streamFeedbackSampleRateDenominator"

    move-object/from16 v15, p5

    invoke-static {v15, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p5 .. p5}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const-string v3, "audioDevicesState"

    move-object/from16 v15, p6

    invoke-static {v15, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "voiceChannelJoinability"

    move-object/from16 v0, p7

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v3, p8

    move-object/from16 v15, p2

    move-object/from16 v17, p3

    move-object/from16 v20, p6

    move-object/from16 v21, p7

    invoke-static/range {v1 .. v21}, Lcom/discord/widgets/voice/model/CallModel$Companion;->access$create(Lcom/discord/widgets/voice/model/CallModel$Companion;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelGuild;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;ZLco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;ZLcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/voice/VoiceChannelJoinability;)Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    check-cast p2, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    check-cast p3, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    check-cast p4, Ljava/lang/Integer;

    check-cast p5, Ljava/lang/Integer;

    check-cast p6, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    check-cast p7, Lcom/discord/utilities/voice/VoiceChannelJoinability;

    check-cast p8, Lcom/discord/models/domain/ModelGuild;

    check-cast p9, Ljava/lang/Long;

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/voice/model/CallModel$Companion$get$1$3;->call(Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/voice/VoiceChannelJoinability;Lcom/discord/models/domain/ModelGuild;Ljava/lang/Long;)Lcom/discord/widgets/voice/model/CallModel;

    move-result-object p1

    return-object p1
.end method
