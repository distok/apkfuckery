.class public final Lcom/discord/widgets/voice/model/CallModel;
.super Ljava/lang/Object;
.source "CallModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/model/CallModel$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/voice/model/CallModel$Companion;


# instance fields
.field private final activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

.field private final audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

.field private final audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

.field private final callFeedbackSampleRateDenominator:I

.field private final cameraState:Lcom/discord/widgets/voice/model/CameraState;

.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelPermissions:Ljava/lang/Long;

.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final guildMaxVideoChannelMembers:Ljava/lang/Integer;

.field private final isChannelSelected:Z

.field private final isVideoCall:Z

.field private final isVideoSupported:Z

.field private final myId:J

.field private final numUsersConnected:I

.field private final participants:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation
.end field

.field private final rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

.field private final selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

.field private final streamFeedbackSampleRateDenominator:I

.field private final timeConnectedMs:J

.field private final videoDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation
.end field

.field private final voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

.field private final voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/voice/model/CallModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/model/CallModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/model/CallModel;->Companion:Lcom/discord/widgets/voice/model/CallModel$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/utilities/media/AudioOutputState;ZLcom/discord/models/domain/ModelChannel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;JJ",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
            "II",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Z",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Z",
            "Lcom/discord/models/domain/ModelChannel;",
            "I",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v5, p13

    move-object/from16 v6, p17

    move/from16 v7, p18

    move-object/from16 v8, p19

    move-object/from16 v9, p22

    const-string v10, "participants"

    invoke-static {p1, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "videoDevices"

    invoke-static {v2, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "voiceSettings"

    invoke-static {v4, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "voiceChannelJoinability"

    invoke-static {v5, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "audioOutputState"

    invoke-static {v6, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "channel"

    invoke-static {v8, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "audioDevicesState"

    invoke-static {v9, v10}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    move-wide v10, p2

    iput-wide v10, v0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    move-wide/from16 v10, p4

    iput-wide v10, v0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    iput-object v3, v0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    iput-object v4, v0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-object/from16 v2, p9

    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move/from16 v2, p10

    iput v2, v0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    move/from16 v2, p11

    iput v2, v0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    move-object/from16 v2, p12

    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    iput-object v5, v0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    move-object/from16 v2, p14

    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    move-object/from16 v2, p15

    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    move/from16 v2, p16

    iput-boolean v2, v0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    iput-object v6, v0, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iput-boolean v7, v0, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    iput-object v8, v0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    move/from16 v2, p20

    iput v2, v0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    move-object/from16 v2, p21

    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    iput-object v9, v0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    if-eqz v3, :cond_0

    sget-object v2, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_OFF:Lcom/discord/widgets/voice/model/CameraState;

    :goto_0
    iput-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v4

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_3

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :cond_6
    :goto_3
    iput-boolean v2, v0, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall:Z

    return-void
.end method

.method private final component15()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    return-object v0
.end method

.method private final component16()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    return v0
.end method

.method private final component17()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method private final component18()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    return v0
.end method

.method private final component19()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method private final component20()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/model/CallModel;Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/utilities/media/AudioOutputState;ZLcom/discord/models/domain/ModelChannel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ILjava/lang/Object;)Lcom/discord/widgets/voice/model/CallModel;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p23

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    goto :goto_1

    :cond_1
    move-wide/from16 v3, p2

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    goto :goto_2

    :cond_2
    move-wide/from16 v5, p4

    :goto_2
    and-int/lit8 v7, v1, 0x8

    if-eqz v7, :cond_3

    iget-object v7, v0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object/from16 v7, p6

    :goto_3
    and-int/lit8 v8, v1, 0x10

    if-eqz v8, :cond_4

    iget-object v8, v0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    goto :goto_4

    :cond_4
    move-object/from16 v8, p7

    :goto_4
    and-int/lit8 v9, v1, 0x20

    if-eqz v9, :cond_5

    iget-object v9, v0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    goto :goto_5

    :cond_5
    move-object/from16 v9, p8

    :goto_5
    and-int/lit8 v10, v1, 0x40

    if-eqz v10, :cond_6

    iget-object v10, v0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    goto :goto_6

    :cond_6
    move-object/from16 v10, p9

    :goto_6
    and-int/lit16 v11, v1, 0x80

    if-eqz v11, :cond_7

    iget v11, v0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    goto :goto_7

    :cond_7
    move/from16 v11, p10

    :goto_7
    and-int/lit16 v12, v1, 0x100

    if-eqz v12, :cond_8

    iget v12, v0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    goto :goto_8

    :cond_8
    move/from16 v12, p11

    :goto_8
    and-int/lit16 v13, v1, 0x200

    if-eqz v13, :cond_9

    iget-object v13, v0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    goto :goto_9

    :cond_9
    move-object/from16 v13, p12

    :goto_9
    and-int/lit16 v14, v1, 0x400

    if-eqz v14, :cond_a

    iget-object v14, v0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    goto :goto_a

    :cond_a
    move-object/from16 v14, p13

    :goto_a
    and-int/lit16 v15, v1, 0x800

    if-eqz v15, :cond_b

    iget-object v15, v0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    goto :goto_b

    :cond_b
    move-object/from16 v15, p14

    :goto_b
    move-object/from16 p14, v15

    and-int/lit16 v15, v1, 0x1000

    if-eqz v15, :cond_c

    iget-object v15, v0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    goto :goto_c

    :cond_c
    move-object/from16 v15, p15

    :goto_c
    move-object/from16 p15, v15

    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-boolean v15, v0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    goto :goto_d

    :cond_d
    move/from16 v15, p16

    :goto_d
    move/from16 p16, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p17

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move-object/from16 p17, v15

    if-eqz v16, :cond_f

    iget-boolean v15, v0, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    goto :goto_f

    :cond_f
    move/from16 v15, p18

    :goto_f
    const/high16 v16, 0x10000

    and-int v16, v1, v16

    move/from16 p18, v15

    if-eqz v16, :cond_10

    iget-object v15, v0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    goto :goto_10

    :cond_10
    move-object/from16 v15, p19

    :goto_10
    const/high16 v16, 0x20000

    and-int v16, v1, v16

    move-object/from16 p19, v15

    if-eqz v16, :cond_11

    iget v15, v0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    goto :goto_11

    :cond_11
    move/from16 v15, p20

    :goto_11
    const/high16 v16, 0x40000

    and-int v16, v1, v16

    move/from16 p20, v15

    if-eqz v16, :cond_12

    iget-object v15, v0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    goto :goto_12

    :cond_12
    move-object/from16 v15, p21

    :goto_12
    const/high16 v16, 0x80000

    and-int v1, v1, v16

    if-eqz v1, :cond_13

    iget-object v1, v0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    goto :goto_13

    :cond_13
    move-object/from16 v1, p22

    :goto_13
    move-object/from16 p1, v2

    move-wide/from16 p2, v3

    move-wide/from16 p4, v5

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move/from16 p10, v11

    move/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p21, v15

    move-object/from16 p22, v1

    invoke-virtual/range {p0 .. p22}, Lcom/discord/widgets/voice/model/CallModel;->copy(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/utilities/media/AudioOutputState;ZLcom/discord/models/domain/ModelChannel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final canInvite()Z
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelChannel;->isPrivateType(I)Z

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isVoiceChannel()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const-wide/16 v4, 0x1

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    invoke-static {v4, v5, v1}, Lcom/discord/utilities/permissions/PermissionUtils;->can(JLjava/lang/Long;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :cond_2
    :goto_1
    return v2
.end method

.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    return-object v0
.end method

.method public final component10()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-object v0
.end method

.method public final component11()Lcom/discord/utilities/voice/VoiceChannelJoinability;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    return-object v0
.end method

.method public final component12()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component13()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component14()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    return v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    return-wide v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Lco/discord/media_engine/VideoInputDeviceDescription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-object v0
.end method

.method public final component6()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    return-object v0
.end method

.method public final component7()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    return-object v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    return v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    return v0
.end method

.method public final copy(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/utilities/media/AudioOutputState;ZLcom/discord/models/domain/ModelChannel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)Lcom/discord/widgets/voice/model/CallModel;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;JJ",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
            "II",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
            "Ljava/lang/Integer;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Z",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Z",
            "Lcom/discord/models/domain/ModelChannel;",
            "I",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            ")",
            "Lcom/discord/widgets/voice/model/CallModel;"
        }
    .end annotation

    move-object/from16 v1, p1

    move-wide/from16 v2, p2

    move-wide/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move/from16 v16, p16

    move-object/from16 v17, p17

    move/from16 v18, p18

    move-object/from16 v19, p19

    move/from16 v20, p20

    move-object/from16 v21, p21

    move-object/from16 v22, p22

    const-string v0, "participants"

    move-object/from16 v23, v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "videoDevices"

    move-object/from16 v1, p6

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceSettings"

    move-object/from16 v1, p8

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceChannelJoinability"

    move-object/from16 v1, p13

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioOutputState"

    move-object/from16 v1, p17

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    move-object/from16 v1, p19

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioDevicesState"

    move-object/from16 v1, p22

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v24, Lcom/discord/widgets/voice/model/CallModel;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct/range {v0 .. v22}, Lcom/discord/widgets/voice/model/CallModel;-><init>(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/utilities/media/AudioOutputState;ZLcom/discord/models/domain/ModelChannel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V

    return-object v24
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/model/CallModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/model/CallModel;

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    iget v1, p1, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    iget v1, p1, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    iget v1, p1, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    iget-object p1, p1, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    return-object v0
.end method

.method public final getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    return-object v0
.end method

.method public final getCallDurationMs(Lcom/discord/utilities/time/Clock;)J
    .locals 4

    const-string v0, "clock"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final getCallFeedbackSampleRateDenominator()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    return v0
.end method

.method public final getCameraState()Lcom/discord/widgets/voice/model/CameraState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->cameraState:Lcom/discord/widgets/voice/model/CameraState;

    return-object v0
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getChannelPermissions()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final getDmRecipient()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    cmp-long v8, v4, v6

    if-eqz v8, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    move-object v1, v3

    :cond_3
    check-cast v1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    :goto_1
    return-object v1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final getGuildMaxVideoChannelMembers()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    move-result-object v0

    return-object v0
.end method

.method public final getMyId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    return-wide v0
.end method

.method public final getNumUsersConnected()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    return v0
.end method

.method public final getParticipants()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    return-object v0
.end method

.method public final getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    return-object v0
.end method

.method public final getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-object v0
.end method

.method public final getStreamFeedbackSampleRateDenominator()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    return v0
.end method

.method public final getTimeConnectedMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    return-wide v0
.end method

.method public final getVideoDevices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    return-object v0
.end method

.method public final getVoiceChannelJoinability()Lcom/discord/utilities/voice/VoiceChannelJoinability;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    return-object v0
.end method

.method public final getVoiceSettings()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lco/discord/media_engine/VideoInputDeviceDescription;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :cond_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/discord/utilities/media/AudioOutputState;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_a
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    if-eqz v2, :cond_b

    goto :goto_a

    :cond_b
    move v3, v2

    :goto_a
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_c
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->hashCode()I

    move-result v1

    :cond_e
    add-int/2addr v0, v1

    return v0
.end method

.method public final isChannelSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    return v0
.end method

.method public final isConnected()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isDeafenedByAnySource()Z
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/model/CallModel;->isSelfDeafened()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/model/CallModel;->isServerDeafened()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public final isMeMutedByAnySource()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isMutedByAnySource()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isMuted()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isMute()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isSelfDeafened()Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->isSelfDeafened()Z

    move-result v0

    return v0
.end method

.method public final isServerDeafened()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isDeaf()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isStreaming()Z
    .locals 5

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isSuppressed()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelVoice$State;->isSuppress()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isVideoCall()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "CallModel(participants="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->participants:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->myId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", timeConnectedMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel;->timeConnectedMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", videoDevices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->videoDevices:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedVideoDevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voiceSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceSettings:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rtcConnectionMetadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->rtcConnectionMetadata:Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", callFeedbackSampleRateDenominator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/voice/model/CallModel;->callFeedbackSampleRateDenominator:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", streamFeedbackSampleRateDenominator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/voice/model/CallModel;->streamFeedbackSampleRateDenominator:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", activeStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->activeStream:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voiceChannelJoinability="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->voiceChannelJoinability:Lcom/discord/utilities/voice/VoiceChannelJoinability;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildMaxVideoChannelMembers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->guildMaxVideoChannelMembers:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guild="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isChannelSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", audioOutputState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isVideoSupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/model/CallModel;->isVideoSupported:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numUsersConnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/voice/model/CallModel;->numUsersConnected:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", channelPermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->channelPermissions:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioDevicesState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel;->audioDevicesState:Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
