.class public final Lcom/discord/widgets/voice/model/CallModel$Companion;
.super Ljava/lang/Object;
.source "CallModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/model/CallModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/model/CallModel$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$create(Lcom/discord/widgets/voice/model/CallModel$Companion;Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelGuild;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;ZLco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;ZLcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/voice/VoiceChannelJoinability;)Lcom/discord/widgets/voice/model/CallModel;
    .locals 1

    invoke-direct/range {p0 .. p20}, Lcom/discord/widgets/voice/model/CallModel$Companion;->create(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelGuild;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;ZLco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;ZLcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/voice/VoiceChannelJoinability;)Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$observeChunk(Lcom/discord/widgets/voice/model/CallModel$Companion;Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/model/CallModel$Companion;->observeChunk(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final create(Lcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelGuild;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;ZLco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;ZLcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreAudioDevices$AudioDevicesState;Lcom/discord/utilities/voice/VoiceChannelJoinability;)Lcom/discord/widgets/voice/model/CallModel;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "Lcom/discord/models/domain/ModelGuild;",
            "JJ",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;Z",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            "Z",
            "Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;",
            "II",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            "Lcom/discord/utilities/voice/VoiceChannelJoinability;",
            ")",
            "Lcom/discord/widgets/voice/model/CallModel;"
        }
    .end annotation

    invoke-interface/range {p9 .. p9}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/16 v22, 0x0

    goto :goto_1

    :cond_0
    invoke-interface/range {p9 .. p9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move/from16 v22, v1

    :goto_1
    new-instance v0, Lcom/discord/widgets/voice/model/CallModel;

    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuild;->getMaxVideoChannelUsers()Ljava/lang/Integer;

    move-result-object v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    move-object/from16 v16, v1

    move-object v2, v0

    move-object/from16 v3, p9

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v8, p11

    move-object/from16 v9, p13

    move-object/from16 v10, p7

    move-object/from16 v11, p16

    move/from16 v12, p17

    move/from16 v13, p18

    move-object/from16 v14, p14

    move-object/from16 v15, p20

    move-object/from16 v17, p2

    move/from16 v18, p12

    move-object/from16 v19, p8

    move/from16 v20, p15

    move-object/from16 v21, p1

    move-object/from16 v23, p10

    move-object/from16 v24, p19

    invoke-direct/range {v2 .. v24}, Lcom/discord/widgets/voice/model/CallModel;-><init>(Ljava/util/Map;JJLjava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;IILcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lcom/discord/utilities/voice/VoiceChannelJoinability;Ljava/lang/Integer;Lcom/discord/models/domain/ModelGuild;ZLcom/discord/utilities/media/AudioOutputState;ZLcom/discord/models/domain/ModelChannel;ILjava/lang/Long;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;)V

    return-object v0
.end method

.method private final observeChunk(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeTimeSelectedMs()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings;->getVoiceConfiguration()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAudioDevices()Lcom/discord/stores/StoreAudioDevices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreAudioDevices;->getAudioDevicesState()Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceParticipants()Lcom/discord/stores/StoreVoiceParticipants;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/discord/stores/StoreVoiceParticipants;->get(J)Lrx/Observable;

    move-result-object v1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v7, 0xfa

    invoke-static {v1, v7, v8, v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    invoke-virtual {v1, v7, v8}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaEngine;->getVideoInputDevices()Lrx/Observable;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaEngine()Lcom/discord/stores/StoreMediaEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaEngine;->getSelectedVideoInputDevice()Lrx/Observable;

    move-result-object v9

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVideoSupport()Lcom/discord/stores/StoreVideoSupport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreVideoSupport;->get()Lrx/Observable;

    move-result-object v10

    new-instance v11, Lcom/discord/widgets/voice/model/CallModel$Companion$observeChunk$1;

    invoke-direct {v11, p1}, Lcom/discord/widgets/voice/model/CallModel$Companion$observeChunk$1;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-static/range {v2 .. v11}, Lrx/Observable;->c(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable\n          .co\u2026            )\n          }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final get(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/model/CallModel;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/model/CallModel$Companion$get$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/voice/model/CallModel$Companion$get$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "StoreStream\n          .g\u2026            }\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
