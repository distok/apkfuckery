.class public final Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;
.super Ljava/lang/Object;
.source "CallModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/model/CallModel$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Chunk"
.end annotation


# instance fields
.field private final audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final channelPermissions:Ljava/lang/Long;

.field private final isVideoSupported:Z

.field private final myUserId:J

.field private final selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

.field private final timeConnectedMs:J

.field private final videoDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation
.end field

.field private final voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

.field private final voiceParticipants:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "JJ",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Z)V"
        }
    .end annotation

    const-string v0, "channel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceConfig"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioOutputState"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceParticipants"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "videoDevices"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-wide p2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    iput-wide p4, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    iput-object p6, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iput-object p7, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iput-object p8, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    iput-object p9, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    iput-object p10, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    iput-object p11, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    iput-boolean p12, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;Lcom/discord/models/domain/ModelChannel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;ZILjava/lang/Object;)Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;
    .locals 13

    move-object v0, p0

    move/from16 v1, p13

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-wide v3, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    goto :goto_1

    :cond_1
    move-wide v3, p2

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    goto :goto_2

    :cond_2
    move-wide/from16 v5, p4

    :goto_2
    and-int/lit8 v7, v1, 0x8

    if-eqz v7, :cond_3

    iget-object v7, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    goto :goto_3

    :cond_3
    move-object/from16 v7, p6

    :goto_3
    and-int/lit8 v8, v1, 0x10

    if-eqz v8, :cond_4

    iget-object v8, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    goto :goto_4

    :cond_4
    move-object/from16 v8, p7

    :goto_4
    and-int/lit8 v9, v1, 0x20

    if-eqz v9, :cond_5

    iget-object v9, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    goto :goto_5

    :cond_5
    move-object/from16 v9, p8

    :goto_5
    and-int/lit8 v10, v1, 0x40

    if-eqz v10, :cond_6

    iget-object v10, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    goto :goto_6

    :cond_6
    move-object/from16 v10, p9

    :goto_6
    and-int/lit16 v11, v1, 0x80

    if-eqz v11, :cond_7

    iget-object v11, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    goto :goto_7

    :cond_7
    move-object/from16 v11, p10

    :goto_7
    and-int/lit16 v12, v1, 0x100

    if-eqz v12, :cond_8

    iget-object v12, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    goto :goto_8

    :cond_8
    move-object/from16 v12, p11

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-boolean v1, v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    goto :goto_9

    :cond_9
    move/from16 v1, p12

    :goto_9
    move-object p1, v2

    move-wide p2, v3

    move-wide/from16 p4, v5

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move/from16 p12, v1

    invoke-virtual/range {p0 .. p12}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->copy(Lcom/discord/models/domain/ModelChannel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Z)Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component10()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    return v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    return-wide v0
.end method

.method public final component4()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    return-object v0
.end method

.method public final component5()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    return-object v0
.end method

.method public final component6()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    return-object v0
.end method

.method public final component7()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final component8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    return-object v0
.end method

.method public final component9()Lco/discord/media_engine/VideoInputDeviceDescription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelChannel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Z)Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            "JJ",
            "Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;",
            "Lcom/discord/utilities/media/AudioOutputState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Z)",
            "Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;"
        }
    .end annotation

    const-string v0, "channel"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceConfig"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioOutputState"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceParticipants"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "videoDevices"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    move-object v1, v0

    move-wide/from16 v3, p2

    move-wide/from16 v5, p4

    move-object/from16 v10, p9

    move-object/from16 v12, p11

    move/from16 v13, p12

    invoke-direct/range {v1 .. v13}, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;-><init>(Lcom/discord/models/domain/ModelChannel;JJLcom/discord/stores/StoreMediaSettings$VoiceConfiguration;Lcom/discord/utilities/media/AudioOutputState;Ljava/util/Map;Ljava/lang/Long;Ljava/util/List;Lco/discord/media_engine/VideoInputDeviceDescription;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    iget-object v1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    iget-boolean p1, p1, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAudioOutputState()Lcom/discord/utilities/media/AudioOutputState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    return-object v0
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getChannelPermissions()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    return-object v0
.end method

.method public final getMyUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    return-wide v0
.end method

.method public final getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    return-object v0
.end method

.method public final getTimeConnectedMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    return-wide v0
.end method

.method public final getVideoDevices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    return-object v0
.end method

.method public final getVoiceConfig()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    return-object v0
.end method

.method public final getVoiceParticipants()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/utilities/media/AudioOutputState;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lco/discord/media_engine/VideoInputDeviceDescription;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public final isVideoSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Chunk(channel="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->myUserId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", timeConnectedMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->timeConnectedMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", voiceConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceConfig:Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", audioOutputState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->audioOutputState:Lcom/discord/utilities/media/AudioOutputState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", voiceParticipants="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->voiceParticipants:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", channelPermissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->channelPermissions:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", videoDevices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->videoDevices:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedVideoDevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->selectedVideoDevice:Lco/discord/media_engine/VideoInputDeviceDescription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isVideoSupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/voice/model/CallModel$Companion$Chunk;->isVideoSupported:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
