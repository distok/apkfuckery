.class public final Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;
.super Lcom/discord/tooltips/TooltipManager$Tooltip;
.source "VoiceControlsSheetSwipeTooltip.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;

    invoke-direct {v0}, Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "VOICE_CONTROLS_SHEET_SWIPE_ACKNOWLEDGED"

    const-string v1, "VOICE_CONTROLS_SHEET_SWIPE"

    invoke-direct {p0, v0, v1}, Lcom/discord/tooltips/TooltipManager$Tooltip;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
