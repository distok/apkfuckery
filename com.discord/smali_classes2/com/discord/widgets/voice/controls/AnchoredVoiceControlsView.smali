.class public final Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;
.super Landroid/widget/FrameLayout;
.source "AnchoredVoiceControlsView.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final cameraStateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final deafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final disconnectButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final muteStateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final pttButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final screenshareButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final speakerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v3, "pttButton"

    const-string v4, "getPttButton()Landroid/widget/Button;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v6, "cameraStateButton"

    const-string v7, "getCameraStateButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v6, "screenshareButton"

    const-string v7, "getScreenshareButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v6, "deafenButton"

    const-string v7, "getDeafenButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v6, "speakerButton"

    const-string v7, "getSpeakerButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v6, "muteStateButton"

    const-string v7, "getMuteStateButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    const-string v6, "disconnectButton"

    const-string v7, "getDisconnectButton()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f0a0b90

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->pttButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0b8b

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->cameraStateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0b91

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->screenshareButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0b8d

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->deafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0b92

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->speakerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0b8f

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->muteStateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0b8e

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->disconnectButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object p2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string p3, "logger"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p3, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_0

    :cond_0
    move-object p3, v0

    :goto_0
    if-nez p3, :cond_1

    new-instance p3, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {p3, p2}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object p2, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    move-object v5, p3

    sget-object p2, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string p2, "floatingViewManager"

    invoke-static {v5, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p2, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, Lcom/discord/tooltips/TooltipManager;

    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Lcom/discord/tooltips/TooltipManager;

    sget-object p2, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {p2}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object p2

    move-object v2, p2

    check-cast v2, Lf/a/l/a;

    sget-object p2, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {p2}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object p2

    move-object v3, p2

    check-cast v3, Ljava/util/Set;

    const/4 v4, 0x0

    const/4 v6, 0x4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object p2, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    :cond_3
    iput-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    const p2, 0x7f0d0020

    invoke-static {p1, p2, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getTooltipManager$p(Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;)Lcom/discord/tooltips/TooltipManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    return-object p0
.end method

.method private final configureScreenShareButtonSparkle(ZLcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/discord/app/AppComponent;",
            "Landroidx/fragment/app/FragmentManager;",
            "JZ",
            "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v9, p0

    if-eqz p1, :cond_1

    iget-object v0, v9, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v13, Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tooltip"

    invoke-static {v13, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/discord/tooltips/TooltipManager;->a:Ljava/util/Map;

    invoke-virtual {v13}, Lcom/discord/tooltips/TooltipManager$Tooltip;->getTooltipName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    new-instance v12, Lcom/discord/tooltips/SparkleView;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "screenshareButton.context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v12, v0, v1, v2}, Lcom/discord/tooltips/SparkleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v10, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;

    move-object v0, v10

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-wide/from16 v3, p4

    move/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;-><init>(Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v12, v10}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v17, 0x0

    iget-object v10, v9, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v11

    sget-object v14, Lcom/discord/floating_view_manager/FloatingViewGravity;->CENTER:Lcom/discord/floating_view_manager/FloatingViewGravity;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-interface/range {p2 .. p2}, Lcom/discord/app/AppComponent;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$2;->INSTANCE:Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$2;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "appComponent.unsubscribeSignal.map { Unit }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v19, 0x30

    const/16 v20, 0x0

    move-object/from16 v18, v0

    invoke-static/range {v10 .. v20}, Lcom/discord/tooltips/TooltipManager;->e(Lcom/discord/tooltips/TooltipManager;Landroid/view/View;Landroid/view/View;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;ILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    if-nez p1, :cond_2

    iget-object v0, v9, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v1, Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;

    invoke-virtual {v0, v1}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private final getCameraStateButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->cameraStateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getDeafenButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->deafenButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getDisconnectButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->disconnectButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getMuteStateButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->muteStateButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getPttButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->pttButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getScreenshareButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->screenshareButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getSpeakerButton()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->speakerButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final configureUI(Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;ZZZZZLcom/discord/widgets/voice/model/CameraState;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;",
            "Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;",
            "ZZZZZ",
            "Lcom/discord/widgets/voice/model/CameraState;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/discord/app/AppComponent;",
            "Landroidx/fragment/app/FragmentManager;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;JZ",
            "Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;",
            ")V"
        }
    .end annotation

    move/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    move-object/from16 v3, p8

    move-object/from16 v4, p9

    move-object/from16 v9, p10

    move-object/from16 v5, p11

    move-object/from16 v6, p12

    move-object/from16 v10, p13

    move-object/from16 v7, p14

    const-string v8, "inputMode"

    move-object/from16 v11, p1

    invoke-static {v11, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "audioDevicesState"

    move-object/from16 v12, p2

    invoke-static {v12, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "cameraState"

    invoke-static {v3, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onMutePressed"

    invoke-static {v4, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onScreenSharePressed"

    invoke-static {v9, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onSpeakerButtonPressed"

    invoke-static {v5, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onCameraButtonPressed"

    invoke-static {v6, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onDisconnectPressed"

    invoke-static {v10, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onDeafenPressed"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "appComponent"

    move-object/from16 v13, p15

    invoke-static {v13, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "fragmentManager"

    move-object/from16 v14, p16

    invoke-static {v14, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "onNavigateToScreenShareNfxSheet"

    move-object/from16 v15, p17

    invoke-static {v15, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "featureContext"

    move-object/from16 v11, p21

    invoke-static {v11, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getPttButton()Landroid/widget/Button;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Enum;->ordinal()I

    move-result v11

    const/4 v12, 0x1

    const/16 v16, 0x8

    const/16 v17, 0x0

    if-eq v11, v12, :cond_0

    const/16 v11, 0x8

    goto :goto_0

    :cond_0
    const/4 v11, 0x0

    :goto_0
    invoke-virtual {v8, v11}, Landroid/widget/Button;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getCameraStateButton()Landroid/widget/ImageView;

    move-result-object v8

    new-instance v11, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$1;

    invoke-direct {v11, v6}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getCameraStateButton()Landroid/widget/ImageView;

    move-result-object v6

    sget-object v8, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_DISABLED:Lcom/discord/widgets/voice/model/CameraState;

    if-eq v3, v8, :cond_1

    goto :goto_1

    :cond_1
    const/4 v12, 0x0

    :goto_1
    if-eqz v12, :cond_2

    const/4 v8, 0x0

    goto :goto_2

    :cond_2
    const/16 v8, 0x8

    :goto_2
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    sget-object v6, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    const v8, 0x7f040155

    const v11, 0x7f040158

    if-ne v3, v6, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getCameraStateButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v8}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v6

    invoke-static {v6}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getCameraStateButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    const v12, 0x7f1203d0

    invoke-virtual {v6, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getCameraStateButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v11}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v6

    invoke-static {v6}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getCameraStateButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    const v12, 0x7f1203cf

    invoke-virtual {v6, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_3
    invoke-virtual/range {p2 .. p2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v3

    instance-of v3, v3, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v3, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v3

    const v6, 0x7f08025c

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v3

    const v6, 0x7f08025d

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_4
    invoke-virtual/range {p2 .. p2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v3

    sget-object v6, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$Earpiece;

    if-eq v3, v6, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v3

    sget-object v6, Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;->INSTANCE:Lcom/discord/stores/StoreAudioDevices$OutputDevice$WiredAudio;

    if-ne v3, v6, :cond_5

    goto :goto_5

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v8}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v6

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_6

    :cond_6
    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v11}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v6

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v6

    instance-of v6, v6, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setActivated(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getSpeakerButton()Landroid/widget/ImageView;

    move-result-object v3

    new-instance v6, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$2;

    invoke-direct {v6, v5}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getDeafenButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setActivated(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getDeafenButton()Landroid/widget/ImageView;

    move-result-object v3

    xor-int/lit8 v5, p6, 0x1

    if-eqz v5, :cond_7

    const/4 v5, 0x0

    goto :goto_7

    :cond_7
    const/16 v5, 0x8

    :goto_7
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getDeafenButton()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v1, :cond_8

    const v1, 0x7f1218c2

    goto :goto_8

    :cond_8
    const v1, 0x7f1205d7

    :goto_8
    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getDeafenButton()Landroid/widget/ImageView;

    move-result-object v1

    new-instance v3, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$3;

    invoke-direct {v3, v7}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getMuteStateButton()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setActivated(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getMuteStateButton()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v0, :cond_9

    const v0, 0x7f1218c9

    goto :goto_9

    :cond_9
    const v0, 0x7f1210a9

    :goto_9
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getMuteStateButton()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$4;

    invoke-direct {v1, v4}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$4;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setActivated(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p6, :cond_a

    const/4 v1, 0x0

    goto :goto_a

    :cond_a
    const/16 v1, 0x8

    :goto_a
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v2, :cond_b

    const v1, 0x7f08038b

    goto :goto_b

    :cond_b
    const v1, 0x7f08038a

    :goto_b
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_d

    if-eqz v2, :cond_c

    const v2, 0x7f121754

    goto :goto_c

    :cond_c
    const v2, 0x7f1215e1

    :goto_c
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getScreenshareButton()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$5;

    invoke-direct {v1, v9}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$5;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    move/from16 v1, p7

    move-object/from16 v2, p15

    move-object/from16 v3, p16

    move-wide/from16 v4, p18

    move/from16 v6, p20

    move-object/from16 v7, p21

    move-object/from16 v8, p17

    move-object/from16 v9, p10

    invoke-direct/range {v0 .. v9}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->configureScreenShareButtonSparkle(ZLcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getDisconnectButton()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$6;

    invoke-direct {v1, v10}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureUI$6;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final hidePtt()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getPttButton()Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final setOnPttPressedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onPttPressed"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->getPttButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/utilities/press/OnPressListener;

    new-instance v2, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$setOnPttPressedListener$1;

    invoke-direct {v2, p1}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$setOnPttPressedListener$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {v1, v2}, Lcom/discord/utilities/press/OnPressListener;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
