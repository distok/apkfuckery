.class public final enum Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;
.super Ljava/lang/Enum;
.source "VoiceControlsOutputSelectorState.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

.field public static final enum BLUETOOTH_ON_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

.field public static final enum SPEAKER_OFF:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

.field public static final enum SPEAKER_OFF_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

.field public static final enum SPEAKER_ON:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

.field public static final enum SPEAKER_ON_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;


# instance fields
.field private final audioOutputIconRes:I

.field private final isButtonActive:Z

.field private final showMoreOptions:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    new-instance v7, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const-string v2, "SPEAKER_ON"

    const/4 v3, 0x0

    const v4, 0x7f080434

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;-><init>(Ljava/lang/String;IIZZ)V

    sput-object v7, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_ON:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const-string v9, "SPEAKER_OFF"

    const/4 v10, 0x1

    const v11, 0x7f080436

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v1

    invoke-direct/range {v8 .. v13}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;-><init>(Ljava/lang/String;IIZZ)V

    sput-object v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_OFF:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const-string v4, "BLUETOOTH_ON_AND_MORE"

    const/4 v5, 0x2

    const v6, 0x7f080435

    const/4 v7, 0x1

    const/4 v8, 0x1

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;-><init>(Ljava/lang/String;IIZZ)V

    sput-object v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->BLUETOOTH_ON_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const-string v4, "SPEAKER_ON_AND_MORE"

    const/4 v5, 0x3

    const v6, 0x7f080434

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;-><init>(Ljava/lang/String;IIZZ)V

    sput-object v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_ON_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const-string v4, "SPEAKER_OFF_AND_MORE"

    const/4 v5, 0x4

    const v6, 0x7f080436

    const/4 v7, 0x0

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;-><init>(Ljava/lang/String;IIZZ)V

    sput-object v1, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_OFF_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->$VALUES:[Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZZ)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->audioOutputIconRes:I

    iput-boolean p4, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->isButtonActive:Z

    iput-boolean p5, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->showMoreOptions:Z

    return-void
.end method

.method public static synthetic getBackgroundTint$default(Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Landroid/content/Context;ZILjava/lang/Object;)I
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getBackgroundTint(Landroid/content/Context;Z)I

    move-result p0

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;
    .locals 1

    const-class v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->$VALUES:[Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    invoke-virtual {v0}, [Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    return-object v0
.end method


# virtual methods
.method public final getAudioOutputIconRes()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->audioOutputIconRes:I

    return v0
.end method

.method public final getBackgroundTint(Landroid/content/Context;Z)I
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->isButtonActive:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    const p2, 0x7f060292

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_0

    :cond_0
    const p2, 0x7f040156

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    const p2, 0x7f0602b5

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_0

    :cond_2
    const p2, 0x7f060191

    invoke-static {p1, p2}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    :goto_0
    return p1
.end method

.method public final getIconTint(Landroid/content/Context;)I
    .locals 3
    .annotation build Landroidx/annotation/ColorRes;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->isButtonActive:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    const v1, 0x7f0400b1

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget p1, v0, Landroid/util/TypedValue;->resourceId:I

    goto :goto_0

    :cond_0
    const p1, 0x7f060292

    :goto_0
    return p1
.end method

.method public final getShowMoreOptions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->showMoreOptions:Z

    return v0
.end method

.method public final isButtonActive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->isButtonActive:Z

    return v0
.end method
