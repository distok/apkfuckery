.class public final Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;
.super Landroid/widget/LinearLayout;
.source "FloatingVoiceControlsView2.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final actionsCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioOutputContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioOutputSelector$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioOutputSelectorMore$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioOutputSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final deafenSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final disconnect$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final inviteSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private isGestureNavigationEnabled:Z

.field private final mute$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final peekContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final pushToTalkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final screenShareSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final screenshare$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stopWatching$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final streamVolumeLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final streamVolumeSlider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final video$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x11

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v3, "peekContainer"

    const-string v4, "getPeekContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "video"

    const-string v7, "getVideo()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "audioOutputContainer"

    const-string v7, "getAudioOutputContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "audioOutputSelector"

    const-string v7, "getAudioOutputSelector()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "audioOutputSelectorMore"

    const-string v7, "getAudioOutputSelectorMore()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "screenshare"

    const-string v7, "getScreenshare()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "mute"

    const-string v7, "getMute()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "disconnect"

    const-string v7, "getDisconnect()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "stopWatching"

    const-string v7, "getStopWatching()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "pushToTalkButton"

    const-string v7, "getPushToTalkButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "streamVolumeLabel"

    const-string v7, "getStreamVolumeLabel()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "streamVolumeSlider"

    const-string v7, "getStreamVolumeSlider()Lcom/discord/views/calls/VolumeSliderView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "actionsCard"

    const-string v7, "getActionsCard()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "audioOutputSheetButton"

    const-string v7, "getAudioOutputSheetButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "inviteSheetButton"

    const-string v7, "getInviteSheetButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "screenShareSheetButton"

    const-string v7, "getScreenShareSheetButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    const-string v6, "deafenSheetButton"

    const-string v7, "getDeafenSheetButton()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p1, 0x7f0a0458

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->peekContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a045f

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->video$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0452

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a044f

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputSelector$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0450

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputSelectorMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a045b

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->screenshare$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0457

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->mute$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0454

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->disconnect$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a045c

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->stopWatching$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0459

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->pushToTalkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a045d

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->streamVolumeLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a045e

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->streamVolumeSlider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a044e

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->actionsCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0451

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0456

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->inviteSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a045a

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->screenShareSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0453

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->deafenSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string p2, "resources"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/display/DisplayUtils;->isGestureNavigationEnabled(Landroid/content/res/Resources;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->isGestureNavigationEnabled:Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d005b

    invoke-static {p1, p2, p0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getActionsCard()Landroid/view/View;

    move-result-object p1

    iget-boolean p2, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->isGestureNavigationEnabled:Z

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    const/4 p2, 0x4

    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final configureMuteButton(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/model/CallModel;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->isMeMutedByAnySource()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0602b5

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getMute()Landroid/widget/ImageView;

    move-result-object v1

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getMute()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz p1, :cond_1

    const v1, 0x7f08037c

    goto :goto_1

    :cond_1
    const v1, 0x7f080387

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getMute()Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureMuteButton$1;

    invoke-direct {v1, p2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureMuteButton$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getMute()Landroid/widget/ImageView;

    move-result-object p2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p1, :cond_2

    const p1, 0x7f1218c9

    goto :goto_2

    :cond_2
    const p1, 0x7f1210a9

    :goto_2
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final configureOutputSelectors(Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Lkotlin/jvm/functions/Function0;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputContainer()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getAudioOutputIconRes()I

    move-result v4

    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "context"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x1

    invoke-virtual {p1, v3, v5}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getBackgroundTint(Landroid/content/Context;Z)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSheetButton()Landroid/widget/TextView;

    move-result-object v0

    xor-int/2addr p3, v5

    if-eqz p3, :cond_1

    const/4 p3, 0x0

    goto :goto_1

    :cond_1
    const/16 p3, 0x8

    :goto_1
    invoke-virtual {v0, p3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSheetButton()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getAudioOutputIconRes()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xe

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setCompoundDrawableWithIntrinsicBounds$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p3}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getIconTint(Landroid/content/Context;)I

    move-result p3

    invoke-static {p0, p3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result p3

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSelectorMore()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSelectorMore()Landroid/widget/ImageView;

    move-result-object p3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getShowMoreOptions()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {p3, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureOutputSelectors$1;

    invoke-direct {p3, p2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureOutputSelectors$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getAudioOutputSheetButton()Landroid/widget/TextView;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureOutputSelectors$2;

    invoke-direct {p3, p2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureOutputSelectors$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureScreenshareButtons(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/model/CallModel;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result p1

    if-eqz p1, :cond_0

    const v0, 0x7f121754

    goto :goto_0

    :cond_0
    const v0, 0x7f121076

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenshare()Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz p3, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_2

    const v1, 0x7f08038b

    const v5, 0x7f08038b

    goto :goto_2

    :cond_2
    const v1, 0x7f08038a

    const v5, 0x7f08038a

    :goto_2
    const/4 v1, -0x1

    if-eqz p1, :cond_3

    const/high16 v4, -0x1000000

    goto :goto_3

    :cond_3
    const/4 v4, -0x1

    :goto_3
    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f0602b5

    invoke-static {p1, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    :goto_4
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenshare()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenshare()Landroid/widget/ImageView;

    move-result-object p1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenshare()Landroid/widget/ImageView;

    move-result-object p1

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenshare()Landroid/widget/ImageView;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureScreenshareButtons$1;

    invoke-direct {v1, p2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureScreenshareButtons$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenShareSheetButton()Landroid/widget/TextView;

    move-result-object p1

    xor-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_5

    goto :goto_5

    :cond_5
    const/16 v2, 0x8

    :goto_5
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenShareSheetButton()Landroid/widget/TextView;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureScreenshareButtons$2;

    invoke-direct {p3, p2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureScreenshareButtons$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenShareSheetButton()Landroid/widget/TextView;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xe

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setCompoundDrawableWithIntrinsicBounds$default(Landroid/widget/TextView;IIIIILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getScreenShareSheetButton()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private final configureStreamVolume(ZFLkotlin/jvm/functions/Function2;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZF",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Float;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getStreamVolumeLabel()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getStreamVolumeSlider()Lcom/discord/views/calls/VolumeSliderView;

    move-result-object v0

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getStreamVolumeSlider()Lcom/discord/views/calls/VolumeSliderView;

    move-result-object p1

    invoke-static {p2}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/calls/VolumeSliderView;->a(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getStreamVolumeSlider()Lcom/discord/views/calls/VolumeSliderView;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/discord/views/calls/VolumeSliderView;->setOnVolumeChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private final configureVideoButton(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/model/CallModel;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getVideoDevices()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v1

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getVideo()Landroid/widget/ImageView;

    move-result-object v3

    if-eqz p1, :cond_1

    if-eqz p3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    const/16 v2, 0x8

    :goto_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 p1, -0x1

    if-eqz v0, :cond_3

    const/high16 p3, -0x1000000

    goto :goto_3

    :cond_3
    const/4 p3, -0x1

    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getVideo()Landroid/widget/ImageView;

    move-result-object v1

    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    if-eqz v0, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p3, 0x7f0602b5

    invoke-static {p1, p3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    :goto_4
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getVideo()Landroid/widget/ImageView;

    move-result-object p3

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getVideo()Landroid/widget/ImageView;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureVideoButton$1;

    invoke-direct {p3, p2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureVideoButton$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final getActionsCard()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->actionsCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getAudioOutputContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getAudioOutputSelector()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputSelector$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getAudioOutputSelectorMore()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputSelectorMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getAudioOutputSheetButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->audioOutputSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDeafenSheetButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->deafenSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDisconnect()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->disconnect$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getInviteSheetButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->inviteSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMute()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->mute$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getPeekContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->peekContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPushToTalkButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->pushToTalkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getScreenShareSheetButton()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->screenShareSheetButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getScreenshare()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->screenshare$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getStopWatching()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->stopWatching$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStreamVolumeLabel()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->streamVolumeLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStreamVolumeSlider()Lcom/discord/views/calls/VolumeSliderView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->streamVolumeSlider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/calls/VolumeSliderView;

    return-object v0
.end method

.method private final getVideo()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->video$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final configureUI(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;ZZZZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ZFLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/model/CallModel;",
            "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;",
            "ZZZZZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;ZF",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Float;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    move-object/from16 v11, p15

    move-object/from16 v12, p18

    move-object/from16 v13, p19

    const-string v14, "model"

    invoke-static {v1, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "outputSelectorState"

    invoke-static {v2, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onStopWatchingClick"

    invoke-static {v5, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onDisconnectClick"

    invoke-static {v6, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onAudioOutputClick"

    invoke-static {v7, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onVideoClick"

    invoke-static {v8, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onMuteClick"

    invoke-static {v9, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onInviteClick"

    invoke-static {v10, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onScreenshareClick"

    invoke-static {v11, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onStreamVolumeChange"

    invoke-static {v12, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "onDeafenPressed"

    invoke-static {v13, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getStopWatching()Landroid/view/View;

    move-result-object v14

    const/16 v16, 0x8

    if-eqz p7, :cond_0

    const/4 v15, 0x0

    goto :goto_0

    :cond_0
    const/16 v15, 0x8

    :goto_0
    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getStopWatching()Landroid/view/View;

    move-result-object v14

    new-instance v15, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$1;

    invoke-direct {v15, v5}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v14, v15}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getDisconnect()Landroid/view/View;

    move-result-object v5

    if-eqz p8, :cond_1

    const/4 v14, 0x0

    goto :goto_1

    :cond_1
    const/16 v14, 0x8

    :goto_1
    invoke-virtual {v5, v14}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getDisconnect()Landroid/view/View;

    move-result-object v5

    new-instance v14, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$2;

    invoke-direct {v14, v6}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v5, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getPushToTalkButton()Landroid/view/View;

    move-result-object v5

    if-eqz p4, :cond_2

    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    const/16 v6, 0x8

    :goto_2
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getDeafenSheetButton()Landroid/widget/TextView;

    move-result-object v5

    if-eqz v3, :cond_3

    const v6, 0x7f1218c2

    goto :goto_3

    :cond_3
    const v6, 0x7f1205d7

    :goto_3
    invoke-static {v0, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getDeafenSheetButton()Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$3;

    invoke-direct {v6, v13}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getDeafenSheetButton()Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setActivated(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getInviteSheetButton()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/model/CallModel;->canInvite()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v15, 0x0

    goto :goto_4

    :cond_4
    const/16 v15, 0x8

    :goto_4
    invoke-virtual {v3, v15}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getInviteSheetButton()Landroid/widget/TextView;

    move-result-object v3

    new-instance v5, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$4;

    invoke-direct {v5, v10}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2$configureUI$4;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {v0, v1, v9}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->configureMuteButton(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;)V

    xor-int/lit8 v3, v4, 0x1

    invoke-direct {v0, v2, v7, v3}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->configureOutputSelectors(Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Lkotlin/jvm/functions/Function0;Z)V

    move/from16 v2, p3

    invoke-direct {v0, v1, v8, v2}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->configureVideoButton(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;Z)V

    invoke-direct {v0, v1, v11, v4}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->configureScreenshareButtons(Lcom/discord/widgets/voice/model/CallModel;Lkotlin/jvm/functions/Function0;Z)V

    move/from16 v1, p16

    move/from16 v2, p17

    invoke-direct {v0, v1, v2, v12}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->configureStreamVolume(ZFLkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final getPeekHeight()I
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getPeekContainer()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public final handleSheetState(I)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getActionsCard()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->isGestureNavigationEnabled:Z

    if-eqz p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final setOnPTTListener(Lcom/discord/utilities/press/OnPressListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getPushToTalkButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
