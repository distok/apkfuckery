.class public final Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;
.super Lcom/discord/tooltips/TooltipManager$Tooltip;
.source "ScreenShareButtonSparkleTooltip.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;

    invoke-direct {v0}, Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "CACHE_KEY_SCREEN_SHARE_BUTTON_SPARKLE_ACKNOWLEDGED"

    const-string v1, "SCREEN_SHARE_BUTTON_SPARKLE"

    invoke-direct {p0, v0, v1}, Lcom/discord/tooltips/TooltipManager$Tooltip;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
