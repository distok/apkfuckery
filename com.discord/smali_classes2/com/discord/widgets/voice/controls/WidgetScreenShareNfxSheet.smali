.class public final Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetScreenShareNfxSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;,
        Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_VOICE_BOTTOM_SHEET_PARAMS:Ljava/lang/String; = "ARG_VOICE_BOTTOM_SHEET_PARAMS"

.field public static final Companion:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;


# instance fields
.field private final cancelButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final shareScreenCta$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    const-string v3, "shareScreenCta"

    const-string v4, "getShareScreenCta()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    const-string v6, "cancelButton"

    const-string v7, "getCancelButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->Companion:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0871

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->shareScreenCta$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0870

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->cancelButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$maybeNavigateToVoiceBottomSheet(Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->maybeNavigateToVoiceBottomSheet()V

    return-void
.end method

.method private final getCancelButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->cancelButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getChannelId()J
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private final getShareScreenCta()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->shareScreenCta$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getVoiceBottomSheetParams()Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_VOICE_BOTTOM_SHEET_PARAMS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    return-object v0
.end method

.method private final maybeNavigateToVoiceBottomSheet()V
    .locals 7

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->getVoiceBottomSheetParams()Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->Companion:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->getChannelId()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->getForwardToFullscreenIfVideoActivated()Z

    move-result v5

    invoke-virtual {v0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->getFeatureContext()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    :cond_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0252

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    new-instance v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;-><init>(Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;)V

    invoke-static {p1, p2, p3, v0}, Lcom/discord/widgets/voice/stream/StreamNavigator;->handleActivityResult(IILandroid/content/Intent;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->maybeNavigateToVoiceBottomSheet()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->getShareScreenCta()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onViewCreated$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->getCancelButton()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onViewCreated$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
