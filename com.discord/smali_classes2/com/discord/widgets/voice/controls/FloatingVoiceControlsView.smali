.class public final Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;
.super Landroid/widget/FrameLayout;
.source "FloatingVoiceControlsView.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final audioOutputSelector$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioOutputSelectorMore$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final disconnect$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final mute$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private permissionProvider:Lcom/discord/app/AppPermissions$Requests;

.field private final stopWatching$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final video$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    const-string v3, "video"

    const-string v4, "getVideo()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    const-string v6, "audioOutputSelector"

    const-string v7, "getAudioOutputSelector()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    const-string v6, "audioOutputSelectorMore"

    const-string v7, "getAudioOutputSelectorMore()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    const-string v6, "mute"

    const-string v7, "getMute()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    const-string v6, "disconnect"

    const-string v7, "getDisconnect()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    const-string v6, "stopWatching"

    const-string v7, "getStopWatching()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p1, 0x7f0a07f6

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->video$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a07f1

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->audioOutputSelector$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a07f2

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->audioOutputSelectorMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a07f4

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->mute$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a07f3

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->disconnect$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a07f5

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->stopWatching$delegate:Lkotlin/properties/ReadOnlyProperty;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0d024b

    invoke-static {p1, p2, p0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final getAudioOutputSelector()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->audioOutputSelector$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getAudioOutputSelectorMore()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->audioOutputSelectorMore$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getDisconnect()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->disconnect$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMute()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->mute$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getStopWatching()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->stopWatching$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getVideo()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->video$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final configureUI(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/model/CallModel;",
            "Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;",
            "ZZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputSelectorState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onStopWatchingClick"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDisconnectClick"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAudioOutputClick"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onVideoClick"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onMuteClick"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getStopWatching()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    const/16 p4, 0x8

    :goto_0
    invoke-virtual {v0, p4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getStopWatching()Landroid/view/View;

    move-result-object p4

    new-instance v0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$1;

    invoke-direct {v0, p6}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getDisconnect()Landroid/view/View;

    move-result-object p4

    if-eqz p5, :cond_1

    const/4 p5, 0x0

    goto :goto_1

    :cond_1
    const/16 p5, 0x8

    :goto_1
    invoke-virtual {p4, p5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getDisconnect()Landroid/view/View;

    move-result-object p4

    new-instance p5, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$2;

    invoke-direct {p5, p7}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p4, p5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->isMeMutedByAnySource()Z

    move-result p4

    const p5, 0x7f040156

    const p6, 0x7f060191

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p7

    invoke-static {p7, p5}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p7

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p7

    invoke-static {p7, p6}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p7

    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getMute()Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {p7}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p7

    invoke-virtual {v0, p7}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getMute()Landroid/widget/ImageView;

    move-result-object p7

    if-eqz p4, :cond_3

    const v0, 0x7f08037c

    goto :goto_3

    :cond_3
    const v0, 0x7f080387

    :goto_3
    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getMute()Landroid/widget/ImageView;

    move-result-object p7

    new-instance v0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$3;

    invoke-direct {v0, p10}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getMute()Landroid/widget/ImageView;

    move-result-object p7

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p10

    if-eqz p4, :cond_4

    const p4, 0x7f1218c9

    goto :goto_4

    :cond_4
    const p4, 0x7f1210a9

    :goto_4
    invoke-virtual {p10, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p7, p4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object p4

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p7

    invoke-virtual {p2}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getAudioOutputIconRes()I

    move-result p10

    invoke-static {p7, p10}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p7

    invoke-virtual {p4, p7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object p4

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p7

    const-string p10, "context"

    invoke-static {p7, p10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v3, 0x0

    invoke-static {p2, p7, v2, v0, v3}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getBackgroundTint$default(Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Landroid/content/Context;ZILjava/lang/Object;)I

    move-result p7

    invoke-static {p7}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p7

    invoke-virtual {p4, p7}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-static {p4, p10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p4}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getIconTint(Landroid/content/Context;)I

    move-result p4

    invoke-static {p0, p4}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result p4

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object p7

    invoke-static {p7, p4}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getAudioOutputSelectorMore()Landroid/widget/ImageView;

    move-result-object p7

    invoke-static {p7, p4}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getAudioOutputSelectorMore()Landroid/widget/ImageView;

    move-result-object p4

    invoke-virtual {p2}, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->getShowMoreOptions()Z

    move-result p2

    if-eqz p2, :cond_5

    const/4 p2, 0x0

    goto :goto_5

    :cond_5
    const/16 p2, 0x8

    :goto_5
    invoke-virtual {p4, p2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getAudioOutputSelector()Landroid/widget/ImageView;

    move-result-object p2

    new-instance p4, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$4;

    invoke-direct {p4, p8}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$4;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p2, p4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object p2

    const/4 p4, 0x1

    if-eqz p2, :cond_6

    const/4 p2, 0x1

    goto :goto_6

    :cond_6
    const/4 p2, 0x0

    :goto_6
    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getVideoDevices()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, p4

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getVideo()Landroid/widget/ImageView;

    move-result-object p7

    if-eqz p1, :cond_7

    if-eqz p3, :cond_7

    goto :goto_7

    :cond_7
    const/4 p4, 0x0

    :goto_7
    if-eqz p4, :cond_8

    const/4 v1, 0x0

    :cond_8
    invoke-virtual {p7, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p2, :cond_9

    const/high16 p1, -0x1000000

    goto :goto_8

    :cond_9
    const/4 p1, -0x1

    :goto_8
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getVideo()Landroid/widget/ImageView;

    move-result-object p3

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    if-eqz p2, :cond_a

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p5}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_9

    :cond_a
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p6}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    :goto_9
    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getVideo()Landroid/widget/ImageView;

    move-result-object p3

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getVideo()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p3

    if-eqz p2, :cond_b

    const p2, 0x7f1203cf

    goto :goto_a

    :cond_b
    const p2, 0x7f1203d0

    :goto_a
    invoke-virtual {p3, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->getVideo()Landroid/widget/ImageView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$5;

    invoke-direct {p2, p9}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView$configureUI$5;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final getPermissionProvider()Lcom/discord/app/AppPermissions$Requests;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->permissionProvider:Lcom/discord/app/AppPermissions$Requests;

    return-object v0
.end method

.method public final setPermissionProvider(Lcom/discord/app/AppPermissions$Requests;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->permissionProvider:Lcom/discord/app/AppPermissions$Requests;

    return-void
.end method
