.class public final Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;
.super Lx/m/c/k;
.source "WidgetScreenShareNfxSheet.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/Intent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;->this$0:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;->invoke(Landroid/content/Intent;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/content/Intent;)V
    .locals 8

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/voice/VoiceEngineServiceController;->Companion:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;->getINSTANCE()Lcom/discord/utilities/voice/VoiceEngineServiceController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->startStream(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;->this$0:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    invoke-virtual {p1}, Lcom/discord/app/AppBottomSheet;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    const-class v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    invoke-static {v0}, Lx/m/c/u;->getOrCreateKotlinClass(Ljava/lang/Class;)Lx/q/b;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/app/AppActivity;->j(Lx/q/b;)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    iget-object p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;->this$0:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string p1, "requireContext()"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;->this$0:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$onActivityResult$1;->this$0:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;

    invoke-virtual {p1}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method
