.class public final Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;
.super Ljava/lang/Object;
.source "AnchoredVoiceControlsView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->configureScreenShareButtonSparkle(ZLcom/discord/app/AppComponent;Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $channelId:J

.field public final synthetic $featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

.field public final synthetic $forwardToFullscreenIfVideoActivated:Z

.field public final synthetic $fragmentManager:Landroidx/fragment/app/FragmentManager;

.field public final synthetic $onNavigateToScreenShareNfxSheet:Lkotlin/jvm/functions/Function0;

.field public final synthetic $onScreenSharePressed:Lkotlin/jvm/functions/Function0;

.field public final synthetic this$0:Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->this$0:Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$fragmentManager:Landroidx/fragment/app/FragmentManager;

    iput-wide p3, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$channelId:J

    iput-boolean p5, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$forwardToFullscreenIfVideoActivated:Z

    iput-object p6, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    iput-object p7, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$onNavigateToScreenShareNfxSheet:Lkotlin/jvm/functions/Function0;

    iput-object p8, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$onScreenSharePressed:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    iget-object p1, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->this$0:Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;

    invoke-static {p1}, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;->access$getTooltipManager$p(Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView;)Lcom/discord/tooltips/TooltipManager;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/ScreenShareButtonSparkleTooltip;

    invoke-virtual {p1, v0}, Lcom/discord/tooltips/TooltipManager;->a(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    sget-object p1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;->Companion:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;->canShow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$fragmentManager:Landroidx/fragment/app/FragmentManager;

    iget-wide v1, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$channelId:J

    new-instance v3, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    iget-boolean v4, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$forwardToFullscreenIfVideoActivated:Z

    iget-object v5, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    invoke-direct {v3, v4, v5}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;-><init>(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;JLcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$onNavigateToScreenShareNfxSheet:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/voice/controls/AnchoredVoiceControlsView$configureScreenShareButtonSparkle$1;->$onScreenSharePressed:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :goto_0
    return-void
.end method
