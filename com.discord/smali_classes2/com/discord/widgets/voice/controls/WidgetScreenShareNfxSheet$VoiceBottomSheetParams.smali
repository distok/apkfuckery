.class public final Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;
.super Ljava/lang/Object;
.source "WidgetScreenShareNfxSheet.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoiceBottomSheetParams"
.end annotation


# instance fields
.field private final featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

.field private final forwardToFullscreenIfVideoActivated:Z


# direct methods
.method public constructor <init>(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V
    .locals 1

    const-string v0, "featureContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    iput-object p2, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;ILjava/lang/Object;)Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->copy(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    return v0
.end method

.method public final component2()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    return-object v0
.end method

.method public final copy(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;
    .locals 1

    const-string v0, "featureContext"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;-><init>(ZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    iget-boolean v1, p1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    iget-object p1, p1, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFeatureContext()Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    return-object v0
.end method

.method public final getForwardToFullscreenIfVideoActivated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "VoiceBottomSheetParams(forwardToFullscreenIfVideoActivated="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->forwardToFullscreenIfVideoActivated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", featureContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;->featureContext:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
