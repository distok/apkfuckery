.class public final Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;
.super Lx/m/c/k;
.source "WidgetScreenShareNfxSheet.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;JLcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$VoiceBottomSheetParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/SharedPreferences$Editor;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;

    invoke-direct {v0}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;->INSTANCE:Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/controls/WidgetScreenShareNfxSheet$Companion$show$1$2;->invoke(Landroid/content/SharedPreferences$Editor;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    const-string v0, "editor"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "CACHE_KEY_SCREEN_SHARE_NFX_SHEET_SHOWN"

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    return-void
.end method
