.class public final Lcom/discord/widgets/voice/feedback/FeedbackView;
.super Landroid/widget/LinearLayout;
.source "FeedbackView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;,
        Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final happyRating$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final issuesAdapter:Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

.field private final issuesCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final issuesHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final issuesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final neutralRating$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final ratingSummaryPrompt$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final sadRating$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewToFeedbackRatingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/view/View;",
            "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v3, "ratingSummaryPrompt"

    const-string v4, "getRatingSummaryPrompt()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v6, "sadRating"

    const-string v7, "getSadRating()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v6, "neutralRating"

    const-string v7, "getNeutralRating()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v6, "happyRating"

    const-string v7, "getHappyRating()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v6, "issuesHeader"

    const-string v7, "getIssuesHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v6, "issuesCard"

    const-string v7, "getIssuesCard()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/FeedbackView;

    const-string v6, "issuesRecycler"

    const-string v7, "getIssuesRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p2, 0x7f0a041d

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->ratingSummaryPrompt$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a041e

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->sadRating$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a041c

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->neutralRating$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0418

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->happyRating$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0419

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a041a

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a041b

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0d0059

    invoke-static {p1, p2, p0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x3

    new-array p1, p1, [Lkotlin/Pair;

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getSadRating()Landroid/view/View;

    move-result-object p2

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackRating;->BAD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p2, 0x0

    aput-object v1, p1, p2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getNeutralRating()Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NEUTRAL:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, p1, v0

    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getHappyRating()Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->GOOD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v1, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, p1, v0

    invoke-static {p1}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->viewToFeedbackRatingMap:Ljava/util/Map;

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getIssuesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesAdapter:Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getIssuesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    return-void
.end method

.method private final getHappyRating()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->happyRating$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getIssuesCard()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getIssuesHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getIssuesRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getNeutralRating()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->neutralRating$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRatingSummaryPrompt()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->ratingSummaryPrompt$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSadRating()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->sadRating$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final updateView(Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ratingSummaryPromptText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedFeedbackRating"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSadRatingClick"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNeutralRatingClick"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onHappyRatingClick"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "issuesHeaderText"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedbackIssues"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onIssueClick"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getRatingSummaryPrompt()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getSadRating()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/voice/feedback/FeedbackView$updateView$1;

    invoke-direct {v0, p3}, Lcom/discord/widgets/voice/feedback/FeedbackView$updateView$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getNeutralRating()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/voice/feedback/FeedbackView$updateView$2;

    invoke-direct {p3, p4}, Lcom/discord/widgets/voice/feedback/FeedbackView$updateView$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getHappyRating()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/voice/feedback/FeedbackView$updateView$3;

    invoke-direct {p3, p5}, Lcom/discord/widgets/voice/feedback/FeedbackView$updateView$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->viewToFeedbackRatingMap:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    const/4 p4, 0x1

    const/4 p5, 0x0

    if-eqz p3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map$Entry;

    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    if-ne p2, v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 p4, 0x0

    :goto_1
    invoke-interface {p3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    invoke-virtual {p3, p4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    :cond_1
    invoke-interface {p7}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, p4

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getIssuesHeader()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getIssuesHeader()Landroid/widget/TextView;

    move-result-object p2

    const/16 p3, 0x8

    if-eqz p1, :cond_2

    const/4 p4, 0x0

    goto :goto_2

    :cond_2
    const/16 p4, 0x8

    :goto_2
    invoke-virtual {p2, p4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getIssuesCard()Landroid/view/View;

    move-result-object p2

    if-eqz p1, :cond_3

    const/4 p4, 0x0

    goto :goto_3

    :cond_3
    const/16 p4, 0x8

    :goto_3
    invoke-virtual {p2, p4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/FeedbackView;->getIssuesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    const/16 p5, 0x8

    :goto_4
    invoke-virtual {p2, p5}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesAdapter:Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

    invoke-virtual {p1, p8}, Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;->setOnIssueClick(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView;->issuesAdapter:Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

    invoke-virtual {p1, p7}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method
