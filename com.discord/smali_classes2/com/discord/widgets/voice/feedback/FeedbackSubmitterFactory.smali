.class public final Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;
.super Ljava/lang/Object;
.source "FeedbackSubmitterFactory.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;

    invoke-direct {v0}, Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;->INSTANCE:Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Lcom/discord/widgets/voice/feedback/PendingFeedback;)Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;
    .locals 3

    const-string v0, "pendingFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;

    check-cast p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-direct {v0, p1, v2, v1, v2}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;

    check-cast p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    invoke-direct {v0, p1, v2, v1, v2}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
