.class public final Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;
.super Lcom/discord/widgets/voice/feedback/PendingFeedback;
.source "PendingFeedback.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/PendingFeedback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallFeedback"
.end annotation


# instance fields
.field private final channelId:J

.field private final durationMs:Ljava/lang/Long;

.field private final feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field private final issueDetails:Ljava/lang/String;

.field private final mediaSessionId:Ljava/lang/String;

.field private final reasonCode:Ljava/lang/Integer;

.field private final reasonDescription:Ljava/lang/String;

.field private final rtcConnectionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "feedbackRating"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/feedback/PendingFeedback;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    iput-object p5, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    iput-object p7, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    iput-object p9, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 13

    move/from16 v0, p10

    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v6, v2

    goto :goto_0

    :cond_0
    move-object/from16 v6, p3

    :goto_0
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_1

    move-object v7, v2

    goto :goto_1

    :cond_1
    move-object/from16 v7, p4

    :goto_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    move-object v8, v2

    goto :goto_2

    :cond_2
    move-object/from16 v8, p5

    :goto_2
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-object v9, v1

    goto :goto_3

    :cond_3
    move-object/from16 v9, p6

    :goto_3
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_4

    move-object v10, v2

    goto :goto_4

    :cond_4
    move-object/from16 v10, p7

    :goto_4
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_5

    move-object v11, v2

    goto :goto_5

    :cond_5
    move-object/from16 v11, p8

    :goto_5
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    move-object v12, v2

    goto :goto_6

    :cond_6
    move-object/from16 v12, p9

    :goto_6
    move-object v3, p0

    move-wide v4, p1

    invoke-direct/range {v3 .. v12}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;-><init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    goto :goto_0

    :cond_0
    move-wide v2, p1

    :goto_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v4, p3

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    goto :goto_2

    :cond_2
    move-object v5, p4

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v6, p5

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p8

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p9

    :goto_7
    move-wide p1, v2

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->copy(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;
    .locals 11

    const-string v0, "feedbackRating"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    move-object v1, v0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;-><init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    return-wide v0
.end method

.method public final getDurationMs()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final getFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method

.method public final getIssueDetails()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getReasonCode()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getReasonDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getRtcConnectionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "CallFeedback(channelId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", rtcConnectionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->rtcConnectionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", durationMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->durationMs:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mediaSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", feedbackRating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", reasonCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonCode:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", reasonDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->reasonDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", issueDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->issueDetails:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
