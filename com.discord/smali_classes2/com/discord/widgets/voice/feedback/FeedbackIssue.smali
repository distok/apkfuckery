.class public final enum Lcom/discord/widgets/voice/feedback/FeedbackIssue;
.super Ljava/lang/Enum;
.source "FeedbackIssue.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
        ">;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum AUDIO_CUT_IN_AND_OUT:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum AUDIO_ECHOS:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum AUDIO_ROBOTIC:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum BACKGROUND_NOISE_TOO_LOUD:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum COULD_NOT_HEAR_AUDIO:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum HEADSET_OR_BLUETOOTH_ISSUE:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum NOBODY_COULD_HEAR_ME:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum SPEAKERPHONE_ISSUE:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_AUDIO_MISSING:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_AUDIO_POOR:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_BLACK:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_BLURRY:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_LAGGING:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_OUT_OF_SYNC:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum STREAM_REPORT_ENDED_STREAM_STOPPED_UNEXPECTEDLY:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field public static final enum VOLUME_TOO_LOW_OR_HIGH:Lcom/discord/widgets/voice/feedback/FeedbackIssue;


# instance fields
.field private final reasonStringRes:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "OTHER"

    const/4 v3, 0x0

    const v4, 0x7f1203be

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "COULD_NOT_HEAR_AUDIO"

    const/4 v3, 0x1

    const v4, 0x7f1203bb

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->COULD_NOT_HEAR_AUDIO:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "NOBODY_COULD_HEAR_ME"

    const/4 v3, 0x2

    const v4, 0x7f1203bd

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->NOBODY_COULD_HEAR_ME:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "AUDIO_ECHOS"

    const/4 v3, 0x3

    const v4, 0x7f1203b7

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->AUDIO_ECHOS:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "AUDIO_ROBOTIC"

    const/4 v3, 0x4

    const v4, 0x7f1203b8

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->AUDIO_ROBOTIC:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "AUDIO_CUT_IN_AND_OUT"

    const/4 v3, 0x5

    const v4, 0x7f1203b6

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->AUDIO_CUT_IN_AND_OUT:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "VOLUME_TOO_LOW_OR_HIGH"

    const/4 v3, 0x6

    const v4, 0x7f1203ba

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->VOLUME_TOO_LOW_OR_HIGH:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "BACKGROUND_NOISE_TOO_LOUD"

    const/4 v3, 0x7

    const v4, 0x7f1203b9

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->BACKGROUND_NOISE_TOO_LOUD:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "SPEAKERPHONE_ISSUE"

    const/16 v3, 0x8

    const v4, 0x7f1203bf

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->SPEAKERPHONE_ISSUE:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "HEADSET_OR_BLUETOOTH_ISSUE"

    const/16 v3, 0x9

    const v4, 0x7f1203bc

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->HEADSET_OR_BLUETOOTH_ISSUE:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_BLACK"

    const/16 v3, 0xa

    const v4, 0x7f12177e

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_BLACK:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_BLURRY"

    const/16 v3, 0xb

    const v4, 0x7f12177f

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_BLURRY:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_LAGGING"

    const/16 v3, 0xc

    const v4, 0x7f121780

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_LAGGING:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_OUT_OF_SYNC"

    const/16 v3, 0xd

    const v4, 0x7f121781

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_OUT_OF_SYNC:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_AUDIO_MISSING"

    const/16 v3, 0xe

    const v4, 0x7f12177c

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_AUDIO_MISSING:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_AUDIO_POOR"

    const/16 v3, 0xf

    const v4, 0x7f12177d

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_AUDIO_POOR:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const-string v2, "STREAM_REPORT_ENDED_STREAM_STOPPED_UNEXPECTEDLY"

    const/16 v3, 0x10

    const v4, 0x7f121782

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_STREAM_STOPPED_UNEXPECTEDLY:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->$VALUES:[Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->reasonStringRes:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/voice/feedback/FeedbackIssue;
    .locals 1

    const-class v0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/voice/feedback/FeedbackIssue;
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->$VALUES:[Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-virtual {v0}, [Lcom/discord/widgets/voice/feedback/FeedbackIssue;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    return-object v0
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Enum;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getReasonStringRes()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->reasonStringRes:I

    return v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
