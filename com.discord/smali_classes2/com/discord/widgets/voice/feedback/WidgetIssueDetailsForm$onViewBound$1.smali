.class public final Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$1;
.super Ljava/lang/Object;
.source "WidgetIssueDetailsForm.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$1;->this$0:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$1;->this$0:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    invoke-static {p1}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->access$getIssueDetailsInput$p(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$1;->this$0:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    invoke-static {v0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->access$getViewModel$p(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->submitForm(Ljava/lang/String;)V

    return-void
.end method
