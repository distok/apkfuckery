.class public final Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;
.super Lf/a/b/l0;
.source "IssueDetailsFormViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event;,
        Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final feedbackSubmitter:Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;

.field private submitted:Z


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;)V
    .locals 1

    const-string v0, "feedbackSubmitter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->feedbackSubmitter:Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    const-string v0, "PublishSubject.create()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-void
.end method


# virtual methods
.method public final observeEvents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public onCleared()V
    .locals 1

    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->submitForm(Ljava/lang/String;)V

    return-void
.end method

.method public final submitForm(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->submitted:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->submitted:Z

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->feedbackSubmitter:Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;

    invoke-interface {v0, p1}, Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;->submit(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event$Close;->INSTANCE:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event$Close;

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method
