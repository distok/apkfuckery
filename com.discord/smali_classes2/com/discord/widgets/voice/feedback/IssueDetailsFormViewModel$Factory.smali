.class public final Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Factory;
.super Ljava/lang/Object;
.source "IssueDetailsFormViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/PendingFeedback;)V
    .locals 1

    const-string v0, "pendingFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Factory;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;->INSTANCE:Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Factory;->pendingFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/voice/feedback/FeedbackSubmitterFactory;->create(Lcom/discord/widgets/voice/feedback/PendingFeedback;)Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;)V

    return-object p1
.end method
