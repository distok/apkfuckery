.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;
.super Lx/m/c/k;
.source "StreamFeedbackSheetNavigator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->enqueueNotice(Ljava/lang/String;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/fragment/app/FragmentActivity;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $mediaSessionId:Ljava/lang/String;

.field public final synthetic $noticeName:Ljava/lang/String;

.field public final synthetic $requestedShowTimestamp:J

.field public final synthetic $storeNotices:Lcom/discord/stores/StoreNotices;

.field public final synthetic $streamKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Lcom/discord/stores/StoreNotices;Ljava/lang/String;)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$requestedShowTimestamp:J

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$streamKey:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$mediaSessionId:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$storeNotices:Lcom/discord/stores/StoreNotices;

    iput-object p6, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$noticeName:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->invoke(Landroidx/fragment/app/FragmentActivity;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Landroidx/fragment/app/FragmentActivity;)Z
    .locals 9

    const-string v0, "fragmentActivity"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->INSTANCE:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;

    invoke-static {v0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->access$getClock$p(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;)Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$requestedShowTimestamp:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xbb8

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v1, "fragmentActivity.supportFragmentManager"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$streamKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$mediaSessionId:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->access$show(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$storeNotices:Lcom/discord/stores/StoreNotices;

    iget-object v4, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;->$noticeName:Ljava/lang/String;

    const-wide/16 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/stores/StoreNotices;->markSeen$default(Lcom/discord/stores/StoreNotices;Ljava/lang/String;JILjava/lang/Object;)V

    const/4 p1, 0x1

    return p1
.end method
