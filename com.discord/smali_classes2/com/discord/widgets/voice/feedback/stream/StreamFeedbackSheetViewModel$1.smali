.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$1;
.super Lx/m/c/k;
.source "StreamFeedbackSheetViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;Ljava/lang/String;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$1;->this$0:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$1;->invoke(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;)V
    .locals 1

    const-string v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$1;->this$0:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;

    invoke-static {v0, p1}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->access$setMostRecentStoreState$p(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;)V

    return-void
.end method
