.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;
.super Ljava/lang/Object;
.source "StreamFeedbackSheetNavigator.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;

.field private static final clock:Lcom/discord/utilities/time/Clock;

.field private static final random:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;

    invoke-direct {v0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->INSTANCE:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->random:Ljava/util/Random;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->clock:Lcom/discord/utilities/time/Clock;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;)Lcom/discord/utilities/time/Clock;
    .locals 0

    sget-object p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$show(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final getNoticeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "Stream feedback notice for streamKey: "

    invoke-static {v0, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->Companion:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;

    sget-object v1, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;->STREAM:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;

    invoke-virtual {v0, v1}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;->newInstance(Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;)Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string v2, "ARG_STREAM_FEEDBACK_STREAM_KEY "

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "ARG_STREAM_FEEDBACK_MEDIA_SESSION_ID"

    invoke-virtual {v1, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-class p2, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/discord/app/AppBottomSheet;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final enqueueNotice(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 24

    move/from16 v0, p3

    const-string v1, "streamKey"

    move-object/from16 v5, p1

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    sget-object v1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->random:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->getNoticeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v10

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    const-wide/16 v19, 0x0

    new-instance v21, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;

    move-object/from16 v2, v21

    move-wide v3, v10

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object v7, v0

    move-object v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator$enqueueNotice$showStreamFeedbackSheetNotice$1;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/discord/stores/StoreNotices;Ljava/lang/String;)V

    const/16 v22, 0x32

    const/16 v23, 0x0

    move-object v1, v15

    move-object v2, v9

    move-object v3, v12

    move-wide v4, v10

    move v6, v13

    move v7, v14

    move-object/from16 v8, v16

    move-wide/from16 v9, v17

    move-wide/from16 v11, v19

    move-object/from16 v13, v21

    move/from16 v14, v22

    move-object/from16 p3, v0

    move-object v0, v15

    move-object/from16 v15, v23

    invoke-direct/range {v1 .. v15}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v1, p3

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    :cond_1
    :goto_0
    return-void
.end method
