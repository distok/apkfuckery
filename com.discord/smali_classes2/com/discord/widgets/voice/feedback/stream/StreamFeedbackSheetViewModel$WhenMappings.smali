.class public final synthetic Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/discord/widgets/voice/feedback/FeedbackRating;->values()[Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const/4 v0, 0x4

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const/4 v2, 0x3

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v4, Lcom/discord/widgets/voice/feedback/FeedbackRating;->GOOD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const/4 v4, 0x0

    const/4 v5, 0x2

    aput v5, v1, v4

    sget-object v4, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NEUTRAL:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    aput v2, v1, v3

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->BAD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    aput v0, v1, v5

    invoke-static {}, Lcom/discord/widgets/voice/feedback/FeedbackRating;->values()[Lcom/discord/widgets/voice/feedback/FeedbackRating;

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$WhenMappings;->$EnumSwitchMapping$1:[I

    aput v3, v0, v3

    aput v5, v0, v5

    return-void
.end method
