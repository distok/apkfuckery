.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;
.super Ljava/lang/Object;
.source "StreamFeedbackSubmitter.kt"

# interfaces
.implements Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;


# instance fields
.field private final analyticsStore:Lcom/discord/stores/StoreAnalytics;

.field private pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/stores/StoreAnalytics;)V
    .locals 1

    const-string v0, "pendingStreamFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/stores/StoreAnalytics;)V

    return-void
.end method


# virtual methods
.method public final getPendingStreamFeedback()Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    return-object v0
.end method

.method public final setPendingStreamFeedback(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    return-void
.end method

.method public submit(Ljava/lang/String;)V
    .locals 8

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v5, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSubmitter;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->trackStreamReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;)V

    return-void
.end method
