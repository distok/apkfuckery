.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;
.super Lf/a/b/l0;
.source "StreamFeedbackSheetViewModel.kt"

# interfaces
.implements Lcom/discord/widgets/feedback/FeedbackSheetViewModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;,
        Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;,
        Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;",
        ">;",
        "Lcom/discord/widgets/feedback/FeedbackSheetViewModel;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;

.field private static final ISSUES_UI_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final issuesUiOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaSessionId:Ljava/lang/String;

.field private mostRecentStoreState:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;

.field private pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private final stream:Lcom/discord/models/domain/ModelApplicationStream;

.field private final streamKey:Ljava/lang/String;

.field private submitOnDismiss:Z

.field private submitted:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->Companion:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_BLACK:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_BLURRY:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_LAGGING:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_OUT_OF_SYNC:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_AUDIO_MISSING:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_AUDIO_POOR:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->STREAM_REPORT_ENDED_STREAM_STOPPED_UNEXPECTEDLY:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->ISSUES_UI_OPTIONS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;Ljava/lang/String;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreAnalytics;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "storeAnalytics"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "streamKey"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeStateObservable"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    sget-object v3, Lx/h/l;->d:Lx/h/l;

    const v4, 0x7f121777

    const v5, 0x7f12178d

    const v6, 0x7f121787

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->streamKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->mediaSessionId:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->storeStateObservable:Lrx/Observable;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitOnDismiss:Z

    sget-object p1, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {p1, p2}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    new-instance p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x16

    const/4 v7, 0x0

    move-object v0, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;-><init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    sget-object p1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->ISSUES_UI_OPTIONS:Ljava/util/List;

    const-string p2, "$this$shuffled"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lx/h/f;->toMutableList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    sget-object p2, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-static {p1, p2}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->issuesUiOptions:Ljava/util/List;

    const/4 p1, 0x0

    const/4 p2, 0x2

    invoke-static {p4, p0, p1, p2, p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;

    new-instance v6, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$1;-><init>(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;)V

    const/4 v4, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getISSUES_UI_OPTIONS$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->ISSUES_UI_OPTIONS:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getMostRecentStoreState$p(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;)Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;

    return-object p0
.end method

.method public static final synthetic access$setMostRecentStoreState$p(Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;

    return-void
.end method

.method private final createViewState(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;)",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;"
        }
    .end annotation

    new-instance v6, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    const v3, 0x7f121777

    const v4, 0x7f12178d

    const v5, 0x7f121787

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)V

    return-object v6
.end method

.method private final emitSubmittedEvent(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;

    invoke-direct {v1, p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;-><init>(Z)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCleared()V
    .locals 1

    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    iget-boolean v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitOnDismiss:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitForm()V

    :cond_0
    return-void
.end method

.method public selectIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V
    .locals 8

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1b

    const/4 v7, 0x0

    move-object v3, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitOnDismiss:Z

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;->getShouldShowCxLinkForIssueDetails()Z

    move-result p1

    :cond_0
    invoke-direct {v1, p2, p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getSelectedFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 p2, 0x2

    if-eq p1, p2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitForm()V

    :goto_0
    return-void
.end method

.method public selectRating(Lcom/discord/widgets/voice/feedback/FeedbackRating;)V
    .locals 9

    const-string v0, "feedbackRating"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v3, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->issuesUiOptions:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->createViewState(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->selectIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->createViewState(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitForm()V

    :goto_0
    return-void
.end method

.method public submitForm()V
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitted:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->submitted:Z

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    invoke-virtual {v1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getSelectedFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    iget-object v3, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->pendingStreamFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    invoke-virtual {v2, v3}, Lcom/discord/stores/StoreAnalytics;->trackStreamReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;)V

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->emitSubmittedEvent(Z)V

    return-void
.end method
