.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;
.super Ljava/lang/Object;
.source "StreamFeedbackSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getISSUES_UI_OPTIONS()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;->access$getISSUES_UI_OPTIONS$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
