.class public final Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "StreamFeedbackSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final mediaSessionId:Ljava/lang/String;

.field private final storeExperiments:Lcom/discord/stores/StoreExperiments;

.field private final streamKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/stores/StoreExperiments;)V
    .locals 1

    const-string v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeExperiments"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->streamKey:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->mediaSessionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->storeExperiments:Lcom/discord/stores/StoreExperiments;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/stores/StoreExperiments;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/discord/stores/StoreExperiments;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->storeExperiments:Lcom/discord/stores/StoreExperiments;

    const-string v1, "2020-08_feedback_modal_helpdesk_link"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory$observeStoreState$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "storeExperiments\n       \u2026            )\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->streamKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->mediaSessionId:Ljava/lang/String;

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v2

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v3

    invoke-direct {p1, v2, v0, v1, v3}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetViewModel;-><init>(Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;Ljava/lang/String;Lrx/Observable;)V

    return-object p1
.end method
