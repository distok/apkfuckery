.class public final Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;
.super Lcom/discord/widgets/voice/feedback/PendingFeedback;
.source "PendingFeedback.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/PendingFeedback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StreamFeedback"
.end annotation


# instance fields
.field private final feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field private final issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

.field private final issueDetails:Ljava/lang/String;

.field private final mediaSessionId:Ljava/lang/String;

.field private final stream:Lcom/discord/models/domain/ModelApplicationStream;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedbackRating"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/feedback/PendingFeedback;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    iput-object p4, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_0

    sget-object p2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    :cond_0
    move-object v2, p2

    and-int/lit8 p2, p6, 0x4

    const/4 p7, 0x0

    if-eqz p2, :cond_1

    move-object v3, p7

    goto :goto_0

    :cond_1
    move-object v3, p3

    :goto_0
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_2

    move-object v4, p7

    goto :goto_1

    :cond_2
    move-object v4, p4

    :goto_1
    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_3

    move-object v5, p7

    goto :goto_2

    :cond_3
    move-object v5, p5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;-><init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->copy(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method

.method public final component3()Lcom/discord/widgets/voice/feedback/FeedbackIssue;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;
    .locals 7

    const-string v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feedbackRating"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;-><init>(Lcom/discord/models/domain/ModelApplicationStream;Lcom/discord/widgets/voice/feedback/FeedbackRating;Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method

.method public final getIssue()Lcom/discord/widgets/voice/feedback/FeedbackIssue;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    return-object v0
.end method

.method public final getIssueDetails()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getStream()Lcom/discord/models/domain/ModelApplicationStream;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StreamFeedback(stream="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->stream:Lcom/discord/models/domain/ModelApplicationStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", feedbackRating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->feedbackRating:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", issue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issue:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mediaSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", issueDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/PendingFeedback$StreamFeedback;->issueDetails:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
