.class public final enum Lcom/discord/widgets/voice/feedback/FeedbackRating;
.super Ljava/lang/Enum;
.source "FeedbackRating.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field public static final enum BAD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field public static final enum GOOD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field public static final enum NEUTRAL:Lcom/discord/widgets/voice/feedback/FeedbackRating;

.field public static final enum NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;


# instance fields
.field private final analyticsValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/discord/widgets/voice/feedback/FeedbackRating;

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const-string v2, "GOOD"

    const/4 v3, 0x0

    const-string v4, "good"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackRating;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;->GOOD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const-string v2, "NEUTRAL"

    const/4 v3, 0x1

    const-string v4, "neutral"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackRating;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NEUTRAL:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const-string v2, "BAD"

    const/4 v3, 0x2

    const-string v4, "bad"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackRating;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;->BAD:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    const-string v2, "NO_RESPONSE"

    const/4 v3, 0x3

    const-string v4, "no response"

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/voice/feedback/FeedbackRating;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/voice/feedback/FeedbackRating;->$VALUES:[Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/FeedbackRating;->analyticsValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    const-class v0, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/voice/feedback/FeedbackRating;
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackRating;->$VALUES:[Lcom/discord/widgets/voice/feedback/FeedbackRating;

    invoke-virtual {v0}, [Lcom/discord/widgets/voice/feedback/FeedbackRating;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/voice/feedback/FeedbackRating;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/FeedbackRating;->analyticsValue:Ljava/lang/String;

    return-object v0
.end method
