.class public final Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;
.super Lcom/discord/app/AppFragment;
.source "WidgetIssueDetailsForm.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final ARG_PENDING_VOICE_FEEDBACK:Ljava/lang/String; = "ARG_PENDING_VOICE_FEEDBACK"

.field public static final ARG_SHOW_CX_PROMPT:Ljava/lang/String; = "ARG_SHOW_CX_PROMPT"

.field public static final Companion:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;


# instance fields
.field private final cxPrompt$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final issueDetailsInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final submitButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    const-string v3, "issueDetailsInput"

    const-string v4, "getIssueDetailsInput()Lcom/google/android/material/textfield/TextInputLayout;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    const-string v6, "cxPrompt"

    const-string v7, "getCxPrompt()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    const-string v6, "submitButton"

    const-string v7, "getSubmitButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->Companion:Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a05e4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->issueDetailsInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a05e3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->cxPrompt$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a05e5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->submitButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getIssueDetailsInput$p(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getIssueDetailsInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSubmitButton$p(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getSubmitButton()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->viewModel:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->handleEvent(Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->viewModel:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    return-void
.end method

.method private final getCxPrompt()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->cxPrompt$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getIssueDetailsInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->issueDetailsInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getPendingFeedback()Lcom/discord/widgets/voice/feedback/PendingFeedback;
    .locals 2

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ARG_PENDING_VOICE_FEEDBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.widgets.voice.feedback.PendingFeedback"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/voice/feedback/PendingFeedback;

    return-object v0
.end method

.method private final getSubmitButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->submitButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event;)V
    .locals 2

    sget-object v0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event$Close;->INSTANCE:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Event$Close;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const p1, 0x7f1203b4

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getIssueDetailsInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/app/AppActivity;->hideKeyboard(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_1
    return-void
.end method

.method private final shouldShowCxPrompt()Z
    .locals 3

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ARG_SHOW_CX_PROMPT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0230

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Factory;

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getPendingFeedback()Lcom/discord/widgets/voice/feedback/PendingFeedback;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel$Factory;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback;)V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026ormViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->viewModel:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 8

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const p1, 0x7f120743

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getSubmitButton()Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getSubmitButton()Landroid/view/View;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$1;-><init>(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getIssueDetailsInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBound$2;-><init>(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)V

    invoke-static {p1, p0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getIssueDetailsInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->showKeyboard(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getCxPrompt()Landroid/widget/TextView;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->shouldShowCxPrompt()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->getCxPrompt()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f120744

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Lf/a/b/g;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {p0, v2, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "getString(\n            R\u2026ubmitRequestURL\n        )"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;->viewModel:Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/feedback/IssueDetailsFormViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/voice/feedback/WidgetIssueDetailsForm;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
