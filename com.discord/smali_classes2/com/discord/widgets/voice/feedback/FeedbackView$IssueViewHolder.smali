.class public final Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "FeedbackView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/FeedbackView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IssueViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;",
        "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
        ">;"
    }
.end annotation


# instance fields
.field private final issueItem:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(ILcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string p2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;->issueItem:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;)Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/voice/feedback/FeedbackView$IssuesAdapter;

    return-object p0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/voice/feedback/FeedbackIssue;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;->issueItem:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->getReasonStringRes()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;->issueItem:Landroid/widget/TextView;

    new-instance v0, Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder$onConfigure$1;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;Lcom/discord/widgets/voice/feedback/FeedbackIssue;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/feedback/FeedbackView$IssueViewHolder;->onConfigure(ILcom/discord/widgets/voice/feedback/FeedbackIssue;)V

    return-void
.end method
