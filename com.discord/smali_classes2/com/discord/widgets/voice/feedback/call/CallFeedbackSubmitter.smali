.class public final Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;
.super Ljava/lang/Object;
.source "CallFeedbackSubmitter.kt"

# interfaces
.implements Lcom/discord/widgets/voice/feedback/FeedbackSubmitter;


# instance fields
.field private final analyticsStore:Lcom/discord/stores/StoreAnalytics;

.field private pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;Lcom/discord/stores/StoreAnalytics;)V
    .locals 1

    const-string v0, "pendingCallFeedback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsStore"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;Lcom/discord/stores/StoreAnalytics;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;Lcom/discord/stores/StoreAnalytics;)V

    return-void
.end method


# virtual methods
.method public final getPendingCallFeedback()Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    return-object v0
.end method

.method public final setPendingCallFeedback(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    return-void
.end method

.method public submit(Ljava/lang/String;)V
    .locals 12

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x7f

    const/4 v11, 0x0

    move-object v9, p1

    invoke-static/range {v0 .. v11}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSubmitter;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAnalytics;->trackCallReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V

    return-void
.end method
