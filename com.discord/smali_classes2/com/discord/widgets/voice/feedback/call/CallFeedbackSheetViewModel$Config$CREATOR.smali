.class public final Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;
.super Ljava/lang/Object;
.source "CallFeedbackSheetViewModel.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CREATOR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    invoke-direct {v0, p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;->createFromParcel(Landroid/os/Parcel;)Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;
    .locals 0

    new-array p1, p1, [Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;->newArray(I)[Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    move-result-object p1

    return-object p1
.end method
