.class public final Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;
.super Ljava/lang/Object;
.source "CallFeedbackSheetViewModel.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;


# instance fields
.field private final callDurationMs:Ljava/lang/Long;

.field private final channelId:J

.field private final mediaSessionId:Ljava/lang/String;

.field private final rtcConnectionId:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->CREATOR:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config$CREATOR;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    move-object v6, p1

    check-cast v6, Ljava/lang/Long;

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget-object p3, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    :cond_1
    move-object v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget-object p4, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    :cond_2
    move-object v4, p4

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    iget-object p5, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    :cond_3
    move-object v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->copy(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;
    .locals 7

    new-instance v6, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    move-object v0, v6

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-object v6
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCallDurationMs()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    return-wide v0
.end method

.method public final getMediaSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRtcConnectionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Config(channelId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", rtcConnectionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mediaSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", callDurationMs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->channelId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object p2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->rtcConnectionId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->callDurationMs:Ljava/lang/Long;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    return-void
.end method
