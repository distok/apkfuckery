.class public final Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "CallFeedbackSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final config:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

.field private final storeExperiments:Lcom/discord/stores/StoreExperiments;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;Lcom/discord/stores/StoreExperiments;)V
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeExperiments"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;->config:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;->storeExperiments:Lcom/discord/stores/StoreExperiments;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;Lcom/discord/stores/StoreExperiments;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;-><init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;Lcom/discord/stores/StoreExperiments;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;->storeExperiments:Lcom/discord/stores/StoreExperiments;

    const-string v1, "2020-08_feedback_modal_helpdesk_link"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory$observeStoreState$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "storeExperiments\n       \u2026            )\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;->config:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;-><init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;Lcom/discord/stores/StoreAnalytics;Lrx/Observable;)V

    return-object p1
.end method
