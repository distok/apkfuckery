.class public final Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;
.super Ljava/lang/Object;
.source "CallFeedbackSheetNavigator.kt"


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;

.field private static final clock:Lcom/discord/utilities/time/Clock;

.field private static final random:Ljava/util/Random;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;

    invoke-direct {v0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->INSTANCE:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->random:Ljava/util/Random;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->clock:Lcom/discord/utilities/time/Clock;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getClock$p(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;)Lcom/discord/utilities/time/Clock;
    .locals 0

    sget-object p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->clock:Lcom/discord/utilities/time/Clock;

    return-object p0
.end method

.method public static final synthetic access$show(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;Landroidx/fragment/app/FragmentManager;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->show(Landroidx/fragment/app/FragmentManager;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method private final getNoticeName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "Call feedback notice for rtcConnectionId: "

    invoke-static {v0, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final show(Landroidx/fragment/app/FragmentManager;JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 7

    new-instance v6, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    move-object v0, v6

    move-wide v1, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    sget-object p2, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;->Companion:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;

    sget-object p3, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;->CALL:Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;

    invoke-virtual {p2, p3}, Lcom/discord/widgets/feedback/WidgetFeedbackSheet$Companion;->newInstance(Lcom/discord/widgets/feedback/WidgetFeedbackSheet$FeedbackType;)Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    invoke-static {p3}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const-string p4, "ARG_CONFIG"

    invoke-virtual {p3, p4, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-class p3, Lcom/discord/widgets/feedback/WidgetFeedbackSheet;

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lcom/discord/app/AppBottomSheet;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final enqueueNotice(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;I)V
    .locals 27

    move/from16 v0, p6

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    sget-object v1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->random:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v9

    move-object/from16 v11, p0

    move-object/from16 v6, p3

    invoke-direct {v11, v6}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->getNoticeName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sget-object v1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v15

    new-instance v14, Lcom/discord/stores/StoreNotices$Notice;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const-wide/16 v21, 0x0

    const-wide/16 v23, 0x0

    new-instance v25, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator$enqueueNotice$showCallFeedbackSheetNotice$1;

    move-object/from16 v1, v25

    move-wide v2, v15

    move-wide/from16 v4, p1

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object v10, v13

    invoke-direct/range {v1 .. v10}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator$enqueueNotice$showCallFeedbackSheetNotice$1;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lcom/discord/stores/StoreNotices;Ljava/lang/String;)V

    const/16 v1, 0x32

    const/16 v26, 0x0

    move-object v12, v14

    move-object v2, v14

    move-object/from16 v14, v17

    move/from16 v17, v18

    move/from16 v18, v19

    move-object/from16 v19, v20

    move-wide/from16 v20, v21

    move-wide/from16 v22, v23

    move-object/from16 v24, v25

    move/from16 v25, v1

    invoke-direct/range {v12 .. v26}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    return-void

    :cond_1
    :goto_0
    move-object/from16 v11, p0

    return-void
.end method
