.class public final Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;
.super Lf/a/b/l0;
.source "CallFeedbackSheetViewModel.kt"

# interfaces
.implements Lcom/discord/widgets/feedback/FeedbackSheetViewModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;,
        Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;,
        Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Factory;,
        Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;",
        ">;",
        "Lcom/discord/widgets/feedback/FeedbackSheetViewModel;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Companion;

.field private static final ISSUES_UI_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation
.end field

.field private static final REASON_CODES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final config:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final issuesUiOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;"
        }
    .end annotation
.end field

.field private mostRecentStoreState:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;

.field private pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private submitOnDismiss:Z

.field private submitted:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 18

    new-instance v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->Companion:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Companion;

    const/16 v0, 0x9

    new-array v1, v0, [Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->COULD_NOT_HEAR_AUDIO:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v4, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->NOBODY_COULD_HEAR_ME:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v5, 0x1

    aput-object v4, v1, v5

    sget-object v6, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->AUDIO_ECHOS:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v7, 0x2

    aput-object v6, v1, v7

    sget-object v8, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->AUDIO_ROBOTIC:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v9, 0x3

    aput-object v8, v1, v9

    sget-object v10, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->AUDIO_CUT_IN_AND_OUT:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v11, 0x4

    aput-object v10, v1, v11

    sget-object v12, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->VOLUME_TOO_LOW_OR_HIGH:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v13, 0x5

    aput-object v12, v1, v13

    sget-object v14, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->BACKGROUND_NOISE_TOO_LOUD:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/4 v15, 0x6

    aput-object v14, v1, v15

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->SPEAKERPHONE_ISSUE:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/16 v16, 0x7

    aput-object v0, v1, v16

    sget-object v15, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->HEADSET_OR_BLUETOOTH_ISSUE:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    const/16 v17, 0x8

    aput-object v15, v1, v17

    invoke-static {v1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->ISSUES_UI_OPTIONS:Ljava/util/List;

    const/16 v1, 0xa

    new-array v13, v1, [Lkotlin/Pair;

    sget-object v1, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    new-instance v9, Lkotlin/Pair;

    invoke-direct {v9, v1, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v9, v13, v3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v2, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v13, v5

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v13, v7

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v6, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x3

    aput-object v1, v13, v2

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v8, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x4

    aput-object v1, v13, v2

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v10, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x5

    aput-object v1, v13, v2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v12, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x6

    aput-object v2, v13, v1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v14, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v13, v16

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v0, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v13, v17

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v15, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/16 v0, 0x9

    aput-object v1, v13, v0

    invoke-static {v13}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->REASON_CODES:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;Lcom/discord/stores/StoreAnalytics;Lrx/Observable;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;",
            "Lcom/discord/stores/StoreAnalytics;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "config"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "storeAnalytics"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "storeStateObservable"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    sget-object v6, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    sget-object v7, Lx/h/l;->d:Lx/h/l;

    const v8, 0x7f1203c4

    const v9, 0x7f1203c0

    const v10, 0x7f1203b5

    move-object v5, v4

    invoke-direct/range {v5 .. v10}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)V

    invoke-direct {v0, v4}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->config:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;

    iput-object v2, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    iput-object v3, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->storeStateObservable:Lrx/Observable;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v4

    iput-object v4, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitOnDismiss:Z

    new-instance v4, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->getChannelId()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->getRtcConnectionId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->getMediaSessionId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->getCallDurationMs()Ljava/lang/Long;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xf0

    const/16 v16, 0x0

    move-object v5, v4

    invoke-direct/range {v5 .. v16}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;-><init>(JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v4, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    sget-object v4, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->ISSUES_UI_OPTIONS:Ljava/util/List;

    const-string v5, "$this$shuffled"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4}, Lx/h/f;->toMutableList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    sget-object v5, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    invoke-static {v4, v5}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->issuesUiOptions:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$Config;->getChannelId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/discord/stores/StoreAnalytics;->trackShowCallFeedbackSheet(J)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v3, v0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;

    new-instance v9, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$1;

    invoke-direct {v9, v0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$1;-><init>(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getISSUES_UI_OPTIONS$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->ISSUES_UI_OPTIONS:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getMostRecentStoreState$p(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;)Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;

    return-object p0
.end method

.method public static final synthetic access$getREASON_CODES$cp()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->REASON_CODES:Ljava/util/Map;

    return-object v0
.end method

.method public static final synthetic access$setMostRecentStoreState$p(Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;

    return-void
.end method

.method private final createViewState(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/feedback/FeedbackRating;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/voice/feedback/FeedbackIssue;",
            ">;)",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;"
        }
    .end annotation

    new-instance v6, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    const v3, 0x7f1203c4

    const v4, 0x7f1203c0

    const v5, 0x7f1203b5

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;-><init>(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;III)V

    return-object v6
.end method

.method private final emitSubmittedEvent(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;

    invoke-direct {v1, p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$Submitted;-><init>(Z)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCleared()V
    .locals 1

    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    iget-boolean v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitOnDismiss:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitForm()V

    :cond_0
    return-void
.end method

.method public selectIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    sget-object v1, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->REASON_CODES:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/Integer;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x9f

    const/4 v11, 0x0

    move-object v8, p2

    invoke-static/range {v0 .. v11}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    sget-object v0, Lcom/discord/widgets/voice/feedback/FeedbackIssue;->OTHER:Lcom/discord/widgets/voice/feedback/FeedbackIssue;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitOnDismiss:Z

    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel$StoreState;->getShouldShowCxLinkForIssueDetails()Z

    move-result p1

    :cond_0
    invoke-direct {v1, p2, p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$Event$NavigateToIssueDetails;-><init>(Lcom/discord/widgets/voice/feedback/PendingFeedback;Z)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    invoke-virtual {p1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getSelectedFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 p2, 0x2

    if-eq p1, p2, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitForm()V

    :goto_0
    return-void
.end method

.method public selectRating(Lcom/discord/widgets/voice/feedback/FeedbackRating;)V
    .locals 13

    const-string v0, "feedbackRating"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xef

    const/4 v12, 0x0

    move-object v7, p1

    invoke-static/range {v1 .. v12}, Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;->copy$default(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;JLjava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->issuesUiOptions:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->createViewState(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->selectIssue(Lcom/discord/widgets/voice/feedback/FeedbackIssue;Ljava/lang/String;)V

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->createViewState(Lcom/discord/widgets/voice/feedback/FeedbackRating;Ljava/util/List;)Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitForm()V

    :goto_0
    return-void
.end method

.method public submitForm()V
    .locals 4

    iget-boolean v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitted:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->submitted:Z

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;

    invoke-virtual {v1}, Lcom/discord/widgets/feedback/FeedbackSheetViewModel$ViewState;->getSelectedFeedbackRating()Lcom/discord/widgets/voice/feedback/FeedbackRating;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    iget-object v3, p0, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->pendingCallFeedback:Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;

    invoke-virtual {v2, v3}, Lcom/discord/stores/StoreAnalytics;->trackCallReportProblem(Lcom/discord/widgets/voice/feedback/PendingFeedback$CallFeedback;)V

    sget-object v2, Lcom/discord/widgets/voice/feedback/FeedbackRating;->NO_RESPONSE:Lcom/discord/widgets/voice/feedback/FeedbackRating;

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetViewModel;->emitSubmittedEvent(Z)V

    return-void
.end method
