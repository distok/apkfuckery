.class public final Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;
.super Ljava/lang/Object;
.source "WidgetCallFullscreenViewModel.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->createUserItemsComparator(ZLjava/lang/String;)Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $mySpectatingStreamKey:Ljava/lang/String;

.field public final synthetic $prioritizeSpectators:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$prioritizeSpectators:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)I
    .locals 10

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v4

    :goto_1
    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v4

    :cond_2
    if-eqz v3, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-eqz v4, :cond_4

    const/4 v6, 0x1

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    :goto_3
    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v7

    if-ne v7, v2, :cond_5

    const/4 v7, 0x1

    goto :goto_4

    :cond_5
    const/4 v7, 0x0

    :goto_4
    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v8

    if-ne v8, v2, :cond_6

    const/4 v8, 0x1

    goto :goto_5

    :cond_6
    const/4 v8, 0x0

    :goto_5
    if-eqz v0, :cond_7

    iget-object v9, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    invoke-static {v3, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    goto :goto_6

    :cond_7
    const/4 v3, 0x0

    :goto_6
    if-eqz v0, :cond_8

    iget-object v9, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    invoke-static {v4, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v1, 0x1

    :cond_8
    iget-boolean v4, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$prioritizeSpectators:Z

    if-eqz v4, :cond_9

    if-eqz v3, :cond_9

    goto :goto_7

    :cond_9
    if-eqz v4, :cond_a

    if-eqz v1, :cond_a

    goto/16 :goto_8

    :cond_a
    if-eqz v4, :cond_b

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getWatchingStream()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getWatchingStream()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    invoke-static {v1, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_b

    goto :goto_7

    :cond_b
    iget-boolean v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$prioritizeSpectators:Z

    if-eqz v1, :cond_c

    if-eqz v0, :cond_c

    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getWatchingStream()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getWatchingStream()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->$mySpectatingStreamKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_c

    goto :goto_8

    :cond_c
    if-eqz v5, :cond_d

    if-nez v6, :cond_d

    goto :goto_7

    :cond_d
    if-nez v5, :cond_e

    if-eqz v6, :cond_e

    goto :goto_8

    :cond_e
    if-eqz v7, :cond_f

    if-nez v8, :cond_f

    :goto_7
    const/4 v2, -0x1

    goto :goto_8

    :cond_f
    if-nez v7, :cond_10

    if-eqz v8, :cond_10

    goto :goto_8

    :cond_10
    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getNickname()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getNickname()Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, v1, p1, p2}, Lcom/discord/models/domain/ModelUser;->compareUserNames(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    :goto_8
    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    check-cast p2, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;->compare(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)I

    move-result p1

    return p1
.end method
