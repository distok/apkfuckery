.class public final Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;
.super Lx/j/h/a/g;
.source "UserRepresentativeColors.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->handleBitmap(JLandroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.discord.widgets.voice.fullscreen.UserRepresentativeColors$handleBitmap$1"
    f = "UserRepresentativeColors.kt"
    l = {
        0x2c
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $bitmap:Landroid/graphics/Bitmap;

.field public final synthetic $userId:J

.field public L$0:Ljava/lang/Object;

.field public L$1:Ljava/lang/Object;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method public constructor <init>(JLandroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$userId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$bitmap:Landroid/graphics/Bitmap;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;

    iget-wide v1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$userId:J

    iget-object v3, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;-><init>(JLandroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;

    sget-object p2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    sget-object v1, Lx/j/g/a;->d:Lx/j/g/a;

    iget v2, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->L$1:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    sget-object v2, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    invoke-static {v2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->access$getRepresentativeColors$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)Ljava/util/HashMap;

    move-result-object v4

    iget-wide v5, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$userId:J

    new-instance v7, Ljava/lang/Long;

    invoke-direct {v7, v5, v6}, Ljava/lang/Long;-><init>(J)V

    invoke-interface {v4, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    return-object v0

    :cond_2
    iget-object v4, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$bitmap:Landroid/graphics/Bitmap;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    const-string v5, "copiedBitmap"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->L$0:Ljava/lang/Object;

    iput-object v4, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->L$1:Ljava/lang/Object;

    iput v3, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->label:I

    invoke-virtual {v2, v4, p0}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->getRepresentativeColorAsync(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_3

    return-object v1

    :cond_3
    :goto_0
    check-cast p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;

    instance-of v1, p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Success;

    if-eqz v1, :cond_4

    move-object v1, p1

    check-cast v1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Success;

    invoke-virtual {v1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Success;->getColor()I

    move-result v1

    goto :goto_1

    :cond_4
    instance-of v1, p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Failure;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    invoke-static {v1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->access$getBLURPLE$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)I

    move-result v1

    :goto_1
    instance-of v2, p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Failure;

    if-eqz v2, :cond_5

    sget-object v2, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Failure;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Failure;->getException()Ljava/lang/Exception;

    move-result-object p1

    iget-wide v3, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$userId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lkotlin/Pair;

    const-string v5, "userId"

    invoke-direct {v4, v5, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v4}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v3

    const-string v4, "Failed to get representative color for user"

    invoke-virtual {v2, v4, p1, v3}, Lcom/discord/app/AppLog;->e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V

    :cond_5
    sget-object p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    invoke-static {p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->access$getRepresentativeColors$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)Ljava/util/HashMap;

    move-result-object v2

    iget-wide v3, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;->$userId:J

    new-instance v5, Ljava/lang/Long;

    invoke-direct {v5, v3, v4}, Ljava/lang/Long;-><init>(J)V

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v2, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->access$getRepresentativeColorsSubject$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    invoke-static {p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->access$getRepresentativeColors$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)Ljava/util/HashMap;

    move-result-object p1

    invoke-virtual {v1, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-object v0

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
