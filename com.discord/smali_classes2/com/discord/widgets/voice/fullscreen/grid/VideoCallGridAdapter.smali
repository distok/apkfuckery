.class public final Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "VideoCallGridAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;"
        }
    .end annotation
.end field

.field private final onParticipantLongClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onParticipantTapped:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onWatchStreamClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final spanSizeLookup:Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onParticipantTapped"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onParticipantLongClicked"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onWatchStreamClicked"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onParticipantTapped:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onParticipantLongClicked:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onWatchStreamClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lx/h/l;->d:Lx/h/l;

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    new-instance p1, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;-><init>(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->spanSizeLookup:Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;

    return-void
.end method

.method public static final synthetic access$getData$p(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$setData$p(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    return-void
.end method

.method private final getDiffUtilCallback(Ljava/util/List;Ljava/util/List;)Landroidx/recyclerview/widget/DiffUtil$Callback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;)",
            "Landroidx/recyclerview/widget/DiffUtil$Callback;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v0, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object p1, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    iget-object p1, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public final getSpanSizeLookup()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->spanSizeLookup:Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onBindViewHolder(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;I)V
    .locals 3

    const-string v0, "holder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onParticipantTapped:Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onParticipantLongClicked:Lkotlin/jvm/functions/Function1;

    iget-object v2, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onWatchStreamClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;->bind(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;
    .locals 2

    const-string p2, "parent"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0d0113

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type com.discord.views.calls.VideoCallParticipantView"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/discord/views/calls/VideoCallParticipantView;

    new-instance p2, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;

    invoke-direct {p2, p1}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;-><init>(Lcom/discord/views/calls/VideoCallParticipantView;)V

    return-object p2
.end method

.method public final setData(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->getDiffUtilCallback(Ljava/util/List;Ljava/util/List;)Landroidx/recyclerview/widget/DiffUtil$Callback;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;Z)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    const-string v1, "DiffUtil.calculateDiff(g\u2026a, newData = data), true)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->data:Ljava/util/List;

    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method
