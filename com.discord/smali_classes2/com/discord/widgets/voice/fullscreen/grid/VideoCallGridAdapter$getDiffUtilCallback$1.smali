.class public final Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;
.super Landroidx/recyclerview/widget/DiffUtil$Callback;
.source "VideoCallGridAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->getDiffUtilCallback(Ljava/util/List;Ljava/util/List;)Landroidx/recyclerview/widget/DiffUtil$Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $newData:Ljava/util/List;

.field public final synthetic $oldData:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$oldData:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$newData:Ljava/util/List;

    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$oldData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$newData:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public areItemsTheSame(II)Z
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$oldData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$newData:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object p1, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->a:Ljava/lang/String;

    iget-object p2, p2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getNewListSize()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$newData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$getDiffUtilCallback$1;->$oldData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
