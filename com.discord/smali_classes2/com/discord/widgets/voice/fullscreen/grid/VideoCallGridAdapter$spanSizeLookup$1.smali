.class public final Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;
.super Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;
.source "VideoCallGridAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;->this$0:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    invoke-direct {p0}, Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpanSize(I)I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;->this$0:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    invoke-static {v0}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->access$getData$p(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;->this$0:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    invoke-static {v0}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->access$getData$p(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int/2addr v0, v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter$spanSizeLookup$1;->this$0:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    invoke-static {v0}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->access$getData$p(Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->getLastIndex(Ljava/util/List;)I

    move-result v0

    if-ne p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1
.end method
