.class public final Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;
.super Lcom/discord/widgets/voice/fullscreen/grid/ResizingGridLayoutManager;
.source "VideoCallGridLayoutManager.kt"


# instance fields
.field private final recyclerviewSizeProvider:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final spanCount:I


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function0;IILandroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/Integer;",
            ">;II",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "recyclerviewSizeProvider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p4, p2, p3}, Lcom/discord/widgets/voice/fullscreen/grid/ResizingGridLayoutManager;-><init>(Landroid/content/Context;II)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;->recyclerviewSizeProvider:Lkotlin/jvm/functions/Function0;

    iput p2, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;->spanCount:I

    return-void
.end method


# virtual methods
.method public calculateExtraLayoutSpace(Landroidx/recyclerview/widget/RecyclerView$State;[I)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extraLayoutSpace"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;->recyclerviewSizeProvider:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    mul-int/lit8 v0, v0, 0x3

    const/4 p1, 0x0

    aput v0, p2, p1

    const/4 p1, 0x1

    aput v0, p2, p1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->calculateExtraLayoutSpace(Landroidx/recyclerview/widget/RecyclerView$State;[I)V

    :goto_0
    return-void
.end method

.method public getSpanCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;->spanCount:I

    return v0
.end method
