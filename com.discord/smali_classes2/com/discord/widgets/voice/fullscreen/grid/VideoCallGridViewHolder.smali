.class public final Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "VideoCallGridViewHolder.kt"


# instance fields
.field private final videoCallParticipantView:Lcom/discord/views/calls/VideoCallParticipantView;


# direct methods
.method public constructor <init>(Lcom/discord/views/calls/VideoCallParticipantView;)V
    .locals 1

    const-string v0, "videoCallParticipantView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;->videoCallParticipantView:Lcom/discord/views/calls/VideoCallParticipantView;

    return-void
.end method


# virtual methods
.method public final bind(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "participantData"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTapped"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLongClicked"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onWatchStreamClicked"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;->videoCallParticipantView:Lcom/discord/views/calls/VideoCallParticipantView;

    invoke-virtual {v0, p1}, Lcom/discord/views/calls/VideoCallParticipantView;->set(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;->videoCallParticipantView:Lcom/discord/views/calls/VideoCallParticipantView;

    invoke-virtual {v0, p4}, Lcom/discord/views/calls/VideoCallParticipantView;->setOnWatchStreamClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object p4, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;->videoCallParticipantView:Lcom/discord/views/calls/VideoCallParticipantView;

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$bind$1;

    invoke-direct {v0, p2, p1}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$bind$1;-><init>(Lkotlin/jvm/functions/Function1;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder;->videoCallParticipantView:Lcom/discord/views/calls/VideoCallParticipantView;

    new-instance p4, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$bind$2;

    invoke-direct {p4, p3, p1}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridViewHolder$bind$2;-><init>(Lkotlin/jvm/functions/Function1;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    invoke-virtual {p2, p4}, Landroid/view/ViewGroup;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method
