.class public final Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;
.super Ljava/lang/Object;
.source "UserRepresentativeColors.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;
    }
.end annotation


# static fields
.field private static final BLURPLE:I

.field public static final INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

.field private static final representativeColors:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final representativeColorsSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    invoke-direct {v0}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->representativeColors:Ljava/util/HashMap;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->representativeColorsSubject:Lrx/subjects/BehaviorSubject;

    const-string v0, "#7289DA"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->BLURPLE:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getBLURPLE$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)I
    .locals 0

    sget p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->BLURPLE:I

    return p0
.end method

.method public static final synthetic access$getRepresentativeColor(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;Landroid/graphics/Bitmap;)Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->getRepresentativeColor(Landroid/graphics/Bitmap;)Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRepresentativeColors$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)Ljava/util/HashMap;
    .locals 0

    sget-object p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->representativeColors:Ljava/util/HashMap;

    return-object p0
.end method

.method public static final synthetic access$getRepresentativeColorsSubject$p(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;)Lrx/subjects/BehaviorSubject;
    .locals 0

    sget-object p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->representativeColorsSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method private final getColorDistance(II)I
    .locals 3

    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    sub-int/2addr v0, v1

    mul-int v0, v0, v0

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    shr-int/lit8 v2, p2, 0x8

    and-int/lit16 v2, v2, 0xff

    sub-int/2addr v1, v2

    mul-int v1, v1, v1

    add-int/2addr v1, v0

    and-int/lit16 p1, p1, 0xff

    and-int/lit16 p2, p2, 0xff

    sub-int/2addr p1, p2

    mul-int p1, p1, p1

    add-int/2addr p1, v1

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    return p1
.end method

.method private final getPrimaryColorsForBitmap(Landroid/graphics/Bitmap;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->fromBitmap(Landroid/graphics/Bitmap;I)Lcom/discord/utilities/quantize/ColorCutQuantizer;

    move-result-object p1

    const-string v0, "quantizer"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/quantize/ColorCutQuantizer;->getQuantizedColors()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/utilities/quantize/Palette$Swatch;

    const-string v0, "firstSwatch"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/quantize/Palette$Swatch;->getRgb()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getRepresentativeColor(Landroid/graphics/Bitmap;)Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;
    .locals 11

    :try_start_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->getPrimaryColorsForBitmap(Landroid/graphics/Bitmap;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    const/16 v2, 0xff

    invoke-static {v0, v2}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result v0

    const v3, 0x7fffffff

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    move v6, v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v7, :cond_1

    invoke-virtual {p1, v5, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v9

    invoke-static {v9, v2}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result v9

    invoke-direct {p0, v0, v9}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->getColorDistance(II)I

    move-result v10

    if-ge v10, v3, :cond_0

    move v6, v9

    move v3, v10

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Success;

    invoke-direct {p1, v6}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Success;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Failure;

    invoke-direct {v0, p1}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult$Failure;-><init>(Ljava/lang/Exception;)V

    return-object v0
.end method


# virtual methods
.method public final getRepresentativeColorAsync(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    sget-object v0, Ly/a/h0;->a:Ly/a/v;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;-><init>(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V

    invoke-static {v0, v1, p2}, Lf/h/a/f/f/n/g;->i0(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final handleBitmap(JLandroid/graphics/Bitmap;)V
    .locals 7

    const-string v0, "bitmap"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Ly/a/r0;->d:Ly/a/r0;

    sget-object v0, Ly/a/h0;->a:Ly/a/v;

    sget-object v2, Ly/a/s1/j;->b:Ly/a/e1;

    const/4 v3, 0x0

    new-instance v4, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;

    const/4 v0, 0x0

    invoke-direct {v4, p1, p2, p3, v0}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$handleBitmap$1;-><init>(JLandroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lf/h/a/f/f/n/g;->M(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Ly/a/x;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    return-void
.end method

.method public final observeRepresentativeColorForUser(J)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->representativeColorsSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$observeRepresentativeColorForUser$1;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$observeRepresentativeColorForUser$1;-><init>(J)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p1

    const-string p2, "representativeColorsSubj\u2026  .distinctUntilChanged()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
