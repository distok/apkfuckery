.class public final Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;
.super Lx/j/h/a/g;
.source "UserRepresentativeColors.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->getRepresentativeColorAsync(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/j/h/a/g;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lx/j/h/a/d;
    c = "com.discord.widgets.voice.fullscreen.UserRepresentativeColors$getRepresentativeColorAsync$2"
    f = "UserRepresentativeColors.kt"
    l = {}
    m = "invokeSuspend"
.end annotation


# instance fields
.field public final synthetic $bitmap:Landroid/graphics/Bitmap;

.field public label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->$bitmap:Landroid/graphics/Bitmap;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lx/j/h/a/g;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;-><init>(Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;

    sget-object p2, Lkotlin/Unit;->a:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lf/h/a/f/f/n/g;->throwOnFailure(Ljava/lang/Object;)V

    sget-object p1, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$getRepresentativeColorAsync$2;->$bitmap:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;->access$getRepresentativeColor(Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors;Landroid/graphics/Bitmap;)Lcom/discord/widgets/voice/fullscreen/UserRepresentativeColors$RepresentativeColorResult;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
