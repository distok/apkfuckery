.class public final Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetGuildCallOnboardingSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final channelId:J

.field private final channelsStore:Lcom/discord/stores/StoreChannels;

.field private final guildsStore:Lcom/discord/stores/StoreGuilds;

.field private final permissionsStore:Lcom/discord/stores/StorePermissions;

.field private final selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private final voiceStatesStore:Lcom/discord/stores/StoreVoiceStates;


# direct methods
.method public constructor <init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceStates;)V
    .locals 1

    const-string v0, "selectedVoiceChannelStore"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionsStore"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelsStore"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildsStore"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voiceStatesStore"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->channelId:J

    iput-object p3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-object p4, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iput-object p5, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->channelsStore:Lcom/discord/stores/StoreChannels;

    iput-object p6, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->guildsStore:Lcom/discord/stores/StoreGuilds;

    iput-object p7, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->voiceStatesStore:Lcom/discord/stores/StoreVoiceStates;

    return-void
.end method

.method public synthetic constructor <init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceStates;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 v0, p8, 0x4

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v0

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    move-object v6, v0

    goto :goto_2

    :cond_2
    move-object v6, p5

    :goto_2
    and-int/lit8 v0, p8, 0x10

    if-eqz v0, :cond_3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    move-object v7, v0

    goto :goto_3

    :cond_3
    move-object v7, p6

    :goto_3
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_4

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceStates()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v0

    move-object v8, v0

    goto :goto_4

    :cond_4
    move-object/from16 v8, p7

    :goto_4
    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;-><init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceStates;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->INSTANCE:Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;

    iget-wide v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->channelId:J

    iget-object v3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->channelsStore:Lcom/discord/stores/StoreChannels;

    iget-object v4, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->guildsStore:Lcom/discord/stores/StoreGuilds;

    iget-object v5, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iget-object v6, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->voiceStatesStore:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual/range {v0 .. v6}, Lcom/discord/utilities/voice/VoiceChannelJoinabilityUtils;->observeJoinability(JLcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreVoiceStates;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory$observeStoreState$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "VoiceChannelJoinabilityU\u2026StoreState(joinability) }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel;

    iget-wide v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->channelId:J

    iget-object v2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/voice/fullscreen/WidgetGuildCallOnboardingSheetViewModel;-><init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lrx/Observable;)V

    return-object p1
.end method
