.class public final Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;
.super Lcom/discord/app/AppFragment;
.source "WidgetCallFullscreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

.field private static final INTENT_EXTRA_CONNECT_ON_LAUNCH:Ljava/lang/String; = "INTENT_EXTRA_CONNECT_ON_LAUNCH"


# instance fields
.field private final actionBarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final audioShareWarning$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/material/bottomsheet/BottomSheetBehavior<",
            "Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;",
            ">;"
        }
    .end annotation
.end field

.field private final callControlsSheetContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private connectedTimerSubscription:Lrx/Subscription;

.field private final defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

.field private final floatingControls$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final floatingControlsExperimental$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private gridAdapter:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

.field private final nonVideoContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final participantsHiddenView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final pictureInPicture$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private privateCallParticipantsAdapter:Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;

.field private final privateCallParticipantsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privateCallParticipantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privateCallStatusPrimary$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privateCallStatusSecondary$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final pushToTalkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final showParticipantsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stopStreamingButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final streamingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private systemWindowInsets:Landroidx/core/view/WindowInsetsCompat;

.field private final toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final tooltipManager:Lcom/discord/tooltips/TooltipManager;

.field private final videoCallGridView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x12

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v3, "floatingControls"

    const-string v4, "getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "floatingControlsExperimental"

    const-string v7, "getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "callControlsSheetContainer"

    const-string v7, "getCallControlsSheetContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "nonVideoContainer"

    const-string v7, "getNonVideoContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "videoCallGridView"

    const-string v7, "getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "pushToTalkButton"

    const-string v7, "getPushToTalkButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "actionBarLayout"

    const-string v7, "getActionBarLayout()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "toolbar"

    const-string v7, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "pictureInPicture"

    const-string v7, "getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "privateCallParticipantsContainer"

    const-string v7, "getPrivateCallParticipantsContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "privateCallParticipantsRecycler"

    const-string v7, "getPrivateCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "privateCallStatusPrimary"

    const-string v7, "getPrivateCallStatusPrimary()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "privateCallStatusSecondary"

    const-string v7, "getPrivateCallStatusSecondary()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "participantsHiddenView"

    const-string v7, "getParticipantsHiddenView()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "streamingView"

    const-string v7, "getStreamingView()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "audioShareWarning"

    const-string v7, "getAudioShareWarning()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "stopStreamingButton"

    const-string v7, "getStopStreamingButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x11

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const-string v6, "showParticipantsButton"

    const-string v7, "getShowParticipantsButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0132

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->floatingControls$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0133

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->floatingControlsExperimental$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0134

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->callControlsSheetContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0136

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->nonVideoContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0141

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->videoCallGridView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0135

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->pushToTalkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0048

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->actionBarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0047

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0139

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->pictureInPicture$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07f0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07f9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07f7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallStatusPrimary$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07f8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallStatusSecondary$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0137

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->participantsHiddenView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a013b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->streamingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a013f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->audioShareWarning$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a013a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->stopStreamingButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0138

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showParticipantsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "logger"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Lcom/discord/floating_view_manager/FloatingViewManager;

    invoke-direct {v1, v0}, Lcom/discord/floating_view_manager/FloatingViewManager;-><init>(Lcom/discord/utilities/logging/Logger;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/floating_view_manager/FloatingViewManager$b;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    move-object v7, v1

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->d:Lcom/discord/tooltips/TooltipManager$a;

    const-string v0, "floatingViewManager"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/tooltips/TooltipManager;

    :cond_2
    if-nez v2, :cond_3

    new-instance v2, Lcom/discord/tooltips/TooltipManager;

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->b:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lf/a/l/a;

    sget-object v0, Lcom/discord/tooltips/TooltipManager$a;->c:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Set;

    const/4 v6, 0x0

    const/4 v8, 0x4

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Lcom/discord/tooltips/TooltipManager;-><init>(Lf/a/l/a;Ljava/util/Set;ILcom/discord/floating_view_manager/FloatingViewManager;I)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/discord/tooltips/TooltipManager$a;->a:Ljava/lang/ref/WeakReference;

    :cond_3
    iput-object v2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    new-instance v0, Lcom/discord/tooltips/DefaultTooltipCreator;

    invoke-direct {v0, v2}, Lcom/discord/tooltips/DefaultTooltipCreator;-><init>(Lcom/discord/tooltips/TooltipManager;)V

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

    return-void
.end method

.method public static final synthetic access$getActionBarTitleAndIconColor(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getActionBarTitleAndIconColor(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getCallControlsSheetContainer$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getCallControlsSheetContainer()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChannelId(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)J
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getChannelId()J

    move-result-wide v0

    return-wide v0
.end method

.method public static final synthetic access$getConnectedTimerSubscription$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->connectedTimerSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getFloatingControlsExperimental$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getNonVideoContainer$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Landroid/view/ViewGroup;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getNonVideoContainer()Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPrivateCallStatusSecondary$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Landroid/widget/TextView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallStatusSecondary()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSystemWindowInsets$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Landroidx/core/view/WindowInsetsCompat;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->systemWindowInsets:Landroidx/core/view/WindowInsetsCompat;

    return-object p0
.end method

.method public static final synthetic access$getTooltipManager$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Lcom/discord/tooltips/TooltipManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    return-object p0
.end method

.method public static final synthetic access$getVideoCallGridView$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$onDisconnectClicked(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/model/CallModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->onDisconnectClicked(Lcom/discord/widgets/voice/model/CallModel;)V

    return-void
.end method

.method public static final synthetic access$setConnectedTimerSubscription$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->connectedTimerSubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setSystemWindowInsets$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Landroidx/core/view/WindowInsetsCompat;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->systemWindowInsets:Landroidx/core/view/WindowInsetsCompat;

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    return-void
.end method

.method public static final synthetic access$setVoiceControlsSheetPeekHeight(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->setVoiceControlsSheetPeekHeight()V

    return-void
.end method

.method public static final synthetic access$tintMenuItem(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Landroid/view/MenuItem;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->tintMenuItem(Landroid/view/MenuItem;I)V

    return-void
.end method

.method private final collapseBottomSheet()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    return-void

    :cond_0
    const-string v0, "bottomSheetBehavior"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final configureActionBar(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getDisplayMode()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getActionBarTitleAndIconColor(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;)I

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getDisplayMode()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getActionBarBackgroundColor(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;)I

    move-result v1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getToolbar()Landroidx/appcompat/widget/Toolbar;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/appcompat/widget/Toolbar;->getOverflowIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v0, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getToolbar()Landroidx/appcompat/widget/Toolbar;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/appcompat/widget/Toolbar;->getNavigationIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v0, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getActionBarLayout()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getTitle(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/discord/app/AppFragment;->setActionBarTitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitleColor(I)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getActionBarLayout()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getShowControls()Z

    move-result p1

    const-wide/16 v1, 0xc8

    invoke-static {v0, p1, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeBy(Landroid/view/View;ZJ)V

    invoke-static {p0}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarTranslucent(Landroidx/fragment/app/Fragment;)V

    const p1, 0x7f06026f

    invoke-static {p0, p1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarColor$default(Landroidx/fragment/app/Fragment;IZILjava/lang/Object;)V

    return-void
.end method

.method private final configureBottomControls(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isIdle()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->getState()I

    move-result v2

    const/4 v4, 0x3

    if-ne v2, v4, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->collapseBottomSheet()V

    goto :goto_0

    :cond_0
    const-string v1, "bottomSheetBehavior"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_1
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getScreenshareEnabled()Z

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    const/4 v4, 0x4

    :goto_1
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-direct {v0, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureSwipeTooltip(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getOutputSelectorState()Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v2

    sget-object v4, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_DISABLED:Lcom/discord/widgets/voice/model/CameraState;

    if-eq v2, v4, :cond_4

    const/4 v10, 0x1

    goto :goto_3

    :cond_4
    const/4 v10, 0x0

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isPushToTalk()Z

    move-result v11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isDeafened()Z

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getStartedAsVideo()Z

    move-result v13

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$1;

    invoke-direct {v2, v0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    new-instance v14, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$2;

    invoke-direct {v14, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v15, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$3;

    invoke-direct {v15, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$3;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v5, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$4;

    invoke-direct {v5, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$4;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isStreamFocused()Z

    move-result v16

    xor-int/lit8 v16, v16, 0x1

    move-object/from16 v18, v15

    move/from16 v15, v16

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isStreamFocused()Z

    move-result v16

    move-object/from16 v17, v14

    move/from16 v14, v16

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$5;

    move-object/from16 v20, v3

    invoke-direct {v3, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$5;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$6;

    move-object/from16 v22, v3

    invoke-direct {v3, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$6;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$7;

    move-object/from16 v21, v3

    invoke-direct {v3, v0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$7;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getShowStreamVolume()Z

    move-result v23

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getPerceptualStreamVolume()F

    move-result v24

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$8;

    move-object/from16 v25, v3

    invoke-direct {v3, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$8;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$9;

    move-object/from16 v26, v3

    invoke-direct {v3, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$9;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    move-object/from16 v16, v17

    move-object/from16 v17, v2

    move-object/from16 v19, v5

    invoke-virtual/range {v7 .. v26}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->configureUI(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;ZZZZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ZFLkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getScreenshareEnabled()Z

    move-result v3

    xor-int/2addr v3, v6

    if-eqz v3, :cond_5

    const/4 v3, 0x0

    goto :goto_4

    :cond_5
    const/16 v3, 0x8

    :goto_4
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getOutputSelectorState()Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v2

    if-eq v2, v4, :cond_6

    const/4 v10, 0x1

    goto :goto_5

    :cond_6
    const/4 v10, 0x0

    :goto_5
    new-instance v14, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$10;

    invoke-direct {v14, v0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$10;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    new-instance v13, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$11;

    iget-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    const-string v3, "viewModel"

    if-eqz v2, :cond_e

    invoke-direct {v13, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$11;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    new-instance v15, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$12;

    invoke-direct {v15, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$12;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$13;

    invoke-direct {v2, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$13;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isStreamFocused()Z

    move-result v4

    xor-int/lit8 v12, v4, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isStreamFocused()Z

    move-result v11

    new-instance v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$14;

    iget-object v5, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    if-eqz v5, :cond_d

    invoke-direct {v4, v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureBottomControls$14;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    move-object/from16 v16, v2

    move-object/from16 v17, v4

    invoke-virtual/range {v7 .. v17}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->configureUI(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getScreenshareEnabled()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getShowControls()Z

    move-result v2

    invoke-direct {v0, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureSwipeTooltip(Z)V

    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isVideoCallGridVisible()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getShowControls()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showControls()V

    goto :goto_6

    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->hideControls()V

    goto :goto_6

    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->cancelFadeAnimations(Landroid/view/View;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setAlpha(F)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->cancelFadeAnimations(Landroid/view/View;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPushToTalkButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isPushToTalk()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    goto :goto_7

    :cond_a
    const/4 v1, 0x0

    :goto_7
    if-nez v1, :cond_b

    goto :goto_8

    :cond_b
    const/4 v6, 0x0

    :goto_8
    if-eqz v6, :cond_c

    const/4 v5, 0x0

    goto :goto_9

    :cond_c
    const/16 v5, 0x8

    :goto_9
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_d
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v1, 0x0

    throw v1

    :cond_e
    const/4 v1, 0x0

    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final configureConnectionStatusText(Lcom/discord/widgets/voice/model/CallModel;)V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->connectedTimerSubscription:Lrx/Subscription;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->isConnected()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const-wide/16 v0, 0x0

    const-wide/16 v3, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v3, v4, v5}, Lrx/Observable;->A(JJLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n            .\u20260L, 1L, TimeUnit.SECONDS)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const/4 v5, 0x0

    new-instance v9, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureConnectionStatusText$1;

    invoke-direct {v9, p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureConnectionStatusText$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/model/CallModel;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureConnectionStatusText$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureConnectionStatusText$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    const/16 v10, 0x1a

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->connectedTimerSubscription:Lrx/Subscription;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lrx/Subscription;->unsubscribe()V

    :cond_2
    iput-object v2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->connectedTimerSubscription:Lrx/Subscription;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallStatusSecondary()Landroid/widget/TextView;

    move-result-object p1

    const v0, 0x7f121a64

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final configureGridUi(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "requireActivity()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallParticipantsContainer()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getParticipantsHiddenView()Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getParticipantsHiddenView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getShowParticipantsHiddenView()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getStreamingView()Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getStreamingView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getStopStreamingButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v4, "resources"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v2, v4, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_3

    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    const/16 v2, 0x8

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getStopStreamingButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$3;

    invoke-direct {v2, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$3;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getShowParticipantsButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$4;

    invoke-direct {v2, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureGridUi$4;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isVideoCallGridVisible()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    goto :goto_4

    :cond_4
    const/16 v2, 0x8

    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getPipParticipant()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_6

    const/4 v2, 0x0

    goto :goto_6

    :cond_6
    const/16 v2, 0x8

    :goto_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getPipParticipant()Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/discord/views/calls/VideoCallParticipantView;->set(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getAudioShareWarning()Landroid/view/View;

    move-result-object v0

    sget-object v2, Lcom/discord/utilities/voice/VoiceViewUtils;->INSTANCE:Lcom/discord/utilities/voice/VoiceViewUtils;

    invoke-virtual {v2}, Lcom/discord/utilities/voice/VoiceViewUtils;->getIsSoundshareSupported()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    if-nez v2, :cond_8

    const/4 v2, 0x1

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    if-eqz v2, :cond_9

    const/4 v1, 0x0

    :cond_9
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->gridAdapter:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    const/4 v1, 0x0

    const-string v2, "gridAdapter"

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->getItemCount()I

    move-result v0

    if-ne v0, v5, :cond_a

    const/4 v0, 0x1

    goto :goto_9

    :cond_a
    const/4 v0, 0x0

    :goto_9
    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getVisibleVideoParticipants()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v5, :cond_b

    goto :goto_a

    :cond_b
    const/4 v5, 0x0

    :goto_a
    iget-object v4, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->gridAdapter:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    if-eqz v4, :cond_d

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getVisibleVideoParticipants()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->setData(Ljava/util/List;)V

    if-eqz v0, :cond_c

    if-nez v5, :cond_c

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    :cond_c
    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureMenu(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureActionBar(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->enableWakeLock()V

    return-void

    :cond_d
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_e
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final configureMenu(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V
    .locals 2

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureMenu$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureMenu$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureMenu$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureMenu$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    const p1, 0x7f0e0002

    invoke-virtual {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;Lrx/functions/Action1;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method private final configurePrivateCallParticipantsUi(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getParticipantsHiddenView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getShowParticipantsHiddenView()Z

    move-result v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallParticipantsContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsAdapter:Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getPrivateCallUserListItems()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureMenu(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureActionBar(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->enableWakeLock()V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallStatusPrimary()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/voice/model/CallModel;->getDmRecipient()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureConnectionStatusText(Lcom/discord/widgets/voice/model/CallModel;)V

    return-void

    :cond_3
    const-string p1, "privateCallParticipantsAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final configureSwipeTooltip(Z)V
    .locals 9

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->defaultTooltipCreator:Lcom/discord/tooltips/DefaultTooltipCreator;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v1

    sget-object v3, Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f121a5d

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string p1, "resources.getString(R.st\u2026s_sheet_tooltip_swipe_up)"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p1, -0xc

    invoke-static {p1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getUnsubscribeSignal()Lrx/subjects/Subject;

    move-result-object p1

    sget-object v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureSwipeTooltip$1;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$configureSwipeTooltip$1;

    invoke-virtual {p1, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v8

    const-string p1, "this.unsubscribeSignal.map { Unit }"

    invoke-static {v8, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/floating_view_manager/FloatingViewGravity;->TOP:Lcom/discord/floating_view_manager/FloatingViewGravity;

    const/4 v5, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v8}, Lcom/discord/tooltips/DefaultTooltipCreator;->a(Landroid/view/View;Ljava/lang/String;Lcom/discord/tooltips/TooltipManager$Tooltip;Lcom/discord/floating_view_manager/FloatingViewGravity;IIZLrx/Observable;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->tooltipManager:Lcom/discord/tooltips/TooltipManager;

    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;->INSTANCE:Lcom/discord/widgets/voice/controls/VoiceControlsSheetSwipeTooltip;

    invoke-virtual {p1, v0}, Lcom/discord/tooltips/TooltipManager;->c(Lcom/discord/tooltips/TooltipManager$Tooltip;)V

    :goto_0
    return-void
.end method

.method private final configureValidUI(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureBottomControls(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getDisplayMode()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configurePrivateCallParticipantsUi(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureGridUi(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    :goto_0
    return-void
.end method

.method private final destroyAllRenderers()V
    .locals 5

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/views/calls/VideoCallParticipantView;->set(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Lcom/discord/views/calls/VideoCallParticipantView;

    if-eqz v4, :cond_0

    check-cast v3, Lcom/discord/views/calls/VideoCallParticipantView;

    invoke-virtual {v3, v1}, Lcom/discord/views/calls/VideoCallParticipantView;->set(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final enableWakeLock()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "requireActivity()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method private final getActionBarBackgroundColor(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;)I
    .locals 1
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f04013b

    invoke-static {p1, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0601b0

    invoke-static {p1, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    const/16 v0, 0xe5

    invoke-static {p1, v0}, Landroidx/core/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result p1

    :goto_0
    return p1
.end method

.method private final getActionBarLayout()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->actionBarLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getActionBarTitleAndIconColor(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;)I
    .locals 1
    .annotation build Landroidx/annotation/ColorInt;
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f040158

    invoke-static {p1, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method private final getAudioShareWarning()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->audioShareWarning$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCallControlsSheetContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->callControlsSheetContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getChannelId()J
    .locals 4

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private final getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->floatingControls$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    return-object v0
.end method

.method private final getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->floatingControlsExperimental$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    return-object v0
.end method

.method private final getNonVideoContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->nonVideoContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getParticipantsHiddenView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->participantsHiddenView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPictureInPicture()Lcom/discord/views/calls/VideoCallParticipantView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->pictureInPicture$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/calls/VideoCallParticipantView;

    return-object v0
.end method

.method private final getPrivateCallParticipantsContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getPrivateCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getPrivateCallStatusPrimary()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallStatusPrimary$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPrivateCallStatusSecondary()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallStatusSecondary$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPushToTalkButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->pushToTalkButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getShowParticipantsButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showParticipantsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getStopStreamingButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->stopStreamingButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getStreamingView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->streamingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getTitle(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)Ljava/lang/CharSequence;
    .locals 4

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getTitleText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getDisplayMode()Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    move-result-object p1

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;->GRID:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/text/SpannableString;

    invoke-direct {p1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040153

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x12

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object v0, p1

    :goto_0
    return-object v0
.end method

.method private final getToolbar()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->videoCallGridView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getVoiceControlsView()Landroid/view/View;
    .locals 3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v1, "2020-08_android_screenshare"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method private final hideControls()V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVoiceControlsView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0xc8

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeBy(Landroid/view/View;ZJ)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "requireActivity()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const-string v1, "requireActivity().window"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "window"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "window.decorView"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x1706

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method

.method private final initializeSystemUiListeners(Landroid/view/ViewGroup;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getActionBarLayout()Landroid/view/ViewGroup;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$2;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$2;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$3;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$3;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getCallControlsSheetContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getNonVideoContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$initializeSystemUiListeners$4;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method

.method public static final launch(Landroid/content/Context;J)V
    .locals 8

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final launch(Landroid/content/Context;JZ)V
    .locals 8

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final launch(Landroid/content/Context;JZLjava/lang/String;)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch(Landroid/content/Context;JZLjava/lang/String;)V

    return-void
.end method

.method private final onDisconnectClicked(Lcom/discord/widgets/voice/model/CallModel;)V
    .locals 9

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->disconnect()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    sget-object v2, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->INSTANCE:Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getRtcConnectionId()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, v1

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getMediaSessionId()Ljava/lang/String;

    move-result-object v1

    :cond_1
    move-object v6, v1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/voice/model/CallModel;->getCallDurationMs(Lcom/discord/utilities/time/Clock;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getCallFeedbackSampleRateDenominator()I

    move-result v8

    invoke-virtual/range {v2 .. v8}, Lcom/discord/widgets/voice/feedback/call/CallFeedbackSheetNavigator;->enqueueNotice(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Long;I)V

    return-void

    :cond_2
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final setUpGridRecycler()V
    .locals 6

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$3;

    invoke-direct {v3, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$3;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->gridAdapter:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v2, "resources"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    new-instance v0, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$layoutManager$1;

    invoke-direct {v3, p0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$setUpGridRecycler$layoutManager$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "videoCallGridView.context"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3, v2, v1, v4}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridLayoutManager;-><init>(Lkotlin/jvm/functions/Function0;IILandroid/content/Context;)V

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->gridAdapter:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    const-string v2, "gridAdapter"

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;->getSpanSizeLookup()Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVideoCallGridView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->gridAdapter:Lcom/discord/widgets/voice/fullscreen/grid/VideoCallGridAdapter;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method

.method private final setVoiceControlsSheetPeekHeight()V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->systemWindowInsets:Landroidx/core/view/WindowInsetsCompat;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/core/view/WindowInsetsCompat;->getSystemWindowInsetBottom()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->getPeekHeight()I

    move-result v1

    iget-object v2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz v2, :cond_1

    add-int/2addr v0, v1

    invoke-virtual {v2, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setPeekHeight(I)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getStreamingView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    return-void

    :cond_1
    const-string v0, "bottomSheetBehavior"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final showControls()V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getVoiceControlsView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0xc8

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeBy(Landroid/view/View;ZJ)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "requireActivity()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const-string v1, "requireActivity().window"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "window"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "window.decorView"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x700

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method

.method private final showNoScreenSharePermissionDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121122

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121118

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026e_permission_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showNoVideoPermissionDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121122

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121121

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026o_permission_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showServerDeafenedDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121659

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121658

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026ver_deafened_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showServerMutedDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f121663

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f121662

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026server_muted_dialog_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showSuppressedDialog()V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v0, "parentFragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "requireContext()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "fragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    const v3, 0x7f1217b0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f1217b3

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026ppressed_permission_body)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1211ee

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    move-object v0, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    move-object v12, v13

    move v13, v14

    move v14, v15

    move-object/from16 v15, v16

    invoke-static/range {v0 .. v15}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final tintMenuItem(Landroid/view/MenuItem;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p2, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final configureUI(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V
    .locals 1

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->configureValidUI(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0183

    return v0
.end method

.method public final handleEvent(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;)V
    .locals 13

    const-string v0, "event"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showSuppressedDialog()V

    goto/16 :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showServerMutedDialog()V

    goto/16 :goto_0

    :cond_1
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showServerDeafenedDialog()V

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showNoVideoPermissionDialog()V

    goto/16 :goto_0

    :cond_3
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "parentFragmentManager"

    if-eqz v0, :cond_4

    sget-object p1, Lf/a/a/n;->f:Lf/a/a/n$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lf/a/a/n$a;->a(Landroidx/fragment/app/FragmentManager;)V

    goto/16 :goto_0

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;

    if-eqz v0, :cond_5

    sget-object v0, Lf/a/a/i;->g:Lf/a/a/i$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;->getGuildMaxVideoChannelUsers()I

    move-result p1

    invoke-virtual {v0, v2, p1}, Lf/a/a/i$a;->a(Landroidx/fragment/app/FragmentManager;I)V

    goto/16 :goto_0

    :cond_5
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;

    if-eqz v0, :cond_6

    sget-object v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;->getUserId()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;->getChannelId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v6

    invoke-static {v6, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget-object v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;->TARGET_AND_DISMISS:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    const/4 v10, 0x0

    const/16 v11, 0x48

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    :cond_6
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowRequestCameraPermissionsDialog;

    if-eqz v0, :cond_7

    new-instance p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$handleEvent$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$handleEvent$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->requestVideoCallPermissions(Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_7
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/discord/utilities/accessibility/AccessibilityUtils;->INSTANCE:Lcom/discord/utilities/accessibility/AccessibilityUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;->getMessageResId()I

    move-result p1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "getString(event.messageResId)"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/accessibility/AccessibilityUtils;->sendAnnouncement(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->INSTANCE:Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->getStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->getMediaSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->getStreamFeedbackSampleRateDenominator()I

    move-result p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/widgets/voice/feedback/stream/StreamFeedbackSheetNavigator;->enqueueNotice(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_9
    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoScreenSharePermissionDialog;

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->showNoScreenSharePermissionDialog()V

    goto :goto_0

    :cond_a
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz p1, :cond_b

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    invoke-static {p0}, Lcom/discord/widgets/voice/stream/StreamNavigator;->requestStartStream(Landroidx/fragment/app/Fragment;)V

    :goto_0
    return-void

    :cond_b
    const-string p1, "bottomSheetBehavior"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1

    :cond_c
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onActivityResult$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onActivityResult$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-static {p1, p2, p3, v0}, Lcom/discord/widgets/voice/stream/StreamNavigator;->handleActivityResult(IILandroid/content/Intent;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const-string v0, "inflater"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_STREAM_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getChannelId()J

    move-result-wide v1

    new-instance v3, Landroidx/lifecycle/ViewModelProvider;

    new-instance v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Factory;

    invoke-direct {v4, v1, v2, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Factory;-><init>(JLjava/lang/String;)V

    invoke-direct {v3, p0, v4}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    invoke-virtual {v3, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v3, "ViewModelProvider(\n     \u2026eenViewModel::class.java)"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->setTargetChannelId(J)V

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->connectedTimerSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->destroyAllRenderers()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 12

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->initializeSystemUiListeners(Landroid/view/ViewGroup;)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "view.context"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0402c2

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const v0, 0x7f120634

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v5, p0

    invoke-static/range {v5 .. v11}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->setUpGridRecycler()V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControls()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView;->setPermissionProvider(Lcom/discord/app/AppPermissions$Requests;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object p1

    const-string v0, "BottomSheetBehavior.from\u2026tingControlsExperimental)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    const-string v0, "bottomSheetBehavior"

    if-eqz p1, :cond_4

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setGestureInsetBottomIgnored(Z)V

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz p1, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setSaveFlags(I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz p1, :cond_2

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {p1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->addBottomSheetCallback(Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$3;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addOnHeightChangedListener(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V

    new-instance p1, Lcom/discord/utilities/press/OnPressListener;

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$listener$1;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    if-eqz v1, :cond_1

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$listener$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    invoke-direct {p1, v0}, Lcom/discord/utilities/press/OnPressListener;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPushToTalkButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getFloatingControlsExperimental()Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/voice/controls/FloatingVoiceControlsView2;->setOnPTTListener(Lcom/discord/utilities/press/OnPressListener;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "INTENT_EXTRA_CONNECT_ON_LAUNCH"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$4;

    invoke-direct {p1, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBound$4;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->requestMicrophone(Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void

    :cond_1
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_2
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_3
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_4
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsAdapter:Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->getPrivateCallParticipantsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->privateCallParticipantsAdapter:Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    const-string v1, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->viewModel:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    const-string v0, "privateCallParticipantsAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
