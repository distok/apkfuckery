.class public final Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetStartCallSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_VOICE_CHANNEL_ID:Ljava/lang/String; = "ARG_VOICE_CHANNEL_ID"

.field public static final Companion:Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;


# instance fields
.field private privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

.field private final startVideoCallItem$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final startVoiceCallItem$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;

    const-string v3, "startVoiceCallItem"

    const-string v4, "getStartVoiceCallItem()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;

    const-string v6, "startVideoCallItem"

    const-string v7, "getStartVideoCallItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0a24

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->startVoiceCallItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0a23

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->startVideoCallItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getPrivateCallLauncher$p(Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;)Lcom/discord/widgets/user/calls/PrivateCallLauncher;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "privateCallLauncher"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setPrivateCallLauncher$p(Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;Lcom/discord/widgets/user/calls/PrivateCallLauncher;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    return-void
.end method

.method private final getStartVideoCallItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->startVideoCallItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStartVoiceCallItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->startVoiceCallItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final show(JLandroidx/fragment/app/FragmentManager;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$Companion;->show(JLandroidx/fragment/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02b3

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p0, p0, v0, v1}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;-><init>(Lcom/discord/app/AppPermissions$Requests;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "ARG_VOICE_CHANNEL_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide p1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->getStartVoiceCallItem()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$onViewCreated$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;J)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;->getStartVideoCallItem()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$onViewCreated$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetStartCallSheet;J)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
