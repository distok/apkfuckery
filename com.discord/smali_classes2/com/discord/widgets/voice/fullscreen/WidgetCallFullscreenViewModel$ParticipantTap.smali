.class public final Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;
.super Ljava/lang/Object;
.source "WidgetCallFullscreenViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ParticipantTap"
.end annotation


# instance fields
.field private final participantFocusKey:Ljava/lang/String;

.field private final timestamp:J


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 1

    const-string v0, "participantFocusKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    iput-wide p2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;Ljava/lang/String;JILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-wide p2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->copy(Ljava/lang/String;J)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    return-wide v0
.end method

.method public final copy(Ljava/lang/String;J)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;
    .locals 1

    const-string v0, "participantFocusKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    iget-wide v2, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getParticipantFocusKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    return-object v0
.end method

.method public final getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    invoke-static {v1, v2}, Ld;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ParticipantTap(participantFocusKey="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->participantFocusKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->timestamp:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
