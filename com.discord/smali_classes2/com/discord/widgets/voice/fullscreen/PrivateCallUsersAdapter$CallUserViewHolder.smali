.class public final Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "PrivateCallUsersAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallUserViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# instance fields
.field private final voiceUserView:Lcom/discord/views/VoiceUserView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d0024

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0140

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/VoiceUserView;

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserViewHolder;->voiceUserView:Lcom/discord/views/VoiceUserView;

    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    check-cast p2, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserItem;

    invoke-virtual {p2}, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserItem;->getCallUser()Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserViewHolder;->voiceUserView:Lcom/discord/views/VoiceUserView;

    sget-object v1, Lcom/discord/views/VoiceUserView;->m:[Lkotlin/reflect/KProperty;

    const v1, 0x7f07006c

    invoke-virtual {v0, p1, v1}, Lcom/discord/views/VoiceUserView;->a(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;I)V

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserViewHolder;->voiceUserView:Lcom/discord/views/VoiceUserView;

    const-string v0, "voiceUserView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserItem;->isTapped()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/discord/views/VoiceUserView;->setSelected(Z)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserViewHolder;->onConfigure(ILcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;)V

    return-void
.end method
