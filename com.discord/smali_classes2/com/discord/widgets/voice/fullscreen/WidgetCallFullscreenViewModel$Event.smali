.class public abstract Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;
.super Ljava/lang/Object;
.source "WidgetCallFullscreenViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Event"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoScreenSharePermissionDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowGuildVideoAtCapacityDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowRequestCameraPermissionsDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;-><init>()V

    return-void
.end method
