.class public final synthetic Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->values()[Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    const/4 v0, 0x2

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->APPLICATION_STREAMING:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    const/4 v2, 0x1

    aput v2, v1, v2

    sget-object v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->DEFAULT:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    const/4 v3, 0x0

    aput v0, v1, v3

    invoke-static {}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->values()[Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    const/4 v1, 0x6

    new-array v4, v1, [I

    sput-object v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v5, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->CONNECTING:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    aput v2, v4, v3

    sget-object v3, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->RECONNECTING:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    aput v0, v4, v0

    sget-object v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->ACTIVE:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    const/4 v0, 0x3

    aput v0, v4, v2

    sget-object v2, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->PAUSED:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    const/4 v2, 0x4

    aput v2, v4, v2

    sget-object v2, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->DENIED_FULL:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    const/4 v2, 0x5

    aput v2, v4, v2

    sget-object v2, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->ENDED:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    aput v1, v4, v0

    return-void
.end method
