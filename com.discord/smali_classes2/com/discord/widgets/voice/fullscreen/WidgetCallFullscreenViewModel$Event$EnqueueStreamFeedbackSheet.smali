.class public final Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;
.super Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;
.source "WidgetCallFullscreenViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnqueueStreamFeedbackSheet"
.end annotation


# instance fields
.field private final mediaSessionId:Ljava/lang/String;

.field private final streamFeedbackSampleRateDenominator:I

.field private final streamKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    iput p3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->copy(Ljava/lang/String;Ljava/lang/String;I)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;I)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;
    .locals 1

    const-string v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    iget p1, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMediaSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getStreamFeedbackSampleRateDenominator()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    return v0
.end method

.method public final getStreamKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "EnqueueStreamFeedbackSheet(streamKey="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mediaSessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->mediaSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", streamFeedbackSampleRateDenominator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;->streamFeedbackSampleRateDenominator:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
