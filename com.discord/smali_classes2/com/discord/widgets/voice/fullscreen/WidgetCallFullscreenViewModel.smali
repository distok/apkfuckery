.class public final Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;
.super Lf/a/b/l0;
.source "WidgetCallFullscreenViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$MenuItem;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;,
        Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private allVideoParticipants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsStore:Lcom/discord/stores/StoreAnalytics;

.field private final applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

.field private autotargetStreamKey:Ljava/lang/String;

.field private final backgroundThreadScheduler:Lrx/Scheduler;

.field private channelId:J

.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final experimentStore:Lcom/discord/stores/StoreExperiments;

.field private focusedVideoParticipantKey:Ljava/lang/String;

.field private forwardVideoGridInteractionSubscription:Lrx/Subscription;

.field private lastParticipantTap:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

.field private final mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

.field private final mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

.field private mostRecentStoreState:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;

.field private final permissionsStore:Lcom/discord/stores/StorePermissions;

.field private final selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

.field private startedAsVideo:Ljava/lang/Boolean;

.field private storeObservableSubscription:Lrx/Subscription;

.field private final streamRtcConnectionStore:Lcom/discord/stores/StoreStreamRtcConnection;

.field private final userSettingsStore:Lcom/discord/stores/StoreUserSettings;

.field private final videoPermissionsManager:Lcom/discord/utilities/permissions/VideoPermissionsManager;

.field private final videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

.field private final voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;


# direct methods
.method public constructor <init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StorePermissions;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    move-object/from16 v11, p13

    move-object/from16 v12, p14

    move-object/from16 v13, p15

    move-object/from16 v14, p16

    const-string v15, "selectedVoiceChannelStore"

    invoke-static {v1, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "userSettingsStore"

    invoke-static {v2, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "applicationStreamingStore"

    invoke-static {v3, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "mediaEngineStore"

    invoke-static {v4, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "mediaSettingsStore"

    invoke-static {v5, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "experimentStore"

    invoke-static {v6, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "permissionsStore"

    invoke-static {v7, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "clock"

    invoke-static {v8, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "backgroundThreadScheduler"

    invoke-static {v9, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "videoPermissionsManager"

    invoke-static {v10, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "voiceEngineServiceController"

    invoke-static {v11, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "streamRtcConnectionStore"

    invoke-static {v12, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "analyticsStore"

    invoke-static {v13, v15}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v15, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Uninitialized;

    invoke-direct {v0, v15}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    move-wide/from16 v14, p1

    iput-wide v14, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->channelId:J

    iput-object v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iput-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->userSettingsStore:Lcom/discord/stores/StoreUserSettings;

    iput-object v3, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object v4, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    iput-object v5, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    iput-object v6, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->experimentStore:Lcom/discord/stores/StoreExperiments;

    iput-object v7, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iput-object v8, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->clock:Lcom/discord/utilities/time/Clock;

    iput-object v9, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->backgroundThreadScheduler:Lrx/Scheduler;

    iput-object v10, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPermissionsManager:Lcom/discord/utilities/permissions/VideoPermissionsManager;

    iput-object v11, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    iput-object v12, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->streamRtcConnectionStore:Lcom/discord/stores/StoreStreamRtcConnection;

    iput-object v13, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    new-instance v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$videoPlayerIdleDetector$1;

    invoke-direct {v3, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$videoPlayerIdleDetector$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x7

    const/4 v9, 0x0

    move-object/from16 p4, v2

    move-wide/from16 p5, v4

    move-object/from16 p7, v6

    move-object/from16 p8, v7

    move-object/from16 p9, v3

    move/from16 p10, v8

    move-object/from16 p11, v9

    invoke-direct/range {p4 .. p11}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;-><init>(JLrx/Scheduler;Lrx/Scheduler;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    sget-object v2, Lx/h/l;->d:Lx/h/l;

    iput-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    move-object/from16 v2, p16

    if-eqz v2, :cond_0

    iput-object v2, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->autotargetStreamKey:Ljava/lang/String;

    sget-object v3, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v3, v2}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreVoiceChannelSelected;->selectVoiceChannel(J)Lrx/Observable;

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StorePermissions;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 19

    move/from16 v0, p17

    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    move-object v10, v1

    goto :goto_0

    :cond_0
    move-object/from16 v10, p8

    :goto_0
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v1

    move-object v11, v1

    goto :goto_1

    :cond_1
    move-object/from16 v11, p9

    :goto_1
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_2

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v1

    const-string v2, "Schedulers.computation()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v13, v1

    goto :goto_2

    :cond_2
    move-object/from16 v13, p11

    :goto_2
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_3

    new-instance v1, Lcom/discord/utilities/permissions/VideoPermissionsManager;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2, v3}, Lcom/discord/utilities/permissions/VideoPermissionsManager;-><init>(Lcom/discord/utilities/permissions/PermissionsManager;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v14, v1

    goto :goto_3

    :cond_3
    move-object/from16 v14, p12

    :goto_3
    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_4

    sget-object v1, Lcom/discord/utilities/voice/VoiceEngineServiceController;->Companion:Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/voice/VoiceEngineServiceController$Companion;->getINSTANCE()Lcom/discord/utilities/voice/VoiceEngineServiceController;

    move-result-object v1

    move-object v15, v1

    goto :goto_4

    :cond_4
    move-object/from16 v15, p13

    :goto_4
    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_5

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getStreamRtcConnection()Lcom/discord/stores/StoreStreamRtcConnection;

    move-result-object v1

    move-object/from16 v16, v1

    goto :goto_5

    :cond_5
    move-object/from16 v16, p14

    :goto_5
    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_6

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v0

    move-object/from16 v17, v0

    goto :goto_6

    :cond_6
    move-object/from16 v17, p15

    :goto_6
    move-object/from16 v2, p0

    move-wide/from16 v3, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v12, p10

    move-object/from16 v18, p16

    invoke-direct/range {v2 .. v18}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;-><init>(JLcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreMediaEngine;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StorePermissions;Lcom/discord/utilities/time/Clock;Lrx/Scheduler;Lcom/discord/utilities/permissions/VideoPermissionsManager;Lcom/discord/utilities/voice/VoiceEngineServiceController;Lcom/discord/stores/StoreStreamRtcConnection;Lcom/discord/stores/StoreAnalytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getForwardVideoGridInteractionSubscription$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->forwardVideoGridInteractionSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getVideoPlayerIdleDetector$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)Lcom/discord/utilities/video/VideoPlayerIdleDetector;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    return-object p0
.end method

.method public static final synthetic access$onIdleStateChanged(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->onIdleStateChanged(Z)V

    return-void
.end method

.method public static final synthetic access$setForwardVideoGridInteractionSubscription$p(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->forwardVideoGridInteractionSubscription:Lrx/Subscription;

    return-void
.end method

.method private final cancelTapForwardingJob()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->forwardVideoGridInteractionSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->forwardVideoGridInteractionSubscription:Lrx/Subscription;

    return-void
.end method

.method private final clearFocusedVideoParticipant()V
    .locals 28
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v6, p0

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    move-object v7, v0

    check-cast v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v7, :cond_1

    iput-object v2, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->stopWatchingStreamIfEnded()V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computeVisibleVideoParticipants()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getMyId()J

    move-result-wide v1

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v3

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v4

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computePipParticipant(JLco/discord/media_engine/VideoInputDeviceDescription;ZLcom/discord/widgets/voice/model/CameraState;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v19

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    iget-object v0, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const v26, 0x3f5fb

    const/16 v27, 0x0

    invoke-static/range {v7 .. v27}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZFILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    :cond_1
    return-void
.end method

.method private final computePipParticipant(JLco/discord/media_engine/VideoInputDeviceDescription;ZLcom/discord/widgets/voice/model/CameraState;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;
    .locals 16
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    :cond_0
    iget-object v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v7, v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v7}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v7

    if-ne v7, v5, :cond_2

    iget-object v6, v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v6}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    if-eqz v6, :cond_1

    goto :goto_1

    :cond_3
    move-object v3, v2

    :goto_1
    move-object v6, v3

    check-cast v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-eqz p4, :cond_4

    sget-object v1, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    move-object/from16 v3, p5

    if-ne v3, v1, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    iget-object v3, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v3, :cond_5

    if-nez v7, :cond_5

    const/4 v3, 0x1

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    :goto_3
    iget-object v7, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-direct {v0, v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->isOneOnOneMeCall(Ljava/util/List;)Z

    move-result v7

    if-nez v7, :cond_7

    if-nez v3, :cond_7

    if-eqz v1, :cond_6

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v1, 0x1

    :goto_5
    if-eqz v6, :cond_a

    if-eqz v1, :cond_a

    if-eqz p3, :cond_8

    invoke-virtual/range {p3 .. p3}, Lco/discord/media_engine/VideoInputDeviceDescription;->getFacing()Lco/discord/media_engine/VideoInputDeviceFacing;

    move-result-object v2

    :cond_8
    sget-object v1, Lco/discord/media_engine/VideoInputDeviceFacing;->Front:Lco/discord/media_engine/VideoInputDeviceFacing;

    if-ne v2, v1, :cond_9

    const/4 v8, 0x1

    goto :goto_6

    :cond_9
    const/4 v8, 0x0

    :goto_6
    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xbd

    invoke-static/range {v6 .. v15}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->a(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZI)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v1

    return-object v1

    :cond_a
    return-object v2
.end method

.method private final computeVisibleVideoParticipants()Ljava/util/List;
    .locals 13
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    invoke-direct {p0, v3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->getParticipantFocusKey(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-static {v3, v4}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    move-object v3, v2

    check-cast v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-nez v3, :cond_2

    iput-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->stopWatchingStreamIfEnded()V

    goto :goto_1

    :cond_2
    iget-object v0, v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->d:Lorg/webrtc/RendererCommon$ScalingType;

    iget-object v1, v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->e:Lorg/webrtc/RendererCommon$ScalingType;

    sget-object v7, Lorg/webrtc/RendererCommon$ScalingType;->SCALE_ASPECT_FIT:Lorg/webrtc/RendererCommon$ScalingType;

    if-ne v0, v7, :cond_3

    if-eq v1, v7, :cond_4

    :cond_3
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xf3

    move-object v6, v7

    invoke-static/range {v3 .. v12}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->a(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZI)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v3

    :cond_4
    invoke-static {v3}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->isOneOnOneMeCall(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v3, v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v3}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_6

    move-object v1, v2

    :cond_7
    check-cast v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    if-eqz v1, :cond_8

    invoke-static {v1}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_8
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    return-object v0
.end method

.method private final createPrivateCallParticipantListItems(Lcom/discord/widgets/voice/model/CallModel;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/voice/model/CallModel;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserItem;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getParticipants()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-direct {p0, p1, v3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->shouldIncludePrivateCallParticipant(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {v1, v0}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserItem;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/discord/widgets/voice/fullscreen/PrivateCallUsersAdapter$CallUserItem;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;Z)V

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object p1
.end method

.method private final createUserItemsComparator(ZLjava/lang/String;)Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Comparator<",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;

    invoke-direct {v0, p2, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$createUserItemsComparator$1;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static synthetic createUserItemsComparator$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;ZLjava/lang/String;ILjava/lang/Object;)Ljava/util/Comparator;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->createUserItemsComparator(ZLjava/lang/String;)Ljava/util/Comparator;

    move-result-object p0

    return-object p0
.end method

.method private final createVideoGridEntriesForParticipant(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;JLcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lco/discord/media_engine/VideoInputDeviceDescription;Z)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;",
            "J",
            "Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;",
            "Lco/discord/media_engine/VideoInputDeviceDescription;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v1, Lx/h/l;->d:Lx/h/l;

    return-object v1

    :cond_0
    const/4 v2, 0x0

    if-eqz p5, :cond_1

    invoke-virtual/range {p5 .. p5}, Lco/discord/media_engine/VideoInputDeviceDescription;->getFacing()Lco/discord/media_engine/VideoInputDeviceFacing;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v2

    :goto_0
    sget-object v4, Lco/discord/media_engine/VideoInputDeviceFacing;->Front:Lco/discord/media_engine/VideoInputDeviceFacing;

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    new-instance v4, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe()Z

    move-result v7

    if-eqz v7, :cond_3

    if-eqz v3, :cond_3

    const/4 v9, 0x1

    goto :goto_2

    :cond_3
    const/4 v9, 0x0

    :goto_2
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xbc

    move-object v7, v4

    move-object/from16 v8, p1

    move/from16 v14, p6

    invoke-direct/range {v7 .. v16}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZI)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelVoice$State;->isSelfStream()Z

    move-result v3

    if-ne v3, v6, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v3

    cmp-long v5, v3, p2

    if-eqz v5, :cond_4

    return-object v1

    :cond_4
    if-eqz p4, :cond_5

    invoke-virtual/range {p4 .. p4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v2

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual/range {p4 .. p4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->getParticipantStreamState(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    move-result-object v2

    goto :goto_3

    :cond_6
    sget-object v2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;->INACTIVE:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    :goto_3
    move-object v8, v2

    new-instance v2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    sget-object v7, Lorg/webrtc/RendererCommon$ScalingType;->SCALE_ASPECT_FIT:Lorg/webrtc/RendererCommon$ScalingType;

    const/4 v5, 0x0

    sget-object v9, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->APPLICATION_STREAMING:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    const/4 v11, 0x0

    const/16 v12, 0x80

    move-object v3, v2

    move-object/from16 v4, p1

    move-object v6, v7

    move/from16 v10, p6

    invoke-direct/range {v3 .. v12}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZI)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    if-eqz p4, :cond_a

    invoke-virtual/range {p4 .. p4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v7

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    cmp-long v9, v7, v3

    if-nez v9, :cond_8

    const/4 v5, 0x1

    :cond_8
    if-eqz v5, :cond_a

    invoke-virtual/range {p4 .. p4}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->getParticipantStreamState(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    move-result-object v9

    new-instance v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    sget-object v8, Lorg/webrtc/RendererCommon$ScalingType;->SCALE_ASPECT_FIT:Lorg/webrtc/RendererCommon$ScalingType;

    const/4 v6, 0x0

    sget-object v10, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->APPLICATION_STREAMING:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    const/4 v12, 0x0

    const/16 v13, 0x80

    move-object v4, v3

    move-object/from16 v5, p1

    move-object v7, v8

    move/from16 v11, p6

    invoke-direct/range {v4 .. v13}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;-><init>(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZI)V

    invoke-direct {v0, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->isStreamFocused(Lcom/discord/models/domain/ModelApplicationStream;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->stopWatchingStreamIfEnded()V

    :cond_a
    :goto_4
    return-object v1
.end method

.method private final emitServerDeafenedDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerDeafenedDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitServerMutedDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowServerMutedDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowNoScreenSharePermissionDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoScreenSharePermissionDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoScreenSharePermissionDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowNoVideoPermissionDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowNoVideoPermissionDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitSuppressedDialogEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowSuppressedDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final enqueueStreamFeedbackSheet(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;

    invoke-direct {v1, p1, p2, p3}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$EnqueueStreamFeedbackSheet;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final getParticipantFocusKey(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)Ljava/lang/String;
    .locals 3

    iget-object v0, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    iget-object p1, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "--STREAM"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getParticipantStreamState(Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;->INACTIVE:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    sget-object p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;->PAUSED:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;->ENDED:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;->ACTIVE:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    goto :goto_0

    :cond_4
    sget-object p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;->CONNECTING:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    :goto_0
    return-object p1
.end method

.method private final hasVideoPermission()Z
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPermissionsManager:Lcom/discord/utilities/permissions/VideoPermissionsManager;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/voice/model/CallModel;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelGuild;->getAfkChannelId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getMyPermissions()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v3, v2, v0}, Lcom/discord/utilities/permissions/VideoPermissionsManager;->hasVideoPermission(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Long;Ljava/lang/Long;)Z

    move-result v0

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private final isOneOnOneMeCall(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v0, v0, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    :goto_0
    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method private final isStreamFocused(Lcom/discord/models/domain/ModelApplicationStream;)Z
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "--STREAM"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/widgets/voice/model/CallModel;->Companion:Lcom/discord/widgets/voice/model/CallModel$Companion;

    iget-wide v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->channelId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/voice/model/CallModel$Companion;->get(J)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->experimentStore:Lcom/discord/stores/StoreExperiments;

    const-string v2, "2020-08_android_screenshare"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$observeStoreState$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->permissionsStore:Lcom/discord/stores/StorePermissions;

    iget-wide v3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->channelId:J

    invoke-virtual {v2, v3, v4}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->streamRtcConnectionStore:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStreamRtcConnection;->observeStreamVolume()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$observeStoreState$2;

    invoke-static {v0, v1, v2, v3, v4}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ate.Invalid\n      }\n    }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onIdleStateChanged(Z)V
    .locals 22
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isIdle()Z

    move-result v0

    move/from16 v5, p1

    if-eq v5, v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const v20, 0x3fff7

    const/16 v21, 0x0

    move/from16 v5, p1

    invoke-static/range {v1 .. v21}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZFILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    goto :goto_0

    :cond_1
    move-object/from16 v1, p0

    :goto_0
    return-void

    :cond_2
    move-object/from16 v1, p0

    return-void
.end method

.method private final shouldIncludePrivateCallParticipant(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isRinging()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isMe()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0
.end method

.method private final shouldShowMoreAudioOutputs(Lcom/discord/widgets/voice/model/CallModel;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getAvailableOutputDevices()Ljava/util/Set;

    move-result-object p1

    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    instance-of v0, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1
.end method

.method private final startTapForwardingJob()V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->cancelTapForwardingJob()V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->backgroundThreadScheduler:Lrx/Scheduler;

    const-wide/16 v2, 0xff

    invoke-static {v2, v3, v0, v1}, Lrx/Observable;->Z(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .time\u2026ackgroundThreadScheduler)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    new-instance v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$startTapForwardingJob$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$startTapForwardingJob$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    new-instance v9, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$startTapForwardingJob$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$startTapForwardingJob$2;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1a

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final stopWatchingStream()V
    .locals 28
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v6, p0

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    move-object v7, v0

    check-cast v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/discord/stores/StoreApplicationStreaming;->stopStream(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-direct {v6, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->isStreamFocused(Lcom/discord/models/domain/ModelApplicationStream;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v2, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computeVisibleVideoParticipants()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getMyId()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v5

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v8

    move-object/from16 v0, p0

    move-wide v1, v2

    move-object v3, v4

    move v4, v5

    move-object v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computePipParticipant(JLco/discord/media_engine/VideoInputDeviceDescription;ZLcom/discord/widgets/voice/model/CameraState;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v19

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    iget-object v0, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const v26, 0x3f5fb

    const/16 v27, 0x0

    invoke-static/range {v7 .. v27}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZFILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    :cond_1
    return-void
.end method

.method private final stopWatchingStreamIfEnded()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getState()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    move-result-object v2

    :cond_2
    sget-object v0, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;->ENDED:Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream$State;

    if-ne v2, v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->stopWatchingStream()V

    :cond_3
    return-void
.end method


# virtual methods
.method public final disableControlFading()V
    .locals 22

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isControlFadingDisabled()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const v20, 0x3ffef

    const/16 v21, 0x0

    invoke-static/range {v1 .. v21}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZFILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    goto :goto_0

    :cond_1
    move-object/from16 v1, p0

    :goto_0
    return-void

    :cond_2
    move-object/from16 v1, p0

    return-void
.end method

.method public final disconnect()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreVoiceChannelSelected;->clear()V

    return-void
.end method

.method public final focusVideoParticipant(Ljava/lang/String;)V
    .locals 28
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v0, p1

    const-string v1, "participantKey"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    move-object v7, v1

    check-cast v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v7, :cond_2

    iget-object v1, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-direct {v6, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->isOneOnOneMeCall(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v0, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computeVisibleVideoParticipants()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getMyId()J

    move-result-wide v1

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v3

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v4

    invoke-virtual {v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computePipParticipant(JLco/discord/media_engine/VideoInputDeviceDescription;ZLcom/discord/widgets/voice/model/CameraState;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v19

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    iget-object v0, v6, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const v26, 0x3f5fb

    const/16 v27, 0x0

    invoke-static/range {v7 .. v27}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZFILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    :cond_2
    return-void
.end method

.method public final handleStoreState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;)V
    .locals 31
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    const-string v0, "storeState"

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Invalid;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Invalid;

    invoke-static {v8, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;

    invoke-virtual {v7, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    goto/16 :goto_17

    :cond_0
    instance-of v0, v8, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    if-eqz v0, :cond_2d

    iget-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->startedAsVideo:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move-object v0, v8

    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->startedAsVideo:Ljava/lang/Boolean;

    :cond_1
    iget-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    const/4 v9, 0x0

    if-nez v1, :cond_2

    move-object v0, v9

    :cond_2
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    const/4 v10, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected()Z

    move-result v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    move-object v11, v8

    check-cast v11, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isChannelSelected()Z

    move-result v1

    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Invalid;

    invoke-virtual {v7, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    return-void

    :cond_4
    iget-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->userSettingsStore:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getMobileOverlay()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;->ENABLED:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;->DISABLED:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;

    :goto_1
    move-object v14, v0

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v12

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getParticipants()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    instance-of v1, v0, Ljava/util/Collection;

    const/4 v13, 0x1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    const/16 v23, 0x0

    goto :goto_3

    :cond_7
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v2

    if-eq v2, v13, :cond_a

    :cond_9
    invoke-virtual {v1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelVoice$State;->isSelfStream()Z

    move-result v1

    if-ne v1, v13, :cond_b

    :cond_a
    const/4 v1, 0x1

    goto :goto_2

    :cond_b
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_8

    const/16 v23, 0x1

    :goto_3
    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_c

    move-object v0, v9

    :cond_c
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isIdle()Z

    move-result v0

    move/from16 v16, v0

    goto :goto_4

    :cond_d
    const/16 v16, 0x0

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_e

    move-object v0, v9

    :cond_e
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isControlFadingDisabled()Z

    move-result v0

    move/from16 v17, v0

    goto :goto_5

    :cond_f
    const/16 v17, 0x0

    :goto_5
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_10
    move-object v0, v9

    :goto_6
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getParticipants()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v7, v10, v0, v13, v9}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->createUserItemsComparator$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;ZLjava/lang/String;ILjava/lang/Object;)Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    new-instance v15, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v2

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v4

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v5

    xor-int/lit8 v6, v16, 0x1

    move-object/from16 v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->createVideoGridEntriesForParticipant(Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;JLcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;Lco/discord/media_engine/VideoInputDeviceDescription;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v15, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_11
    invoke-static {v15}, Lf/h/a/f/f/n/g;->flatten(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_12
    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/voice/model/CallModel;->getVoiceSettings()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getVoiceParticipantsHidden()Z

    move-result v4

    if-eqz v4, :cond_17

    iget-object v4, v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getVoiceState()Lcom/discord/models/domain/ModelVoice$State;

    move-result-object v4

    if-eqz v4, :cond_13

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelVoice$State;->isSelfVideo()Z

    move-result v4

    if-eq v4, v13, :cond_14

    :cond_13
    iget-object v3, v3, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->f:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;

    if-eqz v3, :cond_15

    :cond_14
    const/4 v3, 0x1

    goto :goto_9

    :cond_15
    const/4 v3, 0x0

    :goto_9
    if-eqz v3, :cond_16

    goto :goto_a

    :cond_16
    const/4 v3, 0x0

    goto :goto_b

    :cond_17
    :goto_a
    const/4 v3, 0x1

    :goto_b
    if-eqz v3, :cond_12

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_18
    sget-object v1, Lx/h/l;->d:Lx/h/l;

    :cond_19
    iput-object v1, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->allVideoParticipants:Ljava/util/List;

    invoke-direct {v7, v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->isOneOnOneMeCall(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iput-object v9, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computeVisibleVideoParticipants()Ljava/util/List;

    move-result-object v15

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1b

    sget-object v0, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_DISABLED:Lcom/discord/widgets/voice/model/CameraState;

    :goto_c
    move-object v6, v0

    goto :goto_d

    :cond_1b
    if-eqz v12, :cond_1c

    sget-object v0, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    goto :goto_c

    :cond_1c
    sget-object v0, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_OFF:Lcom/discord/widgets/voice/model/CameraState;

    goto :goto_c

    :goto_d
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getVideoDevices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_1d

    const/4 v0, 0x1

    goto :goto_e

    :cond_1d
    const/4 v0, 0x0

    :goto_e
    if-eqz v0, :cond_1e

    sget-object v0, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v6, v0, :cond_1e

    const/16 v18, 0x1

    goto :goto_f

    :cond_1e
    const/16 v18, 0x0

    :goto_f
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v0

    if-eqz v0, :cond_1f

    if-nez v23, :cond_1f

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;->PRIVATE_CALL_PARTICIPANTS:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    goto :goto_10

    :cond_1f
    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;->GRID:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    :goto_10
    move-object v12, v0

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getInputMode()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    move-result-object v0

    sget-object v2, Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;->PUSH_TO_TALK:Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;

    if-nez v17, :cond_20

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;->GRID:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    if-ne v12, v0, :cond_20

    iget-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->beginIdleDetection()V

    goto :goto_11

    :cond_20
    iget-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->endIdleDetection()V

    :goto_11
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getAudioDevicesState()Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;

    move-result-object v0

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v2

    invoke-direct {v7, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->shouldShowMoreAudioOutputs(Lcom/discord/widgets/voice/model/CallModel;)Z

    move-result v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreAudioDevices$AudioDevicesState;->getSelectedOutputDevice()Lcom/discord/stores/StoreAudioDevices$OutputDevice;

    move-result-object v0

    if-eqz v2, :cond_23

    instance-of v2, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$BluetoothAudio;

    if-eqz v2, :cond_21

    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->BLUETOOTH_ON_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    goto :goto_12

    :cond_21
    instance-of v0, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    if-eqz v0, :cond_22

    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_ON_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    goto :goto_12

    :cond_22
    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_OFF_AND_MORE:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    goto :goto_12

    :cond_23
    instance-of v0, v0, Lcom/discord/stores/StoreAudioDevices$OutputDevice$Speaker;

    if-eqz v0, :cond_24

    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_ON:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    goto :goto_12

    :cond_24
    sget-object v0, Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;->SPEAKER_OFF:Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;

    :goto_12
    move-object/from16 v21, v0

    iget-object v0, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->autotargetStreamKey:Ljava/lang/String;

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v13

    if-eqz v2, :cond_2a

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_26

    :cond_25
    const/4 v1, 0x0

    goto :goto_14

    :cond_26
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_27
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    iget-object v2, v2, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {v2}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getApplicationStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    if-eqz v2, :cond_28

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v2

    goto :goto_13

    :cond_28
    move-object v2, v9

    :goto_13
    invoke-static {v2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    const/4 v1, 0x1

    :goto_14
    if-eqz v1, :cond_29

    invoke-virtual {v7, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->targetAndFocusStream(Ljava/lang/String;)V

    :cond_29
    iput-object v9, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->autotargetStreamKey:Ljava/lang/String;

    :cond_2a
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getMyId()J

    move-result-wide v1

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getSelectedVideoDevice()Lco/discord/media_engine/VideoInputDeviceDescription;

    move-result-object v3

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v4

    move-object/from16 v0, p0

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->computePipParticipant(JLco/discord/media_engine/VideoInputDeviceDescription;ZLcom/discord/widgets/voice/model/CameraState;)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v24

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->createPrivateCallParticipantListItems(Lcom/discord/widgets/voice/model/CallModel;)Ljava/util/List;

    move-result-object v26

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    iget-object v2, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getNoiseCancellation()Ljava/lang/Boolean;

    move-result-object v25

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/voice/model/CallModel;->getVoiceSettings()Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreMediaSettings$VoiceConfiguration;->getVoiceParticipantsHidden()Z

    move-result v3

    if-eqz v3, :cond_2b

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v3

    if-nez v3, :cond_2b

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2b

    sget-object v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;->GRID:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;

    if-ne v12, v3, :cond_2b

    const/16 v27, 0x1

    goto :goto_15

    :cond_2b
    const/16 v27, 0x0

    :goto_15
    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getScreenshareEnabled()Z

    move-result v28

    iget-object v3, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->startedAsVideo:Ljava/lang/Boolean;

    if-eqz v3, :cond_2c

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move/from16 v29, v10

    goto :goto_16

    :cond_2c
    const/16 v29, 0x0

    :goto_16
    sget-object v3, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->INSTANCE:Lcom/discord/utilities/voice/PerceptualVolumeUtils;

    invoke-virtual {v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getStreamVolume()F

    move-result v4

    const/high16 v5, 0x43960000    # 300.0f

    invoke-virtual {v3, v4, v5}, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->amplitudeToPerceptual(FF)F

    move-result v30

    move-object v3, v12

    move-object v12, v0

    move-object v13, v1

    move-object/from16 v19, v6

    move-object/from16 v20, v3

    move-object/from16 v22, v2

    invoke-direct/range {v12 .. v30}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;-><init>(Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZF)V

    invoke-virtual {v7, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    :cond_2d
    :goto_17
    iput-object v8, v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;

    return-void
.end method

.method public modifyPendingViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;
    .locals 23

    move-object/from16 v0, p2

    const-string v1, "pendingViewState"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v1, p1

    instance-of v1, v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v1, :cond_5

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v1, :cond_5

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    invoke-virtual {v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isIdle()Z

    move-result v0

    invoke-virtual {v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getVisibleVideoParticipants()Ljava/util/List;

    move-result-object v1

    new-instance v5, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getVisibleVideoParticipants()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-eq v7, v4, :cond_0

    const/4 v13, 0x1

    goto :goto_1

    :cond_0
    const/4 v13, 0x0

    :goto_1
    move-object/from16 v15, p0

    iget-object v7, v15, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    if-eqz v7, :cond_1

    const/4 v14, 0x1

    goto :goto_2

    :cond_1
    const/4 v14, 0x0

    :goto_2
    iget-boolean v3, v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->h:Z

    if-ne v3, v13, :cond_2

    iget-boolean v3, v6, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->i:Z

    if-eq v3, v14, :cond_3

    :cond_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v3, 0x3f

    move v15, v3

    invoke-static/range {v6 .. v15}, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->a(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;ZLorg/webrtc/RendererCommon$ScalingType;Lorg/webrtc/RendererCommon$ScalingType;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$ApplicationStreamState;Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;ZZI)Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;

    move-result-object v6

    :cond_3
    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const v21, 0x3fffb

    const/16 v22, 0x0

    invoke-static/range {v2 .. v22}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->copy$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;Lcom/discord/widgets/voice/model/CallModel;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$OverlayStatus;Ljava/util/List;ZZZLcom/discord/widgets/voice/model/CameraState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$DisplayMode;Lcom/discord/widgets/voice/controls/VoiceControlsOutputSelectorState;Ljava/lang/String;ZLcom/discord/views/calls/VideoCallParticipantView$ParticipantData;Ljava/lang/Boolean;Ljava/util/List;ZZZFILjava/lang/Object;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    move-result-object v0

    :cond_5
    return-object v0
.end method

.method public bridge synthetic modifyPendingViewState(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;

    check-cast p2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->modifyPendingViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;

    move-result-object p1

    return-object p1
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onCameraPermissionsGranted()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/discord/stores/StoreMediaEngine;->selectDefaultVideoDevice$default(Lcom/discord/stores/StoreMediaEngine;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;

    const v2, 0x7f1203ca

    invoke-direct {v1, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onCleared()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->dispose()V

    return-void
.end method

.method public final onDeafenPressed()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isServerDeafened()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->emitServerDeafenedDialogEvent()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfDeafened()V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    move-object v2, v0

    :goto_0
    check-cast v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;

    invoke-virtual {v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->isDeafened()Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f121a5a

    goto :goto_1

    :cond_3
    const v2, 0x7f121a53

    :goto_1
    invoke-direct {v1, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public final onEmptyStateTapped()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->startTapForwardingJob()V

    return-void
.end method

.method public final onGridParticipantLongPressed(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V
    .locals 6
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "participant"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->g:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    sget-object v1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;->DEFAULT:Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;

    iget-object p1, p1, Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;->b:Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceParticipants$VoiceUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->channelId:J

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowUserSheet;-><init>(JJ)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final onGridParticipantTapped(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)V
    .locals 6
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->getParticipantFocusKey(Lcom/discord/views/calls/VideoCallParticipantView$ParticipantData;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, p1, v1, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;-><init>(Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->lastParticipantTap:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    iput-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->lastParticipantTap:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->getParticipantFocusKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    if-eqz v2, :cond_0

    goto :goto_3

    :cond_0
    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->getTimestamp()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;->getTimestamp()J

    move-result-wide v0

    sub-long/2addr v4, v0

    const-wide/16 v0, 0xff

    cmp-long v2, v4, v0

    if-gtz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusedVideoParticipantKey:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusVideoParticipant(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->clearFocusedVideoParticipant()V

    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->cancelTapForwardingJob()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->lastParticipantTap:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ParticipantTap;

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->startTapForwardingJob()V

    :goto_2
    return-void

    :cond_4
    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->startTapForwardingJob()V

    return-void
.end method

.method public final onMuteClicked()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isSuppressed()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->emitSuppressedDialogEvent()V

    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->isMuted()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->emitServerMutedDialogEvent()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfMuted()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isMeMutedByAnySource()Z

    move-result v0

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;

    if-eqz v0, :cond_3

    const v0, 0x7f121a5b

    goto :goto_0

    :cond_3
    const v0, 0x7f121a56

    :goto_0
    invoke-direct {v2, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;-><init>(I)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public final onPushToTalkPressed(Z)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->beginIdleDetection()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->endIdleDetection()V

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreMediaEngine;->setPttActive(Z)V

    return-void
.end method

.method public final onScreenShareClick()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isStreaming()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->stopScreenShare()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->hasVideoPermission()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->emitShowNoScreenSharePermissionDialogEvent()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$RequestStartStream;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public final onShowParticipantsPressed()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaSettingsStore:Lcom/discord/stores/StoreMediaSettings;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreMediaSettings;->updateVoiceParticipantsHidden(Z)V

    return-void
.end method

.method public final onStreamPerceptualVolumeChanged(FZ)V
    .locals 1

    if-eqz p2, :cond_0

    sget-object p2, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->INSTANCE:Lcom/discord/utilities/voice/PerceptualVolumeUtils;

    const/high16 v0, 0x43960000    # 300.0f

    invoke-virtual {p2, p1, v0}, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->perceptualToAmplitude(FF)F

    move-result p1

    iget-object p2, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->streamRtcConnectionStore:Lcom/discord/stores/StoreStreamRtcConnection;

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreStreamRtcConnection;->updateStreamVolume(F)V

    :cond_0
    return-void
.end method

.method public final requestStopWatchingStreamFromUserInput()V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->stopWatchingStream()V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getMediaSessionId()Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getStreamFeedbackSampleRateDenominator()I

    move-result v0

    invoke-direct {p0, v1, v2, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->enqueueStreamFeedbackSheet(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_2
    return-void
.end method

.method public final setTargetChannelId(J)V
    .locals 10

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->storeObservableSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    iput-wide p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->channelId:J

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->observeStoreState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    const/4 v0, 0x0

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$setTargetChannelId$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$setTargetChannelId$1;-><init>(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final startIdle()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mostRecentStoreState:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState;

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$StoreState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->isVideoCall()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->beginIdleDetection()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final startScreenShare(Landroid/content/Intent;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "intent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-virtual {v0, p1}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->startStream(Landroid/content/Intent;)V

    return-void
.end method

.method public final stopIdle()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->endIdleDetection()V

    return-void
.end method

.method public final stopScreenShare()V
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->voiceEngineServiceController:Lcom/discord/utilities/voice/VoiceEngineServiceController;

    invoke-virtual {v0}, Lcom/discord/utilities/voice/VoiceEngineServiceController;->stopStream()V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getActiveStream()Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/stores/StoreApplicationStreaming$ActiveApplicationStream;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getRtcConnectionMetadata()Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/stores/StoreRtcConnection$RtcConnectionMetadata;->getMediaSessionId()Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getStreamFeedbackSampleRateDenominator()I

    move-result v0

    invoke-direct {p0, v1, v2, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->enqueueStreamFeedbackSheet(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_2
    return-void
.end method

.method public final switchCameraInputPressed()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_DISABLED:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v1, v2, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    return-void

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMediaEngine;->cycleVideoInputDevice()V

    :cond_4
    return-void
.end method

.method public final targetAndFocusStream(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->targetStream(Ljava/lang/String;)V

    sget-object v0, Lcom/discord/models/domain/ModelApplicationStream;->Companion:Lcom/discord/models/domain/ModelApplicationStream$Companion;

    invoke-virtual {v0, p1}, Lcom/discord/models/domain/ModelApplicationStream$Companion;->decodeStreamKey(Ljava/lang/String;)Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getOwnerId()J

    move-result-wide v0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "--STREAM"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->focusVideoParticipant(Ljava/lang/String;)V

    return-void
.end method

.method public final targetStream(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "streamKey"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->applicationStreamingStore:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreApplicationStreaming;->targetStream(Ljava/lang/String;)V

    return-void
.end method

.method public final toggleCameraPressed()V
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->videoPlayerIdleDetector:Lcom/discord/utilities/video/VideoPlayerIdleDetector;

    invoke-virtual {v0}, Lcom/discord/utilities/video/VideoPlayerIdleDetector;->onPreventIdle()V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v1

    sget-object v3, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_DISABLED:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v1, v3, :cond_1

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->hasVideoPermission()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->emitShowNoVideoPermissionDialogEvent()V

    return-void

    :cond_2
    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCameraState()Lcom/discord/widgets/voice/model/CameraState;

    move-result-object v1

    sget-object v3, Lcom/discord/widgets/voice/model/CameraState;->CAMERA_ON:Lcom/discord/widgets/voice/model/CameraState;

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->mediaEngineStore:Lcom/discord/stores/StoreMediaEngine;

    invoke-virtual {v0, v2}, Lcom/discord/stores/StoreMediaEngine;->selectVideoInputDevice(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;

    const v2, 0x7f1203c9

    invoke-direct {v1, v2}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$AccessibilityAnnouncement;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/voice/model/CallModel;->getNumUsersConnected()I

    move-result v1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/voice/model/CallModel;->getGuildMaxVideoChannelMembers()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v1, v2, :cond_5

    iget-object v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v2, v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowCameraCapacityDialog;-><init>(I)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowRequestCameraPermissionsDialog;->INSTANCE:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$Event$ShowRequestCameraPermissionsDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_6
    :goto_1
    return-void
.end method

.method public final tryConnectToVoice()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->selectedVoiceChannelStore:Lcom/discord/stores/StoreVoiceChannelSelected;

    iget-wide v1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->channelId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreVoiceChannelSelected;->selectVoiceChannel(J)Lrx/Observable;

    return-void
.end method

.method public updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V
    .locals 5

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getAnalyticsVideoLayout()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v2

    :goto_0
    invoke-super {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    instance-of v1, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getAnalyticsVideoLayout()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {v1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getMyId()J

    move-result-wide v3

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-nez v0, :cond_2

    move-object p1, v2

    :cond_2
    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState$Valid;->getCallModel()Lcom/discord/widgets/voice/model/CallModel;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/discord/widgets/voice/model/CallModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v2

    :cond_3
    iget-object p1, p0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->analyticsStore:Lcom/discord/stores/StoreAnalytics;

    invoke-virtual {p1, v1, v3, v4, v2}, Lcom/discord/stores/StoreAnalytics;->trackVideoLayoutToggled(Ljava/lang/String;JLcom/discord/models/domain/ModelChannel;)V

    :cond_4
    return-void
.end method

.method public bridge synthetic updateViewState(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel;->updateViewState(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreenViewModel$ViewState;)V

    return-void
.end method
