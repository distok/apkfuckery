.class public final synthetic Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderViewHolder$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->values()[Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v0, 0x7

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderViewHolder$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->FILTER:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->MENTIONS_USER:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v1, 0x2

    aput v1, v0, v2

    sget-object v2, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->FROM_USER:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->HAS:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v1, 0x4

    aput v1, v0, v2

    sget-object v2, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->BEFORE_DATE:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v1, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->IN_CHANNEL:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    const/4 v1, 0x6

    aput v1, v0, v2

    return-void
.end method
