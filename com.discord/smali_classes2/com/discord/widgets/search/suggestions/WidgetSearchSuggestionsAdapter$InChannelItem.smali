.class public final Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;
.super Lcom/discord/utilities/mg_recycler/SingleTypePayload;
.source "WidgetSearchSuggestionsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InChannelItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/SingleTypePayload<",
        "Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;",
        ">;"
    }
.end annotation


# instance fields
.field private final data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;->getChannelId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/utilities/mg_recycler/SingleTypePayload;-><init>(Ljava/lang/Object;Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    return-void
.end method

.method private final component1()Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;ILjava/lang/Object;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->copy(Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;

    invoke-direct {v0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    iget-object p1, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "InChannelItem(data="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;->data:Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
