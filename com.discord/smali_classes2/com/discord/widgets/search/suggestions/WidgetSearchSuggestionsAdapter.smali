.class public final Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetSearchSuggestionsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$FilterItem;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$UserItem;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$RecentQueryItem;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$FilterViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HistoryHeaderViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$UserViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$RecentQueryViewHolder;,
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$Companion;

.field private static final TYPE_FILTER:I = 0x2

.field private static final TYPE_HAS:I = 0x5

.field private static final TYPE_HEADER:I = 0x0

.field private static final TYPE_HISTORY_HEADER:I = 0x1

.field private static final TYPE_IN_CHANNEL:I = 0x4

.field private static final TYPE_RECENT_QUERY:I = 0x6

.field private static final TYPE_USER:I = 0x3


# instance fields
.field private onChannelClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onClearHistoryClicked:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onFilterClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/query/FilterType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onHasClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onRecentQueryClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onUserClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->Companion:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onFilterClicked$1;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onFilterClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onFilterClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onUserClicked$1;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onUserClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onUserClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onChannelClicked$1;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onChannelClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onChannelClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onHasClicked$1;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onHasClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onHasClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onRecentQueryClicked$1;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onRecentQueryClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onRecentQueryClicked:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onClearHistoryClicked$1;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$onClearHistoryClicked$1;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onClearHistoryClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method


# virtual methods
.method public final getOnChannelClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onChannelClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClearHistoryClicked()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onClearHistoryClicked:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnFilterClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/search/query/FilterType;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onFilterClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnHasClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onHasClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnRecentQueryClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onRecentQueryClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnUserClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onUserClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "*",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    packed-switch p2, :pswitch_data_0

    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :pswitch_0
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$RecentQueryViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$RecentQueryViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    goto :goto_0

    :pswitch_1
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    goto :goto_0

    :pswitch_2
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    goto :goto_0

    :pswitch_3
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$UserViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$UserViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    goto :goto_0

    :pswitch_4
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$FilterViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$FilterViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    goto :goto_0

    :pswitch_5
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HistoryHeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HistoryHeaderViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    goto :goto_0

    :pswitch_6
    new-instance p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderViewHolder;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;)V

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final setOnChannelClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onChannelClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnClearHistoryClicked(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onClearHistoryClicked:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setOnFilterClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/query/FilterType;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onFilterClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnHasClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onHasClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnRecentQueryClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onRecentQueryClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setOnUserClicked(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->onUserClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method
