.class public final Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;
.super Lcom/discord/app/AppFragment;
.source "WidgetSearchSuggestions.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

.field private searchStringProvider:Lcom/discord/utilities/search/strings/SearchStringProvider;

.field private final suggestionsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;

    const-string v3, "suggestionsRecycler"

    const-string v4, "getSuggestionsRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    sput-object v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0890

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->suggestionsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->configureUI(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V

    return-void
.end method

.method public static final synthetic access$getSearchStringProvider$p(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;)Lcom/discord/utilities/search/strings/SearchStringProvider;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->searchStringProvider:Lcom/discord/utilities/search/strings/SearchStringProvider;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "searchStringProvider"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setSearchStringProvider$p(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;Lcom/discord/utilities/search/strings/SearchStringProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->searchStringProvider:Lcom/discord/utilities/search/strings/SearchStringProvider;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    const/4 v1, 0x0

    const-string v2, "adapter"

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->getSuggestionItems()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    if-eqz v0, :cond_5

    new-instance v3, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$1;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$1;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->setOnFilterClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    if-eqz v0, :cond_4

    new-instance v3, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$2;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$2;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->setOnUserClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    if-eqz v0, :cond_3

    new-instance v3, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$3;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$3;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->setOnChannelClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    if-eqz v0, :cond_2

    new-instance v3, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$4;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$4;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;)V

    invoke-virtual {v0, v3}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->setOnHasClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$5;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$5;

    invoke-virtual {p1, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->setOnRecentQueryClicked(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$6;->INSTANCE:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$configureUI$6;

    invoke-virtual {p1, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;->setOnClearHistoryClicked(Lkotlin/jvm/functions/Function0;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_6
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final getSuggestionsRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->suggestionsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0257

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object p1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->getSuggestionsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p1, v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->adapter:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;->searchStringProvider:Lcom/discord/utilities/search/strings/SearchStringProvider;

    sget-object v1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->Companion:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;->get(Lcom/discord/utilities/search/strings/SearchStringProvider;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "searchStringProvider"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
