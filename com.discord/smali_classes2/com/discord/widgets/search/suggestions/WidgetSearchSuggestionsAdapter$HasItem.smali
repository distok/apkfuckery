.class public final Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;
.super Lcom/discord/utilities/mg_recycler/SingleTypePayload;
.source "WidgetSearchSuggestionsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HasItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/SingleTypePayload<",
        "Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;",
        ">;"
    }
.end annotation


# instance fields
.field private final data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;->getHasAnswerOption()Lcom/discord/utilities/search/query/node/answer/HasAnswerOption;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/utilities/mg_recycler/SingleTypePayload;-><init>(Ljava/lang/Object;Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    return-void
.end method

.method private final component1()Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;ILjava/lang/Object;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->copy(Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;

    invoke-direct {v0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    iget-object p1, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "HasItem(data="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;->data:Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
