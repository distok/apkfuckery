.class public final Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem$Companion;
.super Ljava/lang/Object;
.source "WidgetSearchSuggestionsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$getType(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem$Companion;Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem$Companion;->getType(Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;)I

    move-result p0

    return p0
.end method

.method private final getType(Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;)I
    .locals 1

    sget-object v0, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;->RECENT_QUERY:Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
