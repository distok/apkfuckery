.class public final Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;
.super Ljava/lang/Object;
.source "WidgetSearchSuggestions.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;


# instance fields
.field private final query:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->Companion:Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;",
            ">;)V"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "suggestionEntries"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    iput-object p2, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;

    invoke-interface {v0}, Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;->getCategory()Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;

    move-result-object v1

    if-eqz p2, :cond_0

    if-eq v1, p2, :cond_1

    :cond_0
    iget-object p2, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    new-instance v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem;

    invoke-direct {v2, v1}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HeaderItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion$Category;)V

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p2, v1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_6

    const/4 v2, 0x1

    if-eq v1, v2, :cond_5

    const/4 v2, 0x2

    if-eq v1, v2, :cond_5

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    const/4 v2, 0x6

    if-eq v1, v2, :cond_2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    new-instance v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$RecentQueryItem;

    check-cast v0, Lcom/discord/utilities/search/suggestion/entries/RecentQuerySuggestion;

    invoke-direct {v2, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$RecentQueryItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/RecentQuerySuggestion;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    new-instance v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;

    check-cast v0, Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;

    invoke-direct {v2, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$InChannelItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/ChannelSuggestion;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    new-instance v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;

    check-cast v0, Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;

    invoke-direct {v2, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$HasItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/HasSuggestion;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    new-instance v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$UserItem;

    check-cast v0, Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;

    invoke-direct {v2, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$UserItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/UserSuggestion;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    new-instance v2, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$FilterItem;

    check-cast v0, Lcom/discord/utilities/search/suggestion/entries/FilterSuggestion;

    invoke-direct {v2, v0}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestionsAdapter$FilterItem;-><init>(Lcom/discord/utilities/search/suggestion/entries/FilterSuggestion;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_7
    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->copy(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;)Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;",
            ">;)",
            "Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;"
        }
    .end annotation

    const-string v0, "query"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "suggestionEntries"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getQuery()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/query/node/QueryNode;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    return-object v0
.end method

.method public final getSuggestionEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/search/suggestion/entries/SearchSuggestion;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    return-object v0
.end method

.method public final getSuggestionItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionItems:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Model(query="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->query:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", suggestionEntries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/search/suggestions/WidgetSearchSuggestions$Model;->suggestionEntries:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
