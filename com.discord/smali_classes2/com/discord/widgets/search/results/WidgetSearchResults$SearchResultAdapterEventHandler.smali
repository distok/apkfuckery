.class public Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;
.super Ljava/lang/Object;
.source "WidgetSearchResults.java"

# interfaces
.implements Lcom/discord/widgets/chat/list/WidgetChatListAdapter$EventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/search/results/WidgetSearchResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SearchResultAdapterEventHandler"
.end annotation


# instance fields
.field private oldestMessageId:J

.field public final synthetic this$0:Lcom/discord/widgets/search/results/WidgetSearchResults;


# direct methods
.method private constructor <init>(Lcom/discord/widgets/search/results/WidgetSearchResults;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->this$0:Lcom/discord/widgets/search/results/WidgetSearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/search/results/WidgetSearchResults;Lcom/discord/widgets/search/results/WidgetSearchResults$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;-><init>(Lcom/discord/widgets/search/results/WidgetSearchResults;)V

    return-void
.end method

.method private jumpToChat(Lcom/discord/models/domain/ModelMessage;)V
    .locals 5

    invoke-static {}, Lcom/discord/stores/StoreStream;->getMessagesLoader()Lcom/discord/stores/StoreMessagesLoader;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelMessage;->getId()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/discord/stores/StoreMessagesLoader;->jumpToMessage(JJ)V

    iget-object p1, p0, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->this$0:Lcom/discord/widgets/search/results/WidgetSearchResults;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lf/a/b/m;->a(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onCallMessageClicked(JLcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;)V
    .locals 0
    .param p3    # Lcom/discord/widgets/chat/list/WidgetChatListAdapterItemCallMessage$CallStatus;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onInteractionStateUpdated(Lcom/discord/stores/StoreChat$InteractionState;)V
    .locals 4
    .param p1    # Lcom/discord/stores/StoreChat$InteractionState;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Lcom/discord/stores/StoreChat$InteractionState;->isAtTop()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->oldestMessageId:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long p1, v0, v2

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->oldestMessageId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreSearch;->loadMore(J)V

    :cond_0
    return-void
.end method

.method public onListClicked()V
    .locals 0

    return-void
.end method

.method public onMessageAuthorAvatarClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onMessageAuthorLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/Long;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onMessageAuthorNameClicked(Lcom/discord/models/domain/ModelMessage;J)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onMessageBlockedGroupClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onMessageClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onMessageLongClicked(Lcom/discord/models/domain/ModelMessage;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onOldestMessageId(JJ)V
    .locals 0

    iput-wide p3, p0, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->oldestMessageId:J

    return-void
.end method

.method public onOpenPinsClicked(Lcom/discord/models/domain/ModelMessage;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onQuickAddReactionClicked(JJJJZ)V
    .locals 0

    return-void
.end method

.method public onQuickDownloadClicked(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 p1, 0x0

    return p1
.end method

.method public onReactionClicked(JJJJZLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 0
    .param p10    # Lcom/discord/models/domain/ModelMessageReaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onReactionLongClicked(JJJZLcom/discord/models/domain/ModelMessageReaction;)V
    .locals 0
    .param p8    # Lcom/discord/models/domain/ModelMessageReaction;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onStickerClicked(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/sticker/dto/ModelSticker;)V
    .locals 0
    .param p1    # Lcom/discord/models/domain/ModelMessage;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/discord/models/sticker/dto/ModelSticker;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->jumpToChat(Lcom/discord/models/domain/ModelMessage;)V

    return-void
.end method

.method public onUrlLongClicked(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/search/results/WidgetSearchResults$SearchResultAdapterEventHandler;->this$0:Lcom/discord/widgets/search/results/WidgetSearchResults;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/discord/widgets/chat/WidgetUrlActions;->launch(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public onUserActivityAction(JJJILcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelApplication;)V
    .locals 0
    .param p8    # Lcom/discord/models/domain/activity/ModelActivity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p9    # Lcom/discord/models/domain/ModelApplication;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method public onUserMentionClicked(JJJ)V
    .locals 0

    return-void
.end method
