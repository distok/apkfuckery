.class public Lcom/discord/widgets/search/WidgetSearch;
.super Lcom/discord/app/AppFragment;
.source "WidgetSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/search/WidgetSearch$Model;
    }
.end annotation


# static fields
.field public static final INTENT_EXTRA_TARGET_ID:Ljava/lang/String; = "INTENT_EXTRA_TARGET_ID"

.field public static final INTENT_EXTRA_TARGET_TYPE:Ljava/lang/String; = "INTENT_EXTRA_SEARCH_TYPE"

.field private static final TARGET_TYPE_CHANNEL:I = 0x1

.field private static final TARGET_TYPE_GUILD:I


# instance fields
.field private searchInput:Lcom/google/android/material/textfield/TextInputLayout;

.field private searchResults:Landroid/view/View;

.field private searchSuggestions:Landroid/view/View;

.field private sendQueryFab:Landroid/view/View;

.field public targetId:J

.field public targetType:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetId:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetType:I

    return-void
.end method

.method private configureSearchInput()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v1, Lf/a/o/d/l;

    invoke-direct {v1, p0}, Lf/a/o/d/l;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditorActionListener(Lcom/google/android/material/textfield/TextInputLayout;Lkotlin/jvm/functions/Function3;)Lkotlin/Unit;

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v1, Lf/a/o/d/c;

    invoke-direct {v1, p0}, Lf/a/o/d/c;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-static {v0, p0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearch;->getStoreSearchInput()Lcom/discord/stores/StoreSearchInput;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearchInput;->getForcedInput()Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/d/d;

    invoke-direct {v1, p0}, Lf/a/o/d/d;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lf/a/o/d/a;->d:Lf/a/o/d/a;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/d/j;

    invoke-direct {v1, p0}, Lf/a/o/d/j;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/search/WidgetSearch$Model;)V
    .locals 4

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {p1}, Lcom/discord/widgets/search/WidgetSearch$Model;->access$000(Lcom/discord/widgets/search/WidgetSearch$Model;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setSingleLineHint(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/discord/widgets/search/WidgetSearch$Model;->access$100(Lcom/discord/widgets/search/WidgetSearch$Model;)Lcom/discord/stores/StoreSearch$DisplayState;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x4

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    const/4 p1, 0x1

    if-eq v0, p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/discord/widgets/search/WidgetSearch;->searchSuggestions:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/search/WidgetSearch;->searchResults:Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/search/WidgetSearch;->sendQueryFab:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchSuggestions:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchResults:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->sendQueryFab:Landroid/view/View;

    invoke-static {p1}, Lcom/discord/widgets/search/WidgetSearch$Model;->access$200(Lcom/discord/widgets/search/WidgetSearch$Model;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v1, 0x0

    :cond_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/search/WidgetSearch;Landroid/text/Spannable;)Lkotlin/Unit;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/WidgetSearch;->handleInputChanged(Landroid/text/Spannable;)Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic g(Lcom/discord/widgets/search/WidgetSearch;Lcom/discord/widgets/search/WidgetSearch$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/WidgetSearch;->configureUI(Lcom/discord/widgets/search/WidgetSearch$Model;)V

    return-void
.end method

.method private handleInputChanged(Landroid/text/Spannable;)Lkotlin/Unit;
    .locals 1
    .param p1    # Landroid/text/Spannable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearch;->getStoreSearchInput()Lcom/discord/stores/StoreSearchInput;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreSearchInput;->updateInput(Ljava/lang/String;)V

    :cond_0
    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method private static launch(JILandroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "INTENT_EXTRA_TARGET_ID"

    invoke-virtual {v0, v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object p0

    const-string p1, "INTENT_EXTRA_SEARCH_TYPE"

    invoke-virtual {p0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object p0

    const-class p1, Lcom/discord/widgets/search/WidgetSearch;

    invoke-static {p3, p1, p0}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static launchForChannel(JLandroid/content/Context;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0, p2}, Lcom/discord/widgets/search/WidgetSearch;->launch(JILandroid/content/Context;)V

    return-void
.end method

.method public static launchForGuild(JLandroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/discord/widgets/search/WidgetSearch;->launch(JILandroid/content/Context;)V

    return-void
.end method

.method private sendQuery(Landroid/content/Context;)V
    .locals 3

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->hideKeyboard()V

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;

    invoke-direct {v2, p1}, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreSearch;->loadInitial(Ljava/lang/String;Lcom/discord/utilities/search/strings/SearchStringProvider;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0253

    return v0
.end method

.method public synthetic h(Landroid/widget/TextView;Ljava/lang/Integer;Landroid/view/KeyEvent;)Ljava/lang/Boolean;
    .locals 0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    const/4 p3, 0x2

    if-eq p2, p3, :cond_0

    const/4 p3, 0x3

    if-eq p2, p3, :cond_0

    const/4 p3, 0x6

    if-eq p2, p3, :cond_0

    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/WidgetSearch;->sendQuery(Landroid/content/Context;)V

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object p1
.end method

.method public synthetic i(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {v0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    iget-object p1, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-static {p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setSelectionEnd(Lcom/google/android/material/textfield/TextInputLayout;)Lkotlin/Unit;

    return-void
.end method

.method public synthetic j(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/search/WidgetSearch;->sendQuery(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic k(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    iget-object p1, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->hideKeyboard(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSearch;->clear()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a0bd4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    const v0, 0x7f0a0884

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->sendQueryFab:Landroid/view/View;

    const v0, 0x7f0a0bd6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchSuggestions:Landroid/view/View;

    const v0, 0x7f0a0bd5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/search/WidgetSearch;->searchResults:Landroid/view/View;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_TARGET_ID"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetId:J

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_SEARCH_TYPE"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetType:I

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->isRecreated()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->showKeyboard(Landroid/view/View;)V

    :cond_0
    iget v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetType:I

    if-nez v0, :cond_1

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetId:J

    new-instance v2, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/stores/StoreSearch;->initForGuild(JLcom/discord/utilities/search/strings/SearchStringProvider;)V

    goto :goto_0

    :cond_1
    if-ne v0, p1, :cond_2

    invoke-static {}, Lcom/discord/stores/StoreStream;->getSearch()Lcom/discord/stores/StoreSearch;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetId:J

    new-instance v2, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/discord/utilities/search/strings/ContextSearchStringProvider;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/stores/StoreSearch;->initForChannel(JLcom/discord/utilities/search/strings/SearchStringProvider;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget v0, p0, Lcom/discord/widgets/search/WidgetSearch;->targetType:I

    iget-wide v1, p0, Lcom/discord/widgets/search/WidgetSearch;->targetId:J

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/discord/widgets/search/WidgetSearch$Model;->get(IJLandroid/content/Context;)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/d/k;

    invoke-direct {v1, p0}, Lf/a/o/d/k;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    invoke-direct {p0}, Lcom/discord/widgets/search/WidgetSearch;->configureSearchInput()V

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->sendQueryFab:Landroid/view/View;

    new-instance v1, Lf/a/o/d/b;

    invoke-direct {v1, p0}, Lf/a/o/d/b;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/search/WidgetSearch;->searchInput:Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v1, Lf/a/o/d/i;

    invoke-direct {v1, p0}, Lf/a/o/d/i;-><init>(Lcom/discord/widgets/search/WidgetSearch;)V

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setStartIconOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
