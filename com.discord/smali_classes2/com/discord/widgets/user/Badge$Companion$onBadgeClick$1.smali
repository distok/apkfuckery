.class public final Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;
.super Lx/m/c/k;
.source "Badge.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/Badge$Companion;->onBadgeClick(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;)Lkotlin/jvm/functions/Function1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/user/Badge;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $context:Landroid/content/Context;

.field public final synthetic $fragmentManager:Landroidx/fragment/app/FragmentManager;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;->$fragmentManager:Landroidx/fragment/app/FragmentManager;

    iput-object p2, p0, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;->$context:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/user/Badge;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;->invoke(Lcom/discord/widgets/user/Badge;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/user/Badge;)V
    .locals 12

    const-string v0, "badge"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/user/Badge;->getShowPremiumUpSell()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v1, Lf/a/a/e/c;->n:Lf/a/a/e/c$b;

    iget-object v2, p0, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;->$fragmentManager:Landroidx/fragment/app/FragmentManager;

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;->$context:Landroid/content/Context;

    const v4, 0x7f12144e

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/discord/widgets/user/Badge;->getObjectType()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/16 v11, 0x118

    const-string v7, "Profile Modal"

    const-string v8, "Badge"

    invoke-static/range {v1 .. v11}, Lf/a/a/e/c$b;->a(Lf/a/a/e/c$b;Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;I)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;->$context:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/discord/widgets/user/Badge;->getTooltip()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/widgets/user/Badge;->getText()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 p1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xc

    invoke-static {v0, v1, p1, v2, v3}, Lf/a/b/p;->j(Landroid/content/Context;Ljava/lang/CharSequence;ILcom/discord/utilities/view/ToastManager;I)V

    :goto_1
    return-void
.end method
