.class public Lcom/discord/widgets/user/WidgetUserMutualGuilds;
.super Lcom/discord/app/AppFragment;
.source "WidgetUserMutualGuilds.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;,
        Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;
    }
.end annotation


# instance fields
.field private adapter:Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

.field private recycler:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method

.method private getUserId()J
    .locals 4

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_USER_ID"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static show(Landroid/content/Context;J)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.discord.intent.extra.EXTRA_USER_ID"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-class p1, Lcom/discord/widgets/user/WidgetUserMutualGuilds;

    invoke-static {p0, p1, v0}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public configureUI(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;)V
    .locals 5

    invoke-static {p1}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;->access$000(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;)I

    move-result v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f10011b

    const v4, 0x7f1210bd

    invoke-static {v1, v3, v4, v0, v2}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-static {p1}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;->access$100(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;)Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds;->adapter:Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

    invoke-static {p1}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;->access$200(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02c6

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a0afd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds;->recycler:Landroidx/recyclerview/widget/RecyclerView;

    new-instance p1, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds;->recycler:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lf/a/o/g/q;

    invoke-direct {v1, p0}, Lf/a/o/g/q;-><init>(Lcom/discord/widgets/user/WidgetUserMutualGuilds;)V

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lrx/functions/Action0;)V

    invoke-static {p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds;->adapter:Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 3

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserMutualGuilds;->getUserId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model;->get(J)Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/g/b;

    invoke-direct {v1, p0}, Lf/a/o/g/b;-><init>(Lcom/discord/widgets/user/WidgetUserMutualGuilds;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
