.class public final Lcom/discord/widgets/user/WidgetUserMentionsViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "WidgetUserMentionsViewModel.kt"


# instance fields
.field private model:Lcom/discord/widgets/user/WidgetUserMentions$Model;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final getModel$app_productionDiscordExternalRelease()Lcom/discord/widgets/user/WidgetUserMentions$Model;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMentionsViewModel;->model:Lcom/discord/widgets/user/WidgetUserMentions$Model;

    return-object v0
.end method

.method public final setModel$app_productionDiscordExternalRelease(Lcom/discord/widgets/user/WidgetUserMentions$Model;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMentionsViewModel;->model:Lcom/discord/widgets/user/WidgetUserMentions$Model;

    return-void
.end method
