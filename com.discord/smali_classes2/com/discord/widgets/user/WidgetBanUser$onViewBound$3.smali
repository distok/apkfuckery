.class public final Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;
.super Ljava/lang/Object;
.source "WidgetBanUser.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/WidgetBanUser;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic $userId:J

.field public final synthetic $userName:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/user/WidgetBanUser;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/WidgetBanUser;JJLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->this$0:Lcom/discord/widgets/user/WidgetBanUser;

    iput-wide p2, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->$guildId:J

    iput-wide p4, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->$userId:J

    iput-object p6, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->$userName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 11

    iget-object p1, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->this$0:Lcom/discord/widgets/user/WidgetBanUser;

    invoke-static {p1}, Lcom/discord/widgets/user/WidgetBanUser;->access$getHistoryRadios$p(Lcom/discord/widgets/user/WidgetBanUser;)Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->this$0:Lcom/discord/widgets/user/WidgetBanUser;

    invoke-static {v0}, Lcom/discord/widgets/user/WidgetBanUser;->access$getDeleteHistoryRadioManager$p(Lcom/discord/widgets/user/WidgetBanUser;)Lcom/discord/views/RadioManager;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/views/RadioManager;->b()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    invoke-virtual {p1}, Landroid/widget/RelativeLayout;->getId()I

    move-result p1

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 p1, 0x0

    goto :goto_1

    :pswitch_1
    const/4 p1, 0x7

    goto :goto_1

    :pswitch_2
    const/4 p1, 0x1

    :goto_1
    iget-object v2, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->this$0:Lcom/discord/widgets/user/WidgetBanUser;

    invoke-static {v2}, Lcom/discord/widgets/user/WidgetBanUser;->access$getReasonEditText$p(Lcom/discord/widgets/user/WidgetBanUser;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v3, v0

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    goto :goto_2

    :cond_1
    move-object v2, v4

    :goto_2
    sget-object v3, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v3}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v5

    iget-wide v6, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->$guildId:J

    iget-wide v8, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->$userId:J

    new-instance v10, Lcom/discord/restapi/RestAPIParams$BanGuildMember;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v10, p1, v2}, Lcom/discord/restapi/RestAPIParams$BanGuildMember;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual/range {v5 .. v10}, Lcom/discord/utilities/rest/RestAPI;->banGuildMember(JJLcom/discord/restapi/RestAPIParams$BanGuildMember;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v1, v0, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->this$0:Lcom/discord/widgets/user/WidgetBanUser;

    const/4 v1, 0x2

    invoke-static {p1, v0, v4, v1, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3$1;-><init>(Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;)V

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetBanUser$onViewBound$3;->this$0:Lcom/discord/widgets/user/WidgetBanUser;

    invoke-static {v0, v1}, Lf/a/b/r;->l(Lrx/functions/Action1;Lcom/discord/app/AppDialog;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a00ed
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
