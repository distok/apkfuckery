.class public Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;
.super Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;
.source "WidgetUserPhoneVerify.java"


# static fields
.field private static final INTENT_EXTRA_PHONE_NUMBER:Ljava/lang/String; = "INTENT_EXTRA_PHONE_NUMBER"


# instance fields
.field private close:Landroid/view/View;

.field private digitVerificationView:Lcom/discord/views/CodeVerificationView;

.field private dimmerView:Lcom/discord/utilities/dimmer/DimmerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;-><init>()V

    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->handleCodeEntered(Ljava/lang/String;)Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method private handleCodeEntered(Ljava/lang/String;)Lkotlin/Unit;
    .locals 3

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_PHONE_NUMBER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/discord/utilities/rest/RestAPI;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    new-instance v2, Lcom/discord/restapi/RestAPIParams$VerificationCode;

    invoke-direct {v2, v0, p1}, Lcom/discord/restapi/RestAPIParams$VerificationCode;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/rest/RestAPI;->phoneVerificationsVerify(Lcom/discord/restapi/RestAPIParams$VerificationCode;)Lrx/Observable;

    move-result-object p1

    invoke-static {}, Lf/a/b/r;->e()Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->dimmerView:Lcom/discord/utilities/dimmer/DimmerView;

    invoke-static {v0}, Lf/a/b/r;->q(Lcom/discord/utilities/dimmer/DimmerView;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lf/a/o/g/v/c;

    invoke-direct {v0, p0}, Lf/a/o/g/v/c;-><init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lf/a/o/g/v/a;

    invoke-direct {v2, p0}, Lf/a/o/g/v/a;-><init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;)V

    invoke-static {v0, v1, v2}, Lf/a/b/r;->k(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method private handleCodeReceived(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/discord/widgets/user/WidgetUserPasswordVerify;->startUpdatePhoneNumber(Lcom/discord/app/AppFragment;Ljava/lang/String;)V

    return-void
.end method

.method public static launch(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v0, v1, v1}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getLaunchIntent(Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;ZZZ)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;->NO_HISTORY_FROM_USER_SETTINGS:Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    if-ne p1, v1, :cond_0

    const/high16 p1, 0x40000000    # 2.0f

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string p1, "INTENT_EXTRA_PHONE_NUMBER"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-class p1, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;

    invoke-static {p0, p1, v0}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static launchForResult(Landroidx/fragment/app/Fragment;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v0, v1, v1}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getLaunchIntent(Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;ZZZ)Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;->NO_HISTORY_FROM_USER_SETTINGS:Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    if-ne p1, v1, :cond_0

    const/high16 p1, 0x40000000    # 2.0f

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string p1, "INTENT_EXTRA_PHONE_NUMBER"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-class p1, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;

    invoke-static {p0, p1, v0, p3}, Lf/a/b/m;->f(Landroidx/fragment/app/Fragment;Ljava/lang/Class;Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public synthetic g(Lcom/discord/models/domain/ModelPhoneVerificationToken;)V
    .locals 0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPhoneVerificationToken;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->handleCodeReceived(Ljava/lang/String;)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02c9

    return v0
.end method

.method public synthetic h(Lcom/discord/utilities/error/Error;)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->digitVerificationView:Lcom/discord/views/CodeVerificationView;

    invoke-virtual {p1}, Lcom/discord/views/CodeVerificationView;->b()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    invoke-static {p1, p2}, Lcom/discord/widgets/user/WidgetUserPasswordVerify;->saveCompletedSuccessfully(II)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/4 p2, -0x1

    invoke-virtual {p1, p2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getMode()Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;->NO_HISTORY_FROM_USER_SETTINGS:Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    if-ne p1, p2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const/4 p2, 0x0

    sget-object p3, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Redirect;->SMS_BACKUP:Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Redirect;

    invoke-static {p1, p2, p3}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->launch(Landroid/content/Context;ZLcom/discord/widgets/settings/account/WidgetSettingsAccount$Redirect;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->isForced()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lf/a/b/m;->a(Landroid/content/Context;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a0b02

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CodeVerificationView;

    iput-object v0, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->digitVerificationView:Lcom/discord/views/CodeVerificationView;

    const v0, 0x7f0a035a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    iput-object v0, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->dimmerView:Lcom/discord/utilities/dimmer/DimmerView;

    const v0, 0x7f0a02ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->close:Landroid/view/View;

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->digitVerificationView:Lcom/discord/views/CodeVerificationView;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->digitVerificationView:Lcom/discord/views/CodeVerificationView;

    new-instance v0, Lf/a/o/g/v/b;

    invoke-direct {v0, p0}, Lf/a/o/g/v/b;-><init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;)V

    invoke-virtual {p1, v0}, Lcom/discord/views/CodeVerificationView;->setOnCodeEntered(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->close:Landroid/view/View;

    invoke-virtual {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->isForced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->close:Landroid/view/View;

    new-instance v0, Lf/a/o/g/v/d;

    invoke-direct {v0, p0}, Lf/a/o/g/v/d;-><init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
