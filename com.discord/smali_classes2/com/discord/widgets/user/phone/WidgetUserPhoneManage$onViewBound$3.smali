.class public final Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;
.super Ljava/lang/Object;
.source "WidgetUserPhoneManage.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    const-string v0, "v"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Phone;

    iget-object v2, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v2}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->access$phoneNumberWithCountryCode(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/discord/restapi/RestAPIParams$Phone;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/rest/RestAPI;->userAddPhone(Lcom/discord/restapi/RestAPIParams$Phone;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->access$getDimmer$p(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object v1

    const-wide/16 v4, 0x0

    const/4 v2, 0x2

    invoke-static {v1, v4, v5, v2}, Lf/a/b/r;->r(Lcom/discord/utilities/dimmer/DimmerView;JI)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    const-string v1, "api\n          .userAddPh\u2026mpose(withDimmer(dimmer))"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v0, v1, v3, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;-><init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;Landroid/view/View;)V

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v2, 0x4

    invoke-static {v1, p1, v3, v2}, Lf/a/b/r;->n(Lrx/functions/Action1;Landroid/content/Context;Lrx/functions/Action1;I)Lrx/Observable$c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
