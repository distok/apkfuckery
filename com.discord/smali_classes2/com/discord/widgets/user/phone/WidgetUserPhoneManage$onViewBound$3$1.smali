.class public final Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;
.super Ljava/lang/Object;
.source "WidgetUserPhoneManage.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $v:Landroid/view/View;

.field public final synthetic this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;

    iput-object p2, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->call(Ljava/lang/Void;)V

    return-void
.end method

.method public final call(Ljava/lang/Void;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;

    iget-object p1, p1, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v0, "requireActivity()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;

    iget-object p1, p1, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {p1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->access$getMode$p(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;

    iget-object v1, v1, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->access$phoneNumberWithCountryCode(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xfa1

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->launchForResult(Landroidx/fragment/app/Fragment;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->$v:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;

    iget-object v0, v0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v0}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->access$getMode$p(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3$1;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;

    iget-object v1, v1, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage$onViewBound$3;->this$0:Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;

    invoke-static {v1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;->access$phoneNumberWithCountryCode(Lcom/discord/widgets/user/phone/WidgetUserPhoneManage;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/discord/widgets/user/phone/WidgetUserPhoneVerify;->launch(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
