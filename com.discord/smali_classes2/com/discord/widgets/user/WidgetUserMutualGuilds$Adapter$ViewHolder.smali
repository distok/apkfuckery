.class public Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetUserMutualGuilds.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;",
        "Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic a:I


# instance fields
.field private serverImage:Landroid/widget/ImageView;

.field private serverName:Landroid/widget/TextView;

.field private serverNick:Landroid/widget/TextView;

.field private serverText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(ILcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0b0d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverImage:Landroid/widget/ImageView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0b10

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverText:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0b0e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverName:Landroid/widget/TextView;

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v0, 0x7f0a0b0f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverNick:Landroid/widget/TextView;

    new-instance p1, Lf/a/o/g/o;

    invoke-direct {p1, p2}, Lf/a/o/g/o;-><init>(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter;)V

    const/4 p2, 0x0

    new-array p2, p2, [Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;)V
    .locals 4

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    invoke-static {p2}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;->access$300(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;)Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const/16 v3, 0x8

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverImage:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/discord/utilities/icon/IconUtils;->setIcon(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelGuild;)V

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverText:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverName:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverNick:Landroid/widget/TextView;

    if-eqz p1, :cond_6

    invoke-static {p2}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;->access$400(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->serverNick:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;->access$400(Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_5

    const/4 v1, 0x0

    :cond_5
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/WidgetUserMutualGuilds$Adapter$ViewHolder;->onConfigure(ILcom/discord/widgets/user/WidgetUserMutualGuilds$Model$Item;)V

    return-void
.end method
