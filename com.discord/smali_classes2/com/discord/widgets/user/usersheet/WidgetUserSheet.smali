.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetUserSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;,
        Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_CHANNEL_ID:Ljava/lang/String; = "ARG_CHANNEL_ID"

.field private static final ARG_FRIEND_TOKEN:Ljava/lang/String; = "ARG_FRIEND_TOKEN"

.field private static final ARG_GUILD_ID:Ljava/lang/String; = "ARG_GUILD_ID"

.field private static final ARG_IS_VOICE_CONTEXT:Ljava/lang/String; = "ARG_IS_VOICE_CONTEXT"

.field private static final ARG_STREAM_PREVIEW_CLICK_BEHAVIOR:Ljava/lang/String; = "ARG_STREAM_PREVIEW_CLICK_BEHAVIOR"

.field private static final ARG_USER_ID:Ljava/lang/String; = "ARG_USER_ID"

.field public static final Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

.field private static final REQUEST_CODE_MOVE_USER:I = 0xfa0


# instance fields
.field private activityViewHolder:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

.field private final addFriendActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final callActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final connectionsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final copyIdButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final developerHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final friendRequestAcceptButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final friendRequestIgnoreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final incomingFriendRequestContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final incomingFriendRequestHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final messageActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final moreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noteHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noteTextFieldWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final pendingFriendRequestActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

.field private final profileActionsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final profileActionsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final richPresenceContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rolesList$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final sheetLoadingContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userProfileAdminCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userProfileAdminView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userProfileConnectionsView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userProfileHeaderView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userProfileVoiceSettingsView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final videoActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

.field private final voiceSettingsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x1c

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v3, "sheetLoadingContainer"

    const-string v4, "getSheetLoadingContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "moreButton"

    const-string v7, "getMoreButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "userProfileHeaderView"

    const-string v7, "getUserProfileHeaderView()Lcom/discord/widgets/user/profile/UserProfileHeaderView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "profileActionsDivider"

    const-string v7, "getProfileActionsDivider()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "profileActionsContainer"

    const-string v7, "getProfileActionsContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "messageActionButton"

    const-string v7, "getMessageActionButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "callActionButton"

    const-string v7, "getCallActionButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "videoActionButton"

    const-string v7, "getVideoActionButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "addFriendActionButton"

    const-string v7, "getAddFriendActionButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "pendingFriendRequestActionButton"

    const-string v7, "getPendingFriendRequestActionButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "incomingFriendRequestHeader"

    const-string v7, "getIncomingFriendRequestHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "incomingFriendRequestContainer"

    const-string v7, "getIncomingFriendRequestContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "friendRequestIgnoreButton"

    const-string v7, "getFriendRequestIgnoreButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "friendRequestAcceptButton"

    const-string v7, "getFriendRequestAcceptButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "richPresenceContainer"

    const-string v7, "getRichPresenceContainer()Landroid/widget/FrameLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "rolesList"

    const-string v7, "getRolesList()Lcom/discord/widgets/roles/RolesListView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "guildContainer"

    const-string v7, "getGuildContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x11

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "guildHeader"

    const-string v7, "getGuildHeader()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x12

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "voiceSettingsHeader"

    const-string v7, "getVoiceSettingsHeader()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x13

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "userProfileVoiceSettingsView"

    const-string v7, "getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x14

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "connectionsHeader"

    const-string v7, "getConnectionsHeader()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x15

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "userProfileConnectionsView"

    const-string v7, "getUserProfileConnectionsView()Lcom/discord/widgets/user/profile/UserProfileConnectionsView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x16

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "noteTextFieldWrap"

    const-string v7, "getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x17

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "noteHeader"

    const-string v7, "getNoteHeader()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x18

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "userProfileAdminCard"

    const-string v7, "getUserProfileAdminCard()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x19

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "userProfileAdminView"

    const-string v7, "getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "developerHeader"

    const-string v7, "getDeveloperHeader()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const-string v6, "copyIdButton"

    const-string v7, "getCopyIdButton()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0b37

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->sheetLoadingContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b39

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->moreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b40

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileHeaderView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b3f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->profileActionsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b3e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->profileActionsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b38

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->messageActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b29

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->callActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b44

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->videoActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b25

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->addFriendActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b3d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->pendingFriendRequestActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b36

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->incomingFriendRequestHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b32

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->incomingFriendRequestContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b31

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->friendRequestIgnoreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b30

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->friendRequestAcceptButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a082b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->richPresenceContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b43

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->rolesList$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b33

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b34

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b41

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->voiceSettingsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b42

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileVoiceSettingsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b2a

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->connectionsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b2b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileConnectionsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b3c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->noteTextFieldWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b3b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->noteHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b26

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileAdminCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b27

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileAdminView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b2f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->developerHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b2d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->copyIdButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method private final acceptFriendRequest(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const v2, 0x7f120023

    invoke-virtual {v0, v1, p1, v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->addRelationship(Ljava/lang/Integer;Ljava/lang/String;I)V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public static final synthetic access$acceptFriendRequest(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->acceptFriendRequest(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$addFriend(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->addFriend(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureUI(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getCopyIdButton$p(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getCopyIdButton()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getNoteTextFieldWrap$p(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$ignoreFriendRequest(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->ignoreFriendRequest()V

    return-void
.end method

.method public static final synthetic access$onStreamPreviewClicked(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    return-void
.end method

.method private final addFriend(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const v2, 0x7f120859

    invoke-virtual {v0, v1, p1, v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->addRelationship(Ljava/lang/Integer;Ljava/lang/String;I)V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final configureConnectionsSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getConnectionsViewState()Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getConnectionsHeader()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;->getShowConnectionsSection()Z

    move-result v2

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileConnectionsView()Lcom/discord/widgets/user/profile/UserProfileConnectionsView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;->getShowConnectionsSection()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureConnectionsSection$onConnectedAccountClick$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureConnectionsSection$onConnectedAccountClick$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureConnectionsSection$onMutualGuildsItemClick$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureConnectionsSection$onMutualGuildsItemClick$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelUser;)V

    new-instance v3, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureConnectionsSection$onMutualFriendsItemClick$1;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureConnectionsSection$onMutualFriendsItemClick$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/models/domain/ModelUser;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileConnectionsView()Lcom/discord/widgets/user/profile/UserProfileConnectionsView;

    move-result-object p1

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/widgets/user/profile/UserProfileConnectionsView;->updateViewState(Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final configureDeveloperSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getCopyIdButton()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureDeveloperSection$$inlined$apply$lambda$1;

    invoke-direct {v1, v0, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureDeveloperSection$$inlined$apply$lambda$1;-><init>(Landroid/view/View;Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getCopyIdButton()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getDeveloperHeader()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_2

    const/4 v2, 0x0

    :cond_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureGuildSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 8

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getRoleItems()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getAdminViewState()Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    xor-int/2addr v2, v3

    const/4 v4, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;->isAdminSectionEnabled()Z

    move-result v5

    if-ne v5, v3, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getGuildContainer()Landroid/view/ViewGroup;

    move-result-object v6

    if-nez v2, :cond_2

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :cond_2
    :goto_1
    const/16 v7, 0x8

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    goto :goto_2

    :cond_3
    const/16 v3, 0x8

    :goto_2
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRolesList()Lcom/discord/widgets/roles/RolesListView;

    move-result-object v3

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    goto :goto_3

    :cond_4
    const/16 v2, 0x8

    :goto_3
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRolesList()Lcom/discord/widgets/roles/RolesListView;

    move-result-object v2

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRolesList()Lcom/discord/widgets/roles/RolesListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f040495

    invoke-static {v3, v6}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/discord/widgets/roles/RolesListView;->updateView(Ljava/util/List;I)V

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getGuildSectionHeaderText()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getGuildHeader()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminCard()Landroid/view/View;

    move-result-object p1

    if-eqz v5, :cond_6

    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    const/16 v0, 0x8

    :goto_4
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    if-eqz v5, :cond_7

    goto :goto_5

    :cond_7
    const/16 v4, 0x8

    :goto_5
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->updateView(Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;)V

    :cond_8
    return-void
.end method

.method private final configureIncomingFriendRequest(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUserRelationshipType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelUserRelationship;->getType(Ljava/lang/Integer;)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getIncomingFriendRequestHeader()Landroid/widget/TextView;

    move-result-object v2

    const/16 v3, 0x8

    if-eqz v0, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getIncomingFriendRequestContainer()Landroid/view/ViewGroup;

    move-result-object v2

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getFriendRequestAcceptButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureIncomingFriendRequest$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureIncomingFriendRequest$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getFriendRequestIgnoreButton()Landroid/widget/Button;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureIncomingFriendRequest$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureIncomingFriendRequest$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureNote(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUserNote()Lcom/discord/stores/StoreUserNotes$UserNoteState;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/stores/StoreUserNotes$UserNoteState$Empty;

    const/4 v2, 0x1

    const-string v3, ""

    const v4, 0x7f121134

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-static {p1, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/google/android/material/textfield/TextInputLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loading;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    const v0, 0x7f120f45

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-static {p1, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    instance-of v0, v0, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUserNote()Lcom/discord/stores/StoreUserNotes$UserNoteState;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;->getNote()Lcom/discord/models/domain/ModelUserNote;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserNote;->getNote()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/google/android/material/textfield/TextInputLayout;->setEnabled(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final configureProfileActionButtons(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 9

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->isMe()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isBot()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUserRelationshipType()I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getProfileActionsDivider()Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    if-eqz v2, :cond_1

    const/4 v8, 0x0

    goto :goto_1

    :cond_1
    const/16 v8, 0x8

    :goto_1
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getProfileActionsContainer()Landroid/view/ViewGroup;

    move-result-object v6

    if-eqz v2, :cond_2

    const/4 v8, 0x0

    goto :goto_2

    :cond_2
    const/16 v8, 0x8

    :goto_2
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    if-nez v2, :cond_3

    return-void

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getMessageActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v6

    if-eq v6, v4, :cond_4

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 v6, 0x1

    :goto_4
    if-eqz v6, :cond_6

    const/4 v6, 0x0

    goto :goto_5

    :cond_6
    const/16 v6, 0x8

    :goto_5
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getMessageActionButton()Landroid/widget/Button;

    move-result-object v2

    new-instance v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$1;

    invoke-direct {v6, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getCallActionButton()Landroid/widget/Button;

    move-result-object v2

    new-instance v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getVideoActionButton()Landroid/widget/Button;

    move-result-object v2

    new-instance v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$3;

    invoke-direct {v6, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$3;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v3, :cond_8

    const/4 v2, 0x3

    if-eq v3, v2, :cond_7

    const/4 v2, 0x4

    if-eq v3, v2, :cond_7

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAddFriendActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPendingFriendRequestActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_6

    :cond_7
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAddFriendActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPendingFriendRequestActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_6

    :cond_8
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAddFriendActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPendingFriendRequestActionButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    :goto_6
    if-nez v0, :cond_9

    if-nez v1, :cond_9

    if-nez v3, :cond_9

    goto :goto_7

    :cond_9
    const/4 v4, 0x0

    :goto_7
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAddFriendActionButton()Landroid/widget/Button;

    move-result-object v0

    if-eqz v4, :cond_a

    goto :goto_8

    :cond_a
    const/16 v5, 0x8

    :goto_8
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getAddFriendActionButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$4;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$4;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getPendingFriendRequestActionButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$5;

    invoke-direct {v1, p0, v3, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureProfileActionButtons$5;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;ILcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V
    .locals 13

    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Uninitialized;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getSheetLoadingContainer()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getSheetLoadingContainer()Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    move-object v0, p1

    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v4

    :goto_0
    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->isMe()Z

    move-result v5

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v6

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getMoreButton()Landroid/view/View;

    move-result-object v7

    if-nez v5, :cond_2

    if-nez v6, :cond_2

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v7, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getConnectionsViewState()Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;->getShowConnectionsSection()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getConnectionsHeader()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/discord/app/AppBottomSheet;->setPeekHeightBottomView(Landroid/view/View;)V

    :cond_4
    sget-object v1, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->Companion:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getRichPresenceContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v5

    iget-object v6, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    invoke-virtual {v1, v2, v3, v5, v6}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;->setRichPresence(Landroid/view/ViewGroup;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;)Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getRichPresence()Lcom/discord/widgets/user/presence/ModelRichPresence;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v9

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/app/AppActivity;->c()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->isMe()Z

    move-result v11

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v12

    move-object v7, v1

    invoke-virtual/range {v7 .. v12}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureUi(Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Landroid/content/Context;ZLcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v1, v3, p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureUiTimestamp(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/app/AppComponent;)V

    iput-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    instance-of v2, v1, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;

    if-nez v2, :cond_5

    move-object v1, v4

    :cond_5
    check-cast v1, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v2

    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$2;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;)V

    invoke-virtual {v1, v2}, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->setOnStreamPreviewClicked(Lkotlin/jvm/functions/Function0;)V

    :cond_6
    iget-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    if-eqz p1, :cond_8

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$3;

    iget-object v2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-eqz v2, :cond_7

    invoke-direct {v1, v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$configureUI$3;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    invoke-virtual {p1, v1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->setOnActivityCustomButtonClicked(Lkotlin/jvm/functions/Function5;)V

    goto :goto_3

    :cond_7
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4

    :cond_8
    :goto_3
    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureProfileActionButtons(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureIncomingFriendRequest(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureVoiceSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureGuildSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureConnectionsSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureNote(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->configureDeveloperSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V

    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_9
    move-object v0, v4

    :goto_4
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/discord/models/domain/activity/ModelActivity;->getGamePlatform()Ljava/lang/String;

    move-result-object v4

    :cond_a
    invoke-virtual {p1, v0, v4}, Lcom/discord/utilities/analytics/AnalyticsTracker;->openUserSheet(Ljava/lang/String;Ljava/lang/String;)V

    :goto_5
    return-void

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final configureVoiceSection(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getVoiceSettingsViewState()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getShowVoiceSettings()Z

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getVoiceSettingsHeader()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eqz p1, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;

    move-result-object v1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;->updateView(Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;)V

    return-void
.end method

.method private final getAddFriendActionButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->addFriendActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getCallActionButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->callActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getConnectionsHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->connectionsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCopyIdButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->copyIdButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1b

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDeveloperHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->developerHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1a

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFriendRequestAcceptButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->friendRequestAcceptButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getFriendRequestIgnoreButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->friendRequestIgnoreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getGuildContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getGuildHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->guildHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getIncomingFriendRequestContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->incomingFriendRequestContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getIncomingFriendRequestHeader()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->incomingFriendRequestHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMessageActionButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->messageActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getMoreButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->moreButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNoteHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->noteHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->noteTextFieldWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getPendingFriendRequestActionButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->pendingFriendRequestActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getProfileActionsContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->profileActionsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getProfileActionsDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->profileActionsDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRichPresenceContainer()Landroid/widget/FrameLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->richPresenceContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private final getRolesList()Lcom/discord/widgets/roles/RolesListView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->rolesList$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/roles/RolesListView;

    return-object v0
.end method

.method private final getSheetLoadingContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->sheetLoadingContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getUserProfileAdminCard()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileAdminCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileAdminView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/user/profile/UserProfileAdminView;

    return-object v0
.end method

.method private final getUserProfileConnectionsView()Lcom/discord/widgets/user/profile/UserProfileConnectionsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileConnectionsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x15

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/user/profile/UserProfileConnectionsView;

    return-object v0
.end method

.method private final getUserProfileHeaderView()Lcom/discord/widgets/user/profile/UserProfileHeaderView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileHeaderView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    return-object v0
.end method

.method private final getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->userProfileVoiceSettingsView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;

    return-object v0
.end method

.method private final getVideoActionButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->videoActionButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getVoiceSettingsHeader()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->voiceSettingsHeader$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final handleBanUser(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/user/WidgetBanUser;->Companion:Lcom/discord/widgets/user/WidgetBanUser$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;->getGuildId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;->getUserId()J

    move-result-wide v4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/user/WidgetBanUser$Companion;->launch(Ljava/lang/String;JJLandroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method private final handleDismissSheet()V
    .locals 0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method

.method private final handleEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;)V
    .locals 2

    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleShowToast(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;)V

    goto/16 :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleShowFriendRequestErrorToast(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;)V

    goto/16 :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleLaunchVoiceCall(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleLaunchVideoCall(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleLaunchSpectate(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;)V

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleLaunchEditMember(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;)V

    goto :goto_0

    :cond_5
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleKickUser(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;)V

    goto :goto_0

    :cond_6
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleBanUser(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;)V

    goto :goto_0

    :cond_7
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;

    if-eqz v0, :cond_8

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleMoveUser(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;)V

    goto :goto_0

    :cond_8
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;

    if-eqz v0, :cond_9

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleRequestPermissionsForSpectateStream(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;)V

    goto :goto_0

    :cond_9
    instance-of v0, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$UserNotFound;

    if-eqz v0, :cond_a

    const p1, 0x7f121965

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleDismissSheet()V

    goto :goto_0

    :cond_a
    instance-of p1, p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$DismissSheet;

    if-eqz p1, :cond_b

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->handleDismissSheet()V

    :goto_0
    return-void

    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleKickUser(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/user/WidgetKickUser;->Companion:Lcom/discord/widgets/user/WidgetKickUser$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;->getGuildId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;->getUserId()J

    move-result-wide v4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v6

    const-string p1, "parentFragmentManager"

    invoke-static {v6, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/user/WidgetKickUser$Companion;->launch(Ljava/lang/String;JJLandroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method private final handleLaunchEditMember(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;)V
    .locals 4

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;->getGuildId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;->getUserId()J

    move-result-wide v2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {v0, v1, v2, v3, p1}, Lcom/discord/widgets/servers/WidgetServerSettingsEditMember;->launch(JJLandroid/app/Activity;)V

    return-void
.end method

.method private final handleLaunchSpectate(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;)V
    .locals 8

    sget-object v0, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method

.method private final handleLaunchVideoCall(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVideoCall(J)V

    return-void

    :cond_0
    const-string p1, "privateCallLauncher"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final handleLaunchVoiceCall(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;->getChannelId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVoiceCall(J)V

    return-void

    :cond_0
    const-string p1, "privateCallLauncher"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final handleMoveUser(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;)V
    .locals 9

    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;->getGuildId()J

    move-result-wide v2

    const/16 v4, 0xfa0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->launchForVoice$default(Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;Landroidx/fragment/app/Fragment;JIZIILjava/lang/Object;)V

    return-void
.end method

.method private final handleRequestPermissionsForSpectateStream(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;)V
    .locals 1

    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$handleRequestPermissionsForSpectateStream$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$handleRequestPermissionsForSpectateStream$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;)V

    invoke-virtual {p0, v0}, Lcom/discord/app/AppBottomSheet;->requestMicrophone(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final handleShowFriendRequestErrorToast(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;)V
    .locals 3

    sget-object v0, Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;->INSTANCE:Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;->getAbortCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;->getUsername()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/utilities/rest/RestAPIAbortMessages$ResponseResolver;->getRelationshipResponse(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->l(Landroidx/fragment/app/Fragment;Ljava/lang/CharSequence;II)V

    return-void
.end method

.method private final handleShowToast(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;)V
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;->getStringRes()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    return-void
.end method

.method private final ignoreFriendRequest()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-eqz v0, :cond_0

    const v1, 0x7f120852

    invoke-virtual {v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->removeRelationship(I)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public static final show(JLandroidx/fragment/app/FragmentManager;)V
    .locals 11

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7a

    const/4 v10, 0x0

    move-wide v1, p0

    move-object v4, p2

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;)V
    .locals 11

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x78

    const/4 v10, 0x0

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;)V
    .locals 11

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x70

    const/4 v10, 0x0

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 11

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x60

    const/4 v10, 0x0

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;)V
    .locals 11

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    const/4 v8, 0x0

    const/16 v9, 0x40

    const/4 v10, 0x0

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show$default(Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;)V
    .locals 9

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$Companion;->show(JLjava/lang/Long;Landroidx/fragment/app/FragmentManager;Ljava/lang/Long;Ljava/lang/Boolean;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
    .locals 18

    move-object/from16 v7, p0

    const-string v0, "compositeSubscription"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CHANNEL_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const-string v3, "viewModel"

    const/4 v6, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v2

    const/4 v8, 0x2

    invoke-static {v2, v7, v6, v8, v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v9

    const-class v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v15, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$1;

    invoke-direct {v15, v7}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    const/16 v16, 0x1e

    const/16 v17, 0x0

    invoke-static/range {v9 .. v17}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v2, v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->observeEvents()Lrx/Observable;

    move-result-object v2

    invoke-static {v2, v7, v6, v8, v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v9

    const-class v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v15, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$2;

    invoke-direct {v15, v7}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$bindSubscriptions$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    const/16 v16, 0x1e

    const/16 v17, 0x0

    invoke-static/range {v9 .. v17}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object v2, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->Companion:Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileHeaderView()Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v0, v2

    move-object v1, v3

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;->bind(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Landroidx/fragment/app/Fragment;Lcom/discord/app/AppComponent;JLjava/lang/Long;)V

    return-void

    :cond_0
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v6
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02d0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p2, 0xfa0

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/channels/WidgetChannelSelector;->Companion:Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;

    new-instance v3, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onActivityResult$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onActivityResult$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move v1, p1

    move-object v2, p3

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;->handleResult$default(Lcom/discord/widgets/channels/WidgetChannelSelector$Companion;ILandroid/content/Intent;Lkotlin/jvm/functions/Function3;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    const-string v0, "inflater"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CHANNEL_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "ARG_IS_VOICE_CONTEXT"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "ARG_STREAM_PREVIEW_CLICK_BEHAVIOR"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    const-string v5, "null cannot be cast to non-null type com.discord.widgets.user.usersheet.WidgetUserSheet.StreamPreviewClickBehavior"

    invoke-static {v2, v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v7, v2

    check-cast v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "ARG_FRIEND_TOKEN"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Landroidx/lifecycle/ViewModelProvider;

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object v2, v10

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;-><init>(JLjava/lang/Long;ZLcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;)V

    invoke-direct {v9, p0, v10}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    invoke-virtual {v9, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    new-instance v0, Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;-><init>(Lcom/discord/app/AppPermissions$Requests;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V

    iput-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->privateCallLauncher:Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppBottomSheet;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onPause()V
    .locals 7

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->activityViewHolder:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeSubscriptions()V

    :cond_0
    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_GUILD_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-lez v6, :cond_1

    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getGuildSubscriptions()Lcom/discord/stores/StoreGuildSubscriptions;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/discord/stores/StoreGuildSubscriptions;->unsubscribeUser(JJ)V

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/discord/app/AppBottomSheet;->hideKeyboard(Landroid/view/View;)V

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->viewModel:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->updateUserNote(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onPause()V

    return-void

    :cond_2
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onResume()V
    .locals 7

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, v0}, Lcom/discord/app/AppBottomSheet;->hideKeyboard$default(Lcom/discord/app/AppBottomSheet;Landroid/view/View;ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_USER_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_GUILD_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-lez v6, :cond_0

    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    sget-object v4, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v4}, Lcom/discord/stores/StoreStream$Companion;->getGuildSubscriptions()Lcom/discord/stores/StoreGuildSubscriptions;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0, v1}, Lcom/discord/stores/StoreGuildSubscriptions;->subscribeUser(JJ)V

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "ARG_USER_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "ARG_CHANNEL_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getMoreButton()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$1;

    move-object v0, p2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;JJ)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileHeaderView()Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/user/Badge;->Companion:Lcom/discord/widgets/user/Badge$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0, v1}, Lcom/discord/widgets/user/Badge$Companion;->onBadgeClick(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->setOnBadgeClick(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;->setOnMuteChecked(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$3;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$3;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;->setOnDeafenChecked(Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileVoiceSettingsView()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$4;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$4;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView;->setOnVolumeChange(Lkotlin/jvm/functions/Function2;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$5;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$5;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnEditMember(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$6;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$6;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnKick(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$7;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$7;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnBan(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$8;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$8;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnServerMute(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$9;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$9;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnServerDeafen(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$10;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$10;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnServerMove(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getUserProfileAdminView()Lcom/discord/widgets/user/profile/UserProfileAdminView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$11;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$11;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserProfileAdminView;->setOnDisconnect(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$12;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$12;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-static {p1, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditTextFocusChangeListener(Lcom/google/android/material/textfield/TextInputLayout;Landroid/view/View$OnFocusChangeListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteTextFieldWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$13;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$13;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, p2, v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnImeActionDone$default(Lcom/google/android/material/textfield/TextInputLayout;ZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getMoreButton()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$14;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$14;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheet;)V

    invoke-static {p1, p2}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    const/4 p1, 0x6

    new-array p1, p1, [Landroid/view/View;

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getConnectionsHeader()Landroid/view/View;

    move-result-object p2

    aput-object p2, p1, v0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getGuildHeader()Landroid/widget/TextView;

    move-result-object p2

    aput-object p2, p1, v1

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getNoteHeader()Landroid/view/View;

    move-result-object p2

    const/4 v0, 0x2

    aput-object p2, p1, v0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getDeveloperHeader()Landroid/view/View;

    move-result-object p2

    const/4 v0, 0x3

    aput-object p2, p1, v0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getIncomingFriendRequestHeader()Landroid/widget/TextView;

    move-result-object p2

    const/4 v0, 0x4

    aput-object p2, p1, v0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet;->getVoiceSettingsHeader()Landroid/view/View;

    move-result-object p2

    const/4 v0, 0x5

    aput-object p2, p1, v0

    invoke-static {p1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$15;

    invoke-direct {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheet$onViewCreated$15;-><init>()V

    invoke-static {p2, v0}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    goto :goto_0

    :cond_0
    return-void
.end method
