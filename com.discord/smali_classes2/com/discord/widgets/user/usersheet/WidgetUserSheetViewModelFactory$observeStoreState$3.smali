.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;
.super Ljava/lang/Object;
.source "WidgetUserSheetViewModelFactory.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->observeStoreState(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserNotes;Lcom/discord/utilities/streams/StreamContextService;Lrx/Scheduler;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $storeGuilds:Lcom/discord/stores/StoreGuilds;

.field public final synthetic $storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

.field public final synthetic $storePermissions:Lcom/discord/stores/StorePermissions;

.field public final synthetic $storeUserNotes:Lcom/discord/stores/StoreUserNotes;

.field public final synthetic $storeUserPresence:Lcom/discord/stores/StoreUserPresence;

.field public final synthetic $storeUserProfile:Lcom/discord/stores/StoreUserProfile;

.field public final synthetic $storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

.field public final synthetic $storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

.field public final synthetic $streamContextService:Lcom/discord/utilities/streams/StreamContextService;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreUserNotes;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object p2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

    iput-object p3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserPresence:Lcom/discord/stores/StoreUserPresence;

    iput-object p4, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storePermissions:Lcom/discord/stores/StorePermissions;

    iput-object p5, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$streamContextService:Lcom/discord/utilities/streams/StreamContextService;

    iput-object p6, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    iput-object p7, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserProfile:Lcom/discord/stores/StoreUserProfile;

    iput-object p8, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    iput-object p9, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserNotes:Lcom/discord/stores/StoreUserNotes;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->call(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;)Lrx/Observable;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    sget-object v1, Lx/h/m;->d:Lx/h/m;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;->component1()Lcom/discord/models/domain/ModelUser;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;->component2()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;->component3()Lcom/discord/models/domain/ModelChannel;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;->component4()Lcom/discord/models/domain/ModelChannel;

    move-result-object v5

    if-nez v2, :cond_0

    const-wide/16 v1, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v2, v3}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3$1;

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    goto/16 :goto_6

    :cond_0
    const/4 v6, 0x1

    const-string v7, "channel.guildId"

    if-eqz v4, :cond_1

    iget-object v8, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v9

    invoke-static {v9, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Long;

    const/4 v12, 0x0

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v11, v6

    invoke-static {v11}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Lcom/discord/stores/StoreGuilds;->observeComputed(JLjava/util/Collection;)Lrx/Observable;

    move-result-object v8

    if-eqz v8, :cond_1

    goto :goto_0

    :cond_1
    new-instance v8, Lg0/l/e/j;

    invoke-direct {v8, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_0
    move-object v9, v8

    if-eqz v4, :cond_2

    iget-object v8, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v10

    invoke-static {v10, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/discord/stores/StoreGuilds;->observeRoles(J)Lrx/Observable;

    move-result-object v8

    if-eqz v8, :cond_2

    goto :goto_1

    :cond_2
    new-instance v8, Lg0/l/e/j;

    invoke-direct {v8, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_1
    move-object v10, v8

    if-eqz v5, :cond_3

    iget-object v8, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v11

    const-string v12, "selectedVoiceChannel.guildId"

    invoke-static {v11, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v13

    invoke-virtual {v8, v11, v12, v13, v14}, Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;

    move-result-object v5

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_2
    move-object v11, v5

    if-eqz v4, :cond_4

    iget-object v5, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeVoiceStates:Lcom/discord/stores/StoreVoiceStates;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v14

    invoke-virtual {v5, v12, v13, v14, v15}, Lcom/discord/stores/StoreVoiceStates;->get(JJ)Lrx/Observable;

    move-result-object v5

    if-eqz v5, :cond_4

    goto :goto_3

    :cond_4
    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_3
    move-object v12, v5

    sget-object v1, Lcom/discord/widgets/user/presence/ModelRichPresence;->Companion:Lcom/discord/widgets/user/presence/ModelRichPresence$Companion;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v13

    iget-object v5, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserPresence:Lcom/discord/stores/StoreUserPresence;

    invoke-virtual {v1, v13, v14, v5}, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion;->get(JLcom/discord/stores/StoreUserPresence;)Lrx/Observable;

    move-result-object v14

    const/4 v1, 0x0

    if-eqz v4, :cond_5

    iget-object v5, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v5

    if-eqz v5, :cond_5

    goto :goto_4

    :cond_5
    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_4
    move-object v15, v5

    if-eqz v4, :cond_6

    iget-object v5, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storePermissions:Lcom/discord/stores/StorePermissions;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Lcom/discord/stores/StorePermissions;->observePermissionsForChannel(J)Lrx/Observable;

    move-result-object v5

    if-eqz v5, :cond_6

    goto :goto_5

    :cond_6
    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_5
    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$streamContextService:Lcom/discord/utilities/streams/StreamContextService;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    invoke-virtual {v1, v7, v8, v6}, Lcom/discord/utilities/streams/StreamContextService;->getForUser(JZ)Lrx/Observable;

    move-result-object v17

    const-string v1, "computedMembersObservable"

    invoke-static {v9, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "guildRolesObservable"

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "mySelectedVoiceChannelVoiceStatesObservable"

    invoke-static {v11, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "currentChannelVoiceStatesObservable"

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {v1}, Lcom/discord/stores/StoreMediaSettings;->getVoiceConfig()Lrx/Observable;

    move-result-object v13

    const-string v1, "guildsObservable"

    invoke-static {v15, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "permissionsObservable"

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserProfile:Lcom/discord/stores/StoreUserProfile;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/discord/stores/StoreUserProfile;->get(J)Lrx/Observable;

    move-result-object v18

    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/discord/stores/StoreUserRelationships;->observe(J)Lrx/Observable;

    move-result-object v19

    iget-object v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;->$storeUserNotes:Lcom/discord/stores/StoreUserNotes;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/discord/stores/StoreUserNotes;->observeUserNote(J)Lrx/Observable;

    move-result-object v20

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3$2;

    invoke-direct {v1, v2, v3, v4}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3$2;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;)V

    move-object/from16 v16, v5

    move-object/from16 v21, v1

    invoke-static/range {v9 .. v21}, Lcom/discord/utilities/rx/ObservableCombineLatestOverloadsKt;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lkotlin/jvm/functions/Function12;)Lrx/Observable;

    move-result-object v1

    :goto_6
    return-object v1
.end method
