.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;
.super Lx/m/c/k;
.source "WidgetUserSheetViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->addRelationship(Ljava/lang/Integer;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/error/Error;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $username:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;->this$0:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    iput-object p2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;->$username:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/error/Error;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;->invoke(Lcom/discord/utilities/error/Error;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/error/Error;)V
    .locals 3

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPIAbortMessages;->INSTANCE:Lcom/discord/utilities/rest/RestAPIAbortMessages;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;Lcom/discord/utilities/error/Error;)V

    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/discord/utilities/rest/RestAPIAbortMessages;->handleAbortCodeOrDefault(Lcom/discord/utilities/error/Error;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
