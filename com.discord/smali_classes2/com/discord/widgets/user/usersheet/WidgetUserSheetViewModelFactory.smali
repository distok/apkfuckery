.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;
.super Ljava/lang/Object;
.source "WidgetUserSheetViewModelFactory.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$BootstrapData;
    }
.end annotation


# instance fields
.field private final channelId:Ljava/lang/Long;

.field private final friendToken:Ljava/lang/String;

.field private final isVoiceContext:Z

.field private final streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

.field private final userId:J


# direct methods
.method public constructor <init>(JLjava/lang/Long;ZLcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Ljava/lang/String;)V
    .locals 1

    const-string v0, "streamPreviewClickBehavior"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->userId:J

    iput-object p3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->channelId:Ljava/lang/Long;

    iput-boolean p4, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->isVoiceContext:Z

    iput-object p5, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    iput-object p6, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->friendToken:Ljava/lang/String;

    return-void
.end method

.method private final observeStoreState(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserNotes;Lcom/discord/utilities/streams/StreamContextService;Lrx/Scheduler;)Lrx/Observable;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreUser;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/stores/StoreVoiceChannelSelected;",
            "Lcom/discord/stores/StoreUserProfile;",
            "Lcom/discord/stores/StoreUserRelationships;",
            "Lcom/discord/stores/StoreVoiceStates;",
            "Lcom/discord/stores/StoreGuilds;",
            "Lcom/discord/stores/StoreMediaSettings;",
            "Lcom/discord/stores/StoreUserPresence;",
            "Lcom/discord/stores/StorePermissions;",
            "Lcom/discord/stores/StoreUserNotes;",
            "Lcom/discord/utilities/streams/StreamContextService;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    iget-wide v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->userId:J

    move-object v3, p1

    invoke-virtual {p1, v1, v2}, Lcom/discord/stores/StoreUser;->observeUser(J)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v2

    iget-object v3, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->channelId:Ljava/lang/Long;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual {p2, v3, v4}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    new-instance v4, Lg0/l/e/j;

    invoke-direct {v4, v3}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object v3, v4

    :goto_0
    invoke-virtual/range {p3 .. p3}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v4

    sget-object v5, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$2;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$2;

    if-eqz v5, :cond_1

    new-instance v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$sam$rx_functions_Func4$0;

    invoke-direct {v6, v5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$sam$rx_functions_Func4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    move-object v5, v6

    :cond_1
    check-cast v5, Lrx/functions/Func4;

    invoke-static {v1, v2, v3, v4, v5}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v1

    new-instance v12, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;

    move-object v2, v12

    move-object/from16 v3, p7

    move-object/from16 v4, p6

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p12

    move-object/from16 v8, p8

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p11

    invoke-direct/range {v2 .. v11}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory$observeStoreState$3;-><init>(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreUserNotes;)V

    invoke-virtual {v1, v12}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/utilities/rx/LeadingEdgeThrottle;

    const-wide/16 v3, 0xfa

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v6, p13

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/discord/utilities/rx/LeadingEdgeThrottle;-><init>(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)V

    new-instance v3, Lg0/l/a/u;

    iget-object v1, v1, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v3, v1, v2}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v3}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 42
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    move-object/from16 v14, p0

    const-string v0, "modelClass"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v30, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    iget-wide v12, v14, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->userId:J

    iget-object v15, v14, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->friendToken:Ljava/lang/String;

    iget-boolean v11, v14, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->isVoiceContext:Z

    iget-object v10, v14, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v27

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserProfile()Lcom/discord/stores/StoreUserProfile;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getVoiceStates()Lcom/discord/stores/StoreVoiceStates;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v9

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v16

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsersNotes()Lcom/discord/stores/StoreUserNotes;

    move-result-object v17

    new-instance v18, Lcom/discord/utilities/streams/StreamContextService;

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0xff

    const/16 v41, 0x0

    move-object/from16 v31, v18

    invoke-direct/range {v31 .. v41}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {}, Lg0/p/a;->a()Lrx/Scheduler;

    move-result-object v0

    move-object/from16 v19, v10

    const-string v10, "Schedulers.computation()"

    invoke-static {v0, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v21, v19

    move-object/from16 v10, v16

    move/from16 v19, v11

    move-object/from16 v11, v17

    move-wide/from16 v16, v12

    move-object/from16 v12, v18

    move-object/from16 v13, v20

    invoke-direct/range {v0 .. v13}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->observeStoreState(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserRelationships;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUserNotes;Lcom/discord/utilities/streams/StreamContextService;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    const-string v1, "observeStoreState(\n     \u2026s.computation()\n        )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v28, 0x3e0

    const/16 v29, 0x0

    move-object v1, v15

    move-object/from16 v15, v30

    move-object/from16 v18, v1

    move-object/from16 v20, v0

    invoke-direct/range {v15 .. v29}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;-><init>(JLjava/lang/String;ZLrx/Observable;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/channel/ChannelUtils;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v30
.end method

.method public final getChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getFriendToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->friendToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getStreamPreviewClickBehavior()Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    return-object v0
.end method

.method public final getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->userId:J

    return-wide v0
.end method

.method public final isVoiceContext()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModelFactory;->isVoiceContext:Z

    return v0
.end method
