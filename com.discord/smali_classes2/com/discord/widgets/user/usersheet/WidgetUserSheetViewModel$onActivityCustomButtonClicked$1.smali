.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;
.super Lx/m/c/k;
.source "WidgetUserSheetViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->onActivityCustomButtonClicked(Landroid/content/Context;JLjava/lang/String;JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelActivityMetaData;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $applicationContext:Landroid/content/Context;

.field public final synthetic $buttonIndex:I


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;->$buttonIndex:I

    iput-object p2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;->$applicationContext:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelActivityMetaData;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;->invoke(Lcom/discord/models/domain/ModelActivityMetaData;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelActivityMetaData;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelActivityMetaData;->getButtonUrls()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;->$buttonIndex:I

    invoke-static {p1, v0}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;->$applicationContext:Landroid/content/Context;

    const-string v1, ""

    invoke-static {v0, p1, v1}, Lcom/discord/utilities/uri/UriHandler;->handleOrUntrusted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
