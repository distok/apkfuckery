.class public final Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;
.super Lf/a/b/l0;
.source "WidgetUserSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;,
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;,
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;,
        Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Companion;

.field public static final LOCATION:Ljava/lang/String; = "User Profile"


# instance fields
.field private final channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private fetchedPreviews:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final friendToken:Ljava/lang/String;

.field private final isVoiceContext:Z

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final restAPISerializeNulls:Lcom/discord/utilities/rest/RestAPI;

.field private final storeApplicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

.field private final storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

.field private final storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

.field private final streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

.field private final userId:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->Companion:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Companion;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;ZLrx/Observable;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/channel/ChannelUtils;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Z",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;",
            ">;",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;",
            "Lcom/discord/stores/StoreMediaSettings;",
            "Lcom/discord/stores/StoreApplicationStreaming;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/stores/StoreApplicationStreamPreviews;",
            "Lcom/discord/utilities/channel/ChannelUtils;",
            ")V"
        }
    .end annotation

    const-string v0, "storeObservable"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "streamPreviewClickBehavior"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeMediaSettings"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeApplicationStreaming"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPISerializeNulls"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeApplicationStreamPreviews"

    invoke-static {p11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channelUtils"

    invoke-static {p12, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-wide p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    iput-object p3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->friendToken:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->isVoiceContext:Z

    iput-object p6, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    iput-object p7, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    iput-object p8, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    iput-object p9, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object p10, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPISerializeNulls:Lcom/discord/utilities/rest/RestAPI;

    iput-object p11, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    iput-object p12, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->fetchedPreviews:Ljava/util/Set;

    invoke-static {p5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p4

    const-class p5, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    new-instance p10, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$1;

    invoke-direct {p10, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 p6, 0x0

    const/4 p7, 0x0

    const/4 p8, 0x0

    const/4 p9, 0x0

    const/16 p11, 0x1e

    const/4 p12, 0x0

    invoke-static/range {p4 .. p12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/String;ZLrx/Observable;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/channel/ChannelUtils;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 15

    move/from16 v0, p13

    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getMediaSettings()Lcom/discord/stores/StoreMediaSettings;

    move-result-object v1

    move-object v9, v1

    goto :goto_0

    :cond_0
    move-object/from16 v9, p7

    :goto_0
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_1

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreaming()Lcom/discord/stores/StoreApplicationStreaming;

    move-result-object v1

    move-object v10, v1

    goto :goto_1

    :cond_1
    move-object/from16 v10, p8

    :goto_1
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_2

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    move-object v11, v1

    goto :goto_2

    :cond_2
    move-object/from16 v11, p9

    :goto_2
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_3

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApiSerializeNulls()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    move-object v12, v1

    goto :goto_3

    :cond_3
    move-object/from16 v12, p10

    :goto_3
    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_4

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplicationStreamPreviews()Lcom/discord/stores/StoreApplicationStreamPreviews;

    move-result-object v0

    move-object v13, v0

    goto :goto_4

    :cond_4
    move-object/from16 v13, p11

    :goto_4
    move-object v2, p0

    move-wide/from16 v3, p1

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v14, p12

    invoke-direct/range {v2 .. v14}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;-><init>(JLjava/lang/String;ZLrx/Observable;Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;Lcom/discord/stores/StoreMediaSettings;Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreApplicationStreamPreviews;Lcom/discord/utilities/channel/ChannelUtils;)V

    return-void
.end method

.method public static final synthetic access$emitDismissSheetEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitDismissSheetEvent()V

    return-void
.end method

.method public static final synthetic access$emitLaunchVideoCallEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitLaunchVideoCallEvent(J)V

    return-void
.end method

.method public static final synthetic access$emitLaunchVoiceCallEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitLaunchVoiceCallEvent(J)V

    return-void
.end method

.method public static final synthetic access$emitShowFriendRequestAbortToast(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitShowFriendRequestAbortToast(ILjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$emitShowToastEvent(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitShowToastEvent(I)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->handleStoreState(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)V

    return-void
.end method

.method private final createAdminViewState(Lcom/discord/models/domain/ModelChannel;ZZLcom/discord/utilities/permissions/ManageUserContext;Lcom/discord/models/domain/ModelVoice$State;)Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;
    .locals 15

    if-nez p1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p4, :cond_3

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanChangeNickname()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanManageRoles()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v2, 0x1

    :goto_1
    move v4, v2

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannel;->isManaged()Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz p2, :cond_4

    if-nez p3, :cond_4

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    goto :goto_4

    :cond_5
    if-eqz p4, :cond_4

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanKick()Z

    move-result v2

    if-ne v2, v1, :cond_4

    :goto_3
    const/4 v5, 0x1

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result v6

    if-eqz p4, :cond_6

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanBan()Z

    move-result v2

    if-ne v2, v1, :cond_6

    const/4 v7, 0x1

    goto :goto_5

    :cond_6
    const/4 v7, 0x0

    :goto_5
    if-eqz p5, :cond_7

    if-eqz p4, :cond_7

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanMute()Z

    move-result v2

    if-ne v2, v1, :cond_7

    const/4 v8, 0x1

    goto :goto_6

    :cond_7
    const/4 v8, 0x0

    :goto_6
    if-eqz p5, :cond_8

    invoke-virtual/range {p5 .. p5}, Lcom/discord/models/domain/ModelVoice$State;->isMute()Z

    move-result v2

    if-ne v2, v1, :cond_8

    const/4 v9, 0x1

    goto :goto_7

    :cond_8
    const/4 v9, 0x0

    :goto_7
    if-eqz p5, :cond_9

    if-eqz p4, :cond_9

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanDeafen()Z

    move-result v2

    if-ne v2, v1, :cond_9

    const/4 v10, 0x1

    goto :goto_8

    :cond_9
    const/4 v10, 0x0

    :goto_8
    if-eqz p5, :cond_a

    invoke-virtual/range {p5 .. p5}, Lcom/discord/models/domain/ModelVoice$State;->isDeaf()Z

    move-result v2

    if-ne v2, v1, :cond_a

    const/4 v11, 0x1

    goto :goto_9

    :cond_a
    const/4 v11, 0x0

    :goto_9
    if-eqz p5, :cond_b

    if-eqz p4, :cond_b

    invoke-virtual/range {p4 .. p4}, Lcom/discord/utilities/permissions/ManageUserContext;->getCanMove()Z

    move-result v2

    if-ne v2, v1, :cond_b

    const/4 v12, 0x1

    goto :goto_a

    :cond_b
    const/4 v12, 0x0

    :goto_a
    const/4 v2, 0x6

    new-array v3, v2, [Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v3, v0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v3, v1

    const/4 v13, 0x2

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v3, v13

    const/4 v13, 0x3

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v3, v13

    const/4 v13, 0x4

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v3, v13

    const/4 v13, 0x5

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    aput-object v14, v3, v13

    const/4 v13, 0x0

    :goto_b
    if-ge v13, v2, :cond_d

    aget-object v14, v3, v13

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    if-eqz v14, :cond_c

    const/4 v13, 0x1

    goto :goto_c

    :cond_c
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_d
    const/4 v13, 0x0

    :goto_c
    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;

    move-object v3, v0

    invoke-direct/range {v3 .. v13}, Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;-><init>(ZZZZZZZZZZ)V

    return-object v0
.end method

.method private final createConnectionsViewState(Lcom/discord/models/domain/ModelUserProfile;ZZ)Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;
    .locals 4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserProfile;->getConnectedAccounts()Ljava/util/List;

    move-result-object p1

    const-string v0, "userProfile.connectedAccounts"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelConnectedAccount;

    new-instance v2, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;

    const-string v3, "connectedAccount"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1}, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ConnectedAccountItem;-><init>(Lcom/discord/models/domain/ModelConnectedAccount;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x1

    if-nez p2, :cond_1

    if-nez p3, :cond_1

    const/4 p2, 0x1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    if-nez p2, :cond_2

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    xor-int/2addr p3, v1

    if-eqz p3, :cond_3

    :cond_2
    const/4 p1, 0x1

    :cond_3
    new-instance p3, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;

    invoke-direct {p3, p1, p2, v0}, Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;-><init>(ZZLjava/util/List;)V

    return-object p3
.end method

.method private final createPrivateChannelWithUser(J)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lrx/Observable<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/channel/ChannelUtils;->createPrivateChannel(J)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private final emitDismissSheetEvent()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$DismissSheet;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$DismissSheet;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitLaunchSpectateEvent(Lcom/discord/models/domain/ModelApplicationStream;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;

    invoke-direct {v1, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchSpectate;-><init>(Lcom/discord/models/domain/ModelApplicationStream;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitLaunchVideoCallEvent(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVideoCall;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitLaunchVoiceCallEvent(J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchVoiceCall;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitRequestStreamPermissionsEvent(Lcom/discord/models/domain/ModelApplicationStream;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;

    invoke-direct {v1, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$RequestPermissionsForSpectateStream;-><init>(Lcom/discord/models/domain/ModelApplicationStream;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowFriendRequestAbortToast(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowFriendRequestErrorToast;-><init>(ILjava/lang/String;)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowToastEvent(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;

    invoke-direct {v1, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$ShowToast;-><init>(I)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;)V
    .locals 23

    move-object/from16 v6, p0

    if-nez p1, :cond_0

    iget-object v0, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$UserNotFound;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$UserNotFound;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getComputedMembers()Ljava/util/Map;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getGuildRoles()Ljava/util/Map;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v16

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-virtual {v15}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v4

    cmp-long v7, v2, v4

    if-nez v7, :cond_1

    const/4 v2, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v15}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v4

    invoke-static {v15, v1}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-static {v0, v1}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelGuildMember$Computed;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-interface {v14, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/discord/models/domain/ModelGuildRole;

    if-eqz v8, :cond_2

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v7}, Lx/h/f;->sorted(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    goto :goto_2

    :cond_4
    sget-object v3, Lx/h/l;->d:Lx/h/l;

    :goto_2
    move-object/from16 v18, v3

    const/4 v3, 0x0

    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    if-nez v16, :cond_5

    goto :goto_3

    :cond_5
    sget-object v7, Lcom/discord/utilities/permissions/ManageUserContext;->Companion:Lcom/discord/utilities/permissions/ManageUserContext$Companion;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v11

    const-string v1, "memberMe.roles"

    invoke-static {v11, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getRoles()Ljava/util/List;

    move-result-object v12

    const-string v1, "member.roles"

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getPermissions()Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v8, v16

    move-object v9, v0

    move-object v10, v15

    invoke-virtual/range {v7 .. v14}, Lcom/discord/utilities/permissions/ManageUserContext$Companion;->from(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Ljava/util/Collection;Ljava/util/Collection;Ljava/lang/Long;Ljava/util/Map;)Lcom/discord/utilities/permissions/ManageUserContext;

    move-result-object v1

    move-object v7, v1

    goto :goto_4

    :cond_6
    :goto_3
    move-object v7, v3

    :goto_4
    if-eqz v17, :cond_7

    invoke-virtual/range {v17 .. v17}, Lcom/discord/models/domain/ModelChannel;->getOwnerId()J

    move-result-wide v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v8

    cmp-long v10, v1, v8

    if-nez v10, :cond_7

    const/4 v1, 0x1

    const/4 v2, 0x1

    goto :goto_5

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getUserRelationshipType()Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/models/domain/ModelUserRelationship;->getType(Ljava/lang/Integer;)I

    move-result v20

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getMySelectedVoiceChannelVoiceStates()Ljava/util/Map;

    move-result-object v1

    invoke-static {v15, v1}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelVoice$State;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getMySelectedVoiceChannelVoiceStates()Ljava/util/Map;

    move-result-object v8

    invoke-static {v0, v8}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelVoice$State;

    iget-boolean v8, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->isVoiceContext:Z

    if-nez v8, :cond_9

    invoke-direct {v6, v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->isInSameVoiceChannel(Lcom/discord/models/domain/ModelVoice$State;Lcom/discord/models/domain/ModelVoice$State;)Z

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_6

    :cond_8
    const/4 v0, 0x0

    const/4 v10, 0x0

    goto :goto_7

    :cond_9
    :goto_6
    const/4 v0, 0x1

    const/4 v10, 0x1

    :goto_7
    new-instance v11, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;

    if-eqz v5, :cond_a

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getSelfMuted()Z

    move-result v0

    goto :goto_8

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getMuted()Z

    move-result v0

    :goto_8
    if-eqz v5, :cond_b

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getSelfDeafened()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_9

    :cond_b
    move-object v1, v3

    :goto_9
    if-nez v5, :cond_c

    sget-object v8, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->INSTANCE:Lcom/discord/utilities/voice/PerceptualVolumeUtils;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getOutputVolume()F

    move-result v9

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {v8, v9, v12, v13, v3}, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->amplitudeToPerceptual$default(Lcom/discord/utilities/voice/PerceptualVolumeUtils;FFILjava/lang/Object;)F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    goto :goto_a

    :cond_c
    move-object v8, v3

    :goto_a
    invoke-direct {v11, v0, v1, v8}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;-><init>(ZLjava/lang/Boolean;Ljava/lang/Float;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getCurrentChannelVoiceStates()Ljava/util/Map;

    move-result-object v0

    invoke-static {v15, v0}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/discord/models/domain/ModelVoice$State;

    if-nez v17, :cond_d

    goto :goto_c

    :cond_d
    invoke-virtual/range {v17 .. v17}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual/range {v17 .. v17}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    :cond_e
    if-eqz v16, :cond_f

    invoke-virtual/range {v16 .. v16}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_b
    move-object v14, v0

    goto :goto_d

    :cond_f
    :goto_c
    move-object v14, v3

    :goto_d
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object v8, v3

    move v3, v5

    move v9, v4

    move-object v4, v7

    move v13, v5

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->createAdminViewState(Lcom/discord/models/domain/ModelChannel;ZZLcom/discord/utilities/permissions/ManageUserContext;Lcom/discord/models/domain/ModelVoice$State;)Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getUserProfile()Lcom/discord/models/domain/ModelUserProfile;

    move-result-object v1

    invoke-direct {v6, v1, v13, v9}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->createConnectionsViewState(Lcom/discord/models/domain/ModelUserProfile;ZZ)Lcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v1

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v2

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v3

    goto :goto_e

    :cond_10
    move-object v3, v8

    :goto_e
    if-eqz v3, :cond_11

    iget-object v2, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->fetchedPreviews:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    iget-object v2, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreamPreviews:Lcom/discord/stores/StoreApplicationStreamPreviews;

    invoke-virtual {v2, v1}, Lcom/discord/stores/StoreApplicationStreamPreviews;->fetchStreamPreviewIfNotFetching(Lcom/discord/utilities/streams/StreamContext;)V

    iget-object v2, v6, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->fetchedPreviews:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_11
    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getRichPresence()Lcom/discord/widgets/user/presence/ModelRichPresence;

    move-result-object v3

    if-eqz v16, :cond_12

    invoke-virtual/range {v16 .. v16}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v19, v4

    goto :goto_f

    :cond_12
    move-object/from16 v19, v8

    :goto_f
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$StoreState;->getUserNote()Lcom/discord/stores/StoreUserNotes$UserNoteState;

    move-result-object v22

    move-object v7, v2

    move-object v8, v15

    move v9, v13

    move-object v13, v3

    move-object/from16 v15, v18

    move-object/from16 v16, v0

    move-object/from16 v18, v1

    invoke-direct/range {v7 .. v22}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;-><init>(Lcom/discord/models/domain/ModelUser;ZZLcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;Lcom/discord/models/domain/ModelVoice$State;Lcom/discord/widgets/user/presence/ModelRichPresence;Ljava/lang/String;Ljava/util/List;Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;Lcom/discord/models/domain/ModelChannel;Lcom/discord/utilities/streams/StreamContext;Ljava/lang/String;ILcom/discord/widgets/user/profile/UserProfileConnectionsView$ViewState;Lcom/discord/stores/StoreUserNotes$UserNoteState;)V

    invoke-virtual {v6, v2}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final isInSameVoiceChannel(Lcom/discord/models/domain/ModelVoice$State;Lcom/discord/models/domain/ModelVoice$State;)Z
    .locals 1

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelVoice$State;->getChannelId()Ljava/lang/Long;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelVoice$State;->getChannelId()Ljava/lang/Long;

    move-result-object v0

    :cond_1
    if-eqz p2, :cond_3

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {p2, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto :goto_2

    :cond_3
    :goto_1
    const/4 p1, 0x0

    :goto_2
    return p1
.end method


# virtual methods
.method public final addRelationship(Ljava/lang/Integer;Ljava/lang/String;I)V
    .locals 12
    .param p3    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const-string v0, "username"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    iget-object v6, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->friendToken:Ljava/lang/String;

    const-string v2, "User Profile"

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, Lcom/discord/utilities/rest/RestAPI;->addRelationship(Ljava/lang/String;JLjava/lang/Integer;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p1, p0, v2, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    new-instance v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$1;

    invoke-direct {v9, p0, p3}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;I)V

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;

    invoke-direct {v7, p0, p2}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$addRelationship$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final banUser()V
    .locals 10

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v4

    const-string v3, "user.username"

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v3, "channel.guildId"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchBanUser;-><init>(Ljava/lang/String;JJ)V

    iget-object v0, v2, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v9}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final disconnectUser()V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    iget-object v3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPISerializeNulls:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v4, "channel.guildId"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    new-instance v8, Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;

    const/4 v0, 0x1

    invoke-direct {v8, v2, v0, v2}, Lcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;-><init>(Ljava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual/range {v3 .. v8}, Lcom/discord/utilities/rest/RestAPI;->disconnectGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMemberDisconnect;)Lrx/Observable;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$disconnectUser$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$disconnectUser$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 v8, 0x0

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$disconnectUser$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$disconnectUser$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final editMember()V
    .locals 7

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    const-string v3, "channel.guildId"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchEditMember;-><init>(JJ)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final guildDeafenUser()V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getAdminViewState()Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;

    move-result-object v0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;->isServerDeafened()Z

    move-result v0

    iget-object v4, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v5, "channel.guildId"

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    sget-object v1, Lcom/discord/restapi/RestAPIParams$GuildMember;->Companion:Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;

    const/4 v3, 0x1

    xor-int/2addr v0, v3

    invoke-virtual {v1, v0}, Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;->createWithDeaf(Z)Lcom/discord/restapi/RestAPIParams$GuildMember;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Lcom/discord/utilities/rest/RestAPI;->changeGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMember;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildDeafenUser$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildDeafenUser$1;

    const/4 v8, 0x0

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildDeafenUser$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildDeafenUser$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final guildMoveForUser()V
    .locals 5

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    const-string v3, "channel.guildId"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchMoveUser;-><init>(J)V

    iget-object v0, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final guildMuteUser()V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getAdminViewState()Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getAdminViewState()Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/user/profile/UserProfileAdminView$ViewState;->isServerMuted()Z

    move-result v0

    iget-object v5, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v3, "channel.guildId"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v8

    sget-object v1, Lcom/discord/restapi/RestAPIParams$GuildMember;->Companion:Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;

    const/4 v3, 0x1

    xor-int/2addr v0, v3

    invoke-virtual {v1, v0}, Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;->createWithMute(Z)Lcom/discord/restapi/RestAPIParams$GuildMember;

    move-result-object v10

    invoke-virtual/range {v5 .. v10}, Lcom/discord/utilities/rest/RestAPI;->changeGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMember;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildMuteUser$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildMuteUser$1;

    const/4 v8, 0x0

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildMuteUser$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$guildMuteUser$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final kickUser()V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isMultiUserDM()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->channelUtils:Lcom/discord/utilities/channel/ChannelUtils;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    invoke-virtual {v3, v4, v5, v0, v1}, Lcom/discord/utilities/channel/ChannelUtils;->removeGroupRecipient(JJ)Lrx/Observable;

    move-result-object v6

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$kickUser$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$kickUser$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x35

    const-string v5, "REST: remove group member"

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v4

    const-string v3, "user.username"

    invoke-static {v4, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v3, "channel.guildId"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event$LaunchKickUser;-><init>(Ljava/lang/String;JJ)V

    iget-object v0, v2, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v9}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final launchVideoCall()V
    .locals 13

    iget-wide v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->createPrivateChannelWithUser(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v3, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVideoCall$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVideoCall$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    new-instance v8, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVideoCall$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVideoCall$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final launchVoiceCall()V
    .locals 13

    iget-wide v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->createPrivateChannelWithUser(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v3, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVoiceCall$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVoiceCall$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    new-instance v8, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVoiceCall$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$launchVoiceCall$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final moveUserToChannel(J)V
    .locals 12

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v4, "channel.guildId"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v6

    sget-object v0, Lcom/discord/restapi/RestAPIParams$GuildMember;->Companion:Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;

    invoke-virtual {v0, p1, p2}, Lcom/discord/restapi/RestAPIParams$GuildMember$Companion;->createWithChannelId(J)Lcom/discord/restapi/RestAPIParams$GuildMember;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Lcom/discord/utilities/rest/RestAPI;->changeGuildMember(JJLcom/discord/restapi/RestAPIParams$GuildMember;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    invoke-static {p1, p2, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v2, p2, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v9, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$moveUserToChannel$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$moveUserToChannel$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 v8, 0x0

    new-instance v7, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$moveUserToChannel$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$moveUserToChannel$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onActivityCustomButtonClicked(Landroid/content/Context;JLjava/lang/String;JI)V
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p1

    const-string v2, "applicationContext"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "sessionId"

    move-object/from16 v6, p4

    invoke-static {v6, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    move-wide/from16 v4, p2

    move-wide/from16 v7, p5

    invoke-virtual/range {v3 .. v8}, Lcom/discord/utilities/rest/RestAPI;->getActivityMetadata(JLjava/lang/String;J)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, p0, v5, v3, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v6

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;

    move/from16 v2, p7

    invoke-direct {v10, v2, v1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$onActivityCustomButtonClicked$1;-><init>(ILandroid/content/Context;)V

    const/4 v7, 0x0

    const-string v8, "REST: Custom Button GetActivityMetadata"

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x35

    const/4 v14, 0x0

    invoke-static/range {v6 .. v14}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method public final onSpectatePermissionsGranted(Lcom/discord/models/domain/ModelApplicationStream;)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeApplicationStreaming:Lcom/discord/stores/StoreApplicationStreaming;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreApplicationStreaming;->targetStream(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->streamPreviewClickBehavior:Lcom/discord/widgets/user/usersheet/WidgetUserSheet$StreamPreviewClickBehavior;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitDismissSheetEvent()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitLaunchSpectateEvent(Lcom/discord/models/domain/ModelApplicationStream;)V

    :goto_0
    return-void
.end method

.method public final onStreamPreviewClicked(Lcom/discord/utilities/streams/StreamContext;)V
    .locals 2

    const-string v0, "streamContext"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->getJoinability()Lcom/discord/utilities/streams/StreamContext$Joinability;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/streams/StreamContext$Joinability;->MISSING_PERMISSIONS:Lcom/discord/utilities/streams/StreamContext$Joinability;

    if-ne v0, v1, :cond_0

    const p1, 0x7f120442

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitShowToastEvent(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->emitRequestStreamPermissionsEvent(Lcom/discord/models/domain/ModelApplicationStream;)V

    :goto_0
    return-void
.end method

.method public final removeRelationship(I)V
    .locals 13
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    const-string v3, "User Profile"

    invoke-virtual {v0, v3, v1, v2}, Lcom/discord/utilities/rest/RestAPI;->removeRelationship(Ljava/lang/String;J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v3, v1, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;

    new-instance v10, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$removeRelationship$1;

    invoke-direct {v10, p0, p1}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$removeRelationship$1;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;I)V

    new-instance v8, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$removeRelationship$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$removeRelationship$2;-><init>(Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final setUserOutputVolume(F)V
    .locals 7

    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    iget-wide v1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    sget-object v3, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->INSTANCE:Lcom/discord/utilities/voice/PerceptualVolumeUtils;

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v3, p1, v4, v5, v6}, Lcom/discord/utilities/voice/PerceptualVolumeUtils;->perceptualToAmplitude$default(Lcom/discord/utilities/voice/PerceptualVolumeUtils;FFILjava/lang/Object;)F

    move-result p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/stores/StoreMediaSettings;->setUserOutputVolume(JF)V

    return-void
.end method

.method public final toggleDeafen(Z)V
    .locals 2

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->isMe()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getVoiceSettingsViewState()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;->isDeafened()Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {p1}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfDeafened()V

    :cond_1
    return-void
.end method

.method public final toggleMute(Z)V
    .locals 2

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getVoiceSettingsViewState()Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/user/usersheet/UserProfileVoiceSettingsView$ViewState;->isMuted()Z

    move-result v1

    if-eq v1, p1, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->isMe()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    invoke-virtual {p1}, Lcom/discord/stores/StoreMediaSettings;->toggleSelfMuted()Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->storeMediaSettings:Lcom/discord/stores/StoreMediaSettings;

    iget-wide v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreMediaSettings;->toggleUserMuted(J)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final updateUserNote(Ljava/lang/String;)V
    .locals 13

    const-string v0, "noteText"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUserNote()Lcom/discord/stores/StoreUserNotes$UserNoteState;

    move-result-object v1

    instance-of v1, v1, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$ViewState$Loaded;->getUserNote()Lcom/discord/stores/StoreUserNotes$UserNoteState;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserNotes$UserNoteState$Loaded;->getNote()Lcom/discord/models/domain/ModelUserNote;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUserNote;->getNote()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iget-wide v4, p0, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel;->userId:J

    new-instance v1, Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;

    invoke-direct {v1, p1}, Lcom/discord/restapi/RestAPIParams$UserNoteUpdate;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5, v1}, Lcom/discord/utilities/rest/RestAPI;->updateUserNotes(JLcom/discord/restapi/RestAPIParams$UserNoteUpdate;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$updateUserNote$1;->INSTANCE:Lcom/discord/widgets/user/usersheet/WidgetUserSheetViewModel$updateUserNote$1;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x35

    const/4 v12, 0x0

    const-string v6, "updateNote"

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method
