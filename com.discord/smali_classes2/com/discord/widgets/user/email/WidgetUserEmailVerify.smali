.class public Lcom/discord/widgets/user/email/WidgetUserEmailVerify;
.super Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;
.source "WidgetUserEmailVerify.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;
    }
.end annotation


# instance fields
.field private verifyEmailChange:Landroid/view/View;

.field private verifyEmailResend:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;-><init>()V

    return-void
.end method

.method private configureUI(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V
    .locals 1

    invoke-static {p1}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;->access$000(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;->access$100(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getMode()Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/discord/widgets/user/email/WidgetUserEmailUpdate;->launch(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->configureUIActions(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V

    :goto_0
    return-void
.end method

.method private configureUIActions(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->verifyEmailChange:Landroid/view/View;

    new-instance v1, Lf/a/o/g/u/b;

    invoke-direct {v1, p0}, Lf/a/o/g/u/b;-><init>(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->verifyEmailResend:Landroid/view/View;

    new-instance v1, Lf/a/o/g/u/d;

    invoke-direct {v1, p0, p1}, Lf/a/o/g/u/d;-><init>(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static synthetic g(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->configureUI(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)V

    return-void
.end method

.method public static launch(Landroid/content/Context;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1, v0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getLaunchIntent(Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;ZZZ)Landroid/content/Intent;

    move-result-object p1

    const-class v0, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    invoke-static {p0, v0, p1}, Lf/a/b/m;->d(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;)V

    return-void
.end method

.method public static launchForResult(Landroidx/fragment/app/Fragment;Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;I)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1, v0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->getLaunchIntent(Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase$Mode;ZZZ)Landroid/content/Intent;

    move-result-object p1

    const-class v0, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;

    invoke-static {p0, v0, p1, p2}, Lf/a/b/m;->f(Landroidx/fragment/app/Fragment;Ljava/lang/Class;Landroid/content/Intent;I)V

    return-void
.end method

.method private showSuccessToast(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const p1, 0x7f121a04

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lf/a/j/b/b/g;->b(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    const/4 v0, 0x4

    invoke-static {p0, p1, v1, v0}, Lf/a/b/p;->l(Landroidx/fragment/app/Fragment;Ljava/lang/CharSequence;II)V

    return-void
.end method


# virtual methods
.method public synthetic f(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;Ljava/lang/Void;)V
    .locals 0

    invoke-static {p1}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;->access$100(Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->showSuccessToast(Ljava/lang/String;)V

    return-void
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02c2

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f0a008f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->verifyEmailResend:Landroid/view/View;

    const v0, 0x7f0a008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/email/WidgetUserEmailVerify;->verifyEmailChange:Landroid/view/View;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 3

    invoke-super {p0}, Lcom/discord/widgets/user/account/WidgetUserAccountVerifyBase;->onViewBoundOrOnResume()V

    invoke-static {}, Lcom/discord/widgets/user/email/WidgetUserEmailVerify$Model;->get()Lrx/Observable;

    move-result-object v0

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lf/a/o/g/u/e;

    invoke-direct {v1, p0}, Lf/a/o/g/u/e;-><init>(Lcom/discord/widgets/user/email/WidgetUserEmailVerify;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
