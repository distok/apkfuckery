.class public final Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;
.super Ljava/lang/Object;
.source "WidgetUserMentions.kt"

# interfaces
.implements Lrx/functions/Action2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/WidgetUserMentions;->configureToolbar(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action2<",
        "Landroid/view/MenuItem;",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildName:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/user/WidgetUserMentions;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/WidgetUserMentions;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->this$0:Lcom/discord/widgets/user/WidgetUserMentions;

    iput-object p2, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->$guildName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/view/MenuItem;Landroid/content/Context;)V
    .locals 3

    const-string p2, "menuItem"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x7f0a06a8

    if-ne p1, p2, :cond_0

    sget-object p1, Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter;->Companion:Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter$Companion;

    iget-object p2, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->this$0:Lcom/discord/widgets/user/WidgetUserMentions;

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    const-string v0, "parentFragmentManager"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->$guildName:Ljava/lang/String;

    iget-object v1, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->this$0:Lcom/discord/widgets/user/WidgetUserMentions;

    invoke-static {v1}, Lcom/discord/widgets/user/WidgetUserMentions;->access$getMentionsLoader$p(Lcom/discord/widgets/user/WidgetUserMentions;)Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;->getFilters()Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1$1;-><init>(Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;)V

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter$Companion;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/user/WidgetUserMentions$WidgetUserMentionFilter;

    :cond_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/MenuItem;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->call(Landroid/view/MenuItem;Landroid/content/Context;)V

    return-void
.end method
