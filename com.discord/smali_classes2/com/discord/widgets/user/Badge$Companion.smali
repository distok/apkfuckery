.class public final Lcom/discord/widgets/user/Badge$Companion;
.super Ljava/lang/Object;
.source "Badge.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/Badge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/Badge$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getBadgesForUser(Lcom/discord/models/domain/ModelUserProfile;IZZLandroid/content/Context;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUserProfile;",
            "IZZ",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/user/Badge;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p5

    const-string v3, "profile"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0x8

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    const-string v5, "profile.user"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isStaff()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Lcom/discord/widgets/user/Badge;

    const v7, 0x7f0803f9

    const v6, 0x7f1216e4

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    const/4 v13, 0x0

    move-object v6, v4

    invoke-direct/range {v6 .. v13}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isPartner()Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Lcom/discord/widgets/user/Badge;

    const v7, 0x7f0803ee

    const v6, 0x7f12124a

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    const/4 v13, 0x0

    move-object v6, v4

    invoke-direct/range {v6 .. v13}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isHypeSquad()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Lcom/discord/widgets/user/Badge;

    const v7, 0x7f0803ec

    const v6, 0x7f120cee

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    const/4 v13, 0x0

    move-object v6, v4

    invoke-direct/range {v6 .. v13}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isHypesquadHouse1()Z

    move-result v4

    const v6, 0x7f120cff

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v4, :cond_5

    if-ne v1, v8, :cond_3

    const/4 v4, 0x1

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    :goto_0
    new-instance v15, Lcom/discord/widgets/user/Badge;

    if-eqz v4, :cond_4

    const v4, 0x7f080357

    const v10, 0x7f080357

    goto :goto_1

    :cond_4
    const v4, 0x7f080356

    const v10, 0x7f080356

    :goto_1
    const v4, 0x7f120cf6

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-array v9, v8, [Ljava/lang/Object;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v7

    invoke-virtual {v2, v6, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v4, 0x18

    const/16 v16, 0x0

    move-object v9, v15

    move-object v6, v15

    move v15, v4

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isHypesquadHouse2()Z

    move-result v4

    const/4 v6, 0x2

    if-eqz v4, :cond_8

    if-ne v1, v6, :cond_6

    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    :goto_2
    new-instance v15, Lcom/discord/widgets/user/Badge;

    if-eqz v4, :cond_7

    const v4, 0x7f080359

    const v10, 0x7f080359

    goto :goto_3

    :cond_7
    const v4, 0x7f080358

    const v10, 0x7f080358

    :goto_3
    const v4, 0x7f120cf7

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-array v9, v8, [Ljava/lang/Object;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v7

    const v4, 0x7f120cff

    invoke-virtual {v2, v4, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v4, 0x18

    const/16 v16, 0x0

    move-object v9, v15

    move-object v6, v15

    move v15, v4

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isHypesquadHouse3()Z

    move-result v4

    const/4 v6, 0x3

    if-eqz v4, :cond_b

    if-ne v1, v6, :cond_9

    const/4 v1, 0x1

    goto :goto_4

    :cond_9
    const/4 v1, 0x0

    :goto_4
    new-instance v4, Lcom/discord/widgets/user/Badge;

    if-eqz v1, :cond_a

    const v1, 0x7f08035b

    const v10, 0x7f08035b

    goto :goto_5

    :cond_a
    const v1, 0x7f08035a

    const v10, 0x7f08035a

    :goto_5
    const v1, 0x7f120cf8

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-array v9, v8, [Ljava/lang/Object;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v7

    const v1, 0x7f120cff

    invoke-virtual {v2, v1, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x18

    const/16 v16, 0x0

    move-object v9, v4

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isBugHunterLevel1()Z

    move-result v1

    const v4, 0x7f120399

    if-eqz v1, :cond_c

    new-instance v1, Lcom/discord/widgets/user/Badge;

    const v10, 0x7f0803ea

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1c

    const/16 v16, 0x0

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isBugHunterLevel2()Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v1, Lcom/discord/widgets/user/Badge;

    const v10, 0x7f0803eb

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1c

    const/16 v16, 0x0

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isVerifiedDeveloper()Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Lcom/discord/widgets/user/Badge;

    const v10, 0x7f0803fa

    const v4, 0x7f121a1c

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1c

    const/16 v16, 0x0

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-static {v1, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isPremiumEarlySupporter()Z

    move-result v1

    if-eqz v1, :cond_10

    new-instance v1, Lcom/discord/widgets/user/Badge;

    const v10, 0x7f0803ef

    const v4, 0x7f120659

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    if-nez p3, :cond_f

    if-eqz p4, :cond_f

    const/4 v13, 0x1

    goto :goto_6

    :cond_f
    const/4 v13, 0x0

    :goto_6
    const/4 v15, 0x4

    const/16 v16, 0x0

    const-string v14, "PREMIUM_EARLY_SUPPORTER"

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->isPremium()Z

    move-result v1

    const v4, 0x7f12143e

    if-eqz v1, :cond_12

    new-instance v1, Lcom/discord/widgets/user/Badge;

    const v10, 0x7f0803ed

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    const v5, 0x7f1212da

    new-array v9, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/discord/models/domain/ModelUserProfile;->getPremiumSince(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v9, v7

    invoke-virtual {v2, v5, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    if-nez p3, :cond_11

    if-eqz p4, :cond_11

    const/4 v13, 0x1

    goto :goto_7

    :cond_11
    const/4 v13, 0x0

    :goto_7
    const-string v14, "PREMIUM"

    move-object v9, v1

    invoke-direct/range {v9 .. v14}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->isPremiumGuildSubscriber()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelUserProfile;->getPremiumGuildMonthsSubscribed()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v9, 0x18

    const v10, 0x7f0803f0

    if-lt v5, v9, :cond_13

    const v10, 0x7f0803f8

    const v12, 0x7f0803f8

    goto/16 :goto_8

    :cond_13
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v9, 0x12

    if-lt v5, v9, :cond_14

    const v10, 0x7f0803f7

    const v12, 0x7f0803f7

    goto :goto_8

    :cond_14
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v9, 0xf

    if-lt v5, v9, :cond_15

    const v10, 0x7f0803f6

    const v12, 0x7f0803f6

    goto :goto_8

    :cond_15
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v9, 0xc

    if-lt v5, v9, :cond_16

    const v10, 0x7f0803f5

    const v12, 0x7f0803f5

    goto :goto_8

    :cond_16
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v9, 0x9

    if-lt v5, v9, :cond_17

    const v10, 0x7f0803f4

    const v12, 0x7f0803f4

    goto :goto_8

    :cond_17
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v9, 0x6

    if-lt v5, v9, :cond_18

    const v10, 0x7f0803f3

    const v12, 0x7f0803f3

    goto :goto_8

    :cond_18
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lt v5, v6, :cond_19

    const v10, 0x7f0803f2

    const v12, 0x7f0803f2

    goto :goto_8

    :cond_19
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x2

    if-lt v5, v6, :cond_1a

    const v10, 0x7f0803f1

    const v12, 0x7f0803f1

    goto :goto_8

    :cond_1a
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v12, 0x7f0803f0

    :goto_8
    new-instance v1, Lcom/discord/widgets/user/Badge;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    const v4, 0x7f1213a4

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/discord/models/domain/ModelUserProfile;->getPremiumGuildSince(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    if-nez p3, :cond_1b

    if-eqz p4, :cond_1b

    const/4 v15, 0x1

    goto :goto_9

    :cond_1b
    const/4 v15, 0x0

    :goto_9
    const-string v16, "PREMIUM_GUILD"

    move-object v11, v1

    invoke-direct/range {v11 .. v16}, Lcom/discord/widgets/user/Badge;-><init>(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1c
    return-object v3
.end method

.method public final onBadgeClick(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;)Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Landroid/content/Context;",
            ")",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/user/Badge;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "fragmentManager"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/user/Badge$Companion$onBadgeClick$1;-><init>(Landroidx/fragment/app/FragmentManager;Landroid/content/Context;)V

    return-object v0
.end method
