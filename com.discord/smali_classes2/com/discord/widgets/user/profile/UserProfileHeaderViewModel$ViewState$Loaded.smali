.class public final Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;
.source "UserProfileHeaderViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final allowAnimatedEmojis:Z

.field private final isMeUserPremium:Z

.field private final isMeUserVerified:Z

.field private final presence:Lcom/discord/models/domain/ModelPresence;

.field private final snowsGivingHypeSquadEventWinner:I

.field private final streamContext:Lcom/discord/utilities/streams/StreamContext;

.field private final user:Lcom/discord/models/domain/ModelUser;

.field private final userNickname:Ljava/lang/String;

.field private final userProfile:Lcom/discord/models/domain/ModelUserProfile;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;ZZIZ)V
    .locals 1

    const-string v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userProfile"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    iput-object p4, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    iput-object p5, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    iput-boolean p6, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    iput-boolean p7, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    iput p8, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    iput-boolean p9, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;ZZIZILjava/lang/Object;)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-boolean v1, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    goto :goto_8

    :cond_8
    move/from16 v1, p9

    :goto_8
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->copy(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;ZZIZ)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    return-object v0
.end method

.method public final component4()Lcom/discord/utilities/streams/StreamContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    return-object v0
.end method

.method public final component5()Lcom/discord/models/domain/ModelUserProfile;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    return v0
.end method

.method public final component9()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;ZZIZ)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;
    .locals 11

    const-string v0, "user"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userProfile"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;

    move-object v1, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;ZZIZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    iget-object v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    iget-object v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    iget-object v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    iget-object v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    iget-boolean v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    iget-boolean v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    iget v1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    iget-boolean p1, p1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowAnimatedEmojis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    return v0
.end method

.method public final getPresence()Lcom/discord/models/domain/ModelPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    return-object v0
.end method

.method public final getSnowsGivingHypeSquadEventWinner()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    return v0
.end method

.method public final getStreamContext()Lcom/discord/utilities/streams/StreamContext;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    return-object v0
.end method

.method public final getUser()Lcom/discord/models/domain/ModelUser;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    return-object v0
.end method

.method public final getUserNickname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserProfile()Lcom/discord/models/domain/ModelUserProfile;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPresence;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/discord/utilities/streams/StreamContext;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUserProfile;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    if-eqz v1, :cond_7

    goto :goto_4

    :cond_7
    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    return v0
.end method

.method public final isMeUserPremium()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    return v0
.end method

.method public final isMeUserVerified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(user="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userNickname="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userNickname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", presence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->presence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", streamContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->streamContext:Lcom/discord/utilities/streams/StreamContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userProfile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->userProfile:Lcom/discord/models/domain/ModelUserProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isMeUserPremium="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isMeUserVerified="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", snowsGivingHypeSquadEventWinner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->snowsGivingHypeSquadEventWinner:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", allowAnimatedEmojis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->allowAnimatedEmojis:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
