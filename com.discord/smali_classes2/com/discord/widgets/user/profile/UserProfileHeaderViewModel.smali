.class public final Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;
.super Lf/a/b/l0;
.source "UserProfileHeaderViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;,
        Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;,
        Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;,
        Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Companion;

.field public static final ME:J = -0x1L


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;->Companion:Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lrx/Observable;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;

    new-instance v8, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$1;-><init>(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;->handleStoreState(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;)V
    .locals 12

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getComputedMembers()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelGuildMember$Computed;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, v1

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getWinnerBadgeExperiment()Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v0

    move v10, v0

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    const/4 v10, -0x1

    :goto_1
    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getRichPresence()Lcom/discord/widgets/user/presence/ModelRichPresence;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v1

    :cond_2
    move-object v5, v1

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getUserProfile()Lcom/discord/models/domain/ModelUserProfile;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v8

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->isVerified()Z

    move-result v9

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;->getAllowAnimatedEmojis()Z

    move-result v11

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;-><init>(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;ZZIZ)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method
