.class public final Lcom/discord/widgets/user/profile/UserProfileHeaderView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "UserProfileHeaderView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/profile/UserProfileHeaderView$BadgeViewHolder;,
        Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;


# instance fields
.field private final badgesAdapter:Lcom/discord/utilities/views/SimpleRecyclerAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/discord/utilities/views/SimpleRecyclerAdapter<",
            "Lcom/discord/widgets/user/Badge;",
            "Lcom/discord/widgets/user/profile/UserProfileHeaderView$BadgeViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final badgesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final customStatusTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private onBadgeClick:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/Badge;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final primaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final secondaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private userProfileHeaderBackgroundColor:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    const-string v3, "primaryName"

    const-string v4, "getPrimaryName()Lcom/discord/views/UsernameView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    const-string v6, "secondaryName"

    const-string v7, "getSecondaryName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    const-string v6, "customStatusTextView"

    const-string v7, "getCustomStatusTextView()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    const-string v6, "userAvatarPresenceView"

    const-string v7, "getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    const-string v6, "badgesRecycler"

    const-string v7, "getBadgesRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->Companion:Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f0a0b20

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->primaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b21

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->secondaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b1e

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->customStatusTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b19

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b1d

    invoke-static {p0, v0}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->badgesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$onBadgeClick$1;->INSTANCE:Lcom/discord/widgets/user/profile/UserProfileHeaderView$onBadgeClick$1;

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->onBadgeClick:Lkotlin/jvm/functions/Function1;

    const v0, 0x7f0d0111

    invoke-static {p1, v0, p0}, Landroid/view/ViewGroup;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/user/profile/RightToLeftGridLayoutManager;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2, v2}, Lcom/discord/widgets/user/profile/RightToLeftGridLayoutManager;-><init>(Landroid/content/Context;IIZ)V

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getBadgesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    new-instance v0, Lcom/discord/utilities/views/SimpleRecyclerAdapter;

    new-instance v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView$1;-><init>(Lcom/discord/widgets/user/profile/UserProfileHeaderView;)V

    const/4 v3, 0x0

    invoke-direct {v0, v3, v1, v2, v3}, Lcom/discord/utilities/views/SimpleRecyclerAdapter;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function2;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->badgesAdapter:Lcom/discord/utilities/views/SimpleRecyclerAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getBadgesRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    sget-object v0, Lcom/discord/R$a;->UserProfileHeaderView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026le.UserProfileHeaderView)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const p2, 0x7f0404a8

    invoke-static {p0, p2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/view/View;I)I

    move-result p2

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->userProfileHeaderBackgroundColor:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public static final bind(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Landroidx/fragment/app/Fragment;Lcom/discord/app/AppComponent;JLjava/lang/Long;)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->Companion:Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/user/profile/UserProfileHeaderView$Companion;->bind(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Landroidx/fragment/app/Fragment;Lcom/discord/app/AppComponent;JLjava/lang/Long;)V

    return-void
.end method

.method private final getBadgesRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->badgesRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getCustomStatusDraweeSpanStringBuilder(Lcom/discord/models/domain/activity/ModelActivity;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 6

    new-instance v0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-direct {v0}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getEmoji()Lcom/discord/models/domain/ModelMessageReaction$Emoji;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    sget-object v3, Lcom/discord/utilities/textprocessing/node/EmojiNode;->Companion:Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;

    const-string v4, "it"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v3, v1, v4, v5, v2}, Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;->from$default(Lcom/discord/utilities/textprocessing/node/EmojiNode$Companion;Lcom/discord/models/domain/ModelMessageReaction$Emoji;IILjava/lang/Object;)Lcom/discord/utilities/textprocessing/node/EmojiNode;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_1

    new-instance v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;

    invoke-direct {v1, p0, p2}, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;-><init>(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Z)V

    invoke-virtual {v2, v0, v1}, Lcom/discord/utilities/textprocessing/node/EmojiNode;->render(Landroid/text/SpannableStringBuilder;Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;)V

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    if-eqz v2, :cond_2

    const/16 p2, 0x2002

    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_2
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    return-object v0
.end method

.method private final getCustomStatusTextView()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->customStatusTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    return-object v0
.end method

.method private final getPrimaryName()Lcom/discord/views/UsernameView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->primaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/UsernameView;

    return-object v0
.end method

.method private final getPrimaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 9

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "context"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f040153

    const v4, 0x7f090001

    const v5, 0x7f0b001f

    const v6, 0x7f040178

    const v7, 0x7f090002

    const v8, 0x7f0b001e

    move-object v0, p1

    move-object v1, p2

    invoke-static/range {v0 .. v8}, Lcom/discord/widgets/user/UserNameFormatterKt;->getSpannableForUserNameWithDiscrim(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;Landroid/content/Context;IIIIII)Landroid/text/SpannableStringBuilder;

    move-result-object p1

    return-object p1
.end method

.method private final getSecondaryName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->secondaryName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSecondaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUserNameWithDiscriminator()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/user/UserAvatarPresenceView;

    return-object v0
.end method


# virtual methods
.method public final getOnBadgeClick()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/discord/widgets/user/Badge;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->onBadgeClick:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    iget v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->userProfileHeaderBackgroundColor:I

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->userProfileHeaderBackgroundColor:I

    invoke-virtual {v0, v1}, Lcom/discord/views/user/UserAvatarPresenceView;->setAvatarBackgroundColor(I)V

    :cond_0
    return-void
.end method

.method public final setOnBadgeClick(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/Badge;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->onBadgeClick:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final updateViewState(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;)V
    .locals 9

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getUserNickname()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;

    move-result-object v2

    new-instance v3, Lcom/discord/views/user/UserAvatarPresenceView$a;

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/discord/views/user/UserAvatarPresenceView$a;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelPresence;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual {v2, v3}, Lcom/discord/views/user/UserAvatarPresenceView;->a(Lcom/discord/views/user/UserAvatarPresenceView$a;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getPrimaryName()Lcom/discord/views/UsernameView;

    move-result-object v2

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getPrimaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/discord/views/UsernameView;->setUsernameText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getPrimaryName()Lcom/discord/views/UsernameView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isBot()Z

    move-result v3

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isSystem()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f1217ce

    goto :goto_0

    :cond_0
    const v4, 0x7f120383

    :goto_0
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isVerifiedBot()Z

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/discord/views/UsernameView;->a(ZIZ)V

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getSecondaryName()Landroid/widget/TextView;

    move-result-object v2

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getSecondaryNameTextForUser(Lcom/discord/models/domain/ModelUser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/discord/widgets/user/Badge;->Companion:Lcom/discord/widgets/user/Badge$Companion;

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getUserProfile()Lcom/discord/models/domain/ModelUserProfile;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getSnowsGivingHypeSquadEventWinner()I

    move-result v5

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserPremium()Z

    move-result v6

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->isMeUserVerified()Z

    move-result v7

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v0, "context"

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {v3 .. v8}, Lcom/discord/widgets/user/Badge$Companion;->getBadgesForUser(Lcom/discord/models/domain/ModelUserProfile;IZZLandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->badgesAdapter:Lcom/discord/utilities/views/SimpleRecyclerAdapter;

    invoke-virtual {v1, v0}, Lcom/discord/utilities/views/SimpleRecyclerAdapter;->setData(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPresence;->getCustomStatusActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$ViewState$Loaded;->getAllowAnimatedEmojis()Z

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getCustomStatusDraweeSpanStringBuilder(Lcom/discord/models/domain/activity/ModelActivity;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getCustomStatusTextView()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getCustomStatusTextView()Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
