.class public final Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;
.super Ljava/lang/Object;
.source "UserProfileHeaderView.kt"

# interfaces
.implements Lcom/discord/utilities/textprocessing/node/EmojiNode$RenderContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/profile/UserProfileHeaderView;->getCustomStatusDraweeSpanStringBuilder(Lcom/discord/models/domain/activity/ModelActivity;Z)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $shouldAnimate:Z

.field private final context:Landroid/content/Context;

.field private final isAnimationEnabled:Z

.field public final synthetic this$0:Lcom/discord/widgets/user/profile/UserProfileHeaderView;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/profile/UserProfileHeaderView;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;->this$0:Lcom/discord/widgets/user/profile/UserProfileHeaderView;

    iput-boolean p2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;->$shouldAnimate:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;->context:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;->isAnimationEnabled:Z

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isAnimationEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderView$getCustomStatusDraweeSpanStringBuilder$1;->isAnimationEnabled:Z

    return v0
.end method
