.class public final Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;
.super Ljava/lang/Object;
.source "UserProfileHeaderViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final channelId:Ljava/lang/Long;

.field private final userId:J


# direct methods
.method public constructor <init>(JLjava/lang/Long;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->userId:J

    iput-object p3, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->channelId:Ljava/lang/Long;

    return-void
.end method

.method public static final synthetic access$observeComputedMembers(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;Ljava/util/Collection;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->observeComputedMembers(Ljava/util/Collection;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final observeComputedMembers(Ljava/util/Collection;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/stores/StoreGuilds;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->channelId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeComputedMembers$1;

    invoke-direct {v0, p3, p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeComputedMembers$1;-><init>(Lcom/discord/stores/StoreGuilds;Ljava/util/Collection;)V

    invoke-virtual {p2, v0}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "storeChannels\n          \u2026emptyMap())\n            }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lx/h/m;->d:Lx/h/m;

    new-instance p2, Lg0/l/e/j;

    invoke-direct {p2, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(emptyMap())"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object p1, p2

    :goto_0
    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v4

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v5

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v6

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserProfile()Lcom/discord/stores/StoreUserProfile;

    move-result-object v7

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v9

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v8

    new-instance v1, Lcom/discord/utilities/streams/StreamContextService;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0xff

    const/16 v20, 0x0

    move-object v10, v1

    invoke-direct/range {v10 .. v20}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v10}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->observeStoreState(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/streams/StreamContextService;)Lrx/Observable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel;-><init>(Lrx/Observable;)V

    return-object v0
.end method

.method public final getChannelId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->channelId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;->userId:J

    return-wide v0
.end method

.method public final observeStoreState(Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserPresence;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreExperiments;Lcom/discord/utilities/streams/StreamContextService;)Lrx/Observable;
    .locals 12
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreUser;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/stores/StoreGuilds;",
            "Lcom/discord/stores/StoreUserPresence;",
            "Lcom/discord/stores/StoreUserProfile;",
            "Lcom/discord/stores/StoreUserSettings;",
            "Lcom/discord/stores/StoreExperiments;",
            "Lcom/discord/utilities/streams/StreamContextService;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;",
            ">;"
        }
    .end annotation

    move-object v0, p1

    const-string v1, "storeUser"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storeChannels"

    move-object v4, p2

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storeGuilds"

    move-object v5, p3

    invoke-static {p3, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storeUserPresence"

    move-object/from16 v6, p4

    invoke-static {v6, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storeUserProfile"

    move-object/from16 v8, p5

    invoke-static {v8, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storeUserSettings"

    move-object/from16 v10, p6

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "storeExperiments"

    move-object/from16 v9, p7

    invoke-static {v9, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "streamContextService"

    move-object/from16 v7, p8

    invoke-static {v7, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$1;

    move-object v11, p0

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$1;-><init>(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;Lcom/discord/stores/StoreUser;)V

    invoke-virtual {v1, v2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2;

    move-object v2, v1

    move-object v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2;-><init>(Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreUserPresence;Lcom/discord/utilities/streams/StreamContextService;Lcom/discord/stores/StoreUserProfile;Lcom/discord/stores/StoreExperiments;Lcom/discord/stores/StoreUserSettings;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "storeUser.observeMe()\n  \u2026            }\n          }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
