.class public final Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;
.super Lx/m/c/k;
.source "UserStatusPresenceCustomView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;->configurePlaceholderEmoji(Lcom/discord/models/domain/emoji/ModelEmojiUnicode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/facebook/imagepipeline/request/ImageRequestBuilder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;

    invoke-direct {v0}, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;->INSTANCE:Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$configurePlaceholderEmoji$1;->invoke(Lcom/facebook/imagepipeline/request/ImageRequestBuilder;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/facebook/imagepipeline/request/ImageRequestBuilder;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;->access$getCUSTOM_EMOJI_PLACEHOLDER_POSTPROCESSOR$cp()Lcom/discord/utilities/fresco/GrayscalePostprocessor;

    move-result-object v0

    iput-object v0, p1, Lcom/facebook/imagepipeline/request/ImageRequestBuilder;->j:Lf/g/j/r/b;

    return-void
.end method
