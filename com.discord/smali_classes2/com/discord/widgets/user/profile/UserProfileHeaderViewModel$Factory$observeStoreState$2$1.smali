.class public final Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2$1;
.super Ljava/lang/Object;
.source "UserProfileHeaderViewModel.kt"

# interfaces
.implements Lrx/functions/Func6;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2;->call(Lkotlin/Pair;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func6<",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Lcom/discord/widgets/user/presence/ModelRichPresence;",
        "Lcom/discord/utilities/streams/StreamContext;",
        "Lcom/discord/models/domain/ModelUserProfile;",
        "Lcom/discord/models/experiments/domain/Experiment;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $meUser:Lcom/discord/models/domain/ModelUser;

.field public final synthetic $targetUser:Lcom/discord/models/domain/ModelUser;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    iput-object p2, p0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2$1;->$targetUser:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/util/Map;Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;Lcom/discord/models/experiments/domain/Experiment;Ljava/lang/Boolean;)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Lcom/discord/widgets/user/presence/ModelRichPresence;",
            "Lcom/discord/utilities/streams/StreamContext;",
            "Lcom/discord/models/domain/ModelUserProfile;",
            "Lcom/discord/models/experiments/domain/Experiment;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;"
        }
    .end annotation

    move-object v0, p0

    new-instance v10, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;

    iget-object v2, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2$1;->$meUser:Lcom/discord/models/domain/ModelUser;

    iget-object v3, v0, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2$1;->$targetUser:Lcom/discord/models/domain/ModelUser;

    const-string v1, "computedMembers"

    move-object v4, p1

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "userProfile"

    move-object v7, p4

    invoke-static {p4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "allowAnimatedEmojis"

    move-object/from16 v5, p6

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p6 .. p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    move-object v1, v10

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;Lcom/discord/models/experiments/domain/Experiment;Z)V

    return-object v10
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map;

    check-cast p2, Lcom/discord/widgets/user/presence/ModelRichPresence;

    check-cast p3, Lcom/discord/utilities/streams/StreamContext;

    check-cast p4, Lcom/discord/models/domain/ModelUserProfile;

    check-cast p5, Lcom/discord/models/experiments/domain/Experiment;

    check-cast p6, Ljava/lang/Boolean;

    invoke-virtual/range {p0 .. p6}, Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$Factory$observeStoreState$2$1;->call(Ljava/util/Map;Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/models/domain/ModelUserProfile;Lcom/discord/models/experiments/domain/Experiment;Ljava/lang/Boolean;)Lcom/discord/widgets/user/profile/UserProfileHeaderViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
