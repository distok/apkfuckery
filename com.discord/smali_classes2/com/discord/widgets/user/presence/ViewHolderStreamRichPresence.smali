.class public final Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;
.super Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;
.source "ViewHolderStreamRichPresence.kt"


# instance fields
.field private final containerView:Landroid/view/View;

.field private final streamPreview:Lcom/discord/views/StreamPreviewView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "containerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a0a68

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "containerView.findViewById(R.id.stream_preview)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/views/StreamPreviewView;

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->streamPreview:Lcom/discord/views/StreamPreviewView;

    return-void
.end method

.method private final configureStreamPreview(Lcom/discord/utilities/streams/StreamContext;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->getPreview()Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->streamPreview:Lcom/discord/views/StreamPreviewView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->streamPreview:Lcom/discord/views/StreamPreviewView;

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->getJoinability()Lcom/discord/utilities/streams/StreamContext$Joinability;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating()Z

    move-result p1

    invoke-virtual {v1, v0, v2, p1}, Lcom/discord/views/StreamPreviewView;->a(Lcom/discord/stores/StoreApplicationStreamPreviews$StreamPreview;Lcom/discord/utilities/streams/StreamContext$Joinability;Z)V

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->streamPreview:Lcom/discord/views/StreamPreviewView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final getRichPresenceStateText(Ljava/lang/String;Lcom/discord/models/domain/activity/ModelActivityParty;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivityParty;->getMaxSize()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_2

    const v0, 0x7f121947

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivityParty;->getCurrentSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/discord/models/domain/activity/ModelActivityParty;->getMaxSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    move-object v0, p2

    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const-string v0, ""

    :goto_1
    const-string p2, "party?.run {\n      when \u2026    )\n      }\n    } ?: \"\""

    invoke-static {v0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x20

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-static {p1, p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p1}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    const-string p2, "largeIv"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object p1

    const-string v0, "smallIv"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIvWrap()Landroid/view/View;

    move-result-object p1

    const-string v0, "smallIvWrap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public configureTextUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 7

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/discord/utilities/streams/StreamContext;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    :goto_0
    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "headerTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v5

    invoke-static {v5, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f121935

    new-array v6, v4, [Ljava/lang/Object;

    aput-object p2, v6, v3

    invoke-static {v5, v2, v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object p2

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f121934

    invoke-static {p2, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p2

    :goto_1
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string p2, "textContainer"

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->isGameActivity()Z

    move-result v1

    if-ne v1, v4, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->getRichPresenceStateText(Ljava/lang/String;Lcom/discord/models/domain/activity/ModelActivityParty;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTextContainer()Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object p2

    const-string v2, "titleTv"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getDetailsTv()Landroid/widget/TextView;

    move-result-object p2

    const-string v2, "detailsTv"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getStateTv()Landroid/widget/TextView;

    move-result-object p2

    const-string v2, "stateTv"

    invoke-static {p2, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTimeTv()Landroid/widget/TextView;

    move-result-object p2

    const-string v1, "timeTv"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->friendlyTime(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_2
    invoke-static {p2, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTextContainer()Landroid/view/View;

    move-result-object p1

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "streamContext must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public configureUi(Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Landroid/content/Context;ZLcom/discord/models/domain/ModelUser;)V
    .locals 0

    const-string p4, "applicationContext"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeTimer()V

    const/4 p3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence;->getPlayingActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p3

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/utilities/streams/StreamContext;->getJoinability()Lcom/discord/utilities/streams/StreamContext$Joinability;

    move-result-object p3

    :cond_1
    sget-object p4, Lcom/discord/utilities/streams/StreamContext$Joinability;->MISSING_PERMISSIONS:Lcom/discord/utilities/streams/StreamContext$Joinability;

    const/4 p5, 0x0

    if-ne p3, p4, :cond_2

    const/4 p3, 0x1

    goto :goto_1

    :cond_2
    const/4 p3, 0x0

    :goto_1
    if-nez p2, :cond_3

    if-eqz p1, :cond_4

    :cond_3
    if-eqz p3, :cond_5

    :cond_4
    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->containerView:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_5
    iget-object p3, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {p3, p5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->configureTextUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-direct {p0, p2}, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->configureStreamPreview(Lcom/discord/utilities/streams/StreamContext;)V

    return-void
.end method

.method public final setOnStreamPreviewClicked(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onStreamPreviewClicked"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence;->streamPreview:Lcom/discord/views/StreamPreviewView;

    new-instance v1, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence$setOnStreamPreviewClicked$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/user/presence/ViewHolderStreamRichPresence$setOnStreamPreviewClicked$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
