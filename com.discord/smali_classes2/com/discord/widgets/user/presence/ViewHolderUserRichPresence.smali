.class public Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;
.super Ljava/lang/Object;
.source "ViewHolderUserRichPresence.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;
    }
.end annotation


# static fields
.field public static final BASE_RP_TYPE:I = 0x0

.field public static final Companion:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;

.field public static final GAME_RP_TYPE:I = 0x1

.field public static final MUSIC_RP_TYPE:I = 0x2

.field public static final PLATFORM_RP_TYPE:I = 0x3

.field public static final STREAM_RP_TYPE:I = 0x4


# instance fields
.field private final buttons:[Landroid/widget/Button;

.field private final containerView:Landroid/view/View;

.field private final detailsTv:Landroid/widget/TextView;

.field private final headerTv:Landroid/widget/TextView;

.field private final largeIv:Lcom/facebook/drawee/view/SimpleDraweeView;

.field private onActivityCustomButtonClicked:Lkotlin/jvm/functions/Function5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function5<",
            "-",
            "Landroid/content/Context;",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private perSecondTimerSubscription:Lrx/Subscription;

.field private final richPresenceType:I

.field private final smallIv:Landroid/widget/ImageView;

.field private final smallIvWrap:Landroid/view/View;

.field private final stateTv:Landroid/widget/TextView;

.field private final textContainer:Landroid/view/View;

.field private final timeTv:Landroid/widget/TextView;

.field private final titleTv:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->Companion:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;I)V
    .locals 2

    const-string v0, "containerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->containerView:Landroid/view/View;

    iput p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->richPresenceType:I

    const p2, 0x7f0a082f

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/facebook/drawee/view/SimpleDraweeView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->largeIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    const p2, 0x7f0a0830

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIv:Landroid/widget/ImageView;

    const p2, 0x7f0a0831

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIvWrap:Landroid/view/View;

    const p2, 0x7f0a082e

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->headerTv:Landroid/widget/TextView;

    const p2, 0x7f0a083a

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->titleTv:Landroid/widget/TextView;

    const p2, 0x7f0a082d

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->detailsTv:Landroid/widget/TextView;

    const p2, 0x7f0a0839

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->timeTv:Landroid/widget/TextView;

    const p2, 0x7f0a0838

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->stateTv:Landroid/widget/TextView;

    const p2, 0x7f0a082c

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->textContainer:Landroid/view/View;

    const/4 p2, 0x2

    new-array p2, p2, [Landroid/widget/Button;

    const v0, 0x7f0a0835

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x0

    aput-object v0, p2, v1

    const v0, 0x7f0a0836

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    const/4 v0, 0x1

    aput-object p1, p2, v0

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->buttons:[Landroid/widget/Button;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/View;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;-><init>(Landroid/view/View;I)V

    return-void
.end method

.method public static final synthetic access$getPerSecondTimerSubscription$p(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->perSecondTimerSubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$handleActivityCustomButtonClick(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Landroid/content/Context;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->handleActivityCustomButtonClick(Landroid/content/Context;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;I)V

    return-void
.end method

.method public static final synthetic access$setPerSecondTimerSubscription$p(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->perSecondTimerSubscription:Lrx/Subscription;

    return-void
.end method

.method private final handleActivityCustomButtonClick(Landroid/content/Context;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;I)V
    .locals 8

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->getSessionId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string p2, "activity?.sessionId ?: return"

    invoke-static {v5, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object p2

    if-eqz p2, :cond_0

    const-string p3, "activity.applicationId ?: return"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    iget-object v2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->onActivityCustomButtonClicked:Lkotlin/jvm/functions/Function5;

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v3, p1

    invoke-interface/range {v2 .. v7}, Lkotlin/jvm/functions/Function5;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method

.method public static synthetic setImageAndVisibilityBy$default(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Landroid/widget/ImageView;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->setImageAndVisibilityBy(Landroid/widget/ImageView;Ljava/lang/String;Z)V

    return-void

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: setImageAndVisibilityBy"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final setRichPresence(Landroid/view/ViewGroup;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;)Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;
    .locals 1

    sget-object v0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->Companion:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$Companion;->setRichPresence(Landroid/view/ViewGroup;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;)Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 18
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v6, p0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v1

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object v7, v0

    :goto_0
    const-string v8, "smallIvWrap"

    const-string v9, "largeIv"

    const-string v10, "smallIv"

    const/16 v11, 0x8

    if-nez v7, :cond_1

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->largeIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-static {v0, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIv:Landroid/widget/ImageView;

    invoke-static {v0, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIvWrap:Landroid/view/View;

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getLargeImage()Ljava/lang/String;

    move-result-object v14

    const-string v2, "it"

    if-eqz v14, :cond_2

    sget-object v12, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-static {v14, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    move-object v13, v1

    invoke-static/range {v12 .. v17}, Lcom/discord/utilities/icon/IconUtils;->getAssetImage$default(Lcom/discord/utilities/icon/IconUtils;Ljava/lang/Long;Ljava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_3

    invoke-virtual {v7}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getSmallImage()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_3

    sget-object v12, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-static {v14, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    move-object v13, v1

    invoke-static/range {v12 .. v17}, Lcom/discord/utilities/icon/IconUtils;->getAssetImage$default(Lcom/discord/utilities/icon/IconUtils;Ljava/lang/Long;Ljava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    move-object v12, v0

    iget-object v1, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->largeIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-static {v1, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p0

    move-object v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v13

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->setImageAndVisibilityBy$default(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Landroid/widget/ImageView;Ljava/lang/String;ZILjava/lang/Object;)V

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->largeIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    invoke-static {v0, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getLargeText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIv:Landroid/widget/ImageView;

    invoke-static {v0, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v6, v0, v12, v1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->setImageAndVisibilityBy(Landroid/widget/ImageView;Ljava/lang/String;Z)V

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIv:Landroid/widget/ImageView;

    invoke-static {v0, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getSmallText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIvWrap:Landroid/view/View;

    invoke-static {v0, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIv:Landroid/widget/ImageView;

    invoke-static {v2, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    const/4 v11, 0x0

    :cond_5
    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public configureCustomButtonsUi(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;Landroid/content/Context;)V
    .locals 17
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "applicationContext"

    move-object/from16 v8, p3

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/activity/ModelActivity;->getButtons()Ljava/util/List;

    move-result-object v1

    move-object/from16 v10, p0

    move-object v9, v1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    move-object/from16 v10, p0

    :goto_0
    iget-object v11, v10, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->buttons:[Landroid/widget/Button;

    array-length v12, v11

    const/4 v13, 0x0

    const/4 v2, 0x0

    const/4 v14, 0x0

    :goto_1
    if-ge v14, v12, :cond_3

    aget-object v15, v11, v14

    add-int/lit8 v16, v2, 0x1

    if-eqz v9, :cond_1

    invoke-static {v9, v2}, Lx/h/f;->getOrNull(Ljava/util/List;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_2

    :cond_1
    const/4 v1, 0x0

    :goto_2
    const-string v3, "button"

    if-nez v1, :cond_2

    invoke-static {v15, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x8

    invoke-virtual {v15, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_2
    invoke-static {v15, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v15, v13}, Landroid/view/View;->setVisibility(I)V

    new-instance v7, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;

    move-object v1, v7

    move-object/from16 v3, p0

    move-object v4, v9

    move-object/from16 v5, p3

    move-object/from16 v6, p1

    move-object v0, v7

    move-object/from16 v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;-><init>(ILcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Ljava/util/List;Landroid/content/Context;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;)V

    invoke-virtual {v15, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_3
    add-int/lit8 v14, v14, 0x1

    move/from16 v2, v16

    goto :goto_1

    :cond_3
    return-void
.end method

.method public configureTextUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getParty()Lcom/discord/models/domain/activity/ModelActivityParty;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityParty;->getMaxSize()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    :cond_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_1

    const v3, 0x7f121947

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityParty;->getCurrentSize()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityParty;->getMaxSize()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const-string v1, ""

    :goto_1
    const-string v2, "model.party?.run {\n     \u2026)\n        }\n      } ?: \"\""

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-static {p2, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p2}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_4
    move-object p2, v0

    :goto_2
    iget-object v1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->headerTv:Landroid/widget/TextView;

    const-string v2, "headerTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->headerTv:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "headerTv.context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p1}, Lcom/discord/utilities/presence/PresenceUtils;->getActivityHeader(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->titleTv:Landroid/widget/TextView;

    const-string v2, "titleTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->detailsTv:Landroid/widget/TextView;

    const-string v2, "detailsTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->stateTv:Landroid/widget/TextView;

    const-string v1, "stateTv"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getLargeText()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_5
    move-object v1, v0

    :goto_3
    invoke-static {p2, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->timeTv:Landroid/widget/TextView;

    const-string v1, "timeTv"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->friendlyTime(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_6
    invoke-static {p2, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public configureUi(Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Landroid/content/Context;ZLcom/discord/models/domain/ModelUser;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string p4, "applicationContext"

    invoke-static {p3, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeTimer()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p2, :cond_1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->containerView:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object p4, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->containerView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureTextUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    iget-object p4, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->textContainer:Landroid/view/View;

    const-string v0, "textContainer"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p4, v0}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual {p0, p5, p1, p3}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureCustomButtonsUi(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;Landroid/content/Context;)V

    return-void
.end method

.method public configureUiTimestamp(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/app/AppComponent;)V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "appComponent"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->friendlyTime(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->perSecondTimerSubscription:Lrx/Subscription;

    if-nez v0, :cond_2

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4, v5, v0}, Lrx/Observable;->A(JJLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v2, "Observable\n          .in\u20260L, 1L, TimeUnit.SECONDS)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {v0, p2, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureUiTimestamp$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureUiTimestamp$1;-><init>(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureUiTimestamp$2;

    invoke-direct {v9, p0, p1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureUiTimestamp$2;-><init>(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Lcom/discord/models/domain/activity/ModelActivity;)V

    const/16 v10, 0x1a

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_2
    return-void

    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeTimer()V

    return-void
.end method

.method public disposeSubscriptions()V
    .locals 0

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeTimer()V

    return-void
.end method

.method public final disposeTimer()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->perSecondTimerSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->perSecondTimerSubscription:Lrx/Subscription;

    return-void
.end method

.method public final friendlyTime(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)Ljava/lang/CharSequence;
    .locals 11

    const-string v0, "$this$friendlyTime"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getEndMs()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    sget-object v5, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getEndMs()J

    move-result-wide v8

    const p1, 0x7f121949

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v5 .. v10}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyString(JJLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getStartMs()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    sget-object v5, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getStartMs()J

    move-result-wide v6

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v8

    const p1, 0x7f12194b

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v5 .. v10}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyString(JJLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final getDetailsTv()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->detailsTv:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getHeaderTv()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->headerTv:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->largeIv:Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method public final getRichPresenceType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->richPresenceType:I

    return v0
.end method

.method public final getSmallIv()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIv:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getSmallIvWrap()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->smallIvWrap:Landroid/view/View;

    return-object v0
.end method

.method public final getStateTv()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->stateTv:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final getTextContainer()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->textContainer:Landroid/view/View;

    return-object v0
.end method

.method public final getTimeTv()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->timeTv:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTitleTv()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->titleTv:Landroid/widget/TextView;

    return-object v0
.end method

.method public final setImageAndVisibilityBy(Landroid/widget/ImageView;Ljava/lang/String;Z)V
    .locals 10

    const-string v0, "$this$setImageAndVisibilityBy"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x6c

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    const/4 p3, 0x0

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    invoke-static {p2}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    xor-int/2addr p2, v0

    if-eqz p2, :cond_2

    goto :goto_2

    :cond_2
    const/16 p3, 0x8

    :goto_2
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final setOnActivityCustomButtonClicked(Lkotlin/jvm/functions/Function5;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function5<",
            "-",
            "Landroid/content/Context;",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/String;",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newOnActivityCustomButtonClicked"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->onActivityCustomButtonClicked:Lkotlin/jvm/functions/Function5;

    return-void
.end method

.method public setTimeTextViews(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->timeTv:Landroid/widget/TextView;

    const-string v1, "timeTv"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->friendlyTime(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method
