.class public final Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;
.super Ljava/lang/Object;
.source "ModelRichPresence.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/presence/ModelRichPresence$Companion;->get(JLcom/discord/stores/StoreUserPresence;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelPresence;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/user/presence/ModelRichPresence;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;->INSTANCE:Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1;->call(Lcom/discord/models/domain/ModelPresence;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelPresence;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelPresence;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/user/presence/ModelRichPresence;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getApplication()Lcom/discord/stores/StoreApplication;

    move-result-object v0

    invoke-virtual {v1}, Lcom/discord/models/domain/activity/ModelActivity;->getApplicationId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/stores/StoreApplication;->get(Ljava/lang/Long;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1$$special$$inlined$let$lambda$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/user/presence/ModelRichPresence$Companion$get$1$$special$$inlined$let$lambda$1;-><init>(Lcom/discord/models/domain/ModelPresence;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/discord/widgets/user/presence/ModelRichPresence;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v0, v2, v0}, Lcom/discord/widgets/user/presence/ModelRichPresence;-><init>(Lcom/discord/models/domain/ModelPresence;Lcom/discord/models/domain/ModelApplication;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    new-instance p1, Lg0/l/e/j;

    invoke-direct {p1, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    :goto_0
    return-object p1
.end method
