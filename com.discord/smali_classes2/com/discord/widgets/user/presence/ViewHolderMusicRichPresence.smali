.class public final Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;
.super Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;
.source "ViewHolderMusicRichPresence.kt"


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final containerView:Landroid/view/View;

.field private final musicDuration:Landroid/widget/TextView;

.field private final musicElapsed:Landroid/widget/TextView;

.field private final musicSeekBar:Landroid/widget/SeekBar;

.field private final playButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "containerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a0835

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    const v0, 0x7f0a0837

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicSeekBar:Landroid/widget/SeekBar;

    const v0, 0x7f0a0833

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicElapsed:Landroid/widget/TextView;

    const v0, 0x7f0a0832

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicDuration:Landroid/widget/TextView;

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->clock:Lcom/discord/utilities/time/Clock;

    return-void
.end method


# virtual methods
.method public configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object p1

    const-string p2, "smallIv"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIvWrap()Landroid/view/View;

    move-result-object p1

    const-string v0, "smallIvWrap"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public configureUi(Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Landroid/content/Context;ZLcom/discord/models/domain/ModelUser;)V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "applicationContext"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeTimer()V

    iget-object p3, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const/16 v1, 0x8

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->isRichPresence()Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_8

    :cond_1
    iget-object v2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "spotify"

    const/4 v5, 0x1

    invoke-static {v2, v4, v5}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getState()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const/16 v6, 0x3b

    const/16 v7, 0x2c

    const/4 v8, 0x4

    invoke-static {v4, v6, v7, v3, v8}, Lx/s/m;->replace$default(Ljava/lang/String;CCZI)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_2
    move-object v4, v0

    :goto_1
    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "headerTv"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "headerTv.context"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, p1}, Lcom/discord/utilities/presence/PresenceUtils;->getActivityHeader(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "titleTv"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getDetails()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getDetailsTv()Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "detailsTv"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v7, 0x7f121941

    new-array v8, v5, [Ljava/lang/Object;

    aput-object v4, v8, v3

    invoke-virtual {p3, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTimeTv()Landroid/widget/TextView;

    move-result-object v4

    const-string v6, "timeTv"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f121940

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getAssets()Lcom/discord/models/domain/activity/ModelActivityAssets;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/discord/models/domain/activity/ModelActivityAssets;->getLargeText()Ljava/lang/String;

    move-result-object v0

    :cond_3
    aput-object v0, v7, v3

    invoke-virtual {p3, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTextContainer()Landroid/view/View;

    move-result-object v0

    const-string v4, "textContainer"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    const-string v0, "playButton"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_4

    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    const/16 v4, 0x8

    :goto_2
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicSeekBar:Landroid/widget/SeekBar;

    const-string v4, "musicSeekBar"

    invoke-static {p2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_5

    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    const/16 v4, 0x8

    :goto_3
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicDuration:Landroid/widget/TextView;

    const-string v4, "musicDuration"

    invoke-static {p2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_6

    const/4 v4, 0x0

    goto :goto_4

    :cond_6
    const/16 v4, 0x8

    :goto_4
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicElapsed:Landroid/widget/TextView;

    const-string v4, "musicElapsed"

    invoke-static {p2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v2, :cond_7

    const/4 v1, 0x0

    :cond_7
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p4, :cond_8

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f12192b

    invoke-virtual {p3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_6

    :cond_8
    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f121944

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    goto :goto_5

    :cond_9
    const/16 v4, 0x3f

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    :goto_5
    aput-object v4, v2, v3

    invoke-virtual {p3, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_6
    sget-object p2, Lcom/discord/utilities/integrations/SpotifyHelper;->INSTANCE:Lcom/discord/utilities/integrations/SpotifyHelper;

    const-string v0, "context"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Lcom/discord/utilities/integrations/SpotifyHelper;->isSpotifyInstalled(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_a

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object p2

    new-instance p3, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$1;

    invoke-direct {p3, p1}, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$1;-><init>(Lcom/discord/models/domain/activity/ModelActivity;)V

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    new-instance p3, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$2;

    invoke-direct {p3, p1}, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$2;-><init>(Lcom/discord/models/domain/activity/ModelActivity;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p2

    new-instance p3, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$3;

    invoke-direct {p3, p1, p5, p4}, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$3;-><init>(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelUser;Z)V

    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7

    :cond_a
    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->playButton:Landroid/widget/Button;

    sget-object p2, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$4;->INSTANCE:Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence$configureUi$4;

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_7
    return-void

    :cond_b
    :goto_8
    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setTimeTextViews(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)V
    .locals 8

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v0}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getEndMs()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getStartMs()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getEndMs()J

    move-result-wide v4

    cmp-long v6, v0, v4

    if-ltz v6, :cond_0

    move-wide v0, v2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/activity/ModelActivityTimestamps;->getStartMs()J

    move-result-wide v4

    sub-long/2addr v0, v4

    :goto_0
    long-to-double v4, v0

    long-to-double v6, v2

    div-double/2addr v4, v6

    const/high16 p1, 0x42c80000    # 100.0f

    float-to-double v6, p1

    mul-double v4, v4, v6

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicSeekBar:Landroid/widget/SeekBar;

    const-string v6, "musicSeekBar"

    invoke-static {p1, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    double-to-int v4, v4

    invoke-virtual {p1, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicElapsed:Landroid/widget/TextView;

    const-string v4, "musicElapsed"

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v1, v5}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyStringSimple(JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderMusicRichPresence;->musicDuration:Landroid/widget/TextView;

    const-string v0, "musicDuration"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3, v5}, Lcom/discord/utilities/time/TimeUtils;->toFriendlyStringSimple(JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
