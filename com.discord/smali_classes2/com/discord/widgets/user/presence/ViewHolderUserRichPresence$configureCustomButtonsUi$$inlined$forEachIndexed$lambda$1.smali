.class public final Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;
.super Ljava/lang/Object;
.source "ViewHolderUserRichPresence.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureCustomButtonsUi(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $activity$inlined:Lcom/discord/models/domain/activity/ModelActivity;

.field public final synthetic $activityButtons$inlined:Ljava/util/List;

.field public final synthetic $applicationContext$inlined:Landroid/content/Context;

.field public final synthetic $index:I

.field public final synthetic $user$inlined:Lcom/discord/models/domain/ModelUser;

.field public final synthetic this$0:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;


# direct methods
.method public constructor <init>(ILcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Ljava/util/List;Landroid/content/Context;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$index:I

    iput-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->this$0:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    iput-object p3, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$activityButtons$inlined:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$applicationContext$inlined:Landroid/content/Context;

    iput-object p5, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$user$inlined:Lcom/discord/models/domain/ModelUser;

    iput-object p6, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$activity$inlined:Lcom/discord/models/domain/activity/ModelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->this$0:Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$applicationContext$inlined:Landroid/content/Context;

    iget-object v1, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$user$inlined:Lcom/discord/models/domain/ModelUser;

    iget-object v2, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$activity$inlined:Lcom/discord/models/domain/activity/ModelActivity;

    iget v3, p0, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence$configureCustomButtonsUi$$inlined$forEachIndexed$lambda$1;->$index:I

    invoke-static {p1, v0, v1, v2, v3}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->access$handleActivityCustomButtonClick(Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;Landroid/content/Context;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/activity/ModelActivity;I)V

    return-void
.end method
