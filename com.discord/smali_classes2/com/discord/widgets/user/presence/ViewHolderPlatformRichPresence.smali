.class public final Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;
.super Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;
.source "ViewHolderPlatformRichPresence.kt"


# instance fields
.field private final connectButton:Landroid/widget/Button;

.field private final containerView:Landroid/view/View;

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "containerView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    const v0, 0x7f0a0835

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->connectButton:Landroid/widget/Button;

    new-instance p1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method public static final synthetic access$getConnectButton$p(Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->connectButton:Landroid/widget/Button;

    return-object p0
.end method

.method private final configureImages(Lcom/discord/utilities/platform/Platform;Lcom/discord/widgets/user/presence/ModelRichPresence;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    invoke-super {v0, v3, v2}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->configureAssetUi(Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/utilities/streams/StreamContext;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/platform/Platform;->getWhitePlatformImage()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object v4

    const-string v5, "smallIv"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v3, :cond_1

    sget-object v8, Lcom/discord/utilities/platform/Platform;->NONE:Lcom/discord/utilities/platform/Platform;

    if-eq v1, v8, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    const/16 v9, 0x8

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    goto :goto_2

    :cond_2
    const/16 v8, 0x8

    :goto_2
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIvWrap()Landroid/view/View;

    move-result-object v4

    const-string v8, "smallIvWrap"

    invoke-static {v4, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object v8

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_3

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    if-eqz v6, :cond_4

    const/4 v9, 0x0

    :cond_4
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    sget-object v4, Lcom/discord/utilities/platform/Platform;->NONE:Lcom/discord/utilities/platform/Platform;

    if-eq v1, v4, :cond_7

    iget-object v4, v0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->connectButton:Landroid/widget/Button;

    const-string v6, "connectButton"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/platform/Platform;->getColorResId()Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_4

    :cond_5
    const/4 v6, 0x0

    :goto_4
    invoke-static {v4, v6}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/view/View;I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setBackgroundColor(I)V

    if-eqz v3, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v4, v3, v2}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    goto :goto_5

    :cond_6
    move-object v3, v2

    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getSmallIv()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_7
    sget-object v3, Lcom/discord/utilities/platform/Platform;->XBOX:Lcom/discord/utilities/platform/Platform;

    const-string v4, "largeIv"

    if-ne v1, v3, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/platform/Platform;->getPlatformImage()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_8

    sget-object v8, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v9

    invoke-static {v9, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/platform/Platform;->getPlatformImage()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    invoke-static/range {v8 .. v13}, Lcom/discord/utilities/images/MGImages;->setImage$default(Lcom/discord/utilities/images/MGImages;Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    return-void

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_b

    if-eqz p2, :cond_a

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPrimaryApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getId()J

    move-result-wide v8

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplication;->getIcon()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    goto :goto_6

    :cond_9
    const-string v1, ""

    :goto_6
    move-object v10, v1

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    invoke-static/range {v8 .. v13}, Lcom/discord/utilities/icon/IconUtils;->getApplicationIcon$default(JLjava/lang/String;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :cond_a
    move-object v9, v2

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getLargeIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v8

    invoke-static {v8, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7c

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :cond_b
    return-void
.end method


# virtual methods
.method public configureUi(Lcom/discord/widgets/user/presence/ModelRichPresence;Lcom/discord/utilities/streams/StreamContext;Landroid/content/Context;ZLcom/discord/models/domain/ModelUser;)V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string p2, "applicationContext"

    invoke-static {p3, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->disposeSubscriptions()V

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/presence/ModelRichPresence;->getPrimaryActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, p2

    :goto_0
    const/16 p4, 0x8

    if-eqz p3, :cond_7

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->isGamePlatform()Z

    move-result p5

    if-nez p5, :cond_1

    goto/16 :goto_3

    :cond_1
    iget-object p5, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p5, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->isXboxActivity()Z

    move-result p5

    if-eqz p5, :cond_2

    sget-object p5, Lcom/discord/utilities/platform/Platform;->XBOX:Lcom/discord/utilities/platform/Platform;

    goto :goto_2

    :cond_2
    sget-object p5, Lcom/discord/utilities/platform/Platform;->Companion:Lcom/discord/utilities/platform/Platform$Companion;

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->getPlatform()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const-string v1, ""

    :goto_1
    invoke-virtual {p5, v1}, Lcom/discord/utilities/platform/Platform$Companion;->from(Ljava/lang/String;)Lcom/discord/utilities/platform/Platform;

    move-result-object p5

    :goto_2
    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "headerTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getHeaderTv()Landroid/widget/TextView;

    move-result-object v3

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "headerTv.context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, p3}, Lcom/discord/utilities/presence/PresenceUtils;->getActivityHeader(Landroid/content/Context;Lcom/discord/models/domain/activity/ModelActivity;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTitleTv()Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "titleTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTimeTv()Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "timeTv"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/discord/models/domain/activity/ModelActivity;->getTimestamps()Lcom/discord/models/domain/activity/ModelActivityTimestamps;

    move-result-object p3

    if-eqz p3, :cond_4

    invoke-virtual {p0, p3}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->friendlyTime(Lcom/discord/models/domain/activity/ModelActivityTimestamps;)Ljava/lang/CharSequence;

    move-result-object p2

    :cond_4
    invoke-static {v1, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->getTextContainer()Landroid/view/View;

    move-result-object p2

    const-string p3, "textContainer"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Landroid/view/View;->setSelected(Z)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->connectButton:Landroid/widget/Button;

    const-string v1, "connectButton"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/discord/utilities/platform/Platform;->getEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 p4, 0x0

    :cond_5
    invoke-virtual {p2, p4}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->connectButton:Landroid/widget/Button;

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p4, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p4

    const v1, 0x7f12192e

    new-array p3, p3, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/discord/utilities/platform/Platform;->getProperName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p3, v0

    invoke-virtual {p4, v1, p3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p5}, Lcom/discord/utilities/platform/Platform;->getEnabled()Z

    move-result p2

    if-eqz p2, :cond_6

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUserConnections()Lcom/discord/stores/StoreUserConnections;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreUserConnections;->getConnectedAccounts()Lrx/Observable;

    move-result-object p2

    new-instance p3, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$1;

    invoke-direct {p3, p5}, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$1;-><init>(Lcom/discord/utilities/platform/Platform;)V

    invoke-virtual {p2, p3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    invoke-virtual {p2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object p2

    const-string p3, "StoreStream\n          .g\u2026  .distinctUntilChanged()"

    invoke-static {p2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;

    const/4 v2, 0x0

    new-instance v3, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$2;

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v3, p2}, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$2;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$3;

    invoke-direct {v6, p0}, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$3;-><init>(Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;)V

    const/16 v7, 0x1a

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p2, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->connectButton:Landroid/widget/Button;

    new-instance p3, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$4;

    invoke-direct {p3, p5}, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence$configureUi$4;-><init>(Lcom/discord/utilities/platform/Platform;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    invoke-direct {p0, p5, p1}, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->configureImages(Lcom/discord/utilities/platform/Platform;Lcom/discord/widgets/user/presence/ModelRichPresence;)V

    return-void

    :cond_7
    :goto_3
    iget-object p1, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->containerView:Landroid/view/View;

    invoke-virtual {p1, p4}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public disposeSubscriptions()V
    .locals 1

    invoke-super {p0}, Lcom/discord/widgets/user/presence/ViewHolderUserRichPresence;->disposeSubscriptions()V

    iget-object v0, p0, Lcom/discord/widgets/user/presence/ViewHolderPlatformRichPresence;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->b()V

    return-void
.end method
