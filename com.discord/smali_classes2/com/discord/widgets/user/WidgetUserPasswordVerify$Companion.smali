.class public final Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;
.super Ljava/lang/Object;
.source "WidgetUserPasswordVerify.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/WidgetUserPasswordVerify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;-><init>()V

    return-void
.end method

.method private final start(Lcom/discord/app/AppFragment;Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "Account Settings Password Verification"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->openModal(Ljava/lang/String;Ljava/lang/String;)V

    const-class v0, Lcom/discord/widgets/user/WidgetUserPasswordVerify;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object p2

    const/16 v1, 0xfa0

    invoke-static {p1, v0, p2, v1}, Lf/a/b/m;->f(Landroidx/fragment/app/Fragment;Ljava/lang/Class;Landroid/content/Intent;I)V

    return-void
.end method

.method public static synthetic startUpdateAccountSettings$default(Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;Lcom/discord/app/AppFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    move-object p4, v0

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;->startUpdateAccountSettings(Lcom/discord/app/AppFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final saveCompletedSuccessfully(II)Z
    .locals 1

    const/16 v0, 0xfa0

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final startRemovePhoneNumber(Lcom/discord/app/AppFragment;)V
    .locals 3

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-object v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->RemovePhoneNumber:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    const-string v2, "INTENT_EXTRA_ACTION"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;->start(Lcom/discord/app/AppFragment;Landroid/os/Bundle;)V

    return-void
.end method

.method public final startUpdateAccountSettings(Lcom/discord/app/AppFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-object v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->UpdateAccountInfo:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    const-string v2, "INTENT_EXTRA_ACTION"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "INTENT_EXTRA_EMAIL"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "INTENT_EXTRA_USERNAME"

    invoke-virtual {v0, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "INTENT_EXTRA_DISCRIMINATOR"

    invoke-virtual {v0, p2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;->start(Lcom/discord/app/AppFragment;Landroid/os/Bundle;)V

    return-void
.end method

.method public final startUpdatePhoneNumber(Lcom/discord/app/AppFragment;Ljava/lang/String;)V
    .locals 3

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneToken"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-object v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->UpdatePhoneNumber:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    const-string v2, "INTENT_EXTRA_ACTION"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "INTENT_EXTRA_PHONE_TOKEN"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;->start(Lcom/discord/app/AppFragment;Landroid/os/Bundle;)V

    return-void
.end method
