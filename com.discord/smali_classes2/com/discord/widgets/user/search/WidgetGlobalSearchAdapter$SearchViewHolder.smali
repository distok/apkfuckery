.class public abstract Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter$SearchViewHolder;
.super Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter$Item;
.source "WidgetGlobalSearchAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SearchViewHolder"
.end annotation


# instance fields
.field private final viewGlobalSearchItem:Lcom/discord/widgets/user/search/ViewGlobalSearchItem;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d0193

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter$Item;-><init>(ILcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v0, "null cannot be cast to non-null type com.discord.widgets.user.search.ViewGlobalSearchItem"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setSelected(Z)V

    iput-object p1, p0, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter$SearchViewHolder;->viewGlobalSearchItem:Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    return-void
.end method


# virtual methods
.method public final getViewGlobalSearchItem()Lcom/discord/widgets/user/search/ViewGlobalSearchItem;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter$SearchViewHolder;->viewGlobalSearchItem:Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    return-object v0
.end method
