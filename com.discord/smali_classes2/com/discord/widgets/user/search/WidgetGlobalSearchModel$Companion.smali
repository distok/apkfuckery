.class public final Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;
.super Ljava/lang/Object;
.source "WidgetGlobalSearchModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;-><init>()V

    return-void
.end method

.method public static synthetic create$default(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;
    .locals 7

    and-int/lit8 p8, p7, 0x10

    if-eqz p8, :cond_0

    const/4 p5, 0x0

    :cond_0
    move-object v5, p5

    and-int/lit8 p5, p7, 0x20

    if-eqz p5, :cond_1

    sget-object p6, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$1;

    :cond_1
    move-object v6, p6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->create(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    move-result-object p0

    return-object p0
.end method

.method private final get(Lrx/Observable;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/Observable;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelChannel;",
            "-",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;",
            "Ljava/lang/Long;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Lg0/l/e/j;

    const-string v1, ""

    invoke-direct {v0, v1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    sget-object v11, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuildSelected;->observeRecentSelectedGuildIds()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreChannelsSelected;->observeId()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreChannelsSelected;->observePreviousId()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreVoiceChannelSelected;->observeSelectedVoiceChannelId()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getMessagesMostRecent()Lcom/discord/stores/StoreMessagesMostRecent;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/stores/StoreMessagesMostRecent;->get()Lrx/Observable;

    move-result-object v5

    const-wide/16 v6, 0xa

    sget-object v15, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v5, v6, v7, v15}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->leadingEdgeThrottle(Lrx/Observable;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getMentions()Lcom/discord/stores/StoreMentions;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/stores/StoreMentions;->getCounts()Lrx/Observable;

    move-result-object v6

    sget-object v7, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$partialSearchContextObservable$1;

    if-eqz v7, :cond_0

    new-instance v8, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func7$0;

    invoke-direct {v8, v7}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func7$0;-><init>(Lkotlin/jvm/functions/Function7;)V

    move-object v7, v8

    :cond_0
    check-cast v7, Lrx/functions/Func7;

    const-wide/16 v8, 0x2

    move-object v10, v15

    invoke-static/range {v0 .. v10}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$searchContextObservable$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$searchContextObservable$1;

    const-wide/16 v4, 0xfa

    sget-object v25, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v1, p1

    move-object/from16 v6, v25

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeAllUsers()Lrx/Observable;

    move-result-object v12

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getPresences()Lcom/discord/stores/StoreUserPresence;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserPresence;->observeAllPresences()Lrx/Observable;

    move-result-object v13

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v14

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreGuilds;->observeComputed()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$usersContextObservable$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$usersContextObservable$1;

    if-eqz v2, :cond_1

    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func4$0;

    invoke-direct {v3, v2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    move-object v2, v3

    :cond_1
    move-object/from16 v16, v2

    check-cast v16, Lrx/functions/Func4;

    const-wide/16 v17, 0xa

    move-object v2, v15

    move-object v15, v1

    move-object/from16 v19, v2

    invoke-static/range {v12 .. v19}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreGuilds;->observeGuilds()Lrx/Observable;

    move-result-object v12

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreChannels;->observeAllChannels()Lrx/Observable;

    move-result-object v13

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getPermissions()Lcom/discord/stores/StorePermissions;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StorePermissions;->observePermissionsForAllChannels()Lrx/Observable;

    move-result-object v14

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreChannels;->observePrivateChannels()Lrx/Observable;

    move-result-object v15

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreChannels;->observeIds()Lrx/Observable;

    move-result-object v16

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreReadStates;->getUnreadChannelIds()Lrx/Observable;

    move-result-object v17

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreReadStates;->getUnreadGuildIds()Lrx/Observable;

    move-result-object v18

    sget-object v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$channelContextObservable$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$channelContextObservable$1;

    if-eqz v3, :cond_2

    new-instance v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func7$0;

    invoke-direct {v4, v3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func7$0;-><init>(Lkotlin/jvm/functions/Function7;)V

    move-object v3, v4

    :cond_2
    move-object/from16 v19, v3

    check-cast v19, Lrx/functions/Func7;

    const-wide/16 v20, 0x3

    move-object/from16 v22, v2

    invoke-static/range {v12 .. v22}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$channelContextObservable$2;

    move-object/from16 v5, p2

    invoke-direct {v4, v5}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$channelContextObservable$2;-><init>(Lkotlin/jvm/functions/Function2;)V

    invoke-virtual {v3, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    invoke-virtual {v3}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreUserGuildSettings;->get()Lrx/Observable;

    move-result-object v14

    invoke-virtual {v11}, Lcom/discord/stores/StoreStream$Companion;->getGuildsSorted()Lcom/discord/stores/StoreGuildsSorted;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreGuildsSorted;->getFlat()Lrx/Observable;

    move-result-object v15

    sget-object v16, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$guildsListObservable$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$guildsListObservable$1;

    const-wide/16 v17, 0x5

    move-object v12, v0

    move-object v13, v3

    move-object/from16 v19, v2

    invoke-static/range {v12 .. v19}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v19

    new-instance v2, Lg0/l/e/j;

    move-object/from16 v4, p3

    invoke-direct {v2, v4}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    new-instance v4, Lg0/l/e/j;

    move-object/from16 v5, p4

    invoke-direct {v4, v5}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    new-instance v5, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$1;

    move-object/from16 v6, p0

    invoke-direct {v5, v6}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$get$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;)V

    new-instance v7, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func6$0;

    invoke-direct {v7, v5}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$sam$rx_functions_Func6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    const-wide/16 v23, 0xc8

    move-object/from16 v16, v0

    move-object/from16 v17, v1

    move-object/from16 v18, v3

    move-object/from16 v20, v2

    move-object/from16 v21, v4

    move-object/from16 v22, v7

    invoke-static/range {v16 .. v25}, Lcom/discord/utilities/rx/ObservableWithLeadingEdgeThrottle;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "ObservableWithLeadingEdg\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic get$default(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;Lrx/Observable;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->get(Lrx/Observable;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final getDefaultUserSearch(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Ljava/util/Set;)Lkotlin/sequences/Sequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)",
            "Lkotlin/sequences/Sequence<",
            "Lcom/discord/models/domain/ModelUser;",
            ">;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->getRecentChannelIds(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)Lkotlin/sequences/Sequence;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$recentDmUserIds$1;

    invoke-direct {v0, p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$recentDmUserIds$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;)V

    invoke-static {p1, v0}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-virtual {p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;->getSmallGuildIds()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$smallGuildUserIds$1;

    invoke-direct {v0, p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$smallGuildUserIds$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;)V

    invoke-static {p2, v0}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p2

    const-string v0, "$this$flatten"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lx/r/m;->d:Lx/r/m;

    invoke-static {p2, v1}, Lf/h/a/f/f/n/g;->q(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p2

    const-string v1, "$this$plus"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "elements"

    invoke-static {p4, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/sequences/Sequence;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p4}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    const/4 p4, 0x1

    aput-object p1, v1, p4

    invoke-static {v1}, Lf/h/a/f/f/n/g;->sequenceOf([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p4, Lx/r/l;->d:Lx/r/l;

    invoke-static {p1, p4}, Lf/h/a/f/f/n/g;->q(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {p1, p2}, Lx/r/q;->plus(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p1

    const-string p2, "$this$distinct"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p2, Lx/r/r;->d:Lx/r/r;

    const-string p4, "$this$distinctBy"

    invoke-static {p1, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "selector"

    invoke-static {p2, p4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lx/r/c;

    invoke-direct {p4, p1, p2}, Lx/r/c;-><init>(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)V

    new-instance p1, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$1;

    invoke-direct {p1, p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;)V

    invoke-static {p4, p1}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    const/16 p2, 0x64

    invoke-static {p1, p2}, Lx/r/q;->take(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$2;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getDefaultUserSearch$2;

    invoke-static {p1, p2}, Lx/r/q;->filterNot(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic getForSend$default(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;Lrx/Observable;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/Observable;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->getForSend(Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final getRecentChannelIds(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;",
            ")",
            "Lkotlin/sequences/Sequence<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getFrecencyChannels()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getFrecencyChannels()Ljava/util/Collection;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getMostRecent()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getRecentChannelIds$$inlined$sortedBy$1;

    invoke-direct {v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getRecentChannelIds$$inlined$sortedBy$1;-><init>()V

    invoke-static {p1, v0}, Lx/h/f;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    const/16 v0, 0x32

    invoke-static {p1, v0}, Lx/r/q;->take(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getRecentChannelIds$2;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getRecentChannelIds$2;

    invoke-static {p1, v0}, Lx/r/q;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final create(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    const-string v1, "searchContext"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "usersContext"

    invoke-static {v7, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "channelContext"

    invoke-static {v8, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "widgetGuildsListModel"

    move-object/from16 v11, p4

    invoke-static {v11, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "lastChannelIdProvider"

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v12, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$2;

    invoke-direct {v12, v7, v0, v8}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$2;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;)V

    new-instance v1, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$3;

    invoke-direct {v1, v8, v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$3;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)V

    new-instance v13, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;

    invoke-direct {v13, v8, v12, v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$2;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getSearchType()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getSanitizedFilter()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;->getComputed()Ljava/util/Map;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getRecentGuildIds()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lx/h/f;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lx/h/n;->d:Lx/h/n;

    :goto_0
    move-object v6, v2

    const/4 v5, 0x2

    const/4 v4, 0x1

    if-eq v14, v4, :cond_5

    if-eq v14, v5, :cond_4

    const/4 v2, 0x3

    if-eq v14, v2, :cond_3

    const/4 v1, 0x4

    if-eq v14, v1, :cond_2

    invoke-static {v15}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->getRecentChannelIds(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)Lkotlin/sequences/Sequence;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getMentionCounts()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v1}, Lx/r/q;->asIterable(Lkotlin/sequences/Sequence;)Ljava/lang/Iterable;

    move-result-object v1

    const-string v3, "$this$union"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "other"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lx/h/f;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v2

    invoke-static {v2, v1}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    invoke-static {v2}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$9;

    invoke-direct {v2, v8}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$9;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;)V

    invoke-static {v1, v2}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$10;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$10;

    invoke-static {v1, v2}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$11;

    invoke-direct {v2, v13, v15}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$11;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    goto/16 :goto_1

    :cond_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;->getChannels()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$channelResults$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$channelResults$1;

    invoke-static {v1, v2}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    new-instance v1, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$channelResults$2;

    move-object/from16 v16, v1

    move-object v11, v2

    move-object v2, v3

    move-object v10, v3

    move-object v3, v12

    move-object/from16 v4, p2

    move-object v5, v15

    move-object/from16 v19, v6

    move-object v6, v13

    invoke-direct/range {v1 .. v6}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$channelResults$2;-><init>(Ljava/util/HashSet;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$2;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Ljava/lang/String;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;)V

    invoke-static {v11, v1}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    invoke-static/range {v19 .. v19}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$selectedGuildUserResults$1;

    invoke-direct {v3, v10}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$selectedGuildUserResults$1;-><init>(Ljava/util/HashSet;)V

    invoke-static {v2, v3}, Lx/r/q;->filterNot(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$selectedGuildUserResults$2;

    invoke-direct {v3, v12, v7, v15}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$selectedGuildUserResults$2;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$2;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Ljava/lang/String;)V

    invoke-static {v2, v3}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    invoke-static {v1, v2}, Lx/r/q;->plus(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;->getChannels()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$5;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$5;

    invoke-static {v1, v2}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$6;

    invoke-direct {v2, v8}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$6;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;)V

    invoke-static {v1, v2}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$7;

    invoke-direct {v2, v13, v15}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$7;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$$inlined$sortedBy$1;

    invoke-direct {v2, v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$$inlined$sortedBy$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)V

    invoke-static {v1, v2}, Lx/r/q;->sortedWith(Lkotlin/sequences/Sequence;Ljava/util/Comparator;)Lkotlin/sequences/Sequence;

    move-result-object v1

    goto :goto_1

    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;->getGuilds()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$2;

    invoke-direct {v3, v1, v15}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$2;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$3;Ljava/lang/String;)V

    invoke-static {v2, v3}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;->getChannels()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$3;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$3;

    invoke-static {v1, v2}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$4;

    invoke-direct {v2, v13, v15}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$4;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    :goto_1
    move-object v2, v1

    move-object/from16 v1, p0

    goto :goto_3

    :cond_5
    move-object/from16 v19, v6

    invoke-static {v15}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v1, v0, v8, v7, v2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->getDefaultUserSearch(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;Ljava/util/Set;)Lkotlin/sequences/Sequence;

    move-result-object v2

    goto :goto_2

    :cond_6
    move-object/from16 v1, p0

    invoke-virtual/range {p2 .. p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$UsersContext;->getUsers()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    :goto_2
    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$1;

    invoke-direct {v3, v12, v15}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$filteredResults$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$2;Ljava/lang/String;)V

    invoke-static {v2, v3}, Lx/r/q;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    :goto_3
    if-eqz v9, :cond_7

    invoke-static {v2, v9}, Lx/r/q;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    :cond_7
    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$$inlined$sortedByDescending$1;

    invoke-direct {v3, v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$$inlined$sortedByDescending$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;)V

    invoke-static {v2, v3}, Lx/r/q;->sortedWith(Lkotlin/sequences/Sequence;Ljava/util/Comparator;)Lkotlin/sequences/Sequence;

    move-result-object v2

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lx/r/q;->take(Lkotlin/sequences/Sequence;I)Lkotlin/sequences/Sequence;

    move-result-object v2

    const/4 v4, 0x0

    if-nez v14, :cond_8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$SearchContext;->getFilter()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    goto :goto_4

    :cond_8
    const/4 v3, 0x0

    :goto_4
    if-eqz v3, :cond_b

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ChannelContext;->getChannels()Ljava/util/Map;

    move-result-object v3

    move-object/from16 v5, p6

    invoke-interface {v5, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_9

    const-string v3, ""

    invoke-virtual {v13, v0, v3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$4;->invoke(Lcom/discord/models/domain/ModelChannel;Ljava/lang/String;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v3

    goto :goto_5

    :cond_9
    const/4 v3, 0x0

    :goto_5
    if-nez v3, :cond_a

    const/4 v5, 0x1

    new-array v0, v5, [Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    const v6, 0x7f1217a8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x0

    move-object v5, v3

    invoke-direct/range {v5 .. v10}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;-><init>(IIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v3, v0, v4

    invoke-static {v0}, Lf/h/a/f/f/n/g;->sequenceOf([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-static {v0, v2}, Lx/r/q;->plus(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v0

    goto :goto_6

    :cond_a
    const/4 v5, 0x1

    const/4 v6, 0x2

    new-array v6, v6, [Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    new-instance v13, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    const v8, 0x7f1214c1

    const/16 v19, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x6

    const/4 v12, 0x0

    const/4 v9, 0x0

    move-object v7, v13

    invoke-direct/range {v7 .. v12}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;-><init>(IIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v13, v6, v4

    aput-object v3, v6, v5

    invoke-static {v6}, Lf/h/a/f/f/n/g;->sequenceOf([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object v3

    new-array v5, v5, [Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    new-instance v6, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    const v17, 0x7f1217a8

    const/16 v18, 0x0

    const/16 v20, 0x6

    const/16 v21, 0x0

    move-object/from16 v16, v6

    invoke-direct/range {v16 .. v21}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;-><init>(IIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v6, v5, v4

    invoke-static {v5}, Lf/h/a/f/f/n/g;->sequenceOf([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object v4

    invoke-static {v3, v4}, Lx/r/q;->plus(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v3

    new-instance v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$results$1;

    invoke-direct {v4, v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$create$results$1;-><init>(Lcom/discord/models/domain/ModelChannel;)V

    invoke-static {v2, v4}, Lx/r/q;->filterNot(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-static {v3, v0}, Lx/r/q;->plus(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v0

    :goto_6
    new-instance v2, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    invoke-static {v0}, Lx/r/q;->toList(Lkotlin/sequences/Sequence;)Ljava/util/List;

    move-result-object v0

    invoke-virtual/range {p4 .. p4}, Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v15, v14, v0, v3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;-><init>(Ljava/lang/String;ILjava/util/List;Ljava/util/List;)V

    goto :goto_7

    :cond_b
    const/4 v5, 0x1

    invoke-static {v2}, Lx/r/q;->firstOrNull(Lkotlin/sequences/Sequence;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_c

    new-array v0, v5, [Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    new-instance v3, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;

    const v5, 0x7f1217a8

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    move-object/from16 p1, v3

    move/from16 p2, v5

    move/from16 p3, v6

    move/from16 p4, v7

    move/from16 p5, v8

    move-object/from16 p6, v9

    invoke-direct/range {p1 .. p6}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemHeader;-><init>(IIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    aput-object v3, v0, v4

    invoke-static {v0}, Lf/h/a/f/f/n/g;->sequenceOf([Ljava/lang/Object;)Lkotlin/sequences/Sequence;

    move-result-object v0

    invoke-static {v0, v2}, Lx/r/q;->plus(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v2

    :cond_c
    new-instance v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    invoke-static {v2}, Lx/r/q;->toList(Lkotlin/sequences/Sequence;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v0

    move-object v3, v15

    move v4, v14

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;-><init>(Ljava/lang/String;ILjava/util/List;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_7
    return-object v2
.end method

.method public final getEMPTY_MATCH_RESULT()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;
    .locals 4

    new-instance v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;-><init>(Ljava/lang/CharSequence;II)V

    return-object v0
.end method

.method public final getForNav(Lrx/Observable;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;",
            ">;"
        }
    .end annotation

    const-string v0, "filterPublisher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;->access$getDEFAULT_PERMISSIONS_PREDICATE$cp()Lkotlin/jvm/functions/Function2;

    move-result-object v3

    sget-object v5, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getForNav$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getForNav$1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v7}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->get$default(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;Lrx/Observable;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final getForSend(Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;",
            ">;"
        }
    .end annotation

    const-string v0, "filterPublisher"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getForSend$1;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getForSend$1;

    sget-object v1, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getForSend$2;->INSTANCE:Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion$getForSend$2;

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$Companion;->get(Lrx/Observable;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final toFuzzyMatchedResult(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;
    .locals 5

    const-string v0, "$this$toFuzzyMatchedResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filter"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/search/SearchUtils;->INSTANCE:Lcom/discord/utilities/search/SearchUtils;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "Locale.ROOT"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/string/StringUtilsKt;->stripAccents(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/discord/utilities/search/SearchUtils;->fuzzyMatch(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;

    const/4 v1, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    invoke-direct {v0, p1, v1, p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;-><init>(Ljava/lang/CharSequence;II)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final toMatchedResult(Ljava/lang/String;Ljava/lang/String;)Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;
    .locals 3

    const-string v0, "$this$toMatchedResult"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "filter"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-static {p1, p2, v0, v1, v2}, Lx/s/r;->indexOf$default(Ljava/lang/CharSequence;Ljava/lang/String;IZI)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v1, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    invoke-direct {v1, p1, v0, p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$MatchedResult;-><init>(Ljava/lang/CharSequence;II)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method
