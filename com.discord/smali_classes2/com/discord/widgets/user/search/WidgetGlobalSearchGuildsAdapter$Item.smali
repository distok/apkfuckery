.class public abstract Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetGlobalSearchGuildsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;",
        "Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsModel$Item;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(ILcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/LayoutRes;
        .end annotation
    .end param

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    new-instance p1, Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item$1;

    invoke-direct {p1, p0, p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;)V

    const/4 v0, 0x0

    new-array v1, v0, [Landroid/view/View;

    invoke-virtual {p0, p1, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    new-instance p1, Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item$2;

    invoke-direct {p1, p0, p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item$2;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter$Item;Lcom/discord/widgets/user/search/WidgetGlobalSearchGuildsAdapter;)V

    new-array p2, v0, [Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->setOnLongClickListener(Lrx/functions/Action3;[Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final configureMentionsCount(Landroid/widget/TextView;I)V
    .locals 6

    const-string v0, "textView"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ge p2, v0, :cond_0

    invoke-static {p1, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "context"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f040454

    const/4 v5, 0x2

    invoke-static {v3, v4, v2, v5, v1}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    const v1, 0x7f121035

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v0, v2

    invoke-virtual {v3, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
