.class public final Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1$1;
.super Lx/m/c/k;
.source "WidgetUserMentions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->call(Landroid/view/MenuItem;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1$1;->this$0:Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1$1;->invoke(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;)V
    .locals 1

    const-string v0, "filters"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1$1;->this$0:Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;

    iget-object v0, v0, Lcom/discord/widgets/user/WidgetUserMentions$configureToolbar$1;->this$0:Lcom/discord/widgets/user/WidgetUserMentions;

    invoke-static {v0}, Lcom/discord/widgets/user/WidgetUserMentions;->access$getMentionsLoader$p(Lcom/discord/widgets/user/WidgetUserMentions;)Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader;->setFilters(Lcom/discord/widgets/user/WidgetUserMentions$Model$MessageLoader$Filters;)V

    return-void
.end method
