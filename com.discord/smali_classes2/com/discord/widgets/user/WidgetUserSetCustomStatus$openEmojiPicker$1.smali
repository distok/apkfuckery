.class public final Lcom/discord/widgets/user/WidgetUserSetCustomStatus$openEmojiPicker$1;
.super Ljava/lang/Object;
.source "WidgetUserSetCustomStatus.kt"

# interfaces
.implements Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->openEmojiPicker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/user/WidgetUserSetCustomStatus;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$openEmojiPicker$1;->this$0:Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEmojiPicked(Lcom/discord/models/domain/emoji/Emoji;)V
    .locals 1

    const-string v0, "emoji"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$openEmojiPicker$1;->this$0:Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    invoke-static {v0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->access$getViewModel$p(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;->setStatusEmoji(Lcom/discord/models/domain/emoji/Emoji;)V

    return-void
.end method
