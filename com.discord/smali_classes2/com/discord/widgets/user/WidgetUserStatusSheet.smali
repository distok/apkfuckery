.class public final Lcom/discord/widgets/user/WidgetUserStatusSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetUserStatusSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;


# instance fields
.field private final custom$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dnd$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final idle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final invisible$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final online$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/user/WidgetUserStatusSheet;

    const-string v3, "online"

    const-string v4, "getOnline()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserStatusSheet;

    const-string v6, "idle"

    const-string v7, "getIdle()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserStatusSheet;

    const-string v6, "dnd"

    const-string v7, "getDnd()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserStatusSheet;

    const-string v6, "invisible"

    const-string v7, "getInvisible()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserStatusSheet;

    const-string v6, "custom"

    const-string v7, "getCustom()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->Companion:Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a0b51

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->online$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b4f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->idle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b4e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->dnd$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b50

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->invisible$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0b4d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->custom$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$clearCustomStatus(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->clearCustomStatus()V

    return-void
.end method

.method public static final synthetic access$openCustomStatusAndDismiss(Lcom/discord/widgets/user/WidgetUserStatusSheet;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->openCustomStatusAndDismiss(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$updateStateAndDismiss(Lcom/discord/widgets/user/WidgetUserStatusSheet;Lcom/discord/models/domain/ModelPresence$Status;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->updateStateAndDismiss(Lcom/discord/models/domain/ModelPresence$Status;)V

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/discord/widgets/user/WidgetUserStatusSheet;Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->updateView(Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;)V

    return-void
.end method

.method private final clearCustomStatus()V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->viewModel:Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;->clearCustomStatus()V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final getCustom()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->custom$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserStatusSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;

    return-object v0
.end method

.method private final getDnd()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->dnd$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserStatusSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getIdle()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->idle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserStatusSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getInvisible()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->invisible$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserStatusSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getOnline()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->online$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserStatusSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final openCustomStatusAndDismiss(Landroid/content/Context;)V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    new-instance v8, Lcom/discord/utilities/analytics/Traits$Source;

    const/4 v1, 0x0

    const-string v2, "Account Panel"

    const-string v3, "Avatar"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x19

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/discord/utilities/analytics/Traits$Source;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v1, "Custom Status Modal"

    invoke-virtual {v0, v1, v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->openModal(Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Source;)V

    sget-object v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->Companion:Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;

    invoke-virtual {v0, p1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;->launch(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method

.method private final setupPresenceLayout(Landroid/view/ViewGroup;IILjava/lang/Integer;)V
    .locals 1
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const v0, 0x7f0a0b4a

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    const p2, 0x7f0a0b4c

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(I)V

    const p2, 0x7f0a0b4b

    if-eqz p4, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById<View>(R.id.\u2026status_presence_subtitle)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public static synthetic setupPresenceLayout$default(Lcom/discord/widgets/user/WidgetUserStatusSheet;Landroid/view/ViewGroup;IILjava/lang/Integer;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->setupPresenceLayout(Landroid/view/ViewGroup;IILjava/lang/Integer;)V

    return-void
.end method

.method private final updateStateAndDismiss(Lcom/discord/models/domain/ModelPresence$Status;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->viewModel:Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;->setStatus(Lcom/discord/models/domain/ModelPresence$Status;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final updateView(Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getCustom()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;

    move-result-object v0

    check-cast p1, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$ViewState$Loaded;->getCustomStatusViewState()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;->updateViewState(Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView$ViewState;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02d1

    return v0
.end method

.method public onPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppBottomSheet;->onResume()V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->viewModel:Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/user/WidgetUserStatusSheet;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/user/WidgetUserStatusSheet$onResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onResume$1;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$Factory;

    invoke-direct {p2}, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, p2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p2, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;

    invoke-virtual {p1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->viewModel:Lcom/discord/widgets/user/WidgetUserStatusSheetViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getOnline()Landroid/view/ViewGroup;

    move-result-object v1

    const v2, 0x7f080440

    const v3, 0x7f1216f1

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->setupPresenceLayout$default(Lcom/discord/widgets/user/WidgetUserStatusSheet;Landroid/view/ViewGroup;IILjava/lang/Integer;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getOnline()Landroid/view/ViewGroup;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getIdle()Landroid/view/ViewGroup;

    move-result-object v1

    const v2, 0x7f08043e

    const v3, 0x7f1216ed

    invoke-static/range {v0 .. v6}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->setupPresenceLayout$default(Lcom/discord/widgets/user/WidgetUserStatusSheet;Landroid/view/ViewGroup;IILjava/lang/Integer;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getIdle()Landroid/view/ViewGroup;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getDnd()Landroid/view/ViewGroup;

    move-result-object p1

    const p2, 0x7f1216ec

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const v0, 0x7f08043d

    const v1, 0x7f1216eb

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->setupPresenceLayout(Landroid/view/ViewGroup;IILjava/lang/Integer;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getDnd()Landroid/view/ViewGroup;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$3;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$3;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getInvisible()Landroid/view/ViewGroup;

    move-result-object p1

    const p2, 0x7f1216ef

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const v0, 0x7f08043f

    const v1, 0x7f1216ee

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->setupPresenceLayout(Landroid/view/ViewGroup;IILjava/lang/Integer;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getInvisible()Landroid/view/ViewGroup;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$4;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$4;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getCustom()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$5;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$5;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet;->getCustom()Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$6;

    invoke-direct {p2, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$onViewCreated$6;-><init>(Lcom/discord/widgets/user/WidgetUserStatusSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/user/profile/UserStatusPresenceCustomView;->setOnClear(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
