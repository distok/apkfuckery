.class public final enum Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;
.super Ljava/lang/Enum;
.source "WidgetUserPasswordVerify.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

.field public static final enum RemovePhoneNumber:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

.field public static final enum UpdateAccountInfo:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

.field public static final enum UpdatePhoneNumber:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    new-instance v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    const-string v2, "UpdateAccountInfo"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->UpdateAccountInfo:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    const-string v2, "RemovePhoneNumber"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->RemovePhoneNumber:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    const-string v2, "UpdatePhoneNumber"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->UpdatePhoneNumber:Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->$VALUES:[Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;
    .locals 1

    const-class v0, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;
    .locals 1

    sget-object v0, Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->$VALUES:[Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    invoke-virtual {v0}, [Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/user/WidgetUserPasswordVerify$Companion$Action;

    return-object v0
.end method
