.class public final Lcom/discord/widgets/user/WidgetUserPasswordVerify$updateAccountInfo$1;
.super Ljava/lang/Object;
.source "WidgetUserPasswordVerify.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/user/WidgetUserPasswordVerify;->updateAccountInfo(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/models/domain/ModelUser;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/user/WidgetUserPasswordVerify;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/user/WidgetUserPasswordVerify;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserPasswordVerify$updateAccountInfo$1;->this$0:Lcom/discord/widgets/user/WidgetUserPasswordVerify;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;)V
    .locals 2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object v0

    const-string v1, "updatedUser"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreAuthentication;->setAuthed(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/user/WidgetUserPasswordVerify$updateAccountInfo$1;->this$0:Lcom/discord/widgets/user/WidgetUserPasswordVerify;

    invoke-static {p1}, Lcom/discord/widgets/user/WidgetUserPasswordVerify;->access$finishWithSuccess(Lcom/discord/widgets/user/WidgetUserPasswordVerify;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/user/WidgetUserPasswordVerify$updateAccountInfo$1;->call(Lcom/discord/models/domain/ModelUser;)V

    return-void
.end method
