.class public final Lcom/discord/widgets/user/WidgetUserSetCustomStatus;
.super Lcom/discord/app/AppFragment;
.source "WidgetUserSetCustomStatus.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;


# instance fields
.field private currentEmojiUri:Ljava/lang/String;

.field private final expirationRadio1Hour$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final expirationRadio30Minutes$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final expirationRadio4Hours$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private expirationRadioManager:Lcom/discord/views/RadioManager;

.field private final expirationRadioNever$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final expirationRadioTomorrow$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final save$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final statusEmoji$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final statusEmojiButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final statusText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v3, "statusEmojiButton"

    const-string v4, "getStatusEmojiButton()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "statusEmoji"

    const-string v7, "getStatusEmoji()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "statusText"

    const-string v7, "getStatusText()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "expirationRadioNever"

    const-string v7, "getExpirationRadioNever()Lcom/google/android/material/radiobutton/MaterialRadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "expirationRadio30Minutes"

    const-string v7, "getExpirationRadio30Minutes()Lcom/google/android/material/radiobutton/MaterialRadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "expirationRadio1Hour"

    const-string v7, "getExpirationRadio1Hour()Lcom/google/android/material/radiobutton/MaterialRadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "expirationRadio4Hours"

    const-string v7, "getExpirationRadio4Hours()Lcom/google/android/material/radiobutton/MaterialRadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "expirationRadioTomorrow"

    const-string v7, "getExpirationRadioTomorrow()Lcom/google/android/material/radiobutton/MaterialRadioButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const-string v6, "save"

    const-string v7, "getSave()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->Companion:Lcom/discord/widgets/user/WidgetUserSetCustomStatus$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0932

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->statusEmojiButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0931

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->statusEmoji$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a093a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->statusText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0937

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadioNever$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0935

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadio30Minutes$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0934

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadio1Hour$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0936

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadio4Hours$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0938

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadioTomorrow$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0939

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->save$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->viewModel:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->handleEvent(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$openEmojiPicker(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->openEmojiPicker()V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->viewModel:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    return-void
.end method

.method public static final synthetic access$updateView(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->updateView(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;)V

    return-void
.end method

.method private final getExpirationRadio1Hour()Lcom/google/android/material/radiobutton/MaterialRadioButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadio1Hour$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/radiobutton/MaterialRadioButton;

    return-object v0
.end method

.method private final getExpirationRadio30Minutes()Lcom/google/android/material/radiobutton/MaterialRadioButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadio30Minutes$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/radiobutton/MaterialRadioButton;

    return-object v0
.end method

.method private final getExpirationRadio4Hours()Lcom/google/android/material/radiobutton/MaterialRadioButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadio4Hours$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/radiobutton/MaterialRadioButton;

    return-object v0
.end method

.method private final getExpirationRadioNever()Lcom/google/android/material/radiobutton/MaterialRadioButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadioNever$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/radiobutton/MaterialRadioButton;

    return-object v0
.end method

.method private final getExpirationRadioTomorrow()Lcom/google/android/material/radiobutton/MaterialRadioButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadioTomorrow$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/radiobutton/MaterialRadioButton;

    return-object v0
.end method

.method private final getSave()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->save$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    return-object v0
.end method

.method private final getStatusEmoji()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->statusEmoji$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getStatusEmojiButton()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->statusEmojiButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStatusText()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->statusText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event;)V
    .locals 3

    instance-of v0, p1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusSuccess;

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusSuccess;

    invoke-virtual {p1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusSuccess;->getSuccessMessageStringRes()I

    move-result p1

    invoke-static {p0, p1, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    const/4 p1, 0x1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, v0}, Lcom/discord/app/AppFragment;->hideKeyboard$default(Lcom/discord/app/AppFragment;Landroid/view/View;ILjava/lang/Object;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusFailure;

    invoke-virtual {p1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Event$SetStatusFailure;->getFailureMessageStringRes()I

    move-result p1

    invoke-static {p0, p1, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final openEmojiPicker()V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$openEmojiPicker$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$openEmojiPicker$1;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    sget-object v2, Lcom/discord/widgets/chat/input/emoji/EmojiContextType;->GLOBAL:Lcom/discord/widgets/chat/input/emoji/EmojiContextType;

    const/4 v3, 0x0

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/chat/input/emoji/EmojiPickerNavigator;->launchBottomSheet$default(Landroidx/fragment/app/FragmentManager;Lcom/discord/widgets/chat/input/emoji/EmojiPickerListener;Lcom/discord/widgets/chat/input/emoji/EmojiContextType;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method private final setExpirationOnCheck(Lcom/google/android/material/radiobutton/MaterialRadioButton;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V
    .locals 1

    new-instance v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$setExpirationOnCheck$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$setExpirationOnCheck$1;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updateView(Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    instance-of v2, v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;

    if-nez v2, :cond_0

    return-void

    :cond_0
    check-cast v1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;

    invoke-virtual {v1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;->getFormState()Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f070093

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;->getEmoji()Lcom/discord/models/domain/emoji/Emoji;

    move-result-object v6

    invoke-static {v3}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v5, v3, v7}, Lcom/discord/models/domain/emoji/Emoji;->getImageUri(ZILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->currentEmojiUri:Ljava/lang/String;

    invoke-static {v3, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v5

    if-eqz v6, :cond_2

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusEmoji()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7c

    const/16 v16, 0x0

    move-object v9, v3

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    iput-object v3, v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->currentEmojiUri:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusEmoji()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v3

    const v6, 0x7f080431

    invoke-virtual {v3, v6}, Lcom/facebook/drawee/view/SimpleDraweeView;->setActualImageResource(I)V

    iput-object v4, v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->currentEmojiUri:Ljava/lang/String;

    :cond_2
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-static {v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v5

    if-eqz v6, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v6

    invoke-virtual {v2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    goto :goto_2

    :cond_4
    move-object v3, v4

    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v6

    invoke-static {v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v3, v6}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$ViewState$Loaded;->getShowStatusClear()Z

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/android/material/textfield/TextInputLayout;->setEndIconVisible(Z)V

    invoke-virtual {v2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState;->getExpiration()Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-eqz v1, :cond_a

    if-eq v1, v5, :cond_9

    const/4 v2, 0x2

    if-eq v1, v2, :cond_8

    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x4

    if-ne v1, v2, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadioTomorrow()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    goto :goto_3

    :cond_6
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio4Hours()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    goto :goto_3

    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio1Hour()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    goto :goto_3

    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio30Minutes()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    goto :goto_3

    :cond_a
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadioNever()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    :goto_3
    iget-object v2, v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadioManager:Lcom/discord/views/RadioManager;

    if-eqz v2, :cond_b

    invoke-virtual {v2, v1}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    return-void

    :cond_b
    const-string v1, "expirationRadioManager"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v4
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02cf

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Factory;

    invoke-direct {v0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026tusViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    iput-object p1, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->viewModel:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_0

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 9

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const v1, 0x7f1205a4

    invoke-virtual {p0, v1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    new-instance v1, Lcom/discord/views/RadioManager;

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/google/android/material/radiobutton/MaterialRadioButton;

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadioNever()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    aput-object v3, v2, p1

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio30Minutes()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio1Hour()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio4Hours()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadioTomorrow()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    const/4 v4, 0x4

    aput-object v3, v2, v4

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object v1, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->expirationRadioManager:Lcom/discord/views/RadioManager;

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusEmojiButton()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$1;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$2;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    invoke-virtual {v1, v2}, Lcom/google/android/material/textfield/TextInputLayout;->setEndIconOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getStatusText()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$3;

    invoke-direct {v2, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$3;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    invoke-static {v1, p0, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadioNever()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;->NEVER:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;

    invoke-direct {p0, v1, v2}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->setExpirationOnCheck(Lcom/google/android/material/radiobutton/MaterialRadioButton;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "requireContext()"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v6, v0, [Ljava/lang/Object;

    const/16 v7, 0x1e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, p1

    const v8, 0x7f10002c

    invoke-static {v1, v3, v8, v7, v6}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio30Minutes()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    new-array v6, v0, [Ljava/lang/Object;

    aput-object v1, v6, p1

    const v1, 0x7f1205a8

    invoke-virtual {p0, v1, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio30Minutes()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v1

    sget-object v3, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;->IN_30_MINUTES:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;

    invoke-direct {p0, v1, v3}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->setExpirationOnCheck(Lcom/google/android/material/radiobutton/MaterialRadioButton;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, p1

    const v7, 0x7f10002b

    invoke-static {v1, v3, v7, v0, v6}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio1Hour()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    new-array v6, v0, [Ljava/lang/Object;

    aput-object v1, v6, p1

    const v1, 0x7f1205a7

    invoke-virtual {p0, v1, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio1Hour()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    sget-object v6, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;->IN_1_HOUR:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;

    invoke-direct {p0, v3, v6}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->setExpirationOnCheck(Lcom/google/android/material/radiobutton/MaterialRadioButton;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v0, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p1

    invoke-static {v3, v2, v7, v4, v5}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio4Hours()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v2, v0, p1

    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadio4Hours()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;->IN_4_HOURS:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->setExpirationOnCheck(Lcom/google/android/material/radiobutton/MaterialRadioButton;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getExpirationRadioTomorrow()Lcom/google/android/material/radiobutton/MaterialRadioButton;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;->TOMORROW:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->setExpirationOnCheck(Lcom/google/android/material/radiobutton/MaterialRadioButton;Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel$FormState$Expiration;)V

    invoke-direct {p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->getSave()Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBound$4;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->viewModel:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    const/4 v1, 0x0

    const-string v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;->viewModel:Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatusViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/user/WidgetUserSetCustomStatus;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/user/WidgetUserSetCustomStatus$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/user/WidgetUserSetCustomStatus;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
