.class public final Lcom/discord/widgets/notice/NoticePopupChannel;
.super Ljava/lang/Object;
.source "NoticePopupChannel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/notice/NoticePopupChannel$Model;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/notice/NoticePopupChannel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/notice/NoticePopupChannel;

    invoke-direct {v0}, Lcom/discord/widgets/notice/NoticePopupChannel;-><init>()V

    sput-object v0, Lcom/discord/widgets/notice/NoticePopupChannel;->INSTANCE:Lcom/discord/widgets/notice/NoticePopupChannel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$createModel(Lcom/discord/widgets/notice/NoticePopupChannel;Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/discord/widgets/notice/NoticePopupChannel;->createModel(Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    move-result-object p0

    return-object p0
.end method

.method private final createModel(Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;)",
            "Lcom/discord/widgets/notice/NoticePopupChannel$Model;"
        }
    .end annotation

    move-object/from16 v7, p1

    move-object/from16 v6, p4

    const/4 v0, 0x0

    if-eqz v6, :cond_19

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    if-nez p6, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-nez v5, :cond_1

    goto/16 :goto_13

    :cond_1
    :goto_0
    new-instance v1, Lcom/discord/utilities/textprocessing/MessageRenderContext;

    move-object v0, v1

    invoke-virtual/range {p7 .. p7}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    const/4 v4, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x8

    const/16 v19, 0x0

    move-object/from16 v13, v19

    move-object/from16 v14, p2

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    invoke-static/range {v14 .. v19}, Lcom/discord/utilities/textprocessing/MessageUtils;->getNickOrUsernames$default(Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/util/Map;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$messageRenderContext$1;->INSTANCE:Lcom/discord/widgets/notice/NoticePopupChannel$createModel$messageRenderContext$1;

    const v11, 0x7f0405b2

    invoke-static {v7, v11}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v11

    const/4 v12, 0x0

    const/4 v15, 0x0

    move-object v14, v15

    const/16 v16, 0x3cc0

    move-object/from16 v20, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v17}, Lcom/discord/utilities/textprocessing/MessageRenderContext;-><init>(Landroid/content/Context;JZLjava/util/Map;Ljava/util/Map;Ljava/util/Map;ILkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelMessage;->getContent()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_2

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v6, v1, v6, v2, v3}, Lcom/discord/utilities/textprocessing/Parsers;->createParser$default(ZZZILjava/lang/Object;)Lcom/discord/simpleast/core/parser/Parser;

    move-result-object v21

    const-string v2, "content"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lcom/discord/utilities/textprocessing/MessageParseState;->Companion:Lcom/discord/utilities/textprocessing/MessageParseState$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/textprocessing/MessageParseState$Companion;->getInitialState()Lcom/discord/utilities/textprocessing/MessageParseState;

    move-result-object v23

    const/16 v24, 0x0

    const/16 v25, 0x4

    const/16 v26, 0x0

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v26}, Lcom/discord/simpleast/core/parser/Parser;->parse$default(Lcom/discord/simpleast/core/parser/Parser;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v2, v20

    invoke-static {v0, v2}, Lcom/discord/utilities/textprocessing/AstRenderer;->render(Ljava/util/Collection;Ljava/lang/Object;)Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :cond_3
    new-instance v0, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-direct {v0}, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;-><init>()V

    :goto_1
    move-object v10, v0

    move-object v7, v3

    invoke-static {v10}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v2, ""

    if-eqz v0, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelMessage;->getEmbeds()Ljava/util/List;

    move-result-object v0

    const-string v3, "message.embeds"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelMessageEmbed;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageEmbed;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move-object v0, v7

    :goto_2
    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    move-object v0, v2

    :goto_3
    invoke-virtual {v10, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_6
    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_7

    goto :goto_4

    :cond_7
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v8, -0x1

    cmp-long v5, v3, v8

    if-nez v5, :cond_8

    goto :goto_5

    :cond_8
    :goto_4
    if-nez v0, :cond_9

    goto :goto_6

    :cond_9
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v8, 0x0

    cmp-long v0, v3, v8

    if-nez v0, :cond_b

    :goto_5
    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    move-object v2, v0

    :cond_a
    new-instance v0, Lkotlin/Pair;

    invoke-direct {v0, v2, v7}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_8

    :cond_b
    :goto_6
    if-eqz p3, :cond_c

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_c
    move-object v0, v7

    :goto_7
    if-eqz v0, :cond_d

    move-object v2, v0

    :cond_d
    const/16 v0, 0x23

    invoke-static {v0}, Lf/e/c/a/a;->E(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v3

    :goto_8
    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    const/4 v5, 0x2

    if-eqz v0, :cond_e

    move-object/from16 v4, p4

    invoke-static {v4, v7, v5, v7}, Lcom/discord/utilities/icon/IconUtils;->getForChannel$default(Lcom/discord/models/domain/ModelChannel;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    :cond_e
    move-object/from16 v4, p4

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xc

    const/16 v16, 0x0

    const-string v12, "asset://asset/images/default_icon_selected.jpg"

    move-object/from16 v11, p3

    invoke-static/range {v11 .. v16}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Lcom/discord/models/domain/ModelGuild;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_9
    move-object v11, v0

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->isDM()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelMessage;->getAuthor()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    const-string v2, "sender"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v3, p5

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuildMember$Computed;

    invoke-virtual {v0, v2, v4}, Lcom/discord/models/domain/ModelUser;->getNickOrUsername(Lcom/discord/models/domain/ModelGuildMember$Computed;Lcom/discord/models/domain/ModelChannel;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/text/SpannableStringBuilder;

    const-string v3, ": "

    invoke-static {v0, v3}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const v0, 0x7f04048d

    move-object/from16 v12, p1

    invoke-static {v12, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    new-array v3, v5, [Ljava/lang/Object;

    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v13, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    aput-object v13, v3, v6

    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v0, v3, v1

    invoke-static {v3}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v13

    const/16 v14, 0x21

    invoke-virtual {v2, v3, v6, v13, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_a

    :cond_f
    invoke-virtual {v10, v6, v2}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_b

    :cond_10
    move-object/from16 v12, p1

    :goto_b
    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelMessage;->getAttachments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-static {v0}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelMessageAttachment;

    goto :goto_c

    :cond_11
    move-object v0, v7

    :goto_c
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageAttachment;->isSpoilerAttachment()Z

    move-result v2

    if-ne v2, v1, :cond_12

    const v0, 0x7f0405b2

    invoke-static {v12, v0, v6, v5, v7}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    invoke-static {v12, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v6, 0x2

    goto :goto_10

    :cond_12
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageAttachment;->getType()Lcom/discord/models/domain/ModelMessageAttachment$Type;

    move-result-object v1

    goto :goto_d

    :cond_13
    move-object v1, v7

    :goto_d
    sget-object v2, Lcom/discord/models/domain/ModelMessageAttachment$Type;->IMAGE:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    if-ne v1, v2, :cond_15

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageAttachment;->getProxyUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_14

    goto :goto_e

    :cond_14
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageAttachment;->getUrl()Ljava/lang/String;

    move-result-object v1

    :goto_e
    move-object v13, v1

    move-object v14, v7

    move-object v15, v14

    const/4 v6, 0x2

    goto :goto_11

    :cond_15
    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelMessageAttachment;->getType()Lcom/discord/models/domain/ModelMessageAttachment$Type;

    move-result-object v0

    goto :goto_f

    :cond_16
    move-object v0, v7

    :goto_f
    sget-object v1, Lcom/discord/models/domain/ModelMessageAttachment$Type;->VIDEO:Lcom/discord/models/domain/ModelMessageAttachment$Type;

    if-ne v0, v1, :cond_17

    const v1, 0x7f080575

    const v0, 0x7f04013b

    invoke-static {v12, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v13, 0x0

    const/4 v14, 0x4

    const/4 v15, 0x0

    move-object/from16 v0, p1

    move v3, v13

    move v4, v14

    const/4 v6, 0x2

    move-object v5, v15

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/drawable/DrawableCompat;->getDrawable$default(Landroid/content/Context;IIZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    const v1, 0x7f0803e3

    const v0, 0x7f040155

    invoke-static {v12, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v2

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/drawable/DrawableCompat;->getDrawable$default(Landroid/content/Context;IIZILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v14, v0

    move-object v13, v7

    goto :goto_11

    :cond_17
    const/4 v6, 0x2

    move-object v0, v7

    :goto_10
    move-object v14, v0

    move-object v13, v7

    move-object v15, v13

    :goto_11
    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelChannel;->isPrivateType(I)Z

    move-result v0

    if-eqz v0, :cond_18

    const v0, 0x7f0402d6

    goto :goto_12

    :cond_18
    const v0, 0x7f040308

    :goto_12
    const/4 v1, 0x0

    invoke-static {v12, v0, v1, v6, v7}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v0

    invoke-static {v12, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;

    move-object/from16 v2, p4

    invoke-direct {v1, v2, v12}, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;-><init>(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)V

    new-instance v2, Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    move-object v7, v2

    move-object v12, v15

    move-object v15, v0

    move-object/from16 v16, v1

    invoke-direct/range {v7 .. v16}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;)V

    return-object v2

    :cond_19
    :goto_13
    return-object v0
.end method


# virtual methods
.method public final enqueue(Landroid/content/Context;Ljava/lang/String;Lcom/discord/models/domain/ModelMessage;Lkotlin/jvm/functions/Function1;)V
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const-string v4, "context"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "noticeName"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "message"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "onClick"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Lg0/l/e/j;

    invoke-direct {v5, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    new-instance v6, Lg0/l/e/j;

    invoke-direct {v6, v2}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lcom/discord/stores/StoreGuilds;->observeFromChannelId(J)Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelMessage;->getChannelId()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelMessage;->getGuildId()Ljava/lang/Long;

    move-result-object v9

    const-wide/16 v10, 0x0

    if-eqz v9, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    :goto_0
    const-string v12, "message.guildId ?: 0"

    invoke-static {v9, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual {v4, v13, v14}, Lcom/discord/stores/StoreGuilds;->observeComputed(J)Lrx/Observable;

    move-result-object v9

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreChannelsSelected;->observeId()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v13

    invoke-virtual {v13}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v13

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v14

    invoke-virtual {v14}, Lcom/discord/stores/StoreChannels;->observeNames()Lrx/Observable;

    move-result-object v14

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/models/domain/ModelMessage;->getGuildId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_1
    invoke-static {v2, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Lcom/discord/stores/StoreGuilds;->observeRoles(J)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$1;

    move-object/from16 v15, p0

    invoke-direct {v2, v15}, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$1;-><init>(Lcom/discord/widgets/notice/NoticePopupChannel;)V

    new-instance v12, Lcom/discord/widgets/notice/NoticePopupChannel$sam$rx_functions_Func9$0;

    invoke-direct {v12, v2}, Lcom/discord/widgets/notice/NoticePopupChannel$sam$rx_functions_Func9$0;-><init>(Lkotlin/jvm/functions/Function9;)V

    move-object v10, v4

    move-object v11, v13

    move-object v2, v12

    move-object v12, v14

    move-object v13, v0

    move-object v14, v2

    invoke-static/range {v5 .. v14}, Lrx/Observable;->c(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;)Lrx/Observable;

    move-result-object v0

    const-string v2, "Observable\n        .comb\u2026is::createModel\n        )"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3

    const/16 v21, 0x0

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v21}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-class v2, Lcom/discord/widgets/notice/NoticePopupChannel;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;

    invoke-direct {v8, v1, v3}, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    move-object v1, v0

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move v8, v9

    move-object v9, v10

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
