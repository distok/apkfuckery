.class public final Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;
.super Lx/m/c/k;
.source "NoticePopupChannel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/notice/NoticePopupChannel;->createModel(Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $channel:Lcom/discord/models/domain/ModelChannel;

.field public final synthetic $context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;->$context:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result p1

    invoke-static {p1}, Lcom/discord/models/domain/ModelChannel;->isPrivateType(I)Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;->$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$createModel$onClickTopRightIcon$1;->$channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;->launch(Landroid/content/Context;JZ)V

    :cond_0
    return-void
.end method
