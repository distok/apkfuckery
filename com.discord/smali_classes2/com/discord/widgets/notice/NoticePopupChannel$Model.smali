.class public final Lcom/discord/widgets/notice/NoticePopupChannel$Model;
.super Ljava/lang/Object;
.source "NoticePopupChannel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/notice/NoticePopupChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Model"
.end annotation


# instance fields
.field private final noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

.field private noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

.field private noticeBodyImageUrl:Ljava/lang/String;

.field private final noticeIconTopRight:Landroid/graphics/drawable/Drawable;

.field private final noticeIconUrl:Ljava/lang/String;

.field private final noticeSubtitle:Ljava/lang/CharSequence;

.field private final noticeTitle:Ljava/lang/CharSequence;

.field private final onClickTopRightIcon:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            "Landroid/graphics/drawable/Drawable;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "noticeTitle"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "noticeBody"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickTopRightIcon"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    iput-object p4, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    iput-object p6, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    iput-object p8, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    iput-object p9, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/notice/NoticePopupChannel$Model;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->copy(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final component8()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final component9()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Lcom/facebook/drawee/span/DraweeSpanStringBuilder;",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            "Landroid/graphics/drawable/Drawable;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/discord/widgets/notice/NoticePopupChannel$Model;"
        }
    .end annotation

    const-string v0, "noticeTitle"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "noticeBody"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickTopRightIcon"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    move-object v1, v0

    move-object v3, p2

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v10}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/facebook/drawee/span/DraweeSpanStringBuilder;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    iget-object v1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNoticeBody()Lcom/facebook/drawee/span/DraweeSpanStringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    return-object v0
.end method

.method public final getNoticeBodyBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getNoticeBodyImageDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getNoticeBodyImageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getNoticeIconTopRight()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getNoticeIconUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getNoticeSubtitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getNoticeTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getOnClickTopRightIcon()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    return v0
.end method

.method public final setNoticeBodyBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public final setNoticeBodyImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public final setNoticeBodyImageUrl(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Model(noticeTitle="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeSubtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeSubtitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeBody="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBody:Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeIconUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeBodyBackgroundDrawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeBodyImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeBodyImageDrawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", noticeIconTopRight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClickTopRightIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
