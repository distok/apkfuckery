.class public final synthetic Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$1;
.super Lx/m/c/i;
.source "NoticePopupChannel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function9;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/notice/NoticePopupChannel;->enqueue(Landroid/content/Context;Ljava/lang/String;Lcom/discord/models/domain/ModelMessage;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function9<",
        "Landroid/content/Context;",
        "Lcom/discord/models/domain/ModelMessage;",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;",
        "Ljava/lang/Long;",
        "Lcom/discord/models/domain/ModelUser;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuildRole;",
        ">;",
        "Lcom/discord/widgets/notice/NoticePopupChannel$Model;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/notice/NoticePopupChannel;)V
    .locals 7

    const-class v3, Lcom/discord/widgets/notice/NoticePopupChannel;

    const/16 v1, 0x9

    const-string v4, "createModel"

    const-string v5, "createModel(Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/discord/models/domain/ModelMessage;",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelUser;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuildRole;",
            ">;)",
            "Lcom/discord/widgets/notice/NoticePopupChannel$Model;"
        }
    .end annotation

    const-string v0, "p1"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p5"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p7"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p8"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p9"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    iget-object v1, v0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v1, Lcom/discord/widgets/notice/NoticePopupChannel;

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v10}, Lcom/discord/widgets/notice/NoticePopupChannel;->access$createModel(Lcom/discord/widgets/notice/NoticePopupChannel;Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/Context;

    check-cast p2, Lcom/discord/models/domain/ModelMessage;

    check-cast p3, Lcom/discord/models/domain/ModelGuild;

    check-cast p4, Lcom/discord/models/domain/ModelChannel;

    check-cast p5, Ljava/util/Map;

    check-cast p6, Ljava/lang/Long;

    check-cast p7, Lcom/discord/models/domain/ModelUser;

    check-cast p8, Ljava/util/Map;

    check-cast p9, Ljava/util/Map;

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$1;->invoke(Landroid/content/Context;Lcom/discord/models/domain/ModelMessage;Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Lcom/discord/models/domain/ModelUser;Ljava/util/Map;Ljava/util/Map;)Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    move-result-object p1

    return-object p1
.end method
