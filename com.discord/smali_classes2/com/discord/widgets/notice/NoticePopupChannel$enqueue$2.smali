.class public final Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;
.super Lx/m/c/k;
.source "NoticePopupChannel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/notice/NoticePopupChannel;->enqueue(Landroid/content/Context;Ljava/lang/String;Lcom/discord/models/domain/ModelMessage;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/notice/NoticePopupChannel$Model;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $noticeName:Ljava/lang/String;

.field public final synthetic $onClick:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;->$noticeName:Ljava/lang/String;

    iput-object p2, p0, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;->$onClick:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/notice/NoticePopupChannel$Model;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;->invoke(Lcom/discord/widgets/notice/NoticePopupChannel$Model;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/notice/NoticePopupChannel$Model;)V
    .locals 18

    move-object/from16 v0, p0

    if-eqz p1, :cond_0

    sget-object v1, Lcom/discord/widgets/notice/NoticePopup;->INSTANCE:Lcom/discord/widgets/notice/NoticePopup;

    iget-object v2, v0, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;->$noticeName:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeSubtitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeBody()Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeBodyBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeBodyImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeBodyImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeIconUrl()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getNoticeIconTopRight()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/notice/NoticePopupChannel$Model;->getOnClickTopRightIcon()Lkotlin/jvm/functions/Function1;

    move-result-object v14

    iget-object v15, v0, Lcom/discord/widgets/notice/NoticePopupChannel$enqueue$2;->$onClick:Lkotlin/jvm/functions/Function1;

    const/16 v16, 0xd00

    const/16 v17, 0x0

    invoke-static/range {v1 .. v17}, Lcom/discord/widgets/notice/NoticePopup;->enqueue$default(Lcom/discord/widgets/notice/NoticePopup;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;Ljava/lang/Integer;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method
