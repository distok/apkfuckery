.class public final Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;
.super Lx/m/c/k;
.source "NoticePopup.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/notice/NoticePopup;->enqueue(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Integer;Landroid/graphics/drawable/Drawable;Ljava/lang/Integer;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/fragment/app/FragmentActivity;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $noticeAutoDismissPeriodSecs:Ljava/lang/Integer;

.field public final synthetic $noticeBody:Ljava/lang/CharSequence;

.field public final synthetic $noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field public final synthetic $noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

.field public final synthetic $noticeBodyImageUrl:Ljava/lang/String;

.field public final synthetic $noticeIconResId:Ljava/lang/Integer;

.field public final synthetic $noticeIconTopRight:Landroid/graphics/drawable/Drawable;

.field public final synthetic $noticeIconUrl:Ljava/lang/String;

.field public final synthetic $noticeName:Ljava/lang/String;

.field public final synthetic $noticeSubtitle:Ljava/lang/CharSequence;

.field public final synthetic $noticeTitle:Ljava/lang/CharSequence;

.field public final synthetic $onClick:Lkotlin/jvm/functions/Function1;

.field public final synthetic $onClickTopRightIcon:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$onClick:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeName:Ljava/lang/String;

    iput-object p3, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeAutoDismissPeriodSecs:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconResId:Ljava/lang/Integer;

    iput-object p6, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyImageUrl:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    iput-object p8, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    iput-object p9, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeTitle:Ljava/lang/CharSequence;

    iput-object p10, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeSubtitle:Ljava/lang/CharSequence;

    iput-object p11, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBody:Ljava/lang/CharSequence;

    iput-object p12, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    iput-object p13, p0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$onClickTopRightIcon:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->invoke(Landroidx/fragment/app/FragmentActivity;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Landroidx/fragment/app/FragmentActivity;)Z
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v7, p1

    const-string v1, "activity"

    invoke-static {v7, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Lf/m/a/h;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lf/m/a/h;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v9

    :goto_0
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    move-object v1, v9

    :cond_1
    check-cast v1, Landroid/view/ViewGroup;

    const/4 v10, 0x0

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ltz v2, :cond_5

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v4, v4, Lf/m/a/b;

    if-eqz v4, :cond_3

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    check-cast v4, Lf/m/a/b;

    goto :goto_2

    :cond_2
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type com.tapadoo.alerter.Alert"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move-object v4, v9

    :goto_2
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-static {v4}, Landroidx/core/view/ViewCompat;->animate(Landroid/view/View;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroidx/core/view/ViewPropertyAnimatorCompat;->alpha(F)Landroidx/core/view/ViewPropertyAnimatorCompat;

    move-result-object v5

    new-instance v6, Lf/m/a/f;

    invoke-direct {v6, v4}, Lf/m/a/f;-><init>(Lf/m/a/b;)V

    invoke-virtual {v5, v6}, Landroidx/core/view/ViewPropertyAnimatorCompat;->withEndAction(Ljava/lang/Runnable;)Landroidx/core/view/ViewPropertyAnimatorCompat;

    :cond_4
    if-eq v3, v2, :cond_5

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lf/m/a/h;->b:Ljava/lang/ref/WeakReference;

    new-instance v11, Lf/m/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const v3, 0x7f0d0242

    move-object v1, v11

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lf/m/a/b;-><init>(Landroid/content/Context;ILandroid/util/AttributeSet;II)V

    iput-object v11, v8, Lf/m/a/h;->a:Lf/m/a/b;

    const v1, 0x7f04013d

    invoke-static {v7, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    iget-object v2, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v2, :cond_6

    invoke-virtual {v2, v1}, Lf/m/a/b;->setAlertBackgroundColor(I)V

    :cond_6
    iget-object v1, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v1, :cond_7

    sget v2, Lcom/tapadoo/alerter/R$d;->llAlertBackground:I

    invoke-virtual {v1, v2}, Lf/m/a/b;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    new-instance v3, Lf/m/a/k;

    const-string v4, "it"

    invoke-static {v2, v4}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lf/m/a/a;

    invoke-direct {v4, v1}, Lf/m/a/a;-><init>(Lf/m/a/b;)V

    invoke-direct {v3, v2, v4}, Lf/m/a/k;-><init>(Landroid/view/View;Lf/m/a/k$a;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_7
    iget-object v1, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v1, :cond_8

    invoke-virtual {v1, v10}, Lf/m/a/b;->setVibrationEnabled(Z)V

    :cond_8
    iget-object v1, v8, Lf/m/a/h;->a:Lf/m/a/b;

    const/4 v2, 0x1

    if-eqz v1, :cond_9

    invoke-virtual {v1, v2}, Lf/m/a/b;->setEnableInfiniteDuration(Z)V

    :cond_9
    const v1, 0x7f01002f

    iget-object v3, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    const-string v4, "AnimationUtils.loadAnima\u2026lert?.context, animation)"

    invoke-static {v1, v4}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Lf/m/a/b;->setEnterAnimation$alerter_release(Landroid/view/animation/Animation;)V

    :cond_a
    new-instance v1, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$1;

    invoke-direct {v1, v0}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$1;-><init>(Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;)V

    const-string v3, "onClickListener"

    invoke-static {v1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v3, :cond_b

    invoke-virtual {v3, v1}, Lf/m/a/b;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    new-instance v1, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$2;

    invoke-direct {v1, v0}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$2;-><init>(Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;)V

    const-string v3, "listener"

    invoke-static {v1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v3, :cond_c

    invoke-virtual {v3, v1}, Lf/m/a/b;->setOnHideListener$alerter_release(Lf/m/a/i;)V

    :cond_c
    iget-object v1, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lf/m/a/b;->getLayoutContainer()Landroid/view/View;

    move-result-object v1

    goto :goto_3

    :cond_d
    move-object v1, v9

    :goto_3
    if-eqz v1, :cond_19

    sget-object v3, Lcom/discord/widgets/notice/NoticePopup;->INSTANCE:Lcom/discord/widgets/notice/NoticePopup;

    iget-object v4, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeAutoDismissPeriodSecs:Ljava/lang/Integer;

    new-instance v5, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$$special$$inlined$also$lambda$1;

    invoke-direct {v5, v0, v7}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$$special$$inlined$also$lambda$1;-><init>(Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;Landroidx/fragment/app/FragmentActivity;)V

    invoke-static {v3, v4, v5}, Lcom/discord/widgets/notice/NoticePopup;->access$getAutoDismissAnimator(Lcom/discord/widgets/notice/NoticePopup;Ljava/lang/Integer;Lkotlin/jvm/functions/Function0;)Landroid/animation/ValueAnimator;

    move-result-object v3

    new-instance v4, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$3$1;

    invoke-direct {v4, v3}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$3$1;-><init>(Landroid/animation/ValueAnimator;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v3, 0x7f0a0793

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/facebook/drawee/view/SimpleDraweeView;

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconUrl:Ljava/lang/String;

    const-string v4, "noticeIconImageView"

    if-eqz v3, :cond_e

    invoke-static {v12, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconUrl:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x7c

    const/16 v19, 0x0

    move-object v11, v12

    move-object v12, v3

    invoke-static/range {v11 .. v19}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    goto :goto_4

    :cond_e
    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconResId:Ljava/lang/Integer;

    if-eqz v3, :cond_f

    sget-object v11, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    invoke-static {v12, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconResId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lcom/discord/utilities/images/MGImages;->setImage$default(Lcom/discord/utilities/images/MGImages;Landroid/widget/ImageView;ILcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :cond_f
    :goto_4
    const v3, 0x7f0a0790

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/facebook/drawee/view/SimpleDraweeView;

    const-string v3, "noticeBodyImageView"

    invoke-static {v12, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyImageUrl:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_10

    goto :goto_5

    :cond_10
    const/4 v3, 0x0

    goto :goto_6

    :cond_11
    :goto_5
    const/4 v3, 0x1

    :goto_6
    const/16 v4, 0x8

    if-eqz v3, :cond_12

    const/4 v3, 0x0

    goto :goto_7

    :cond_12
    const/16 v3, 0x8

    :goto_7
    invoke-virtual {v12, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyImageUrl:Ljava/lang/String;

    if-eqz v3, :cond_13

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x7c

    const/16 v19, 0x0

    move-object v11, v12

    move-object v12, v3

    invoke-static/range {v11 .. v19}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    goto :goto_8

    :cond_13
    iget-object v13, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBodyImageDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v13, :cond_14

    sget-object v11, Lcom/discord/utilities/images/MGImages;->INSTANCE:Lcom/discord/utilities/images/MGImages;

    const/4 v14, 0x0

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lcom/discord/utilities/images/MGImages;->setImage$default(Lcom/discord/utilities/images/MGImages;Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :cond_14
    :goto_8
    const v3, 0x7f0a0795

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v5, "view.findViewById<TextView>(R.id.popup_title)"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeTitle:Ljava/lang/CharSequence;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x7f0a0794

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v5, "view.findViewById<TextView>(R.id.popup_subtitle)"

    invoke-static {v3, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeSubtitle:Ljava/lang/CharSequence;

    invoke-static {v3, v5}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    const v3, 0x7f0a0791

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;

    iget-object v5, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBody:Ljava/lang/CharSequence;

    instance-of v6, v5, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    const-string v11, "popupBodyTextView"

    if-eqz v6, :cond_15

    check-cast v5, Lcom/facebook/drawee/span/DraweeSpanStringBuilder;

    invoke-virtual {v3, v5}, Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;->setDraweeSpanStringBuilder(Lcom/facebook/drawee/span/DraweeSpanStringBuilder;)V

    goto :goto_9

    :cond_15
    invoke-static {v3, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBody:Ljava/lang/CharSequence;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_9
    invoke-static {v3, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeBody:Ljava/lang/CharSequence;

    if-eqz v5, :cond_16

    invoke-static {v5}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/2addr v5, v2

    if-ne v5, v2, :cond_16

    const/4 v5, 0x1

    goto :goto_a

    :cond_16
    const/4 v5, 0x0

    :goto_a
    if-eqz v5, :cond_17

    const/4 v4, 0x0

    :cond_17
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v4, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$3$2;

    invoke-direct {v4, v3}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$3$2;-><init>(Lcom/discord/utilities/view/text/SimpleDraweeSpanTextView;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    const v3, 0x7f0a0792

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iget-object v4, v0, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;->$noticeIconTopRight:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_18

    goto :goto_b

    :cond_18
    const v4, 0x7f040308

    const/4 v5, 0x2

    invoke-static {v7, v4, v10, v5, v9}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v4

    invoke-static {v7, v4}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    :goto_b
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v4, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$$special$$inlined$also$lambda$2;

    invoke-direct {v4, v1, v0, v7}, Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1$$special$$inlined$also$lambda$2;-><init>(Landroid/view/View;Lcom/discord/widgets/notice/NoticePopup$enqueue$notice$1;Landroidx/fragment/app/FragmentActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_19
    sget-object v1, Lf/m/a/h;->b:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    if-eqz v1, :cond_1a

    new-instance v3, Lf/m/a/g;

    invoke-direct {v3, v8}, Lf/m/a/g;-><init>(Lf/m/a/h;)V

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1a
    iget-object v1, v8, Lf/m/a/h;->a:Lf/m/a/b;

    if-eqz v1, :cond_1b

    const v3, 0x7f0a0647

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    :cond_1b
    if-eqz v9, :cond_1c

    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07005f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v9, v1}, Landroidx/core/view/ViewCompat;->setElevation(Landroid/view/View;F)V

    :cond_1c
    return v2
.end method
