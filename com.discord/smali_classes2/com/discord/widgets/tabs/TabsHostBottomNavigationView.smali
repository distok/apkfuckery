.class public final Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;
.super Landroid/widget/LinearLayout;
.source "TabsHostBottomNavigationView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final friendsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final friendsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final friendsNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final heightChangedListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final homeIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final homeItem$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final homeNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private iconToNavigationTabMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/view/View;",
            "+",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field

.field private final mentionsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final mentionsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private navigationTabToViewMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            "+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final searchIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchItem$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final tabsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private tintableIconToNavigationTabMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/widget/ImageView;",
            "+",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field

.field private final userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private userAvatarPresenceViewController:Lcom/discord/views/user/UserAvatarPresenceViewController;

.field private final userSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xd

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v3, "tabsContainer"

    const-string v4, "getTabsContainer()Landroid/widget/LinearLayout;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "homeItem"

    const-string v7, "getHomeItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "homeIcon"

    const-string v7, "getHomeIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "homeNotificationsBadge"

    const-string v7, "getHomeNotificationsBadge()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "friendsItem"

    const-string v7, "getFriendsItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "friendsIcon"

    const-string v7, "getFriendsIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "friendsNotificationsBadge"

    const-string v7, "getFriendsNotificationsBadge()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "searchItem"

    const-string v7, "getSearchItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "searchIcon"

    const-string v7, "getSearchIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "mentionsItem"

    const-string v7, "getMentionsItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "mentionsIcon"

    const-string v7, "getMentionsIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "userSettingsItem"

    const-string v7, "getUserSettingsItem()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    const-string v6, "userAvatarPresenceView"

    const-string v7, "getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const p1, 0x7f0a0a89

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->tabsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a83

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a82

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a84

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a80

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a7f

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a81

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a88

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a87

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a86

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a85

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a8b

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0a8a

    invoke-static {p0, p1}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->heightChangedListeners:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const p2, 0x7f0a0a89

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->tabsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a83

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a82

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a84

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a80

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a7f

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a81

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a88

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a87

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a86

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a85

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a8b

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a8a

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->heightChangedListeners:Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->initialize(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const p2, 0x7f0a0a89

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->tabsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a83

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a82

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a84

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a80

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a7f

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a81

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a88

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a87

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a86

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a85

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a8b

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p2, 0x7f0a0a8a

    invoke-static {p0, p2}, Ly/a/g0;->f(Landroid/view/View;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->heightChangedListeners:Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->initialize(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final getFriendsIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getFriendsItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFriendsNotificationsBadge()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->friendsNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getHomeIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getHomeItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getHomeNotificationsBadge()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->homeNotificationsBadge$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMentionsIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getMentionsItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->mentionsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSearchIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getSearchItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->searchItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getTabsContainer()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->tabsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userAvatarPresenceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/user/UserAvatarPresenceView;

    return-object v0
.end method

.method private final getUserSettingsItem()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userSettingsItem$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final initialize(Landroid/content/Context;)V
    .locals 13

    const v0, 0x7f0d00f4

    invoke-static {p1, v0, p0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getTabsContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getTabsContainer()Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p1, 0x4

    new-array v0, p1, [Lkotlin/Pair;

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeIcon()Landroid/widget/ImageView;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    new-instance v4, Lkotlin/Pair;

    invoke-direct {v4, v2, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v2, 0x0

    aput-object v4, v0, v2

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsIcon()Landroid/widget/ImageView;

    move-result-object v4

    sget-object v5, Lcom/discord/widgets/tabs/NavigationTab;->FRIENDS:Lcom/discord/widgets/tabs/NavigationTab;

    new-instance v6, Lkotlin/Pair;

    invoke-direct {v6, v4, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v0, v1

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getSearchIcon()Landroid/widget/ImageView;

    move-result-object v4

    sget-object v6, Lcom/discord/widgets/tabs/NavigationTab;->SEARCH:Lcom/discord/widgets/tabs/NavigationTab;

    new-instance v7, Lkotlin/Pair;

    invoke-direct {v7, v4, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v4, 0x2

    aput-object v7, v0, v4

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getMentionsIcon()Landroid/widget/ImageView;

    move-result-object v7

    sget-object v8, Lcom/discord/widgets/tabs/NavigationTab;->MENTIONS:Lcom/discord/widgets/tabs/NavigationTab;

    new-instance v9, Lkotlin/Pair;

    invoke-direct {v9, v7, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v7, 0x3

    aput-object v9, v0, v7

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->tintableIconToNavigationTabMap:Ljava/util/Map;

    const/4 v0, 0x5

    new-array v9, v0, [Lkotlin/Pair;

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeIcon()Landroid/widget/ImageView;

    move-result-object v10

    new-instance v11, Lkotlin/Pair;

    invoke-direct {v11, v10, v3}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v9, v2

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsIcon()Landroid/widget/ImageView;

    move-result-object v10

    new-instance v11, Lkotlin/Pair;

    invoke-direct {v11, v10, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v9, v1

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getSearchIcon()Landroid/widget/ImageView;

    move-result-object v10

    new-instance v11, Lkotlin/Pair;

    invoke-direct {v11, v10, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v9, v4

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getMentionsIcon()Landroid/widget/ImageView;

    move-result-object v10

    new-instance v11, Lkotlin/Pair;

    invoke-direct {v11, v10, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v11, v9, v7

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;

    move-result-object v10

    sget-object v11, Lcom/discord/widgets/tabs/NavigationTab;->SETTINGS:Lcom/discord/widgets/tabs/NavigationTab;

    new-instance v12, Lkotlin/Pair;

    invoke-direct {v12, v10, v11}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v12, v9, p1

    invoke-static {v9}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v9

    iput-object v9, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->iconToNavigationTabMap:Ljava/util/Map;

    new-array v0, v0, [Lkotlin/Pair;

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeItem()Landroid/view/View;

    move-result-object v9

    new-instance v10, Lkotlin/Pair;

    invoke-direct {v10, v3, v9}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v10, v0, v2

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsItem()Landroid/view/View;

    move-result-object v2

    new-instance v3, Lkotlin/Pair;

    invoke-direct {v3, v5, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v3, v0, v1

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getSearchItem()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v6, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v4

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getMentionsItem()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v8, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, v7

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserSettingsItem()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lkotlin/Pair;

    invoke-direct {v2, v11, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v2, v0, p1

    invoke-static {v0}, Lx/h/f;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->navigationTabToViewMap:Ljava/util/Map;

    new-instance p1, Lcom/discord/views/user/UserAvatarPresenceViewController;

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserAvatarPresenceView()Lcom/discord/views/user/UserAvatarPresenceView;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/views/user/UserAvatarPresenceViewController;-><init>(Lcom/discord/views/user/UserAvatarPresenceView;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreUserPresence;Lcom/discord/utilities/streams/StreamContextService;I)V

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userAvatarPresenceViewController:Lcom/discord/views/user/UserAvatarPresenceViewController;

    return-void
.end method

.method private final updateNotificationBadges(II)V
    .locals 7

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeNotificationsBadge()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeNotificationsBadge()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez p1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x8

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeNotificationsBadge()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f121035

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v2

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsNotificationsBadge()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsNotificationsBadge()Landroid/widget/TextView;

    move-result-object p1

    if-lez p2, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_3

    const/4 v4, 0x0

    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsNotificationsBadge()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f120d7a

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final addHeightChangedListener(Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;)V
    .locals 1

    const-string v0, "heightChangedListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->heightChangedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    iget-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->heightChangedListeners:Ljava/util/Set;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;

    invoke-interface {p3, p2}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;->onHeightChanged(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final updateView(Lcom/discord/widgets/tabs/NavigationTab;Lkotlin/jvm/functions/Function1;ZJLjava/util/Set;IILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            "Lkotlin/Unit;",
            ">;ZJ",
            "Ljava/util/Set<",
            "+",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;II",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "selectedTab"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTabSelected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "visibleTabs"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSearchClick"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSettingsLongPress"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getTabsContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-interface {p6}, Ljava/util/Set;->size()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeItem()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-interface {p6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsItem()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->FRIENDS:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-interface {p6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getSearchItem()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->SEARCH:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-interface {p6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getMentionsItem()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->MENTIONS:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-interface {p6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserSettingsItem()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->SETTINGS:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-interface {p6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p6

    if-eqz p6, :cond_4

    const/4 v2, 0x0

    :cond_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p6, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->tintableIconToNavigationTabMap:Ljava/util/Map;

    const/4 v0, 0x0

    if-eqz p6, :cond_10

    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p6

    invoke-interface {p6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p6

    :goto_4
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/tabs/NavigationTab;

    if-ne v1, p1, :cond_5

    const v1, 0x7f040176

    goto :goto_5

    :cond_5
    const v1, 0x7f040158

    :goto_5
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v2, v1}, Lcom/discord/utilities/color/ColorCompatKt;->tintWithColor(Landroid/widget/ImageView;I)V

    goto :goto_4

    :cond_6
    iget-object p6, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->iconToNavigationTabMap:Ljava/util/Map;

    if-eqz p6, :cond_f

    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p6

    invoke-interface {p6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p6

    :goto_6
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/tabs/NavigationTab;

    if-ne p1, v1, :cond_7

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_7

    :cond_7
    const/high16 v1, 0x3f000000    # 0.5f

    :goto_7
    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_6

    :cond_8
    iget-object p6, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->navigationTabToViewMap:Ljava/util/Map;

    if-eqz p6, :cond_e

    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p6

    invoke-interface {p6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p6

    :goto_8
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/tabs/NavigationTab;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-ne p1, v2, :cond_9

    const/4 v2, 0x1

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_8

    :cond_a
    iget-object p1, p0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->userAvatarPresenceViewController:Lcom/discord/views/user/UserAvatarPresenceViewController;

    if-eqz p1, :cond_d

    iget-wide v0, p1, Lcom/discord/views/user/UserAvatarPresenceViewController;->a:J

    iput-wide p4, p1, Lcom/discord/views/user/UserAvatarPresenceViewController;->a:J

    cmp-long p6, v0, p4

    if-eqz p6, :cond_b

    invoke-virtual {p1}, Lcom/discord/utilities/viewcontroller/RxViewController;->bind()V

    :cond_b
    invoke-direct {p0, p7, p8}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->updateNotificationBadges(II)V

    if-eqz p3, :cond_c

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeItem()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$4;

    invoke-direct {p3, p2}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$4;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsItem()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$5;

    invoke-direct {p3, p2}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$5;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getSearchItem()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$6;

    invoke-direct {p3, p9}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$6;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getMentionsItem()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$7;

    invoke-direct {p3, p2}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$7;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserSettingsItem()Landroid/view/View;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$8;

    invoke-direct {p3, p2}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$8;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserSettingsItem()Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$9;

    invoke-direct {p2, p10}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$9;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_a

    :cond_c
    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getHomeItem()Landroid/view/View;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$10;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$10;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getFriendsItem()Landroid/view/View;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$11;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$11;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getSearchItem()Landroid/view/View;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$12;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$12;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getMentionsItem()Landroid/view/View;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$13;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$13;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserSettingsItem()Landroid/view/View;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$14;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$14;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->getUserSettingsItem()Landroid/view/View;

    move-result-object p1

    sget-object p2, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$15;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$updateView$15;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :goto_a
    return-void

    :cond_d
    const-string p1, "userAvatarPresenceViewController"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_e
    const-string p1, "navigationTabToViewMap"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_f
    const-string p1, "iconToNavigationTabMap"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_10
    const-string p1, "tintableIconToNavigationTabMap"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
