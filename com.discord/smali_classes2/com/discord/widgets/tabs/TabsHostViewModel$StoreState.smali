.class public final Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;
.super Ljava/lang/Object;
.source "TabsHostViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/tabs/TabsHostViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final guildIdToGuildMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation
.end field

.field private final leftPanelState:Lcom/discord/panels/PanelState;

.field private final myUserId:J

.field private final numTotalMentions:I

.field private final selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

.field private final userRelationships:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/panels/PanelState;",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            "J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;I",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "leftPanelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildIdToGuildMap"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userRelationships"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    iput-wide p3, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    iput-object p5, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    iput p6, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    iput-object p7, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;ILjava/lang/Object;)Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-wide p3, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    :cond_2
    move-wide v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p5, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    :cond_3
    move-object v2, p5

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget p6, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    :cond_4
    move v3, p6

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p7, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    :cond_5
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-wide p5, v0

    move-object p7, v2

    move p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->copy(Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;)Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/panels/PanelState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    return-object v0
.end method

.method public final component2()Lcom/discord/widgets/tabs/NavigationTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    return-wide v0
.end method

.method public final component4()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    return-object v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    return v0
.end method

.method public final component6()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;)Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/panels/PanelState;",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            "J",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;I",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;"
        }
    .end annotation

    const-string v0, "leftPanelState"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedTab"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildIdToGuildMap"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userRelationships"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

    move-object v1, v0

    move-wide v4, p3

    move v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;-><init>(Lcom/discord/panels/PanelState;Lcom/discord/widgets/tabs/NavigationTab;JLjava/util/Map;ILjava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    iget-object v1, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    iget-object v1, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    iget-wide v2, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    iget v1, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    iget-object p1, p1, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildIdToGuildMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    return-object v0
.end method

.method public final getLeftPanelState()Lcom/discord/panels/PanelState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    return-object v0
.end method

.method public final getMyUserId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    return-wide v0
.end method

.method public final getNumTotalMentions()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    return v0
.end method

.method public final getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    return-object v0
.end method

.method public final getUserRelationships()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    invoke-static {v2, v3}, Ld;->a(J)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "StoreState(leftPanelState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->leftPanelState:Lcom/discord/panels/PanelState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedTab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->selectedTab:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", myUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->myUserId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildIdToGuildMap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->guildIdToGuildMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numTotalMentions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->numTotalMentions:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", userRelationships="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->userRelationships:Ljava/util/Map;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->B(Ljava/lang/StringBuilder;Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
