.class public final Lcom/discord/widgets/tabs/WidgetTabsHost;
.super Lcom/discord/app/AppFragment;
.source "WidgetTabsHost.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/tabs/WidgetTabsHost$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final BOTTOM_TABS_DOWNWARD_ANIMATION_DURATION_MS:J = 0xc8L

.field private static final BOTTOM_TABS_UPWARD_ANIMATION_DURATION_MS:J = 0xfaL

.field public static final Companion:Lcom/discord/widgets/tabs/WidgetTabsHost$Companion;


# instance fields
.field private bottomNavAnimator:Landroid/animation/ValueAnimator;

.field private final bottomNavigationView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final friendsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

.field private final homeContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final mentionsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final navHost$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final nonFullscreenNavHost$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private previousBottomNavHeight:I

.field private previousShowBottomNav:Ljava/lang/Boolean;

.field private final tabToTabSelectionListenerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            "Lcom/discord/widgets/tabs/OnTabSelectedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final userSettingsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "bottomNavigationView"

    const-string v7, "getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "navHost"

    const-string v7, "getNavHost()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "nonFullscreenNavHost"

    const-string v7, "getNonFullscreenNavHost()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "homeContent"

    const-string v7, "getHomeContent()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "friendsContent"

    const-string v7, "getFriendsContent()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "mentionsContent"

    const-string v7, "getMentionsContent()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const-string v6, "userSettingsContent"

    const-string v7, "getUserSettingsContent()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/tabs/WidgetTabsHost$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/tabs/WidgetTabsHost$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/tabs/WidgetTabsHost;->Companion:Lcom/discord/widgets/tabs/WidgetTabsHost$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0bdf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bde

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->bottomNavigationView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0be4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->navHost$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0be5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->nonFullscreenNavHost$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0be2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->homeContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0be0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->friendsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0be3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->mentionsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0be6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->userSettingsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->tabToTabSelectionListenerMap:Ljava/util/Map;

    sget-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->Provider:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;->get()Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    return-void
.end method

.method public static final synthetic access$getBottomNavigationView$p(Lcom/discord/widgets/tabs/WidgetTabsHost;)Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/tabs/WidgetTabsHost;)Lcom/discord/widgets/tabs/TabsHostViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/tabs/WidgetTabsHost;Lcom/discord/widgets/tabs/TabsHostViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->handleEvent(Lcom/discord/widgets/tabs/TabsHostViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$onSearchClick(Lcom/discord/widgets/tabs/WidgetTabsHost;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->onSearchClick()V

    return-void
.end method

.method public static final synthetic access$onSettingsLongPress(Lcom/discord/widgets/tabs/WidgetTabsHost;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->onSettingsLongPress()V

    return-void
.end method

.method public static final synthetic access$setPanelWindowInsetsListeners(Lcom/discord/widgets/tabs/WidgetTabsHost;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->setPanelWindowInsetsListeners(Z)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/tabs/WidgetTabsHost;Lcom/discord/widgets/tabs/TabsHostViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/discord/widgets/tabs/WidgetTabsHost;Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->updateViews(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V

    return-void
.end method

.method private final configureSystemStatusBar()V
    .locals 11

    const v0, 0x7f04013d

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarColor$default(Landroidx/fragment/app/Fragment;IZILjava/lang/Object;)V

    iget-object v1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->observeState()Lrx/Observable;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/tabs/WidgetTabsHost;

    new-instance v8, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;

    invoke-direct {v8, p0, v0}, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;I)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->bottomNavigationView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    return-object v0
.end method

.method private final getContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getFriendsContent()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->friendsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getHomeContent()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->homeContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMentionsContent()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->mentionsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNavHost()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->navHost$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getNonFullscreenNavHost()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->nonFullscreenNavHost$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getUserSettingsContent()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->userSettingsContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/tabs/TabsHostViewModel$Event;)V
    .locals 2

    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel$Event$TrackFriendsListShown;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostViewModel$Event$TrackFriendsListShown;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->handleFriendsListShown()V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel$Event$DismissSearchDialog;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostViewModel$Event$DismissSearchDialog;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/discord/widgets/user/search/WidgetGlobalSearch;->Companion:Lcom/discord/widgets/user/search/WidgetGlobalSearch$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "parentFragmentManager"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/user/search/WidgetGlobalSearch$Companion;->dismiss(Landroidx/fragment/app/FragmentManager;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final handleFriendsListShown()V
    .locals 1

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-virtual {v0}, Lcom/discord/utilities/analytics/AnalyticsTracker;->friendsListViewed()V

    return-void
.end method

.method private final navigateToTab(Lcom/discord/widgets/tabs/NavigationTab;)V
    .locals 5

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getHomeContent()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/16 v4, 0x8

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getFriendsContent()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->FRIENDS:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_3

    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    const/16 v1, 0x8

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getMentionsContent()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->MENTIONS:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne p1, v1, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-eqz v1, :cond_5

    const/4 v1, 0x0

    goto :goto_5

    :cond_5
    const/16 v1, 0x8

    :goto_5
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getUserSettingsContent()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->SETTINGS:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne p1, v1, :cond_6

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_7

    goto :goto_7

    :cond_7
    const/16 v3, 0x8

    :goto_7
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->tabToTabSelectionListenerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/tabs/OnTabSelectedListener;

    if-eqz p1, :cond_8

    invoke-interface {p1}, Lcom/discord/widgets/tabs/OnTabSelectedListener;->onTabSelected()V

    :cond_8
    return-void
.end method

.method private final onSearchClick()V
    .locals 4

    sget-object v0, Lcom/discord/widgets/user/search/WidgetGlobalSearch;->Companion:Lcom/discord/widgets/user/search/WidgetGlobalSearch$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/widgets/user/search/WidgetGlobalSearch$Companion;->show$default(Lcom/discord/widgets/user/search/WidgetGlobalSearch$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method private final onSettingsLongPress()V
    .locals 1

    sget-object v0, Lcom/discord/widgets/user/WidgetUserStatusSheet;->Companion:Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/user/WidgetUserStatusSheet$Companion;->show(Landroidx/fragment/app/Fragment;)V

    return-void
.end method

.method private final setPanelWindowInsetsListeners(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getContainer()Landroid/view/ViewGroup;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$1;->INSTANCE:Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$1;

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getHomeContent()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$2;

    invoke-direct {v1, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$2;-><init>(Z)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getNavHost()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getNonFullscreenNavHost()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$3;

    invoke-direct {v1, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$3;-><init>(Z)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$4;->INSTANCE:Lcom/discord/widgets/tabs/WidgetTabsHost$setPanelWindowInsetsListeners$4;

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestApplyInsets()V

    return-void
.end method

.method public static synthetic setPanelWindowInsetsListeners$default(Lcom/discord/widgets/tabs/WidgetTabsHost;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->setPanelWindowInsetsListeners(Z)V

    return-void
.end method

.method private final updateNavHostMargins(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V
    .locals 4

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getNavHost()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    sget-object v2, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne v0, v2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getBottomNavHeight()I

    move-result p1

    :goto_0
    iget v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v1, v0, v2, v3, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getNavHost()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private final updateViews(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V
    .locals 13

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getShowBottomNav()Z

    move-result v11

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getBottomNavHeight()I

    move-result v12

    invoke-direct {p0, v1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->navigateToTab(Lcom/discord/widgets/tabs/NavigationTab;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$1;

    iget-object v3, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;

    if-eqz v3, :cond_6

    invoke-direct {v2, v3}, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$1;-><init>(Lcom/discord/widgets/tabs/TabsHostViewModel;)V

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getMyUserId()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getVisibleTabs()Ljava/util/Set;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getNumHomeNotifications()I

    move-result v7

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getNumFriendsNotifications()I

    move-result v8

    new-instance v9, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$2;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    new-instance v10, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$3;

    invoke-direct {v10, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$3;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    move v3, v11

    invoke-virtual/range {v0 .. v10}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->updateView(Lcom/discord/widgets/tabs/NavigationTab;Lkotlin/jvm/functions/Function1;ZJLjava/util/Set;IILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->updateNavHostMargins(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->previousShowBottomNav:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    iget v1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->previousBottomNavHeight:I

    const/4 v2, 0x0

    if-eq v12, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez p1, :cond_1

    if-eqz v1, :cond_5

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->bottomNavAnimator:Landroid/animation/ValueAnimator;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getTranslationY()F

    move-result p1

    const/4 v1, 0x2

    if-eqz v11, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    new-array v1, v1, [F

    aput p1, v1, v2

    const/4 p1, 0x0

    aput p1, v1, v0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    new-instance v0, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v0, 0xfa

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->bottomNavAnimator:Landroid/animation/ValueAnimator;

    goto :goto_1

    :cond_3
    if-lez v12, :cond_4

    new-array v1, v1, [F

    aput p1, v1, v2

    int-to-float p1, v12

    aput p1, v1, v0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    new-instance v0, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->bottomNavAnimator:Landroid/animation/ValueAnimator;

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object p1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_5
    :goto_1
    iput v12, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->previousBottomNavHeight:I

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->previousShowBottomNav:Ljava/lang/Boolean;

    return-void

    :cond_6
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02b9

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v10, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3f

    const/4 v9, 0x0

    move-object v1, v10

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;-><init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreUserRelationships;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, v0, v10}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/tabs/TabsHostViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026ostViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/tabs/TabsHostViewModel;

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->bottomNavAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->getBottomNavigationView()Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/tabs/BottomNavViewObserver;->Companion:Lcom/discord/widgets/tabs/BottomNavViewObserver$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/BottomNavViewObserver$Companion;->getINSTANCE()Lcom/discord/widgets/tabs/BottomNavViewObserver;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;->addHeightChangedListener(Lcom/discord/widgets/tabs/TabsHostBottomNavigationView$HeightChangedListener;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->setPanelWindowInsetsListeners$default(Lcom/discord/widgets/tabs/WidgetTabsHost;ZILjava/lang/Object;)V

    new-instance v0, Lcom/discord/widgets/tabs/WidgetTabsHost$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$onViewBound$1;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    const/4 v2, 0x2

    invoke-static {p0, v0, p1, v2, v1}, Lcom/discord/app/AppFragment;->setOnBackPressed$default(Lcom/discord/app/AppFragment;Lrx/functions/Func0;IILjava/lang/Object;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;

    const/4 v1, 0x0

    const-string v2, "viewModel"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v3, "viewModel.observeViewSta\u2026  .distinctUntilChanged()"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/tabs/WidgetTabsHost$onViewBoundOrOnResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->viewModel:Lcom/discord/widgets/tabs/TabsHostViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/TabsHostViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/tabs/WidgetTabsHost;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/tabs/WidgetTabsHost$onViewBoundOrOnResume$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->configureSystemStatusBar()V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public final registerTabSelectionListener(Lcom/discord/widgets/tabs/NavigationTab;Lcom/discord/widgets/tabs/OnTabSelectedListener;)V
    .locals 1

    const-string v0, "navigationTab"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onTabSelectedListener"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost;->tabToTabSelectionListenerMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
