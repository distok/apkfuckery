.class public final Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;
.super Lx/m/c/k;
.source "WidgetTabsHost.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/tabs/WidgetTabsHost;->configureSystemStatusBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $defaultStatusBarColor:I

.field public final synthetic this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/tabs/WidgetTabsHost;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;

    iput p2, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->$defaultStatusBarColor:I

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->invoke(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 4

    const-string v0, "state"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/discord/widgets/tabs/WidgetTabsHost;->access$setPanelWindowInsetsListeners(Lcom/discord/widgets/tabs/WidgetTabsHost;Z)V

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;

    const v1, 0x7f06026f

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->$defaultStatusBarColor:I

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$configureSystemStatusBar$1;->this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarColor$default(Landroidx/fragment/app/Fragment;IZILjava/lang/Object;)V

    return-void
.end method
