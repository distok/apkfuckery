.class public final Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;
.super Ljava/lang/Object;
.source "TabsHostViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/tabs/TabsHostViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final storeGuilds:Lcom/discord/stores/StoreGuilds;

.field private final storeMentions:Lcom/discord/stores/StoreMentions;

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;

.field private final storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

.field private final storeUser:Lcom/discord/stores/StoreUser;

.field private final storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3f

    const/4 v8, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;-><init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreUserRelationships;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreUserRelationships;)V
    .locals 1

    const-string v0, "storeNavigation"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeTabsNavigation"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeUser"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeGuilds"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeMentions"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeUserRelationships"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    iput-object p3, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    iput-object p4, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    iput-object p5, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeMentions:Lcom/discord/stores/StoreMentions;

    iput-object p6, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreUserRelationships;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getTabsNavigation()Lcom/discord/stores/StoreTabsNavigation;

    move-result-object p2

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getMentions()Lcom/discord/stores/StoreMentions;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object p6

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-direct/range {p2 .. p8}, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;-><init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreMentions;Lcom/discord/stores/StoreUserRelationships;)V

    return-void
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreNavigation;->observeLeftPanelState()Lrx/Observable;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreTabsNavigation;->observeSelectedTab()Lrx/Observable;

    move-result-object v2

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeUser:Lcom/discord/stores/StoreUser;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMeId()Lrx/Observable;

    move-result-object v3

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeGuilds:Lcom/discord/stores/StoreGuilds;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuilds;->observeGuilds()Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeMentions:Lcom/discord/stores/StoreMentions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreMentions;->getTotal()Lrx/Observable;

    move-result-object v5

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeUserRelationships:Lcom/discord/stores/StoreUserRelationships;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v6

    sget-object v7, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostViewModel$Factory$observeStoreState$1;

    invoke-static/range {v1 .. v7}, Lrx/Observable;->f(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026onships\n        )\n      }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/tabs/TabsHostViewModel;

    sget-object v0, Lcom/discord/widgets/tabs/BottomNavViewObserver;->Companion:Lcom/discord/widgets/tabs/BottomNavViewObserver$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/BottomNavViewObserver$Companion;->getINSTANCE()Lcom/discord/widgets/tabs/BottomNavViewObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    iget-object v2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/discord/widgets/tabs/TabsHostViewModel;-><init>(Lcom/discord/widgets/tabs/BottomNavViewObserver;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation;Lrx/Observable;)V

    return-object p1
.end method
