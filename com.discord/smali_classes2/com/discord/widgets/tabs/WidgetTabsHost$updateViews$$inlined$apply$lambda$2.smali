.class public final Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$2;
.super Ljava/lang/Object;
.source "WidgetTabsHost.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/tabs/WidgetTabsHost;->updateViews(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/tabs/WidgetTabsHost;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$2;->this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/tabs/WidgetTabsHost$updateViews$$inlined$apply$lambda$2;->this$0:Lcom/discord/widgets/tabs/WidgetTabsHost;

    invoke-static {v0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->access$getBottomNavigationView$p(Lcom/discord/widgets/tabs/WidgetTabsHost;)Lcom/discord/widgets/tabs/TabsHostBottomNavigationView;

    move-result-object v0

    const-string v1, "animator"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    const-string v1, "null cannot be cast to non-null type kotlin.Float"

    invoke-static {p1, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    return-void
.end method
