.class public final Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;
.super Ljava/lang/Object;
.source "TabsHostViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/tabs/TabsHostViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAT_LEAST_ONE_GUILD_TABS()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/widgets/tabs/TabsHostViewModel;->access$getAT_LEAST_ONE_GUILD_TABS$cp()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getNON_HOME_TAB_DESTINATIONS()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/widgets/tabs/TabsHostViewModel;->access$getNON_HOME_TAB_DESTINATIONS$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getNO_GUILD_TABS()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/widgets/tabs/TabsHostViewModel;->access$getNO_GUILD_TABS$cp()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final getTAB_DESTINATIONS()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/discord/widgets/tabs/TabsHostViewModel;->access$getTAB_DESTINATIONS$cp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
