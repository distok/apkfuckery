.class public final Lcom/discord/widgets/tabs/TabsHostViewModel;
.super Lf/a/b/l0;
.source "TabsHostViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;,
        Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;,
        Lcom/discord/widgets/tabs/TabsHostViewModel$Event;,
        Lcom/discord/widgets/tabs/TabsHostViewModel$Factory;,
        Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final AT_LEAST_ONE_GUILD_TABS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;

.field private static final NON_HOME_TAB_DESTINATIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field

.field private static final NO_GUILD_TABS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAB_DESTINATIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/tabs/NavigationTab;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bottomNavViewObserver:Lcom/discord/widgets/tabs/BottomNavViewObserver;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/tabs/TabsHostViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;

.field private storeState:Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private final storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->Companion:Lcom/discord/widgets/tabs/TabsHostViewModel$Companion;

    invoke-static {}, Lcom/discord/widgets/tabs/NavigationTab;->values()[Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->TAB_DESTINATIONS:Ljava/util/List;

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-static {v0, v1}, Lx/h/f;->minus(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->NON_HOME_TAB_DESTINATIONS:Ljava/util/List;

    const/4 v0, 0x3

    new-array v2, v0, [Lcom/discord/widgets/tabs/NavigationTab;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    sget-object v4, Lcom/discord/widgets/tabs/NavigationTab;->FRIENDS:Lcom/discord/widgets/tabs/NavigationTab;

    const/4 v5, 0x1

    aput-object v4, v2, v5

    sget-object v6, Lcom/discord/widgets/tabs/NavigationTab;->SETTINGS:Lcom/discord/widgets/tabs/NavigationTab;

    const/4 v7, 0x2

    aput-object v6, v2, v7

    invoke-static {v2}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/discord/widgets/tabs/TabsHostViewModel;->NO_GUILD_TABS:Ljava/util/Set;

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/discord/widgets/tabs/NavigationTab;

    aput-object v1, v2, v3

    aput-object v4, v2, v5

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->SEARCH:Lcom/discord/widgets/tabs/NavigationTab;

    aput-object v1, v2, v7

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->MENTIONS:Lcom/discord/widgets/tabs/NavigationTab;

    aput-object v1, v2, v0

    const/4 v0, 0x4

    aput-object v6, v2, v0

    invoke-static {v2}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->AT_LEAST_ONE_GUILD_TABS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/tabs/BottomNavViewObserver;Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/stores/StoreNavigation;Lrx/Observable;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/tabs/BottomNavViewObserver;",
            "Lcom/discord/stores/StoreTabsNavigation;",
            "Lcom/discord/stores/StoreNavigation;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "bottomNavViewObserver"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeTabsNavigation"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeNavigation"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeStateObservable"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    sget-object v2, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    sget-object v7, Lcom/discord/widgets/tabs/TabsHostViewModel;->NO_GUILD_TABS:Ljava/util/Set;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;-><init>(Lcom/discord/widgets/tabs/NavigationTab;ZIJLjava/util/Set;II)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->bottomNavViewObserver:Lcom/discord/widgets/tabs/BottomNavViewObserver;

    iput-object p2, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    iput-object p3, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object p4, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeStateObservable:Lrx/Observable;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p3

    const/4 p4, 0x0

    const/4 v0, 0x2

    invoke-static {p3, p0, p4, v0, p4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/tabs/TabsHostViewModel;

    new-instance v7, Lcom/discord/widgets/tabs/TabsHostViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/tabs/TabsHostViewModel$1;-><init>(Lcom/discord/widgets/tabs/TabsHostViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/BottomNavViewObserver;->observeHeight()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0, p4, v0, p4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/tabs/TabsHostViewModel;

    new-instance v7, Lcom/discord/widgets/tabs/TabsHostViewModel$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/tabs/TabsHostViewModel$2;-><init>(Lcom/discord/widgets/tabs/TabsHostViewModel;)V

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {p2}, Lcom/discord/stores/StoreTabsNavigation;->observeDismissTabsDialogEvent()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0, p4, v0, p4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/tabs/TabsHostViewModel;

    new-instance v7, Lcom/discord/widgets/tabs/TabsHostViewModel$3;

    invoke-direct {v7, p0}, Lcom/discord/widgets/tabs/TabsHostViewModel$3;-><init>(Lcom/discord/widgets/tabs/TabsHostViewModel;)V

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$dismissSearchDialog(Lcom/discord/widgets/tabs/TabsHostViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostViewModel;->dismissSearchDialog()V

    return-void
.end method

.method public static final synthetic access$getAT_LEAST_ONE_GUILD_TABS$cp()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->AT_LEAST_ONE_GUILD_TABS:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic access$getNON_HOME_TAB_DESTINATIONS$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->NON_HOME_TAB_DESTINATIONS:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getNO_GUILD_TABS$cp()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->NO_GUILD_TABS:Ljava/util/Set;

    return-object v0
.end method

.method public static final synthetic access$getTAB_DESTINATIONS$cp()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->TAB_DESTINATIONS:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$handleBottomNavHeight(Lcom/discord/widgets/tabs/TabsHostViewModel;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/TabsHostViewModel;->handleBottomNavHeight(I)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/tabs/TabsHostViewModel;Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/tabs/TabsHostViewModel;->handleStoreState(Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;)V

    return-void
.end method

.method private final dismissSearchDialog()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostViewModel$Event$DismissSearchDialog;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostViewModel$Event$DismissSearchDialog;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitTrackFriendsListShown()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostViewModel$Event$TrackFriendsListShown;->INSTANCE:Lcom/discord/widgets/tabs/TabsHostViewModel$Event$TrackFriendsListShown;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleBottomNavHeight(I)V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7b

    const/4 v11, 0x0

    move v4, p1

    invoke-static/range {v1 .. v11}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->copy$default(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;Lcom/discord/widgets/tabs/NavigationTab;ZIJLjava/util/Set;IIILjava/lang/Object;)Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;)V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeState:Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getLeftPanelState()Lcom/discord/panels/PanelState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getLeftPanelState()Lcom/discord/panels/PanelState;

    move-result-object v2

    sget-object v3, Lcom/discord/panels/PanelState$c;->a:Lcom/discord/panels/PanelState$c;

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_1

    sget-object v2, Lcom/discord/panels/PanelState$d;->a:Lcom/discord/panels/PanelState$d;

    invoke-static {v0, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    sget-object v2, Lcom/discord/widgets/tabs/TabsHostViewModel;->NON_HOME_TAB_DESTINATIONS:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    sget-object v2, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne v1, v2, :cond_2

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v2, 0x1

    :goto_3
    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getGuildIdToGuildMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_4

    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->AT_LEAST_ONE_GUILD_TABS:Ljava/util/Set;

    goto :goto_4

    :cond_4
    sget-object v0, Lcom/discord/widgets/tabs/TabsHostViewModel;->NO_GUILD_TABS:Ljava/util/Set;

    :goto_4
    move-object v6, v0

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getUserRelationships()Ljava/util/Map;

    move-result-object v0

    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_6

    const/4 v8, 0x1

    goto :goto_6

    :cond_6
    const/4 v8, 0x0

    :goto_6
    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v8, v7}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_7
    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v8

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getMyUserId()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getNumTotalMentions()I

    move-result v7

    const/4 v9, 0x4

    const/4 v10, 0x0

    invoke-static/range {v0 .. v10}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->copy$default(Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;Lcom/discord/widgets/tabs/NavigationTab;ZIJLjava/util/Set;IIILjava/lang/Object;)Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final handleBackPress()Z
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/TabsHostViewModel;->TAB_DESTINATIONS:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/discord/widgets/tabs/TabsHostViewModel;->selectTab(Lcom/discord/widgets/tabs/NavigationTab;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/tabs/TabsHostViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final selectTab(Lcom/discord/widgets/tabs/NavigationTab;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "tab"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->requireViewState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;

    invoke-virtual {v0}, Lcom/discord/widgets/tabs/TabsHostViewModel$ViewState;->getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, p1, v2, v3, v4}, Lcom/discord/stores/StoreTabsNavigation;->selectTab$default(Lcom/discord/stores/StoreTabsNavigation;Lcom/discord/widgets/tabs/NavigationTab;ZILjava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_2

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/tabs/TabsHostViewModel;->emitTrackFriendsListShown()V

    goto :goto_2

    :cond_1
    sget-object p1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne v0, p1, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeState:Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/tabs/TabsHostViewModel$StoreState;->getLeftPanelState()Lcom/discord/panels/PanelState;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v4

    :goto_0
    sget-object v0, Lcom/discord/panels/PanelState$c;->a:Lcom/discord/panels/PanelState$c;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/discord/stores/StoreNavigation$PanelAction;->CLOSE:Lcom/discord/stores/StoreNavigation$PanelAction;

    goto :goto_1

    :cond_3
    sget-object p1, Lcom/discord/stores/StoreNavigation$PanelAction;->OPEN:Lcom/discord/stores/StoreNavigation$PanelAction;

    :goto_1
    iget-object v0, p0, Lcom/discord/widgets/tabs/TabsHostViewModel;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-static {v0, p1, v4, v3, v4}, Lcom/discord/stores/StoreNavigation;->setNavigationPanelAction$default(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;ILjava/lang/Object;)V

    :goto_2
    return-void
.end method
