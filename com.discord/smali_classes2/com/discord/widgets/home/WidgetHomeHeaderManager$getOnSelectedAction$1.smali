.class public final Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;
.super Ljava/lang/Object;
.source "WidgetHomeHeaderManager.kt"

# interfaces
.implements Lrx/functions/Action2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHomeHeaderManager;->getOnSelectedAction(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/app/AppFragment;Lcom/discord/widgets/home/PanelLayout;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action2<",
        "Landroid/view/MenuItem;",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $appFragment:Lcom/discord/app/AppFragment;

.field public final synthetic $panelLayout:Lcom/discord/widgets/home/PanelLayout;

.field public final synthetic $this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/app/AppFragment;Lcom/discord/widgets/home/PanelLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$appFragment:Lcom/discord/app/AppFragment;

    iput-object p3, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$panelLayout:Lcom/discord/widgets/home/PanelLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final launchForSearch(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelChannel;->isPrivateType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannelId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/discord/widgets/search/WidgetSearch;->launchForChannel(JLandroid/content/Context;)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyType(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    const-string v1, "channel?.guildId ?: 0L"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/discord/widgets/search/WidgetSearch;->launchForGuild(JLandroid/content/Context;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private final launchForStartGroup(Landroid/content/Context;)V
    .locals 9

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannelId()J

    move-result-wide v3

    const/4 v5, 0x0

    const-string v6, "Toolbar"

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/channel/ChannelUtils;->inviteToChannel$default(Lcom/discord/utilities/channel/ChannelUtils;Landroid/content/Context;JLjava/lang/Long;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public call(Landroid/view/MenuItem;Landroid/content/Context;)V
    .locals 6

    const-string v0, "menuItem"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/user/calls/PrivateCallLauncher;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$appFragment:Lcom/discord/app/AppFragment;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "appFragment.parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v1, p2, v2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;-><init>(Lcom/discord/app/AppPermissions$Requests;Lcom/discord/app/AppComponent;Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getVoiceChannelSelected()Lcom/discord/stores/StoreVoiceChannelSelected;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreVoiceChannelSelected;->clear()V

    goto :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannelId()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVideoCall(J)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p2}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->launchForStartGroup(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_3
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$this_getOnSelectedAction:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannelId()J

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/user/calls/PrivateCallLauncher;->launchVoiceCall(J)V

    goto :goto_0

    :pswitch_4
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$panelLayout:Lcom/discord/widgets/home/PanelLayout;

    invoke-interface {p1}, Lcom/discord/widgets/home/PanelLayout;->openEndPanel()V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p2}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->launchForSearch(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/discord/widgets/friends/WidgetFriendsAdd;->Companion:Lcom/discord/widgets/friends/WidgetFriendsAdd$Companion;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    const-string v3, "Toolbar"

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/friends/WidgetFriendsAdd$Companion;->show$default(Lcom/discord/widgets/friends/WidgetFriendsAdd$Companion;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    :goto_0
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->$appFragment:Lcom/discord/app/AppFragment;

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-static {p1, v0, p2, v0}, Lcom/discord/app/AppFragment;->hideKeyboard$default(Lcom/discord/app/AppFragment;Landroid/view/View;ILjava/lang/Object;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0683
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/MenuItem;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;->call(Landroid/view/MenuItem;Landroid/content/Context;)V

    return-void
.end method
