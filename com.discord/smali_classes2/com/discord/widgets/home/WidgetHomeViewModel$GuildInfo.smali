.class public final Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;
.super Ljava/lang/Object;
.source "WidgetHomeViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/home/WidgetHomeViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GuildInfo"
.end annotation


# instance fields
.field private final guildId:J

.field private final isLurking:Z


# direct methods
.method public constructor <init>(JZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    iput-boolean p3, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;JZILjava/lang/Object;)Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-boolean p3, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->copy(JZ)Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    return-wide v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    return v0
.end method

.method public final copy(JZ)Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;-><init>(JZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    iget-boolean p1, p1, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final isLurking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GuildInfo(guildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", isLurking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
