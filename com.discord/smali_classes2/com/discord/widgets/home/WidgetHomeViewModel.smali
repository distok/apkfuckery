.class public final Lcom/discord/widgets/home/WidgetHomeViewModel;
.super Lf/a/b/l0;
.source "WidgetHomeViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;,
        Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;,
        Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;,
        Lcom/discord/widgets/home/WidgetHomeViewModel$Event;,
        Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final navPanelActionObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreNavigation$PanelAction;",
            ">;"
        }
    .end annotation
.end field

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private widgetHomeModel:Lcom/discord/widgets/home/WidgetHomeModel;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreNavigation;Lrx/Observable;Lrx/Observable;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreNavigation;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;",
            ">;",
            "Lrx/Observable<",
            "Lcom/discord/stores/StoreNavigation$PanelAction;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "storeNavigation"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "storeStateObservable"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "navPanelActionObservable"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;

    sget-object v5, Lcom/discord/panels/PanelState$a;->a:Lcom/discord/panels/PanelState$a;

    invoke-direct {v4, v5, v5}, Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;-><init>(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;)V

    invoke-direct {v0, v4}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/discord/widgets/home/WidgetHomeViewModel;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object v2, v0, Lcom/discord/widgets/home/WidgetHomeViewModel;->storeStateObservable:Lrx/Observable;

    iput-object v3, v0, Lcom/discord/widgets/home/WidgetHomeViewModel;->navPanelActionObservable:Lrx/Observable;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/discord/widgets/home/WidgetHomeViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-virtual/range {p2 .. p2}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    const-string v2, "storeStateObservable\n   \u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v0, v2, v4, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v5

    const-class v6, Lcom/discord/widgets/home/WidgetHomeViewModel;

    new-instance v11, Lcom/discord/widgets/home/WidgetHomeViewModel$1;

    invoke-direct {v11, v0}, Lcom/discord/widgets/home/WidgetHomeViewModel$1;-><init>(Lcom/discord/widgets/home/WidgetHomeViewModel;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v12, 0x1e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-static {v3, v0, v2, v4, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v14

    const-class v15, Lcom/discord/widgets/home/WidgetHomeViewModel;

    new-instance v1, Lcom/discord/widgets/home/WidgetHomeViewModel$2;

    invoke-direct {v1, v0}, Lcom/discord/widgets/home/WidgetHomeViewModel$2;-><init>(Lcom/discord/widgets/home/WidgetHomeViewModel;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x1e

    const/16 v22, 0x0

    move-object/from16 v20, v1

    invoke-static/range {v14 .. v22}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreNavigation;Lrx/Observable;Lrx/Observable;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/home/WidgetHomeViewModel;-><init>(Lcom/discord/stores/StoreNavigation;Lrx/Observable;Lrx/Observable;)V

    return-void
.end method

.method public static final synthetic access$handleNavDrawerAction(Lcom/discord/widgets/home/WidgetHomeViewModel;Lcom/discord/stores/StoreNavigation$PanelAction;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel;->handleNavDrawerAction(Lcom/discord/stores/StoreNavigation$PanelAction;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/home/WidgetHomeViewModel;Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel;->handleStoreState(Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V

    return-void
.end method

.method private final emitClosePanelsEvent()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ClosePanels;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ClosePanels;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitShowWelcomeSheet(J)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final emitUnlockLeftPanelEvent()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$UnlockLeftPanel;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Event$UnlockLeftPanel;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleNavDrawerAction(Lcom/discord/stores/StoreNavigation$PanelAction;)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHomeViewModel;->emitUnlockLeftPanelEvent()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHomeViewModel;->emitClosePanelsEvent()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/discord/widgets/home/WidgetHomeViewModel;->emitOpenLeftPanelEvent()V

    :goto_0
    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;->getLeftPanelState()Lcom/discord/panels/PanelState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;->getRightPanelState()Lcom/discord/panels/PanelState;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;->getGuildInfo()Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->getGuildId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;->getGuildInfo()Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;->isLurking()Z

    move-result p1

    new-instance v4, Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;

    invoke-direct {v4, v0, v1}, Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;-><init>(Lcom/discord/panels/PanelState;Lcom/discord/panels/PanelState;)V

    invoke-virtual {p0, v4}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-direct {p0, v2, v3}, Lcom/discord/widgets/home/WidgetHomeViewModel;->emitShowWelcomeSheet(J)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final emitOpenLeftPanelEvent()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$OpenLeftPanel;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Event$OpenLeftPanel;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final getWidgetHomeModel$app_productionDiscordExternalRelease()Lcom/discord/widgets/home/WidgetHomeModel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->widgetHomeModel:Lcom/discord/widgets/home/WidgetHomeModel;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/home/WidgetHomeViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onEndPanelStateChange(Lcom/discord/panels/PanelState;)V
    .locals 1

    const-string v0, "panelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreNavigation;->setRightPanelState(Lcom/discord/panels/PanelState;)V

    return-void
.end method

.method public final onStartPanelStateChange(Lcom/discord/panels/PanelState;)V
    .locals 1

    const-string v0, "panelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreNavigation;->setLeftPanelState(Lcom/discord/panels/PanelState;)V

    return-void
.end method

.method public final setWidgetHomeModel$app_productionDiscordExternalRelease(Lcom/discord/widgets/home/WidgetHomeModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel;->widgetHomeModel:Lcom/discord/widgets/home/WidgetHomeModel;

    return-void
.end method
