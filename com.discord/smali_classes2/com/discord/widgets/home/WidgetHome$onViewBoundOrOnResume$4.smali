.class public final Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;
.super Ljava/lang/Object;
.source "WidgetHome.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHome;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/utilities/surveys/SurveyUtils$Survey;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;

    invoke-direct {v0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;-><init>()V

    sput-object v0, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)Ljava/lang/Boolean;
    .locals 1

    sget-object v0, Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils$Survey$None;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/surveys/SurveyUtils$Survey;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;->call(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
