.class public final Lcom/discord/widgets/home/WidgetHomeViewModel$1;
.super Lx/m/c/k;
.source "WidgetHomeViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHomeViewModel;-><init>(Lcom/discord/stores/StoreNavigation;Lrx/Observable;Lrx/Observable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/home/WidgetHomeViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHomeViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$1;->this$0:Lcom/discord/widgets/home/WidgetHomeViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$1;->invoke(Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$1;->this$0:Lcom/discord/widgets/home/WidgetHomeViewModel;

    const-string v1, "storeState"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel;->access$handleStoreState(Lcom/discord/widgets/home/WidgetHomeViewModel;Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;)V

    return-void
.end method
