.class public final Lcom/discord/widgets/home/WidgetHomeModel;
.super Ljava/lang/Object;
.source "WidgetHomeModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/home/WidgetHomeModel$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/home/WidgetHomeModel$Companion;

.field public static final TYPE_UNAVAILABLE:I = -0x1


# instance fields
.field private final channel:Lcom/discord/models/domain/ModelChannel;

.field private final dmPresence:Lcom/discord/models/domain/ModelPresence;

.field private final isCallConnected:Z

.field private final isFriend:Z

.field private final isVideoSupported:Z

.field private final nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

.field private final type:I

.field private final unreadCount:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/home/WidgetHomeModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/home/WidgetHomeModel;->Companion:Lcom/discord/widgets/home/WidgetHomeModel$Companion;

    return-void
.end method

.method public constructor <init>(ILcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelPresence;IZZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    iput-object p3, p0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    iput p4, p0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    iput-boolean p5, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    iput-boolean p6, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    iput-boolean p7, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    iput-object p8, p0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/home/WidgetHomeModel;ILcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelPresence;IZZZLcom/discord/models/domain/ModelUser$NsfwAllowance;ILjava/lang/Object;)Lcom/discord/widgets/home/WidgetHomeModel;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move p1, v2

    move-object p2, v3

    move-object p3, v4

    move p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/discord/widgets/home/WidgetHomeModel;->copy(ILcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelPresence;IZZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)Lcom/discord/widgets/home/WidgetHomeModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    return v0
.end method

.method public final component2()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final component3()Lcom/discord/models/domain/ModelPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    return v0
.end method

.method public final component8()Lcom/discord/models/domain/ModelUser$NsfwAllowance;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    return-object v0
.end method

.method public final copy(ILcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelPresence;IZZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)Lcom/discord/widgets/home/WidgetHomeModel;
    .locals 10

    new-instance v9, Lcom/discord/widgets/home/WidgetHomeModel;

    move-object v0, v9

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/home/WidgetHomeModel;-><init>(ILcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelPresence;IZZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)V

    return-object v9
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/home/WidgetHomeModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/home/WidgetHomeModel;

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    iget v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    iget-object v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    iget-object v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    iget v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    iget-boolean v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    iget-boolean v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    iget-boolean v1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    iget-object p1, p1, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/discord/models/domain/ModelChannel;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public final getDmPresence()Lcom/discord/models/domain/ModelPresence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    return-object v0
.end method

.method public final getNsfwAllowed()Lcom/discord/models/domain/ModelUser$NsfwAllowance;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    return v0
.end method

.method public final getUnreadCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelPresence;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    move v3, v1

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    return v0
.end method

.method public final isCallConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    return v0
.end method

.method public final isChannelNsfw()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isNsfw()Z

    move-result v0

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final isFriend()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    return v0
.end method

.method public final isNsfwUnConsented()Z
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildsNsfw()Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v1

    const-string v2, "channel.guildId"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreGuildsNsfw;->isGuildNsfwGateAgreed(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isVideoSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "WidgetHomeModel(type="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->channel:Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", dmPresence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->dmPresence:Lcom/discord/models/domain/ModelPresence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unreadCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->unreadCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isFriend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isFriend:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isCallConnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isCallConnected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isVideoSupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->isVideoSupported:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", nsfwAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeModel;->nsfwAllowed:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
