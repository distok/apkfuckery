.class public final Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;
.super Ljava/lang/Object;
.source "WidgetHomePanelNsfw.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHomePanelNsfw;->configureUI(Lcom/discord/widgets/home/WidgetHomeModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $model:Lcom/discord/widgets/home/WidgetHomeModel;

.field public final synthetic this$0:Lcom/discord/widgets/home/WidgetHomePanelNsfw;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHomePanelNsfw;Lcom/discord/widgets/home/WidgetHomeModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;->this$0:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;->$model:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;->this$0:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    invoke-static {p1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->access$getGuildsNsfwStore$p(Lcom/discord/widgets/home/WidgetHomePanelNsfw;)Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;->$model:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    const-string v1, "model.channel?.guildId ?: 0L"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreGuildsNsfw;->allow(J)V

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;->this$0:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->access$setContainerViewHidden(Lcom/discord/widgets/home/WidgetHomePanelNsfw;Z)V

    return-void
.end method
