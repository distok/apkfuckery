.class public final Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetHomeViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/home/WidgetHomeViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final storeGuildSelected:Lcom/discord/stores/StoreGuildSelected;

.field private final storeLurking:Lcom/discord/stores/StoreLurking;

.field private final storeNavigation:Lcom/discord/stores/StoreNavigation;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;-><init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreLurking;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreLurking;)V
    .locals 1

    const-string v0, "storeNavigation"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeGuildSelected"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeLurking"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeGuildSelected:Lcom/discord/stores/StoreGuildSelected;

    iput-object p3, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeLurking:Lcom/discord/stores/StoreLurking;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreLurking;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    sget-object p3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p3}, Lcom/discord/stores/StoreStream$Companion;->getLurking()Lcom/discord/stores/StoreLurking;

    move-result-object p3

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;-><init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreLurking;)V

    return-void
.end method

.method public static final synthetic access$getStoreLurking$p(Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;)Lcom/discord/stores/StoreLurking;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeLurking:Lcom/discord/stores/StoreLurking;

    return-object p0
.end method

.method private final observeStoreState()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/home/WidgetHomeViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeGuildSelected:Lcom/discord/stores/StoreGuildSelected;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildSelected;->observeSelectedGuildId()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1;-><init>(Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v1}, Lcom/discord/stores/StoreNavigation;->observeLeftPanelState()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v2}, Lcom/discord/stores/StoreNavigation;->observeRightPanelState()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$1;

    invoke-static {v1, v2, v0, v3}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ildInfo\n        )\n      }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/home/WidgetHomeViewModel;

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->observeStoreState()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->storeNavigation:Lcom/discord/stores/StoreNavigation;

    invoke-virtual {v2}, Lcom/discord/stores/StoreNavigation;->getNavigationPanelAction()Lrx/Observable;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/discord/widgets/home/WidgetHomeViewModel;-><init>(Lcom/discord/stores/StoreNavigation;Lrx/Observable;Lrx/Observable;)V

    return-object p1
.end method
