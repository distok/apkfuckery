.class public final Lcom/discord/widgets/home/WidgetHome;
.super Lcom/discord/app/AppFragment;
.source "WidgetHome.kt"

# interfaces
.implements Lcom/discord/widgets/tabs/OnTabSelectedListener;
.implements Lf/a/f/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/home/WidgetHome$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/home/WidgetHome$Companion;

.field private static final DELAY_DRAWER_OPEN_FINISH:J = 0x7d0L

.field private static final DELAY_DRAWER_OPEN_START:J = 0x3e8L


# instance fields
.field private final centerPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final chatInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final connectedList$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

.field private final guildList$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildListAddHint$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final inlineVoiceCallControls$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final leftPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final leftPanelManager:Lcom/discord/widgets/home/LeftPanelManager;

.field private final localeManager:Lcom/discord/utilities/locale/LocaleManager;

.field private onGuildListAddHintCreate:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final overlappingPanels$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private panelLoading:Lcom/discord/widgets/home/WidgetHomePanelLoading;

.field private panelNsfw:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

.field private final rightPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final rightPanelRoundedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

.field private final toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final toolbarTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unreadCountView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xe

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/home/WidgetHome;

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "toolbarTitle"

    const-string v7, "getToolbarTitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "guildListAddHint"

    const-string v7, "getGuildListAddHint()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "unreadCountView"

    const-string v7, "getUnreadCountView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "container"

    const-string v7, "getContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "overlappingPanels"

    const-string v7, "getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "leftPanel"

    const-string v7, "getLeftPanel()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "rightPanel"

    const-string v7, "getRightPanel()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "rightPanelRoundedContainer"

    const-string v7, "getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "centerPanel"

    const-string v7, "getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "guildList"

    const-string v7, "getGuildList()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "connectedList"

    const-string v7, "getConnectedList()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "chatInput"

    const-string v7, "getChatInput()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const-string v6, "inlineVoiceCallControls"

    const-string v7, "getInlineVoiceCallControls()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/home/WidgetHome$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/home/WidgetHome$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/home/WidgetHome;->Companion:Lcom/discord/widgets/home/WidgetHome$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0047

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0abd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->toolbarTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04f0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->guildListAddHint$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0577

    invoke-static {p0, v0}, Ly/a/g0;->e(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->unreadCountView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bcc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bcd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->overlappingPanels$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0573

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->leftPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0576

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->rightPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a064c

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->rightPanelRoundedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0568

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->centerPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bcb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->guildList$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bc5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->connectedList$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bbf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->chatInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bc2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->inlineVoiceCallControls$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/home/LeftPanelManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {v0, v1, v1, v2, v1}, Lcom/discord/widgets/home/LeftPanelManager;-><init>(Lcom/discord/stores/StoreChannelsSelected;Lcom/discord/stores/StoreGuildSelected;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->leftPanelManager:Lcom/discord/widgets/home/LeftPanelManager;

    new-instance v0, Lcom/discord/utilities/locale/LocaleManager;

    invoke-direct {v0}, Lcom/discord/utilities/locale/LocaleManager;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->localeManager:Lcom/discord/utilities/locale/LocaleManager;

    sget-object v0, Lcom/discord/widgets/home/WidgetHome$onGuildListAddHintCreate$1;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$onGuildListAddHintCreate$1;

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->onGuildListAddHintCreate:Lkotlin/jvm/functions/Function1;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getTabsNavigation()Lcom/discord/stores/StoreTabsNavigation;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    sget-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->Provider:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;->get()Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    return-void
.end method

.method public static final synthetic access$configureFirstOpen(Lcom/discord/widgets/home/WidgetHome;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->configureFirstOpen()V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHome;->configureUI(Lcom/discord/widgets/home/WidgetHomeModel;)V

    return-void
.end method

.method public static final synthetic access$getChatInput$p(Lcom/discord/widgets/home/WidgetHome;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getChatInput()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getConnectedList$p(Lcom/discord/widgets/home/WidgetHome;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getConnectedList()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGuildList$p(Lcom/discord/widgets/home/WidgetHome;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getGuildList()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getInlineVoiceCallControls$p(Lcom/discord/widgets/home/WidgetHome;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getInlineVoiceCallControls()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getLocaleManager$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/utilities/locale/LocaleManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/home/WidgetHome;->localeManager:Lcom/discord/utilities/locale/LocaleManager;

    return-object p0
.end method

.method public static final synthetic access$getOverlappingPanels$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/HomePanelsLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPanelNsfw$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/WidgetHomePanelNsfw;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/home/WidgetHome;->panelNsfw:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/WidgetHomeViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleBackPressed(Lcom/discord/widgets/home/WidgetHome;)Z
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->handleBackPressed()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHome;->handleEvent(Lcom/discord/widgets/home/WidgetHomeViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$handleGlobalStatusIndicatorState(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHome;->handleGlobalStatusIndicatorState(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V

    return-void
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHome;->handleViewState(Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$setPanelNsfw$p(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomePanelNsfw;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHome;->panelNsfw:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    return-void
.end method

.method public static final synthetic access$showSurvey(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHome;->showSurvey(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V

    return-void
.end method

.method public static final synthetic access$showUrgentMessageDialog(Lcom/discord/widgets/home/WidgetHome;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->showUrgentMessageDialog()V

    return-void
.end method

.method private final configureFirstOpen()V
    .locals 12

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getConnectionOpen()Lcom/discord/stores/StoreConnectionOpen;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/discord/stores/StoreConnectionOpen;->observeConnectionOpen$default(Lcom/discord/stores/StoreConnectionOpen;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreChannels;->observeAllChannels()Lrx/Observable;

    move-result-object v0

    sget-object v2, Lcom/discord/widgets/home/WidgetHome$configureFirstOpen$1;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$configureFirstOpen$1;

    invoke-static {v1, v0, v2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0, v3}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lg0/l/a/u1$a;->a:Lg0/l/a/u1;

    new-instance v2, Lg0/l/a/u;

    iget-object v0, v0, Lrx/Observable;->d:Lrx/Observable$a;

    invoke-direct {v2, v0, v1}, Lg0/l/a/u;-><init>(Lrx/Observable$a;Lrx/Observable$b;)V

    invoke-static {v2}, Lrx/Observable;->c0(Lrx/Observable$a;)Lrx/Observable;

    move-result-object v3

    const-string v0, "Observable\n        .comb\u2026       }\n        .first()"

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    new-instance v9, Lcom/discord/widgets/home/WidgetHome$configureFirstOpen$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/home/WidgetHome$configureFirstOpen$2;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final configureLeftPanel()V
    .locals 12

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->leftPanelManager:Lcom/discord/widgets/home/LeftPanelManager;

    invoke-virtual {v0}, Lcom/discord/widgets/home/LeftPanelManager;->observeShouldLockOpen()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    new-instance v9, Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final configureNavigationDrawerAction(Lcom/discord/stores/StoreNavigation;)V
    .locals 12

    invoke-virtual {p1}, Lcom/discord/stores/StoreNavigation;->getNavigationPanelAction()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/stores/StoreNavigation;

    new-instance v9, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;

    invoke-direct {v9, p0, p1}, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;-><init>(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/stores/StoreNavigation;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final configureOverlappingPanels()V
    .locals 5

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;

    new-instance v3, Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lcom/discord/panels/OverlappingPanelsLayout;->registerStartPanelStateListeners([Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    new-array v1, v1, [Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;

    new-instance v2, Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$2;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/discord/panels/OverlappingPanelsLayout;->registerEndPanelStateListeners([Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/home/WidgetHomeModel;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel;->setWidgetHomeModel$app_productionDiscordExternalRelease(Lcom/discord/widgets/home/WidgetHomeModel;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->isOnHomeTab()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeHeaderManager;

    invoke-virtual {v0, p0, p1}, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->configure(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeModel;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->panelNsfw:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->configureUI(Lcom/discord/widgets/home/WidgetHomeModel;)V

    :cond_1
    return-void

    :cond_2
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->centerPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    return-object v0
.end method

.method private final getChatInput()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->chatInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getConnectedList()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->connectedList$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getGuildList()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->guildList$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getGuildListAddHint()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->guildListAddHint$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getInlineVoiceCallControls()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->inlineVoiceCallControls$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getLeftPanel()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->leftPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->overlappingPanels$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/home/HomePanelsLayout;

    return-object v0
.end method

.method private final getRightPanel()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->rightPanel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->rightPanelRoundedContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    return-object v0
.end method

.method private final getToolbar()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final getToolbarTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->toolbarTitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUnreadCountView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->unreadCountView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/home/WidgetHome;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleBackPressed()Z
    .locals 3

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/panels/OverlappingPanelsLayout;->getSelectedPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->moveTaskToBack(Z)Z

    :goto_0
    return v1
.end method

.method private final handleEvent(Lcom/discord/widgets/home/WidgetHomeViewModel$Event;)V
    .locals 2

    sget-object v0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$OpenLeftPanel;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Event$OpenLeftPanel;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->openStartPanel()V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ClosePanels;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ClosePanels;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/panels/OverlappingPanelsLayout;->closePanels()V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$UnlockLeftPanel;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeViewModel$Event$UnlockLeftPanel;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object p1

    sget-object v0, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->UNLOCKED:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    invoke-virtual {p1, v0}, Lcom/discord/panels/OverlappingPanelsLayout;->setStartPanelLockState(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->getGuildId()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/home/WidgetHome;->showWelcomeSheet(J)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final handleGlobalStatusIndicatorState(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 0

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->unroundPanelCorners()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->roundPanelCorners()V

    :goto_0
    return-void
.end method

.method private final handleHomeConfig(Lcom/discord/widgets/home/HomeConfig;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/home/HomeConfig;->getGuildWelcomeSheetId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/home/HomeConfig;->getGuildWelcomeSheetId()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/home/WidgetHome;->showWelcomeSheet(J)V

    :cond_1
    return-void
.end method

.method private final handleViewState(Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;->getLeftPanelState()Lcom/discord/panels/PanelState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/panels/OverlappingPanelsLayout;->handleStartPanelState(Lcom/discord/panels/PanelState;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$ViewState;->getRightPanelState()Lcom/discord/panels/PanelState;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->handleEndPanelState(Lcom/discord/panels/PanelState;)V

    return-void
.end method

.method private final isOnHomeTab()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->storeTabsNavigation:Lcom/discord/stores/StoreTabsNavigation;

    invoke-virtual {v0}, Lcom/discord/stores/StoreTabsNavigation;->getSelectedTab()Lcom/discord/widgets/tabs/NavigationTab;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final roundPanelCorners()V
    .locals 2

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopLeftRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopRightRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopLeftRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopRightRadius(F)V

    return-void
.end method

.method private final setPanelWindowInsetsListeners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setForwardingWindowInsetsListener(Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getLeftPanel()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/home/WidgetHome$setPanelWindowInsetsListeners$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/home/WidgetHome$setPanelWindowInsetsListeners$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/home/WidgetHome$setPanelWindowInsetsListeners$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/home/WidgetHome$setPanelWindowInsetsListeners$2;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanel()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/home/WidgetHome$setPanelWindowInsetsListeners$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/home/WidgetHome$setPanelWindowInsetsListeners$3;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method

.method private final setUpToolbar()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040155

    invoke-static {v0, v1}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v0

    const v1, 0x7f080375

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f121856

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p0, v3, v1, v2, v0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getToolbar()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/home/WidgetHome$setUpToolbar$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/home/WidgetHome$setUpToolbar$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final showSurvey(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V
    .locals 18

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/surveys/SurveyUtils$Survey;->getNoticeKey()Ljava/lang/String;

    move-result-object v2

    new-instance v13, Lcom/discord/widgets/home/WidgetHome$showSurvey$1;

    move-object/from16 v1, p1

    invoke-direct {v13, v1}, Lcom/discord/widgets/home/WidgetHome$showSurvey$1;-><init>(Lcom/discord/utilities/surveys/SurveyUtils$Survey;)V

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x5

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const/16 v14, 0xe6

    const/16 v16, 0x0

    move-object v1, v15

    move-object/from16 v17, v0

    move-object v0, v15

    move-object/from16 v15, v16

    invoke-direct/range {v1 .. v15}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v1, v17

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    return-void
.end method

.method private final showUrgentMessageDialog()V
    .locals 18

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v0

    new-instance v15, Lcom/discord/stores/StoreNotices$Notice;

    sget-object v13, Lcom/discord/widgets/home/WidgetHome$showUrgentMessageDialog$1;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$showUrgentMessageDialog$1;

    const-string v2, "URGENT_MESSAGE_DIALOG"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const/16 v14, 0x26

    const/16 v16, 0x0

    move-object v1, v15

    move-object/from16 v17, v0

    move-object v0, v15

    move-object/from16 v15, v16

    invoke-direct/range {v1 .. v15}, Lcom/discord/stores/StoreNotices$Notice;-><init>(Ljava/lang/String;Lcom/discord/utilities/time/Clock;JIZLjava/util/List;JJLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v1, v17

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreNotices;->requestToShow(Lcom/discord/stores/StoreNotices$Notice;)V

    return-void
.end method

.method private final showWelcomeSheet(J)V
    .locals 3

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildWelcomeScreens()Lcom/discord/stores/StoreGuildWelcomeScreens;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuildWelcomeScreens;->hasWelcomeScreenBeenSeen(J)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet;->Companion:Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/widgets/guilds/join/WidgetGuildWelcomeSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;J)V

    :cond_0
    return-void
.end method

.method private final unroundPanelCorners()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopLeftRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getCenterPanel()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopRightRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopLeftRadius(F)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;->updateTopRightRadius(F)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0226

    return v0
.end method

.method public final getPanelLayout()Lcom/discord/widgets/home/PanelLayout;
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    return-object v0
.end method

.method public final lockCloseRightPanel(Z)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->CLOSE:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->UNLOCKED:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->setEndPanelLockState(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    sget-object p1, Lf/a/f/a;->b:Lf/a/f/a;

    new-instance p1, Lcom/discord/widgets/home/WidgetHome$onCreate$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/home/WidgetHome$onCreate$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const-string v0, "provider"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object p1, Lf/a/f/a;->a:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public onGestureRegionsUpdate(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    const-string v0, "gestureRegions"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->setChildGestureRegions(Ljava/util/List;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    invoke-static {}, Lf/a/f/b$b;->a()Lf/a/f/b;

    move-result-object v0

    const-string v1, "gestureRegionsListener"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lf/a/f/b;->e:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->panelNsfw:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->unbind()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    invoke-static {}, Lf/a/f/b$b;->a()Lf/a/f/b;

    move-result-object v0

    const-string v1, "gestureRegionsListener"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lf/a/f/b;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-interface {p0, v1}, Lf/a/f/b$a;->onGestureRegionsUpdate(Ljava/util/List;)V

    iget-object v0, v0, Lf/a/f/b;->e:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onTabSelected()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeViewModel;->getWidgetHomeModel$app_productionDiscordExternalRelease()Lcom/discord/widgets/home/WidgetHomeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeHeaderManager;

    invoke-virtual {v1, p0, v0}, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->configure(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeModel;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getOverlappingPanels()Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/panels/OverlappingPanelsLayout;->getSelectedPanel()Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    move-result-object v0

    sget-object v1, Lcom/discord/panels/OverlappingPanelsLayout$Panel;->CENTER:Lcom/discord/panels/OverlappingPanelsLayout$Panel;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getToolbarTitle()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->sendAccessibilityEvent(I)V

    :cond_1
    return-void

    :cond_2
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 9

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    new-instance v0, Lcom/discord/utilities/locale/LocaleManager;

    invoke-direct {v0}, Lcom/discord/utilities/locale/LocaleManager;-><init>()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/utilities/locale/LocaleManager;->getPrimaryLocale(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    const/16 v3, 0x8

    invoke-static {v3}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v3

    if-eqz v0, :cond_1

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_1

    :cond_1
    move v4, v3

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :goto_2
    iget v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v2, v4, v0, v3, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getRightPanelRoundedContainer()Lcom/discord/utilities/view/rounded/RoundedRelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    new-instance v8, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;-><init>(Lcom/discord/stores/StoreNavigation;Lcom/discord/stores/StoreGuildSelected;Lcom/discord/stores/StoreLurking;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, p0, v8}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v2, Lcom/discord/widgets/home/WidgetHomeViewModel;

    invoke-virtual {v0, v2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v2, "ViewModelProvider(\n     \u2026omeViewModel::class.java)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/home/WidgetHomeViewModel;

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->setUpToolbar()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object v0

    goto :goto_3

    :cond_3
    move-object v0, v2

    :goto_3
    instance-of v3, v0, Lcom/discord/widgets/tabs/WidgetTabsHost;

    if-nez v3, :cond_4

    move-object v0, v2

    :cond_4
    check-cast v0, Lcom/discord/widgets/tabs/WidgetTabsHost;

    if-eqz v0, :cond_5

    sget-object v3, Lcom/discord/widgets/tabs/NavigationTab;->HOME:Lcom/discord/widgets/tabs/NavigationTab;

    invoke-virtual {v0, v3, p0}, Lcom/discord/widgets/tabs/WidgetTabsHost;->registerTabSelectionListener(Lcom/discord/widgets/tabs/NavigationTab;Lcom/discord/widgets/tabs/OnTabSelectedListener;)V

    :cond_5
    new-instance v0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    const-string v4, "childFragmentManager"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1, v3}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;-><init>(Landroid/view/View;Landroidx/fragment/app/FragmentManager;)V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->panelNsfw:Lcom/discord/widgets/home/WidgetHomePanelNsfw;

    new-instance v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;

    invoke-direct {v0, p1}, Lcom/discord/widgets/home/WidgetHomePanelLoading;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->panelLoading:Lcom/discord/widgets/home/WidgetHomePanelLoading;

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHome;->onGuildListAddHintCreate:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getGuildListAddHint()Landroid/view/View;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p1, Lcom/discord/widgets/home/WidgetHome$onViewBound$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBound$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/4 v0, 0x2

    invoke-static {p0, p1, v1, v0, v2}, Lcom/discord/app/AppFragment;->setOnBackPressed$default(Lcom/discord/app/AppFragment;Lrx/functions/Func0;IILjava/lang/Object;)V

    sget-object p1, Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink;->Companion:Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeNuxSamsungLink$Companion;->enqueue(Landroid/content/Context;Lcom/discord/utilities/time/Clock;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->setPanelWindowInsetsListeners()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 14

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_HOME_CONFIG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/home/HomeConfig;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/home/HomeConfig;

    invoke-direct {p0, v0}, Lcom/discord/widgets/home/WidgetHome;->handleHomeConfig(Lcom/discord/widgets/home/HomeConfig;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->panelLoading:Lcom/discord/widgets/home/WidgetHomePanelLoading;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Lcom/discord/widgets/home/WidgetHomePanelLoading;->configure(Lcom/discord/app/AppFragment;)V

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    const-string v1, "viewModel"

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->viewModel:Lcom/discord/widgets/home/WidgetHomeViewModel;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object v0, Lcom/discord/widgets/home/WidgetHomeModel;->Companion:Lcom/discord/widgets/home/WidgetHomeModel$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel$Companion;->get()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/home/WidgetHome;

    new-instance v9, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$3;

    invoke-direct {v9, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$3;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object v0, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;->INSTANCE:Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/accessibility/AccessibilityDetectionNavigator;->enqueueNoticeWhenEnabled(Lcom/discord/app/AppComponent;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNavigation()Lcom/discord/stores/StoreNavigation;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/discord/widgets/home/WidgetHome;->configureNavigationDrawerAction(Lcom/discord/stores/StoreNavigation;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->configureOverlappingPanels()V

    sget-object v3, Lcom/discord/utilities/surveys/SurveyUtils;->INSTANCE:Lcom/discord/utilities/surveys/SurveyUtils;

    invoke-virtual {v3}, Lcom/discord/utilities/surveys/SurveyUtils;->getSurveyToShow()Lrx/Observable;

    move-result-object v4

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$1;

    invoke-virtual {v3, v4}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;->INSTANCE:Lcom/discord/utilities/rx/ObservableExtensionsKt$filterNull$2;

    invoke-virtual {v3, v4}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v3

    const-string v4, "filter { it != null }.map { it!! }"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$4;

    invoke-virtual {v3, v4}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v5

    const-string v3, "SurveyUtils\n        .get\u2026SurveyUtils.Survey.None }"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v6, Lcom/discord/widgets/home/WidgetHome;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    new-instance v11, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$5;

    invoke-direct {v11, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$5;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/16 v12, 0x1e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getNux()Lcom/discord/stores/StoreNux;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreNux;->getNuxState()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$6;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$6;

    invoke-virtual {v3, v4}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v5

    const-string v3, "StoreStream\n        .get\u2026 .filter { it.firstOpen }"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static/range {v5 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    invoke-static {v3, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/home/WidgetHome;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$7;

    invoke-direct {v10, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$7;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$8;->INSTANCE:Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$8;

    invoke-virtual {v0, v3}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v3, "StoreStream\n        .get\u2026sUnreadUrgentMessages() }"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/home/WidgetHome;

    new-instance v10, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$9;

    invoke-direct {v10, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$9;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->configureLeftPanel()V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome;->globalStatusIndicatorStateObserver:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->observeState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/home/WidgetHome;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v7, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$10;

    invoke-direct {v7, p0}, Lcom/discord/widgets/home/WidgetHome$onViewBoundOrOnResume$10;-><init>(Lcom/discord/widgets/home/WidgetHome;)V

    const/16 v8, 0x1e

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_2
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_3
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public final setOnGuildListAddHintCreate(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onGuildListAddHintCreate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHome;->onGuildListAddHintCreate:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final unreadCountView()Landroid/widget/TextView;
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHome;->getUnreadCountView()Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method
