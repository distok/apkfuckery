.class public final Lcom/discord/widgets/home/WidgetHomeHeaderManager;
.super Ljava/lang/Object;
.source "WidgetHomeHeaderManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;
    }
.end annotation


# static fields
.field private static final ANALYTICS_SOURCE:Ljava/lang/String; = "Toolbar"

.field public static final INSTANCE:Lcom/discord/widgets/home/WidgetHomeHeaderManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager;

    invoke-direct {v0}, Lcom/discord/widgets/home/WidgetHomeHeaderManager;-><init>()V

    sput-object v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeHeaderManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getHeaderData(Lcom/discord/widgets/home/WidgetHomeModel;Landroid/content/Context;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;
    .locals 7

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_6

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannelId()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v6, v0, v4

    if-lez v6, :cond_1

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v2, v1, p2, v3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/utilities/channel/GuildChannelIconUtilsKt;->guildChannelIcon(Lcom/discord/models/domain/ModelChannel;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;

    invoke-direct {v0, v2, v2}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    if-eqz p1, :cond_3

    sget-object v1, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v1, p1, p2, v3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    :cond_3
    const p1, 0x7f080330

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object p1

    if-eqz p1, :cond_5

    sget-object v1, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v1, p1, p2, v3}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->getDisplayName(Lcom/discord/models/domain/ModelChannel;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    :cond_5
    const p1, 0x7f0802da

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_6
    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;

    const p1, 0x7f120476

    invoke-virtual {p2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, v2}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_0
    return-object v0
.end method

.method private final getOnConfigureAction(Lcom/discord/widgets/home/WidgetHomeModel;)Lrx/functions/Action1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/home/WidgetHomeModel;",
            ")",
            "Lrx/functions/Action1<",
            "Landroid/view/Menu;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnConfigureAction$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnConfigureAction$1;-><init>(Lcom/discord/widgets/home/WidgetHomeModel;)V

    return-object v0
.end method

.method private final getOnSelectedAction(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/app/AppFragment;Lcom/discord/widgets/home/PanelLayout;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;-><init>(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/app/AppFragment;Lcom/discord/widgets/home/PanelLayout;)V

    return-object v0
.end method


# virtual methods
.method public final configure(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeModel;)V
    .locals 5

    const-string/jumbo v0, "widgetHome"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    invoke-virtual {p2}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/discord/models/domain/ModelChannel;->isGuildTextyType(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/discord/widgets/home/WidgetHome;->lockCloseRightPanel(Z)V

    invoke-virtual {p1}, Lcom/discord/app/AppFragment;->bindToolbar()Lkotlin/Unit;

    invoke-virtual {p1}, Lcom/discord/app/AppFragment;->setActionBarTitleLayoutExpandedTappableArea()Lkotlin/Unit;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v3, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeHeaderManager;

    const-string v4, "it"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, p2, v0}, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->getHeaderData(Lcom/discord/widgets/home/WidgetHomeModel;Landroid/content/Context;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;->component1()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$HeaderData;->component2()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(Ljava/lang/CharSequence;Ljava/lang/Integer;)Lkotlin/Unit;

    :cond_1
    new-instance v0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;

    invoke-direct {v0, p2, p2, p1}, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/widgets/home/WidgetHome;)V

    invoke-virtual {p1, v0}, Lcom/discord/app/AppFragment;->setActionBarTitleClick(Landroid/view/View$OnClickListener;)Lkotlin/Unit;

    const v0, 0x7f0e0006

    sget-object v3, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeHeaderManager;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHome;->getPanelLayout()Lcom/discord/widgets/home/PanelLayout;

    move-result-object v4

    invoke-direct {v3, p2, p1, v4}, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->getOnSelectedAction(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/app/AppFragment;Lcom/discord/widgets/home/PanelLayout;)Lcom/discord/widgets/home/WidgetHomeHeaderManager$getOnSelectedAction$1;

    move-result-object v4

    invoke-direct {v3, p2}, Lcom/discord/widgets/home/WidgetHomeHeaderManager;->getOnConfigureAction(Lcom/discord/widgets/home/WidgetHomeModel;)Lrx/functions/Action1;

    move-result-object v3

    invoke-virtual {p1, v0, v4, v3}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;Lrx/functions/Action1;)Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p2}, Lcom/discord/widgets/home/WidgetHomeModel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isDM()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->isSystemDM()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/discord/app/AppFragment;->getActionBarTitleLayout()Lcom/discord/views/ToolbarTitleLayout;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p2}, Lcom/discord/widgets/home/WidgetHomeModel;->getDmPresence()Lcom/discord/models/domain/ModelPresence;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/discord/views/ToolbarTitleLayout;->a(Lcom/discord/models/domain/ModelPresence;Z)V

    :cond_3
    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHome;->unreadCountView()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p2}, Lcom/discord/widgets/home/WidgetHomeModel;->getUnreadCount()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-lez v0, :cond_4

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    const/4 v0, 0x0

    if-eqz v1, :cond_5

    goto :goto_3

    :cond_5
    move-object p2, v0

    :goto_3
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_7
    return-void
.end method
