.class public final Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;
.super Lcom/discord/widgets/home/WidgetHomeViewModel$Event;
.source "WidgetHomeViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/home/WidgetHomeViewModel$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowGuildWelcomeSheet"
.end annotation


# instance fields
.field private final guildId:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/home/WidgetHomeViewModel$Event;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;JILjava/lang/Object;)Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->copy(J)Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    return-wide v0
.end method

.method public final copy(J)Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;-><init>(J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ShowGuildWelcomeSheet(guildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Event$ShowGuildWelcomeSheet;->guildId:J

    const-string v3, ")"

    invoke-static {v0, v1, v2, v3}, Lf/e/c/a/a;->v(Ljava/lang/StringBuilder;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
