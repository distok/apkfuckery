.class public final Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;
.super Lx/m/c/k;
.source "WidgetHome.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHome;->configureLeftPanel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/home/WidgetHome;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHome;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v0}, Lcom/discord/widgets/home/WidgetHome;->access$getOverlappingPanels$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->setStartPanelUseFullPortraitWidth(Z)V

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->OPEN:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/panels/OverlappingPanelsLayout$LockState;->UNLOCKED:Lcom/discord/panels/OverlappingPanelsLayout$LockState;

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome$configureLeftPanel$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v0}, Lcom/discord/widgets/home/WidgetHome;->access$getOverlappingPanels$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/panels/OverlappingPanelsLayout;->setStartPanelLockState(Lcom/discord/panels/OverlappingPanelsLayout$LockState;)V

    return-void
.end method
