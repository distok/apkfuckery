.class public final Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$1;
.super Ljava/lang/Object;
.source "WidgetHome.kt"

# interfaces
.implements Lcom/discord/panels/OverlappingPanelsLayout$PanelStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHome;->configureOverlappingPanels()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/home/WidgetHome;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHome;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPanelStateChange(Lcom/discord/panels/PanelState;)V
    .locals 1

    const-string v0, "panelState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome$configureOverlappingPanels$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v0}, Lcom/discord/widgets/home/WidgetHome;->access$getViewModel$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/WidgetHomeViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel;->onStartPanelStateChange(Lcom/discord/panels/PanelState;)V

    return-void
.end method
