.class public final Lcom/discord/widgets/home/WidgetHomePanelNsfw;
.super Ljava/lang/Object;
.source "WidgetHomePanelNsfw.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/home/WidgetHomePanelNsfw$Companion;
    }
.end annotation


# static fields
.field private static final Companion:Lcom/discord/widgets/home/WidgetHomePanelNsfw$Companion;

.field private static final HIDE_DELAY_MILLIS:J = 0x1f4L


# instance fields
.field private final fragmentManager:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroidx/fragment/app/FragmentManager;",
            ">;"
        }
    .end annotation
.end field

.field private guildsNsfwAcceptView:Landroid/widget/Button;

.field private guildsNsfwContainerView:Landroid/view/View;

.field private guildsNsfwDescriptionView:Landroid/widget/TextView;

.field private final guildsNsfwHideHandler:Landroid/os/Handler;

.field private final guildsNsfwHideRunnable:Ljava/lang/Runnable;

.field private guildsNsfwImageView:Landroid/widget/ImageView;

.field private guildsNsfwRejectView:Landroid/widget/Button;

.field private final guildsNsfwStore:Lcom/discord/stores/StoreGuildsNsfw;

.field private guildsNsfwTitleView:Landroid/widget/TextView;

.field private stub:Landroid/view/ViewStub;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->Companion:Lcom/discord/widgets/home/WidgetHomePanelNsfw$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroidx/fragment/app/FragmentManager;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fragmentManager"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->fragmentManager:Ljava/lang/ref/WeakReference;

    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuildsNsfw()Lcom/discord/stores/StoreGuildsNsfw;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwStore:Lcom/discord/stores/StoreGuildsNsfw;

    const p2, 0x7f0a056c

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->stub:Landroid/view/ViewStub;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideHandler:Landroid/os/Handler;

    new-instance p1, Lcom/discord/widgets/home/WidgetHomePanelNsfw$guildsNsfwHideRunnable$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/home/WidgetHomePanelNsfw$guildsNsfwHideRunnable$1;-><init>(Lcom/discord/widgets/home/WidgetHomePanelNsfw;)V

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public static final synthetic access$getGuildsNsfwStore$p(Lcom/discord/widgets/home/WidgetHomePanelNsfw;)Lcom/discord/stores/StoreGuildsNsfw;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwStore:Lcom/discord/stores/StoreGuildsNsfw;

    return-object p0
.end method

.method public static final synthetic access$setContainerViewHidden(Lcom/discord/widgets/home/WidgetHomePanelNsfw;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->setContainerViewHidden(Z)V

    return-void
.end method

.method private final setChatListHidden(Z)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->fragmentManager:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/fragment/app/FragmentManager;

    if-eqz v0, :cond_1

    const-string v1, "fragmentManager.get() ?: return"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0a0bc0

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "fragmentManager.findFrag\u2026dget_chat_list) ?: return"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "fragmentManager.beginTransaction()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentTransaction;->hide(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentTransaction;->show(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    :goto_0
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    :cond_1
    return-void
.end method

.method private final setContainerViewHidden(Z)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_2

    :cond_0
    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-direct {p0, v1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->setChatListHidden(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    if-nez p1, :cond_5

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->setChatListHidden(Z)V

    :cond_5
    :goto_0
    return-void
.end method

.method private final toggleContainerVisibility(ZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)V
    .locals 10

    sget-object v0, Lcom/discord/models/domain/ModelUser$NsfwAllowance;->DISALLOWED:Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p3, v0, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p1, :cond_a

    if-nez p2, :cond_1

    if-eqz p3, :cond_a

    :cond_1
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->stub:Landroid/view/ViewStub;

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->stub:Landroid/view/ViewStub;

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    sget-object v0, Lcom/discord/widgets/home/WidgetHomePanelNsfw$toggleContainerVisibility$1$1;->INSTANCE:Lcom/discord/widgets/home/WidgetHomePanelNsfw$toggleContainerVisibility$1$1;

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    invoke-virtual {p1}, Landroid/view/View;->requestApplyInsets()V

    const v0, 0x7f0a056f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwAcceptView:Landroid/widget/Button;

    const v0, 0x7f0a0570

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwRejectView:Landroid/widget/Button;

    const v0, 0x7f0a056e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwImageView:Landroid/widget/ImageView;

    const v0, 0x7f0a0572

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwTitleView:Landroid/widget/TextView;

    const v0, 0x7f0a0571

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwDescriptionView:Landroid/widget/TextView;

    :cond_2
    if-eqz p3, :cond_7

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    if-eqz p1, :cond_9

    iget-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwAcceptView:Landroid/widget/Button;

    if-eqz p2, :cond_3

    invoke-static {p2, v2}, Landroidx/core/view/ViewKt;->setVisible(Landroid/view/View;Z)V

    :cond_3
    iget-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwImageView:Landroid/widget/ImageView;

    if-eqz p2, :cond_4

    const p3, 0x7f0804cd

    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_4
    iget-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwRejectView:Landroid/widget/Button;

    if-eqz p2, :cond_5

    const p3, 0x7f120297

    invoke-static {p1, p3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwTitleView:Landroid/widget/TextView;

    if-eqz p2, :cond_6

    const p3, 0x7f1200e3

    invoke-static {p1, p3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    iget-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwDescriptionView:Landroid/widget/TextView;

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string p3, "view.context"

    invoke-static {v3, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f1200e2

    new-array v0, v1, [Ljava/lang/Object;

    sget-object v1, Lf/a/b/g;->a:Lf/a/b/g;

    const-wide v4, 0x1ac68a0653L

    const-string v6, "h_5206f3f2-0ee4-4380-b50a-25319e45bc7c"

    invoke-virtual {v1, v4, v5, v6}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1, p3, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1c

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwDescriptionView:Landroid/widget/TextView;

    if-eqz p1, :cond_9

    iget-object p3, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    if-eqz p3, :cond_8

    const p2, 0x7f1200e1

    invoke-static {p3, p2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object p2

    :cond_8
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    :goto_1
    invoke-direct {p0, v2}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->setContainerViewHidden(Z)V

    goto :goto_2

    :cond_a
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideHandler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideRunnable:Ljava/lang/Runnable;

    const-wide/16 v0, 0x1f4

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_2
    return-void
.end method


# virtual methods
.method public final configureUI(Lcom/discord/widgets/home/WidgetHomeModel;)V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->isChannelNsfw()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->isNsfwUnConsented()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getNsfwAllowed()Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->toggleContainerVisibility(ZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwRejectView:Landroid/widget/Button;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$1;-><init>(Lcom/discord/widgets/home/WidgetHomePanelNsfw;Lcom/discord/widgets/home/WidgetHomeModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwAcceptView:Landroid/widget/Button;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/home/WidgetHomePanelNsfw$configureUI$2;-><init>(Lcom/discord/widgets/home/WidgetHomePanelNsfw;Lcom/discord/widgets/home/WidgetHomeModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public final dispatchApplyWindowInsets(Landroidx/core/view/WindowInsetsCompat;)V
    .locals 1

    const-string v0, "insets"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwContainerView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Landroidx/core/view/ViewCompat;->dispatchApplyWindowInsets(Landroid/view/View;Landroidx/core/view/WindowInsetsCompat;)Landroidx/core/view/WindowInsetsCompat;

    :cond_0
    return-void
.end method

.method public final unbind()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomePanelNsfw;->guildsNsfwHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
