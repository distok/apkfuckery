.class public final Lcom/discord/widgets/home/WidgetHomePanelLoading;
.super Ljava/lang/Object;
.source "WidgetHomePanelLoading.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/home/WidgetHomePanelLoading$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/home/WidgetHomePanelLoading$Companion;

.field private static panelInitialized:Z


# instance fields
.field private final panelCenter:Landroid/view/View;

.field private final panelLoading:Landroid/view/View;

.field private final panelLoadingLogo:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/home/WidgetHomePanelLoading$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/home/WidgetHomePanelLoading$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->Companion:Lcom/discord/widgets/home/WidgetHomePanelLoading$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0a0574

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.home_panel_loading)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoading:Landroid/view/View;

    const v0, 0x7f0a0575

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.home_panel_loading_logo)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoadingLogo:Landroid/view/View;

    const v0, 0x7f0a0568

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.home_panel_center)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelCenter:Landroid/view/View;

    invoke-direct {p0}, Lcom/discord/widgets/home/WidgetHomePanelLoading;->centerLogoRelativeToLoadingScreen()V

    return-void
.end method

.method public static final synthetic access$setLoadingPanelVisibility(Lcom/discord/widgets/home/WidgetHomePanelLoading;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/home/WidgetHomePanelLoading;->setLoadingPanelVisibility(ZZ)V

    return-void
.end method

.method private final centerLogoRelativeToLoadingScreen()V
    .locals 5

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoadingLogo:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoadingLogo:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "panelLoadingLogo.resources"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/display/DisplayUtils;->getStatusBarHeight(Landroid/content/res/Resources;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, -0x40000000    # -2.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Lf/h/a/f/f/n/g;->roundToInt(F)I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoadingLogo:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private final setLoadingPanelVisibility(ZZ)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoading:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    sput-boolean p1, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelInitialized:Z

    const/16 v1, 0x8

    if-eqz p1, :cond_3

    if-eqz p2, :cond_2

    iget-object v3, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoading:Landroid/view/View;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x7

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeOut$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    iget-object v10, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelCenter:Landroid/view/View;

    const-wide/16 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xf

    const/16 v17, 0x0

    invoke-static/range {v10 .. v17}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeIn$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v3, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoading:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelCenter:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/home/WidgetHome;

    invoke-virtual {v1, v2}, Lcom/discord/stores/StoreAnalytics;->appUiViewed(Ljava/lang/Class;)V

    goto :goto_2

    :cond_3
    iget-object v3, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelCenter:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelLoading:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final configure(Lcom/discord/app/AppFragment;)V
    .locals 12

    const-string v0, "fragment"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/home/WidgetHomeModel;->Companion:Lcom/discord/widgets/home/WidgetHomeModel$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/home/WidgetHomeModel$Companion;->getInitialized()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/home/WidgetHomePanelLoading;

    new-instance v9, Lcom/discord/widgets/home/WidgetHomePanelLoading$configure$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/home/WidgetHomePanelLoading$configure$1;-><init>(Lcom/discord/widgets/home/WidgetHomePanelLoading;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-boolean p1, Lcom/discord/widgets/home/WidgetHomePanelLoading;->panelInitialized:Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/home/WidgetHomePanelLoading;->setLoadingPanelVisibility(ZZ)V

    return-void
.end method
