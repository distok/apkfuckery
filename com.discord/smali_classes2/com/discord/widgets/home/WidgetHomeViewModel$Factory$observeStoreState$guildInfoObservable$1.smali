.class public final Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1;
.super Ljava/lang/Object;
.source "WidgetHomeViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Ljava/lang/Long;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1;->this$0:Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1;->call(Ljava/lang/Long;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/Long;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/home/WidgetHomeViewModel$GuildInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1;->this$0:Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;

    invoke-static {v0}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;->access$getStoreLurking$p(Lcom/discord/widgets/home/WidgetHomeViewModel$Factory;)Lcom/discord/stores/StoreLurking;

    move-result-object v0

    const-string v1, "selectedGuildId"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreLurking;->isLurkingObs(J)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/home/WidgetHomeViewModel$Factory$observeStoreState$guildInfoObservable$1$1;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
