.class public final Lcom/discord/widgets/home/HomeConfig;
.super Ljava/lang/Object;
.source "HomeConfig.kt"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final ageGated:Z

.field private final guildWelcomeSheetId:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2, v0}, Lcom/discord/widgets/home/HomeConfig;-><init>(Ljava/lang/Long;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    iput-boolean p2, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Long;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/home/HomeConfig;-><init>(Ljava/lang/Long;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/home/HomeConfig;Ljava/lang/Long;ZILjava/lang/Object;)Lcom/discord/widgets/home/HomeConfig;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/home/HomeConfig;->copy(Ljava/lang/Long;Z)Lcom/discord/widgets/home/HomeConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    return v0
.end method

.method public final copy(Ljava/lang/Long;Z)Lcom/discord/widgets/home/HomeConfig;
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/HomeConfig;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/home/HomeConfig;-><init>(Ljava/lang/Long;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/home/HomeConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/home/HomeConfig;

    iget-object v0, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    iget-boolean p1, p1, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAgeGated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    return v0
.end method

.method public final getGuildWelcomeSheetId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "HomeConfig(guildWelcomeSheetId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/home/HomeConfig;->guildWelcomeSheetId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", ageGated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/home/HomeConfig;->ageGated:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
