.class public final Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;
.super Lx/m/c/k;
.source "WidgetHome.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHome;->configureNavigationDrawerAction(Lcom/discord/stores/StoreNavigation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreNavigation$PanelAction;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $this_configureNavigationDrawerAction:Lcom/discord/stores/StoreNavigation;

.field public final synthetic this$0:Lcom/discord/widgets/home/WidgetHome;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/stores/StoreNavigation;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;->$this_configureNavigationDrawerAction:Lcom/discord/stores/StoreNavigation;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreNavigation$PanelAction;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;->invoke(Lcom/discord/stores/StoreNavigation$PanelAction;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreNavigation$PanelAction;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;->$this_configureNavigationDrawerAction:Lcom/discord/stores/StoreNavigation;

    iget-object v1, p0, Lcom/discord/widgets/home/WidgetHome$configureNavigationDrawerAction$1;->this$0:Lcom/discord/widgets/home/WidgetHome;

    invoke-static {v1}, Lcom/discord/widgets/home/WidgetHome;->access$getOverlappingPanels$p(Lcom/discord/widgets/home/WidgetHome;)Lcom/discord/widgets/home/HomePanelsLayout;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/discord/stores/StoreNavigation;->setNavigationPanelAction(Lcom/discord/stores/StoreNavigation$PanelAction;Lcom/discord/widgets/home/PanelLayout;)V

    return-void
.end method
