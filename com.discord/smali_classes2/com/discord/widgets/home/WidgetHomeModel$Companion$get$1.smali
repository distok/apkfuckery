.class public final Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;
.super Ljava/lang/Object;
.source "WidgetHomeModel.kt"

# interfaces
.implements Lrx/functions/Func7;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHomeModel$Companion;->get()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func7<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/models/domain/ModelChannel;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelPresence;",
        ">;",
        "Ljava/lang/Long;",
        "Ljava/lang/Integer;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Ljava/lang/Integer;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/widgets/home/WidgetHomeModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;

    invoke-direct {v0}, Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;->INSTANCE:Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/Boolean;)Lcom/discord/widgets/home/WidgetHomeModel;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelChannel;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPresence;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/discord/widgets/home/WidgetHomeModel;"
        }
    .end annotation

    move-object/from16 v0, p6

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v2

    invoke-static {v2}, Lcom/discord/models/domain/ModelChannel;->isPrivateType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v4, p4

    goto :goto_1

    :cond_1
    move-object v4, p4

    move-object v2, v3

    :goto_1
    invoke-static {p4, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    const/4 v6, 0x0

    :goto_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, -0x1

    :goto_3
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    if-eqz v4, :cond_4

    move-object v5, p3

    invoke-static {v4, p3}, Lf/e/c/a/a;->b0(Lcom/discord/models/domain/ModelUser;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelPresence;

    goto :goto_4

    :cond_4
    move-object v4, v3

    :goto_4
    const-string v5, "unreadCount"

    move-object/from16 v7, p5

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p5 .. p5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const-string v7, "userRelationships"

    invoke-static {v0, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/discord/models/domain/ModelChannel;->getDMRecipient()Lcom/discord/models/domain/ModelUser;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_5

    :cond_5
    move-object v7, v3

    :goto_5
    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/discord/models/domain/ModelUserRelationship;->isType(Ljava/lang/Integer;I)Z

    move-result v7

    const-string v0, "isVideoSupported"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p7 .. p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getNsfwAllowed()Lcom/discord/models/domain/ModelUser$NsfwAllowance;

    move-result-object v0

    move-object v9, v0

    goto :goto_6

    :cond_6
    move-object v9, v3

    :goto_6
    new-instance v10, Lcom/discord/widgets/home/WidgetHomeModel;

    move-object v0, v10

    move v1, v2

    move-object v2, p2

    move-object v3, v4

    move v4, v5

    move v5, v7

    move v7, v8

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/home/WidgetHomeModel;-><init>(ILcom/discord/models/domain/ModelChannel;Lcom/discord/models/domain/ModelPresence;IZZZLcom/discord/models/domain/ModelUser$NsfwAllowance;)V

    return-object v10
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    check-cast p2, Lcom/discord/models/domain/ModelChannel;

    check-cast p3, Ljava/util/Map;

    check-cast p4, Ljava/lang/Long;

    check-cast p5, Ljava/lang/Integer;

    check-cast p6, Ljava/util/Map;

    check-cast p7, Ljava/lang/Boolean;

    invoke-virtual/range {p0 .. p7}, Lcom/discord/widgets/home/WidgetHomeModel$Companion$get$1;->call(Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelChannel;Ljava/util/Map;Ljava/lang/Long;Ljava/lang/Integer;Ljava/util/Map;Ljava/lang/Boolean;)Lcom/discord/widgets/home/WidgetHomeModel;

    move-result-object p1

    return-object p1
.end method
