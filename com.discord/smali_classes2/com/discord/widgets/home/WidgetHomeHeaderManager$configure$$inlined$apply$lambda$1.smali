.class public final Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "WidgetHomeHeaderManager.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/home/WidgetHomeHeaderManager;->configure(Lcom/discord/widgets/home/WidgetHome;Lcom/discord/widgets/home/WidgetHomeModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $model$inlined:Lcom/discord/widgets/home/WidgetHomeModel;

.field public final synthetic $this_apply:Lcom/discord/widgets/home/WidgetHomeModel;

.field public final synthetic $widgetHome$inlined:Lcom/discord/widgets/home/WidgetHome;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/widgets/home/WidgetHomeModel;Lcom/discord/widgets/home/WidgetHome;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/widgets/home/WidgetHomeModel;

    iput-object p2, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;->$model$inlined:Lcom/discord/widgets/home/WidgetHomeModel;

    iput-object p3, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;->$widgetHome$inlined:Lcom/discord/widgets/home/WidgetHome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;->$this_apply:Lcom/discord/widgets/home/WidgetHomeModel;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHomeModel;->getType()I

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/home/WidgetHomeHeaderManager$configure$$inlined$apply$lambda$1;->$widgetHome$inlined:Lcom/discord/widgets/home/WidgetHome;

    invoke-virtual {p1}, Lcom/discord/widgets/home/WidgetHome;->getPanelLayout()Lcom/discord/widgets/home/PanelLayout;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/widgets/home/PanelLayout;->openEndPanel()V

    :goto_0
    return-void
.end method
