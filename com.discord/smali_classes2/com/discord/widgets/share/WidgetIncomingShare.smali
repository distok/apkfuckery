.class public final Lcom/discord/widgets/share/WidgetIncomingShare;
.super Lcom/discord/app/AppFragment;
.source "WidgetIncomingShare.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/share/WidgetIncomingShare$Model;,
        Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;,
        Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;,
        Lcom/discord/widgets/share/WidgetIncomingShare$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/share/WidgetIncomingShare$Companion;

.field private static final EXTRA_RECIPIENT:Ljava/lang/String; = "EXTRA_RECIPIENT"

.field private static final FLIPPER_RESULTS:I = 0x0

.field private static final FLIPPER_RESULTS_EMPTY:I = 0x1

.field private static final FLIPPER_SEARCH:I = 0x0

.field private static final FLIPPER_SELECTED:I = 0x1


# instance fields
.field private final activityActionPreview$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final commentInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final commentPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;",
            ">;"
        }
    .end annotation
.end field

.field private final dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private previewAdapter:Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;

.field private final previewList$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final previewListWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private queryString:Ljava/lang/String;

.field private resultsAdapter:Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

.field private final resultsFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchEt$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final searchQueryPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final searchResultsRv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final selectedReceiver$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final selectedReceiverPublisher:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedReceiverRemoveIv$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xc

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v3, "dimmer"

    const-string v4, "getDimmer()Lcom/discord/utilities/dimmer/DimmerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "commentInput"

    const-string v7, "getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "previewListWrap"

    const-string v7, "getPreviewListWrap()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "previewList"

    const-string v7, "getPreviewList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "activityActionPreview"

    const-string v7, "getActivityActionPreview()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "scrollView"

    const-string v7, "getScrollView()Landroidx/core/widget/NestedScrollView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "resultsFlipper"

    const-string v7, "getResultsFlipper()Landroid/widget/ViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "searchFlipper"

    const-string v7, "getSearchFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "searchEt"

    const-string v7, "getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "searchResultsRv"

    const-string v7, "getSearchResultsRv()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "selectedReceiver"

    const-string v7, "getSelectedReceiver()Lcom/discord/widgets/user/search/ViewGlobalSearchItem;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const-string v6, "selectedReceiverRemoveIv"

    const-string v7, "getSelectedReceiverRemoveIv()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/share/WidgetIncomingShare$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/share/WidgetIncomingShare$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/share/WidgetIncomingShare;->Companion:Lcom/discord/widgets/share/WidgetIncomingShare$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a035a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0404

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->commentInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0406

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->previewListWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0405

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->previewList$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0403

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->activityActionPreview$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0878

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a05a6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->resultsFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a087f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0407

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchEt$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0408

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchResultsRv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bb4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiver$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bb5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiverRemoveIv$delegate:Lkotlin/properties/ReadOnlyProperty;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->contentPublisher:Lrx/subjects/BehaviorSubject;

    const-string v0, ""

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->commentPublisher:Lrx/subjects/BehaviorSubject;

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchQueryPublisher:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiverPublisher:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$configureUi(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$Model;Lcom/discord/utilities/time/Clock;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/share/WidgetIncomingShare;->configureUi(Lcom/discord/widgets/share/WidgetIncomingShare$Model;Lcom/discord/utilities/time/Clock;)V

    return-void
.end method

.method public static final synthetic access$getCommentInput$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCommentPublisher$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->commentPublisher:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getContentPublisher$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->contentPublisher:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getQueryString$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->queryString:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getScrollView$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Landroidx/core/widget/NestedScrollView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getScrollView()Landroidx/core/widget/NestedScrollView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSearchEt$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSearchQueryPublisher$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchQueryPublisher:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getSearchResultsRv$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchResultsRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSelectedReceiverPublisher$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiverPublisher:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$initialize(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare;->initialize(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V

    return-void
.end method

.method public static final synthetic access$onSendClicked(Lcom/discord/widgets/share/WidgetIncomingShare;Landroid/content/Context;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;ZIZ)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/discord/widgets/share/WidgetIncomingShare;->onSendClicked(Landroid/content/Context;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;ZIZ)V

    return-void
.end method

.method public static final synthetic access$onSendCompleted(Lcom/discord/widgets/share/WidgetIncomingShare;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->onSendCompleted()V

    return-void
.end method

.method public static final synthetic access$setQueryString$p(Lcom/discord/widgets/share/WidgetIncomingShare;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->queryString:Ljava/lang/String;

    return-void
.end method

.method private final configureAdapter(Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;)V
    .locals 8

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getPreviewList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getPreviewList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setNestedScrollingEnabled(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getPreviewList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    const/16 p1, 0x8

    invoke-static {p1}, Lcom/discord/utilities/dimen/DimenUtils;->dpToPixels(I)I

    move-result v3

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getPreviewList()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    new-instance v7, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    move v2, v3

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/view/recycler/PaddedItemDecorator;-><init>(IIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p1, v7}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method

.method private final configureUi(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->previewAdapter:Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;

    const/4 v1, 0x0

    const-string v2, "previewAdapter"

    if-eqz v0, :cond_4

    new-instance v3, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$5;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$5;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V

    invoke-virtual {v0, p1, v3}, Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;->setData(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Lkotlin/jvm/functions/Function1;)V

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->previewAdapter:Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getUris()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getPreviewListWrap()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    if-eqz p1, :cond_2

    const p1, 0x7f12008c

    goto :goto_2

    :cond_2
    const p1, 0x7f1218f5

    :goto_2
    invoke-static {v0, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setHint(Lcom/google/android/material/textfield/TextInputLayout;I)V

    return-void

    :cond_3
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final configureUi(Lcom/discord/widgets/share/WidgetIncomingShare$Model;Lcom/discord/utilities/time/Clock;)V
    .locals 11

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getContentModel()Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/share/WidgetIncomingShare;->configureUi(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$1;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$Model;)V

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditorActionListener(Lcom/google/android/material/textfield/TextInputLayout;Lkotlin/jvm/functions/Function3;)Lkotlin/Unit;

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getReceiver()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    :cond_0
    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getComment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getContentModel()Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getUris()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getGameInviteModel()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_6

    const v0, 0x7f0e000d

    goto :goto_4

    :cond_6
    const v0, 0x7f0e000c

    :goto_4
    new-instance v4, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$2;

    invoke-direct {v4, p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$2;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$Model;)V

    invoke-virtual {p0, v0, v4, v1}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu(ILrx/functions/Action2;Lrx/functions/Action1;)Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getReceiver()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v0

    instance-of v4, v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;

    if-eqz v4, :cond_7

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSelectedReceiver()Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getReceiver()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;

    invoke-virtual {v0, v4}, Lcom/discord/widgets/user/search/ViewGlobalSearchItem;->onConfigure(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;)V

    goto :goto_5

    :cond_7
    instance-of v4, v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;

    if-eqz v4, :cond_8

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSelectedReceiver()Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getReceiver()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;

    invoke-virtual {v0, v4}, Lcom/discord/widgets/user/search/ViewGlobalSearchItem;->onConfigure(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;)V

    goto :goto_5

    :cond_8
    instance-of v0, v0, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSelectedReceiver()Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getReceiver()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;

    invoke-virtual {v0, v4}, Lcom/discord/widgets/user/search/ViewGlobalSearchItem;->onConfigure(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;)V

    goto :goto_5

    :cond_9
    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    :cond_a
    const v6, 0x7f0e000c

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v5, p0

    invoke-static/range {v5 .. v10}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu$default(Lcom/discord/app/AppFragment;ILrx/functions/Action2;Lrx/functions/Action1;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    :cond_b
    :goto_5
    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getResultsFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getSearchModel()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;->getData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->resultsAdapter:Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

    if-eqz v0, :cond_12

    new-instance v4, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$$inlined$apply$lambda$1;

    invoke-direct {v4, p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$Model;)V

    invoke-virtual {v0, v4}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setOnUpdated(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple$OnUpdated;)V

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getSearchModel()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel;->getData()Ljava/util/List;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_c
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    invoke-interface {v7}, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getReceiver()Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;

    move-result-object v8

    if-eqz v8, :cond_d

    invoke-interface {v8}, Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;->getKey()Ljava/lang/String;

    move-result-object v8

    goto :goto_7

    :cond_d
    move-object v8, v1

    :goto_7
    invoke-static {v7, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_e
    invoke-virtual {v0, v5}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    new-instance v1, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$$inlined$apply$lambda$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare$configureUi$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$Model;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;->setOnSelectedListener(Lkotlin/jvm/functions/Function4;)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getActivityActionPreview()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getGameInviteModel()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object v1

    if-eqz v1, :cond_f

    goto :goto_8

    :cond_f
    const/4 v2, 0x0

    :goto_8
    if-eqz v2, :cond_10

    goto :goto_9

    :cond_10
    const/16 v3, 0x8

    :goto_9
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$Model;->getGameInviteModel()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    move-result-object p1

    if-eqz p1, :cond_11

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getActivityActionPreview()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;->bind(Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/utilities/time/Clock;)V

    :cond_11
    return-void

    :cond_12
    const-string p1, "resultsAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final finish()V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method private final getActivityActionPreview()Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->activityActionPreview$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite;

    return-object v0
.end method

.method private final getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->commentInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getDimmer()Lcom/discord/utilities/dimmer/DimmerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    return-object v0
.end method

.method private final getPreviewList()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->previewList$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getPreviewListWrap()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->previewListWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getResultsFlipper()Landroid/widget/ViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->resultsFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method private final getScrollView()Landroidx/core/widget/NestedScrollView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/core/widget/NestedScrollView;

    return-object v0
.end method

.method private final getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchEt$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getSearchFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getSearchResultsRv()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchResultsRv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getSelectedReceiver()Lcom/discord/widgets/user/search/ViewGlobalSearchItem;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiver$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/user/search/ViewGlobalSearchItem;

    return-object v0
.end method

.method private final getSelectedReceiverRemoveIv()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiverRemoveIv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/share/WidgetIncomingShare;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final initialize(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V
    .locals 6

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getPreselectedRecipientChannel()Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getPreselectedRecipientChannel()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/discord/utilities/channel/ChannelSelector;->findAndSet(J)V

    const-string v2, "context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v2

    const v3, 0x10008001

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lf/a/b/m;->c(Landroid/content/Context;ZLandroid/content/Intent;I)V

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->finish()V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getRecipient()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setSelectionEnd(Lcom/google/android/material/textfield/TextInputLayout;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getSharedText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->contentPublisher:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getUris()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    add-int/lit8 v2, v1, 0x1

    const/4 v3, 0x0

    if-ltz v1, :cond_2

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v3

    :goto_1
    const/4 v5, 0x4

    invoke-static {v4, v0, v3, v5, v3}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->getMimeType$default(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "share"

    invoke-static {v3, v0, v1}, Lcom/discord/utilities/analytics/AnalyticsTracker;->addAttachment(Ljava/lang/String;Ljava/lang/String;I)V

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    throw v3

    :cond_3
    return-void
.end method

.method private final onSendClicked(Landroid/content/Context;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;ZIZ)V
    .locals 20

    move-object/from16 v7, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p5, :cond_0

    const v2, 0x7f12046e

    const/16 v4, 0xc

    invoke-static {v3, v2, v1, v0, v4}, Lf/a/b/p;->i(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;I)V

    return-void

    :cond_0
    instance-of v2, v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v2

    move-object v5, v4

    check-cast v5, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;

    invoke-virtual {v5}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/discord/utilities/channel/ChannelSelector;->selectChannel(Lcom/discord/models/domain/ModelChannel;)V

    new-instance v8, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$filter$1;

    invoke-direct {v8, v4}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$filter$1;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;)V

    invoke-virtual {v5}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemChannel;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v9

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xc

    invoke-static/range {v8 .. v13}, Lf/a/b/r;->d(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;I)Lrx/Observable$c;

    move-result-object v2

    goto :goto_0

    :cond_1
    instance-of v2, v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/discord/utilities/channel/ChannelSelector;->Companion:Lcom/discord/utilities/channel/ChannelSelector$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/channel/ChannelSelector$Companion;->getInstance()Lcom/discord/utilities/channel/ChannelSelector;

    move-result-object v2

    move-object v5, v4

    check-cast v5, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;

    invoke-virtual {v5}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemUser;->getUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v5

    invoke-virtual {v2, v3, v5, v6}, Lcom/discord/utilities/channel/ChannelSelector;->findAndSetDirectMessage(Landroid/content/Context;J)V

    new-instance v8, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$filter$2;

    invoke-direct {v8, v4}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$filter$2;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;)V

    invoke-interface/range {p2 .. p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v9

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xc

    invoke-static/range {v8 .. v13}, Lf/a/b/r;->d(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;I)Lrx/Observable$c;

    move-result-object v2

    goto :goto_0

    :cond_2
    instance-of v2, v4, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;

    if-eqz v2, :cond_f

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v2

    move-object v5, v4

    check-cast v5, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;

    invoke-virtual {v5}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemGuild;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v5

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Lcom/discord/stores/StoreGuildSelected;->set(J)V

    new-instance v8, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$filter$3;

    invoke-direct {v8, v4}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$filter$3;-><init>(Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;)V

    invoke-interface/range {p2 .. p2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v9

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    const/16 v13, 0xc

    invoke-static/range {v8 .. v13}, Lf/a/b/r;->d(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;I)Lrx/Observable$c;

    move-result-object v2

    :goto_0
    invoke-virtual/range {p4 .. p4}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->getUris()Ljava/util/List;

    move-result-object v5

    const-string v6, "context.contentResolver"

    const/16 v8, 0xa

    if-eqz v5, :cond_4

    new-instance v9, Ljava/util/ArrayList;

    invoke-static {v5, v8}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/Uri;

    sget-object v11, Lcom/lytefast/flexinput/model/Attachment;->Companion:Lcom/lytefast/flexinput/model/Attachment$Companion;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-static {v12, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11, v10, v12}, Lcom/lytefast/flexinput/model/Attachment$Companion;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Lcom/lytefast/flexinput/model/Attachment;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v5, v9

    goto :goto_2

    :cond_4
    sget-object v5, Lx/h/l;->d:Lx/h/l;

    :goto_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-static {v5, v8}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v9, v8}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/lytefast/flexinput/model/Attachment;

    invoke-virtual {v10}, Lcom/lytefast/flexinput/model/Attachment;->getUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    invoke-static {v11, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10, v11}, Lcom/discord/utilities/rest/SendUtilsKt;->computeFileSizeMegabytes(Landroid/net/Uri;Landroid/content/ContentResolver;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    invoke-static {v9}, Lx/h/f;->maxOrNull(Ljava/lang/Iterable;)Ljava/lang/Float;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    move v13, v6

    goto :goto_4

    :cond_6
    const/4 v6, 0x0

    const/4 v13, 0x0

    :goto_4
    invoke-static {v9}, Lx/h/f;->sumOfFloat(Ljava/lang/Iterable;)F

    move-result v11

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    const/4 v15, 0x1

    if-eqz v6, :cond_8

    :cond_7
    const/4 v6, 0x0

    goto :goto_5

    :cond_8
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/lytefast/flexinput/model/Attachment;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isImage(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z

    move-result v8

    if-eqz v8, :cond_9

    const/4 v6, 0x1

    :goto_5
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_b

    :cond_a
    const/16 v19, 0x0

    goto :goto_6

    :cond_b
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/lytefast/flexinput/model/Attachment;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/discord/utilities/attachments/AttachmentUtilsKt;->isVideo(Lcom/lytefast/flexinput/model/Attachment;Landroid/content/ContentResolver;)Z

    move-result v9

    if-eqz v9, :cond_c

    const/16 v19, 0x1

    :goto_6
    sget-object v8, Lcom/discord/utilities/rest/SendUtils;->INSTANCE:Lcom/discord/utilities/rest/SendUtils;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v10

    const-string v1, "parentFragmentManager"

    invoke-static {v10, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v18, 0x0

    move-object/from16 v9, p1

    move/from16 v12, p6

    move/from16 v14, p7

    const/4 v1, 0x1

    move-object v15, v5

    move/from16 v16, v6

    move/from16 v17, v19

    invoke-virtual/range {v8 .. v18}, Lcom/discord/utilities/rest/SendUtils;->tryShowFilesTooLargeDialog(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;FIFZLjava/util/List;ZZLkotlin/jvm/functions/Function0;)Z

    move-result v8

    if-eqz v8, :cond_d

    return-void

    :cond_d
    sget-object v8, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v8}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v9

    invoke-virtual {v9}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v9

    invoke-virtual {v8}, Lcom/discord/stores/StoreStream$Companion;->getChannelsSelected()Lcom/discord/stores/StoreChannelsSelected;

    move-result-object v8

    invoke-virtual {v8}, Lcom/discord/stores/StoreChannelsSelected;->observeSelectedChannel()Lrx/Observable;

    move-result-object v8

    invoke-virtual {v8, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v10

    const-string v2, "StoreStream\n            \u2026         .compose(filter)"

    invoke-static {v10, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v11, 0x3e8

    const/4 v13, 0x0

    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    sget-object v8, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$1;->INSTANCE:Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$1;

    invoke-static {v9, v2, v8}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v2, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;

    move-object/from16 v8, p3

    invoke-direct {v2, v7, v5, v8}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Ljava/util/List;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V

    invoke-virtual {v1, v2}, Lrx/Observable;->w(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const-string v2, "Observable\n        .comb\u2026ervable.empty()\n        }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v7, Lcom/discord/widgets/share/WidgetIncomingShare;->resultsAdapter:Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

    if-eqz v2, :cond_e

    invoke-static {v1, v7, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object v1

    const-wide/16 v8, 0x0

    new-instance v2, Lf/a/b/j0;

    invoke-direct {v2, v1, v8, v9}, Lf/a/b/j0;-><init>(Lcom/discord/utilities/dimmer/DimmerView;J)V

    invoke-virtual {v0, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v10

    const-string v0, "Observable\n        .comb\u2026rs.withDimmer(dimmer, 0))"

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v11, Lcom/discord/widgets/share/WidgetIncomingShare;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    new-instance v16, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$3;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object v2, v5

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move v5, v6

    move/from16 v6, v19

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$3;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Ljava/util/List;Landroid/content/Context;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;ZZ)V

    const/16 v17, 0x1e

    const/16 v18, 0x0

    invoke-static/range {v10 .. v18}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_e
    const-string v1, "resultsAdapter"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_f
    return-void
.end method

.method private final onSendCompleted()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "this.context ?: return"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.discord.intent.action.SDK"

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "com.discord.intent.extra.EXTRA_CONTINUE_IN_APP"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const v3, 0x10008000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lf/a/b/m;->c(Landroid/content/Context;ZLandroid/content/Intent;I)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->finish()V

    :cond_1
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d022f

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 18

    move-object/from16 v6, p0

    const-string v0, "view"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    if-nez v7, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->finish()V

    return-void

    :cond_0
    new-instance v8, Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x3f

    const/16 v17, 0x0

    move-object v9, v2

    invoke-direct/range {v9 .. v17}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;-><init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v8

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v8, v6, Lcom/discord/widgets/share/WidgetIncomingShare;->previewAdapter:Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;

    const/4 v0, 0x0

    if-eqz v8, :cond_1

    invoke-direct {v6, v8}, Lcom/discord/widgets/share/WidgetIncomingShare;->configureAdapter(Lcom/discord/widgets/share/WidgetIncomingShare$Adapter;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getCommentInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$1;

    invoke-direct {v2, v6}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$1;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;)V

    invoke-static {v1, v6, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    new-instance v1, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchResultsRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object v2, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    invoke-virtual {v2, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v1

    check-cast v1, Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

    iput-object v1, v6, Lcom/discord/widgets/share/WidgetIncomingShare;->resultsAdapter:Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchResultsRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroidx/core/view/ViewCompat;->setNestedScrollingEnabled(Landroid/view/View;Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchResultsRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$2;

    invoke-direct {v2, v6}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$2;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;)V

    invoke-static {v1, v6, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchEt()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$3;

    invoke-direct {v2, v6}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$3;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;)V

    invoke-static {v1, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditTextFocusChangeListener(Lcom/google/android/material/textfield/TextInputLayout;Landroid/view/View$OnFocusChangeListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSelectedReceiverRemoveIv()Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$4;

    invoke-direct {v2, v6}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$4;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v6, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiverPublisher:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const v1, 0x7f010002

    invoke-static {v7, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/share/WidgetIncomingShare;->getSearchFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const v1, 0x7f010005

    invoke-static {v7, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$5;

    invoke-direct {v0, v6}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBound$5;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;)V

    invoke-virtual {v6, v0}, Lcom/discord/app/AppFragment;->setOnNewIntentListener(Lkotlin/jvm/functions/Function1;)V

    sget-object v0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->Companion:Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;->get(Landroid/content/Intent;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/discord/widgets/share/WidgetIncomingShare;->initialize(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;)V

    return-void

    :cond_1
    const-string v1, "previewAdapter"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->searchQueryPublisher:Lrx/subjects/BehaviorSubject;

    const-string v1, "searchQueryPublisher"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/discord/stores/StoreGuilds$Actions;->requestMembers(Lcom/discord/app/AppComponent;Lrx/Observable;Z)V

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->selectedReceiverPublisher:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;)V

    invoke-virtual {v1, v2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const-string v2, "selectedReceiverPublishe\u2026  )\n          }\n        }"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/share/WidgetIncomingShare;->resultsAdapter:Lcom/discord/widgets/user/search/WidgetGlobalSearchAdapter;

    if-eqz v2, :cond_0

    invoke-static {v1, p0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/share/WidgetIncomingShare;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$2;

    invoke-direct {v9, p0, v0}, Lcom/discord/widgets/share/WidgetIncomingShare$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare;Lcom/discord/utilities/time/Clock;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "resultsAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
