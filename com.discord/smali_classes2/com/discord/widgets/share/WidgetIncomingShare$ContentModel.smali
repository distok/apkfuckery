.class public final Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;
.super Ljava/lang/Object;
.source "WidgetIncomingShare.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/share/WidgetIncomingShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContentModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;


# instance fields
.field private final activity:Lcom/discord/models/domain/activity/ModelActivity;

.field private final activityActionUri:Landroid/net/Uri;

.field private final preselectedRecipientChannel:Ljava/lang/Long;

.field private final recipient:Ljava/lang/String;

.field private final sharedText:Ljava/lang/CharSequence;

.field private final uris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->Companion:Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3f

    const/4 v8, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;-><init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "+",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    iput-object p4, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    const-string p1, ""

    :cond_0
    and-int/lit8 p8, p7, 0x2

    const/4 v0, 0x0

    if-eqz p8, :cond_1

    move-object p8, v0

    goto :goto_0

    :cond_1
    move-object p8, p2

    :goto_0
    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v1, p3

    :goto_1
    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    move-object v2, v0

    goto :goto_2

    :cond_3
    move-object v2, p4

    :goto_2
    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    move-object v3, v0

    goto :goto_3

    :cond_4
    move-object v3, p5

    :goto_3
    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    goto :goto_4

    :cond_5
    move-object v0, p6

    :goto_4
    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v1

    move-object p6, v2

    move-object p7, v3

    move-object p8, v0

    invoke-direct/range {p2 .. p8}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;-><init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;ILjava/lang/Object;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->copy(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final component6()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "+",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/discord/models/domain/activity/ModelActivity;",
            ")",
            "Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;"
        }
    .end annotation

    new-instance v7, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;-><init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)V

    return-object v7
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    iget-object v1, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    iget-object v1, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    iget-object p1, p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActivity()Lcom/discord/models/domain/activity/ModelActivity;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    return-object v0
.end method

.method public final getActivityActionUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getPreselectedRecipientChannel()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    return-object v0
.end method

.method public final getRecipient()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    return-object v0
.end method

.method public final getSharedText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getUris()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/discord/models/domain/activity/ModelActivity;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ContentModel(sharedText="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->sharedText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", uris="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->uris:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preselectedRecipientChannel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->preselectedRecipientChannel:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->recipient:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", activityActionUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activityActionUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", activity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;->activity:Lcom/discord/models/domain/activity/ModelActivity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
