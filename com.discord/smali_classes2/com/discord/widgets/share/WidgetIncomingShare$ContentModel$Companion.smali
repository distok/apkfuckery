.class public final Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;
.super Ljava/lang/Object;
.source "WidgetIncomingShare.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Landroid/content/Intent;)Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;
    .locals 9

    const-string v0, "recentIntent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/ShareUtils;->INSTANCE:Lcom/discord/utilities/ShareUtils;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/discord/utilities/ShareUtils;->getSharedContent(Landroid/content/Intent;Z)Lcom/discord/utilities/ShareUtils$SharedContent;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    invoke-virtual {v1, p1}, Lcom/discord/utilities/intent/IntentUtils;->getDirectShareId(Landroid/content/Intent;)Ljava/lang/Long;

    move-result-object v5

    const-string v1, "EXTRA_RECIPIENT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x41c4677b

    if-eq v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const-string v3, "com.discord.intent.action.SDK"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    move-object v7, v1

    goto :goto_1

    :cond_2
    :goto_0
    move-object v7, v2

    :goto_1
    const-string v1, "com.discord.intent.extra.EXTRA_ACTIVITY"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_3

    new-instance v1, Lcom/discord/models/domain/Model$JsonReader;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/discord/models/domain/Model$JsonReader;-><init>(Ljava/io/Reader;)V

    new-instance p1, Lcom/discord/models/domain/activity/ModelActivity;

    invoke-direct {p1}, Lcom/discord/models/domain/activity/ModelActivity;-><init>()V

    invoke-virtual {v1, p1}, Lcom/discord/models/domain/Model$JsonReader;->parse(Lcom/discord/models/domain/Model;)Lcom/discord/models/domain/Model;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/activity/ModelActivity;

    move-object v8, p1

    goto :goto_2

    :cond_3
    move-object v8, v2

    :goto_2
    new-instance p1, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;

    invoke-virtual {v0}, Lcom/discord/utilities/ShareUtils$SharedContent;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/utilities/ShareUtils$SharedContent;->getUris()Ljava/util/List;

    move-result-object v4

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;-><init>(Ljava/lang/CharSequence;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Landroid/net/Uri;Lcom/discord/models/domain/activity/ModelActivity;)V

    return-object p1
.end method
