.class public final Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;
.super Ljava/lang/Object;
.source "WidgetIncomingShare.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/share/WidgetIncomingShare;->onSendClicked(Landroid/content/Context;Lcom/discord/widgets/user/search/WidgetGlobalSearchModel$ItemDataPayload;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;Lcom/discord/widgets/share/WidgetIncomingShare$ContentModel;ZIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        "+",
        "Lcom/discord/models/domain/ModelChannel;",
        ">;",
        "Lrx/Observable<",
        "+",
        "Lkotlin/Pair<",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        "+",
        "Lcom/discord/utilities/messagesend/MessageResult;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic $data:Ljava/util/List;

.field public final synthetic $gameInviteModel:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

.field public final synthetic this$0:Lcom/discord/widgets/share/WidgetIncomingShare;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/share/WidgetIncomingShare;Ljava/util/List;Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->this$0:Lcom/discord/widgets/share/WidgetIncomingShare;

    iput-object p2, p0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->$data:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->$gameInviteModel:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->call(Lkotlin/Pair;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lkotlin/Pair;)Lrx/Observable;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            "+",
            "Lcom/discord/models/domain/ModelChannel;",
            ">;)",
            "Lrx/Observable<",
            "+",
            "Lkotlin/Pair<",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/utilities/messagesend/MessageResult;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual/range {p1 .. p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelChannel;

    if-eqz v2, :cond_3

    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getMessages()Lcom/discord/stores/StoreMessages;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v4

    const-string v2, "meUser"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->this$0:Lcom/discord/widgets/share/WidgetIncomingShare;

    invoke-static {v2}, Lcom/discord/widgets/share/WidgetIncomingShare;->access$getCommentInput$p(Lcom/discord/widgets/share/WidgetIncomingShare;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v2

    invoke-static {v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v8, v0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->$data:Ljava/util/List;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-object v2, v0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->$gameInviteModel:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    const/4 v12, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getApplication()Lcom/discord/models/domain/ModelApplication;

    move-result-object v2

    move-object v13, v2

    goto :goto_0

    :cond_0
    move-object v13, v12

    :goto_0
    iget-object v2, v0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->$gameInviteModel:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getActivity()Lcom/discord/models/domain/activity/ModelActivity;

    move-result-object v2

    move-object v14, v2

    goto :goto_1

    :cond_1
    move-object v14, v12

    :goto_1
    iget-object v2, v0, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;->$gameInviteModel:Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/discord/widgets/chat/list/ViewEmbedGameInvite$Model;->getMessageActivity()Lcom/discord/models/domain/ModelMessage$Activity;

    move-result-object v2

    move-object/from16 v20, v2

    goto :goto_2

    :cond_2
    move-object/from16 v20, v12

    :goto_2
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x3880

    const/16 v19, 0x0

    move-object v2, v3

    move-wide v3, v4

    move-object v5, v1

    move-object v12, v13

    move-object v13, v14

    move-object/from16 v14, v20

    invoke-static/range {v2 .. v19}, Lcom/discord/stores/StoreMessages;->sendMessage$default(Lcom/discord/stores/StoreMessages;JLcom/discord/models/domain/ModelUser;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/discord/models/domain/ModelMessage$MessageReference;Lcom/discord/models/domain/ModelAllowedMentions;Lcom/discord/models/domain/ModelApplication;Lcom/discord/models/domain/activity/ModelActivity;Lcom/discord/models/domain/ModelMessage$Activity;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2$$special$$inlined$let$lambda$1;

    invoke-direct {v3, v0, v1}, Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2$$special$$inlined$let$lambda$1;-><init>(Lcom/discord/widgets/share/WidgetIncomingShare$onSendClicked$2;Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v2, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    goto :goto_3

    :cond_3
    sget-object v1, Lg0/l/a/f;->e:Lrx/Observable;

    :goto_3
    return-object v1
.end method
