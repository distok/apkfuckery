.class public final Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;
.super Ljava/lang/Object;
.source "WidgetGlobalStatusIndicator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->setupContainerClicks(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

.field public final synthetic this$0:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->this$0:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    iput-object p2, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v1

    const-string v2, "requireContext()"

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/utilities/streams/StreamContext;->isCurrentUserParticipating()Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    sget-object v4, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->this$0:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplicationStream;->getChannelId()J

    move-result-wide v6

    const/4 v8, 0x0

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/streams/StreamContext;->getStream()Lcom/discord/models/domain/ModelApplicationStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelApplicationStream;->getEncodedStreamKey()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getSelectedVoiceChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v3, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen;->Companion:Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->this$0:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getSelectedVoiceChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;->launch$default(Lcom/discord/widgets/voice/fullscreen/WidgetCallFullscreen$Companion;Landroid/content/Context;JZLjava/lang/String;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object v11, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;->Companion:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->this$0:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v12

    const-string v1, "parentFragmentManager"

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;->$viewState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getSelectedVoiceChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v13

    const/4 v15, 0x1

    sget-object v16, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;->HOME:Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;

    invoke-virtual/range {v11 .. v16}, Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;JZLcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet$FeatureContext;)Lcom/discord/widgets/voice/sheet/WidgetVoiceBottomSheet;

    :goto_0
    return-void
.end method
