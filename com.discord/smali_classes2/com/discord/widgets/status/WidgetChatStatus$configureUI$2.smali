.class public final Lcom/discord/widgets/status/WidgetChatStatus$configureUI$2;
.super Ljava/lang/Object;
.source "WidgetChatStatus.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/status/WidgetChatStatus;->configureUI(Lcom/discord/widgets/status/WidgetChatStatus$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/status/WidgetChatStatus$Model;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/status/WidgetChatStatus$Model;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/status/WidgetChatStatus$configureUI$2;->$data:Lcom/discord/widgets/status/WidgetChatStatus$Model;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetChatStatus$configureUI$2;->$data:Lcom/discord/widgets/status/WidgetChatStatus$Model;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetChatStatus$Model;->getUnreadChannelId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreReadStates;->markAsRead(Ljava/lang/Long;)V

    return-void
.end method
