.class public final Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;
.super Ljava/lang/Object;
.source "WidgetGlobalStatusIndicatorState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "State"
.end annotation


# instance fields
.field private final isCustomBackground:Z

.field private final isVisible:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    iput-boolean p2, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;ZZILjava/lang/Object;)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->copy(ZZ)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    return v0
.end method

.method public final copy(ZZ)Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;
    .locals 1

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;-><init>(ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    iget-boolean v1, p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    iget-boolean p1, p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final isCustomBackground()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "State(isVisible="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isCustomBackground="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isCustomBackground:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
