.class public final Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;
.super Lcom/discord/app/AppFragment;
.source "WidgetGlobalStatusIndicator.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

.field private final connectingVectorReplayCallback:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;

.field private final indicatorContent$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private indicatorContentAnimating:Z

.field private indicatorContentDelayRunnable:Ljava/lang/Runnable;

.field private final indicatorIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final indicatorRoot$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final indicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

.field private final indicatorStatus$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    const-string v3, "indicatorRoot"

    const-string v4, "getIndicatorRoot()Landroid/view/ViewGroup;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    const-string v6, "indicatorContent"

    const-string v7, "getIndicatorContent()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    const-string v6, "indicatorStatus"

    const-string v7, "getIndicatorStatus()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    const-string v6, "indicatorIcon"

    const-string v7, "getIndicatorIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a04b0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorRoot$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04b1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04b3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorStatus$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04b2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->Provider:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;->get()Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;-><init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)V

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVectorReplayCallback:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->configureUI(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$configureUIVisibility(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->configureUIVisibility(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V

    return-void
.end method

.method public static final synthetic access$getConnectingVector$p(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    return-object p0
.end method

.method public static final synthetic access$getIndicatorContent$p(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$pulseFadeIndicatorContent(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->pulseFadeIndicatorContent()V

    return-void
.end method

.method public static final synthetic access$setConnectingVector$p(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    return-void
.end method

.method private final bindDelay(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$bindDelay$1;

    invoke-direct {v0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$bindDelay$1;-><init>(Lrx/Observable;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private final configureUI(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState;)V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    sget-object v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Inactive;->INSTANCE:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Inactive;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    instance-of v2, p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->updateState(ZZ)V

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVectorReplayCallback:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;

    invoke-virtual {v0, v1}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->unregisterAnimationCallback(Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;)Z

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->stop()V

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Offline;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Offline;

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Offline;->getAirplaneMode()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->setupOfflineState(Z)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$Connecting;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->setupConnectingState()V

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_4

    check-cast p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;

    invoke-direct {p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->setupContainerClicks(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->setupIndicatorStatus(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->resetContentAnimationsAndVisibility()V

    :goto_0
    return-void
.end method

.method private final configureUIVisibility(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;)V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorRoot()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;->isVisible()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final getIndicatorContent()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContent$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getIndicatorIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getIndicatorRoot()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorRoot$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getIndicatorStatus()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorStatus$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final pulseFadeIndicatorContent()V
    .locals 13

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$pulseFadeIndicatorContent$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$pulseFadeIndicatorContent$1;-><init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)V

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContentDelayRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContentDelayRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$pulseFadeIndicatorContent$2;

    invoke-direct {v10, p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$pulseFadeIndicatorContent$2;-><init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)V

    const/4 v11, 0x6

    const/4 v12, 0x0

    invoke-static/range {v5 .. v12}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeIn$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    :goto_1
    iput-boolean v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContentAnimating:Z

    return-void
.end method

.method private final resetContentAnimationsAndVisibility()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->cancelFadeAnimations(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContentDelayRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorContent()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContentAnimating:Z

    return-void
.end method

.method private final setupConnectingState()V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->resetContentAnimationsAndVisibility()V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorRoot()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04013d

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorStatus()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040153

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorStatus()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f12050f

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "requireContext()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f0402f5

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v2, v3, v4, v5, v1}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v2

    invoke-static {v0, v2}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->create(Landroid/content/Context;I)Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorIcon()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVector:Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->connectingVectorReplayCallback:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$connectingVectorReplayCallback$1;

    invoke-virtual {v2, v1}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->registerAnimationCallback(Landroidx/vectordrawable/graphics/drawable/Animatable2Compat$AnimationCallback;)V

    invoke-virtual {v2}, Landroidx/vectordrawable/graphics/drawable/AnimatedVectorDrawableCompat;->start()V

    move-object v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private final setupContainerClicks(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorRoot()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$setupContainerClicks$1;-><init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setupIndicatorStatus(Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;)V
    .locals 13

    iget-boolean v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorContentAnimating:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->pulseFadeIndicatorContent()V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorRoot()Landroid/view/ViewGroup;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/voice/VoiceViewUtils;->INSTANCE:Lcom/discord/utilities/voice/VoiceViewUtils;

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getConnectionState()Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getConnectionQuality()Lcom/discord/rtcconnection/RtcConnection$Quality;

    move-result-object v3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "requireContext()"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3, v4}, Lcom/discord/utilities/voice/VoiceViewUtils;->getConnectionStatusColor(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Quality;Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    sget-object v0, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getSelectedVoiceChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getSelectedVoiceChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelChannel;->getType()I

    move-result v9

    const/4 v10, 0x0

    const/16 v11, 0x8

    const/4 v12, 0x0

    invoke-static/range {v6 .. v12}, Lcom/discord/utilities/channel/ChannelUtils;->getDisplayName$default(Lcom/discord/utilities/channel/ChannelUtils;Landroid/content/Context;Ljava/lang/String;IZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getConnectionState()Lcom/discord/rtcconnection/RtcConnection$State;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getHasVideo()Z

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/discord/utilities/voice/VoiceViewUtils;->getConnectedText(Landroid/content/Context;Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/utilities/streams/StreamContext;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v4, " / "

    invoke-static {v3, v4, v0}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const-string v0, ": "

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorStatus()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorStatus()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060292

    invoke-static {v2, v3}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getHasVideo()Z

    move-result v2

    invoke-virtual {p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$ViewState$CallOngoing;->getStreamContext()Lcom/discord/utilities/streams/StreamContext;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/discord/utilities/voice/VoiceViewUtils;->getCallIndicatorIcon(ZLcom/discord/utilities/streams/StreamContext;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private final setupOfflineState(Z)V
    .locals 5

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->resetContentAnimationsAndVisibility()V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorRoot()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04013d

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorStatus()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040153

    invoke-static {v1, v2}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorStatus()Landroid/widget/TextView;

    move-result-object v0

    if-eqz p1, :cond_0

    const v1, 0x7f1210cc

    goto :goto_0

    :cond_0
    const v1, 0x7f1210cb

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->getIndicatorIcon()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v4, "requireContext()"

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f0402f4

    invoke-static {p1, v4, v3, v2, v1}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f0402f6

    invoke-static {p1, v4, v3, v2, v1}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result p1

    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0201

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory;

    invoke-direct {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026torViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;

    iput-object p1, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->viewModel:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;

    return-void
.end method

.method public onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    invoke-direct {p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->resetContentAnimationsAndVisibility()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$onViewBound$1;->INSTANCE:Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$onViewBound$1;

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setOnApplyWindowInsetsListener(Landroid/view/View;Landroidx/core/view/OnApplyWindowInsetsListener;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->indicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    invoke-virtual {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->observeState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    new-instance v7, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->viewModel:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;->bindDelay(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const-string v2, "viewModel\n        .obser\u2026te()\n        .bindDelay()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$onViewBoundOrOnResume$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicator$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/status/WidgetGlobalStatusIndicator;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method
