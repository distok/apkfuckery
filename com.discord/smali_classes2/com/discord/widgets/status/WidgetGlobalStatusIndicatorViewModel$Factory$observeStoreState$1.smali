.class public final Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;
.super Ljava/lang/Object;
.source "WidgetGlobalStatusIndicatorViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory;->observeStoreState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;

    invoke-direct {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1;->call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelChannel;)Lrx/Observable;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelChannel;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$StoreState;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    if-nez v0, :cond_1

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getConnectivity()Lcom/discord/stores/StoreConnectivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreConnectivity;->observeState()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1$1;->INSTANCE:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1$1;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$sam$rx_functions_Func1$0;

    invoke-direct {v2, v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lg0/k/b;

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreRtcConnection;->getConnectionState()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getRtcConnection()Lcom/discord/stores/StoreRtcConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/stores/StoreRtcConnection;->getQuality()Lrx/Observable;

    move-result-object v4

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannel;->getGuildId()Ljava/lang/Long;

    move-result-object v6

    const-string v7, "channel.guildId"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getVoiceParticipants()Lcom/discord/stores/StoreVoiceParticipants;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/discord/stores/StoreVoiceParticipants;->get(J)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/discord/utilities/streams/StreamContextService;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xff

    const/16 v16, 0x0

    move-object v6, v2

    invoke-direct/range {v6 .. v16}, Lcom/discord/utilities/streams/StreamContextService;-><init>(Lcom/discord/stores/StoreApplicationStreaming;Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StorePermissions;Lcom/discord/stores/StoreUser;Lcom/discord/stores/StoreVoiceChannelSelected;Lcom/discord/stores/StoreVoiceStates;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreApplicationStreamPreviews;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v2}, Lcom/discord/utilities/streams/StreamContextService;->getForActiveStream()Lrx/Observable;

    move-result-object v6

    sget-object v2, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1$2;->INSTANCE:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$Factory$observeStoreState$1$2;

    if-eqz v2, :cond_2

    new-instance v7, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$sam$rx_functions_Func6$0;

    invoke-direct {v7, v2}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorViewModel$sam$rx_functions_Func6$0;-><init>(Lkotlin/jvm/functions/Function6;)V

    move-object v2, v7

    :cond_2
    move-object v7, v2

    check-cast v7, Lrx/functions/Func6;

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v0

    invoke-static/range {v1 .. v7}, Lrx/Observable;->f(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func6;)Lrx/Observable;

    move-result-object v0

    :goto_0
    return-object v0
.end method
