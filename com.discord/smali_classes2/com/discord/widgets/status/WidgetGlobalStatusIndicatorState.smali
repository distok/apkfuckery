.class public final Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;
.super Ljava/lang/Object;
.source "WidgetGlobalStatusIndicatorState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;,
        Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;
    }
.end annotation


# static fields
.field public static final Provider:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

.field private static final callIndicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;


# instance fields
.field private final stateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->Provider:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$Provider;

    new-instance v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    invoke-direct {v0}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;-><init>()V

    sput-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->callIndicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->stateSubject:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method public static final synthetic access$getCallIndicatorState$cp()Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;
    .locals 1

    sget-object v0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->callIndicatorState:Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;

    return-object v0
.end method


# virtual methods
.method public final observeState()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->stateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "stateSubject.distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final updateState(ZZ)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState;->stateSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/status/WidgetGlobalStatusIndicatorState$State;-><init>(ZZ)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
