.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsPremiumGuildSubscription.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;

.field public static final VIEW_INDEX_FAILURE:I = 0x1

.field public static final VIEW_INDEX_LOADED:I = 0x2

.field public static final VIEW_INDEX_LOADING:I


# instance fields
.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildSubscriptionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noGuildsIv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noGuildsSubtitleTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noGuildsTitleTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final premiumGuildRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private premiumGuildSubscriptionsAdapter:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

.field private final retry$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private sampleGuildsAdapter:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;

.field private final sampleGuildsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subscriptionMarketingView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subtextContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subtextTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xb

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v3, "subtextContainer"

    const-string v4, "getSubtextContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "subtextTv"

    const-string v7, "getSubtextTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "retry"

    const-string v7, "getRetry()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "premiumGuildRecycler"

    const-string v7, "getPremiumGuildRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "sampleGuildsRecycler"

    const-string v7, "getSampleGuildsRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "flipper"

    const-string v7, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "guildSubscriptionUpsellView"

    const-string v7, "getGuildSubscriptionUpsellView()Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "subscriptionMarketingView"

    const-string v7, "getSubscriptionMarketingView()Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "noGuildsIv"

    const-string v7, "getNoGuildsIv()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "noGuildsTitleTv"

    const-string v7, "getNoGuildsTitleTv()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const-string v6, "noGuildsSubtitleTv"

    const-string v7, "getNoGuildsSubtitleTv()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->Companion:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0988

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->subtextContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0987

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->subtextTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0985

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0984

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->premiumGuildRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0986

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->sampleGuildsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a097f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0989

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->guildSubscriptionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0980

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->subscriptionMarketingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0981

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->noGuildsIv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0983

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->noGuildsTitleTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0982

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->noGuildsSubtitleTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleSampleGuildSelected(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->handleSampleGuildSelected(J)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    return-void
.end method

.method public static final synthetic access$showContent(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->showContent(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;)V

    return-void
.end method

.method public static final synthetic access$showFailureUI(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->showFailureUI()V

    return-void
.end method

.method public static final synthetic access$showLoadingUI(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->showLoadingUI()V

    return-void
.end method

.method private final configureNoGuildsViews(Z)V
    .locals 4

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getNoGuildsIv()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getNoGuildsTitleTv()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    :goto_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getNoGuildsSubtitleTv()Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getGuildSubscriptionUpsellView()Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->guildSubscriptionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;

    return-object v0
.end method

.method private final getNoGuildsIv()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->noGuildsIv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNoGuildsSubtitleTv()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->noGuildsSubtitleTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getNoGuildsTitleTv()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->noGuildsTitleTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPremiumGuildRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->premiumGuildRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getRetry()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->retry$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSampleGuildsRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->sampleGuildsRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getSubscriptionMarketingView()Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->subscriptionMarketingView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;

    return-object v0
.end method

.method private final getSubtextContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->subtextContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSubtextTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->subtextTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleSampleGuildSelected(J)V
    .locals 2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildSelected()Lcom/discord/stores/StoreGuildSelected;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreGuildSelected;->dispatchSampleGuildIdSelected(J)V

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string p2, "com.discord.intent.extra.EXTRA_OPEN_PANEL"

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const p2, 0x10008000

    invoke-virtual {p1, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "requireContext()"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p2, v0, p1, v1}, Lf/a/b/m;->c(Landroid/content/Context;ZLandroid/content/Intent;I)V

    return-void
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->Companion:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method

.method private final showContent(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;)V
    .locals 12

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPremiumGuildSubscriptionItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getSubtextContainer()Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->premiumGuildSubscriptionsAdapter:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getCanCancelBoosts()Z

    move-result v7

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getCanUncancelBoosts()Z

    move-result v8

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPremiumGuildSubscriptionItems()Ljava/util/List;

    move-result-object v3

    new-instance v5, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;

    invoke-direct {v5, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    new-instance v6, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$2;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$3;

    invoke-direct {v4, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$3;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    invoke-virtual/range {v2 .. v8}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->configure(Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;ZZ)V

    goto :goto_1

    :cond_1
    const-string p1, "premiumGuildSubscriptionsAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->sampleGuildsAdapter:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getSampleGuildItems()Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$4;

    invoke-direct {v3, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$4;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    invoke-virtual {v0, v2, v3}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;->configure(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPendingAction()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    move-result-object v0

    instance-of v2, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    const-string v3, "requireContext()"

    const-string v4, "viewModel"

    if-eqz v2, :cond_4

    check-cast v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->getTargetGuildId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    sget-object v5, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionConfirmation;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionConfirmation$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->getTargetGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->getSlotId()J

    move-result-wide v9

    invoke-virtual/range {v5 .. v10}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionConfirmation$Companion;->create(Landroid/content/Context;JJ)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->consumePendingAction()V

    goto/16 :goto_2

    :cond_3
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_4
    instance-of v2, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;

    if-eqz v2, :cond_6

    check-cast v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;->getTargetGuildId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    sget-object v5, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer;->Companion:Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;->getPreviousGuildId()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;->getTargetGuildId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;->getSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v11

    invoke-virtual/range {v5 .. v11}, Lcom/discord/widgets/servers/premiumguild/WidgetPremiumGuildSubscriptionTransfer$Companion;->create(Landroid/content/Context;JJLcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->consumePendingAction()V

    goto/16 :goto_2

    :cond_5
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_6
    instance-of v2, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Cancel;

    const-string v3, "extra_slot_id"

    const-string v5, "fragmentManager"

    const-string v6, "parentFragmentManager"

    if-eqz v2, :cond_8

    sget-object v2, Lf/a/a/b/a;->l:Lf/a/a/b/a$b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v7

    invoke-static {v7, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Cancel;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Cancel;->getSlotId()J

    move-result-wide v8

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/a/b/a;

    invoke-direct {v0}, Lf/a/a/b/a;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-class v2, Lf/a/a/b/a;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->consumePendingAction()V

    goto :goto_2

    :cond_7
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_8
    instance-of v2, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Uncancel;

    if-eqz v2, :cond_a

    sget-object v2, Lf/a/a/b/b;->m:Lf/a/a/b/b$b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v7

    invoke-static {v7, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Uncancel;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Uncancel;->getSlotId()J

    move-result-wide v8

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v7, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/a/b/b;

    invoke-direct {v0}, Lf/a/a/b/b;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, v3, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-class v2, Lf/a/a/b/b;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->consumePendingAction()V

    goto :goto_2

    :cond_9
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_a
    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getSubscriptionMarketingView()Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$7;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$7;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    invoke-virtual {v0, v1, v2}, Lcom/discord/views/premiumguild/PremiumSubscriptionMarketingView;->a(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getGuildSubscriptionUpsellView()Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getSampleGuildItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/views/premiumguild/PremiumGuildSubscriptionUpsellView;->a(Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;Z)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getSampleGuildItems()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->configureNoGuildsViews(Z)V

    return-void

    :cond_b
    const-string p1, "sampleGuildsAdapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method private final showFailureUI()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method

.method private final showLoadingUI()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0299

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    const/4 p1, -0x1

    if-ne p2, p1, :cond_2

    const/4 p1, 0x0

    if-eqz p3, :cond_0

    const-wide/16 v0, 0x0

    const-string p2, "EXTRA_GUILD_ID"

    invoke-virtual {p3, p2, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    const-string p3, "null cannot be cast to non-null type com.discord.models.domain.GuildId /* = kotlin.Long */"

    invoke-static {p2, p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p2, p3}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->handleGuildSearchCallback(J)V

    goto :goto_1

    :cond_1
    const-string p2, "viewModel"

    invoke-static {p2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 11

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const v3, 0x7f121971

    invoke-virtual {p0, v3}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const v3, 0x7f1213a3

    invoke-virtual {p0, v3}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    sget-object v3, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getPremiumGuildRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v3, v4}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v4

    check-cast v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    iput-object v4, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->premiumGuildSubscriptionsAdapter:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getSampleGuildsRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v3, v4}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v3

    check-cast v3, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;

    iput-object v3, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->sampleGuildsAdapter:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;

    sget-object v3, Lf/a/b/g;->a:Lf/a/b/g;

    const-wide v4, 0x53d357e4d0L

    invoke-virtual {v3, v4, v5, v2}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getSubtextTv()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string p1, "view.context"

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x7f1213a2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v0

    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string p1, "getString(\n            R\u2026pdeskArticleUrl\n        )"

    invoke-static {v5, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1c

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getSubtextTv()Landroid/widget/TextView;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBound$1;

    invoke-direct {v1, v2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBound$1;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getRetry()Landroid/view/View;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBound$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBound$2;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->getPremiumGuildRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 11

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory;

    invoke-direct {v2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory;-><init>()V

    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(\n     \u2026ildViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    iput-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->viewModel:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v1, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    new-instance v8, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-class v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
