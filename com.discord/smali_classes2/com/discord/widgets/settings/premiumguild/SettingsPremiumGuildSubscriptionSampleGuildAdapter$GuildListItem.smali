.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "SettingsPremiumGuildSubscriptionSampleGuildAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GuildListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;",
        "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final count$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final icon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final name$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final progress$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;

    const/4 v1, 0x4

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string v3, "icon"

    const-string v4, "getIcon()Lcom/facebook/drawee/view/SimpleDraweeView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/s;

    const-string v6, "name"

    const-string v7, "getName()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Lx/m/c/s;

    const-string v6, "count"

    const-string v7, "getCount()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x3

    new-instance v4, Lx/m/c/s;

    const-string v6, "progress"

    const-string v7, "getProgress()Lcom/discord/views/premiumguild/PremiumGuildProgressView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d015e

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    const p1, 0x7f0a0124

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->icon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0125

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->name$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0123

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->count$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a0126

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->progress$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;

    return-object p0
.end method

.method private final getCount()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->count$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getIcon()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->icon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->name$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getProgress()Lcom/discord/views/premiumguild/PremiumGuildProgressView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->progress$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premiumguild/PremiumGuildProgressView;

    return-object v0
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;)V
    .locals 8

    const-string v0, "data"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    invoke-virtual {p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_0
    const-string v1, "data.guild.premiumSubscriptionCount ?: 0"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->getIcon()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v1

    invoke-virtual {p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelGuild;ILcom/discord/utilities/images/MGImages$ChangeDetector;ZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->getName()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->getCount()Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v3, "itemView"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "itemView.context"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1000df

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, p1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->getProgress()Lcom/discord/views/premiumguild/PremiumGuildProgressView;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelGuild;->getPremiumTier()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/discord/views/premiumguild/PremiumGuildProgressView;->a(II)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem$onConfigure$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem$onConfigure$1;-><init>(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$GuildListItem;->onConfigure(ILcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;)V

    return-void
.end method
