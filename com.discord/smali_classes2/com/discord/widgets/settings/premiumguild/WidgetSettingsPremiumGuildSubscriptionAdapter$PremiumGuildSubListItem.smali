.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
.source "WidgetSettingsPremiumGuildSubscriptionAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PremiumGuildSubListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
        "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;",
        "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
        ">;"
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final action$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final cancelled$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final cooldown$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final date$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const-class v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    const/4 v1, 0x4

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lx/m/c/s;

    const-string v3, "date"

    const-string v4, "getDate()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v3, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    new-instance v4, Lx/m/c/s;

    const-string v6, "cooldown"

    const-string v7, "getCooldown()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    new-instance v4, Lx/m/c/s;

    const-string v6, "action"

    const-string v7, "getAction()Landroid/widget/TextView;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    const/4 v2, 0x3

    new-instance v4, Lx/m/c/s;

    const-string v6, "cancelled"

    const-string v7, "getCancelled()Landroid/view/View;"

    invoke-direct {v4, v0, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v4, v1, v2

    sput-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)V
    .locals 1

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f0d015b

    invoke-direct {p0, v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;-><init>(ILcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)V

    const p1, 0x7f0a011e

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->date$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a011d

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->cooldown$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a011b

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->action$delegate:Lkotlin/properties/ReadOnlyProperty;

    const p1, 0x7f0a011c

    invoke-static {p0, p1}, Ly/a/g0;->i(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->cancelled$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    return-object p0
.end method

.method public static final synthetic access$showPremiumGuildSubPopup(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;Landroid/view/View;ZZZLcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->showPremiumGuildSubPopup(Landroid/view/View;ZZZLcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V

    return-void
.end method

.method private final getAction()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->action$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCancelled()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->cancelled$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCooldown()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->cooldown$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDate()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->date$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final showPremiumGuildSubPopup(Landroid/view/View;ZZZLcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V
    .locals 5

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f13002d

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v1, Landroid/widget/PopupMenu;

    invoke-direct {v1, v0, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object p1

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v2, 0x7f0e0019

    invoke-virtual {p1, v2, v0}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object p1

    const v0, 0x7f0a06a4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v2, 0x7f0a06a3

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0a06a5

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {p5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCanceled()Z

    move-result v3

    const-string v4, "transfer"

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    xor-int/2addr p2, v4

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const-string p2, "cancel"

    invoke-static {v0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    if-nez v3, :cond_0

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const-string p3, "uncancel"

    invoke-static {v2, p3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v3, :cond_1

    if-eqz p4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    new-instance p2, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$showPremiumGuildSubPopup$1;

    invoke-direct {p2, p0, p5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$showPremiumGuildSubPopup$1;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V

    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    new-instance p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$showPremiumGuildSubPopup$2;

    invoke-direct {p1, p0, p5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$showPremiumGuildSubPopup$2;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    new-instance p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$showPremiumGuildSubPopup$3;

    invoke-direct {p1, p0, p5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$showPremiumGuildSubPopup$3;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V

    invoke-interface {v2, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method


# virtual methods
.method public onConfigure(ILcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;)V
    .locals 29

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "data"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->onConfigure(ILjava/lang/Object;)V

    instance-of v2, v1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    if-eqz v2, :cond_11

    iget-object v2, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v3, "itemView"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v3

    invoke-interface {v3}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v12

    move-object v3, v1

    check-cast v3, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    const/4 v15, 0x1

    const/4 v11, 0x0

    if-eqz v4, :cond_1

    const/4 v10, 0x1

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v3}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCooldownExpiresAtTimestamp()J

    move-result-wide v4

    cmp-long v6, v4, v12

    if-lez v6, :cond_2

    const/4 v9, 0x1

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    :goto_2
    invoke-virtual {v3}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v4

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getId()J

    move-result-wide v4

    goto :goto_3

    :cond_3
    const-wide/16 v4, 0x0

    :goto_3
    const/16 v6, 0x16

    ushr-long/2addr v4, v6

    const-wide v16, 0x14aa2cab000L

    add-long v4, v4, v16

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v6

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCanceled()Z

    move-result v16

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getCancelled()Landroid/view/View;

    move-result-object v6

    if-eqz v16, :cond_4

    const/4 v7, 0x0

    goto :goto_4

    :cond_4
    const/16 v7, 0x8

    :goto_4
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getDate()Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "MMMM dd, yyy"

    invoke-static {v7, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v8, ""

    const-string v7, "context"

    if-eqz v16, :cond_5

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionEndsAt()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    const v6, 0x7f121395

    new-array v4, v15, [Ljava/lang/Object;

    sget-object v19, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-static {v2, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1c

    const/16 v24, 0x0

    move-object v14, v4

    move-object/from16 v4, v19

    const v15, 0x7f121395

    move-object v6, v2

    move-object/from16 v25, v7

    move-object/from16 v7, v20

    move-object/from16 v26, v8

    move-object/from16 v8, v21

    move/from16 v27, v9

    move/from16 v9, v22

    move/from16 v28, v10

    move/from16 v10, v23

    const/4 v15, 0x0

    move-object/from16 v11, v24

    invoke-static/range {v4 .. v11}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v14, v15

    const v4, 0x7f121395

    invoke-virtual {v2, v4, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(\n     \u2026(it, context)\n          )"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x4

    const-string v6, "*"

    move-object/from16 v14, v26

    invoke-static {v4, v6, v14, v15, v5}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v4

    move-object v11, v4

    goto :goto_5

    :cond_5
    move-object/from16 v25, v7

    move-object v14, v8

    move/from16 v27, v9

    move/from16 v28, v10

    const/4 v15, 0x0

    const/4 v11, 0x0

    :goto_5
    const/4 v10, 0x2

    move/from16 v9, v27

    if-eqz v9, :cond_6

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCooldownExpiresAtTimestamp()J

    move-result-wide v3

    sub-long/2addr v3, v12

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    div-long v7, v3, v5

    mul-long v5, v5, v7

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x36ee80

    div-long v12, v3, v5

    mul-long v5, v5, v12

    sub-long/2addr v3, v5

    const-wide/32 v5, 0xea60

    div-long/2addr v3, v5

    move-object/from16 v6, v25

    invoke-static {v2, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v15

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    const/4 v12, 0x1

    aput-object v7, v5, v12

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v10

    const v3, 0x7f121311

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v3, "context.getString(\n     \u2026.toString()\n            )"

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v13, 0x0

    const/16 v17, 0x3c

    const/16 v18, 0x0

    move-object v4, v2

    move-object v12, v6

    move-object v6, v3

    move v3, v9

    move-object v9, v13

    const/4 v13, 0x2

    move/from16 v10, v17

    move-object v13, v11

    move-object/from16 v11, v18

    invoke-static/range {v4 .. v11}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_6

    :cond_6
    move v3, v9

    move-object v13, v11

    move-object/from16 v12, v25

    const/4 v4, 0x0

    :goto_6
    if-nez v13, :cond_7

    if-nez v4, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getCooldown()Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    :cond_7
    if-eqz v13, :cond_8

    if-eqz v4, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getCooldown()Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v4, " - "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_8
    if-eqz v13, :cond_9

    if-nez v4, :cond_9

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getCooldown()Landroid/widget/TextView;

    move-result-object v4

    invoke-static {v4, v13}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_9
    if-nez v13, :cond_a

    if-eqz v4, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getCooldown()Landroid/widget/TextView;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_a
    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getAction()Landroid/widget/TextView;

    move-result-object v4

    move/from16 v11, v28

    if-nez v11, :cond_c

    if-eqz v3, :cond_b

    goto :goto_8

    :cond_b
    const v5, 0x7f12139a

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_9

    :cond_c
    :goto_8
    move-object v8, v14

    :goto_9
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_f

    if-nez v16, :cond_d

    iget-object v4, v0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    invoke-static {v4}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->access$getCanCancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Z

    move-result v4

    if-nez v4, :cond_f

    :cond_d
    if-eqz v16, :cond_e

    iget-object v4, v0, Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;->adapter:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    check-cast v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    invoke-static {v4}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->access$getCanUncancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Z

    move-result v4

    if-eqz v4, :cond_e

    goto :goto_a

    :cond_e
    const/16 v19, 0x0

    goto :goto_b

    :cond_f
    :goto_a
    const/16 v19, 0x1

    :goto_b
    if-eqz v19, :cond_10

    invoke-static {v2, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f0402fb

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v2, v4, v15, v5, v6}, Lcom/discord/utilities/drawable/DrawableCompat;->getThemedDrawableRes$default(Landroid/content/Context;IIILjava/lang/Object;)I

    move-result v2

    goto :goto_c

    :cond_10
    const/4 v2, 0x0

    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getAction()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v15, v15, v2, v15}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->getAction()Landroid/widget/TextView;

    move-result-object v2

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;

    invoke-direct {v4, v0, v11, v3, v1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;ZZLcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_11
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Incorrect List Item Type or null data"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public bridge synthetic onConfigure(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->onConfigure(ILcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;)V

    return-void
.end method
