.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetSettingsPremiumGuildSubscriptionAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$GuildListItem;,
        Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$HeaderListItem;,
        Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;,
        Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
        ">;"
    }
.end annotation


# instance fields
.field private canCancelBoosts:Z

.field private canUncancelBoosts:Z

.field private cancelListener:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private subscribeListener:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private transferListener:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$subscribeListener$1;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$subscribeListener$1;

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->subscribeListener:Lkotlin/jvm/functions/Function1;

    sget-object p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$transferListener$1;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$transferListener$1;

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->transferListener:Lkotlin/jvm/functions/Function2;

    sget-object p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$cancelListener$1;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$cancelListener$1;

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->cancelListener:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static final synthetic access$getCanCancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->canCancelBoosts:Z

    return p0
.end method

.method public static final synthetic access$getCanUncancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->canUncancelBoosts:Z

    return p0
.end method

.method public static final synthetic access$getCancelListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Lkotlin/jvm/functions/Function2;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->cancelListener:Lkotlin/jvm/functions/Function2;

    return-object p0
.end method

.method public static final synthetic access$getSubscribeListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Lkotlin/jvm/functions/Function1;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->subscribeListener:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method

.method public static final synthetic access$getTransferListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Lkotlin/jvm/functions/Function2;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->transferListener:Lkotlin/jvm/functions/Function2;

    return-object p0
.end method

.method public static final synthetic access$setCanCancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->canCancelBoosts:Z

    return-void
.end method

.method public static final synthetic access$setCanUncancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->canUncancelBoosts:Z

    return-void
.end method

.method public static final synthetic access$setCancelListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->cancelListener:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static final synthetic access$setSubscribeListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->subscribeListener:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$setTransferListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->transferListener:Lkotlin/jvm/functions/Function2;

    return-void
.end method


# virtual methods
.method public final configure(Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Long;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionItems"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscribeListener"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferListener"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelListener"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    iput-object p2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->subscribeListener:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->transferListener:Lkotlin/jvm/functions/Function2;

    iput-object p4, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->cancelListener:Lkotlin/jvm/functions/Function2;

    iput-boolean p5, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->canCancelBoosts:Z

    iput-boolean p6, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->canUncancelBoosts:Z

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    const/4 p1, 0x1

    if-eq p2, p1, :cond_1

    const/4 p1, 0x2

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$HeaderListItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$HeaderListItem;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$GuildListItem;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$GuildListItem;-><init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)V

    :goto_0
    return-object p1
.end method
