.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;
.super Ljava/lang/Object;
.source "SettingsPremiumGuildSubscriptionSampleGuildAdapter.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item$Companion;

.field public static final TYPE_GUILD:I = 0x1


# instance fields
.field private final guild:Lcom/discord/models/domain/ModelGuild;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->Companion:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;)V
    .locals 1

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;Lcom/discord/models/domain/ModelGuild;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->copy(Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;
    .locals 1

    const-string v0, "guild"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;

    invoke-direct {v0, p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object p1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Item(guild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
