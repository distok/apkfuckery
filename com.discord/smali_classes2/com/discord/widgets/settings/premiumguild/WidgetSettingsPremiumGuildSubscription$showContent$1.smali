.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;
.super Lx/m/c/k;
.source "WidgetSettingsPremiumGuildSubscription.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->showContent(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;->invoke(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;J)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;J)V
    .locals 1

    const-string v0, "slot"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    invoke-static {v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->access$getViewModel$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->transferClicked(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;J)V

    sget-object p1, Lcom/discord/widgets/guilds/list/WidgetGuildSelector;->Companion:Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$showContent$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-static {p2}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/discord/widgets/guilds/list/WidgetGuildSelector$Companion;->createForResult(Landroidx/fragment/app/Fragment;Ljava/util/List;)V

    return-void
.end method
