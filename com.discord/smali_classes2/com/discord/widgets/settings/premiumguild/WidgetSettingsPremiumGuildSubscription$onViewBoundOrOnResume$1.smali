.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;
.super Lx/m/c/k;
.source "WidgetSettingsPremiumGuildSubscription.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;->invoke(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    invoke-static {v0, p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->access$showContent(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loading;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    invoke-static {p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->access$showLoadingUI(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Failure;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;

    invoke-static {p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;->access$showFailureUI(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscription;)V

    :cond_2
    :goto_0
    return-void
.end method
