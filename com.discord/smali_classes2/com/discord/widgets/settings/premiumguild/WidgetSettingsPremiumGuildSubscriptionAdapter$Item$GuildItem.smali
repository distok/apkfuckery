.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;
.super Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;
.source "WidgetSettingsPremiumGuildSubscriptionAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GuildItem"
.end annotation


# instance fields
.field private final guild:Lcom/discord/models/domain/ModelGuild;

.field private final subscriptionCount:I


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelGuild;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    iput p2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;Lcom/discord/models/domain/ModelGuild;IILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->copy(Lcom/discord/models/domain/ModelGuild;I)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    return v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelGuild;I)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;-><init>(Lcom/discord/models/domain/ModelGuild;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    iget p1, p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuild()Lcom/discord/models/domain/ModelGuild;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSubscriptionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    return v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "GuildItem(guild="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->guild:Lcom/discord/models/domain/ModelGuild;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;->subscriptionCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
