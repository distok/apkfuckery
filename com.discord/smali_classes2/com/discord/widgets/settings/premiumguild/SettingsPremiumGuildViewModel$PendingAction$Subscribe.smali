.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;
.super Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;
.source "SettingsPremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Subscribe"
.end annotation


# instance fields
.field private final slotId:J

.field private final targetGuildId:Ljava/lang/Long;


# direct methods
.method public constructor <init>(JLjava/lang/Long;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    iput-object p3, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;-><init>(JLjava/lang/Long;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;JLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-wide p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    iget-object p3, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->copy(JLjava/lang/Long;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    return-wide v0
.end method

.method public final component2()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    return-object v0
.end method

.method public final copy(JLjava/lang/Long;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;-><init>(JLjava/lang/Long;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    iget-wide v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    iget-wide v2, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSlotId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    return-wide v0
.end method

.method public final getTargetGuildId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Subscribe(slotId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->slotId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", targetGuildId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->targetGuildId:Ljava/lang/Long;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->y(Ljava/lang/StringBuilder;Ljava/lang/Long;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
