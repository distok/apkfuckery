.class public final synthetic Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;
.super Lx/m/c/i;
.source "SettingsPremiumGuildViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function4<",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
        "Ljava/util/Map<",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelGuild;",
        ">;",
        "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
        "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;

    invoke-direct {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    const/4 v1, 0x4

    const-string v3, "<init>"

    const-string v4, "<init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p4"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    check-cast p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    check-cast p3, Ljava/util/Map;

    check-cast p4, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$2;->invoke(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
