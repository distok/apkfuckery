.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;
.super Lf/a/b/l0;
.source "SettingsPremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;,
        Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;,
        Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;,
        Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory;,
        Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Companion;

.field private static final NUM_SAMPLE_GUILDS:I = 0x4

.field private static final UNUSED_PREMIUM_GUILD_SUBSCRIPTION_GUILD_ID:Ljava/lang/Long;


# instance fields
.field private final storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

.field private final storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->Companion:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreSubscriptions;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            "Lcom/discord/stores/StoreSubscriptions;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "storePremiumGuildSubscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeSubscriptions"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeObservable"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    iput-object p2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->fetchData()V

    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;

    new-instance v6, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$1;-><init>(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->handleStoreState(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;)V

    return-void
.end method

.method private final createPremiumGuildSubscriptionItems(Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscription;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscription;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getPremiumGuildSubscription()Lcom/discord/models/domain/ModelPremiumGuildSubscription;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelPremiumGuildSubscription;->getGuildId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :cond_0
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    move-object v5, v4

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->UNUSED_PREMIUM_GUILD_SUBSCRIPTION_GUILD_ID:Ljava/lang/Long;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_3
    move-object v5, v3

    :goto_3
    invoke-direct {v4, v2, v5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;-><init>(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_5

    new-instance p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$HeaderItem;

    const v2, 0x7f12135a

    invoke-direct {p1, v2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$HeaderItem;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/models/domain/ModelGuild;

    if-eqz v2, :cond_6

    if-eqz v4, :cond_6

    new-instance v2, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {v2, v4, v5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$GuildItem;-><init>(Lcom/discord/models/domain/ModelGuild;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    if-eqz p3, :cond_7

    invoke-virtual {p3}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_7
    move-object v5, v3

    :goto_5
    invoke-direct {v4, v2, v5}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;-><init>(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    return-object v0
.end method

.method private final fetchData()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription;->fetchUserGuildPremiumState()V

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;)V
    .locals 16
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->getPremiumGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->getSubscriptionState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v2

    instance-of v3, v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loading;

    if-nez v3, :cond_d

    instance-of v3, v2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loading;

    if-eqz v3, :cond_0

    goto/16 :goto_7

    :cond_0
    instance-of v3, v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    if-nez v3, :cond_c

    instance-of v3, v2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    if-eqz v3, :cond_1

    goto/16 :goto_6

    :cond_1
    instance-of v3, v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v3, :cond_b

    instance-of v3, v2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v3, :cond_b

    check-cast v2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {v2}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelSubscription;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v6

    if-ne v6, v5, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    if-eqz v6, :cond_4

    :cond_3
    const/4 v10, 0x0

    goto :goto_3

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v7

    goto :goto_1

    :cond_5
    const/4 v7, 0x0

    :goto_1
    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-ne v7, v8, :cond_8

    move-object v7, v1

    check-cast v7, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v7}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v7

    new-instance v8, Ljava/util/LinkedHashMap;

    invoke-direct {v8}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v10}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCanceled()Z

    move-result v10

    xor-int/2addr v10, v5

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v10, v9}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_7
    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v7

    const/4 v8, 0x2

    if-le v7, v8, :cond_3

    :cond_8
    const/4 v10, 0x1

    :goto_3
    xor-int/lit8 v11, v6, 0x1

    check-cast v1, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->getGuilds()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v2}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelSubscription;

    invoke-direct {v0, v1, v4, v2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->createPremiumGuildSubscriptionItems(Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscription;)Ljava/util/List;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->getGuilds()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lx/h/f;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v1

    new-instance v13, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v13, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelGuild;

    new-instance v4, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;

    invoke-direct {v4, v2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;-><init>(Lcom/discord/models/domain/ModelGuild;)V

    invoke-interface {v13, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    const/4 v14, 0x0

    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object v1

    if-eqz v1, :cond_a

    goto :goto_5

    :cond_a
    sget-object v1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_5
    move-object v15, v1

    new-instance v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-object v9, v1

    invoke-direct/range {v9 .. v15}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;-><init>(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    goto :goto_8

    :cond_b
    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Failure;

    goto :goto_8

    :cond_c
    :goto_6
    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Failure;

    goto :goto_8

    :cond_d
    :goto_7
    sget-object v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loading;

    :goto_8
    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final cancelClicked(JZ)V
    .locals 10
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v1, :cond_2

    if-eqz p3, :cond_1

    new-instance p3, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Cancel;

    invoke-direct {p3, p1, p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Cancel;-><init>(J)V

    goto :goto_0

    :cond_1
    new-instance p3, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Uncancel;

    invoke-direct {p3, p1, p2}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Uncancel;-><init>(J)V

    :goto_0
    move-object v6, p3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public final consumePendingAction()V
    .locals 10
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final getStorePremiumGuildSubscription()Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    return-object v0
.end method

.method public final getStoreSubscriptions()Lcom/discord/stores/StoreSubscriptions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    return-object v0
.end method

.method public final handleGuildSearchCallback(J)V
    .locals 10
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPendingAction()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    move-result-object v0

    instance-of v2, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPendingAction()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;JLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;

    move-result-object p1

    :goto_0
    move-object v6, p1

    goto :goto_1

    :cond_1
    instance-of v0, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPendingAction()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    const-wide/16 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;JLjava/lang/Long;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->getPendingAction()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    move-result-object p1

    goto :goto_0

    :goto_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public final retryClicked()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;->fetchData()V

    return-void
.end method

.method public final subscribeClicked(J)V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v6, v0

    move-wide v7, p1

    invoke-direct/range {v6 .. v11}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Subscribe;-><init>(JLjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x0

    const/16 v8, 0x2f

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final transferClicked(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;J)V
    .locals 15
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "slot"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v12, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v12

    move-object/from16 v2, p1

    move-wide/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction$Transfer;-><init>(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;JLjava/lang/Long;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v13, 0x2f

    const/4 v14, 0x0

    move-object v1, v0

    move v2, v8

    move v3, v9

    move-object v4, v10

    move-object v5, v11

    move-object v6, v12

    move v8, v13

    move-object v9, v14

    invoke-static/range {v1 .. v9}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-result-object v0

    move-object v1, p0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_1
    move-object v1, p0

    return-void
.end method
