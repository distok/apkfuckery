.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetSettingsPremiumGuildSubscriptionAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->onConfigure(ILcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;

.field public final synthetic $hasCooldown:Z

.field public final synthetic $isActivePremiumGuildSubscription:Z

.field public final synthetic this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;ZZLcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    iput-boolean p2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$isActivePremiumGuildSubscription:Z

    iput-boolean p3, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$hasCooldown:Z

    iput-object p4, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$data:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$isActivePremiumGuildSubscription:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$hasCooldown:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    invoke-static {p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->access$getAdapter$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->access$getSubscribeListener$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$data:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;

    check-cast v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$hasCooldown:Z

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    invoke-static {v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->access$getAdapter$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->access$getCanCancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Z

    move-result v4

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;

    invoke-static {v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->access$getAdapter$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;

    move-result-object v0

    invoke-static {v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;->access$getCanUncancelBoosts$p(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter;)Z

    move-result v5

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem$onConfigure$1;->$data:Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;

    move-object v6, v0

    check-cast v6, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    move-object v2, p1

    invoke-static/range {v1 .. v6}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;->access$showPremiumGuildSubPopup(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$PremiumGuildSubListItem;Landroid/view/View;ZZZLcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;)V

    :goto_1
    return-void
.end method
