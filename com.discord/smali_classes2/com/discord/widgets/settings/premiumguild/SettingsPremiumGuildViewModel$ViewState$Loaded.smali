.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;
.source "SettingsPremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final canCancelBoosts:Z

.field private final canUncancelBoosts:Z

.field private final pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

.field private final premiumGuildSubscriptionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final sampleGuildItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;


# direct methods
.method public constructor <init>(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;",
            ">;",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionItems"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sampleGuildItems"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPremiumTier"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    iput-boolean p2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    iput-object p3, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    iput-object p4, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    iput-object p5, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    iput-object p6, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->copy(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    return v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    return-object v0
.end method

.method public final component6()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final copy(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;",
            ">;",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionItems"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sampleGuildItems"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPremiumTier"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;-><init>(ZZLjava/util/List;Ljava/util/List;Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    iget-boolean v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    iget-boolean v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object p1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCanCancelBoosts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    return v0
.end method

.method public final getCanUncancelBoosts()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    return v0
.end method

.method public final getPendingAction()Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    return-object v0
.end method

.method public final getPremiumGuildSubscriptionItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    return-object v0
.end method

.method public final getSampleGuildItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildSubscriptionSampleGuildAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    return-object v0
.end method

.method public final getUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Loaded(canCancelBoosts="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canCancelBoosts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", canUncancelBoosts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->canUncancelBoosts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", premiumGuildSubscriptionItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->premiumGuildSubscriptionItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sampleGuildItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->sampleGuildItems:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pendingAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->pendingAction:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$PendingAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userPremiumTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$ViewState$Loaded;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
