.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;
.super Ljava/lang/Object;
.source "SettingsPremiumGuildViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final guilds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation
.end field

.field private final premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

.field private final subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

.field private final userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")V"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guilds"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPremiumTier"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object p2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    iput-object p3, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    iput-object p4, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->copy(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object v0
.end method

.method public final component3()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    return-object v0
.end method

.method public final component4()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
            ")",
            "Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;"
        }
    .end annotation

    const-string v0, "premiumGuildSubscriptionState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guilds"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userPremiumTier"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;-><init>(Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    iget-object p1, p1, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getGuilds()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelGuild;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    return-object v0
.end method

.method public final getPremiumGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-object v0
.end method

.method public final getSubscriptionState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object v0
.end method

.method public final getUserPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(premiumGuildSubscriptionState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->premiumGuildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->subscriptionState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guilds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->guilds:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", userPremiumTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$StoreState;->userPremiumTier:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
