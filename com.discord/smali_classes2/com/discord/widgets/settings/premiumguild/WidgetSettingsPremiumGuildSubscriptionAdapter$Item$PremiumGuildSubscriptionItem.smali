.class public final Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;
.super Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;
.source "WidgetSettingsPremiumGuildSubscriptionAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PremiumGuildSubscriptionItem"
.end annotation


# instance fields
.field private final subscriptionEndsAt:Ljava/lang/String;

.field private final subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;)V
    .locals 1

    const-string v0, "subscriptionSlot"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    iput-object p2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->copy(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;)Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;
    .locals 1

    const-string v0, "subscriptionSlot"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    invoke-direct {v0, p1, p2}, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;-><init>(Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    iget-object v1, p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    iget-object p1, p1, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSubscriptionEndsAt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubscriptionSlot()Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    return-object v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "PremiumGuildSubscriptionItem(subscriptionSlot="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionSlot:Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionEndsAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premiumguild/WidgetSettingsPremiumGuildSubscriptionAdapter$Item$PremiumGuildSubscriptionItem;->subscriptionEndsAt:Ljava/lang/String;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
