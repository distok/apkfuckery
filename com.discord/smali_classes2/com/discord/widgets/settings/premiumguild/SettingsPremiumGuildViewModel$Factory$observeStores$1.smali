.class public final Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;
.super Ljava/lang/Object;
.source "SettingsPremiumGuildViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;

    invoke-direct {v0}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelUser;)Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getPremiumTier()Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;->NONE:Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premiumguild/SettingsPremiumGuildViewModel$Factory$observeStores$1;->call(Lcom/discord/models/domain/ModelUser;)Lcom/discord/models/domain/ModelSubscriptionPlan$PremiumTier;

    move-result-object p1

    return-object p1
.end method
