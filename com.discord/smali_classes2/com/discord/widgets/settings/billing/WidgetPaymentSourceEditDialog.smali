.class public final Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;
.super Lcom/discord/app/AppDialog;
.source "WidgetPaymentSourceEditDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;,
        Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ARG_PAYMENT_SOURCE_ID:Ljava/lang/String; = "ARG_PAYMENT_SOURCE_ID"

.field public static final Companion:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;


# instance fields
.field private final addressInput1$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final addressInput2$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final caProvinces$delegate:Lkotlin/Lazy;

.field private final cityInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final countryInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final defaultCheckbox$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final deleteBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final errorText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final helpText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final nameInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final paymentSourceId$delegate:Lkotlin/Lazy;

.field private final paymentSourceView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final postalCodeInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final saveBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stateInput$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final usStates$delegate:Lkotlin/Lazy;

.field private validationManager:Lcom/discord/utilities/view/validators/ValidationManager;

.field private viewModel:Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xe

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v3, "paymentSourceView"

    const-string v4, "getPaymentSourceView()Lcom/discord/widgets/settings/billing/PaymentSourceView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "helpText"

    const-string v7, "getHelpText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "errorText"

    const-string v7, "getErrorText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "nameInput"

    const-string v7, "getNameInput()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "addressInput1"

    const-string v7, "getAddressInput1()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "addressInput2"

    const-string v7, "getAddressInput2()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "cityInput"

    const-string v7, "getCityInput()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "stateInput"

    const-string v7, "getStateInput()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "postalCodeInput"

    const-string v7, "getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "countryInput"

    const-string v7, "getCountryInput()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "defaultCheckbox"

    const-string v7, "getDefaultCheckbox()Landroid/widget/CheckBox;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "toolbar"

    const-string v7, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "saveBtn"

    const-string v7, "getSaveBtn()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const-string v6, "deleteBtn"

    const-string v7, "getDeleteBtn()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->Companion:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppDialog;-><init>()V

    const v0, 0x7f0a0770

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->paymentSourceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a076c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->helpText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a076b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->errorText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a076d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->nameInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0766

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->addressInput1$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0767

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->addressInput2$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0768

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->cityInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a076f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->stateInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a076e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->postalCodeInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0769

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->countryInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a076a

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->defaultCheckbox$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0047

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0352

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->saveBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0351

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->deleteBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$paymentSourceId$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$paymentSourceId$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->paymentSourceId$delegate:Lkotlin/Lazy;

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$usStates$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$usStates$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->usStates$delegate:Lkotlin/Lazy;

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$caProvinces$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$caProvinces$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->caProvinces$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$deletePaymentSource(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;Lcom/discord/models/domain/ModelPaymentSource;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->deletePaymentSource(Lcom/discord/models/domain/ModelPaymentSource;)V

    return-void
.end method

.method public static final synthetic access$getPaymentSourceId$p(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPaymentSourceId()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSaveBtn$p(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)Lcom/discord/views/LoadingButton;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getSaveBtn()Lcom/discord/views/LoadingButton;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStateInput$p(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$initPaymentSourceInfo(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->initPaymentSourceInfo(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;)V

    return-void
.end method

.method public static final synthetic access$selectState(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->selectState([Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V

    return-void
.end method

.method public static final synthetic access$updatePaymentSource(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;Lcom/discord/models/domain/ModelPaymentSource;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->updatePaymentSource(Lcom/discord/models/domain/ModelPaymentSource;)V

    return-void
.end method

.method private final deletePaymentSource(Lcom/discord/models/domain/ModelPaymentSource;)V
    .locals 12

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getSaveBtn()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPaymentSource;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/rest/RestAPI;->deletePaymentSource(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x2

    invoke-static {p1, p0, v2, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    new-instance v9, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$deletePaymentSource$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$deletePaymentSource$1;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    new-instance v7, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$deletePaymentSource$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$deletePaymentSource$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    new-instance v8, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$deletePaymentSource$3;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$deletePaymentSource$3;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    const/4 v6, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final getAddressInput1()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->addressInput1$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getAddressInput2()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->addressInput2$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getCaProvinces()[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->caProvinces$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    return-object v0
.end method

.method private static synthetic getCaProvinces$annotations()V
    .locals 0

    return-void
.end method

.method private final getCityInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->cityInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getCountryInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->countryInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getDefaultCheckbox()Landroid/widget/CheckBox;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->defaultCheckbox$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    return-object v0
.end method

.method private final getDeleteBtn()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->deleteBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getErrorText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->errorText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getHelpText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->helpText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNameInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->nameInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getPaymentSourceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->paymentSourceId$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private final getPaymentSourceView()Lcom/discord/widgets/settings/billing/PaymentSourceView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->paymentSourceView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/billing/PaymentSourceView;

    return-object v0
.end method

.method private final getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->postalCodeInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getSaveBtn()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->saveBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getStateInput()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->stateInput$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getStatesFor(Lcom/discord/models/domain/ModelPaymentSource;)[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;
    .locals 2

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPaymentSource;->getBillingAddress()Lcom/discord/models/domain/billing/ModelBillingAddress;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getCountry()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x85e

    if-eq v0, v1, :cond_1

    const/16 v1, 0xa9e

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "US"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getUsStates()[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string v0, "CA"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getCaProvinces()[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    move-result-object p1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    new-array p1, p1, [Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    :goto_1
    return-object p1
.end method

.method private final getToolbar()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->toolbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final getUsStates()[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->usStates$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    return-object v0
.end method

.method private static synthetic getUsStates$annotations()V
    .locals 0

    return-void
.end method

.method private final initPaymentSourceInfo(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;)V
    .locals 11

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->initValidator(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;->component1()Lcom/discord/models/domain/ModelPaymentSource;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;->component2()Z

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPaymentSourceView()Lcom/discord/widgets/settings/billing/PaymentSourceView;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/discord/widgets/settings/billing/PaymentSourceView;->bind(Lcom/discord/models/domain/ModelPaymentSource;Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getHelpText()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "requireContext()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v3, v0, Lcom/discord/models/domain/ModelPaymentSource$ModelPaymentSourcePaypal;

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-eqz v3, :cond_0

    const v3, 0x7f121279

    new-array v4, v10, [Ljava/lang/Object;

    const-string v5, "https://www.paypal.com"

    aput-object v5, v4, v9

    invoke-virtual {p0, v3, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    const v3, 0x7f121278

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const-string/jumbo v4, "when (paymentSource) {\n \u2026edit_help_card)\n        }"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStatesFor(Lcom/discord/models/domain/ModelPaymentSource;)[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPaymentSource;->getBillingAddress()Lcom/discord/models/domain/billing/ModelBillingAddress;

    move-result-object v2

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getNameInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getAddressInput1()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getLine_1()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getAddressInput2()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getLine_2()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getCityInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getCity()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getPostalCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    const-string v4, "states"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v4, v1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_2

    aget-object v6, v1, v5

    invoke-virtual {v6}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getState()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;->getLabel()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getState()Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-static {v3, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getCountryInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getDefaultCheckbox()Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPaymentSource;->getDefault()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getSaveBtn()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getDeleteBtn()Landroid/widget/Button;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getDeleteBtn()Landroid/widget/Button;

    move-result-object p1

    const v2, 0x3e99999a    # 0.3f

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setAlpha(F)V

    goto :goto_4

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getDeleteBtn()Landroid/widget/Button;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$3;

    invoke-direct {v2, p0, v0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$3;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;Lcom/discord/models/domain/ModelPaymentSource;)V

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getSaveBtn()Lcom/discord/views/LoadingButton;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$4;

    invoke-direct {v2, p0, v0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$4;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;Lcom/discord/models/domain/ModelPaymentSource;)V

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelPaymentSource;->getBillingAddress()Lcom/discord/models/domain/billing/ModelBillingAddress;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getCountry()Ljava/lang/String;

    move-result-object p1

    const-string v0, "CA"

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    const v0, 0x7f1202d0

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    const v0, 0x7f1202cd

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    :cond_5
    array-length p1, v1

    if-nez p1, :cond_6

    const/4 v9, 0x1

    :cond_6
    if-eqz v9, :cond_8

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p1, v10}, Landroid/widget/EditText;->setInputType(I)V

    :cond_7
    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-virtual {p1, v10}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    goto :goto_5

    :cond_8
    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$5;

    invoke-direct {v0, p0, v1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$5;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditTextClickListener(Lcom/google/android/material/textfield/TextInputLayout;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$6;

    invoke-direct {v0, p0, v1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$initPaymentSourceInfo$6;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnEditorActionListener(Lcom/google/android/material/textfield/TextInputLayout;Lkotlin/jvm/functions/Function3;)Lkotlin/Unit;

    :cond_9
    :goto_5
    return-void
.end method

.method private final initValidator(Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;)V
    .locals 9

    invoke-virtual {p1}, Lcom/discord/widgets/settings/billing/PaymentSourceAdapter$PaymentSourceItem;->component1()Lcom/discord/models/domain/ModelPaymentSource;

    move-result-object p1

    new-instance v0, Lcom/discord/utilities/view/validators/ValidationManager;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/discord/utilities/view/validators/Input;

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getNameInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    sget-object v6, Lcom/discord/utilities/view/validators/BasicTextInputValidator;->Companion:Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;

    const v7, 0x7f1202cc

    invoke-virtual {v6, v7}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v5, v8

    const-string v7, "name"

    invoke-direct {v2, v7, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getAddressInput1()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    const v7, 0x7f1202c4

    invoke-virtual {v6, v7}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v7

    aput-object v7, v5, v8

    const-string v7, "line_1"

    invoke-direct {v2, v7, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, v4

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getCityInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    const v7, 0x7f1202c7

    invoke-virtual {v6, v7}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v7

    aput-object v7, v5, v8

    const-string v7, "city"

    invoke-direct {v2, v7, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    const/4 v3, 0x2

    aput-object v2, v1, v3

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelPaymentSource;->getBillingAddress()Lcom/discord/models/domain/billing/ModelBillingAddress;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/billing/ModelBillingAddress;->getCountry()Ljava/lang/String;

    move-result-object p1

    const-string v7, "CA"

    invoke-static {p1, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f1202d1

    goto :goto_0

    :cond_0
    const p1, 0x7f1202d4

    :goto_0
    invoke-virtual {v6, p1}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object p1

    aput-object p1, v5, v8

    const-string p1, "state"

    invoke-direct {v2, p1, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    const/4 p1, 0x3

    aput-object v2, v1, p1

    const/4 p1, 0x4

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    new-array v4, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    const v5, 0x7f1202cf

    invoke-virtual {v6, v5}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v5

    aput-object v5, v4, v8

    const-string v5, "postal_code"

    invoke-direct {v2, v5, v3, v4}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, p1

    invoke-direct {v0, v1}, Lcom/discord/utilities/view/validators/ValidationManager;-><init>([Lcom/discord/utilities/view/validators/Input;)V

    iput-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->validationManager:Lcom/discord/utilities/view/validators/ValidationManager;

    return-void
.end method

.method private final selectState([Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V
    .locals 8

    sget-object v0, Lf/a/a/o;->i:Lf/a/a/o$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f12127b

    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.payme\u2026source_edit_select_state)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    array-length v4, p1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    array-length v4, p1

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v4, :cond_0

    aget-object v7, p1, v6

    invoke-virtual {v7}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;->getLabel()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    new-array v4, v5, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-static {v3, v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v3, [Ljava/lang/CharSequence;

    new-instance v4, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$selectState$2;

    invoke-direct {v4, p0, p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$selectState$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lf/a/a/o$a;->a(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;[Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function1;)Lf/a/a/o;

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->requestFocus()Z

    return-void
.end method

.method private final updatePaymentSource(Lcom/discord/models/domain/ModelPaymentSource;)V
    .locals 17

    move-object/from16 v0, p0

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->Companion:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStateInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-direct/range {p0 .. p1}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getStatesFor(Lcom/discord/models/domain/ModelPaymentSource;)[Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;

    move-result-object v2

    const-string v3, "getStatesFor(paymentSource)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v6, 0x1

    const/4 v7, 0x0

    if-ge v5, v3, :cond_1

    aget-object v8, v2, v5

    invoke-virtual {v8}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;->getLabel()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v1, v6}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    move-object v8, v7

    :goto_1
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$StateEntry;->getValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object v13, v2

    goto :goto_2

    :cond_2
    move-object v13, v1

    :goto_2
    iget-object v1, v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->validationManager:Lcom/discord/utilities/view/validators/ValidationManager;

    if-eqz v1, :cond_4

    invoke-static {v1, v4, v6, v7}, Lcom/discord/utilities/view/validators/ValidationManager;->validate$default(Lcom/discord/utilities/view/validators/ValidationManager;ZILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return-void

    :cond_3
    new-instance v1, Lcom/discord/models/domain/PatchPaymentSourceRaw;

    new-instance v2, Lcom/discord/models/domain/billing/ModelBillingAddress;

    sget-object v3, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->Companion:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getNameInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getAddressInput1()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getAddressInput2()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getCityInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getPostalCodeInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getCountryInput()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;->access$getTextOrEmpty(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$Companion;Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v14

    move-object v8, v2

    invoke-direct/range {v8 .. v15}, Lcom/discord/models/domain/billing/ModelBillingAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getDefaultCheckbox()Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/discord/models/domain/PatchPaymentSourceRaw;-><init>(Lcom/discord/models/domain/billing/ModelBillingAddress;Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getErrorText()Landroid/widget/TextView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getSaveBtn()Lcom/discord/views/LoadingButton;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    sget-object v2, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelPaymentSource;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/discord/utilities/rest/RestAPI;->updatePaymentSource(Ljava/lang/String;Lcom/discord/models/domain/PatchPaymentSourceRaw;)Lrx/Observable;

    move-result-object v1

    invoke-static {v1, v4, v6, v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v0, v7, v2, v7}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-instance v14, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$updatePaymentSource$1;

    invoke-direct {v14, v0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$updatePaymentSource$1;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    new-instance v12, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$updatePaymentSource$2;

    invoke-direct {v12, v0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$updatePaymentSource$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    new-instance v13, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$updatePaymentSource$3;

    invoke-direct {v13, v0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$updatePaymentSource$3;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_4
    const-string v1, "validationManager"

    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0247

    return v0
.end method

.method public final handleError(Lcom/discord/utilities/error/Error;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getErrorText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v1

    const-string v2, "error.response"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/discord/utilities/error/Error$Response;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->validationManager:Lcom/discord/utilities/view/validators/ValidationManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v3

    invoke-static {v3, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/discord/utilities/error/Error$Response;->getMessages()Ljava/util/Map;

    move-result-object v3

    const-string v4, "error.response.messages"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/discord/utilities/view/validators/ValidationManager;->setErrors(Ljava/util/Map;)Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error$Response;->getMessage()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0x8

    invoke-static {v0, p1, v3, v1, v2}, Lf/a/b/p;->j(Landroid/content/Context;Ljava/lang/CharSequence;ILcom/discord/utilities/view/ToastManager;I)V

    :cond_1
    return-void

    :cond_2
    const-string p1, "validationManager"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/discord/app/AppDialog;->onStart()V

    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const v2, 0x7f1302fb

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 11

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppDialog;->onViewBound(Landroid/view/View;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$Factory;

    invoke-direct {v1}, Lcom/discord/widgets/settings/billing/SettingsBillingViewModel$Factory;-><init>()V

    invoke-direct {p1, v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026ingViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;

    iput-object p1, p0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->viewModel:Lcom/discord/widgets/settings/billing/SettingsBillingViewModel;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$$inlined$filterIs$1;->INSTANCE:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$$inlined$filterIs$1;

    invoke-virtual {p1, v1}, Lrx/Observable;->v(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$$inlined$filterIs$2;->INSTANCE:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$$inlined$filterIs$2;

    invoke-virtual {p1, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v1, "filter { it is T }.map { it as T }"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$1;->INSTANCE:Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$1;

    invoke-virtual {p1, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$2;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-virtual {p1, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string v1, "viewModel\n        .obser\u2026ymentSourceId }\n        }"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationBuffered(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p1

    const-string v1, "viewModel\n        .obser\u2026ffered()\n        .take(1)"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$3;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$3;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getToolbar()Landroidx/appcompat/widget/Toolbar;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$4;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;->getSaveBtn()Lcom/discord/views/LoadingButton;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$5;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog$onViewBound$5;-><init>(Lcom/discord/widgets/settings/billing/WidgetPaymentSourceEditDialog;)V

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const-string p1, "viewModel"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
