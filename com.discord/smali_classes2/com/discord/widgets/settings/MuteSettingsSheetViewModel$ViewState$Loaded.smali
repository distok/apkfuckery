.class public final Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;
.source "MuteSettingsSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final isChannelMuted:Z

.field private final isGuildMuted:Z

.field private final muteEndTime:Ljava/lang/String;

.field private final notificationOverride:I

.field private final settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

.field private final subtitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;I)V
    .locals 1

    const-string v0, "settingsType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    iput-object p2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    iput-boolean p4, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    iput-object p5, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    iput p6, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p7, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p3

    :goto_0
    and-int/lit8 v0, p7, 0x8

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    move v6, p4

    :goto_1
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    move-object v7, v0

    goto :goto_2

    :cond_2
    move-object v7, p5

    :goto_2
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_3

    sget v0, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_UNSET:I

    move v8, v0

    goto :goto_3

    :cond_3
    move v8, p6

    :goto_3
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;-><init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;IILjava/lang/Object;)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->copy(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;I)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    return v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    return v0
.end method

.method public final copy(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;I)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;
    .locals 8

    const-string v0, "settingsType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;-><init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    iget-object v1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    iget-boolean v1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    iget p1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMuteEndTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public final getNotificationOverride()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    return v0
.end method

.method public final getSettingsType()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    return-object v0
.end method

.method public final getSubtitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    move v3, v2

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final isChannelMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    return v0
.end method

.method public final isGuildMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(settingsType="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->settingsType:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isChannelMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isGuildMuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", muteEndTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->muteEndTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", notificationOverride="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->notificationOverride:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
