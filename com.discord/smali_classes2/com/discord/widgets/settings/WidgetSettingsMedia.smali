.class public final Lcom/discord/widgets/settings/WidgetSettingsMedia;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsMedia.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;


# instance fields
.field private final allowAnimatedEmojiCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final attachmentsCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final embedsCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final imageCompressionCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final imageCompressionContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final imageCompressionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final linksCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickersAlwaysAnimateCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickersAnimateOnInteractionCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private stickersAnimationRadioManager:Lcom/discord/views/RadioManager;

.field private final stickersAnimationRadios$delegate:Lkotlin/Lazy;

.field private final stickersContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickersNeverAnimateCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final syncCS$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private userSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xc

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v3, "attachmentsCS"

    const-string v4, "getAttachmentsCS()Lcom/discord/views/CheckedSetting;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "embedsCS"

    const-string v7, "getEmbedsCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "linksCS"

    const-string v7, "getLinksCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "syncCS"

    const-string v7, "getSyncCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "allowAnimatedEmojiCS"

    const-string v7, "getAllowAnimatedEmojiCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "imageCompressionContainer"

    const-string v7, "getImageCompressionContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "imageCompressionCS"

    const-string v7, "getImageCompressionCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "imageCompressionUpsellView"

    const-string v7, "getImageCompressionUpsellView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "stickersContainer"

    const-string v7, "getStickersContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "stickersAlwaysAnimateCS"

    const-string v7, "getStickersAlwaysAnimateCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "stickersAnimateOnInteractionCS"

    const-string v7, "getStickersAnimateOnInteractionCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v6, "stickersNeverAnimateCS"

    const-string v7, "getStickersNeverAnimateCS()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->Companion:Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a09d4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->attachmentsCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09d8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->embedsCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09d9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->linksCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09de

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->syncCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09d3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->allowAnimatedEmojiCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09d5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->imageCompressionContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09d6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->imageCompressionCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09d7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->imageCompressionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09dc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09da

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAlwaysAnimateCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09db

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAnimateOnInteractionCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09dd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersNeverAnimateCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsMedia$stickersAnimationRadios$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$stickersAnimationRadios$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAnimationRadios$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$configureStickerAnimationRadio(Lcom/discord/widgets/settings/WidgetSettingsMedia;ILcom/discord/views/CheckedSetting;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->configureStickerAnimationRadio(ILcom/discord/views/CheckedSetting;I)V

    return-void
.end method

.method public static final synthetic access$getAllowAnimatedEmojiCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getAllowAnimatedEmojiCS()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStickersAlwaysAnimateCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getStickersAlwaysAnimateCS()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStickersAnimateOnInteractionCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getStickersAnimateOnInteractionCS()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStickersContainer$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getStickersContainer()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStickersNeverAnimateCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getStickersNeverAnimateCS()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getUserSettings$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/stores/StoreUserSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "userSettings"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setUserSettings$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;Lcom/discord/stores/StoreUserSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    return-void
.end method

.method private final configureStickerAnimationRadio(ILcom/discord/views/CheckedSetting;I)V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;

    invoke-direct {v0, p0, p3}, Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;I)V

    invoke-virtual {p2, v0}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAnimationRadioManager:Lcom/discord/views/RadioManager;

    if-eqz v0, :cond_0

    if-ne p1, p3, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    :cond_0
    return-void
.end method

.method private final getAllowAnimatedEmojiCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->allowAnimatedEmojiCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getAttachmentsCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->attachmentsCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getEmbedsCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->embedsCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getImageCompressionCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->imageCompressionCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getImageCompressionContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->imageCompressionContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getImageCompressionUpsellView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->imageCompressionUpsellView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getLinksCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->linksCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getStickersAlwaysAnimateCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAlwaysAnimateCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getStickersAnimateOnInteractionCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAnimateOnInteractionCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getStickersAnimationRadios()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/views/CheckedSetting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAnimationRadios$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getStickersContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStickersNeverAnimateCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersNeverAnimateCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getSyncCS()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->syncCS$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->Companion:Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02ab

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const p1, 0x7f121971

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const p1, 0x7f12181d

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 17

    move-object/from16 v0, p0

    const-string v1, "view"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v2

    iput-object v2, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    new-instance v2, Lcom/discord/views/RadioManager;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getStickersAnimationRadios()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object v2, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->stickersAnimationRadioManager:Lcom/discord/views/RadioManager;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getAttachmentsCS()Lcom/discord/views/CheckedSetting;

    move-result-object v2

    iget-object v3, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    const-string v4, "userSettings"

    const/4 v5, 0x0

    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/discord/stores/StoreUserSettings;->getShowAttachmentMediaInline()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getAttachmentsCS()Lcom/discord/views/CheckedSetting;

    move-result-object v2

    const v3, 0x7f120d80

    invoke-virtual {v0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v6, "getString(R.string.inline_attachment_media_help)"

    invoke-static {v3, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Object;

    const-string v8, "8"

    const/4 v9, 0x0

    aput-object v8, v7, v9

    invoke-static {v7, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v7

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v7, "java.lang.String.format(format, *args)"

    invoke-static {v3, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x2

    invoke-static {v2, v3, v9, v7}, Lcom/discord/views/CheckedSetting;->i(Lcom/discord/views/CheckedSetting;Ljava/lang/CharSequence;ZI)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getAttachmentsCS()Lcom/discord/views/CheckedSetting;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$1;

    invoke-direct {v3, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-virtual {v2, v3}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v1

    const-string v2, "2020-09_mobile_image_compression"

    invoke-virtual {v1, v2, v6}, Lcom/discord/stores/StoreExperiments;->getUserExperiment(Ljava/lang/String;Z)Lcom/discord/models/experiments/domain/Experiment;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v10

    const-string v2, "requireContext()"

    invoke-static {v10, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f120d6c

    new-array v3, v6, [Ljava/lang/Object;

    const-string v8, "https://discord.com"

    aput-object v8, v3, v9

    invoke-virtual {v0, v2, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const-string v2, "getString(R.string.image\u2026upsell, BuildConfig.HOST)"

    invoke-static {v11, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v12, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$imageCompressionSubtext$1;

    invoke-direct {v12, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$imageCompressionSubtext$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x18

    const/16 v16, 0x0

    invoke-static/range {v10 .. v16}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getImageCompressionCS()Lcom/discord/views/CheckedSetting;

    move-result-object v3

    new-instance v8, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$2;

    invoke-direct {v8, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-virtual {v3, v8}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getImageCompressionCS()Lcom/discord/views/CheckedSetting;

    move-result-object v3

    iget-object v8, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz v8, :cond_7

    invoke-virtual {v8}, Lcom/discord/stores/StoreUserSettings;->getAutoImageCompressionEnabled()Z

    move-result v8

    invoke-virtual {v3, v8}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getImageCompressionContainer()Landroid/view/View;

    move-result-object v3

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/experiments/domain/Experiment;->getBucket()I

    move-result v1

    const/4 v8, 0x3

    if-ne v1, v8, :cond_0

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-eqz v6, :cond_1

    goto :goto_1

    :cond_1
    const/16 v9, 0x8

    :goto_1
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getImageCompressionUpsellView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getEmbedsCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getInlineEmbedMedia()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getEmbedsCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$3;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$3;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getLinksCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getRenderEmbeds()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getLinksCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$4;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$4;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getSyncCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/discord/stores/StoreUserSettings;->getSyncTextAndImages()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getSyncCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$5;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$5;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    iget-object v1, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getAllowAnimatedEmojisObservable()Lrx/Observable;

    move-result-object v1

    const-string v2, "userSettings\n        .al\u2026wAnimatedEmojisObservable"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0, v5, v7, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$6;

    invoke-direct {v14, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$6;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->getAllowAnimatedEmojiCS()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$$inlined$apply$lambda$1;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    iget-object v1, v0, Lcom/discord/widgets/settings/WidgetSettingsMedia;->userSettings:Lcom/discord/stores/StoreUserSettings;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->observeStickerAnimationSettings()Lrx/Observable;

    move-result-object v1

    const-string v2, "userSettings\n        .ob\u2026tickerAnimationSettings()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v0, v5, v7, v5}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;

    invoke-direct {v14, v0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_2
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5

    :cond_3
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5

    :cond_4
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5

    :cond_5
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5

    :cond_6
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5

    :cond_7
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5

    :cond_8
    invoke-static {v4}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v5
.end method

.method public onViewBoundOrOnResume()V
    .locals 12

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    const-string v1, "2020-09_dsti_user_settings"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreExperiments;->observeUserExperiment(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsMedia;

    new-instance v9, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
