.class public final Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;
.super Lx/m/c/k;
.source "WidgetSettingsAppearance.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettingsAppearance;->showHolyLight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Long;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettingsAppearance;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;->invoke(Ljava/lang/Long;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Long;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->access$getThemeHolyLightView$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Landroid/view/View;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->access$tryEnableTorchMode(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Z)V

    return-void
.end method
