.class public final Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs$onResume$1;
.super Lx/m/c/k;
.source "WidgetSettingsNotificationsOs.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs$onResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs$onResume$1;->invoke(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V
    .locals 2

    const-string v0, "settings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs$onResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;->access$getNotificationsSwitch$p(Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs$onResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;->access$getNotificationsInAppSwitch$p(Lcom/discord/widgets/settings/WidgetSettingsNotificationsOs;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isEnabledInApp()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    return-void
.end method
