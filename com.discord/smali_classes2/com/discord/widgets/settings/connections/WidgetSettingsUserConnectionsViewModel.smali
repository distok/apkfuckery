.class public final Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;
.super Lf/a/b/l0;
.source "WidgetSettingsUserConnectionsViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;,
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;,
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState;,
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus;,
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final joinStateSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus;",
            ">;>;"
        }
    .end annotation
.end field

.field private final joinStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final restApi:Lcom/discord/utilities/rest/RestAPI;

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private final storeUserConnections:Lcom/discord/stores/StoreUserConnections;


# direct methods
.method public constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreUserConnections;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreUserConnections;",
            "Lcom/discord/utilities/rest/RestAPI;",
            ")V"
        }
    .end annotation

    const-string v0, "storeStateObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeUserConnections"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restApi"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->storeStateObservable:Lrx/Observable;

    iput-object p2, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->storeUserConnections:Lcom/discord/stores/StoreUserConnections;

    iput-object p3, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->restApi:Lcom/discord/utilities/rest/RestAPI;

    new-instance p3, Ljava/util/LinkedHashMap;

    invoke-direct {p3}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p3, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStatusMap:Ljava/util/Map;

    sget-object p3, Lx/h/m;->d:Lx/h/m;

    invoke-static {p3}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object p3

    iput-object p3, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStateSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p2}, Lcom/discord/stores/StoreUserConnections;->fetchConnectedAccounts()V

    new-instance p2, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$1;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)V

    invoke-static {p1, p3, p2}, Lrx/Observable;->j(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026te(storeState, joinMap) }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    new-instance v6, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$2;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$combineState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;Ljava/util/Map;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->combineState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;Ljava/util/Map;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getJoinStateSubject$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStateSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getJoinStatusMap$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStatusMap:Ljava/util/Map;

    return-object p0
.end method

.method public static final synthetic access$handleConnectionsState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->handleConnectionsState(Ljava/util/List;)V

    return-void
.end method

.method private final combineState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;Ljava/util/Map;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;->getConnectedAccounts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/models/domain/ModelConnectedAccount;

    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelConnectedAccount;->getIntegrations()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/models/domain/ModelConnectedAccountIntegration;

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelConnectedAccountIntegration;->getGuild()Lcom/discord/models/domain/ModelConnectedIntegrationGuild;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelConnectedIntegrationGuild;->getId()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$StoreState;->getGuilds()Ljava/util/Map;

    move-result-object v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    sget-object v8, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus$Joined;->INSTANCE:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus$Joined;

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {p2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus;

    if-eqz v7, :cond_0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v3, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;

    invoke-direct {v4, v2, v3}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;-><init>(Lcom/discord/models/domain/ModelConnectedAccount;Ljava/util/Map;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private final handleConnectionsState(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    sget-object p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Empty;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    new-instance v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Loaded;

    invoke-direct {v0, p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Loaded;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final getRestApi()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->restApi:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final joinConnectionIntegrationGuild(J)V
    .locals 13

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStatusMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus$Joining;->INSTANCE:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus$Joining;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStateSubject:Lrx/subjects/BehaviorSubject;

    iget-object v1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinStatusMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->restApi:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->joinGuildFromIntegration(J)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    new-instance v8, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;

    invoke-direct {v8, p0, p1, p2}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;J)V

    new-instance v10, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$2;

    invoke-direct {v10, p0, p1, p2}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$2;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;J)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x16

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
