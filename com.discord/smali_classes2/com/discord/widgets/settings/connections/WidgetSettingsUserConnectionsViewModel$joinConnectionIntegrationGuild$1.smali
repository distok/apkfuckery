.class public final Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;
.super Lx/m/c/k;
.source "WidgetSettingsUserConnectionsViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->joinConnectionIntegrationGuild(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/error/Error;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $integrationId:J

.field public final synthetic this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    iput-wide p2, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->$integrationId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/error/Error;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->invoke(Lcom/discord/utilities/error/Error;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/error/Error;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    invoke-static {p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->access$getJoinStatusMap$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)Ljava/util/Map;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->$integrationId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus$JoinFailed;->INSTANCE:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$JoinStatus$JoinFailed;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    invoke-static {p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->access$getJoinStateSubject$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$joinConnectionIntegrationGuild$1;->this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    invoke-static {v0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;->access$getJoinStatusMap$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
