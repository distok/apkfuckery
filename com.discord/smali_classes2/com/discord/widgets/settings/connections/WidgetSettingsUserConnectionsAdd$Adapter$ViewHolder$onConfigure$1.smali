.class public final Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetSettingsUserConnectionsAdd.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder;->onConfigure(ILcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;

.field public final synthetic this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder;Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder$onConfigure$1;->this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder;

    iput-object p2, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder$onConfigure$1;->$data:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder$onConfigure$1;->this$0:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder;->getDialogDismissCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder$onConfigure$1;->$data:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;->getPlatform()Lcom/discord/utilities/platform/Platform;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/platform/Platform;->XBOX:Lcom/discord/utilities/platform/Platform;

    const-string v2, "v.context"

    const-string v3, "v"

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAddXbox;->Companion:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAddXbox$Companion;

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAddXbox$Companion;->launch(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserConnections()Lcom/discord/stores/StoreUserConnections;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$Adapter$ViewHolder$onConfigure$1;->$data:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;

    invoke-virtual {v1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsAdd$PlatformItem;->getPlatform()Lcom/discord/utilities/platform/Platform;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/utilities/platform/Platform;->getPlatformId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/discord/stores/StoreUserConnections;->authorizeConnection(Ljava/lang/String;Landroid/content/Context;)V

    :goto_0
    return-void
.end method
