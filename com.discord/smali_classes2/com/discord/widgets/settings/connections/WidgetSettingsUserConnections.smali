.class public final Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsUserConnections.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$UserConnectionItem;,
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;,
        Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final CONNECTION_ID:Ljava/lang/String; = "connection_id"

.field public static final Companion:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;

.field public static final PLATFORM_NAME:Ljava/lang/String; = "platform_name"

.field public static final PLATFORM_TITLE:Ljava/lang/String; = "platform_title"


# instance fields
.field private adapter:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;

.field private final emptyView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;

    const-string v3, "emptyView"

    const-string v4, "getEmptyView()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;

    const-string v6, "recyclerView"

    const-string v7, "getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->Companion:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a02ed

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->emptyView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02ee

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;)Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->viewModel:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->handleViewState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->viewModel:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    return-void
.end method

.method private final getEmptyView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->emptyView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final handleViewState(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->adapter:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->getItemCount()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    instance-of v2, p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Uninitialized;

    const/16 v3, 0x8

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getEmptyView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_1
    instance-of v2, p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Empty;

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getEmptyView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    instance-of v2, p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Loaded;

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getEmptyView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    check-cast p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ViewState$Loaded;->getData()Ljava/util/List;

    move-result-object p1

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;

    new-instance v3, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$UserConnectionItem;

    invoke-direct {v3, v2}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$UserConnectionItem;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$ConnectionState;)V

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->adapter:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    :cond_4
    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    if-le p1, v0, :cond_5

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    :cond_5
    :goto_2
    return-void
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->Companion:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d029a

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$Factory;

    invoke-direct {v0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026onsViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    iput-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->viewModel:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 6

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;

    invoke-direct {p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    const-string v3, "it"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v3, "it.supportFragmentManager"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$onViewBound$$inlined$let$lambda$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$onViewBound$$inlined$let$lambda$1;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;)V

    invoke-direct {v1, v2, p1, v3}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;

    iput-object p1, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->adapter:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$Adapter;

    :cond_0
    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const p1, 0x7f121971

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const p1, 0x7f120523

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    const v1, 0x7f0e0008

    new-instance v2, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$onViewBound$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$onViewBound$2;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu$default(Lcom/discord/app/AppFragment;ILrx/functions/Action2;Lrx/functions/Action1;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 10

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;->viewModel:Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnectionsViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$onViewBoundOrOnResume$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/connections/WidgetSettingsUserConnections;)V

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "viewModel"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method
