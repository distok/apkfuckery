.class public final Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1$2;
.super Lx/m/c/k;
.source "WidgetSettingsDeveloper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;-><init>(Lcom/discord/utilities/experiments/RegisteredExperiment;Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1$2;->this$0:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1$2;->this$0:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;

    iget-object v1, v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;->this$0:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1;

    iget-object v1, v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1;->$experimentStore:Lcom/discord/stores/StoreExperiments;

    iget-object v0, v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1$$special$$inlined$map$lambda$1;->$registeredExperiment:Lcom/discord/utilities/experiments/RegisteredExperiment;

    invoke-virtual {v0}, Lcom/discord/utilities/experiments/RegisteredExperiment;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreExperiments;->clearOverride(Ljava/lang/String;)V

    return-void
.end method
