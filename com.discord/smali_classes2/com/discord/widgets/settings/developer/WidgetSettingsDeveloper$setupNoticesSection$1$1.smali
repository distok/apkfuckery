.class public final Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$1$1;
.super Lcom/discord/utilities/views/SwipeableItemTouchHelper;
.source "WidgetSettingsDeveloper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$1;->invoke()Landroidx/recyclerview/widget/ItemTouchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $deleteConfig:Lcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;Lcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;Lcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;)V
    .locals 7

    iput-object p1, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$1$1;->$deleteConfig:Lcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/discord/utilities/views/SwipeableItemTouchHelper;-><init>(IILcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;Lcom/discord/utilities/views/SwipeableItemTouchHelper$SwipeRevealConfiguration;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public onSwiped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    const-string p2, "viewHolder"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of p2, p1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$NoticeViewHolder;

    if-nez p2, :cond_0

    return-void

    :cond_0
    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object p2

    check-cast p1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$NoticeViewHolder;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$NoticeViewHolder;->getNoticeName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/discord/stores/StoreNotices;->clearSeen(Ljava/lang/String;)V

    return-void
.end method
