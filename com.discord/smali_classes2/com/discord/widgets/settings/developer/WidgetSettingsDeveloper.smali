.class public final Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsDeveloper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$NoticeViewHolder;,
        Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;


# instance fields
.field private final crashDiscordJniBridge$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final crashDiscordNonfatal$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final crashKotlin$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private experimentOverridesAdapter:Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;

.field private final experimentsRv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final noticesRv$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    const-string v3, "experimentsRv"

    const-string v4, "getExperimentsRv()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    const-string v6, "noticesRv"

    const-string v7, "getNoticesRv()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    const-string v6, "crashKotlin"

    const-string v7, "getCrashKotlin()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    const-string v6, "crashDiscordJniBridge"

    const-string v7, "getCrashDiscordJniBridge()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    const-string v6, "crashDiscordNonfatal"

    const-string v7, "getCrashDiscordNonfatal()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->Companion:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0339

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->experimentsRv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a033a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->noticesRv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0338

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->crashKotlin$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0336

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->crashDiscordJniBridge$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0337

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->crashDiscordNonfatal$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getExperimentOverridesAdapter$p(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;)Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->experimentOverridesAdapter:Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "experimentOverridesAdapter"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$setExperimentOverridesAdapter$p(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->experimentOverridesAdapter:Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;

    return-void
.end method

.method private final getCrashDiscordJniBridge()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->crashDiscordJniBridge$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCrashDiscordNonfatal()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->crashDiscordNonfatal$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCrashKotlin()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->crashKotlin$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getExperimentsRv()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->experimentsRv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getNoticesRv()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->noticesRv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->Companion:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method

.method private final setupCrashes()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->getCrashKotlin()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupCrashes$1;->INSTANCE:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupCrashes$1;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->getCrashDiscordNonfatal()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupCrashes$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupCrashes$2;-><init>(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->getCrashDiscordJniBridge()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupCrashes$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupCrashes$3;-><init>(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setupExperimentSection()V
    .locals 12

    new-instance v0, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;

    invoke-direct {v0}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->experimentOverridesAdapter:Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->getExperimentsRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->experimentOverridesAdapter:Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getExperiments()Lcom/discord/stores/StoreExperiments;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreExperiments;->observeOverrides()Lrx/Observable;

    move-result-object v1

    new-instance v3, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1;

    invoke-direct {v3, v0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$1;-><init>(Lcom/discord/stores/StoreExperiments;)V

    invoke-virtual {v1, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "experimentStore\n        \u2026              }\n        }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupExperimentSection$2;-><init>(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;)V

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "experimentOverridesAdapter"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method private final setupNoticesSection()V
    .locals 13

    new-instance v0, Lcom/discord/utilities/views/SimpleRecyclerAdapter;

    sget-object v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$noticesAdapter$1;->INSTANCE:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$noticesAdapter$1;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3, v2}, Lcom/discord/utilities/views/SimpleRecyclerAdapter;-><init>(Ljava/util/List;Lkotlin/jvm/functions/Function2;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->getNoticesRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance v1, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$1;-><init>(Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;)V

    invoke-virtual {v1}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$1;->invoke()Landroidx/recyclerview/widget/ItemTouchHelper;

    move-result-object v1

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->getNoticesRv()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getNotices()Lcom/discord/stores/StoreNotices;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreNotices;->observeNoticesSeen()Lrx/Observable;

    move-result-object v1

    sget-object v3, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$2;->INSTANCE:Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$2;

    invoke-virtual {v1, v3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    const-string v3, "StoreStream\n        .get\u2026      .toList()\n        }"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const/4 v3, 0x2

    invoke-static {v1, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;

    new-instance v10, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$3;

    invoke-direct {v10, v0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper$setupNoticesSection$3;-><init>(Lcom/discord/utilities/views/SimpleRecyclerAdapter;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d029c

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled(Z)Landroidx/appcompat/widget/Toolbar;

    const p1, 0x7f12060f

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->setupCrashes()V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 0

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->setupExperimentSection()V

    invoke-direct {p0}, Lcom/discord/widgets/settings/developer/WidgetSettingsDeveloper;->setupNoticesSection()V

    return-void
.end method
