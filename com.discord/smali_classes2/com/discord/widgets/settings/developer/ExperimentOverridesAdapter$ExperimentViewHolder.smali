.class public final Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$ExperimentViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ExperimentOverridesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExperimentViewHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;)V
    .locals 14

    const-string v0, "item"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0a03ec

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.\u2026experiment_override_view)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/views/experiments/ExperimentOverrideView;

    invoke-interface {p1}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;->getApiName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;->getOverrideBucket()Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;->getBucketDescriptions()Ljava/util/List;

    move-result-object v12

    invoke-interface {p1}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;->getOnOverrideBucketSelected()Lkotlin/jvm/functions/Function1;

    move-result-object v13

    invoke-interface {p1}, Lcom/discord/widgets/settings/developer/ExperimentOverridesAdapter$Item;->getOnOverrideBucketCleared()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    const-string v4, "experimentName"

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "experimentApiName"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "bucketDescriptions"

    invoke-static {v12, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "onOverrideBucketSelected"

    invoke-static {v13, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "onOverrideBucketCleared"

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->d:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->f:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const-string v5, "\n"

    move-object v4, v12

    invoke-static/range {v4 .. v11}, Lx/h/f;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-nez v3, :cond_0

    new-instance v2, Lcom/discord/views/experiments/ExperimentOverrideView$b;

    const/4 v4, 0x0

    const-string v5, "Select..."

    invoke-direct {v2, v4, v5}, Lcom/discord/views/experiments/ExperimentOverrideView$b;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_1

    new-instance v6, Lcom/discord/views/experiments/ExperimentOverrideView$b;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "Bucket "

    invoke-static {v8, v5}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/discord/views/experiments/ExperimentOverrideView$b;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->g:Landroid/widget/Spinner;

    new-instance v5, Lcom/discord/views/experiments/ExperimentOverrideView$a;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "context"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v5, v6, v1}, Lcom/discord/views/experiments/ExperimentOverrideView$a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    if-eqz v3, :cond_2

    iget-object v1, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->g:Landroid/widget/Spinner;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    iget-object v1, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->g:Landroid/widget/Spinner;

    new-instance v2, Lf/a/n/h0/a;

    invoke-direct {v2, v13}, Lf/a/n/h0/a;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->h:Landroid/widget/TextView;

    new-instance v2, Lf/a/n/h0/b;

    invoke-direct {v2, p1}, Lf/a/n/h0/b;-><init>(Lkotlin/jvm/functions/Function0;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, v0, Lcom/discord/views/experiments/ExperimentOverrideView;->h:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    const/16 v4, 0x8

    :goto_2
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
