.class public final Lcom/discord/widgets/settings/WidgetSettingsAppearance;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsAppearance.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;,
        Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;

.field private static final EASTER_EGG_UNLOCK_TIMEOUT:I = 0x5

.field private static final HOLY_LIGHT_UNLOCK_COUNT:I = 0x5

.field private static final PURE_EVIL_HINT_COUNT:I = 0x3

.field private static final PURE_EVIL_UNLOCK_COUNT:I = 0x8


# instance fields
.field private final fontScalingReset$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final fontScalingSeekbar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final fontScalingText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final holyLightEasterEggCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final messageAvatar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final messageTag$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final messageText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final messageTimestamp$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final messageUsername$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final newFontScaleSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final pureEvilEasterEggCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final pureEvilEasterEggSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final syncSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final themeDarkCs$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final themeHolyLightView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final themeLightCs$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final themePureEvilSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private toastManager:Lcom/discord/utilities/view/ToastManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xd

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v3, "themeLightCs"

    const-string v4, "getThemeLightCs()Lcom/discord/views/CheckedSetting;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "themeDarkCs"

    const-string v7, "getThemeDarkCs()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "themePureEvilSwitch"

    const-string v7, "getThemePureEvilSwitch()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "syncSwitch"

    const-string v7, "getSyncSwitch()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "themeHolyLightView"

    const-string v7, "getThemeHolyLightView()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "fontScalingSeekbar"

    const-string v7, "getFontScalingSeekbar()Landroid/widget/SeekBar;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "fontScalingReset"

    const-string v7, "getFontScalingReset()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "fontScalingText"

    const-string v7, "getFontScalingText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "messageAvatar"

    const-string v7, "getMessageAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "messageUsername"

    const-string v7, "getMessageUsername()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "messageTag"

    const-string v7, "getMessageTag()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "messageTimestamp"

    const-string v7, "getMessageTimestamp()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    const-string v6, "messageText"

    const-string v7, "getMessageText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->Companion:Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0972

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themeLightCs$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0971

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themeDarkCs$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0973

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themePureEvilSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0970

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->syncSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a096f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themeHolyLightView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a096c

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->fontScalingSeekbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a096b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->fontScalingReset$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0969

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->fontScalingText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a024f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageAvatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0256

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageUsername$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0258

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageTag$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0259

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageTimestamp$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a024e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageText$delegate:Lkotlin/properties/ReadOnlyProperty;

    invoke-static {}, Lrx/subjects/BehaviorSubject;->f0()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->newFontScaleSubject:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getThemePureEvil()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->pureEvilEasterEggSubject:Lrx/subjects/BehaviorSubject;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->pureEvilEasterEggCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->holyLightEasterEggCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lcom/discord/utilities/view/ToastManager;

    invoke-direct {v0}, Lcom/discord/utilities/view/ToastManager;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->toastManager:Lcom/discord/utilities/view/ToastManager;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->configureUI(Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V

    return-void
.end method

.method public static final synthetic access$getFontScaleString(Lcom/discord/widgets/settings/WidgetSettingsAppearance;IZ)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScaleString(IZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFontScalingText$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Landroid/widget/TextView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScalingText()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getHolyLightEasterEggCounter$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->holyLightEasterEggCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method public static final synthetic access$getNewFontScaleSubject$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->newFontScaleSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getPureEvilEasterEggCounter$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->pureEvilEasterEggCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method public static final synthetic access$getPureEvilEasterEggSubject$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Lrx/subjects/BehaviorSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->pureEvilEasterEggSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getThemeDarkCs$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeDarkCs()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getThemeHolyLightView$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeHolyLightView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getThemeLightCs$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeLightCs()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getToastManager$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)Lcom/discord/utilities/view/ToastManager;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->toastManager:Lcom/discord/utilities/view/ToastManager;

    return-object p0
.end method

.method public static final synthetic access$setToastManager$p(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Lcom/discord/utilities/view/ToastManager;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->toastManager:Lcom/discord/utilities/view/ToastManager;

    return-void
.end method

.method public static final synthetic access$showHolyLight(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->showHolyLight()V

    return-void
.end method

.method public static final synthetic access$tryEnableTorchMode(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->tryEnableTorchMode(Z)V

    return-void
.end method

.method private final configureFontScalingUI(I)V
    .locals 3

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/discord/utilities/font/FontUtils;->INSTANCE:Lcom/discord/utilities/font/FontUtils;

    invoke-virtual {v2, v1}, Lcom/discord/utilities/font/FontUtils;->getSystemFontScaleInt(Landroid/content/ContentResolver;)I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScalingText()Landroid/widget/TextView;

    move-result-object v2

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-direct {p0, v1, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScaleString(IZ)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScalingSeekbar()Landroid/widget/SeekBar;

    move-result-object p1

    add-int/lit8 v1, v1, -0x50

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void
.end method

.method private final configureThemeOption(Lcom/discord/views/CheckedSetting;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureThemeOption$1;

    invoke-direct {v0, p0, p2}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureThemeOption$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V
    .locals 5

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->getCurrentTheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "light"

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeLightCs()Lcom/discord/views/CheckedSetting;

    move-result-object v2

    xor-int/lit8 v3, v0, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/discord/views/CheckedSetting;->g(ZZ)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeLightCs()Lcom/discord/views/CheckedSetting;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->configureThemeOption(Lcom/discord/views/CheckedSetting;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeLightCs()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeDarkCs()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/discord/views/CheckedSetting;->g(ZZ)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeDarkCs()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    const-string v1, "dark"

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->configureThemeOption(Lcom/discord/views/CheckedSetting;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeDarkCs()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemePureEvilSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->getCurrentTheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pureEvil"

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/discord/views/CheckedSetting;->g(ZZ)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemePureEvilSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->getCanSeePureEvil()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemePureEvilSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$3;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$3;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->getFontScale()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->configureFontScalingUI(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScalingSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$4;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$4;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScalingReset()Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$5;->INSTANCE:Lcom/discord/widgets/settings/WidgetSettingsAppearance$configureUI$5;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->setupMessage(Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V

    return-void
.end method

.method private final getFontScaleString(IZ)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x29

    const-string v1, "% ("

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f12002c

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const p1, 0x7f12002b

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static synthetic getFontScaleString$default(Lcom/discord/widgets/settings/WidgetSettingsAppearance;IZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScaleString(IZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final getFontScalingReset()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->fontScalingReset$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFontScalingSeekbar()Landroid/widget/SeekBar;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->fontScalingSeekbar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    return-object v0
.end method

.method private final getFontScalingText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->fontScalingText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMessageAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageAvatar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getMessageTag()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageTag$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getMessageText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMessageTimestamp()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageTimestamp$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMessageUsername()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->messageUsername$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSyncSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->syncSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getThemeDarkCs()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themeDarkCs$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getThemeHolyLightView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themeHolyLightView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getThemeLightCs()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themeLightCs$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getThemePureEvilSwitch()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->themePureEvilSwitch$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->Companion:Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method

.method private final setupMessage(Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;)V
    .locals 7

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getMessageAvatar()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object v1

    const v2, 0x7f07006b

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x18

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Lcom/discord/models/domain/ModelUser;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getMessageTag()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getMessageUsername()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->getMeUser()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getMessageTimestamp()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/discord/utilities/time/TimeUtils;->toReadableTimeString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getMessageText()Landroid/widget/TextView;

    move-result-object p1

    const v0, 0x7f12198c    # 1.9419993E38f

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final showHolyLight()V
    .locals 12

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->toastManager:Lcom/discord/utilities/view/ToastManager;

    const v2, 0x7f121827

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Lf/a/b/p;->e(Landroid/content/Context;IILcom/discord/utilities/view/ToastManager;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->tryEnableTorchMode(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getThemeHolyLightView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-static {v1, v2, v0}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .timer(3, TimeUnit.SECONDS)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    new-instance v9, Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$showHolyLight$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method private final tryEnableTorchMode(Z)V
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "camera"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    check-cast v0, Landroid/hardware/camera2/CameraManager;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v0}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1, p1}, Landroid/hardware/camera2/CameraManager;->setTorchMode(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const-string v1, "Unable to turn on flashlight"

    invoke-virtual {v0, v1, p1}, Lcom/discord/app/AppLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0294

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const p1, 0x7f12010e

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    const p1, 0x7f121971

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->toastManager:Lcom/discord/utilities/view/ToastManager;

    invoke-virtual {v0}, Lcom/discord/utilities/view/ToastManager;->close()V

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getFontScalingSeekbar()Landroid/widget/SeekBar;

    move-result-object p1

    const/16 v0, 0x46

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    sget-object v0, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model;->Companion:Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model$Companion;

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->pureEvilEasterEggSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v1

    const-string v2, "pureEvilEasterEggSubject.distinctUntilChanged()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$Model$Companion;->get(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    new-instance v9, Lcom/discord/widgets/settings/WidgetSettingsAppearance$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsAppearance;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->newFontScaleSubject:Lrx/subjects/BehaviorSubject;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x190

    invoke-virtual {v0, v4, v5, v3}, Lrx/Observable;->o(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v3, "newFontScaleSubject\n    \u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/WidgetSettingsAppearance;

    sget-object v10, Lcom/discord/widgets/settings/WidgetSettingsAppearance$onViewBoundOrOnResume$2;->INSTANCE:Lcom/discord/widgets/settings/WidgetSettingsAppearance$onViewBoundOrOnResume$2;

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getSyncSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getThemeSync()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance;->getSyncSwitch()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsAppearance$onViewBoundOrOnResume$3;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/WidgetSettingsAppearance$onViewBoundOrOnResume$3;-><init>(Lcom/discord/stores/StoreUserSettings;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    return-void
.end method
