.class public final Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;
.super Ljava/lang/Object;
.source "WidgetSettings.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettings;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettings;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 13

    new-instance p1, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;-><init>(Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->invoke(Z)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->uploadSystemLog()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v0, "javaClass.name"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$2;-><init>(Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;)V

    new-instance v9, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$3;

    invoke-direct {v9, p1}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$3;-><init>(Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;)V

    new-instance v10, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$4;

    invoke-direct {v10, p1}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$4;-><init>(Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;)V

    const/4 v7, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method
