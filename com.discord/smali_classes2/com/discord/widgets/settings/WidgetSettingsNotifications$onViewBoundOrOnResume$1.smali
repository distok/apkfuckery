.class public final Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;
.super Lx/m/c/k;
.source "WidgetSettingsNotifications.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettingsNotifications;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->invoke(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;)V
    .locals 2

    const-string v0, "settings"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotifications;->access$getSettingsWrap$p(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotifications;->access$getEnabledToggle$p(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotifications;->access$getEnabledInAppToggle$p(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isEnabledInApp()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotifications;->access$getBlinkToggle$p(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isDisableBlink()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotifications;->access$getVibrateToggle$p(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isDisableVibrate()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsNotifications$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsNotifications;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettingsNotifications;->access$getSoundsToggle$p(Lcom/discord/widgets/settings/WidgetSettingsNotifications;)Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/fcm/NotificationClient$SettingsV2;->isDisableSound()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    return-void
.end method
