.class public final Lcom/discord/widgets/settings/WidgetSettingsPrivacy;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsPrivacy.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/WidgetSettingsPrivacy$LocalState;,
        Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;,
        Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;


# instance fields
.field private final dataAllowScreenreaderDetection$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dataBasicService$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dataPersonalization$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dataPrivacyControls$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dataRequest$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dataRequestLink$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final dataStatistics$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final defaultGuildsRestrictedView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private dialog:Landroidx/appcompat/app/AlertDialog;

.field private final dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final explicitContentRadio0$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final explicitContentRadio1$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final explicitContentRadio2$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final explicitContentRadios$delegate:Lkotlin/Lazy;

.field private final friendSourceRadios$delegate:Lkotlin/Lazy;

.field private final privacyFriendSource0$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privacyFriendSource1$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final privacyFriendSource2$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private radioManagerExplicit:Lcom/discord/views/RadioManager;

.field private final userSettings:Lcom/discord/stores/StoreUserSettings;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0xf

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v3, "explicitContentRadio0"

    const-string v4, "getExplicitContentRadio0()Lcom/discord/views/CheckedSetting;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "explicitContentRadio1"

    const-string v7, "getExplicitContentRadio1()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "explicitContentRadio2"

    const-string v7, "getExplicitContentRadio2()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "privacyFriendSource0"

    const-string v7, "getPrivacyFriendSource0()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "privacyFriendSource1"

    const-string v7, "getPrivacyFriendSource1()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "privacyFriendSource2"

    const-string v7, "getPrivacyFriendSource2()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "defaultGuildsRestrictedView"

    const-string v7, "getDefaultGuildsRestrictedView()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataPrivacyControls"

    const-string v7, "getDataPrivacyControls()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataStatistics"

    const-string v7, "getDataStatistics()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataPersonalization"

    const-string v7, "getDataPersonalization()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataBasicService"

    const-string v7, "getDataBasicService()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataAllowScreenreaderDetection"

    const-string v7, "getDataAllowScreenreaderDetection()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataRequest"

    const-string v7, "getDataRequest()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dataRequestLink"

    const-string v7, "getDataRequestLink()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const-string v6, "dimmer"

    const-string v7, "getDimmer()Lcom/discord/utilities/dimmer/DimmerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->Companion:Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a09c3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadio0$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09c4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadio1$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09c5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadio2$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$explicitContentRadios$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$explicitContentRadios$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadios$delegate:Lkotlin/Lazy;

    const v0, 0x7f0a09c6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->privacyFriendSource0$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09c7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->privacyFriendSource1$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09c8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->privacyFriendSource2$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$friendSourceRadios$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$friendSourceRadios$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->friendSourceRadios$delegate:Lkotlin/Lazy;

    const v0, 0x7f0a09c0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->defaultGuildsRestrictedView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09bf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataPrivacyControls$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09cd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataStatistics$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09c9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataPersonalization$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09be

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataBasicService$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09cc

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataAllowScreenreaderDetection$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09ca

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataRequest$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a09cb

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataRequestLink$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a035a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->userSettings:Lcom/discord/stores/StoreUserSettings;

    return-void
.end method

.method public static final synthetic access$configureRequestDataButton(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureRequestDataButton(ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureUI(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V

    return-void
.end method

.method public static final synthetic access$confirmConsent(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;Lkotlin/jvm/functions/Function5;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->confirmConsent(Landroid/content/Context;Lkotlin/jvm/functions/Function5;)V

    return-void
.end method

.method public static final synthetic access$getDataAllowScreenreaderDetection$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataAllowScreenreaderDetection()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDataPersonalization$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataPersonalization()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDataStatistics$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataStatistics()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDimmer$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/utilities/dimmer/DimmerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getExplicitContentRadio0$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadio0()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getExplicitContentRadio1$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadio1()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getExplicitContentRadio2$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadio2()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPrivacyFriendSource0$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getPrivacyFriendSource0()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPrivacyFriendSource1$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getPrivacyFriendSource1()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPrivacyFriendSource2$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/views/CheckedSetting;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getPrivacyFriendSource2()Lcom/discord/views/CheckedSetting;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getUserSettings$p(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)Lcom/discord/stores/StoreUserSettings;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->userSettings:Lcom/discord/stores/StoreUserSettings;

    return-object p0
.end method

.method public static final synthetic access$onRequestDataClick(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;Lcom/discord/utilities/rest/RestAPI$HarvestState;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->onRequestDataClick(Landroid/content/Context;Lcom/discord/utilities/rest/RestAPI$HarvestState;)V

    return-void
.end method

.method public static final synthetic access$showDefaultGuildsRestrictedExistingServers(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->showDefaultGuildsRestrictedExistingServers(Z)V

    return-void
.end method

.method public static final synthetic access$toggleConsent(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;ZLjava/lang/String;Lcom/discord/views/CheckedSetting;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->toggleConsent(ZLjava/lang/String;Lcom/discord/views/CheckedSetting;)V

    return-void
.end method

.method public static final synthetic access$updateDefaultGuildsRestricted(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->updateDefaultGuildsRestricted(ZZ)V

    return-void
.end method

.method public static final synthetic access$updateFriendSourceFlags(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->updateFriendSourceFlags(IZ)V

    return-void
.end method

.method private final configureDefaultGuildsRestricted(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDefaultGuildsRestrictedView()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getDefaultRestrictedGuilds()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDefaultGuildsRestrictedView()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureDefaultGuildsRestricted$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureDefaultGuildsRestricted$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureExplicitContentRadio(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;Lcom/discord/views/CheckedSetting;I)V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureExplicitContentRadio$1;

    invoke-direct {v0, p0, p3}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureExplicitContentRadio$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;I)V

    invoke-virtual {p2, v0}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->radioManagerExplicit:Lcom/discord/views/RadioManager;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getExplicitContentFilter()I

    move-result p1

    if-ne p1, p3, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->radioManagerExplicit:Lcom/discord/views/RadioManager;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Lcom/discord/views/RadioManager;->a(Landroid/widget/Checkable;)V

    :cond_0
    return-void
.end method

.method private final configureFriendSourceRadio(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureFriendSourceRadio$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureFriendSourceRadio$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    invoke-virtual {v0, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    new-instance v3, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureFriendSourceRadio$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureFriendSourceRadio$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    invoke-virtual {v0, v3}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    new-instance v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureFriendSourceRadio$3;

    invoke-direct {v4, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureFriendSourceRadio$3;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    invoke-virtual {v0, v4}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;->isAll()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/views/CheckedSetting;

    invoke-virtual {v4, v0}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/views/CheckedSetting;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;->isMutualFriends()Z

    move-result v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-nez v5, :cond_3

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v5, 0x1

    :goto_3
    invoke-virtual {v4, v5}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/views/CheckedSetting;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getFriendSourceFlags()Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUserSettings$FriendSourceFlags;->isMutualGuilds()Z

    move-result p1

    goto :goto_4

    :cond_4
    const/4 p1, 0x0

    :goto_4
    if-nez p1, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    const/4 v1, 0x1

    :cond_6
    invoke-virtual {v3, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    return-void
.end method

.method private final configurePrivacyControls(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "context ?: return"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataPrivacyControls()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataStatistics()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getConsents()Lcom/discord/models/domain/Consents;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/Consents;->getUsageStatistics()Lcom/discord/models/domain/Consent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/Consent;->getConsented()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataStatistics()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configurePrivacyControls$1;

    invoke-direct {v2, p0, v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configurePrivacyControls$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataPersonalization()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getConsents()Lcom/discord/models/domain/Consents;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/Consents;->getPersonalization()Lcom/discord/models/domain/Consent;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/Consent;->getConsented()Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataPersonalization()Lcom/discord/views/CheckedSetting;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configurePrivacyControls$2;

    invoke-direct {v1, p0, v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configurePrivacyControls$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;)V

    invoke-virtual {p1, v1}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    :cond_0
    return-void
.end method

.method private final configureRequestDataButton(ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataRequest()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureDefaultGuildsRestricted(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureFriendSourceRadio(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadios()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureExplicitContentRadio(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;Lcom/discord/views/CheckedSetting;I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadios()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureExplicitContentRadio(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;Lcom/discord/views/CheckedSetting;I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadios()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    invoke-direct {p0, p1, v0, v1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureExplicitContentRadio(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;Lcom/discord/views/CheckedSetting;I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configurePrivacyControls(Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getMe()Lcom/discord/models/domain/ModelUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelUser;->isVerified()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->getHarvestState()Lcom/discord/utilities/rest/RestAPI$HarvestState;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureRequestDataButton(ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V

    return-void
.end method

.method private final confirmConsent(Landroid/content/Context;Lkotlin/jvm/functions/Function5;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function5<",
            "-",
            "Landroidx/appcompat/app/AlertDialog;",
            "-",
            "Landroid/widget/TextView;",
            "-",
            "Landroid/widget/TextView;",
            "-",
            "Landroid/widget/TextView;",
            "-",
            "Landroid/widget/TextView;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const v0, 0x7f0d012d

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    const-string v1, "AlertDialog.Builder(this\u2026(false)\n        .create()"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0a0b60

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v1, "dialogView.findViewById(\u2026alog_confirmation_header)"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0a0b61

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const-string v1, "dialogView.findViewById(\u2026dialog_confirmation_text)"

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0a0b5e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const-string v1, "dialogView.findViewById(\u2026alog_confirmation_cancel)"

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0a0b5f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const-string v0, "dialogView.findViewById(\u2026log_confirmation_confirm)"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p2

    move-object v2, p1

    invoke-interface/range {v1 .. v6}, Lkotlin/jvm/functions/Function5;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private final getDataAllowScreenreaderDetection()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataAllowScreenreaderDetection$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getDataBasicService()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataBasicService$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getDataPersonalization()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataPersonalization$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getDataPrivacyControls()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataPrivacyControls$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDataRequest()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataRequest$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getDataRequestLink()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataRequestLink$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDataStatistics()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dataStatistics$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getDefaultGuildsRestrictedView()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->defaultGuildsRestrictedView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getDimmer()Lcom/discord/utilities/dimmer/DimmerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    return-object v0
.end method

.method private final getExplicitContentRadio0()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadio0$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getExplicitContentRadio1()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadio1$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getExplicitContentRadio2()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadio2$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getExplicitContentRadios()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/views/CheckedSetting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->explicitContentRadios$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getFriendSourceRadios()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/views/CheckedSetting;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->friendSourceRadios$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getPrivacyFriendSource0()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->privacyFriendSource0$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getPrivacyFriendSource1()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->privacyFriendSource1$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getPrivacyFriendSource2()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->privacyFriendSource2$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method public static final launch(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->Companion:Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;

    invoke-virtual {v0, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Companion;->launch(Landroid/content/Context;)V

    return-void
.end method

.method private final onRequestDataClick(Landroid/content/Context;Lcom/discord/utilities/rest/RestAPI$HarvestState;)V
    .locals 4

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;)V

    new-instance v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$2;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;)V

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$3;

    invoke-direct {v2, p0, p1, v1, v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$3;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$2;Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$1;)V

    new-instance v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$4;

    invoke-direct {v0, p0, p1, v2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$4;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$3;)V

    instance-of p1, p2, Lcom/discord/utilities/rest/RestAPI$HarvestState$NeverRequested;

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$4;->invoke()V

    goto :goto_0

    :cond_0
    instance-of p1, p2, Lcom/discord/utilities/rest/RestAPI$HarvestState$LastRequested;

    if-eqz p1, :cond_2

    check-cast p2, Lcom/discord/utilities/rest/RestAPI$HarvestState$LastRequested;

    invoke-virtual {p2}, Lcom/discord/utilities/rest/RestAPI$HarvestState$LastRequested;->getData()Lcom/discord/models/domain/Harvest;

    move-result-object p1

    const-wide/16 v1, 0x0

    const/4 p2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, p2, v3}, Lcom/discord/models/domain/Harvest;->canRequest$default(Lcom/discord/models/domain/Harvest;JILjava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onRequestDataClick$4;->invoke()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/discord/models/domain/Harvest;->nextAvailableRequestInMillis()J

    move-result-wide p1

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->showNextAvailableRequestAlert(J)V

    :cond_2
    :goto_0
    return-void
.end method

.method private final showDefaultGuildsRestrictedExistingServers(Z)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d02b0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a09c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$showDefaultGuildsRestrictedExistingServers$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$showDefaultGuildsRestrictedExistingServers$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Z)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a09c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$showDefaultGuildsRestrictedExistingServers$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$showDefaultGuildsRestrictedExistingServers$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Z)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dialog:Landroidx/appcompat/app/AlertDialog;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatDialog;->dismiss()V

    :cond_0
    new-instance p1, Landroidx/appcompat/app/AlertDialog$Builder;

    const-string v1, "view"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dialog:Landroidx/appcompat/app/AlertDialog;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_1
    return-void
.end method

.method private final showNextAvailableRequestAlert(J)V
    .locals 8

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    if-eqz v7, :cond_0

    const-string v0, "context ?: return"

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-wide v1, p1

    move-object v3, v7

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;JLandroid/content/Context;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const p2, 0x7f1205bc

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {v7, p2, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026ted_status_note, dateStr)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    invoke-direct {p2, v7}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f1205d3

    invoke-virtual {p2, v0}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setTitle(I)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object p1

    const p2, 0x7f1211ee

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, p2, v1, v0, v1}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->setPositiveButton$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;ILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    const-string v0, "parentFragmentManager"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Builder;->show(Landroidx/fragment/app/FragmentManager;)V

    :cond_0
    return-void
.end method

.method private final toggleConsent(ZLjava/lang/String;Lcom/discord/views/CheckedSetting;)V
    .locals 4

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->setConsent(ZLjava/lang/String;)Lrx/Observable;

    move-result-object p2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p2, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p2

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object v0

    new-instance v1, Lf/a/b/j0;

    const-wide/16 v2, 0x64

    invoke-direct {v1, v0, v2, v3}, Lf/a/b/j0;-><init>(Lcom/discord/utilities/dimmer/DimmerView;J)V

    invoke-virtual {p2, v1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p2

    sget-object v0, Lf/a/b/r;->a:Lf/a/b/r;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$toggleConsent$1;

    invoke-direct {v2, p3}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$toggleConsent$1;-><init>(Lcom/discord/views/CheckedSetting;)V

    new-instance v3, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$toggleConsent$2;

    invoke-direct {v3, p3, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$toggleConsent$2;-><init>(Lcom/discord/views/CheckedSetting;Z)V

    invoke-virtual {v0, v1, v2, v3}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object p1

    invoke-virtual {p2, p1}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method

.method private final updateDefaultGuildsRestricted(ZZ)V
    .locals 11

    const/4 v0, 0x0

    if-nez p2, :cond_0

    new-instance p2, Lg0/l/e/j;

    invoke-direct {p2, v0}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    sget-object p2, Lx/h/n;->d:Lx/h/n;

    new-instance v1, Lg0/l/e/j;

    invoke-direct {v1, p2}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    move-object p2, v1

    goto :goto_0

    :cond_1
    sget-object p2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p2}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/stores/StoreGuilds;->observeGuilds()Lrx/Observable;

    move-result-object p2

    sget-object v1, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$updateDefaultGuildsRestricted$1;->INSTANCE:Lcom/discord/widgets/settings/WidgetSettingsPrivacy$updateDefaultGuildsRestricted$1;

    invoke-virtual {p2, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p2

    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lrx/Observable;->U(I)Lrx/Observable;

    move-result-object p2

    const-string/jumbo v1, "when {\n      !applyToExi\u2026 }\n    }\n        .take(1)"

    invoke-static {p2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p2

    const/4 v1, 0x2

    invoke-static {p2, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$updateDefaultGuildsRestricted$2;

    invoke-direct {v8, p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$updateDefaultGuildsRestricted$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Z)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dialog:Landroidx/appcompat/app/AlertDialog;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroidx/appcompat/app/AppCompatDialog;->dismiss()V

    :cond_2
    return-void
.end method

.method private final updateFriendSourceFlags(IZ)V
    .locals 6

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    invoke-virtual {v0}, Lcom/discord/views/CheckedSetting;->isChecked()Z

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/views/CheckedSetting;

    invoke-virtual {v2}, Lcom/discord/views/CheckedSetting;->isChecked()Z

    move-result v2

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getFriendSourceRadios()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/views/CheckedSetting;

    invoke-virtual {v4}, Lcom/discord/views/CheckedSetting;->isChecked()Z

    move-result v4

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-nez v4, :cond_1

    :cond_0
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/discord/stores/StoreUserSettings;->setFriendSourceFlags(Lcom/discord/app/AppActivity;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02af

    return v0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->dialog:Landroidx/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 21

    move-object/from16 v0, p0

    const-string v1, "view"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v4}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const v5, 0x7f121971

    invoke-virtual {v0, v5}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const v5, 0x7f12146d

    invoke-virtual {v0, v5}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    new-instance v5, Lcom/discord/views/RadioManager;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getExplicitContentRadios()Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/discord/views/RadioManager;-><init>(Ljava/util/List;)V

    iput-object v5, v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->radioManagerExplicit:Lcom/discord/views/RadioManager;

    sget-object v5, Lf/a/b/g;->a:Lf/a/b/g;

    const-wide v6, 0x53d1eac657L

    invoke-virtual {v5, v6, v7, v4}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f1205c3

    new-array v10, v3, [Ljava/lang/Object;

    aput-object v8, v10, v1

    invoke-virtual {v0, v9, v10}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const-string v8, "getString(\n        R.str\u2026yControlsArticleUrl\n    )"

    invoke-static {v12, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataPersonalization()Lcom/discord/views/CheckedSetting;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v11

    const-string v9, "view.context"

    invoke-static {v11, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1c

    const/16 v17, 0x0

    invoke-static/range {v11 .. v17}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v8, v10, v3}, Lcom/discord/views/CheckedSetting;->h(Ljava/lang/CharSequence;Z)V

    const-wide v10, 0x53d3d0de1cL

    invoke-virtual {v5, v10, v11, v4}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f1205be

    new-array v11, v3, [Ljava/lang/Object;

    aput-object v8, v11, v1

    invoke-virtual {v0, v10, v11}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    const-string v8, "getString(\n        R.str\u2026yTrackingArticleUrl\n    )"

    invoke-static {v13, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataAllowScreenreaderDetection()Lcom/discord/views/CheckedSetting;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1c

    const/16 v18, 0x0

    invoke-static/range {v12 .. v18}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v8, v10, v3}, Lcom/discord/views/CheckedSetting;->h(Ljava/lang/CharSequence;Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataAllowScreenreaderDetection()Lcom/discord/views/CheckedSetting;

    move-result-object v8

    new-instance v10, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBound$$inlined$apply$lambda$1;

    invoke-direct {v10, v8, v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBound$$inlined$apply$lambda$1;-><init>(Lcom/discord/views/CheckedSetting;Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    invoke-virtual {v8, v10}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    const-wide v10, 0x53d1ea1c57L

    invoke-virtual {v5, v10, v11, v4}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const v10, 0x7f1205c0

    invoke-virtual {v0, v10}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.data_\u2026trols_basic_service_note)"

    invoke-static {v10, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v12, 0x28

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v8, 0x29

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x4

    const-string v13, "(onClick)"

    invoke-static {v10, v13, v11, v1, v12}, Lx/s/m;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataBasicService()Lcom/discord/views/CheckedSetting;

    move-result-object v10

    const v11, 0x7f0a093b

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_0

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataBasicService()Lcom/discord/views/CheckedSetting;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1c

    const/16 v20, 0x0

    invoke-static/range {v14 .. v20}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v10, v11, v3}, Lcom/discord/views/CheckedSetting;->h(Ljava/lang/CharSequence;Z)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataBasicService()Lcom/discord/views/CheckedSetting;

    move-result-object v10

    sget-object v11, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBound$2;->INSTANCE:Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBound$2;

    invoke-virtual {v10, v11}, Lcom/discord/views/CheckedSetting;->e(Landroid/view/View$OnClickListener;)V

    const-wide v10, 0x53d1e9852cL

    invoke-virtual {v5, v10, v11, v4}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataRequestLink()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v14, 0x5b

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v14, 0x7f121170

    invoke-virtual {v0, v14}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "]("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1c

    const/16 v18, 0x0

    invoke-static/range {v12 .. v18}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataRequestLink()Landroid/widget/TextView;

    move-result-object v8

    new-instance v11, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBound$3;

    invoke-direct {v11, v10}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBound$3;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v8, 0x7f1205d1

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v5, v6, v7, v4}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v1

    invoke-virtual {v0, v8, v10}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const-string v1, "getString(\n        R.str\u2026A_PRIVACY_CONTROLS)\n    )"

    invoke-static {v12, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->getDataStatistics()Lcom/discord/views/CheckedSetting;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1c

    const/16 v17, 0x0

    invoke-static/range {v11 .. v17}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/discord/views/CheckedSetting;->h(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    sget-object v0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model;->Companion:Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model$Companion;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$Model$Companion;->get()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    new-instance v9, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBoundOrOnResume$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x1e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->userSettings:Lcom/discord/stores/StoreUserSettings;

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->observeAllowAccessibilityDetection()Lrx/Observable;

    move-result-object v0

    const-string v3, "userSettings\n        .ob\u2026wAccessibilityDetection()"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p0, v1, v2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    new-instance v10, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBoundOrOnResume$2;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;)V

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
