.class public final Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;
.super Ljava/lang/Object;
.source "WidgetMuteSettingsSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $appContext:Landroid/content/Context;

.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;->this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    iput-object p2, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;->$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;->this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->access$getViewModel$p(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;->$appContext:Landroid/content/Context;

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v1, v2, v0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->selectMuteDurationMs(JLandroid/content/Context;)V

    return-void
.end method
