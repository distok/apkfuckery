.class public final Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;
.super Lf/a/b/l0;
.source "MuteSettingsSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;,
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;,
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;,
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;,
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;,
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;,
        Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Companion;

.field public static final MUTE_DURATION_ALWAYS:J


# instance fields
.field private final clock:Lcom/discord/utilities/time/Clock;

.field private final config:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;

.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final storeStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private final storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->Companion:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;Lrx/Observable;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/utilities/time/Clock;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreUserGuildSettings;",
            "Lcom/discord/utilities/time/Clock;",
            ")V"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeStateObservable"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeUserGuildSettings"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->config:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;

    iput-object p2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeStateObservable:Lrx/Observable;

    iput-object p3, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    iput-object p4, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static {p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    new-instance v6, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$1;-><init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->handleStoreState(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;)V

    return-void
.end method

.method private final emitDismissEvent()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    sget-object v1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$Dismiss;->INSTANCE:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$Dismiss;

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;)V
    .locals 13
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;->getGuild()Lcom/discord/models/domain/ModelGuild;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v1

    if-eqz v0, :cond_0

    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GUILD:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    :goto_0
    move-object v4, v2

    goto :goto_1

    :cond_0
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isDM()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->DM:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isGroup()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GROUP_DM:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isCategory()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->CATEGORY:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GUILD_CHANNEL:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->UNKNOWN:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    goto :goto_0

    :goto_1
    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->UNKNOWN:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    if-ne v4, v2, :cond_5

    sget-object p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Failure;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_5
    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_8

    if-eq v2, v3, :cond_7

    const/4 v0, 0x2

    if-eq v2, v0, :cond_7

    const/4 v0, 0x3

    if-eq v2, v0, :cond_6

    const/4 v0, 0x4

    if-eq v2, v0, :cond_7

    const-string v0, ""

    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_8
    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v5, v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;->getGuildNotificationSettings()Lcom/discord/models/domain/ModelNotificationSettings;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v6, 0x0

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelNotificationSettings;->getChannelOverrides()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;->getChannel()Lcom/discord/models/domain/ModelChannel;

    move-result-object v9

    if-eqz v9, :cond_a

    invoke-virtual {v9}, Lcom/discord/models/domain/ModelChannel;->getId()J

    move-result-wide v9

    const-string v11, "channelOverride"

    invoke-static {v8, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->getChannelId()J

    move-result-wide v11

    cmp-long v8, v9, v11

    if-nez v8, :cond_a

    const/4 v8, 0x1

    goto :goto_3

    :cond_a
    const/4 v8, 0x0

    :goto_3
    if-eqz v8, :cond_9

    goto :goto_4

    :cond_b
    move-object v7, v6

    :goto_4
    check-cast v7, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;

    goto :goto_5

    :cond_c
    move-object v7, v6

    :goto_5
    new-instance v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    const-string v8, "subtitle"

    invoke-static {v5, v8}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelChannel;->isPrivate()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;->getGuildNotificationSettings()Lcom/discord/models/domain/ModelNotificationSettings;

    move-result-object p1

    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelNotificationSettings;->isMuted()Z

    move-result p1

    if-ne p1, v3, :cond_d

    const/4 p1, 0x1

    goto :goto_6

    :cond_d
    const/4 p1, 0x0

    :goto_6
    if-eqz v7, :cond_e

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->isMuted()Z

    move-result v1

    if-ne v1, v3, :cond_e

    const/4 v2, 0x1

    :cond_e
    if-eqz v7, :cond_f

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->getMuteEndTime()Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    goto :goto_7

    :cond_f
    move-object v8, v6

    :goto_7
    if-eqz v7, :cond_10

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelNotificationSettings$ChannelOverride;->getMessageNotifications()I

    move-result v1

    goto :goto_8

    :cond_10
    sget v1, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_UNSET:I

    :goto_8
    move v9, v1

    move-object v3, v0

    move v6, v2

    move v7, p1

    invoke-direct/range {v3 .. v9}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;-><init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;Ljava/lang/String;ZZLjava/lang/String;I)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final emitNotificationSettingsEvent(J)V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$NavigateToChannelSettings;

    invoke-direct {v1, p1, p2}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$NavigateToChannelSettings;-><init>(J)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public final getClock()Lcom/discord/utilities/time/Clock;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    return-object v0
.end method

.method public final getConfig()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->config:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;

    return-object v0
.end method

.method public final getStoreStateObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeStateObservable:Lrx/Observable;

    return-object v0
.end method

.method public final getStoreUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onChannelSettingsSelected()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->config:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;

    instance-of v1, v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;->getChannelId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->emitNotificationSettingsEvent(J)V

    :cond_0
    return-void
.end method

.method public final selectMuteDurationMs(JLandroid/content/Context;)V
    .locals 7
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "appContext"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmp-long v3, p1, v1

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->clock:Lcom/discord/utilities/time/Clock;

    invoke-interface {v1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v1

    add-long/2addr v1, p1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, v0, p2, v0}, Lcom/discord/utilities/time/TimeUtils;->toUTCDateTime$default(Ljava/lang/Long;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/discord/models/domain/ModelMuteConfig;

    invoke-direct {v0, p1}, Lcom/discord/models/domain/ModelMuteConfig;-><init>(Ljava/lang/String;)V

    :goto_0
    move-object v6, v0

    iget-object p1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->config:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;

    instance-of p2, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Guild;

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    check-cast p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Guild;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Guild;->getGuildId()J

    move-result-wide v3

    const/4 v5, 0x1

    move-object v2, p3

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreUserGuildSettings;->setGuildMuted(Landroid/content/Context;JZLcom/discord/models/domain/ModelMuteConfig;)V

    goto :goto_1

    :cond_1
    instance-of p2, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    check-cast p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;->getChannelId()J

    move-result-wide v3

    const/4 v5, 0x1

    move-object v2, p3

    invoke-virtual/range {v1 .. v6}, Lcom/discord/stores/StoreUserGuildSettings;->setChannelMuted(Landroid/content/Context;JZLcom/discord/models/domain/ModelMuteConfig;)V

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->emitDismissEvent()V

    return-void
.end method

.method public final unmute(Landroid/content/Context;)V
    .locals 8
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "appContext"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->config:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;

    instance-of v1, v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    check-cast v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;->getChannelId()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/discord/stores/StoreUserGuildSettings;->setChannelMuted(Landroid/content/Context;JZLcom/discord/models/domain/ModelMuteConfig;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->emitDismissEvent()V

    :cond_0
    return-void
.end method
