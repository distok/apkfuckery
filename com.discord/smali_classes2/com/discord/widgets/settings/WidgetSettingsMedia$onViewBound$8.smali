.class public final Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;
.super Lx/m/c/k;
.source "WidgetSettingsMedia.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettingsMedia;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->invoke(Ljava/lang/Integer;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Integer;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    const-string v1, "currentStickerAnimationSettings"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-static {v2}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->access$getStickersAlwaysAnimateCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->access$configureStickerAnimationRadio(Lcom/discord/widgets/settings/WidgetSettingsMedia;ILcom/discord/views/CheckedSetting;I)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-static {v2}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->access$getStickersAnimateOnInteractionCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->access$configureStickerAnimationRadio(Lcom/discord/widgets/settings/WidgetSettingsMedia;ILcom/discord/views/CheckedSetting;I)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$onViewBound$8;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-static {v1}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->access$getStickersNeverAnimateCS$p(Lcom/discord/widgets/settings/WidgetSettingsMedia;)Lcom/discord/views/CheckedSetting;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2}, Lcom/discord/widgets/settings/WidgetSettingsMedia;->access$configureStickerAnimationRadio(Lcom/discord/widgets/settings/WidgetSettingsMedia;ILcom/discord/views/CheckedSetting;I)V

    return-void
.end method
