.class public Lcom/discord/widgets/settings/WidgetSettingsLanguage;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsLanguage.java"


# static fields
.field public static final REQUEST_CODE_USER_LOCALE:I = 0xfa5


# instance fields
.field private language:Landroid/view/View;

.field private languageFlag:Landroid/widget/ImageView;

.field private languageText:Landroid/widget/TextView;

.field private syncSwitch:Lcom/discord/views/CheckedSetting;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    return-void
.end method

.method private configureUI(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->languageText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->getLocaleResId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->languageFlag:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->getLocaleFlagResId(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    return-void
.end method

.method public static synthetic f(Lcom/discord/widgets/settings/WidgetSettingsLanguage;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->configureUI(Ljava/lang/String;)V

    return-void
.end method

.method public static getAsStringInLocale(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, -0x1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v0, "zh-TW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v1, 0x1c

    goto/16 :goto_0

    :sswitch_1
    const-string/jumbo v0, "zh-CN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "sv-SE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "pt-BR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_4
    const-string v0, "es-ES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "en-GB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "vi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "uk"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "tr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "th"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_9

    goto/16 :goto_0

    :cond_9
    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "ru"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_a

    goto/16 :goto_0

    :cond_a
    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_b
    const-string v0, "ro"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "pl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_c

    goto/16 :goto_0

    :cond_c
    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "no"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_d

    goto/16 :goto_0

    :cond_d
    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "nl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_e

    goto/16 :goto_0

    :cond_e
    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "lt"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_f

    goto/16 :goto_0

    :cond_f
    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_10
    const-string v0, "ko"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_10

    goto/16 :goto_0

    :cond_10
    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_11

    goto/16 :goto_0

    :cond_11
    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_12
    const-string v0, "it"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_12

    goto/16 :goto_0

    :cond_12
    const/16 v1, 0xa

    goto/16 :goto_0

    :sswitch_13
    const-string v0, "hu"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_13

    goto/16 :goto_0

    :cond_13
    const/16 v1, 0x9

    goto/16 :goto_0

    :sswitch_14
    const-string v0, "hr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_14

    goto/16 :goto_0

    :cond_14
    const/16 v1, 0x8

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "hi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_15

    goto :goto_0

    :cond_15
    const/4 v1, 0x7

    goto :goto_0

    :sswitch_16
    const-string v0, "fr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_16

    goto :goto_0

    :cond_16
    const/4 v1, 0x6

    goto :goto_0

    :sswitch_17
    const-string v0, "fi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_17

    goto :goto_0

    :cond_17
    const/4 v1, 0x5

    goto :goto_0

    :sswitch_18
    const-string v0, "el"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_18

    goto :goto_0

    :cond_18
    const/4 v1, 0x4

    goto :goto_0

    :sswitch_19
    const-string v0, "de"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_19

    goto :goto_0

    :cond_19
    const/4 v1, 0x3

    goto :goto_0

    :sswitch_1a
    const-string v0, "da"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1a

    goto :goto_0

    :cond_1a
    const/4 v1, 0x2

    goto :goto_0

    :sswitch_1b
    const-string v0, "cs"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1b

    goto :goto_0

    :cond_1b
    const/4 v1, 0x1

    goto :goto_0

    :sswitch_1c
    const-string v0, "bg"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1c

    goto :goto_0

    :cond_1c
    const/4 v1, 0x0

    :goto_0
    packed-switch v1, :pswitch_data_0

    const-string p0, "English, USA"

    return-object p0

    :pswitch_0
    const-string/jumbo p0, "\u7e41\u9ad4\u4e2d\u6587"

    return-object p0

    :pswitch_1
    const-string/jumbo p0, "\u4e2d\u6587"

    return-object p0

    :pswitch_2
    const-string p0, "Svenska"

    return-object p0

    :pswitch_3
    const-string p0, "Portugu\u00eas do Brasil"

    return-object p0

    :pswitch_4
    const-string p0, "Espa\u00f1ol"

    return-object p0

    :pswitch_5
    const-string p0, "English, UK"

    return-object p0

    :pswitch_6
    const-string p0, "Ti\u1ebfng Vi\u1ec7t"

    return-object p0

    :pswitch_7
    const-string/jumbo p0, "\u0423\u043a\u0440\u0430\u0457\u043d\u0441\u044c\u043a\u0430"

    return-object p0

    :pswitch_8
    const-string p0, "T\u00fcrk\u00e7e"

    return-object p0

    :pswitch_9
    const-string/jumbo p0, "\u0e44\u0e17\u0e22"

    return-object p0

    :pswitch_a
    const-string/jumbo p0, "\u0420\u0443\u0441\u0441\u043a\u0438\u0439"

    return-object p0

    :pswitch_b
    const-string p0, "Rom\u00e2n\u0103"

    return-object p0

    :pswitch_c
    const-string p0, "Polski"

    return-object p0

    :pswitch_d
    const-string p0, "Norsk"

    return-object p0

    :pswitch_e
    const-string p0, "Nederlands"

    return-object p0

    :pswitch_f
    const-string p0, "Lietuvi\u0161kai"

    return-object p0

    :pswitch_10
    const-string/jumbo p0, "\ud55c\uad6d\uc5b4"

    return-object p0

    :pswitch_11
    const-string/jumbo p0, "\u65e5\u672c\u8a9e"

    return-object p0

    :pswitch_12
    const-string p0, "Italiano"

    return-object p0

    :pswitch_13
    const-string p0, "Magyar"

    return-object p0

    :pswitch_14
    const-string p0, "Hrvatski"

    return-object p0

    :pswitch_15
    const-string/jumbo p0, "\u0939\u093f\u0902\u0926\u0940"

    return-object p0

    :pswitch_16
    const-string p0, "Fran\u00e7ais"

    return-object p0

    :pswitch_17
    const-string p0, "Suomi"

    return-object p0

    :pswitch_18
    const-string/jumbo p0, "\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ac"

    return-object p0

    :pswitch_19
    const-string p0, "Deutsch"

    return-object p0

    :pswitch_1a
    const-string p0, "Dansk"

    return-object p0

    :pswitch_1b
    const-string/jumbo p0, "\u010ce\u0161tina"

    return-object p0

    :pswitch_1c
    const-string/jumbo p0, "\u0431\u044a\u043b\u0433\u0430\u0440\u0441\u043a\u0438"

    return-object p0

    :sswitch_data_0
    .sparse-switch
        0xc45 -> :sswitch_1c
        0xc70 -> :sswitch_1b
        0xc7d -> :sswitch_1a
        0xc81 -> :sswitch_19
        0xca7 -> :sswitch_18
        0xcc3 -> :sswitch_17
        0xccc -> :sswitch_16
        0xd01 -> :sswitch_15
        0xd0a -> :sswitch_14
        0xd0d -> :sswitch_13
        0xd2b -> :sswitch_12
        0xd37 -> :sswitch_11
        0xd64 -> :sswitch_10
        0xd88 -> :sswitch_f
        0xdbe -> :sswitch_e
        0xdc1 -> :sswitch_d
        0xdfc -> :sswitch_c
        0xe3d -> :sswitch_b
        0xe43 -> :sswitch_a
        0xe74 -> :sswitch_9
        0xe7e -> :sswitch_8
        0xe96 -> :sswitch_7
        0xeb3 -> :sswitch_6
        0x5c1f87f -> :sswitch_5
        0x5c43e2d -> :sswitch_4
        0x65fb4b9 -> :sswitch_3
        0x68ae5fc -> :sswitch_2
        0x6e72b6a -> :sswitch_1
        0x6e72d82 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getLocaleFlagResId(Ljava/lang/String;)I
    .locals 2
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, -0x1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v0, "zh-TW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v1, 0x1c

    goto/16 :goto_0

    :sswitch_1
    const-string/jumbo v0, "zh-CN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "sv-SE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "pt-BR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_4
    const-string v0, "es-ES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "en-GB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "vi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "uk"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "tr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "th"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_9

    goto/16 :goto_0

    :cond_9
    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "ru"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_a

    goto/16 :goto_0

    :cond_a
    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_b
    const-string v0, "ro"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "pl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_c

    goto/16 :goto_0

    :cond_c
    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "no"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_d

    goto/16 :goto_0

    :cond_d
    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "nl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_e

    goto/16 :goto_0

    :cond_e
    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "lt"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_f

    goto/16 :goto_0

    :cond_f
    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_10
    const-string v0, "ko"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_10

    goto/16 :goto_0

    :cond_10
    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_11

    goto/16 :goto_0

    :cond_11
    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_12
    const-string v0, "it"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_12

    goto/16 :goto_0

    :cond_12
    const/16 v1, 0xa

    goto/16 :goto_0

    :sswitch_13
    const-string v0, "hu"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_13

    goto/16 :goto_0

    :cond_13
    const/16 v1, 0x9

    goto/16 :goto_0

    :sswitch_14
    const-string v0, "hr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_14

    goto/16 :goto_0

    :cond_14
    const/16 v1, 0x8

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "hi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_15

    goto :goto_0

    :cond_15
    const/4 v1, 0x7

    goto :goto_0

    :sswitch_16
    const-string v0, "fr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_16

    goto :goto_0

    :cond_16
    const/4 v1, 0x6

    goto :goto_0

    :sswitch_17
    const-string v0, "fi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_17

    goto :goto_0

    :cond_17
    const/4 v1, 0x5

    goto :goto_0

    :sswitch_18
    const-string v0, "el"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_18

    goto :goto_0

    :cond_18
    const/4 v1, 0x4

    goto :goto_0

    :sswitch_19
    const-string v0, "de"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_19

    goto :goto_0

    :cond_19
    const/4 v1, 0x3

    goto :goto_0

    :sswitch_1a
    const-string v0, "da"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1a

    goto :goto_0

    :cond_1a
    const/4 v1, 0x2

    goto :goto_0

    :sswitch_1b
    const-string v0, "cs"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1b

    goto :goto_0

    :cond_1b
    const/4 v1, 0x1

    goto :goto_0

    :sswitch_1c
    const-string v0, "bg"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1c

    goto :goto_0

    :cond_1c
    const/4 v1, 0x0

    :goto_0
    packed-switch v1, :pswitch_data_0

    const p0, 0x7f0804a9

    return p0

    :pswitch_0
    const p0, 0x7f0804c5

    return p0

    :pswitch_1
    const p0, 0x7f0804c4

    return p0

    :pswitch_2
    const p0, 0x7f0804be

    return p0

    :pswitch_3
    const p0, 0x7f0804ba

    return p0

    :pswitch_4
    const p0, 0x7f0804aa

    return p0

    :pswitch_5
    const p0, 0x7f0804a8

    return p0

    :pswitch_6
    const p0, 0x7f0804c3

    return p0

    :pswitch_7
    const p0, 0x7f0804c1

    return p0

    :pswitch_8
    const p0, 0x7f0804c0

    return p0

    :pswitch_9
    const p0, 0x7f0804bf

    return p0

    :pswitch_a
    const p0, 0x7f0804bc

    return p0

    :pswitch_b
    const p0, 0x7f0804bb

    return p0

    :pswitch_c
    const p0, 0x7f0804b9

    return p0

    :pswitch_d
    const p0, 0x7f0804b8

    return p0

    :pswitch_e
    const p0, 0x7f0804b7

    return p0

    :pswitch_f
    const p0, 0x7f0804b6

    return p0

    :pswitch_10
    const p0, 0x7f0804b5

    return p0

    :pswitch_11
    const p0, 0x7f0804b4

    return p0

    :pswitch_12
    const p0, 0x7f0804b3

    return p0

    :pswitch_13
    const p0, 0x7f0804b2

    return p0

    :pswitch_14
    const p0, 0x7f0804b1

    return p0

    :pswitch_15
    const p0, 0x7f0804af

    return p0

    :pswitch_16
    const p0, 0x7f0804ad

    return p0

    :pswitch_17
    const p0, 0x7f0804ac

    return p0

    :pswitch_18
    const p0, 0x7f0804a7

    return p0

    :pswitch_19
    const p0, 0x7f0804a6

    return p0

    :pswitch_1a
    const p0, 0x7f0804a5

    return p0

    :pswitch_1b
    const p0, 0x7f0804a4

    return p0

    :pswitch_1c
    const p0, 0x7f0804a3

    return p0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc45 -> :sswitch_1c
        0xc70 -> :sswitch_1b
        0xc7d -> :sswitch_1a
        0xc81 -> :sswitch_19
        0xca7 -> :sswitch_18
        0xcc3 -> :sswitch_17
        0xccc -> :sswitch_16
        0xd01 -> :sswitch_15
        0xd0a -> :sswitch_14
        0xd0d -> :sswitch_13
        0xd2b -> :sswitch_12
        0xd37 -> :sswitch_11
        0xd64 -> :sswitch_10
        0xd88 -> :sswitch_f
        0xdbe -> :sswitch_e
        0xdc1 -> :sswitch_d
        0xdfc -> :sswitch_c
        0xe3d -> :sswitch_b
        0xe43 -> :sswitch_a
        0xe74 -> :sswitch_9
        0xe7e -> :sswitch_8
        0xe96 -> :sswitch_7
        0xeb3 -> :sswitch_6
        0x5c1f87f -> :sswitch_5
        0x5c43e2d -> :sswitch_4
        0x65fb4b9 -> :sswitch_3
        0x68ae5fc -> :sswitch_2
        0x6e72b6a -> :sswitch_1
        0x6e72d82 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getLocaleResId(Ljava/lang/String;)I
    .locals 2
    .annotation build Landroidx/annotation/StringRes;
    .end annotation

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, -0x1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string/jumbo v0, "zh-TW"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    goto/16 :goto_0

    :cond_0
    const/16 v1, 0x1c

    goto/16 :goto_0

    :sswitch_1
    const-string/jumbo v0, "zh-CN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    goto/16 :goto_0

    :cond_1
    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "sv-SE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "pt-BR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_4
    const-string v0, "es-ES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "en-GB"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "vi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_7
    const-string v0, "uk"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_8
    const-string v0, "tr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_9
    const-string v0, "th"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_9

    goto/16 :goto_0

    :cond_9
    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_a
    const-string v0, "ru"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_a

    goto/16 :goto_0

    :cond_a
    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_b
    const-string v0, "ro"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_c
    const-string v0, "pl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_c

    goto/16 :goto_0

    :cond_c
    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_d
    const-string v0, "no"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_d

    goto/16 :goto_0

    :cond_d
    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_e
    const-string v0, "nl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_e

    goto/16 :goto_0

    :cond_e
    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v0, "lt"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_f

    goto/16 :goto_0

    :cond_f
    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_10
    const-string v0, "ko"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_10

    goto/16 :goto_0

    :cond_10
    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_11
    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_11

    goto/16 :goto_0

    :cond_11
    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_12
    const-string v0, "it"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_12

    goto/16 :goto_0

    :cond_12
    const/16 v1, 0xa

    goto/16 :goto_0

    :sswitch_13
    const-string v0, "hu"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_13

    goto/16 :goto_0

    :cond_13
    const/16 v1, 0x9

    goto/16 :goto_0

    :sswitch_14
    const-string v0, "hr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_14

    goto/16 :goto_0

    :cond_14
    const/16 v1, 0x8

    goto/16 :goto_0

    :sswitch_15
    const-string v0, "hi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_15

    goto :goto_0

    :cond_15
    const/4 v1, 0x7

    goto :goto_0

    :sswitch_16
    const-string v0, "fr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_16

    goto :goto_0

    :cond_16
    const/4 v1, 0x6

    goto :goto_0

    :sswitch_17
    const-string v0, "fi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_17

    goto :goto_0

    :cond_17
    const/4 v1, 0x5

    goto :goto_0

    :sswitch_18
    const-string v0, "el"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_18

    goto :goto_0

    :cond_18
    const/4 v1, 0x4

    goto :goto_0

    :sswitch_19
    const-string v0, "de"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_19

    goto :goto_0

    :cond_19
    const/4 v1, 0x3

    goto :goto_0

    :sswitch_1a
    const-string v0, "da"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1a

    goto :goto_0

    :cond_1a
    const/4 v1, 0x2

    goto :goto_0

    :sswitch_1b
    const-string v0, "cs"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1b

    goto :goto_0

    :cond_1b
    const/4 v1, 0x1

    goto :goto_0

    :sswitch_1c
    const-string v0, "bg"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1c

    goto :goto_0

    :cond_1c
    const/4 v1, 0x0

    :goto_0
    packed-switch v1, :pswitch_data_0

    const p0, 0x7f1206b7

    return p0

    :pswitch_0
    const p0, 0x7f121ade

    return p0

    :pswitch_1
    const p0, 0x7f121add

    return p0

    :pswitch_2
    const p0, 0x7f1217b4

    return p0

    :pswitch_3
    const p0, 0x7f12149a

    return p0

    :pswitch_4
    const p0, 0x7f1206ef

    return p0

    :pswitch_5
    const p0, 0x7f1206b6

    return p0

    :pswitch_6
    const p0, 0x7f121a2a

    return p0

    :pswitch_7
    const p0, 0x7f1218b3

    return p0

    :pswitch_8
    const p0, 0x7f121868

    return p0

    :pswitch_9
    const p0, 0x7f121824

    return p0

    :pswitch_a
    const p0, 0x7f121581

    return p0

    :pswitch_b
    const p0, 0x7f12154a

    return p0

    :pswitch_c
    const p0, 0x7f1212c5

    return p0

    :pswitch_d
    const p0, 0x7f1210eb

    return p0

    :pswitch_e
    const p0, 0x7f1210ea

    return p0

    :pswitch_f
    const p0, 0x7f120f70

    return p0

    :pswitch_10
    const p0, 0x7f120efd

    return p0

    :pswitch_11
    const p0, 0x7f120e8e

    return p0

    :pswitch_12
    const p0, 0x7f120e8c

    return p0

    :pswitch_13
    const p0, 0x7f120ce7

    return p0

    :pswitch_14
    const p0, 0x7f120ce6

    return p0

    :pswitch_15
    const p0, 0x7f120cd8

    return p0

    :pswitch_16
    const p0, 0x7f120847

    return p0

    :pswitch_17
    const p0, 0x7f120745

    return p0

    :pswitch_18
    const p0, 0x7f120667

    return p0

    :pswitch_19
    const p0, 0x7f1205d6

    return p0

    :pswitch_1a
    const p0, 0x7f1205b7

    return p0

    :pswitch_1b
    const p0, 0x7f1205a2

    return p0

    :pswitch_1c
    const p0, 0x7f1202b9

    return p0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc45 -> :sswitch_1c
        0xc70 -> :sswitch_1b
        0xc7d -> :sswitch_1a
        0xc81 -> :sswitch_19
        0xca7 -> :sswitch_18
        0xcc3 -> :sswitch_17
        0xccc -> :sswitch_16
        0xd01 -> :sswitch_15
        0xd0a -> :sswitch_14
        0xd0d -> :sswitch_13
        0xd2b -> :sswitch_12
        0xd37 -> :sswitch_11
        0xd64 -> :sswitch_10
        0xd88 -> :sswitch_f
        0xdbe -> :sswitch_e
        0xdc1 -> :sswitch_d
        0xdfc -> :sswitch_c
        0xe3d -> :sswitch_b
        0xe43 -> :sswitch_a
        0xe74 -> :sswitch_9
        0xe7e -> :sswitch_8
        0xe96 -> :sswitch_7
        0xeb3 -> :sswitch_6
        0x5c1f87f -> :sswitch_5
        0x5c43e2d -> :sswitch_4
        0x65fb4b9 -> :sswitch_3
        0x68ae5fc -> :sswitch_2
        0x6e72b6a -> :sswitch_1
        0x6e72d82 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static launch(Landroid/content/Context;)V
    .locals 3

    const-class v0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {p0, v0, v1, v2}, Lf/a/b/m;->e(Landroid/content/Context;Ljava/lang/Class;Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02a8

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    new-instance p2, Lf/a/o/f/b;

    invoke-direct {p2, p0}, Lf/a/o/f/b;-><init>(Lcom/discord/widgets/settings/WidgetSettingsLanguage;)V

    invoke-static {p1, p3, p2}, Lcom/discord/widgets/settings/WidgetSettingsLanguageSelect;->handleResult(ILandroid/content/Intent;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const v0, 0x7f121971

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const v0, 0x7f120f00

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled()Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f0a09a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->language:Landroid/view/View;

    const v0, 0x7f0a09a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->languageText:Landroid/widget/TextView;

    const v0, 0x7f0a09a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->languageFlag:Landroid/widget/ImageView;

    const v0, 0x7f0a09ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/discord/views/CheckedSetting;

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->syncSwitch:Lcom/discord/views/CheckedSetting;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    invoke-static {}, Lcom/discord/stores/StoreStream;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getLocaleObservable()Lrx/Observable;

    move-result-object v1

    invoke-static {p0}, Lf/a/b/r;->p(Lcom/discord/app/AppComponent;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lf/a/o/f/d;

    invoke-direct {v2, p0}, Lf/a/o/f/d;-><init>(Lcom/discord/widgets/settings/WidgetSettingsLanguage;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3}, Lf/a/b/r;->g(Lrx/functions/Action1;Ljava/lang/Class;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->syncSwitch:Lcom/discord/views/CheckedSetting;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getLocaleSync()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->syncSwitch:Lcom/discord/views/CheckedSetting;

    new-instance v2, Lf/a/o/f/a;

    invoke-direct {v2, v0}, Lf/a/o/f/a;-><init>(Lcom/discord/stores/StoreUserSettings;)V

    invoke-virtual {v1, v2}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsLanguage;->language:Landroid/view/View;

    if-eqz v0, :cond_1

    new-instance v1, Lf/a/o/f/c;

    invoke-direct {v1, p0}, Lf/a/o/f/c;-><init>(Lcom/discord/widgets/settings/WidgetSettingsLanguage;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method
