.class public final Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetChoosePlanAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;->onConfigure(ILcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $data:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;

.field public final synthetic this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;->$data:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;->this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;

    invoke-static {p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;->access$getAdapter$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    move-result-object p1

    invoke-static {p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->access$getOnClickPlan$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;)Lkotlin/jvm/functions/Function3;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;->$data:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;

    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->getPurchaseSku()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;->$data:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;

    check-cast v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    invoke-virtual {v1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->getOldSkuName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader$onConfigure$1;->$data:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;

    check-cast v2, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    invoke-virtual {v2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->getUpgradeSkuDetails()Lcom/android/billingclient/api/SkuDetails;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
