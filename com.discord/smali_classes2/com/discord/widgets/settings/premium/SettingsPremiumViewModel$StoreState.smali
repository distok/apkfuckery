.class public final Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;
.super Ljava/lang/Object;
.source "SettingsPremiumViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

.field private final entitlementState:Lcom/discord/stores/StoreEntitlements$State;

.field private final guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

.field private final paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

.field private final purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

.field private final renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

.field private final skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

.field private final subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)V
    .locals 1

    const-string v0, "paymentSourcesState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionsState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "entitlementState"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildSubscriptionState"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renewalInvoicePreviewFetch"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentInvoicePreviewFetch"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetailsState"

    invoke-static {p7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchaseState"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    iput-object p3, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    iput-object p4, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iput-object p5, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    iput-object p6, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    iput-object p7, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    iput-object p8, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;ILjava/lang/Object;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->copy(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object v0
.end method

.method public final component3()Lcom/discord/stores/StoreEntitlements$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    return-object v0
.end method

.method public final component4()Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-object v0
.end method

.method public final component5()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    return-object v0
.end method

.method public final component6()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    return-object v0
.end method

.method public final component7()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    return-object v0
.end method

.method public final component8()Lcom/discord/stores/StoreGooglePlayPurchases$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;
    .locals 10

    const-string v0, "paymentSourcesState"

    move-object v2, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionsState"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "entitlementState"

    move-object v4, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildSubscriptionState"

    move-object v5, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renewalInvoicePreviewFetch"

    move-object v6, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentInvoicePreviewFetch"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetailsState"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchaseState"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;-><init>(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    iget-object p1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentInvoicePreviewFetch()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    return-object v0
.end method

.method public final getEntitlementState()Lcom/discord/stores/StoreEntitlements$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    return-object v0
.end method

.method public final getGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    return-object v0
.end method

.method public final getPaymentSourcesState()Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    return-object v0
.end method

.method public final getPurchaseState()Lcom/discord/stores/StoreGooglePlayPurchases$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    return-object v0
.end method

.method public final getRenewalInvoicePreviewFetch()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    return-object v0
.end method

.method public final getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    return-object v0
.end method

.method public final getSubscriptionsState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(paymentSourcesState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->paymentSourcesState:Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionsState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", entitlementState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->entitlementState:Lcom/discord/stores/StoreEntitlements$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildSubscriptionState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->guildSubscriptionState:Lcom/discord/stores/StorePremiumGuildSubscription$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", renewalInvoicePreviewFetch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->renewalInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentInvoicePreviewFetch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->currentInvoicePreviewFetch:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skuDetailsState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchaseState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->purchaseState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
