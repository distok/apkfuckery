.class public final Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;
.source "SettingsPremiumViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

.field private final entitlements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;"
        }
    .end annotation
.end field

.field private final guildSubscriptions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation
.end field

.field private final hasAnyPremiumGuildSubscriptions:Z

.field private final isBusy:Z

.field private final paymentSources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPaymentSource;",
            ">;"
        }
    .end annotation
.end field

.field private final premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

.field private final purchases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation
.end field

.field private final renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

.field private final skuDetails:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelPaymentSource;",
            ">;Z",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;Z",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)V"
        }
    .end annotation

    const-string v0, "paymentSources"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "entitlements"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildSubscriptions"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchases"

    invoke-static {p10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    iput-boolean p3, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    iput-object p4, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    iput-object p5, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    iput-boolean p6, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    iput-object p7, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    iput-object p8, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    iput-object p9, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    iput-object p10, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    goto :goto_9

    :cond_9
    move-object/from16 v1, p10

    :goto_9
    move-object p1, v2

    move-object p2, v3

    move p3, v4

    move-object p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->copy(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/ModelSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    return-object v0
.end method

.method public final component10()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPaymentSource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    return v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    return v0
.end method

.method public final component7()Lcom/discord/models/domain/billing/ModelInvoicePreview;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    return-object v0
.end method

.method public final component8()Lcom/discord/models/domain/billing/ModelInvoicePreview;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    return-object v0
.end method

.method public final component9()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/ModelPaymentSource;",
            ">;Z",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;Z",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;",
            "Lcom/discord/models/domain/billing/ModelInvoicePreview;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;"
        }
    .end annotation

    const-string v0, "paymentSources"

    move-object v3, p2

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "entitlements"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildSubscriptions"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchases"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    move-object v1, v0

    move-object v2, p1

    move v4, p3

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;-><init>(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    iget-boolean v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    iget-boolean v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    iget-object p1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    return-object v0
.end method

.method public final getEntitlements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    return-object v0
.end method

.method public final getGuildSubscriptions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    return-object v0
.end method

.method public final getHasAnyPremiumGuildSubscriptions()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    return v0
.end method

.method public final getPaymentSources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelPaymentSource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    return-object v0
.end method

.method public final getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    return-object v0
.end method

.method public final getPurchases()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    return-object v0
.end method

.method public final getRenewalInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    return-object v0
.end method

.method public final getSkuDetails()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    move v3, v2

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method

.method public final isBusy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(premiumSubscription="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->premiumSubscription:Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentSources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->paymentSources:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isBusy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", entitlements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->entitlements:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", guildSubscriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->guildSubscriptions:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasAnyPremiumGuildSubscriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->hasAnyPremiumGuildSubscriptions:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", renewalInvoicePreview="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->renewalInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentInvoicePreview="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->currentInvoicePreview:Lcom/discord/models/domain/billing/ModelInvoicePreview;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skuDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchases="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->A(Ljava/lang/StringBuilder;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
