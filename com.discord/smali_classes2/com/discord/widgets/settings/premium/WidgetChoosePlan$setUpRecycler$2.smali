.class public final Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;
.super Lx/m/c/k;
.source "WidgetChoosePlan.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->setUpRecycler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function3<",
        "Lcom/discord/utilities/billing/GooglePlaySku;",
        "Ljava/lang/String;",
        "Lcom/android/billingclient/api/SkuDetails;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

.field public final synthetic this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlan;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;Lcom/discord/utilities/analytics/Traits$Location;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->$locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/utilities/billing/GooglePlaySku;

    check-cast p2, Ljava/lang/String;

    check-cast p3, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->invoke(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "sku"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v3

    const-string v4, "upgradePrice"

    const-string v5, "fragmentManager"

    const-string v6, "upgradeSkuDetails.price"

    const-string v7, "parentFragmentManager"

    if-eqz v3, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v3

    sget-object v8, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-ne v3, v8, :cond_0

    sget-object v9, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v10, v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->$locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    sget-object v3, Lcom/discord/utilities/analytics/Traits$Subscription;->Companion:Lcom/discord/utilities/analytics/Traits$Subscription$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/discord/utilities/analytics/Traits$Subscription$Companion;->withGatewayPlanId(Ljava/lang/String;)Lcom/discord/utilities/analytics/Traits$Subscription;

    move-result-object v11

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x30

    const/16 v17, 0x0

    const-string v12, "premium_upsell"

    const-string v13, "plan_select"

    invoke-static/range {v9 .. v17}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowStep$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    sget-object v3, Lf/a/a/e/a;->n:Lf/a/a/e/a$b;

    iget-object v8, v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    invoke-virtual {v8}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$1;

    invoke-direct {v7, v0, v1, v2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;)V

    new-instance v9, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$2;

    invoke-direct {v9, v0, v1, v2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onContinueClickListener"

    invoke-static {v7, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onUpgradeClickListener"

    invoke-static {v9, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lf/a/a/e/a;

    invoke-direct {v2}, Lf/a/a/e/a;-><init>()V

    iput-object v7, v2, Lf/a/a/e/a;->d:Lkotlin/jvm/functions/Function0;

    iput-object v9, v2, Lf/a/a/e/a;->e:Lkotlin/jvm/functions/Function0;

    iput-object v10, v2, Lf/a/a/e/a;->f:Ljava/lang/String;

    iput-boolean v1, v2, Lf/a/a/e/a;->g:Z

    const-class v1, Lf/a/a/e/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v8, v1}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v3

    if-eqz v3, :cond_3

    if-eqz p3, :cond_3

    sget-object v8, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v9, v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->$locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    sget-object v3, Lcom/discord/utilities/analytics/Traits$Subscription;->Companion:Lcom/discord/utilities/analytics/Traits$Subscription$Companion;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/discord/utilities/analytics/Traits$Subscription$Companion;->withGatewayPlanId(Ljava/lang/String;)Lcom/discord/utilities/analytics/Traits$Subscription;

    move-result-object v10

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x30

    const/16 v16, 0x0

    const-string/jumbo v11, "yearly_upsell"

    const-string v12, "plan_select"

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowStep$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    sget-object v3, Lf/a/a/e/b;->n:Lf/a/a/e/b$b;

    iget-object v8, v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    invoke-virtual {v8}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v8

    invoke-static {v8, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$3;

    invoke-direct {v7, v0, v1, v2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$3;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;)V

    new-instance v9, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$4;

    invoke-direct {v9, v0, v1, v2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2$4;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v6

    sget-object v11, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v6, v11, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v1

    sget-object v6, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-ne v1, v6, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v8, v5}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "onMonthlyClickListener"

    invoke-static {v7, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "onYearlyClickListener"

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lf/a/a/e/b;

    invoke-direct {v3}, Lf/a/a/e/b;-><init>()V

    iput-object v7, v3, Lf/a/a/e/b;->d:Lkotlin/jvm/functions/Function0;

    iput-object v9, v3, Lf/a/a/e/b;->e:Lkotlin/jvm/functions/Function0;

    iput-object v10, v3, Lf/a/a/e/b;->f:Ljava/lang/String;

    iput-boolean v2, v3, Lf/a/a/e/b;->g:Z

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v4, "ARG_IS_TIER_1"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v3, v2}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-class v1, Lf/a/a/e/b;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v8, v1}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    iget-object v3, v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    invoke-static {v3}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->access$getViewModel$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    move-result-object v3

    iget-object v4, v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;->$locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    const-string v5, "plan_select"

    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->buy(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Ljava/lang/String;)V

    :goto_2
    return-void
.end method
