.class public final Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;
.super Ljava/lang/Object;
.source "SettingsPremiumViewModel.kt"

# interfaces
.implements Lrx/functions/Func7;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;->observeStores(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreGooglePlaySkuDetails;Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/utilities/rest/RestAPI;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func7<",
        "Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;",
        "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;",
        "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;",
        "Lcom/discord/stores/StoreEntitlements$State;",
        "Lcom/discord/stores/StorePremiumGuildSubscription$State;",
        "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;",
        "Lcom/discord/stores/StoreGooglePlayPurchases$State;",
        "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;

    invoke-direct {v0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;->INSTANCE:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;
    .locals 10

    new-instance v9, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;

    const-string v0, "paymentSourcesState"

    move-object v1, p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;->getSubscriptionsState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v2

    const-string v0, "entitlementsState"

    move-object v3, p4

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildSubscriptionState"

    move-object v4, p5

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;->getInvoicePreviewFetch()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    move-result-object v5

    invoke-virtual {p3}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;->getInvoicePreviewFetch()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    move-result-object v6

    const-string v0, "skuDetailsState"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchaseState"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;-><init>(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)V

    return-object v9
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    check-cast p2, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;

    check-cast p3, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;

    check-cast p4, Lcom/discord/stores/StoreEntitlements$State;

    check-cast p5, Lcom/discord/stores/StorePremiumGuildSubscription$State;

    check-cast p6, Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    check-cast p7, Lcom/discord/stores/StoreGooglePlayPurchases$State;

    invoke-virtual/range {p0 .. p7}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;->call(Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/stores/StorePremiumGuildSubscription$State;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
