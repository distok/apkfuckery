.class public final Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;
.super Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;
.source "WidgetChoosePlanAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Header"
.end annotation


# instance fields
.field private final titleStringResId:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;IILjava/lang/Object;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->copy(I)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    return v0
.end method

.method public final copy(I)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    invoke-direct {v0, p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    iget v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    iget p1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;->get_type()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTitleStringResId()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Header(titleStringResId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;->titleStringResId:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
