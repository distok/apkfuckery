.class public abstract Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;
.super Ljava/lang/Object;
.source "WidgetChoosePlanAdapter.kt"

# interfaces
.implements Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Item"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Divider;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Companion;

.field public static final TYPE_DIVIDER:I = 0x2

.field public static final TYPE_HEADER:I = 0x0

.field public static final TYPE_PLAN:I = 0x1


# instance fields
.field private final _type:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;->Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Companion;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;->_type:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;-><init>(I)V

    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;->_type:I

    return v0
.end method

.method public final get_type()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;->_type:I

    return v0
.end method
