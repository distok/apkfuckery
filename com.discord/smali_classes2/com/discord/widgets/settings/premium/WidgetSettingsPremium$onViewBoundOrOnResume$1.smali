.class public final Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;
.super Lx/m/c/k;
.source "WidgetSettingsPremium.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;->invoke(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    check-cast p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    invoke-static {v0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->access$showContent(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    invoke-static {p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->access$showLoadingUI(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Failure;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    invoke-static {p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->access$showFailureUI(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    :cond_2
    :goto_0
    return-void
.end method
