.class public final Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;
.super Ljava/lang/Object;
.source "ChoosePlanViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final oldSkuName:Ljava/lang/String;

.field private final viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;)V
    .locals 1

    const-string v0, "viewType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;->viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;->oldSkuName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;->SWITCH_PLANS:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;)V

    return-void
.end method

.method private final observeStores()Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlaySkuDetails()Lcom/discord/stores/StoreGooglePlaySkuDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreGooglePlaySkuDetails;->getState()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreGooglePlayPurchases;->getState()Lrx/Observable;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/stores/StoreGooglePlayPurchases;->getQueryState()Lrx/Observable;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->getSubscriptions()Lrx/Observable;

    move-result-object v0

    sget-object v4, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;

    if-eqz v4, :cond_0

    new-instance v5, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$sam$rx_functions_Func4$0;

    invoke-direct {v5, v4}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$sam$rx_functions_Func4$0;-><init>(Lkotlin/jvm/functions/Function4;)V

    move-object v4, v5

    :cond_0
    check-cast v4, Lrx/functions/Func4;

    invoke-static {v1, v2, v3, v0, v4}, Lrx/Observable;->h(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func4;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026     ::StoreState\n      )"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;->viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;->oldSkuName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lrx/Observable;)V

    return-object p1
.end method
