.class public final Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;
.super Ljava/lang/Object;
.source "ChoosePlanViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreState"
.end annotation


# instance fields
.field private final purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

.field private final purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

.field private final skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

.field private final subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V
    .locals 1

    const-string v0, "skuDetailsState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasesState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasesQueryState"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionsState"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    iput-object p3, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    iput-object p4, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;ILjava/lang/Object;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->copy(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    return-object v0
.end method

.method public final component2()Lcom/discord/stores/StoreGooglePlayPurchases$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    return-object v0
.end method

.method public final component3()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    return-object v0
.end method

.method public final component4()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object v0
.end method

.method public final copy(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;
    .locals 1

    const-string v0, "skuDetailsState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasesState"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchasesQueryState"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptionsState"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;-><init>(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    iget-object p1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPurchasesQueryState()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    return-object v0
.end method

.method public final getPurchasesState()Lcom/discord/stores/StoreGooglePlayPurchases$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    return-object v0
.end method

.method public final getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    return-object v0
.end method

.method public final getSubscriptionsState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "StoreState(skuDetailsState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->skuDetailsState:Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchasesState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesState:Lcom/discord/stores/StoreGooglePlayPurchases$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchasesQueryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptionsState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->subscriptionsState:Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
