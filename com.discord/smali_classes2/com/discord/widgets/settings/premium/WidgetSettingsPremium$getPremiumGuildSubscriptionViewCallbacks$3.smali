.class public final Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;
.super Lx/m/c/k;
.source "WidgetSettingsPremium.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPremiumGuildSubscriptionViewCallbacks(ZLjava/lang/String;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $skuName:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;->$skuName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 13

    sget-object v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    sget-object v2, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;->SWITCH_PLANS:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    iget-object v3, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;->$skuName:Ljava/lang/String;

    new-instance v12, Lcom/discord/utilities/analytics/Traits$Location;

    const-string v5, "User Settings"

    const-string v6, "Discord Nitro"

    const-string v7, "Button CTA"

    const-string v8, "buy"

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v4, v12

    invoke-direct/range {v4 .. v11}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;->launch$default(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;Lcom/discord/app/AppFragment;Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;ILjava/lang/Object;)V

    return-void
.end method
