.class public final Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;
.super Lf/a/b/l0;
.source "ChoosePlanViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;,
        Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;,
        Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;,
        Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

.field private final oldSkuName:Ljava/lang/String;

.field private final viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;",
            "Ljava/lang/String;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "viewType"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeObservable"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->fetchData()V

    invoke-static {p3}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    new-instance v6, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$1;-><init>(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases;->observeEvents()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    new-instance v6, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$2;-><init>(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;)V

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->handleEvent(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->handleStoreState(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;)V

    return-void
.end method

.method public static synthetic buy$default(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->buy(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Ljava/lang/String;)V

    return-void
.end method

.method private final fetchData()V
    .locals 1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->queryPurchases()V

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->querySkuDetails()V

    return-void
.end method

.method private final getCurrentPlanItems(Ljava/util/Map;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    sget-object v0, Lx/h/l;->d:Lx/h/l;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    if-eqz v1, :cond_4

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    invoke-virtual {v2, v1}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->fromSkuName(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/android/billingclient/api/SkuDetails;

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    invoke-virtual {v4}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/android/billingclient/api/SkuDetails;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->getDowngrade(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/billingclient/api/SkuDetails;

    if-eqz v0, :cond_2

    if-eqz v8, :cond_2

    move-object v6, v4

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_3

    if-eqz p1, :cond_3

    move-object v6, v1

    goto :goto_2

    :cond_3
    move-object v6, v3

    :goto_2
    const/4 p1, 0x3

    new-array p1, p1, [Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;

    const/4 v0, 0x0

    new-instance v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    const v2, 0x7f12035a

    invoke-direct {v1, v2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;-><init>(I)V

    aput-object v1, p1, v0

    const/4 v0, 0x1

    new-instance v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    iget-object v7, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    const/4 v9, 0x1

    move-object v3, v1

    invoke-direct/range {v3 .. v9}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;-><init>(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Z)V

    aput-object v1, p1, v0

    const/4 v0, 0x2

    new-instance v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Divider;

    invoke-direct {v1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Divider;-><init>()V

    aput-object v1, p1, v0

    invoke-static {p1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_4
    return-object v0
.end method

.method private final getHeaderForSkuSection(Lcom/discord/utilities/billing/GooglePlaySku$Section;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Section;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Section$Companion;

    invoke-virtual {v1, p1}, Lcom/discord/utilities/billing/GooglePlaySku$Section$Companion;->getHeaderResource(Lcom/discord/utilities/billing/GooglePlaySku$Section;)I

    move-result p1

    invoke-direct {v0, p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;-><init>(I)V

    return-object v0
.end method

.method private final getItemsForViewType(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/16 v0, 0x13

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_9

    if-eq p1, v2, :cond_5

    const/4 v3, 0x2

    if-eq p1, v3, :cond_4

    const/4 v3, 0x3

    if-ne p1, v3, :cond_3

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object p1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_2

    aget-object v5, p1, v4

    invoke-virtual {v5}, Lcom/discord/utilities/billing/GooglePlaySku;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v6

    if-ne v6, p3, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    if-eqz v6, :cond_1

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v3, p2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPlansWithHeaders(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    goto :goto_6

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    invoke-direct {p0, p2, p3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPremiumGuildPlans(Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)Ljava/util/List;

    move-result-object p1

    goto :goto_6

    :cond_5
    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object p1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v0, :cond_8

    aget-object v5, p1, v4

    invoke-virtual {v5}, Lcom/discord/utilities/billing/GooglePlaySku;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v6

    if-ne v6, p3, :cond_6

    invoke-static {v5}, Lcom/discord/utilities/billing/GooglePlaySkuKt;->isTier1(Lcom/discord/utilities/billing/GooglePlaySku;)Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x1

    goto :goto_3

    :cond_6
    const/4 v6, 0x0

    :goto_3
    if-eqz v6, :cond_7

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_8
    invoke-direct {p0, v3, p2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPlansWithHeaders(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    goto :goto_6

    :cond_9
    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object p1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v0, :cond_c

    aget-object v5, p1, v4

    invoke-virtual {v5}, Lcom/discord/utilities/billing/GooglePlaySku;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v6

    if-ne v6, p3, :cond_a

    invoke-static {v5}, Lcom/discord/utilities/billing/GooglePlaySkuKt;->isTier2(Lcom/discord/utilities/billing/GooglePlaySku;)Z

    move-result v6

    if-eqz v6, :cond_a

    const/4 v6, 0x1

    goto :goto_5

    :cond_a
    const/4 v6, 0x0

    :goto_5
    if-eqz v6, :cond_b

    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_c
    invoke-direct {p0, v3, p2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPlansWithHeaders(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    :goto_6
    return-object p1
.end method

.method public static synthetic getItemsForViewType$default(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILjava/lang/Object;)Ljava/util/List;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->MONTHLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getItemsForViewType(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final getPlanForSku(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/billing/GooglePlaySku;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/android/billingclient/api/SkuDetails;

    const/4 v0, 0x0

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    move-object v6, p2

    check-cast v6, Lcom/android/billingclient/api/SkuDetails;

    iget-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getUpgrade()Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v0

    :goto_1
    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v1

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v1, v2, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    if-eqz v2, :cond_4

    if-nez p2, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    return-object v0

    :cond_4
    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    iget-object v5, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p2

    move-object v2, p1

    move-object v4, p1

    invoke-direct/range {v1 .. v9}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;-><init>(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p2

    :cond_5
    return-object v0
.end method

.method private final getPlansWithHeaders(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/utilities/billing/GooglePlaySku;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/discord/utilities/billing/GooglePlaySku$Section;

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Section;->PREMIUM:Lcom/discord/utilities/billing/GooglePlaySku$Section;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Section;->PREMIUM_AND_PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Section;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku$Section;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Section;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/discord/utilities/billing/GooglePlaySku$Section;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v9, v8

    check-cast v9, Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-static {v9}, Lcom/discord/utilities/billing/GooglePlaySkuKt;->getSection(Lcom/discord/utilities/billing/GooglePlaySku;)Lcom/discord/utilities/billing/GooglePlaySku$Section;

    move-result-object v9

    if-ne v9, v5, :cond_1

    const/4 v9, 0x1

    goto :goto_2

    :cond_1
    const/4 v9, 0x0

    :goto_2
    if-eqz v9, :cond_0

    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v1, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v3

    if-eqz v2, :cond_4

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v4}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v1}, Lx/h/f;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-static {v2}, Lcom/discord/utilities/billing/GooglePlaySkuKt;->getSection(Lcom/discord/utilities/billing/GooglePlaySku;)Lcom/discord/utilities/billing/GooglePlaySku$Section;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getHeaderForSkuSection(Lcom/discord/utilities/billing/GooglePlaySku$Section;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Header;

    move-result-object v2

    invoke-static {v2}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-direct {p0, v4, p2}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPlanForSku(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    invoke-static {v2, v3}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    invoke-static {v0}, Lf/h/a/f/f/n/g;->flatten(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getPremiumGuildPlans(Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;",
            ")",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    const/16 v1, 0x13

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    sget-object v4, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    invoke-virtual {v4, v0}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->fromSkuName(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object p2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v1, :cond_2

    aget-object v6, p2, v5

    invoke-virtual {v6}, Lcom/discord/utilities/billing/GooglePlaySku;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlaySku;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v8

    if-ne v7, v8, :cond_0

    invoke-virtual {v6}, Lcom/discord/utilities/billing/GooglePlaySku;->getPremiumSubscriptionCount()I

    move-result v7

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlaySku;->getPremiumSubscriptionCount()I

    move-result v8

    if-le v7, v8, :cond_0

    const/4 v7, 0x1

    goto :goto_1

    :cond_0
    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_1

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v4, p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPlansWithHeaders(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_3
    invoke-static {}, Lcom/discord/utilities/billing/GooglePlaySku;->values()[Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v1, :cond_6

    aget-object v6, v0, v5

    invoke-virtual {v6}, Lcom/discord/utilities/billing/GooglePlaySku;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v7

    if-ne v7, p2, :cond_4

    invoke-virtual {v6}, Lcom/discord/utilities/billing/GooglePlaySku;->getPremiumSubscriptionCount()I

    move-result v7

    if-lez v7, :cond_4

    const/4 v7, 0x1

    goto :goto_3

    :cond_4
    const/4 v7, 0x0

    :goto_3
    if-eqz v7, :cond_5

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_6
    invoke-direct {p0, v4, p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getPlansWithHeaders(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getProrationMode(Lcom/android/billingclient/api/SkuDetails;Lcom/android/billingclient/api/SkuDetails;)I
    .locals 3

    invoke-virtual {p2}, Lcom/android/billingclient/api/SkuDetails;->c()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/android/billingclient/api/SkuDetails;->c()J

    move-result-wide p1

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 p1, 0x4

    return p1

    :cond_1
    const/4 p1, 0x2

    return p1
.end method

.method private final handleEvent(Lcom/discord/stores/StoreGooglePlayPurchases$Event;)V
    .locals 9

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    if-eqz v0, :cond_3

    instance-of v1, p1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v0

    check-cast p1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;->getNewSkuName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/billingclient/api/SkuDetails;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/billingclient/api/SkuDetails;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    sget-object v3, Lcom/discord/utilities/analytics/Traits$Subscription;->Companion:Lcom/discord/utilities/analytics/Traits$Subscription$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;->getNewSkuName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/discord/utilities/analytics/Traits$Subscription$Companion;->withGatewayPlanId(Ljava/lang/String;)Lcom/discord/utilities/analytics/Traits$Subscription;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowCompleted$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$Payment;Lcom/discord/utilities/analytics/Traits$StoreSku;Ljava/lang/String;ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQuerySuccess;->getNewSkuName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1, v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    instance-of v0, p1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;

    if-eqz v0, :cond_3

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    sget-object v0, Lcom/discord/utilities/analytics/Traits$Subscription;->Companion:Lcom/discord/utilities/analytics/Traits$Subscription$Companion;

    check-cast p1, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;

    invoke-virtual {p1}, Lcom/discord/stores/StoreGooglePlayPurchases$Event$PurchaseQueryFailure;->getNewSkuName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/analytics/Traits$Subscription$Companion;->withGatewayPlanId(Ljava/lang/String;)Lcom/discord/utilities/analytics/Traits$Subscription;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowFailed$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;

    const v1, 0x7f1202e6

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;-><init>(I)V

    iget-object p1, p1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v0}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;)V
    .locals 10

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getPurchasesState()Lcom/discord/stores/StoreGooglePlayPurchases$State;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getSubscriptionsState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getCurrentPlanItems(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    move-result-object v1

    check-cast v1, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getItemsForViewType$default(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/util/Map;Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v9, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getPurchasesQueryState()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    move-result-object v3

    invoke-static {v0, v1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->filterNotNull(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getPurchasesState()Lcom/discord/stores/StoreGooglePlayPurchases$State;

    move-result-object v0

    check-cast v0, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->getPurchases()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;->getSubscriptionsState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object p1

    check-cast p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v8

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)V

    goto :goto_0

    :cond_0
    sget-object v9, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;

    :goto_0
    invoke-virtual {p0, v9}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final buy(Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Ljava/lang/String;)V
    .locals 14

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "sku"

    move-object v4, p1

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "locationTrait"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "fromStep"

    move-object/from16 v5, p4

    invoke-static {v5, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v3

    instance-of v6, v3, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    const/4 v7, 0x0

    if-nez v6, :cond_0

    move-object v3, v7

    :cond_0
    check-cast v3, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/billingclient/api/SkuDetails;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/billingclient/api/SkuDetails;

    sget-object v9, Lcom/discord/utilities/analytics/Traits$Subscription;->Companion:Lcom/discord/utilities/analytics/Traits$Subscription$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/discord/utilities/analytics/Traits$Subscription$Companion;->withGatewayPlanId(Ljava/lang/String;)Lcom/discord/utilities/analytics/Traits$Subscription;

    move-result-object v9

    iput-object v2, v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->locationTrait:Lcom/discord/utilities/analytics/Traits$Location;

    if-eqz v8, :cond_8

    if-eqz v1, :cond_1

    if-nez v6, :cond_1

    goto/16 :goto_1

    :cond_1
    new-instance v10, Lcom/android/billingclient/api/BillingFlowParams$a;

    invoke-direct {v10, v7}, Lcom/android/billingclient/api/BillingFlowParams$a;-><init>(Lf/e/a/a/o;)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v11, v10, Lcom/android/billingclient/api/BillingFlowParams$a;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    if-eqz v6, :cond_5

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    xor-int/lit8 v11, v11, 0x1

    if-eqz v11, :cond_5

    sget-object v11, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->getPurchases()Ljava/util/List;

    move-result-object v12

    invoke-virtual {v11, v12, v1}, Lcom/discord/utilities/premium/PremiumUtils;->findPurchaseForSkuName(Ljava/util/List;Ljava/lang/String;)Lcom/android/billingclient/api/Purchase;

    move-result-object v12

    const v13, 0x7f1202e6

    if-nez v12, :cond_2

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object/from16 v2, p3

    move-object v3, v9

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowFailed$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;

    invoke-direct {v2, v13}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;-><init>(I)V

    iget-object v1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_2
    invoke-direct {p0, v6, v8}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->getProrationMode(Lcom/android/billingclient/api/SkuDetails;Lcom/android/billingclient/api/SkuDetails;)I

    move-result v6

    iput v6, v10, Lcom/android/billingclient/api/BillingFlowParams$a;->d:I

    invoke-virtual {v12}, Lcom/android/billingclient/api/Purchase;->a()Ljava/lang/String;

    move-result-object v8

    iput-object v1, v10, Lcom/android/billingclient/api/BillingFlowParams$a;->b:Ljava/lang/String;

    iput-object v8, v10, Lcom/android/billingclient/api/BillingFlowParams$a;->c:Ljava/lang/String;

    const/4 v8, 0x4

    if-ne v6, v8, :cond_5

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v11, v3, v1}, Lcom/discord/utilities/premium/PremiumUtils;->findSubscriptionForSku(Ljava/util/List;Ljava/lang/String;)Lcom/discord/models/domain/ModelSubscription;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, v7

    :goto_0
    if-nez v1, :cond_4

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object/from16 v2, p3

    move-object v3, v9

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowFailed$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;

    invoke-direct {v2, v13}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;-><init>(I)V

    iget-object v1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_4
    sget-object v3, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v3}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v3

    new-instance v6, Lcom/discord/stores/PendingDowngrade;

    invoke-virtual {v12}, Lcom/android/billingclient/api/Purchase;->a()Ljava/lang/String;

    move-result-object v8

    const-string v11, "purchase.purchaseToken"

    invoke-static {v8, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v8, v1, v4}, Lcom/discord/stores/PendingDowngrade;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Lcom/discord/stores/StoreGooglePlayPurchases;->updatePendingDowngrade(Lcom/discord/stores/PendingDowngrade;)V

    :cond_5
    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->getMeInternal$app_productionDiscordExternalRelease()Lcom/discord/models/domain/ModelUser$Me;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    :cond_6
    sget-object v1, Lcom/discord/utilities/users/UserUtils;->INSTANCE:Lcom/discord/utilities/users/UserUtils;

    invoke-virtual {v1, v7}, Lcom/discord/utilities/users/UserUtils;->getObfuscatedUserId(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    iput-object v1, v10, Lcom/android/billingclient/api/BillingFlowParams$a;->a:Ljava/lang/String;

    :cond_7
    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x30

    const/4 v11, 0x0

    const-string v4, "external_payment"

    move-object/from16 v2, p3

    move-object v3, v9

    move-object/from16 v5, p4

    move-object v9, v11

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowStep$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Ljava/lang/String;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;

    invoke-virtual {v10}, Lcom/android/billingclient/api/BillingFlowParams$a;->a()Lcom/android/billingclient/api/BillingFlowParams;

    move-result-object v3

    const-string v4, "builder.build()"

    invoke-static {v3, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;-><init>(Lcom/android/billingclient/api/BillingFlowParams;)V

    iget-object v1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void

    :cond_8
    :goto_1
    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object/from16 v2, p3

    move-object v3, v9

    invoke-static/range {v1 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->paymentFlowFailed$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;Lcom/discord/utilities/analytics/Traits$StoreSku;Lcom/discord/utilities/analytics/Traits$Payment;ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;

    const v3, 0x7f1202e7

    invoke-direct {v2, v3}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;-><init>(I)V

    iget-object v1, v1, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v1, v2}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    :cond_9
    return-void
.end method

.method public final getOldSkuName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->oldSkuName:Ljava/lang/String;

    return-object v0
.end method

.method public final getViewType()Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->viewType:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
