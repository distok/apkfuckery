.class public final synthetic Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;
.super Lx/m/c/i;
.source "ChoosePlanViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function4<",
        "Lcom/discord/stores/StoreGooglePlaySkuDetails$State;",
        "Lcom/discord/stores/StoreGooglePlayPurchases$State;",
        "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;",
        "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
        "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;

    invoke-direct {v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-class v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    const/4 v1, 0x4

    const-string v3, "<init>"

    const-string v4, "<init>(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lx/m/c/i;-><init>(ILjava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p3"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p4"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;-><init>(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    check-cast p2, Lcom/discord/stores/StoreGooglePlayPurchases$State;

    check-cast p3, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    check-cast p4, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory$observeStores$1;->invoke(Lcom/discord/stores/StoreGooglePlaySkuDetails$State;Lcom/discord/stores/StoreGooglePlayPurchases$State;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
