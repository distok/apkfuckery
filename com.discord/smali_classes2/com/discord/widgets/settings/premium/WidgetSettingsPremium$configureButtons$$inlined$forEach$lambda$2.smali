.class public final Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;
.super Ljava/lang/Object;
.source "WidgetSettingsPremium.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureButtons(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $button:Landroid/widget/Button;

.field public final synthetic $premiumSubscription$inlined:Lcom/discord/models/domain/ModelSubscription;

.field public final synthetic this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;


# direct methods
.method public constructor <init>(Landroid/widget/Button;Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->$button:Landroid/widget/Button;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    iput-object p3, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->$premiumSubscription$inlined:Lcom/discord/models/domain/ModelSubscription;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 12

    sget-object v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->$button:Landroid/widget/Button;

    invoke-static {v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->access$getBuyTier1Monthly$p(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)Landroid/widget/Button;

    move-result-object v2

    invoke-static {p1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;->BUY_PREMIUM_TIER_1:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;->BUY_PREMIUM_TIER_2:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    :goto_0
    move-object v2, p1

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->$premiumSubscription$inlined:Lcom/discord/models/domain/ModelSubscription;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    move-object v3, p1

    new-instance p1, Lcom/discord/utilities/analytics/Traits$Location;

    const/4 v9, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    const-string v5, "User Settings"

    const-string v6, "Discord Nitro"

    const-string v7, "Button CTA"

    const-string v8, "buy"

    move-object v4, p1

    invoke-direct/range {v4 .. v11}, Lcom/discord/utilities/analytics/Traits$Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sget-object v4, Lcom/discord/utilities/analytics/Traits$Subscription;->Companion:Lcom/discord/utilities/analytics/Traits$Subscription$Companion;

    iget-object v5, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->$button:Landroid/widget/Button;

    iget-object v6, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;->this$0:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    invoke-static {v6}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->access$getBuyTier1Monthly$p(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)Landroid/widget/Button;

    move-result-object v6

    invoke-static {v5, v6}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-object v5, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v5}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_2
    sget-object v5, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v5}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-virtual {v4, v5}, Lcom/discord/utilities/analytics/Traits$Subscription$Companion;->withGatewayPlanId(Ljava/lang/String;)Lcom/discord/utilities/analytics/Traits$Subscription;

    move-result-object v5

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;->launch(Lcom/discord/app/AppFragment;Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;Lcom/discord/utilities/analytics/Traits$Location;Lcom/discord/utilities/analytics/Traits$Subscription;)V

    return-void
.end method
