.class public final Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;
.super Lf/a/b/l0;
.source "WidgetSettingsGiftingViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;,
        Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;,
        Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;,
        Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private onGiftCodeResolved:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method public constructor <init>(Lrx/Observable;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    sget-object v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$onGiftCodeResolved$1;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$onGiftCodeResolved$1;

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->onGiftCodeResolved:Lkotlin/jvm/functions/Function1;

    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEntitlements()Lcom/discord/stores/StoreEntitlements;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreEntitlements;->fetchMyGiftEntitlements()V

    invoke-static {p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;

    new-instance v8, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getSubscriptions$p(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->handleStoreState(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;)V

    return-void
.end method

.method public static final synthetic access$onHandleGiftCode(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;Lcom/discord/stores/StoreGifting$GiftState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->onHandleGiftCode(Lcom/discord/stores/StoreGifting$GiftState;)V

    return-void
.end method

.method private final buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreEntitlements$State;",
            "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelGift;",
            ">;>;)",
            "Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;"
        }
    .end annotation

    instance-of v0, p1, Lcom/discord/stores/StoreEntitlements$State$Loading;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loading;

    goto :goto_1

    :cond_0
    instance-of v0, p1, Lcom/discord/stores/StoreEntitlements$State$Failure;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Failure;

    goto :goto_1

    :cond_1
    instance-of v0, p1, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_2

    move-object v0, v2

    :cond_2
    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getExpandedSkuIds()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    sget-object v0, Lx/h/n;->d:Lx/h/n;

    :goto_0
    move-object v6, v0

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-nez v1, :cond_4

    move-object v0, v2

    :cond_4
    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getLastCopiedCode()Ljava/lang/String;

    move-result-object v2

    :cond_5
    move-object v8, v2

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    check-cast p1, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    invoke-virtual {p1}, Lcom/discord/stores/StoreEntitlements$State$Loaded;->getGiftableEntitlements()Ljava/util/Map;

    move-result-object v4

    move-object v3, v0

    move-object v5, p2

    move-object v7, p3

    invoke-direct/range {v3 .. v8}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;-><init>(Ljava/util/Map;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Set;Ljava/util/Map;Ljava/lang/String;)V

    move-object p1, v0

    :goto_1
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleStoreState(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;)V
    .locals 10
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getResolvingGiftState()Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;->getEntitlementState()Lcom/discord/stores/StoreEntitlements$State;

    move-result-object v1

    instance-of v3, v1, Lcom/discord/stores/StoreEntitlements$State$Loading;

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    instance-of v3, v1, Lcom/discord/stores/StoreEntitlements$State$Failure;

    if-eqz v3, :cond_3

    :goto_1
    sget-object p1, Lx/h/m;->d:Lx/h/m;

    invoke-direct {p0, v1, v0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;

    move-result-object p1

    goto/16 :goto_5

    :cond_3
    instance-of v3, v1, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    if-eqz v3, :cond_a

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move-object v4, v1

    check-cast v4, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    invoke-virtual {v4}, Lcom/discord/stores/StoreEntitlements$State$Loaded;->getGiftableEntitlements()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelEntitlement;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelEntitlement;->getSkuId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelEntitlement;->getSubscriptionPlan()Lcom/discord/models/domain/ModelSubscriptionPlan;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSubscriptionPlan;->getId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto :goto_3

    :cond_5
    move-object v6, v2

    :goto_3
    invoke-interface {v3, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$StoreState;->getMyResolvedGifts()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelGift;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGift;->getSkuId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGift;->getSkuId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    invoke-virtual {v3}, Lcom/discord/models/domain/ModelGift;->getSkuId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-eqz v4, :cond_7

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    invoke-direct {p0, v1, v0, v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;

    move-result-object p1

    :goto_5
    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void

    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final onHandleGiftCode(Lcom/discord/stores/StoreGifting$GiftState;)V
    .locals 5
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_e

    new-instance v1, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getMyEntitlements()Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lx/h/m;->d:Lx/h/m;

    invoke-direct {v1, v3, v4}, Lcom/discord/stores/StoreEntitlements$State$Loaded;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    instance-of v3, p1, Lcom/discord/stores/StoreGifting$GiftState$Loading;

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    instance-of v3, p1, Lcom/discord/stores/StoreGifting$GiftState$Redeeming;

    if-eqz v3, :cond_2

    :goto_0
    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$Resolving;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$Resolving;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getMyGifts()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;

    move-result-object p1

    goto/16 :goto_5

    :cond_2
    instance-of v3, p1, Lcom/discord/stores/StoreGifting$GiftState$LoadFailed;

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    instance-of v3, p1, Lcom/discord/stores/StoreGifting$GiftState$RedeemedFailed;

    if-eqz v3, :cond_4

    goto :goto_1

    :cond_4
    instance-of v3, p1, Lcom/discord/stores/StoreGifting$GiftState$Invalid;

    if-eqz v3, :cond_5

    :goto_1
    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$Error;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$Error;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getMyGifts()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;

    move-result-object p1

    goto :goto_5

    :cond_5
    instance-of v3, p1, Lcom/discord/stores/StoreGifting$GiftState$Revoking;

    if-eqz v3, :cond_6

    goto :goto_2

    :cond_6
    instance-of v4, p1, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    if-eqz v4, :cond_d

    :goto_2
    instance-of v4, p1, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    if-nez v4, :cond_7

    move-object v4, v2

    goto :goto_3

    :cond_7
    move-object v4, p1

    :goto_3
    check-cast v4, Lcom/discord/stores/StoreGifting$GiftState$Resolved;

    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lcom/discord/stores/StoreGifting$GiftState$Resolved;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v4

    if-eqz v4, :cond_8

    move-object v2, v4

    goto :goto_4

    :cond_8
    if-nez v3, :cond_9

    move-object p1, v2

    :cond_9
    check-cast p1, Lcom/discord/stores/StoreGifting$GiftState$Revoking;

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/discord/stores/StoreGifting$GiftState$Revoking;->getGift()Lcom/discord/models/domain/ModelGift;

    move-result-object v2

    :cond_a
    :goto_4
    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGift;->isClaimedByMe()Z

    move-result p1

    if-eqz p1, :cond_b

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getMyGifts()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;

    move-result-object p1

    goto :goto_5

    :cond_b
    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->onGiftCodeResolved:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelGift;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState$NotResolving;

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getMyGifts()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v1, p1, v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->buildViewState(Lcom/discord/stores/StoreEntitlements$State;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Map;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState;

    move-result-object p1

    :goto_5
    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_c
    return-void

    :cond_d
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_e
    return-void
.end method


# virtual methods
.method public final handleCopyClicked(Ljava/lang/String;)V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "giftCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v6, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;Ljava/util/Map;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Set;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final handleSkuClicked(JLjava/lang/Long;)V
    .locals 9
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->getExpandedSkuIds()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v4, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGifting()Lcom/discord/stores/StoreGifting;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreGifting;->fetchMyGiftsForSku(JLjava/lang/Long;)V

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1b

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;Ljava/util/Map;Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ResolvingGiftState;Ljava/util/Set;Ljava/util/Map;Ljava/lang/String;ILjava/lang/Object;)Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    move-result-object p1

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public onCleared()V
    .locals 1

    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->b()V

    return-void
.end method

.method public final redeemGiftCode(Ljava/lang/String;Lcom/discord/app/AppComponent;)V
    .locals 11
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    const-string v0, "giftCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appComponent"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGifting()Lcom/discord/stores/StoreGifting;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/discord/stores/StoreGifting;->requestGift(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, p2, v1, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;

    const/4 v4, 0x0

    new-instance v5, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$redeemGiftCode$1;

    invoke-direct {v5, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$redeemGiftCode$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$redeemGiftCode$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$redeemGiftCode$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;)V

    const/16 v9, 0x1a

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final setOnGiftCodeResolved(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onGiftCodeResolved"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->onGiftCodeResolved:Lkotlin/jvm/functions/Function1;

    return-void
.end method
