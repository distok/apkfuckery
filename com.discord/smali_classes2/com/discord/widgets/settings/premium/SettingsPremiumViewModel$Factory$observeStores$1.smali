.class public final Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;
.super Ljava/lang/Object;
.source "SettingsPremiumViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;->observeStores(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreGooglePlaySkuDetails;Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/utilities/rest/RestAPI;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic $restAPI:Lcom/discord/utilities/rest/RestAPI;

.field public final synthetic this$0:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;->this$0:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;->$restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;->call(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;->this$0:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;->$restAPI:Lcom/discord/utilities/rest/RestAPI;

    const-string v2, "state"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;->access$getSubscriptionsAndInvoicePreview(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Z)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
