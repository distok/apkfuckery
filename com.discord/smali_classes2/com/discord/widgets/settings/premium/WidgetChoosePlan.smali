.class public final Lcom/discord/widgets/settings/premium/WidgetChoosePlan;
.super Lcom/discord/app/AppFragment;
.source "WidgetChoosePlan.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;

.field public static final REQUEST_CODE_CHOOSE_PLAN:I = 0xfa0

.field public static final RESULT_EXTRA_LOCATION_TRAIT:Ljava/lang/String; = "result_extra_location_trait"

.field public static final RESULT_EXTRA_OLD_SKU_NAME:Ljava/lang/String; = "result_extra_current_sku_name"

.field public static final RESULT_VIEW_TYPE:Ljava/lang/String; = "result_view_type"


# instance fields
.field private adapter:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

.field private final dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptyBody$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emptyContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private planLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private final recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    const-string v3, "recyclerView"

    const-string v4, "getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    const-string v6, "dimmer"

    const-string v7, "getDimmer()Lcom/discord/utilities/dimmer/DimmerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    const-string v6, "emptyContainer"

    const-string v7, "getEmptyContainer()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    const-string v6, "emptyBody"

    const-string v7, "getEmptyBody()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->Companion:Lcom/discord/widgets/settings/premium/WidgetChoosePlan$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a02a1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a035a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a029f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->emptyContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a02a0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->emptyBody$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;)Lkotlin/Unit;
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->configureUI(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;)Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->viewModel:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->handleEvent(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->viewModel:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;)Lkotlin/Unit;
    .locals 5

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    instance-of v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loading;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v0, v2

    goto :goto_1

    :cond_0
    instance-of v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->adapter:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    if-eqz v1, :cond_3

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getEmptyContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->getPurchasesQueryState()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    move-result-object p1

    sget-object v1, Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$InProgress;->INSTANCE:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState$InProgress;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x2

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    const/4 v3, 0x1

    invoke-static {p1, v3, v4, v1, v2}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getDimmer()Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object p1

    invoke-static {p1, v4, v4, v1, v2}, Lcom/discord/utilities/dimmer/DimmerView;->setDimmed$default(Lcom/discord/utilities/dimmer/DimmerView;ZZILjava/lang/Object;)V

    :goto_1
    return-object v0

    :cond_3
    const-string p1, "adapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getDimmer()Lcom/discord/utilities/dimmer/DimmerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->dimmer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/dimmer/DimmerView;

    return-object v0
.end method

.method private final getEmptyBody()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->emptyBody$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmptyContainer()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->emptyContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->recyclerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event;)V
    .locals 10

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$ErrorSkuPurchase;->getMessage()I

    move-result p1

    invoke-static {p0, p1, v2, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    goto/16 :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->requireAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v1

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$StartSkuPurchase;->getBillingParams()Lcom/android/billingclient/api/BillingFlowParams;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->launchBillingFlow(Landroid/app/Activity;Lcom/android/billingclient/api/BillingFlowParams;)I

    goto/16 :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;->getSkuName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->fromSkuName(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v0

    if-eqz v0, :cond_6

    new-instance v9, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$handleEvent$onDismiss$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$handleEvent$onDismiss$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;)V

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    const-string v4, "parentFragmentManager"

    if-eqz v3, :cond_5

    const/4 v5, 0x1

    if-eq v3, v5, :cond_4

    const/4 v6, 0x2

    const-string v7, "resources"

    if-eq v3, v6, :cond_3

    const/4 v6, 0x3

    if-eq v3, v6, :cond_3

    if-eq v3, v1, :cond_2

    goto/16 :goto_0

    :cond_2
    sget-object v3, Lf/a/a/b/c;->k:Lf/a/a/b/c$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlaySku;->getPremiumSubscriptionCount()I

    move-result v7

    const/4 v6, 0x0

    move-object v4, p1

    invoke-virtual/range {v3 .. v9}, Lf/a/a/b/c$a;->a(Landroidx/fragment/app/FragmentManager;Landroid/content/res/Resources;Ljava/lang/String;IZLkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lf/a/a/e/e;->h:Lf/a/a/e/e$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-static {v1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Event$CompleteSkuPurchase;->getPlanName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "fragmentManager"

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v7}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDismiss"

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "planName"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const p1, 0x7f120315

    invoke-virtual {v3, p1, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(R.st\u2026plan_activated, planName)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf/a/a/e/e;

    invoke-direct {v0}, Lf/a/a/e/e;-><init>()V

    iput-object v9, v0, Lf/a/a/e/e;->d:Lkotlin/jvm/functions/Function0;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "extra_plan_text"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    const-class p1, Lf/a/a/e/e;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/discord/app/AppDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    sget-object p1, Lf/a/a/e/d;->l:Lf/a/a/e/d$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0, v9, v5}, Lf/a/a/e/d$a;->a(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Z)V

    goto :goto_0

    :cond_5
    sget-object p1, Lf/a/a/e/d;->l:Lf/a/a/e/d$a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0, v9, v2}, Lf/a/a/e/d$a;->a(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;Z)V

    nop

    :cond_6
    :goto_0
    return-void
.end method

.method private final setUpRecycler()V
    .locals 5

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->planLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v2, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v1, v2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object v0

    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->adapter:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "result_extra_location_trait"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.utilities.analytics.Traits.Location"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/utilities/analytics/Traits$Location;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->adapter:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    const-string v2, "adapter"

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    new-instance v4, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;

    invoke-direct {v4, p0, v0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$setUpRecycler$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;Lcom/discord/utilities/analytics/Traits$Location;)V

    invoke-virtual {v1, v4}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->setOnClickPlan(Lkotlin/jvm/functions/Function3;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->planLayoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->adapter:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_1
    const-string v0, "planLayoutManager"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d01d7

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "result_extra_current_sku_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "result_view_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.discord.widgets.settings.premium.WidgetChoosePlan.ViewType"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;

    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v2, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;

    invoke-direct {v2, v0, p1}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$Factory;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan$ViewType;Ljava/lang/String;)V

    invoke-direct {v1, p0, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    invoke-virtual {v1, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(this, \u2026del::class.java\n        )"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->viewModel:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 7

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->setUpRecycler()V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->getEmptyBody()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lf/a/b/g;->a:Lf/a/b/g;

    const/4 v3, 0x0

    const-wide v4, 0x53d4f93245L

    invoke-virtual {v2, v4, v5, v3}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f1213d4

    invoke-virtual {p0, v2, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(\n            R\u2026sk.GOOGLE_PLAY)\n        )"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1c

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->viewModel:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    const-string v1, "viewModel"

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$onViewBoundOrOnResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;->viewModel:Lcom/discord/widgets/settings/premium/ChoosePlanViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/premium/WidgetChoosePlan;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$onViewBoundOrOnResume$2;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlan$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlan;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
