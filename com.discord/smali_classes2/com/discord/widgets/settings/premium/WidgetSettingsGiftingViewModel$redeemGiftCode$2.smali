.class public final synthetic Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$redeemGiftCode$2;
.super Lx/m/c/i;
.source "WidgetSettingsGiftingViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->redeemGiftCode(Ljava/lang/String;Lcom/discord/app/AppComponent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/i;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/stores/StoreGifting$GiftState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;)V
    .locals 7

    const-class v3, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;

    const/4 v1, 0x1

    const-string v4, "onHandleGiftCode"

    const-string v5, "onHandleGiftCode(Lcom/discord/stores/StoreGifting$GiftState;)V"

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lx/m/c/i;-><init>(ILjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/stores/StoreGifting$GiftState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel$redeemGiftCode$2;->invoke(Lcom/discord/stores/StoreGifting$GiftState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/stores/StoreGifting$GiftState;)V
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx/m/c/c;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;

    invoke-static {v0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;->access$onHandleGiftCode(Lcom/discord/widgets/settings/premium/WidgetSettingsGiftingViewModel;Lcom/discord/stores/StoreGifting$GiftState;)V

    return-void
.end method
