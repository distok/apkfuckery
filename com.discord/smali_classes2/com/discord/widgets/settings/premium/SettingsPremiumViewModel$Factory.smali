.class public final Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;
.super Ljava/lang/Object;
.source "SettingsPremiumViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getSubscriptionsAndInvoicePreview(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Z)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;->getSubscriptionsAndInvoicePreview(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Z)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final getSubscriptionsAndInvoicePreview(Lcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Z)Lrx/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;",
            ">;"
        }
    .end annotation

    instance-of v0, p2, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v0, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/models/domain/ModelSubscription;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz p3, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result p3

    if-nez p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-direct {v3, v2, v5, p3}, Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;-><init>(Ljava/lang/String;ZZ)V

    invoke-virtual {p1, v3}, Lcom/discord/utilities/rest/RestAPI;->getInvoicePreview(Lcom/discord/restapi/RestAPIParams$InvoicePreviewBody;)Lrx/Observable;

    move-result-object p1

    invoke-static {p1, v4, v5, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$getSubscriptionsAndInvoicePreview$1;

    invoke-direct {p3, p2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$getSubscriptionsAndInvoicePreview$1;-><init>(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V

    invoke-virtual {p1, p3}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    new-instance p3, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$getSubscriptionsAndInvoicePreview$2;

    invoke-direct {p3, p2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$getSubscriptionsAndInvoicePreview$2;-><init>(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;)V

    invoke-virtual {p1, p3}, Lrx/Observable;->I(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    const-string p2, "restAPI\n              .g\u2026ch.Error)\n              }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;

    new-instance p3, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;

    invoke-direct {p3, v1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;-><init>(Lcom/discord/models/domain/billing/ModelInvoicePreview;)V

    invoke-direct {p1, p2, p3}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;-><init>(Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;)V

    new-instance p2, Lg0/l/e/j;

    invoke-direct {p2, p1}, Lg0/l/e/j;-><init>(Ljava/lang/Object;)V

    const-string p1, "Observable.just(\n       \u2026nvoice(null))\n          )"

    invoke-static {p2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p2

    :cond_2
    sget-object p1, Lg0/l/a/g;->e:Lrx/Observable;

    const-string p2, "Observable.never()"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final observeStores(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreGooglePlaySkuDetails;Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/utilities/rest/RestAPI;)Lrx/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePaymentSources;",
            "Lcom/discord/stores/StoreSubscriptions;",
            "Lcom/discord/stores/StoreEntitlements;",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            "Lcom/discord/stores/StoreGooglePlaySkuDetails;",
            "Lcom/discord/stores/StoreGooglePlayPurchases;",
            "Lcom/discord/utilities/rest/RestAPI;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/stores/StorePaymentSources;->getPaymentSources()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p2}, Lcom/discord/stores/StoreSubscriptions;->getSubscriptions()Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;

    invoke-direct {v1, p0, p7}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$1;-><init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;Lcom/discord/utilities/rest/RestAPI;)V

    invoke-virtual {p1, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p2}, Lcom/discord/stores/StoreSubscriptions;->getSubscriptions()Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$2;

    invoke-direct {p2, p0, p7}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$2;-><init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;Lcom/discord/utilities/rest/RestAPI;)V

    invoke-virtual {p1, p2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {p3}, Lcom/discord/stores/StoreEntitlements;->getEntitlementState()Lrx/Observable;

    move-result-object v3

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p4, p1, p2, p1}, Lcom/discord/stores/StorePremiumGuildSubscription;->getPremiumGuildSubscriptionsState$default(Lcom/discord/stores/StorePremiumGuildSubscription;Ljava/lang/Long;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    invoke-virtual {p5}, Lcom/discord/stores/StoreGooglePlaySkuDetails;->getState()Lrx/Observable;

    move-result-object v5

    invoke-virtual {p6}, Lcom/discord/stores/StoreGooglePlayPurchases;->getState()Lrx/Observable;

    move-result-object v6

    sget-object v7, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;->INSTANCE:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory$observeStores$3;

    invoke-static/range {v0 .. v7}, Lrx/Observable;->e(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func7;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable\n          .co\u2026            )\n          }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPaymentSources()Lcom/discord/stores/StorePaymentSources;

    move-result-object v2

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEntitlements()Lcom/discord/stores/StoreEntitlements;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPaymentSources()Lcom/discord/stores/StorePaymentSources;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getSubscriptions()Lcom/discord/stores/StoreSubscriptions;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEntitlements()Lcom/discord/stores/StoreEntitlements;

    move-result-object v9

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getPremiumGuildSubscriptions()Lcom/discord/stores/StorePremiumGuildSubscription;

    move-result-object v10

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlaySkuDetails()Lcom/discord/stores/StoreGooglePlaySkuDetails;

    move-result-object v11

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGooglePlayPurchases()Lcom/discord/stores/StoreGooglePlayPurchases;

    move-result-object v12

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v13

    move-object v6, p0

    invoke-direct/range {v6 .. v13}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;->observeStores(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/stores/StoreGooglePlaySkuDetails;Lcom/discord/stores/StoreGooglePlayPurchases;Lcom/discord/utilities/rest/RestAPI;)Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v6

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;-><init>(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V

    return-object p1
.end method
