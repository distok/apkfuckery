.class public final Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;
.super Lf/a/b/l0;
.source "SettingsPremiumViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;,
        Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;,
        Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;,
        Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;,
        Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$SubscriptionAndInvoice;,
        Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private launchPremiumTabStartTimeMs:J

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeEntitlements:Lcom/discord/stores/StoreEntitlements;

.field private final storePaymentsSources:Lcom/discord/stores/StorePaymentSources;

.field private final storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

.field private final storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;


# direct methods
.method public constructor <init>(Lcom/discord/stores/StorePaymentSources;Lcom/discord/stores/StoreSubscriptions;Lcom/discord/stores/StoreEntitlements;Lcom/discord/stores/StorePremiumGuildSubscription;Lcom/discord/utilities/rest/RestAPI;Lrx/Observable;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StorePaymentSources;",
            "Lcom/discord/stores/StoreSubscriptions;",
            "Lcom/discord/stores/StoreEntitlements;",
            "Lcom/discord/stores/StorePremiumGuildSubscription;",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "storePaymentsSources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeSubscriptions"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeEntitlements"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storePremiumGuildSubscription"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "storeObservable"

    invoke-static {p6, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storePaymentsSources:Lcom/discord/stores/StorePaymentSources;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    iput-object p3, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storeEntitlements:Lcom/discord/stores/StoreEntitlements;

    iput-object p4, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    iput-object p5, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->fetchData()V

    invoke-static {p6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x2

    invoke-static {p1, p0, p2, p3, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    new-instance v6, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$1;-><init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$fetchData(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->fetchData()V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->handleStoreState(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;)V

    return-void
.end method

.method public static final synthetic access$onCancelError(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->onCancelError()V

    return-void
.end method

.method private final fetchData()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storePaymentsSources:Lcom/discord/stores/StorePaymentSources;

    invoke-virtual {v0}, Lcom/discord/stores/StorePaymentSources;->fetchPaymentSources()V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    invoke-virtual {v0}, Lcom/discord/stores/StoreSubscriptions;->fetchSubscriptions()V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storeEntitlements:Lcom/discord/stores/StoreEntitlements;

    const-wide v1, 0x73df54a4a020016L

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreEntitlements;->fetchMyEntitlementsForApplication(J)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    invoke-virtual {v0}, Lcom/discord/stores/StorePremiumGuildSubscription;->fetchUserGuildPremiumState()V

    sget-object v0, Lcom/discord/utilities/billing/GooglePlayBillingManager;->INSTANCE:Lcom/discord/utilities/billing/GooglePlayBillingManager;

    invoke-virtual {v0}, Lcom/discord/utilities/billing/GooglePlayBillingManager;->querySkuDetails()V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;)V
    .locals 22

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getPaymentSourcesState()Lcom/discord/stores/StorePaymentSources$PaymentSourcesState;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getSubscriptionsState()Lcom/discord/stores/StoreSubscriptions$SubscriptionsState;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getEntitlementState()Lcom/discord/stores/StoreEntitlements$State;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getGuildSubscriptionState()Lcom/discord/stores/StorePremiumGuildSubscription$State;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getRenewalInvoicePreviewFetch()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getCurrentInvoicePreviewFetch()Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getSkuDetailsState()Lcom/discord/stores/StoreGooglePlaySkuDetails$State;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$StoreState;->getPurchaseState()Lcom/discord/stores/StoreGooglePlayPurchases$State;

    move-result-object v7

    instance-of v8, v0, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loaded;

    if-eqz v8, :cond_5

    instance-of v8, v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    if-eqz v8, :cond_5

    instance-of v8, v2, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    if-eqz v8, :cond_5

    instance-of v8, v3, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    if-eqz v8, :cond_5

    instance-of v8, v4, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;

    if-eqz v8, :cond_5

    instance-of v8, v5, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;

    if-eqz v8, :cond_5

    instance-of v8, v6, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    if-eqz v8, :cond_5

    instance-of v8, v7, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    if-eqz v8, :cond_5

    check-cast v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;

    invoke-virtual {v1}, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Loaded;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    move-object v11, v8

    check-cast v11, Lcom/discord/models/domain/ModelSubscription;

    invoke-virtual {v11}, Lcom/discord/models/domain/ModelSubscription;->getType()Lcom/discord/models/domain/ModelSubscription$Type;

    move-result-object v11

    sget-object v12, Lcom/discord/models/domain/ModelSubscription$Type;->PREMIUM:Lcom/discord/models/domain/ModelSubscription$Type;

    if-ne v11, v12, :cond_1

    const/4 v11, 0x1

    goto :goto_0

    :cond_1
    const/4 v11, 0x0

    :goto_0
    if-eqz v11, :cond_0

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    :goto_1
    move-object v12, v8

    check-cast v12, Lcom/discord/models/domain/ModelSubscription;

    if-eqz v12, :cond_3

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Long;

    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v1, v10

    sget-object v8, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v1, v9

    invoke-static {v1}, Lx/h/f;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v12, v1}, Lcom/discord/models/domain/ModelSubscription;->hasAnyOfPlans(Ljava/util/Set;)Z

    move-result v10

    move/from16 v17, v10

    goto :goto_2

    :cond_3
    const/16 v17, 0x0

    :goto_2
    new-instance v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    check-cast v0, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loaded;

    invoke-virtual {v0}, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Loaded;->getPaymentSources()Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x0

    check-cast v2, Lcom/discord/stores/StoreEntitlements$State$Loaded;

    invoke-virtual {v2}, Lcom/discord/stores/StoreEntitlements$State$Loaded;->getOwnedEntitlements()Ljava/util/Map;

    move-result-object v0

    const-wide v8, 0x73df54a4a020016L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    :goto_3
    move-object v15, v0

    check-cast v3, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;

    invoke-virtual {v3}, Lcom/discord/stores/StorePremiumGuildSubscription$State$Loaded;->getPremiumGuildSubscriptionSlotMap()Ljava/util/Map;

    move-result-object v16

    check-cast v4, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;

    invoke-virtual {v4}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;->getModelInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object v18

    check-cast v5, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;

    invoke-virtual {v5}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Invoice;->getModelInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object v19

    check-cast v6, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;

    invoke-virtual {v6}, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v20

    check-cast v7, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;

    invoke-virtual {v7}, Lcom/discord/stores/StoreGooglePlayPurchases$State$Loaded;->getPurchases()Ljava/util/List;

    move-result-object v21

    move-object v11, v1

    invoke-direct/range {v11 .. v21}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;-><init>(Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;)V

    goto :goto_5

    :cond_5
    instance-of v0, v0, Lcom/discord/stores/StorePaymentSources$PaymentSourcesState$Failure;

    if-nez v0, :cond_7

    instance-of v0, v1, Lcom/discord/stores/StoreSubscriptions$SubscriptionsState$Failure;

    if-nez v0, :cond_7

    instance-of v0, v3, Lcom/discord/stores/StorePremiumGuildSubscription$State$Failure;

    if-nez v0, :cond_7

    instance-of v0, v4, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$InvoicePreviewFetch$Error;

    if-nez v0, :cond_7

    instance-of v0, v6, Lcom/discord/stores/StoreGooglePlaySkuDetails$State$Failure;

    if-eqz v0, :cond_6

    goto :goto_4

    :cond_6
    sget-object v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loading;

    goto :goto_5

    :cond_7
    :goto_4
    sget-object v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Failure;->INSTANCE:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Failure;

    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final markBusy()V
    .locals 14

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3fb

    const/4 v13, 0x0

    invoke-static/range {v1 .. v13}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;Lcom/discord/models/domain/ModelSubscription;Ljava/util/List;ZLjava/util/List;Ljava/util/Map;ZLcom/discord/models/domain/billing/ModelInvoicePreview;Lcom/discord/models/domain/billing/ModelInvoicePreview;Ljava/util/Map;Ljava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private final onCancelError()V
    .locals 3

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;

    const v2, 0x7f1212d8

    invoke-direct {v1, v2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;-><init>(I)V

    iget-object v0, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final cancelSubscription()V
    .locals 12
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-virtual {p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->markBusy()V

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/discord/utilities/rest/RestAPI;->deleteSubscription(Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v9, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$cancelSubscription$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$cancelSubscription$1;-><init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V

    const/4 v8, 0x0

    new-instance v7, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$cancelSubscription$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$cancelSubscription$2;-><init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V

    const/16 v10, 0x16

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final getEventSubject()Lrx/Observable;
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getLaunchPremiumTabStartTimeMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->launchPremiumTabStartTimeMs:J

    return-wide v0
.end method

.method public final getRestAPI()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final getStoreEntitlements()Lcom/discord/stores/StoreEntitlements;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storeEntitlements:Lcom/discord/stores/StoreEntitlements;

    return-object v0
.end method

.method public final getStorePaymentsSources()Lcom/discord/stores/StorePaymentSources;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storePaymentsSources:Lcom/discord/stores/StorePaymentSources;

    return-object v0
.end method

.method public final getStorePremiumGuildSubscription()Lcom/discord/stores/StorePremiumGuildSubscription;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storePremiumGuildSubscription:Lcom/discord/stores/StorePremiumGuildSubscription;

    return-object v0
.end method

.method public final getStoreSubscriptions()Lcom/discord/stores/StoreSubscriptions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->storeSubscriptions:Lcom/discord/stores/StoreSubscriptions;

    return-object v0
.end method

.method public final onRetryClicked()V
    .locals 0
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->fetchData()V

    return-void
.end method

.method public final setLaunchPremiumTabStartTimeMs(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->launchPremiumTabStartTimeMs:J

    return-void
.end method
