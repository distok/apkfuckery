.class public final Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;
.source "ChoosePlanViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final isEmpty:Z

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final purchases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation
.end field

.field private final purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

.field private final skuDetails:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final subscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "purchasesQueryState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "items"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchases"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptions"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    iput-object p3, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    iput-object p4, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    iput-object p5, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    iput-boolean p6, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;ZILjava/lang/Object;)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->copy(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    return v0
.end method

.method public final copy(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;Z)",
            "Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;"
        }
    .end annotation

    const-string v0, "purchasesQueryState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "items"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "purchases"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subscriptions"

    invoke-static {p5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;-><init>(Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    iget-boolean p1, p1, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    return-object v0
.end method

.method public final getPurchases()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/billingclient/api/Purchase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    return-object v0
.end method

.method public final getPurchasesQueryState()Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    return-object v0
.end method

.method public final getSkuDetails()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    return-object v0
.end method

.method public final getSubscriptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelSubscription;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(purchasesQueryState="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchasesQueryState:Lcom/discord/stores/StoreGooglePlayPurchases$QueryState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skuDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->skuDetails:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchases="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->purchases:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subscriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->subscriptions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isEmpty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premium/ChoosePlanViewModel$ViewState$Loaded;->isEmpty:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
