.class public final Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;
.super Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;
.source "WidgetChoosePlanAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$HeaderViewHolder;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$DividerViewHolder;,
        Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple<",
        "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
        ">;"
    }
.end annotation


# instance fields
.field private onClickPlan:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/discord/utilities/billing/GooglePlaySku;",
            "-",
            "Ljava/lang/String;",
            "-",
            "Lcom/android/billingclient/api/SkuDetails;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$onClickPlan$1;->INSTANCE:Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$onClickPlan$1;

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->onClickPlan:Lkotlin/jvm/functions/Function3;

    return-void
.end method

.method public static final synthetic access$getOnClickPlan$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;)Lkotlin/jvm/functions/Function3;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->onClickPlan:Lkotlin/jvm/functions/Function3;

    return-object p0
.end method

.method public static final synthetic access$setOnClickPlan$p(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;Lkotlin/jvm/functions/Function3;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->onClickPlan:Lkotlin/jvm/functions/Function3;

    return-void
.end method


# virtual methods
.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/discord/utilities/mg_recycler/MGRecyclerViewHolder<",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;",
            "Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    const/4 p1, 0x1

    if-eq p2, p1, :cond_1

    const/4 p1, 0x2

    if-ne p2, p1, :cond_0

    new-instance p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$DividerViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$DividerViewHolder;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->invalidViewTypeException(I)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    new-instance p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$PlanViewHeader;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;)V

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$HeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$HeaderViewHolder;-><init>(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;)V

    :goto_0
    return-object p1
.end method

.method public final setOnClickPlan(Lkotlin/jvm/functions/Function3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/discord/utilities/billing/GooglePlaySku;",
            "-",
            "Ljava/lang/String;",
            "-",
            "Lcom/android/billingclient/api/SkuDetails;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onClickPlan"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter;->onClickPlan:Lkotlin/jvm/functions/Function3;

    return-void
.end method
