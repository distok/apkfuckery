.class public final Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;
.super Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;
.source "WidgetChoosePlanAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Plan"
.end annotation


# instance fields
.field private final displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

.field private final isCurrentPlan:Z

.field private final oldSkuName:Ljava/lang/String;

.field private final purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

.field private final skuDetails:Lcom/android/billingclient/api/SkuDetails;

.field private final upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;


# direct methods
.method public constructor <init>(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Z)V
    .locals 2

    const-string v0, "displaySku"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    iput-object p2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    iput-object p3, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    iput-object p4, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    iput-boolean p6, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p7, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v6, v1

    goto :goto_0

    :cond_0
    move-object v6, p4

    :goto_0
    and-int/lit8 v0, p7, 0x10

    if-eqz v0, :cond_1

    move-object v7, v1

    goto :goto_1

    :cond_1
    move-object v7, p5

    :goto_1
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    const/4 v8, 0x0

    goto :goto_2

    :cond_2
    move v8, p6

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v2 .. v8}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;-><init>(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;ZILjava/lang/Object;)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->copy(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Z)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    return-object v0
.end method

.method public final component2()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final component3()Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    return v0
.end method

.method public final copy(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Z)Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;
    .locals 8

    const-string v0, "displaySku"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "skuDetails"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;-><init>(Lcom/discord/utilities/billing/GooglePlaySku;Lcom/android/billingclient/api/SkuDetails;Lcom/discord/utilities/billing/GooglePlaySku;Ljava/lang/String;Lcom/android/billingclient/api/SkuDetails;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    iget-object v1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    iget-boolean p1, p1, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDisplaySku()Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item;->get_type()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getOldSkuName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPurchaseSku()Lcom/discord/utilities/billing/GooglePlaySku;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    return-object v0
.end method

.method public final getSkuDetails()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public final getUpgradeSkuDetails()Lcom/android/billingclient/api/SkuDetails;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final isCurrentPlan()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Plan(displaySku="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->displaySku:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", skuDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->skuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", purchaseSku="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->purchaseSku:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", oldSkuName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->oldSkuName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", upgradeSkuDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->upgradeSkuDetails:Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isCurrentPlan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/settings/premium/WidgetChoosePlanAdapter$Item$Plan;->isCurrentPlan:Z

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->C(Ljava/lang/StringBuilder;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
