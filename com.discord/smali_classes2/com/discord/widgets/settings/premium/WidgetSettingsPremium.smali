.class public final Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsPremium.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;

.field private static final INTENT_SECTION:Ljava/lang/String; = "intent_section"

.field public static final SECTION_NITRO:I = 0x1

.field public static final SECTION_NITRO_CLASSIC:I = 0x0

.field private static final VIEW_INDEX_CONTENT:I = 0x0

.field private static final VIEW_INDEX_ERROR:I = 0x2

.field private static final VIEW_INDEX_LOADING:I = 0x1


# instance fields
.field private final activeGuildSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final activeSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final billingBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final billingDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final billingGooglePlayManage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final billingInfoTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostCountText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostDiscountText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier1Container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier1Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier2Container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final buyTier2Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private connection:Landroid/content/ServiceConnection;

.field private final creditContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final creditDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final creditNitro$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final creditNitroClassic$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final grandfathered$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final legalese$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final paymentContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final retryButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final statusNoticeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final statusNoticeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final statusNoticeTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final stickersPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subscriptionContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final uploadPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x1c

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v3, "uploadPerks"

    const-string v4, "getUploadPerks()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "stickersPerks"

    const-string v7, "getStickersPerks()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "boostDiscountText"

    const-string v7, "getBoostDiscountText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "boostCountText"

    const-string v7, "getBoostCountText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "buyTier2Monthly"

    const-string v7, "getBuyTier2Monthly()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "buyTier1Monthly"

    const-string v7, "getBuyTier1Monthly()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "buyTier1Container"

    const-string v7, "getBuyTier1Container()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "buyTier2Container"

    const-string v7, "getBuyTier2Container()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "viewFlipper"

    const-string v7, "getViewFlipper()Landroid/widget/ViewFlipper;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "subscriptionContainer"

    const-string v7, "getSubscriptionContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "activeSubscriptionView"

    const-string v7, "getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "activeGuildSubscriptionView"

    const-string v7, "getActiveGuildSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "paymentContainer"

    const-string v7, "getPaymentContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "billingInfoTv"

    const-string v7, "getBillingInfoTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "billingGooglePlayManage"

    const-string v7, "getBillingGooglePlayManage()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "billingBtn"

    const-string v7, "getBillingBtn()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "billingDivider"

    const-string v7, "getBillingDivider()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x11

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "retryButton"

    const-string v7, "getRetryButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x12

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "legalese"

    const-string v7, "getLegalese()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x13

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "grandfathered"

    const-string v7, "getGrandfathered()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x14

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "scrollView"

    const-string v7, "getScrollView()Landroid/widget/ScrollView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x15

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "creditContainer"

    const-string v7, "getCreditContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x16

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "creditNitro"

    const-string v7, "getCreditNitro()Lcom/discord/views/premium/AccountCreditView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x17

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "creditNitroClassic"

    const-string v7, "getCreditNitroClassic()Lcom/discord/views/premium/AccountCreditView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x18

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "creditDivider"

    const-string v7, "getCreditDivider()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x19

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "statusNoticeContainer"

    const-string v7, "getStatusNoticeContainer()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "statusNoticeTv"

    const-string v7, "getStatusNoticeTv()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const-string v6, "statusNoticeButton"

    const-string v7, "getStatusNoticeButton()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->Companion:Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a07be

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->uploadPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->stickersPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07ca

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->boostDiscountText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c9

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->boostCountText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07ce

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier2Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07cf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier1Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier1Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d7

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier2Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d8

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->subscriptionContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->activeSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07bf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->activeGuildSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07cd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->paymentContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingInfoTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingGooglePlayManage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a005d

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->retryButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c6

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->legalese$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c5

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->grandfathered$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d1

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07c3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a031a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditNitro$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a031b

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditNitroClassic$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a031c

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d3

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->statusNoticeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d4

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->statusNoticeTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a07d2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->statusNoticeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getBuyTier1Monthly$p(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)Landroid/widget/Button;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->handleEvent(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$scrollToSection(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/Integer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->scrollToSection(Ljava/lang/Integer;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    return-void
.end method

.method public static final synthetic access$showCancelConfirmationAlert(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->showCancelConfirmationAlert(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V

    return-void
.end method

.method public static final synthetic access$showContent(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->showContent(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V

    return-void
.end method

.method public static final synthetic access$showDesktopManageAlert(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->showDesktopManageAlert()V

    return-void
.end method

.method public static final synthetic access$showFailureUI(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->showFailureUI()V

    return-void
.end method

.method public static final synthetic access$showLoadingUI(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->showLoadingUI()V

    return-void
.end method

.method private final configureAccountCredit(Ljava/util/List;Lcom/discord/models/domain/ModelSubscription;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/ModelEntitlement;",
            ">;",
            "Lcom/discord/models/domain/ModelSubscription;",
            ")V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/discord/models/domain/ModelEntitlement;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelEntitlement;->getParentId()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelEntitlement;->getSubscriptionPlan()Lcom/discord/models/domain/ModelSubscriptionPlan;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscriptionPlan;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v4

    if-nez v3, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v8, v6, v4

    if-nez v8, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_2
    sget-object v4, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v4

    if-nez v3, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v6, v4

    if-nez v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getCreditContainer()Landroid/view/View;

    move-result-object p1

    const/4 v3, 0x1

    if-gtz v1, :cond_7

    if-lez v2, :cond_6

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    goto :goto_4

    :cond_7
    :goto_3
    const/4 v4, 0x1

    :goto_4
    const/16 v5, 0x8

    if-eqz v4, :cond_8

    const/4 v4, 0x0

    goto :goto_5

    :cond_8
    const/16 v4, 0x8

    :goto_5
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getCreditDivider()Landroid/view/View;

    move-result-object p1

    if-lez v1, :cond_9

    if-lez v2, :cond_9

    goto :goto_6

    :cond_9
    const/4 v3, 0x0

    :goto_6
    if-eqz v3, :cond_a

    goto :goto_7

    :cond_a
    const/16 v0, 0x8

    :goto_7
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getCreditNitroClassic()Lcom/discord/views/premium/AccountCreditView;

    move-result-object p1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_1:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v3

    invoke-virtual {p1, v3, v4, v1, p2}, Lcom/discord/views/premium/AccountCreditView;->a(JILcom/discord/models/domain/ModelSubscription;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getCreditNitro()Lcom/discord/views/premium/AccountCreditView;

    move-result-object p1

    sget-object v0, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_MONTH_TIER_2:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1, v2, p2}, Lcom/discord/views/premium/AccountCreditView;->a(JILcom/discord/models/domain/ModelSubscription;)V

    return-void
.end method

.method private final configureActiveSubscriptionView(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 24

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getCurrentInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isNonePlan()Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v7

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    const/4 v8, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    sget-object v10, Lcom/discord/utilities/billing/GooglePlaySku;->Companion:Lcom/discord/utilities/billing/GooglePlaySku$Companion;

    invoke-virtual {v10, v9}, Lcom/discord/utilities/billing/GooglePlaySku$Companion;->fromSkuName(Ljava/lang/String;)Lcom/discord/utilities/billing/GooglePlaySku;

    move-result-object v9

    goto :goto_2

    :cond_2
    move-object v9, v8

    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getSubscriptionContainer()Landroid/view/View;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getHasAnyPremiumGuildSubscriptions()Z

    move-result v11

    if-nez v11, :cond_4

    if-eqz v6, :cond_3

    goto :goto_3

    :cond_3
    const/4 v11, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 v11, 0x1

    :goto_4
    const/16 v12, 0x8

    if-eqz v11, :cond_5

    const/4 v11, 0x0

    goto :goto_5

    :cond_5
    const/16 v11, 0x8

    :goto_5
    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v10

    if-eqz v6, :cond_6

    if-eqz v2, :cond_6

    const/4 v11, 0x1

    goto :goto_6

    :cond_6
    const/4 v11, 0x0

    :goto_6
    if-eqz v11, :cond_7

    const/4 v11, 0x0

    goto :goto_7

    :cond_7
    const/16 v11, 0x8

    :goto_7
    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    if-eqz v6, :cond_1c

    if-eqz v2, :cond_1c

    invoke-static {v1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->getInvoiceItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v10, v6

    check-cast v10, Lcom/discord/models/domain/billing/ModelInvoiceItem;

    invoke-virtual {v10}, Lcom/discord/models/domain/billing/ModelInvoiceItem;->getSubscriptionPlanId()J

    move-result-wide v13

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPlanId()J

    move-result-wide v15

    cmp-long v11, v13, v15

    if-eqz v11, :cond_a

    invoke-virtual {v10}, Lcom/discord/models/domain/billing/ModelInvoiceItem;->getSubscriptionPlanId()J

    move-result-wide v10

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v13

    if-eqz v13, :cond_9

    invoke-virtual {v13}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getPlanId()J

    move-result-wide v13

    cmp-long v15, v10, v13

    if-nez v15, :cond_9

    goto :goto_8

    :cond_9
    const/4 v10, 0x0

    goto :goto_9

    :cond_a
    :goto_8
    const/4 v10, 0x1

    :goto_9
    if-eqz v10, :cond_8

    goto :goto_a

    :cond_b
    move-object v6, v8

    :goto_a
    check-cast v6, Lcom/discord/models/domain/billing/ModelInvoiceItem;

    if-eqz v9, :cond_c

    invoke-static {v9}, Lcom/discord/utilities/billing/GooglePlaySkuKt;->isBundledSku(Lcom/discord/utilities/billing/GooglePlaySku;)Z

    move-result v2

    if-ne v2, v4, :cond_c

    invoke-virtual {v9}, Lcom/discord/utilities/billing/GooglePlaySku;->getPremiumSubscriptionCount()I

    move-result v2

    move/from16 v19, v2

    goto :goto_b

    :cond_c
    const/4 v2, 0x0

    const/16 v19, 0x0

    :goto_b
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/billingclient/api/SkuDetails;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_c

    :cond_d
    move-object v2, v8

    :goto_c
    if-eqz v7, :cond_e

    if-eqz v2, :cond_e

    sget-object v3, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v3, v10, v11}, Lcom/discord/utilities/premium/PremiumUtils;->microAmountToMinor(J)I

    move-result v2

    goto :goto_d

    :cond_e
    if-eqz v6, :cond_f

    invoke-virtual {v6}, Lcom/discord/models/domain/billing/ModelInvoiceItem;->getAmount()I

    move-result v2

    goto :goto_d

    :cond_f
    const/4 v2, 0x0

    :goto_d
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v3

    sget-object v6, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;->MONTHLY:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    if-ne v3, v6, :cond_10

    const/4 v3, 0x1

    goto :goto_e

    :cond_10
    const/4 v3, 0x0

    :goto_e
    invoke-direct {v0, v3, v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPriceText(ZI)Ljava/lang/String;

    move-result-object v17

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription$Status;->isAccountHold()Z

    move-result v2

    if-nez v2, :cond_13

    if-eqz v9, :cond_11

    invoke-virtual {v9}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v2

    goto :goto_f

    :cond_11
    move-object v2, v8

    :goto_f
    sget-object v3, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_GUILD:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v2, v3, :cond_13

    if-eqz v9, :cond_12

    invoke-virtual {v9}, Lcom/discord/utilities/billing/GooglePlaySku;->getType()Lcom/discord/utilities/billing/GooglePlaySku$Type;

    move-result-object v2

    goto :goto_10

    :cond_12
    move-object v2, v8

    :goto_10
    sget-object v3, Lcom/discord/utilities/billing/GooglePlaySku$Type;->PREMIUM_TIER_1:Lcom/discord/utilities/billing/GooglePlaySku$Type;

    if-eq v2, v3, :cond_13

    const/4 v2, 0x1

    goto :goto_11

    :cond_13
    const/4 v2, 0x0

    :goto_11
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPurchases()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPremiumSubscriptionViewCallbacks(Lcom/discord/models/domain/ModelSubscription;ZLjava/util/List;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription$Status;->isAccountHold()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-virtual {v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getManageBillingCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    goto :goto_12

    :cond_14
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v3

    if-eqz v3, :cond_15

    move-object/from16 v20, v8

    goto :goto_13

    :cond_15
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription$Status;->isCanceled()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-virtual {v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getRestoreCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    goto :goto_12

    :cond_16
    invoke-virtual {v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getManagePlanCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    :goto_12
    move-object/from16 v20, v3

    :goto_13
    if-eqz v7, :cond_17

    goto :goto_14

    :cond_17
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v3

    if-eqz v3, :cond_18

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getPlanId()J

    move-result-wide v10

    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->NONE_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v13

    cmp-long v3, v10, v13

    if-eqz v3, :cond_19

    :cond_18
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v3

    if-eqz v3, :cond_1a

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getPlanId()J

    move-result-wide v10

    sget-object v3, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->NONE_YEAR:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v3}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v13

    cmp-long v3, v10, v13

    if-nez v3, :cond_1a

    :cond_19
    const/4 v3, 0x1

    const/16 v23, 0x1

    goto :goto_15

    :cond_1a
    :goto_14
    const/4 v3, 0x0

    const/16 v23, 0x0

    :goto_15
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v13

    invoke-static {v1}, Lcom/discord/views/ActiveSubscriptionView;->b(Lcom/discord/models/domain/ModelSubscription;)Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    move-result-object v14

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v15

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getTrialId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1b

    const/4 v3, 0x1

    const/16 v16, 0x1

    goto :goto_16

    :cond_1b
    const/4 v3, 0x0

    const/16 v16, 0x0

    :goto_16
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy()Z

    move-result v18

    invoke-virtual {v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getManagePremiumGuildCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v21

    invoke-virtual {v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getCancelCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v22

    invoke-virtual/range {v13 .. v23}, Lcom/discord/views/ActiveSubscriptionView;->a(Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;Lcom/discord/models/domain/ModelSubscription$Status;ZLjava/lang/CharSequence;ZILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Z)V

    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getCurrentInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object v2

    if-eqz v2, :cond_20

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->getInvoiceItems()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_20

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/discord/models/domain/billing/ModelInvoiceItem;

    if-eqz v1, :cond_1e

    invoke-virtual {v6}, Lcom/discord/models/domain/billing/ModelInvoiceItem;->getSubscriptionPlanId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v6}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/discord/models/domain/ModelSubscription;->hasAnyOfPlans(Ljava/util/Set;)Z

    move-result v6

    goto :goto_17

    :cond_1e
    const/4 v6, 0x0

    :goto_17
    if-eqz v6, :cond_1d

    goto :goto_18

    :cond_1f
    move-object v3, v8

    :goto_18
    check-cast v3, Lcom/discord/models/domain/billing/ModelInvoiceItem;

    goto :goto_19

    :cond_20
    move-object v3, v8

    :goto_19
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getActiveGuildSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getHasAnyPremiumGuildSubscriptions()Z

    move-result v6

    if-eqz v6, :cond_21

    if-eqz v3, :cond_21

    const/4 v6, 0x1

    goto :goto_1a

    :cond_21
    const/4 v6, 0x0

    :goto_1a
    if-eqz v6, :cond_22

    const/4 v6, 0x0

    goto :goto_1b

    :cond_22
    const/16 v6, 0x8

    :goto_1b
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getHasAnyPremiumGuildSubscriptions()Z

    move-result v2

    if-eqz v2, :cond_36

    if-eqz v3, :cond_36

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getGuildSubscriptions()Ljava/util/Map;

    move-result-object v2

    if-eqz v9, :cond_23

    if-eqz v7, :cond_23

    invoke-virtual {v9}, Lcom/discord/utilities/billing/GooglePlaySku;->getPremiumSubscriptionCount()I

    move-result v6

    :goto_1c
    move/from16 v19, v6

    goto :goto_1e

    :cond_23
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    if-eqz v1, :cond_24

    sget-object v9, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {v9, v1}, Lcom/discord/utilities/premium/PremiumUtils;->getPremiumGuildSubscriptionCountFromSubscription(Lcom/discord/models/domain/ModelSubscription;)I

    move-result v9

    goto :goto_1d

    :cond_24
    const/4 v9, 0x0

    :goto_1d
    sub-int/2addr v6, v9

    goto :goto_1c

    :goto_1e
    if-eqz v1, :cond_25

    sget-object v6, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->PREMIUM_GUILD_MONTH:Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v6}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/discord/models/domain/ModelSubscription;->hasAnyOfPlans(Ljava/util/Set;)Z

    move-result v6

    goto :goto_1f

    :cond_25
    const/4 v6, 0x0

    :goto_1f
    invoke-virtual {v3}, Lcom/discord/models/domain/billing/ModelInvoiceItem;->getAmount()I

    move-result v3

    invoke-direct {v0, v6, v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPriceText(ZI)Ljava/lang/String;

    move-result-object v17

    if-eqz v1, :cond_26

    invoke-static {v1}, Lcom/discord/views/ActiveSubscriptionView;->b(Lcom/discord/models/domain/ModelSubscription;)Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    move-result-object v3

    goto :goto_20

    :cond_26
    move-object v3, v8

    :goto_20
    if-eqz v7, :cond_27

    sget-object v6, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    if-ne v3, v6, :cond_27

    const/4 v3, 0x1

    goto :goto_21

    :cond_27
    const/4 v3, 0x0

    :goto_21
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getActiveGuildSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v6

    if-eqz v7, :cond_29

    if-eqz v3, :cond_28

    goto :goto_22

    :cond_28
    const/4 v9, 0x0

    goto :goto_23

    :cond_29
    :goto_22
    const/4 v9, 0x1

    :goto_23
    if-eqz v9, :cond_2a

    const/4 v12, 0x0

    :cond_2a
    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v8

    :cond_2b
    invoke-direct {v0, v3, v8}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPremiumGuildSubscriptionViewCallbacks(ZLjava/lang/String;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    instance-of v6, v2, Ljava/util/Collection;

    if-eqz v6, :cond_2c

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2c

    goto :goto_24

    :cond_2c
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelPremiumGuildSubscriptionSlot;->getCanceled()Z

    move-result v6

    if-eqz v6, :cond_2d

    const/4 v2, 0x1

    goto :goto_25

    :cond_2e
    :goto_24
    const/4 v2, 0x0

    :goto_25
    if-eqz v7, :cond_30

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v6

    if-eqz v6, :cond_2f

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSubscription$Status;->isCanceled()Z

    move-result v6

    goto :goto_26

    :cond_2f
    const/4 v6, 0x0

    :goto_26
    if-eqz v6, :cond_30

    const/4 v5, 0x1

    :cond_30
    if-eqz v5, :cond_31

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getRestoreCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    :goto_27
    move-object/from16 v20, v6

    goto :goto_28

    :cond_31
    if-eqz v1, :cond_32

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v6

    if-eqz v6, :cond_32

    invoke-virtual {v6}, Lcom/discord/models/domain/ModelSubscription$Status;->isAccountHold()Z

    move-result v6

    if-ne v6, v4, :cond_32

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getManageBillingCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    goto :goto_27

    :cond_32
    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getManagePlanCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    goto :goto_27

    :goto_28
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getActiveGuildSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;

    move-result-object v13

    sget-object v14, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    if-nez v2, :cond_35

    if-eqz v5, :cond_33

    goto :goto_29

    :cond_33
    if-eqz v1, :cond_34

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v1

    if-eqz v1, :cond_34

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription$Status;->isAccountHold()Z

    move-result v1

    if-ne v1, v4, :cond_34

    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->ACCOUNT_HOLD:Lcom/discord/models/domain/ModelSubscription$Status;

    goto :goto_2a

    :cond_34
    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->ACTIVE:Lcom/discord/models/domain/ModelSubscription$Status;

    goto :goto_2a

    :cond_35
    :goto_29
    sget-object v1, Lcom/discord/models/domain/ModelSubscription$Status;->CANCELED:Lcom/discord/models/domain/ModelSubscription$Status;

    :goto_2a
    move-object v15, v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->isBusy()Z

    move-result v18

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getManagePremiumGuildCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v21

    invoke-virtual {v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;->getCancelCallback()Lkotlin/jvm/functions/Function0;

    move-result-object v22

    sget v1, Lcom/discord/views/ActiveSubscriptionView;->l:I

    const/16 v16, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v13 .. v23}, Lcom/discord/views/ActiveSubscriptionView;->a(Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;Lcom/discord/models/domain/ModelSubscription$Status;ZLjava/lang/CharSequence;ZILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Z)V

    :cond_36
    return-void
.end method

.method private final configureButtonText(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    sget-object v2, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_1_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v2}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/billingclient/api/SkuDetails;

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-direct {p0, v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPriceText(Lcom/android/billingclient/api/SkuDetails;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object v0

    if-eqz p1, :cond_1

    sget-object v1, Lcom/discord/utilities/billing/GooglePlaySku;->PREMIUM_TIER_2_MONTHLY:Lcom/discord/utilities/billing/GooglePlaySku;

    invoke-virtual {v1}, Lcom/discord/utilities/billing/GooglePlaySku;->getSkuName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/android/billingclient/api/SkuDetails;

    :cond_1
    invoke-direct {p0, v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPriceText(Lcom/android/billingclient/api/SkuDetails;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic configureButtonText$default(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/util/Map;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureButtonText(Ljava/util/Map;)V

    return-void
.end method

.method private final configureButtons(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureButtonText(Ljava/util/Map;)V

    const/4 p2, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v2

    if-ne v2, v1, :cond_1

    new-array p1, v0, [Landroid/widget/Button;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object v2

    aput-object v2, p1, p2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object p2

    aput-object p2, p1, v1

    invoke-static {p1}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p2, v1, v2, v0, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setEnabledAlpha$default(Landroid/view/View;ZFILjava/lang/Object;)V

    new-instance v2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    invoke-virtual {p2, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    new-array v0, v0, [Landroid/widget/Button;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier1Monthly()Landroid/widget/Button;

    move-result-object v2

    aput-object v2, v0, p2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier2Monthly()Landroid/widget/Button;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    if-nez p1, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;

    invoke-direct {v3, v2, p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureButtons$$inlined$forEach$lambda$2;-><init>(Landroid/widget/Button;Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private final configureGrandfatheredHeader(Lcom/discord/models/domain/ModelSubscription;)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v3, 0x2

    if-eq p1, v3, :cond_3

    const/4 v3, 0x3

    if-eq p1, v3, :cond_2

    goto :goto_1

    :cond_2
    const p1, 0x7f121310

    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getGRANDFATHERED_YEARLY_END_DATE()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const p1, 0x7f12130b

    new-array v0, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getGRANDFATHERED_MONTHLY_END_DATE()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getGrandfathered()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {v0}, Lf/a/j/b/b/g;->b(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getGrandfathered()Landroid/widget/TextView;

    move-result-object p1

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_6

    goto :goto_3

    :cond_6
    const/16 v2, 0x8

    :goto_3
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final configureLegalese(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 12

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getRenewalInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object p1

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->isPremiumSubscription()Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getInterval()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionInterval;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    const v2, 0x7f12030c

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    const v2, 0x7f12030b

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getGoogleSubscriptionRenewalPrice(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "requireContext()"

    const/4 v4, 0x0

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->getTotal()I

    move-result p1

    goto :goto_1

    :cond_4
    const/4 p1, 0x0

    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const v6, 0x7f121814

    invoke-virtual {p0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v4

    const v4, 0x7f12146f

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    aput-object v0, v1, v3

    invoke-virtual {p0, v2, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "getString(\n            l\u2026    priceString\n        )"

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1c

    const/4 v11, 0x0

    invoke-static/range {v5 .. v11}, Lcom/discord/utilities/textprocessing/Parsers;->parseMaskedLinks$default(Landroid/content/Context;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function1;Ljava/lang/Integer;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void

    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getLegalese()Landroid/widget/TextView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private final configurePaymentInfo(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 22

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getRenewalInvoicePreview()Lcom/discord/models/domain/billing/ModelInvoicePreview;

    move-result-object v2

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v5

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v6

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x1

    if-nez v6, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    if-eq v6, v8, :cond_4

    if-eq v6, v7, :cond_4

    :goto_3
    const/4 v6, 0x0

    goto :goto_4

    :cond_4
    const/4 v6, 0x1

    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v10

    const/4 v11, 0x3

    const-string v12, ""

    const-string v13, "requireContext()"

    if-eqz v1, :cond_8

    sget-object v14, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodStart()Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    :cond_5
    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v6

    :goto_5
    move-object v15, v6

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    if-eq v3, v8, :cond_7

    if-eq v3, v7, :cond_6

    const/4 v3, 0x0

    const/16 v19, 0x0

    goto :goto_6

    :cond_6
    const/16 v3, 0x1e

    const/16 v19, 0x1e

    goto :goto_6

    :cond_7
    const/4 v3, 0x3

    const/16 v19, 0x3

    :goto_6
    const/16 v20, 0xc

    const/16 v21, 0x0

    move-object/from16 v16, v6

    invoke-static/range {v14 .. v21}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    goto :goto_7

    :cond_8
    move-object v3, v12

    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPaymentContainer()Landroid/view/View;

    move-result-object v6

    if-eqz v1, :cond_9

    const/4 v14, 0x1

    goto :goto_8

    :cond_9
    const/4 v14, 0x0

    :goto_8
    if-eqz v14, :cond_a

    const/4 v14, 0x0

    goto :goto_9

    :cond_a
    const/16 v14, 0x8

    :goto_9
    invoke-virtual {v6, v14}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingDivider()Landroid/view/View;

    move-result-object v6

    if-eqz v4, :cond_b

    const/4 v14, 0x0

    goto :goto_a

    :cond_b
    const/16 v14, 0x8

    :goto_a
    invoke-virtual {v6, v14}, Landroid/view/View;->setVisibility(I)V

    if-eqz v1, :cond_10

    invoke-static {v1}, Lcom/discord/views/ActiveSubscriptionView;->b(Lcom/discord/models/domain/ModelSubscription;)Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    move-result-object v6

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/billingclient/api/SkuDetails;

    if-eqz v14, :cond_c

    invoke-virtual {v14}, Lcom/android/billingclient/api/SkuDetails;->a()Ljava/lang/String;

    move-result-object v14

    goto :goto_b

    :cond_c
    const/4 v14, 0x0

    :goto_b
    if-eqz v5, :cond_d

    if-eqz v14, :cond_d

    move-object v12, v14

    goto :goto_d

    :cond_d
    sget-object v14, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    if-ne v6, v14, :cond_e

    const v6, 0x7f121438

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v12, "getString(R.string.premium_tier_2)"

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_c
    move-object v12, v6

    goto :goto_d

    :cond_e
    sget-object v14, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_CLASSIC:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    if-ne v6, v14, :cond_f

    const v6, 0x7f121436

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v12, "getString(R.string.premium_tier_1)"

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_c

    :cond_f
    sget-object v14, Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;->PREMIUM_GUILD:Lcom/discord/views/ActiveSubscriptionView$ActiveSubscriptionType;

    if-ne v6, v14, :cond_10

    const v6, 0x7f1213a3

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v12, "getString(R.string.premi\u2026guild_subscription_title)"

    invoke-static {v6, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_c

    :cond_10
    :goto_d
    if-eqz v4, :cond_11

    const v2, 0x7f120301

    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x0

    goto/16 :goto_11

    :cond_11
    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    if-eq v6, v9, :cond_15

    if-eq v6, v8, :cond_14

    if-eq v6, v11, :cond_13

    if-eq v6, v7, :cond_12

    goto :goto_10

    :cond_12
    const v2, 0x7f1213f8

    new-array v3, v9, [Ljava/lang/Object;

    sget-object v14, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual {v1}, Lcom/discord/models/domain/ModelSubscription;->getAccountHoldEstimatedExpirationTimestamp()J

    move-result-wide v15

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v18, 0x0

    const/16 v19, 0x4

    const/16 v20, 0x0

    move-object/from16 v17, v6

    invoke-static/range {v14 .. v20}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;JLandroid/content/Context;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v3, v7

    invoke-virtual {v0, v2, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_11

    :cond_13
    const/4 v7, 0x0

    const v2, 0x7f1213fe

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v3, v6, v7

    invoke-virtual {v0, v2, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_11

    :cond_14
    const/4 v7, 0x0

    const v2, 0x7f121401

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v12, v6, v7

    aput-object v3, v6, v9

    invoke-virtual {v0, v2, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_11

    :cond_15
    const/4 v7, 0x0

    const v6, 0x7f121404

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v7

    invoke-direct {v0, v1, v10}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getGoogleSubscriptionRenewalPrice(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_16

    goto :goto_f

    :cond_16
    if-eqz v2, :cond_17

    invoke-virtual {v2}, Lcom/discord/models/domain/billing/ModelInvoicePreview;->getTotal()I

    move-result v2

    goto :goto_e

    :cond_17
    const/4 v2, 0x0

    :goto_e
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    :goto_f
    aput-object v3, v8, v9

    invoke-virtual {v0, v6, v8}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_11

    :cond_18
    :goto_10
    const/4 v7, 0x0

    const/4 v2, 0x0

    :goto_11
    move-object v15, v2

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingInfoTv()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v15, :cond_19

    goto :goto_12

    :cond_19
    const/4 v9, 0x0

    :goto_12
    if-eqz v9, :cond_1a

    const/4 v3, 0x0

    goto :goto_13

    :cond_1a
    const/16 v3, 0x8

    :goto_13
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    if-eqz v15, :cond_1b

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingInfoTv()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14, v13}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x3c

    const/16 v21, 0x0

    invoke-static/range {v14 .. v21}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1b
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingBtn()Landroid/widget/Button;

    move-result-object v2

    if-eqz v4, :cond_1c

    const v3, 0x7f120300

    invoke-virtual {v0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_14

    :cond_1c
    const/4 v3, 0x0

    :goto_14
    invoke-static {v2, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingBtn()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configurePaymentInfo$1;

    invoke-direct {v3, v4}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configurePaymentInfo$1;-><init>(Z)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingGooglePlayManage()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v5, :cond_1d

    const/4 v3, 0x0

    goto :goto_15

    :cond_1d
    const/16 v3, 0x8

    :goto_15
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBillingGooglePlayManage()Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configurePaymentInfo$2;

    invoke-direct {v3, v5, v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configurePaymentInfo$2;-><init>(ZLcom/discord/models/domain/ModelSubscription;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureStatusNotice(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v3

    :goto_0
    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getStatus()Lcom/discord/models/domain/ModelSubscription$Status;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lcom/discord/models/domain/ModelSubscription$Status;->isAccountHold()Z

    move-result v7

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    :goto_2
    if-nez v4, :cond_4

    if-eqz v7, :cond_3

    goto :goto_3

    :cond_3
    const/4 v8, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 v8, 0x1

    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStatusNoticeContainer()Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    if-eqz v8, :cond_5

    const/4 v11, 0x0

    goto :goto_5

    :cond_5
    const/16 v11, 0x8

    :goto_5
    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    if-eqz v8, :cond_13

    if-nez v1, :cond_6

    goto/16 :goto_11

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v8

    const/4 v9, 0x2

    const/16 v11, 0xa

    const-string v12, "requireContext()"

    if-eqz v4, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getCurrentPeriodEnd()Ljava/lang/String;

    move-result-object v14

    sget-object v13, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1c

    const/16 v20, 0x0

    invoke-static/range {v13 .. v20}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/text/DateFormat;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v13

    if-eqz v13, :cond_7

    invoke-virtual {v13}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_7

    if-eqz v2, :cond_7

    invoke-interface {v2, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/billingclient/api/SkuDetails;

    goto :goto_6

    :cond_7
    move-object v2, v3

    :goto_6
    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->values()[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v13

    const/4 v14, 0x0

    :goto_7
    if-ge v14, v11, :cond_a

    aget-object v15, v13, v14

    invoke-virtual {v15}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v16

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v18

    if-eqz v18, :cond_8

    invoke-virtual/range {v18 .. v18}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getPlanId()J

    move-result-wide v18

    cmp-long v20, v16, v18

    if-nez v20, :cond_8

    const/16 v16, 0x1

    goto :goto_8

    :cond_8
    const/16 v16, 0x0

    :goto_8
    if-eqz v16, :cond_9

    move-object v3, v15

    goto :goto_9

    :cond_9
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    :cond_a
    :goto_9
    if-eqz v8, :cond_b

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_a

    :cond_b
    invoke-direct {v0, v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPlanString(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)Ljava/lang/String;

    move-result-object v2

    :goto_a
    const v3, 0x7f1213e6

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v2, v8, v6

    aput-object v4, v8, v5

    invoke-virtual {v0, v3, v8}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_f

    :cond_c
    if-eqz v7, :cond_11

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_e

    if-eqz v2, :cond_d

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/billingclient/api/SkuDetails;

    goto :goto_b

    :cond_d
    move-object v2, v3

    :goto_b
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/android/billingclient/api/SkuDetails;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_e

    goto :goto_e

    :cond_e
    invoke-static {}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->values()[Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v2

    const/4 v4, 0x0

    :goto_c
    if-ge v4, v11, :cond_10

    aget-object v8, v2, v4

    invoke-virtual {v8}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->getPlanId()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-static {v13}, Lf/h/a/f/f/n/g;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v13

    invoke-virtual {v1, v13}, Lcom/discord/models/domain/ModelSubscription;->hasAnyOfPlans(Ljava/util/Set;)Z

    move-result v13

    if-eqz v13, :cond_f

    move-object v3, v8

    goto :goto_d

    :cond_f
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_10
    :goto_d
    invoke-direct {v0, v3}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getPlanString(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)Ljava/lang/String;

    move-result-object v2

    :goto_e
    const v3, 0x7f1213fc

    new-array v4, v9, [Ljava/lang/Object;

    sget-object v13, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelSubscription;->getAccountHoldEstimatedExpirationTimestamp()J

    move-result-wide v14

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v17, 0x0

    const/16 v18, 0x4

    const/16 v19, 0x0

    move-object/from16 v16, v8

    invoke-static/range {v13 .. v19}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDate$default(Lcom/discord/utilities/time/TimeUtils;JLandroid/content/Context;IILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v6

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_f

    :cond_11
    const-string v2, ""

    :goto_f
    move-object v14, v2

    const-string/jumbo v2, "when {\n      hasRenewalM\u2026 }\n      else -> \"\"\n    }"

    invoke-static {v14, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStatusNoticeTv()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x3c

    const/16 v20, 0x0

    invoke-static/range {v13 .. v20}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStatusNoticeButton()Landroid/widget/Button;

    move-result-object v2

    if-eqz v7, :cond_12

    goto :goto_10

    :cond_12
    const/16 v6, 0x8

    :goto_10
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    if-eqz v7, :cond_13

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStatusNoticeButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f1213fb

    invoke-virtual {v0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(R.string.premi\u2026ge_payment_method_button)"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3c

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStatusNoticeButton()Landroid/widget/Button;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureStatusNotice$1;

    invoke-direct {v3, v0, v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$configureStatusNotice$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_13
    :goto_11
    return-void
.end method

.method private final getActiveGuildSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->activeGuildSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/ActiveSubscriptionView;

    return-object v0
.end method

.method private final getActiveSubscriptionView()Lcom/discord/views/ActiveSubscriptionView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->activeSubscriptionView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/ActiveSubscriptionView;

    return-object v0
.end method

.method private final getBillingBtn()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getBillingDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBillingGooglePlayManage()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingGooglePlayManage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBillingInfoTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->billingInfoTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostCountText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->boostCountText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBoostDiscountText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->boostDiscountText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getBuyTier1Container()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier1Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBuyTier1Monthly()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier1Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getBuyTier2Container()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier2Container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getBuyTier2Monthly()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->buyTier2Monthly$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getCreditContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x15

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCreditDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCreditNitro()Lcom/discord/views/premium/AccountCreditView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditNitro$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premium/AccountCreditView;

    return-object v0
.end method

.method private final getCreditNitroClassic()Lcom/discord/views/premium/AccountCreditView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->creditNitroClassic$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/premium/AccountCreditView;

    return-object v0
.end method

.method private final getGoogleSubscriptionRenewalPrice(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/android/billingclient/api/SkuDetails;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;->getPaymentGatewayPlanId()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/billingclient/api/SkuDetails;

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getRenewalMutations()Lcom/discord/models/domain/ModelSubscription$SubscriptionRenewalMutations;

    move-result-object p1

    if-nez p1, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object v1

    :cond_3
    :goto_1
    return-object v1
.end method

.method private final getGrandfathered()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->grandfathered$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getLegalese()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->legalese$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPaymentContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->paymentContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPlanString(Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_4

    const/4 v0, 0x5

    if-eq p1, v0, :cond_3

    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    const/4 v0, 0x7

    if-eq p1, v0, :cond_1

    :goto_0
    const-string p1, ""

    goto :goto_1

    :cond_1
    const p1, 0x7f1213ed

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const p1, 0x7f1213e9

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_3
    const p1, 0x7f1213ec

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_4
    const p1, 0x7f1213e8

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    const-string/jumbo v0, "when (planType) {\n      \u2026       else -> \"\"\n      }"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getPremiumGuildSubscriptionViewCallbacks(ZLjava/lang/String;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;
    .locals 12

    if-eqz p1, :cond_0

    new-instance p1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    new-instance v4, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$1;

    invoke-direct {v4, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    new-instance v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$2;

    invoke-direct {v1, p0, p2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/String;)V

    new-instance v2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;

    invoke-direct {v2, p0, p2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$3;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/String;)V

    new-instance v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$4;

    invoke-direct {v3, p0, p2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$4;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/String;)V

    new-instance v5, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$5;

    invoke-direct {v5, p0, p2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$5;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/String;)V

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    new-instance v10, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$6;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumGuildSubscriptionViewCallbacks$6;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    move-object v6, p1

    invoke-direct/range {v6 .. v11}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-object p1
.end method

.method private final getPremiumSubscriptionViewCallbacks(Lcom/discord/models/domain/ModelSubscription;ZLjava/util/List;)Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelSubscription;",
            "Z",
            "Ljava/util/List<",
            "+",
            "Lcom/android/billingclient/api/Purchase;",
            ">;)",
            "Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->isGoogleSubscription()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p2, :cond_0

    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$manageBundledPremiumGuildCallback$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$manageBundledPremiumGuildCallback$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v4, p2

    instance-of p2, p3, Ljava/util/Collection;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/android/billingclient/api/Purchase;

    invoke-virtual {p3}, Lcom/android/billingclient/api/Purchase;->c()Z

    move-result p3

    xor-int/2addr p3, v0

    if-eqz p3, :cond_3

    :goto_1
    if-eqz v0, :cond_4

    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$managePlanCallback$2;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$managePlanCallback$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    goto :goto_2

    :cond_4
    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$managePlanCallback$3;

    invoke-direct {p2, p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$managePlanCallback$3;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V

    :goto_2
    move-object v2, p2

    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    new-instance v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$1;

    invoke-direct {v1, p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V

    new-instance v3, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$2;

    invoke-direct {v3, p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V

    new-instance v5, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$3;

    invoke-direct {v5, p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$3;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Lcom/discord/models/domain/ModelSubscription;)V

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    goto :goto_3

    :cond_5
    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->isAppleSubscription()Z

    move-result p1

    if-eqz p1, :cond_6

    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    goto :goto_3

    :cond_6
    new-instance p2, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$4;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$4;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    new-instance v9, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$5;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$getPremiumSubscriptionViewCallbacks$5;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, p2

    invoke-direct/range {v6 .. v11}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$Companion$SubscriptionViewCallbacks;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    :goto_3
    return-object p2
.end method

.method private final getPriceText(Lcom/android/billingclient/api/SkuDetails;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_0

    const v0, 0x7f121407

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/android/billingclient/api/SkuDetails;->b()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f12176e

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(R.string.stream_premium_upsell_cta)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method private final getPriceText(ZI)Ljava/lang/String;
    .locals 4

    if-eqz p1, :cond_0

    const p1, 0x7f12031e

    goto :goto_0

    :cond_0
    const p1, 0x7f120324

    :goto_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "requireContext()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, v2}, Lcom/discord/utilities/billing/PremiumUtilsKt;->getFormattedPrice(ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p2

    aput-object p2, v0, v1

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "getString(billingInterva\u2026mount, requireContext()))"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getRetryButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->retryButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getScrollView()Landroid/widget/ScrollView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method private final getStatusNoticeButton()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->statusNoticeButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1b

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getStatusNoticeContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->statusNoticeContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStatusNoticeTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->statusNoticeTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1a

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getStickersPerks()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->stickersPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSubscriptionContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->subscriptionContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getUploadPerks()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->uploadPerks$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getViewFlipper()Landroid/widget/ViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event;)V
    .locals 2

    instance-of v0, p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Event$ErrorToast;->getErrorStringResId()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_0
    return-void
.end method

.method private final scrollToSection(Ljava/lang/Integer;)V
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier1Container()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    add-int/2addr v1, p1

    goto :goto_2

    :cond_1
    :goto_0
    const/4 v1, 0x1

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v1, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBuyTier2Container()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, 0x0

    :goto_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getScrollView()Landroid/widget/ScrollView;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    return-void
.end method

.method private final scrollToTop()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getScrollView()Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Landroid/widget/ScrollView;->scrollTo(II)V

    return-void
.end method

.method private final showCancelConfirmationAlert(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V
    .locals 17

    move-object/from16 v0, p0

    sget-object v1, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f1212e4

    invoke-virtual {v0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lf/a/b/g;->a:Lf/a/b/g;

    const/4 v6, 0x0

    const-wide v7, 0x53d4f93245L

    invoke-virtual {v5, v7, v8, v6}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const v5, 0x7f1212dc

    invoke-virtual {v0, v5, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(\n            R\u2026sk.GOOGLE_PLAY)\n        )"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1212e3

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f1210ce

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0a06fc

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$showCancelConfirmationAlert$1;

    move-object/from16 v9, p1

    invoke-direct {v8, v9}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$showCancelConfirmationAlert$1;-><init>(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;)V

    new-instance v9, Lkotlin/Pair;

    invoke-direct {v9, v7, v8}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v9}, Lf/h/a/f/f/n/g;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const v11, 0x7f040445

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1dc0

    const/16 v16, 0x0

    invoke-static/range {v1 .. v16}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showContent(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V
    .locals 13

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscription;->getPlanType()Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/models/domain/ModelSubscriptionPlan$SubscriptionPlanType;->isPremiumSubscription()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelSubscription;->isNonePlan()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Attempting to open WidgetSettingsPremium with non-Premium "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "and non-PremiumGuild subscription: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelSubscription;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string p1, "StringBuilder()\n        \u2026}\")\n          .toString()"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_2
    return-void

    :cond_3
    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureActiveSubscriptionView(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureGrandfatheredHeader(Lcom/discord/models/domain/ModelSubscription;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureLegalese(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureButtons(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getEntitlements()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureAccountCredit(Ljava/util/List;Lcom/discord/models/domain/ModelSubscription;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configurePaymentInfo(Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getPremiumSubscription()Lcom/discord/models/domain/ModelSubscription;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$ViewState$Loaded;->getSkuDetails()Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureStatusNotice(Lcom/discord/models/domain/ModelSubscription;Ljava/util/Map;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, -0x1

    const-string v2, "intent_section"

    if-eqz p1, :cond_4

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_2

    :cond_4
    move-object p1, v1

    :goto_2
    if-nez p1, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v0, :cond_6

    :goto_3
    const-wide/16 v3, 0x12c

    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v3, v4, v0}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v3, "Observable\n            .\u20260, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-static {v0, p0, v1, v3, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$showContent$1;

    invoke-direct {v10, p0, p1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$showContent$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/lang/Integer;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    move-object v5, p1

    sget-object v3, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    const-string v4, "Error Scrolling to section"

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_4
    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method private final showDesktopManageAlert()V
    .locals 17

    move-object/from16 v0, p0

    sget-object v1, Lcom/discord/widgets/notice/WidgetNoticeDialog;->Companion:Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    const-string v3, "parentFragmentManager"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f120300

    invoke-virtual {v0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lf/a/b/g;->a:Lf/a/b/g;

    const/4 v6, 0x0

    const-wide v7, 0x53d4f93245L

    invoke-virtual {v5, v7, v8, v6}, Lf/a/b/g;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const v5, 0x7f1213c1

    invoke-virtual {v0, v5, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "getString(\n            R\u2026sk.GOOGLE_PLAY)\n        )"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f121377

    invoke-virtual {v0, v5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1ff0

    const/16 v16, 0x0

    invoke-static/range {v1 .. v16}, Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;->show$default(Lcom/discord/widgets/notice/WidgetNoticeDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/discord/stores/StoreNotices$Dialog$Type;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;IILjava/lang/Object;)V

    return-void
.end method

.method private final showFailureUI()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getRetryButton()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$showFailureUI$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$showFailureUI$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final showLoadingUI()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getViewFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->scrollToTop()V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d02ae

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/discord/app/AppFragment;->onActivityResult(IILandroid/content/Intent;)V

    const p2, 0xfbbc

    if-ne p1, p2, :cond_3

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    const/4 p2, 0x0

    const-string p3, "viewModel"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->getLaunchPremiumTabStartTimeMs()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iget-object p1, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->getLaunchPremiumTabStartTimeMs()J

    move-result-wide p1

    sub-long v2, v0, p1

    goto :goto_0

    :cond_0
    invoke-static {p3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_1
    :goto_0
    sget-object p1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string p2, "Premium Purchase Tab"

    invoke-virtual {p1, p2, v2, v3}, Lcom/discord/utilities/analytics/AnalyticsTracker;->externalViewClosed(Ljava/lang/String;J)V

    goto :goto_1

    :cond_2
    invoke-static {p3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw p2

    :cond_3
    :goto_1
    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->connection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->connection:Landroid/content/ServiceConnection;

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onResume()V

    sget-object v0, Lcom/discord/utilities/premium/PremiumUtils;->INSTANCE:Lcom/discord/utilities/premium/PremiumUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/premium/PremiumUtils;->warmupBillingTabs(Landroid/content/Context;)Landroidx/browser/customtabs/CustomTabsServiceConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->connection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 8

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    const p1, 0x7f121971

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(I)Lkotlin/Unit;

    const p1, 0x7f12143e

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getUploadPerks()Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const v4, 0x7f12074c

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p1

    const v4, 0x7f12074b

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const v4, 0x7f12074a

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v3, v5

    const v4, 0x7f1212f5

    invoke-virtual {p0, v4, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStickersPerks()Landroid/widget/TextView;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    const/16 v4, 0x21

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p1

    const v4, 0x7f1212fb

    invoke-virtual {p0, v4, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getStickersPerks()Landroid/widget/TextView;

    move-result-object v2

    sget-object v3, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->Companion:Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag$Companion;->getINSTANCE()Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;

    move-result-object v3

    invoke-virtual {v3}, Lcom/discord/widgets/chat/input/sticker/StickerPickerFeatureFlag;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBoostDiscountText()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f120bcf

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v6

    const v7, 0x3e99999a    # 0.3f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, p1

    invoke-virtual {p0, v3, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f10008d

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, p1

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "resources.getQuantityStr\u2026PTIONS_WITH_PREMIUM\n    )"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->getBoostCountText()Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f120bd0

    new-array v5, v0, [Ljava/lang/Object;

    aput-object v2, v5, p1

    invoke-virtual {p0, v4, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextAndVisibilityBy(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-static {p0, v1, v0, v1}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->configureButtonText$default(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;Ljava/util/Map;ILjava/lang/Object;)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;

    invoke-direct {v2}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel$Factory;-><init>()V

    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v1, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object v0

    const-string v1, "ViewModelProvider(requir\u2026iumViewModel::class.java)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    iput-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    const-string v1, "viewModel"

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    const-wide/16 v3, 0xc8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v5}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Observable;->q()Lrx/Observable;

    move-result-object v0

    const-string v3, "viewModel\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    new-instance v10, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-class v5, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;->viewModel:Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/settings/premium/SettingsPremiumViewModel;->getEventSubject()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    new-instance v10, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$2;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-class v5, Lcom/discord/widgets/settings/premium/WidgetSettingsPremium;

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method
