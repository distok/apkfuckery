.class public final synthetic Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->values()[Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v0, 0x6

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GUILD:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v1, v2

    sget-object v4, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->DM:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v4, 0x2

    aput v4, v1, v3

    sget-object v5, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GROUP_DM:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v5, 0x3

    aput v5, v1, v4

    sget-object v6, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GUILD_CHANNEL:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v6, 0x4

    aput v6, v1, v5

    sget-object v7, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->CATEGORY:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v7, 0x5

    aput v7, v1, v6

    invoke-static {}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->values()[Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    new-array v1, v0, [I

    sput-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$WhenMappings;->$EnumSwitchMapping$1:[I

    aput v3, v1, v4

    aput v4, v1, v3

    invoke-static {}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->values()[Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$WhenMappings;->$EnumSwitchMapping$2:[I

    aput v3, v0, v3

    aput v4, v0, v4

    aput v5, v0, v2

    aput v6, v0, v6

    return-void
.end method
