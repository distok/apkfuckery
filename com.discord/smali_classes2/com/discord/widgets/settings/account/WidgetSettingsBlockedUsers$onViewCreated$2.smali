.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$2;
.super Lx/m/c/k;
.source "WidgetSettingsBlockedUsers.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$2;->invoke(Lcom/discord/models/domain/ModelUser;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelUser;)V
    .locals 3

    const-string v0, "user"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    invoke-static {v0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->access$getViewModel$p(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->onClickUnblock(J)V

    return-void
.end method
