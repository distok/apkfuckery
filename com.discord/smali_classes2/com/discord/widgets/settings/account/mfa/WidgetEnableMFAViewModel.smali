.class public final Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;
.super Lf/a/b/l0;
.source "WidgetEnableMFAViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;,
        Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private encodedTotpSecret:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private subs:Lrx/subscriptions/CompositeSubscription;

.field private totpSecret:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    new-instance v0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;-><init>(ZLjava/lang/Integer;)V

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->subs:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method public static final synthetic access$getSubs$p(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;)Lrx/subscriptions/CompositeSubscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->subs:Lrx/subscriptions/CompositeSubscription;

    return-object p0
.end method

.method public static final synthetic access$handleMFAFailure(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->handleMFAFailure()V

    return-void
.end method

.method public static final synthetic access$handleMFASuccess(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->handleMFASuccess(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setSubs$p(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;Lrx/subscriptions/CompositeSubscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->subs:Lrx/subscriptions/CompositeSubscription;

    return-void
.end method

.method private final handleMFAFailure()V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    new-instance v0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;-><init>(ZLjava/lang/Integer;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleMFASuccess(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAuthentication()Lcom/discord/stores/StoreAuthentication;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/discord/stores/StoreAuthentication;->setAuthed(Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p1, v2, v1}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;-><init>(ZLjava/lang/Integer;)V

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMFA()Lcom/discord/stores/StoreMFA;

    move-result-object p1

    sget-object v0, Lcom/discord/stores/StoreMFA$MFAActivationState;->PENDING_ENABLED:Lcom/discord/stores/StoreMFA$MFAActivationState;

    invoke-virtual {p1, v0}, Lcom/discord/stores/StoreMFA;->updatePendingMFAState(Lcom/discord/stores/StoreMFA$MFAActivationState;)V

    return-void
.end method


# virtual methods
.method public final enableMFA(Landroid/content/Context;Ljava/lang/String;)V
    .locals 12
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mfaCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$ViewState;-><init>(ZLjava/lang/Integer;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    sget-object v0, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v0}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v0

    new-instance v3, Lcom/discord/restapi/RestAPIParams$EnableMFA;

    iget-object v4, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->encodedTotpSecret:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->password:Ljava/lang/String;

    if-eqz v5, :cond_0

    invoke-direct {v3, p2, v4, v5}, Lcom/discord/restapi/RestAPIParams$EnableMFA;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/discord/utilities/rest/RestAPI;->enableMFA(Lcom/discord/restapi/RestAPIParams$EnableMFA;)Lrx/Observable;

    move-result-object p2

    const-wide/16 v3, 0x7d0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v3, v4, v0}, Lrx/Observable;->p(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p2

    const-string v0, "RestAPI\n        .api\n   \u20260, TimeUnit.MILLISECONDS)"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {p2, v0, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p2

    const/4 v0, 0x2

    invoke-static {p2, p0, v2, v0, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    new-instance v9, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$enableMFA$1;

    invoke-direct {v9, p0}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$enableMFA$1;-><init>(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;)V

    new-instance v7, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$enableMFA$2;

    invoke-direct {v7, p0}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$enableMFA$2;-><init>(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;)V

    const-class v4, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;

    new-instance v6, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$enableMFA$3;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel$enableMFA$3;-><init>(Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;)V

    const/4 v8, 0x0

    const/16 v10, 0x10

    const/4 v11, 0x0

    move-object v5, p1

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final getPassword()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->password:Ljava/lang/String;

    return-object v0
.end method

.method public final getTotpSecret()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->totpSecret:Ljava/lang/String;

    return-object v0
.end method

.method public onCleared()V
    .locals 1

    invoke-super {p0}, Lf/a/b/l0;->onCleared()V

    iget-object v0, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->b()V

    return-void
.end method

.method public final setPassword(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "password"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->password:Ljava/lang/String;

    return-void
.end method

.method public final setTotpSecret(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    const-string v0, "secret"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->totpSecret:Ljava/lang/String;

    sget-object v0, Lcom/discord/utilities/auth/AuthUtils;->INSTANCE:Lcom/discord/utilities/auth/AuthUtils;

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/discord/utilities/auth/AuthUtils;->encodeTotpSecret(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/account/mfa/WidgetEnableMFAViewModel;->encodedTotpSecret:Ljava/lang/String;

    :cond_0
    return-void
.end method
