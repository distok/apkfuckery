.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;
.super Lcom/discord/app/AppFragment;
.source "WidgetSettingsBlockedUsers.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$Companion;

.field private static final VIEW_INDEX_EMPTY:I = 0x1

.field private static final VIEW_INDEX_RECYCLER:I


# instance fields
.field private adapter:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

.field private final blockedUsersRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    const-string v3, "flipper"

    const-string v4, "getFlipper()Lcom/discord/app/AppViewFlipper;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    const-string v6, "blockedUsersRecycler"

    const-string v7, "getBlockedUsersRecycler()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->Companion:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a00fd

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a00fe

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->blockedUsersRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$configureUI(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->configureUI(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->viewModel:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->handleEvent(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->viewModel:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Uninitialized;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Empty;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->showEmptyView()V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->showBlockedUsers(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;)V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getBlockedUsersRecycler()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->blockedUsersRecycler$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->flipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->handleShowToast(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final handleShowToast(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;)V
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;->getStringRes()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    return-void
.end method

.method private final showBlockedUsers(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->adapter:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;->getItems()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;->setData(Ljava/util/List;)V

    return-void

    :cond_0
    const-string p1, "adapter"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p1, 0x0

    throw p1
.end method

.method private final showEmptyView()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->getFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0298

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory;

    invoke-direct {v0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory;-><init>()V

    invoke-direct {p1, p0, v0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(this, \u2026ewModel::class.java\n    )"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->viewModel:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 13

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->viewModel:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    const-string v1, "viewModel"

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewBoundOrOnResume$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->viewModel:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->observeEvents()Lrx/Observable;

    move-result-object v0

    invoke-static {v0, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v10, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewBoundOrOnResume$2;

    invoke-direct {v10, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)V

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const p1, 0x7f121994    # 1.942001E38f

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    const/4 p1, 0x0

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->getBlockedUsersRecycler()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    sget-object p2, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;->Companion:Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;

    new-instance v1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    invoke-direct {v1, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p2, v1}, Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter$Companion;->configure(Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;)Lcom/discord/utilities/mg_recycler/MGRecyclerAdapter;

    move-result-object p1

    check-cast p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->adapter:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    const-string p2, "adapter"

    if-eqz p1, :cond_1

    new-instance v1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$2;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)V

    invoke-virtual {p1, v1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;->setOnClickUnblock(Lkotlin/jvm/functions/Function1;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;->adapter:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    if-eqz p1, :cond_0

    new-instance p2, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$3;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers$onViewCreated$3;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsers;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;->setOnClickUserProfile(Lkotlin/jvm/functions/Function1;)V

    return-void

    :cond_0
    invoke-static {p2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
