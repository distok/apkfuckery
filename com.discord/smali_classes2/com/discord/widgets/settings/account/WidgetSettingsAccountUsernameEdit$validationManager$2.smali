.class public final Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$validationManager$2;
.super Lx/m/c/k;
.source "WidgetSettingsAccountUsernameEdit.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/discord/utilities/view/validators/ValidationManager;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$validationManager$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/discord/utilities/view/validators/ValidationManager;
    .locals 10

    new-instance v0, Lcom/discord/utilities/view/validators/ValidationManager;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/discord/utilities/view/validators/Input;

    new-instance v2, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;

    iget-object v3, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$validationManager$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    invoke-static {v3}, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;->access$getUsernameWrap$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    sget-object v6, Lcom/discord/utilities/view/validators/BasicTextInputValidator;->Companion:Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;

    const v7, 0x7f1219f3

    invoke-virtual {v6, v7}, Lcom/discord/utilities/view/validators/BasicTextInputValidator$Companion;->createRequiredInputValidator(I)Lcom/discord/utilities/view/validators/BasicTextInputValidator;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const-string v6, "username"

    invoke-direct {v2, v6, v3, v5}, Lcom/discord/utilities/view/validators/Input$TextInputLayoutInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/discord/utilities/view/validators/Input$EditTextInput;

    iget-object v3, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$validationManager$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    invoke-static {v3}, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;->access$getDiscriminator$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v3

    new-array v5, v4, [Lcom/discord/utilities/view/validators/InputValidator;

    sget-object v6, Lcom/discord/utilities/auth/AuthUtils;->INSTANCE:Lcom/discord/utilities/auth/AuthUtils;

    const v8, 0x7f1212ea

    const v9, 0x7f1212f0

    invoke-virtual {v6, v8, v9}, Lcom/discord/utilities/auth/AuthUtils;->createDiscriminatorInputValidator(II)Lcom/discord/utilities/view/validators/InputValidator;

    move-result-object v6

    aput-object v6, v5, v7

    const-string v6, "discriminator"

    invoke-direct {v2, v6, v3, v5}, Lcom/discord/utilities/view/validators/Input$EditTextInput;-><init>(Ljava/lang/String;Lcom/google/android/material/textfield/TextInputLayout;[Lcom/discord/utilities/view/validators/InputValidator;)V

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/discord/utilities/view/validators/ValidationManager;-><init>([Lcom/discord/utilities/view/validators/Input;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$validationManager$2;->invoke()Lcom/discord/utilities/view/validators/ValidationManager;

    move-result-object v0

    return-object v0
.end method
