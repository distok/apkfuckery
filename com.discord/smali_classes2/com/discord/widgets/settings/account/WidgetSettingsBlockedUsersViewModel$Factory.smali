.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetSettingsBlockedUsersViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    invoke-virtual {p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;-><init>(Lrx/Observable;Lcom/discord/utilities/rest/RestAPI;)V

    return-object p1
.end method

.method public final observeStores()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserRelationships()Lcom/discord/stores/StoreUserRelationships;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserRelationships;->observe()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory$observeStores$1;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory$observeStores$1;

    invoke-virtual {v0, v1}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v0

    const-string v1, "StoreStream\n            \u2026          }\n            }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
