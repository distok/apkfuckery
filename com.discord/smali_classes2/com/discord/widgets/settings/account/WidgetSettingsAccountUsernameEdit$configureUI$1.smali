.class public final Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1;
.super Ljava/lang/Object;
.source "WidgetSettingsAccountUsernameEdit.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;->configureUI(Lcom/discord/models/domain/ModelUser;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 11

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    invoke-static {p1}, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;->access$getDiscriminator$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1$1;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1;)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    sget-object v0, Lf/a/a/e/c;->n:Lf/a/a/e/c$b;

    iget-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string p1, "parentFragmentManager"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    iget-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit$configureUI$1;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccountUsernameEdit;

    const p2, 0x7f12145e

    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1c8

    const-string v5, "User Settings"

    const-string v6, "Edit Account"

    invoke-static/range {v0 .. v10}, Lf/a/a/e/c$b;->a(Lf/a/a/e/c$b;Landroidx/fragment/app/FragmentManager;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;I)V

    :cond_0
    return-void
.end method
