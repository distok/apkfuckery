.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;
.super Lf/a/b/l0;
.source "WidgetSettingsBlockedUsersViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState;,
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event;,
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;,
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;,
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Factory;,
        Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState;",
        ">;"
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Companion;

.field private static final LOCATION:Ljava/lang/String; = "Blocked Users List"

.field public static final VIEW_TYPE_BLOCKED_USER_ITEM:I


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->Companion:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Companion;

    return-void
.end method

.method public constructor <init>(Lrx/Observable;Lcom/discord/utilities/rest/RestAPI;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;",
            ">;",
            "Lcom/discord/utilities/rest/RestAPI;",
            ")V"
        }
    .end annotation

    const-string v0, "storeObservable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restAPI"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Uninitialized;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Uninitialized;

    invoke-direct {p0, v0}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->storeObservable:Lrx/Observable;

    iput-object p2, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object p2

    iput-object p2, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const/4 p2, 0x0

    const/4 v0, 0x2

    invoke-static {p1, p0, p2, v0, p2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    new-instance v7, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$1;

    invoke-direct {v7, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$1;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$emitShowToastEvent(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->emitShowToastEvent(I)V

    return-void
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->handleStoreState(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;)V

    return-void
.end method

.method private final emitShowToastEvent(I)V
    .locals 2
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    new-instance v1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;

    invoke-direct {v1, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event$ShowToast;-><init>(I)V

    iget-object p1, v0, Lrx/subjects/PublishSubject;->e:Lrx/subjects/PublishSubject$b;

    invoke-virtual {p1, v1}, Lrx/subjects/PublishSubject$b;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private final getItems(Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lx/h/f;->asSequence(Ljava/util/Map;)Lkotlin/sequences/Sequence;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;

    invoke-static {p1, v0}, Lx/r/q;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    sget-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$2;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$2;

    invoke-static {p1, v0}, Lx/r/q;->sortedWith(Lkotlin/sequences/Sequence;Ljava/util/Comparator;)Lkotlin/sequences/Sequence;

    move-result-object p1

    invoke-static {p1}, Lx/r/q;->toList(Lkotlin/sequences/Sequence;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final handleStoreState(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;)V
    .locals 1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;->getUsers()Ljava/util/Map;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->getItems(Ljava/util/Map;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Empty;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Empty;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;

    invoke-direct {v0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$ViewState$Loaded;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final getRestAPI()Lcom/discord/utilities/rest/RestAPI;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    return-object v0
.end method

.method public final getStoreObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->storeObservable:Lrx/Observable;

    return-object v0
.end method

.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onClickUnblock(J)V
    .locals 11

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    const-string v1, "Blocked Users List"

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->removeRelationship(Ljava/lang/String;J)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;

    new-instance v8, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$onClickUnblock$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$onClickUnblock$1;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;)V

    new-instance v6, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$onClickUnblock$2;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$onClickUnblock$2;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x16

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method
