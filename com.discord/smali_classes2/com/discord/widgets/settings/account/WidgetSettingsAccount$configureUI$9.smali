.class public final Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;
.super Ljava/lang/Object;
.source "WidgetSettingsAccount.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->configureUI(Lcom/discord/widgets/settings/account/WidgetSettingsAccount$Model;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $user:Lcom/discord/models/domain/ModelUser;

.field public final synthetic this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/account/WidgetSettingsAccount;Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    iput-object p2, p0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v1}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->access$getState$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccount;)Lcom/discord/utilities/stateful/StatefulViews;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v2}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->access$getAccountAvatar$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccount;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/discord/utilities/stateful/StatefulViews;->hasChanged(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApiSerializeNulls()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v1

    :goto_0
    new-instance v15, Lcom/discord/restapi/RestAPIParams$UserInfo;

    iget-object v2, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v2}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->access$getState$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccount;)Lcom/discord/utilities/stateful/StatefulViews;

    move-result-object v2

    iget-object v3, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v3}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->access$getAccountAvatar$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccount;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getId()I

    move-result v3

    const/4 v14, 0x0

    invoke-virtual {v2, v3, v14}, Lcom/discord/utilities/stateful/StatefulViews;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    iget-object v2, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getUsername()Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    iget-object v2, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-virtual {v2}, Lcom/discord/models/domain/ModelUser;->getEmail()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    sget-object v2, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v2}, Lcom/discord/stores/StoreStream$Companion;->getNotifications()Lcom/discord/stores/StoreNotifications;

    move-result-object v2

    invoke-virtual {v2}, Lcom/discord/stores/StoreNotifications;->getPushToken()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x384

    const/16 v16, 0x0

    move-object v2, v15

    move-object v0, v14

    move-object/from16 v14, v16

    invoke-direct/range {v2 .. v14}, Lcom/discord/restapi/RestAPIParams$UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v1, v15}, Lcom/discord/utilities/rest/RestAPI;->patchUser(Lcom/discord/restapi/RestAPIParams$UserInfo;)Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, v3, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    move-object v2, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    const/4 v4, 0x2

    invoke-static {v1, v3, v2, v4, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    iget-object v2, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v2}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount;->access$getDimmer$p(Lcom/discord/widgets/settings/account/WidgetSettingsAccount;)Lcom/discord/utilities/dimmer/DimmerView;

    move-result-object v2

    const-wide/16 v5, 0x0

    invoke-static {v2, v5, v6, v4}, Lf/a/b/r;->r(Lcom/discord/utilities/dimmer/DimmerView;JI)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9$1;

    invoke-direct {v2, v0}, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9$1;-><init>(Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;)V

    iget-object v3, v0, Lcom/discord/widgets/settings/account/WidgetSettingsAccount$configureUI$9;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsAccount;

    invoke-static {v2, v3}, Lf/a/b/r;->m(Lrx/functions/Action1;Lcom/discord/app/AppFragment;)Lrx/Observable$c;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
