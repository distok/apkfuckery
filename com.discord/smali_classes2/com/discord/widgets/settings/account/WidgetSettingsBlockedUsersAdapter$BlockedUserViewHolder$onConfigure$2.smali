.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$2;
.super Ljava/lang/Object;
.source "WidgetSettingsBlockedUsersAdapter.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;->onConfigure(ILcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $user:Lcom/discord/models/domain/ModelUser;

.field public final synthetic this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;Lcom/discord/models/domain/ModelUser;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;

    iput-object p2, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$2;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$2;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;

    invoke-static {p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;->access$getAdapter$p(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;->getOnClickUnblock()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$2;->$user:Lcom/discord/models/domain/ModelUser;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
