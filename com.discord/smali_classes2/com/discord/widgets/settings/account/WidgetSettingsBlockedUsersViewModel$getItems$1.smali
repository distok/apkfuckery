.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;
.super Lx/m/c/k;
.source "WidgetSettingsBlockedUsersViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel;->getItems(Ljava/util/Map;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/Map$Entry<",
        "+",
        "Ljava/lang/Long;",
        "+",
        "Lcom/discord/models/domain/ModelUser;",
        ">;",
        "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;

    invoke-direct {v0}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;-><init>()V

    sput-object v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;->INSTANCE:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/util/Map$Entry;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/Long;",
            "+",
            "Lcom/discord/models/domain/ModelUser;",
            ">;)",
            "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;"
        }
    .end annotation

    const-string v0, "userEntry"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-direct {v0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;-><init>(Lcom/discord/models/domain/ModelUser;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Map$Entry;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$getItems$1;->invoke(Ljava/util/Map$Entry;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;

    move-result-object p1

    return-object p1
.end method
