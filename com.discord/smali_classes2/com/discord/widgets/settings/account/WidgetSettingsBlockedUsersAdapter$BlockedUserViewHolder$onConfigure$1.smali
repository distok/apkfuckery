.class public final Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$1;
.super Ljava/lang/Object;
.source "WidgetSettingsBlockedUsersAdapter.kt"

# interfaces
.implements Lrx/functions/Action3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;->onConfigure(ILcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action3<",
        "Landroid/view/View;",
        "Ljava/lang/Integer;",
        "Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$1;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;)V
    .locals 0

    invoke-virtual {p3}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;->component1()Lcom/discord/models/domain/ModelUser;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$1;->this$0:Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;

    invoke-static {p2}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;->access$getAdapter$p(Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder;)Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;

    move-result-object p2

    invoke-virtual {p2}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter;->getOnClickUserProfile()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/view/View;

    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersAdapter$BlockedUserViewHolder$onConfigure$1;->call(Landroid/view/View;Ljava/lang/Integer;Lcom/discord/widgets/settings/account/WidgetSettingsBlockedUsersViewModel$Item;)V

    return-void
.end method
