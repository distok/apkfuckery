.class public final Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;
.super Ljava/lang/Object;
.source "WidgetSettingsPrivacy.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->configureRequestDataButton(ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $harvestState:Lcom/discord/utilities/rest/RestAPI$HarvestState;

.field public final synthetic $isMeVerified:Z

.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettingsPrivacy;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;ZLcom/discord/utilities/rest/RestAPI$HarvestState;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    iput-boolean p2, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->$isMeVerified:Z

    iput-object p3, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->$harvestState:Lcom/discord/utilities/rest/RestAPI$HarvestState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string v0, "context ?: return@setOnClickListener"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->$isMeVerified:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->$harvestState:Lcom/discord/utilities/rest/RestAPI$HarvestState;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    invoke-static {v1, p1, v0}, Lcom/discord/widgets/settings/WidgetSettingsPrivacy;->access$onRequestDataClick(Lcom/discord/widgets/settings/WidgetSettingsPrivacy;Landroid/content/Context;Lcom/discord/utilities/rest/RestAPI$HarvestState;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsPrivacy$configureRequestDataButton$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsPrivacy;

    const v0, 0x7f1205d0

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {p1, v0, v1, v2}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :cond_1
    :goto_0
    return-void
.end method
