.class public final Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;
.super Ljava/lang/Object;
.source "WidgetChangeLog.kt"

# interfaces
.implements Landroidx/core/widget/NestedScrollView$OnScrollChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetChangeLog;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetChangeLog;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScrollChange(Landroidx/core/widget/NestedScrollView;IIII)V
    .locals 0

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$getMaxScrolledPercent$p(Lcom/discord/widgets/settings/WidgetChangeLog;)I

    move-result p2

    mul-int/lit8 p3, p3, 0x64

    iget-object p4, p0, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p4}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$getScrollView$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroidx/core/widget/NestedScrollView;

    move-result-object p4

    invoke-static {p4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getContentView(Landroidx/core/widget/NestedScrollView;)Landroid/view/View;

    move-result-object p4

    invoke-virtual {p4}, Landroid/view/View;->getHeight()I

    move-result p4

    iget-object p5, p0, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p5}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$getScrollView$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroidx/core/widget/NestedScrollView;

    move-result-object p5

    invoke-virtual {p5}, Landroid/widget/FrameLayout;->getHeight()I

    move-result p5

    sub-int/2addr p4, p5

    const/4 p5, 0x1

    if-ge p4, p5, :cond_0

    const/4 p4, 0x1

    :cond_0
    div-int/2addr p3, p4

    if-ge p2, p3, :cond_1

    move p2, p3

    :cond_1
    invoke-static {p1, p2}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$setMaxScrolledPercent$p(Lcom/discord/widgets/settings/WidgetChangeLog;I)V

    return-void
.end method
