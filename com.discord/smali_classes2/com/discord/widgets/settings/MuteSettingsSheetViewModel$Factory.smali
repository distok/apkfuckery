.class public final Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "MuteSettingsSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final channelId:J

.field private final guildId:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->guildId:J

    iput-wide p3, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->channelId:J

    return-void
.end method

.method private final observeStoreState(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUserGuildSettings;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/stores/StoreGuilds;",
            "Lcom/discord/stores/StoreChannels;",
            "Lcom/discord/stores/StoreUserGuildSettings;",
            ")",
            "Lrx/Observable<",
            "Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    iget-wide v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->guildId:J

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->channelId:J

    invoke-virtual {p2, v0, v1}, Lcom/discord/stores/StoreChannels;->observeChannel(J)Lrx/Observable;

    move-result-object p2

    invoke-virtual {p3}, Lcom/discord/stores/StoreUserGuildSettings;->get()Lrx/Observable;

    move-result-object p3

    sget-object v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory$observeStoreState$1;->INSTANCE:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory$observeStoreState$1;

    invoke-static {p1, p2, p3, v0}, Lrx/Observable;->i(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    const-string p2, "Observable.combineLatest\u2026uildId]\n        )\n      }"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->guildId:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-eqz p1, :cond_0

    new-instance p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Guild;

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Guild;-><init>(J)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;

    iget-wide v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->channelId:J

    invoke-direct {p1, v0, v1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config$Channel;-><init>(J)V

    :goto_0
    new-instance v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChannels()Lcom/discord/stores/StoreChannels;

    move-result-object v3

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->observeStoreState(Lcom/discord/stores/StoreGuilds;Lcom/discord/stores/StoreChannels;Lcom/discord/stores/StoreUserGuildSettings;)Lrx/Observable;

    move-result-object v2

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v1

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object v3

    invoke-direct {v0, p1, v2, v1, v3}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;-><init>(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Config;Lrx/Observable;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/utilities/time/Clock;)V

    return-object v0
.end method

.method public final getChannelId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->channelId:J

    return-wide v0
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;->guildId:J

    return-wide v0
.end method
