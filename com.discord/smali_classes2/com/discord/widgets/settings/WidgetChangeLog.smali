.class public final Lcom/discord/widgets/settings/WidgetChangeLog;
.super Lcom/discord/app/AppFragment;
.source "WidgetChangeLog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/WidgetChangeLog$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/settings/WidgetChangeLog$Companion;

.field private static final INTENT_EXTRA_BODY:Ljava/lang/String; = "INTENT_EXTRA_BODY"

.field private static final INTENT_EXTRA_REVISION:Ljava/lang/String; = "INTENT_EXTRA_REVISION"

.field private static final INTENT_EXTRA_VERSION:Ljava/lang/String; = "INTENT_EXTRA_VERSION"

.field private static final INTENT_EXTRA_VIDEO:Ljava/lang/String; = "INTENT_EXTRA_VIDEO"


# instance fields
.field private final bodyTv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private maxScrolledPercent:I

.field private openedTimestamp:J

.field private final scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final thumbnailControllerListener:Lcom/discord/widgets/settings/WidgetChangeLog$thumbnailControllerListener$1;

.field private thumbnailDraweeController:Lcom/facebook/drawee/controller/AbstractDraweeController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/drawee/controller/AbstractDraweeController<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final thumbnailIv$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final videoOverlay$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final videoVw$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/WidgetChangeLog;

    const-string v3, "bodyTv"

    const-string v4, "getBodyTv()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetChangeLog;

    const-string v6, "thumbnailIv"

    const-string v7, "getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetChangeLog;

    const-string v6, "videoVw"

    const-string v7, "getVideoVw()Landroid/widget/VideoView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetChangeLog;

    const-string v6, "videoOverlay"

    const-string v7, "getVideoOverlay()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetChangeLog;

    const-string v6, "scrollView"

    const-string v7, "getScrollView()Landroidx/core/widget/NestedScrollView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/WidgetChangeLog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetChangeLog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/WidgetChangeLog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/WidgetChangeLog;->Companion:Lcom/discord/widgets/settings/WidgetChangeLog$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0156

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->bodyTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0162

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailIv$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0164

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->videoVw$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0165

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->videoOverlay$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a015a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetChangeLog$thumbnailControllerListener$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$thumbnailControllerListener$1;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailControllerListener:Lcom/discord/widgets/settings/WidgetChangeLog$thumbnailControllerListener$1;

    return-void
.end method

.method public static final synthetic access$getMaxScrolledPercent$p(Lcom/discord/widgets/settings/WidgetChangeLog;)I
    .locals 0

    iget p0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->maxScrolledPercent:I

    return p0
.end method

.method public static final synthetic access$getScrollView$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroidx/core/widget/NestedScrollView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getScrollView()Landroidx/core/widget/NestedScrollView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getThumbnailIv$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getVideoOverlay$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoOverlay()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getVideoVw$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroid/widget/VideoView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$hideVideoOverlay(Lcom/discord/widgets/settings/WidgetChangeLog;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->hideVideoOverlay()V

    return-void
.end method

.method public static final synthetic access$setMaxScrolledPercent$p(Lcom/discord/widgets/settings/WidgetChangeLog;I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->maxScrolledPercent:I

    return-void
.end method

.method public static final synthetic access$showVideoOverlay(Lcom/discord/widgets/settings/WidgetChangeLog;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->showVideoOverlay()V

    return-void
.end method

.method private final configureFooter()V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f0a0163

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/discord/widgets/settings/WidgetChangeLog$configureFooter$$inlined$apply$lambda$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$configureFooter$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a0158

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/discord/widgets/settings/WidgetChangeLog$configureFooter$$inlined$apply$lambda$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$configureFooter$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a0159

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v1, Lcom/discord/widgets/settings/WidgetChangeLog$configureFooter$$inlined$apply$lambda$3;

    invoke-direct {v1, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$configureFooter$$inlined$apply$lambda$3;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private final configureMedia(Ljava/lang/String;)V
    .locals 5

    const-string v0, ".mp4"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2}, Lx/s/m;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object v2

    const/16 v3, 0x8

    if-eqz v0, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoOverlay()Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_1

    const/4 v3, 0x0

    :cond_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Lf/g/g/a/a/b;->a()Lf/g/g/a/a/d;

    move-result-object v0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()Lcom/facebook/drawee/interfaces/DraweeController;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->k:Lcom/facebook/drawee/interfaces/DraweeController;

    invoke-virtual {v0, p1}, Lf/g/g/a/a/d;->g(Ljava/lang/String;)Lf/g/g/a/a/d;

    move-result-object p1

    iput-boolean v1, p1, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->j:Z

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailControllerListener:Lcom/discord/widgets/settings/WidgetChangeLog$thumbnailControllerListener$1;

    iput-object v0, p1, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->h:Lcom/facebook/drawee/controller/ControllerListener;

    invoke-virtual {p1}, Lcom/facebook/drawee/controller/AbstractDraweeControllerBuilder;->a()Lcom/facebook/drawee/controller/AbstractDraweeController;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailDraweeController:Lcom/facebook/drawee/controller/AbstractDraweeController;

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailDraweeController:Lcom/facebook/drawee/controller/AbstractDraweeController;

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(Lcom/facebook/drawee/interfaces/DraweeController;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ImageView;->requestLayout()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$1;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$3;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    return-void
.end method

.method private final getBodyTv()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->bodyTv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDateString(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 3

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const v0, 0x7f12041c

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "mostRecentIntent.getStri\u2026tring.change_log_md_date)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    :goto_1
    invoke-static {p1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "DateFormat.getMediumDate\u2026mat(context).format(date)"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, p1

    :catch_0
    return-object v0
.end method

.method private final getScrollView()Landroidx/core/widget/NestedScrollView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->scrollView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/core/widget/NestedScrollView;

    return-object v0
.end method

.method private final getThumbnailIv()Lcom/facebook/drawee/view/SimpleDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailIv$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/SimpleDraweeView;

    return-object v0
.end method

.method private final getVideoOverlay()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->videoOverlay$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getVideoVw()Landroid/widget/VideoView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->videoVw$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetChangeLog;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    return-object v0
.end method

.method private final hideVideoOverlay()V
    .locals 7

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoOverlay()Landroid/view/View;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/settings/WidgetChangeLog$hideVideoOverlay$1;->INSTANCE:Lcom/discord/widgets/settings/WidgetChangeLog$hideVideoOverlay$1;

    const-wide/16 v1, 0xc8

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeOut$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method public static final launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    sget-object v0, Lcom/discord/widgets/settings/WidgetChangeLog;->Companion:Lcom/discord/widgets/settings/WidgetChangeLog$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/settings/WidgetChangeLog$Companion;->launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final showVideoOverlay()V
    .locals 8

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoOverlay()Landroid/view/View;

    move-result-object v0

    sget-object v3, Lcom/discord/widgets/settings/WidgetChangeLog$showVideoOverlay$1;->INSTANCE:Lcom/discord/widgets/settings/WidgetChangeLog$showVideoOverlay$1;

    sget-object v4, Lcom/discord/widgets/settings/WidgetChangeLog$showVideoOverlay$2;->INSTANCE:Lcom/discord/widgets/settings/WidgetChangeLog$showVideoOverlay$2;

    const-wide/16 v1, 0xc8

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/view/extensions/ViewExtensions;->fadeIn$default(Landroid/view/View;JLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    return-void
.end method

.method private final track(Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const v0, 0x7f12041c

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "mostRecentIntent.getStri\u2026tring.change_log_md_date)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "INTENT_EXTRA_REVISION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const v1, 0x7f120425

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    const-string v2, "mostRecentIntent.getStri\u2026g.change_log_md_revision)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    if-eqz p3, :cond_2

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p3

    invoke-interface {p3}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->openedTimestamp:J

    sub-long/2addr v3, v5

    const/16 p3, 0x3e8

    int-to-long v5, p3

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    const-string v3, "seconds_open"

    invoke-interface {v2, v3, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget p3, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->maxScrolledPercent:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    const-string v3, "max_scrolled_percentage"

    invoke-interface {v2, v3, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    sget-object p3, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    invoke-static {p2, v2}, Lx/h/f;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p3, p1, v0, v1, p2}, Lcom/discord/utilities/analytics/AnalyticsTracker;->changeLogEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static synthetic track$default(Lcom/discord/widgets/settings/WidgetChangeLog;Ljava/lang/String;Ljava/util/Map;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x1

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/settings/WidgetChangeLog;->track(Ljava/lang/String;Ljava/util/Map;Z)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0185

    return v0
.end method

.method public onDestroy()V
    .locals 8

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_EXTRA_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const v0, 0x7f12041c

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "mostRecentIntent.getStri\u2026tring.change_log_md_date)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v1}, Lcom/discord/stores/StoreStream$Companion;->getChangeLog()Lcom/discord/stores/StoreChangeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/discord/stores/StoreChangeLog;->markSeen(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    const-string v3, "change_log_closed"

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/discord/widgets/settings/WidgetChangeLog;->track$default(Lcom/discord/widgets/settings/WidgetChangeLog;Ljava/lang/String;Ljava/util/Map;ZILjava/lang/Object;)V

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailDraweeController:Lcom/facebook/drawee/controller/AbstractDraweeController;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->thumbnailControllerListener:Lcom/discord/widgets/settings/WidgetChangeLog$thumbnailControllerListener$1;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/controller/AbstractDraweeController;->A(Lcom/facebook/drawee/controller/ControllerListener;)V

    :cond_0
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getVideoVw()Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_0
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 9

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog;->openedTimestamp:J

    const-string v3, "change_log_opened"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/discord/widgets/settings/WidgetChangeLog;->track$default(Lcom/discord/widgets/settings/WidgetChangeLog;Ljava/lang/String;Ljava/util/Map;ZILjava/lang/Object;)V

    const/4 p1, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const p1, 0x7f121ab3

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "requireContext()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->getDateString(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "INTENT_EXTRA_BODY"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f12041b

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object v2, p1

    const-string p1, "mostRecentIntent.getStri\u2026tring.change_log_md_body)"

    invoke-static {v2, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getBodyTv()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$1;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    const/16 v7, 0x1c

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getBodyTv()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "INTENT_EXTRA_VIDEO"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const p1, 0x7f120426

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    const-string v0, "mostRecentIntent.getStri\u2026ring.change_log_md_video)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->configureMedia(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->configureFooter()V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetChangeLog;->getScrollView()Landroidx/core/widget/NestedScrollView;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetChangeLog$onViewBound$2;-><init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    invoke-virtual {p1, v0}, Landroidx/core/widget/NestedScrollView;->setOnScrollChangeListener(Landroidx/core/widget/NestedScrollView$OnScrollChangeListener;)V

    return-void
.end method
