.class public final synthetic Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$WhenMappings;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method public static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->values()[Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v0, 0x6

    new-array v0, v0, [I

    sput-object v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GUILD:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GUILD_CHANNEL:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v1, 0x3

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v4, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->DM:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    aput v1, v0, v2

    sget-object v1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->GROUP_DM:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v1, 0x4

    aput v1, v0, v3

    sget-object v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;->CATEGORY:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    const/4 v2, 0x5

    aput v2, v0, v1

    return-void
.end method
