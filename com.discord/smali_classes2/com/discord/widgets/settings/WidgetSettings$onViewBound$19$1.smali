.class public final Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;
.super Lx/m/c/k;
.source "WidgetSettings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->this$0:Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method

.method public static synthetic invoke$default(Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;ZILjava/lang/Object;)V
    .locals 0

    const/4 p3, 0x1

    and-int/2addr p2, p3

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->invoke(Z)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->this$0:Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;

    iget-object v0, v0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettings;->access$getSettingsUploadDebugLogs$p(Lcom/discord/widgets/settings/WidgetSettings;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->this$0:Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;

    iget-object v0, v0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    invoke-static {v0}, Lcom/discord/widgets/settings/WidgetSettings;->access$getSettingsUploadDebugLogs$p(Lcom/discord/widgets/settings/WidgetSettings;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->this$0:Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;

    iget-object p1, p1, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    const v1, 0x7f121907

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19$1;->this$0:Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;

    iget-object p1, p1, Lcom/discord/widgets/settings/WidgetSettings$onViewBound$19;->this$0:Lcom/discord/widgets/settings/WidgetSettings;

    const v1, 0x7f121aba

    :goto_0
    invoke-virtual {p1, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
