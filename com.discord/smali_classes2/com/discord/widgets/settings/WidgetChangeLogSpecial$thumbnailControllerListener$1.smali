.class public final Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1;
.super Lf/g/g/c/c;
.source "WidgetChangeLogSpecial.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetChangeLogSpecial;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/g/c/c<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetChangeLogSpecial;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetChangeLogSpecial;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1;->this$0:Lcom/discord/widgets/settings/WidgetChangeLogSpecial;

    invoke-direct {p0}, Lf/g/g/c/c;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lf/g/g/c/c;->onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    :try_start_0
    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1;->this$0:Lcom/discord/widgets/settings/WidgetChangeLogSpecial;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLogSpecial;->access$getVideoOverlay$p(Lcom/discord/widgets/settings/WidgetChangeLogSpecial;)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_1

    goto :goto_1

    :cond_1
    const/16 p2, 0x8

    :goto_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1;->this$0:Lcom/discord/widgets/settings/WidgetChangeLogSpecial;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLogSpecial;->access$getVideoOverlay$p(Lcom/discord/widgets/settings/WidgetChangeLogSpecial;)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1$onFinalImageSet$1;

    invoke-direct {p2, p0}, Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1$onFinalImageSet$1;-><init>(Lcom/discord/widgets/settings/WidgetChangeLogSpecial$thumbnailControllerListener$1;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    move-object v2, p1

    sget-object v0, Lcom/discord/app/AppLog;->e:Lcom/discord/app/AppLog;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v1, "Failed to set changelog thumbnail image."

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/logging/Logger;->e$default(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;ILjava/lang/Object;)V

    :goto_2
    return-void
.end method
