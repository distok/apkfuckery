.class public final Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;
.super Ljava/lang/Object;
.source "WidgetChangeLog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetChangeLog;->configureMedia(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetChangeLog;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetChangeLog;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$getVideoVw$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroid/widget/VideoView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/VideoView;->isPlaying()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$showVideoOverlay(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$getVideoVw$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroid/widget/VideoView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/VideoView;->pause()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$hideVideoOverlay(Lcom/discord/widgets/settings/WidgetChangeLog;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetChangeLog;->access$getVideoVw$p(Lcom/discord/widgets/settings/WidgetChangeLog;)Landroid/widget/VideoView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/VideoView;->start()V

    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetChangeLog$configureMedia$2;->this$0:Lcom/discord/widgets/settings/WidgetChangeLog;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    const-string v1, "change_log_video_interacted"

    invoke-static/range {v0 .. v5}, Lcom/discord/widgets/settings/WidgetChangeLog;->track$default(Lcom/discord/widgets/settings/WidgetChangeLog;Ljava/lang/String;Ljava/util/Map;ZILjava/lang/Object;)V

    return-void
.end method
