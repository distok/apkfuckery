.class public final Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$1;
.super Ljava/lang/Object;
.source "WidgetMuteSettingsSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$1;->this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$1;->this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    invoke-static {p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->access$getViewModel$p(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$1;->this$0:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    invoke-virtual {v0}, Lcom/discord/app/AppBottomSheet;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    invoke-static {v0}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/discord/app/AppActivity;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->unmute(Landroid/content/Context;)V

    return-void
.end method
