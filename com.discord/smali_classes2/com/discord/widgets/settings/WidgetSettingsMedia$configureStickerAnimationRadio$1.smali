.class public final Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;
.super Ljava/lang/Object;
.source "WidgetSettingsMedia.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/settings/WidgetSettingsMedia;->configureStickerAnimationRadio(ILcom/discord/views/CheckedSetting;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $stickerAnimationSetting:I

.field public final synthetic this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/settings/WidgetSettingsMedia;I)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    iput p2, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;->$stickerAnimationSetting:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;->this$0:Lcom/discord/widgets/settings/WidgetSettingsMedia;

    invoke-virtual {v0}, Lcom/discord/app/AppFragment;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/settings/WidgetSettingsMedia$configureStickerAnimationRadio$1;->$stickerAnimationSetting:I

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreUserSettings;->setStickerAnimationSettings(Lcom/discord/app/AppActivity;I)V

    return-void
.end method
