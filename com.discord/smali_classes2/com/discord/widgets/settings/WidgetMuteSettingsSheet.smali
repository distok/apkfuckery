.class public final Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetMuteSettingsSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ACTIVE_OPACITY:F = 1.0f

.field private static final ARG_CHANNEL_ID:Ljava/lang/String; = "ARG_CHANNEL_ID"

.field private static final ARG_GUILD_ID:Ljava/lang/String; = "ARG_GUILD_ID"

.field public static final Companion:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;

.field private static final INACTIVE_OPACITY:F = 0.2f


# instance fields
.field private final muteOptions$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationSettingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationSettingsButtonContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationSettingsButtonOverridesLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationSettingsChannelMuteDetails$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationSettingsLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final optionAlways$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final optionEightHours$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final optionFifteenMinutes$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final optionOneHour$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final optionTwentyFourHours$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final subtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final title$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unmute$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unmuteDetails$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final unmuteLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x10

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v3, "title"

    const-string v4, "getTitle()Landroid/widget/TextView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "subtitle"

    const-string v7, "getSubtitle()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "optionFifteenMinutes"

    const-string v7, "getOptionFifteenMinutes()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "optionOneHour"

    const-string v7, "getOptionOneHour()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "optionEightHours"

    const-string v7, "getOptionEightHours()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "optionTwentyFourHours"

    const-string v7, "getOptionTwentyFourHours()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "optionAlways"

    const-string v7, "getOptionAlways()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "unmute"

    const-string v7, "getUnmute()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "unmuteLabel"

    const-string v7, "getUnmuteLabel()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "unmuteDetails"

    const-string v7, "getUnmuteDetails()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "muteOptions"

    const-string v7, "getMuteOptions()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "notificationSettingsButton"

    const-string v7, "getNotificationSettingsButton()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "notificationSettingsLabel"

    const-string v7, "getNotificationSettingsLabel()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "notificationSettingsButtonContainer"

    const-string v7, "getNotificationSettingsButtonContainer()Landroid/view/ViewGroup;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "notificationSettingsButtonOverridesLabel"

    const-string v7, "getNotificationSettingsButtonOverridesLabel()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const-string v6, "notificationSettingsChannelMuteDetails"

    const-string v7, "getNotificationSettingsChannelMuteDetails()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->Companion:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a06d9

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->title$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d8

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->subtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d5

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionFifteenMinutes$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d6

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionOneHour$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d4

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionEightHours$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d7

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionTwentyFourHours$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d3

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionAlways$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06da

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->unmute$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06dc

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->unmuteLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06db

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->unmuteDetails$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06ce

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->muteOptions$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06cf

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d1

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d0

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsButtonContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06d2

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsButtonOverridesLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a06cd

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsChannelMuteDetails$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->viewModel:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->handleEvent(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->viewModel:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->updateViews(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;)V

    return-void
.end method

.method private final configureNotificationSettings(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;)V
    .locals 12

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsButtonOverridesLabel()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getNotificationOverride()I

    move-result v1

    sget v2, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_ALL:I

    if-ne v1, v2, :cond_0

    const v1, 0x7f1207a4

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    sget v2, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_MENTIONS:I

    if-ne v1, v2, :cond_1

    const v1, 0x7f120806

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    sget v2, Lcom/discord/models/domain/ModelNotificationSettings;->FREQUENCY_NOTHING:I

    if-ne v1, v2, :cond_2

    const v1, 0x7f120800

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getSettingsType()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/16 v1, 0x8

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_7

    const/4 v3, 0x4

    if-eq v0, v3, :cond_7

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted()Z

    move-result v0

    const/4 v3, 0x0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isGuildMuted()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :cond_4
    :goto_1
    if-eqz v2, :cond_6

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsLabel()Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsButton()Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsChannelMuteDetails()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsChannelMuteDetails()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted()Z

    move-result p1

    if-eqz p1, :cond_5

    const p1, 0x7f12077b

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string p1, "requireContext()"

    invoke-static {v4, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x7f1207e2

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string p1, "getString(R.string.form_\u2026nel_override_guild_muted)"

    invoke-static {v5, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x7f060238

    invoke-static {p0, p1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsLabel()Landroid/widget/TextView;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsButton()Landroid/view/ViewGroup;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$configureNotificationSettings$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$configureNotificationSettings$1;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsChannelMuteDetails()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsButtonContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_7
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsButtonContainer()Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getNotificationSettingsChannelMuteDetails()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    return-void
.end method

.method private final configureUnmuteButton(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;)V
    .locals 22

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->isChannelMuted()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getUnmuteLabel()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    const-string v12, "requireContext()"

    invoke-static {v4, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f1218cb

    const/4 v13, 0x1

    new-array v6, v13, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getSubtitle()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v0, v5, v6}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "getString(R.string.unmut\u2026nnel, viewState.subtitle)"

    invoke-static {v5, v6}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f040153

    invoke-static {v0, v6}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v4 .. v11}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getUnmuteDetails()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getSettingsType()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    const v5, 0x7f060057

    if-eq v4, v13, :cond_1

    const/4 v6, 0x2

    if-eq v4, v6, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getMuteEndTime()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    const v4, 0x7f1207df

    invoke-virtual {v0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    const v6, 0x7f1207e0

    new-array v7, v13, [Ljava/lang/Object;

    invoke-direct {v0, v4}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->parseMuteEndtime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-virtual {v0, v6, v7}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    move-object v15, v4

    const-string/jumbo v4, "when (val muteEndTime = \u2026EndTime))\n              }"

    invoke-static {v15, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v5}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x38

    const/16 v21, 0x0

    invoke-static/range {v14 .. v21}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_2

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v12}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getMuteEndTime()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    const v6, 0x7f1207e4

    invoke-virtual {v0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    const v7, 0x7f1207e5

    new-array v8, v13, [Ljava/lang/Object;

    invoke-direct {v0, v6}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->parseMuteEndtime(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v3

    invoke-virtual {v0, v7, v8}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :goto_1
    const-string/jumbo v7, "when (val muteEndTime = \u2026dTime))\n                }"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v5}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x38

    const/4 v12, 0x0

    move-object v5, v4

    invoke-static/range {v5 .. v12}, Lcom/discord/utilities/textprocessing/Parsers;->parseMarkdown$default(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/Integer;Ljava/lang/Integer;ZLkotlin/jvm/functions/Function3;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    :goto_2
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getUnmute()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getMuteOptions()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getUnmute()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getMuteOptions()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void
.end method

.method private final getMuteOptions()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->muteOptions$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getNotificationSettingsButton()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getNotificationSettingsButtonContainer()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsButtonContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getNotificationSettingsButtonOverridesLabel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsButtonOverridesLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNotificationSettingsChannelMuteDetails()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsChannelMuteDetails$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNotificationSettingsLabel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->notificationSettingsLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getOptionAlways()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionAlways$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getOptionEightHours()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionEightHours$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getOptionFifteenMinutes()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionFifteenMinutes$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getOptionOneHour()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionOneHour$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getOptionTwentyFourHours()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->optionTwentyFourHours$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getSubtitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->subtitle$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getTitle()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->title$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUnmute()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->unmute$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final getUnmuteDetails()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->unmuteDetails$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getUnmuteLabel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->unmuteLabel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final handleEvent(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event;)V
    .locals 8

    instance-of v0, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$Dismiss;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$NavigateToChannelSettings;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings;->Companion:Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v0, "requireContext()"

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$NavigateToChannelSettings;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Event$NavigateToChannelSettings;->getChannelId()J

    move-result-wide v3

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;->launch$default(Lcom/discord/widgets/channels/WidgetChannelNotificationSettings$Companion;Landroid/content/Context;JZILjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final parseMuteEndtime(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    sget-object v0, Lcom/discord/utilities/time/TimeUtils;->INSTANCE:Lcom/discord/utilities/time/TimeUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    const-string v1, "requireContext()"

    invoke-static {v2, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/time/TimeUtils;->renderUtcDateTime$default(Lcom/discord/utilities/time/TimeUtils;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;IIILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static final showForChannel(JLandroidx/fragment/app/FragmentManager;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->Companion:Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$Companion;->showForChannel(JLandroidx/fragment/app/FragmentManager;)V

    return-void
.end method

.method private final updateViews(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState;)V
    .locals 3

    instance-of v0, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    const/4 v1, 0x4

    if-eqz v0, :cond_5

    check-cast p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getSettingsType()Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$SettingsType;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const v0, 0x7f1210b6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f1210b7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v0, 0x7f1210b9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    const v0, 0x7f1210b8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getTitle()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getSubtitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->configureUnmuteButton(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->configureNotificationSettings(Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Loaded;)V

    goto :goto_2

    :cond_5
    instance-of p1, p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$ViewState$Failure;

    if-eqz p1, :cond_6

    const p1, 0x7f1205df

    const/4 v0, 0x0

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    :cond_6
    :goto_2
    return-void
.end method


# virtual methods
.method public bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V
    .locals 11

    const-string v0, "compositeSubscription"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppBottomSheet;->bindSubscriptions(Lrx/subscriptions/CompositeSubscription;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getUnmute()Landroid/view/ViewGroup;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$1;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->viewModel:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    const/4 v0, 0x0

    const-string v1, "viewModel"

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$2;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->viewModel:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;->observeEvents()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$3;

    invoke-direct {v6, p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$bindSubscriptions$3;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;)V

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method

.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d023a

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "ARG_GUILD_ID"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "ARG_CHANNEL_ID"

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v2, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel$Factory;-><init>(JJ)V

    invoke-direct {p1, p0, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class v0, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(\n     \u2026eetViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    iput-object p1, p0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->viewModel:Lcom/discord/widgets/settings/MuteSettingsSheetViewModel;

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getAppActivity()Lcom/discord/app/AppActivity;

    move-result-object p1

    invoke-static {p1}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/discord/app/AppActivity;->c()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getOptionFifteenMinutes()Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$1;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getOptionOneHour()Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$2;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getOptionEightHours()Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$3;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$3;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getOptionTwentyFourHours()Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$4;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$4;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;->getOptionAlways()Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;

    invoke-direct {v0, p0, p1}, Lcom/discord/widgets/settings/WidgetMuteSettingsSheet$onViewCreated$5;-><init>(Lcom/discord/widgets/settings/WidgetMuteSettingsSheet;Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
