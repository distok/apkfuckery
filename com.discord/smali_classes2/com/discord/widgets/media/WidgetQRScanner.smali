.class public final Lcom/discord/widgets/media/WidgetQRScanner;
.super Lcom/discord/app/AppFragment;
.source "WidgetQRScanner.kt"

# interfaces
.implements Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/media/WidgetQRScanner$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/media/WidgetQRScanner$Companion;

.field private static final EXTRA_SHOW_HELP_CHIP:Ljava/lang/String; = "SHOW_HELP_CHIP"

.field private static final MAIN_BACK_CAMERA:I


# instance fields
.field private final helpChip$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final scannerView$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/media/WidgetQRScanner;

    const-string v3, "helpChip"

    const-string v4, "getHelpChip()Lcom/google/android/material/chip/Chip;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/media/WidgetQRScanner;

    const-string v6, "scannerView"

    const-string v7, "getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/media/WidgetQRScanner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/media/WidgetQRScanner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/media/WidgetQRScanner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/media/WidgetQRScanner;->Companion:Lcom/discord/widgets/media/WidgetQRScanner$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0813

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetQRScanner;->helpChip$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0812

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetQRScanner;->scannerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getScannerView$p(Lcom/discord/widgets/media/WidgetQRScanner;)Lme/dm7/barcodescanner/zxing/ZXingScannerView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object p0

    return-object p0
.end method

.method private final getHelpChip()Lcom/google/android/material/chip/Chip;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetQRScanner;->helpChip$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetQRScanner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/chip/Chip;

    return-object v0
.end method

.method private final getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetQRScanner;->scannerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetQRScanner;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d024d

    return v0
.end method

.method public handleResult(Lcom/google/zxing/Result;)V
    .locals 6

    if-eqz p1, :cond_5

    iget-object v2, p1, Lcom/google/zxing/Result;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-static {v2}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "uri"

    if-eqz v0, :cond_0

    sget-object v0, Lf/a/b/q0/b;->E:Lf/a/b/q0/b;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lf/a/b/q0/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/discord/utilities/intent/IntentUtils;->INSTANCE:Lcom/discord/utilities/intent/IntentUtils;

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/utilities/intent/IntentUtils;->isDiscordAppUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    sget-object v0, Lf/a/b/q0/b;->E:Lf/a/b/q0/b;

    sget-object v0, Lf/a/b/q0/b;->C:Lkotlin/text/Regex;

    const-string v1, "it"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lkotlin/text/Regex;->matchEntire(Ljava/lang/CharSequence;)Lkotlin/text/MatchResult;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    const-string v0, "requireContext()"

    if-eqz p1, :cond_3

    check-cast p1, Lx/s/e;

    invoke-virtual {p1}, Lx/s/e;->getGroupValues()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    sget-object v1, Lcom/discord/widgets/auth/WidgetRemoteAuth;->Companion:Lcom/discord/widgets/auth/WidgetRemoteAuth$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2, p1}, Lcom/discord/widgets/auth/WidgetRemoteAuth$Companion;->launch(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    sget-object p1, Lcom/discord/utilities/uri/UriHandler;->INSTANCE:Lcom/discord/utilities/uri/UriHandler;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/discord/utilities/uri/UriHandler;->handle$default(Lcom/discord/utilities/uri/UriHandler;Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    goto :goto_1

    :cond_4
    const p1, 0x7f1214b2

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_5
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object v0

    invoke-virtual {v0}, La0/a/a/a/a;->a()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object v0

    invoke-virtual {v0}, La0/a/a/a/a;->a()V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object p1

    sget-object v0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->A:Ljava/util/List;

    invoke-virtual {p1, v0}, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->setFormats(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object p1

    invoke-virtual {p1, p0}, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->setResultHandler(Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getScannerView()Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 4

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    const v0, 0x7f060029

    invoke-static {p0, v0}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {p0, v0, v1, v2, v3}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarColor$default(Landroidx/fragment/app/Fragment;IZILjava/lang/Object;)V

    const/4 v0, 0x1

    invoke-static {p0, v1, v0, v3}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f120f6b

    invoke-virtual {p0, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "SHOW_HELP_CHIP"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetQRScanner;->getHelpChip()Lcom/google/android/material/chip/Chip;

    move-result-object v2

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/media/WidgetQRScanner;)V

    new-instance v1, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$2;

    invoke-direct {v1, p0}, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$2;-><init>(Lcom/discord/widgets/media/WidgetQRScanner;)V

    invoke-virtual {p0, v0, v1}, Lcom/discord/app/AppFragment;->requestCameraQRScanner(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
