.class public final Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$1;
.super Lx/m/c/k;
.source "WidgetQRScanner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/media/WidgetQRScanner;->onViewBoundOrOnResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/media/WidgetQRScanner;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/media/WidgetQRScanner;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/media/WidgetQRScanner;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetQRScanner$onViewBoundOrOnResume$1;->this$0:Lcom/discord/widgets/media/WidgetQRScanner;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetQRScanner;->access$getScannerView$p(Lcom/discord/widgets/media/WidgetQRScanner;)Lme/dm7/barcodescanner/zxing/ZXingScannerView;

    move-result-object v0

    iget-object v1, v0, La0/a/a/a/a;->h:La0/a/a/a/c;

    if-nez v1, :cond_0

    new-instance v1, La0/a/a/a/c;

    invoke-direct {v1, v0}, La0/a/a/a/c;-><init>(La0/a/a/a/a;)V

    iput-object v1, v0, La0/a/a/a/a;->h:La0/a/a/a/c;

    :cond_0
    iget-object v0, v0, La0/a/a/a/a;->h:La0/a/a/a/c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, La0/a/a/a/b;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, La0/a/a/a/b;-><init>(La0/a/a/a/c;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
