.class public final Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "WidgetMedia.kt"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/media/WidgetMedia;->configureAndStartControlsAnimation(Landroid/animation/ValueAnimator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/media/WidgetMedia;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/media/WidgetMedia;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    const-string v0, "animator"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetMedia;->access$getActionBar$p(Lcom/discord/widgets/media/WidgetMedia;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetMedia;->access$isVideo(Lcom/discord/widgets/media/WidgetMedia;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetMedia;->access$getPlayerControlsHeight$p(Lcom/discord/widgets/media/WidgetMedia;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetMedia;->access$getToolbarHeight$p(Lcom/discord/widgets/media/WidgetMedia;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v1}, Lcom/discord/widgets/media/WidgetMedia;->access$getPlayerControlsHeight$p(Lcom/discord/widgets/media/WidgetMedia;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    neg-float p1, p1

    div-float/2addr p1, v0

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetMedia;->access$getPlayerControlView$p(Lcom/discord/widgets/media/WidgetMedia;)Lcom/google/android/exoplayer2/ui/PlayerControlView;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    :cond_1
    return-void
.end method
