.class public final Lcom/discord/widgets/media/WidgetMedia;
.super Lcom/discord/app/AppFragment;
.source "WidgetMedia.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;,
        Lcom/discord/widgets/media/WidgetMedia$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/media/WidgetMedia$Companion;

.field private static final INTENT_HEIGHT:Ljava/lang/String; = "INTENT_MEDIA_HEIGHT"

.field private static final INTENT_IMAGE_URL:Ljava/lang/String; = "INTENT_IMAGE_URL"

.field private static final INTENT_MEDIA_SOURCE:Ljava/lang/String; = "INTENT_MEDIA_SOURCE"

.field private static final INTENT_TITLE:Ljava/lang/String; = "INTENT_TITLE"

.field private static final INTENT_URL:Ljava/lang/String; = "INTENT_MEDIA_URL"

.field private static final INTENT_WIDTH:Ljava/lang/String; = "INTENT_MEDIA_WIDTH"

.field private static final SHOW_CONTROLS_TIMEOUT_MS:J = 0xbb8L

.field private static final VERTICAL_CONTROLS_ANIMATION_DURATION_MS:J = 0xc8L


# instance fields
.field private final actionBar$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

.field private final container$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

.field private controlsAnimator:Landroid/animation/ValueAnimator;

.field private controlsVisibilitySubscription:Lrx/Subscription;

.field private imageUri:Landroid/net/Uri;

.field private final mediaImage$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final mediaLoadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private mediaSource:Lcom/discord/player/MediaSource;

.field private final playerControlView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private playerControlsHeight:I

.field private playerPausedByFragmentLifecycle:Z

.field private final playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private toolbarHeight:I

.field private videoUrl:Ljava/lang/String;

.field private viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/media/WidgetMedia;

    const-string v3, "container"

    const-string v4, "getContainer()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    const-string v6, "actionBar"

    const-string v7, "getActionBar()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    const-string v6, "mediaImage"

    const-string v7, "getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    const-string v6, "playerView"

    const-string v7, "getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    const-string v6, "playerControlView"

    const-string v7, "getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    const-string v6, "mediaLoadingIndicator"

    const-string v7, "getMediaLoadingIndicator()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/media/WidgetMedia$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/media/WidgetMedia;->Companion:Lcom/discord/widgets/media/WidgetMedia$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0bce

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0048

    invoke-static {p0, v0}, Ly/a/g0;->e(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->actionBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bcf

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->mediaImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bd2

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bd1

    invoke-static {p0, v0}, Ly/a/g0;->e(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->playerControlView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0bd0

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->mediaLoadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/discord/widgets/media/WidgetMedia;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getActionBar()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getControlsAnimationAction$p(Lcom/discord/widgets/media/WidgetMedia;)Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    return-object p0
.end method

.method public static final synthetic access$getControlsVisibilitySubscription$p(Lcom/discord/widgets/media/WidgetMedia;)Lrx/Subscription;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsVisibilitySubscription:Lrx/Subscription;

    return-object p0
.end method

.method public static final synthetic access$getMediaImage$p(Lcom/discord/widgets/media/WidgetMedia;)Lcom/facebook/samples/zoomable/ZoomableDraweeView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPlayerControlView$p(Lcom/discord/widgets/media/WidgetMedia;)Lcom/google/android/exoplayer2/ui/PlayerControlView;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPlayerControlsHeight$p(Lcom/discord/widgets/media/WidgetMedia;)I
    .locals 0

    iget p0, p0, Lcom/discord/widgets/media/WidgetMedia;->playerControlsHeight:I

    return p0
.end method

.method public static final synthetic access$getToolbarHeight$p(Lcom/discord/widgets/media/WidgetMedia;)I
    .locals 0

    iget p0, p0, Lcom/discord/widgets/media/WidgetMedia;->toolbarHeight:I

    return p0
.end method

.method public static final synthetic access$handleImageProgressComplete(Lcom/discord/widgets/media/WidgetMedia;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->handleImageProgressComplete()V

    return-void
.end method

.method public static final synthetic access$handlePlayerEvent(Lcom/discord/widgets/media/WidgetMedia;Lcom/discord/player/AppMediaPlayer$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/media/WidgetMedia;->handlePlayerEvent(Lcom/discord/player/AppMediaPlayer$Event;)V

    return-void
.end method

.method public static final synthetic access$hideControls(Lcom/discord/widgets/media/WidgetMedia;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->hideControls()V

    return-void
.end method

.method public static final synthetic access$isVideo(Lcom/discord/widgets/media/WidgetMedia;)Z
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->isVideo()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$onMediaClick(Lcom/discord/widgets/media/WidgetMedia;)V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->onMediaClick()V

    return-void
.end method

.method public static final synthetic access$setControlsAnimationAction$p(Lcom/discord/widgets/media/WidgetMedia;Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    return-void
.end method

.method public static final synthetic access$setControlsVisibilitySubscription$p(Lcom/discord/widgets/media/WidgetMedia;Lrx/Subscription;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsVisibilitySubscription:Lrx/Subscription;

    return-void
.end method

.method public static final synthetic access$setPlayerControlsHeight$p(Lcom/discord/widgets/media/WidgetMedia;I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/media/WidgetMedia;->playerControlsHeight:I

    return-void
.end method

.method public static final synthetic access$setToolbarHeight$p(Lcom/discord/widgets/media/WidgetMedia;I)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/media/WidgetMedia;->toolbarHeight:I

    return-void
.end method

.method private final configureAndStartControlsAnimation(Landroid/animation/ValueAnimator;)V
    .locals 2

    new-instance v0, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroidx/interpolator/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetMedia$configureAndStartControlsAnimation$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private final configureMediaImage()V
    .locals 14

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->setIsLongpressEnabled(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v0

    new-instance v2, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-virtual {v0, v2}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->setTapListener(Landroid/view/GestureDetector$SimpleOnGestureListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v0

    sget-object v2, Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;->a:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    sget-object v2, Lf/g/g/e/v;->l:Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;

    const-string v3, "ScalingUtils.ScaleType.FIT_CENTER"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/discord/utilities/images/MGImages;->setScaleType(Landroid/widget/ImageView;Lcom/facebook/drawee/drawable/ScalingUtils$ScaleType;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v4

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "mediaImage.context"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/discord/widgets/media/WidgetMedia;->imageUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, v2}, Lcom/discord/widgets/media/WidgetMedia;->getFormattedUrl(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    new-instance v11, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;

    invoke-direct {v11, p0}, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    const/16 v12, 0x7c

    const/4 v13, 0x0

    invoke-static/range {v4 .. v13}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;[Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;Lcom/facebook/drawee/controller/ControllerListener;ILjava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "imageUri"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method private final getActionBar()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->actionBar$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getContainer()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->container$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFormattedUrl(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 5

    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "INTENT_MEDIA_WIDTH"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "INTENT_MEDIA_HEIGHT"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {p1, v0}, Lcom/discord/utilities/display/DisplayUtils;->resizeToFitScreen(Landroid/content/Context;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    const-string v2, ".gif"

    invoke-static {v0, v2, v3, v1}, Lx/s/m;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    const-string v0, "&format="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/discord/utilities/string/StringUtilsKt;->getSTATIC_IMAGE_EXTENSION()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "?width="

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "&height="

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->mediaImage$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    return-object v0
.end method

.method private final getMediaLoadingIndicator()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->mediaLoadingIndicator$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->playerControlView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/PlayerControlView;

    return-object v0
.end method

.method private final getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->playerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer2/ui/PlayerView;

    return-object v0
.end method

.method private final getToolbarTranslationY()F
    .locals 1

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getActionBar()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final handleImageProgressComplete()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->videoUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->hideLoadingIndicator()V

    :cond_0
    return-void
.end method

.method private final handlePlayerEvent(Lcom/discord/player/AppMediaPlayer$Event;)V
    .locals 4

    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$b;->a:Lcom/discord/player/AppMediaPlayer$Event$b;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->showLoadingIndicator()V

    goto/16 :goto_0

    :cond_0
    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$a;->a:Lcom/discord/player/AppMediaPlayer$Event$a;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "viewModel"

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    if-eqz p1, :cond_1

    invoke-virtual {p1, v1}, Lcom/discord/widgets/media/WidgetMediaViewModel;->setShowCoverFrame(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->hideLoadingIndicator()V

    goto :goto_0

    :cond_1
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_2
    instance-of v0, p1, Lcom/discord/player/AppMediaPlayer$Event$c;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/discord/player/AppMediaPlayer$Event$c;

    iget-wide v1, p1, Lcom/discord/player/AppMediaPlayer$Event$c;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/widgets/media/WidgetMediaViewModel;->setCurrentPlayerPositionMs(J)V

    goto :goto_0

    :cond_3
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$d;->a:Lcom/discord/player/AppMediaPlayer$Event$d;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean p1, p0, Lcom/discord/widgets/media/WidgetMedia;->playerPausedByFragmentLifecycle:Z

    if-nez p1, :cond_9

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    if-eqz p1, :cond_5

    invoke-virtual {p1, v1}, Lcom/discord/widgets/media/WidgetMediaViewModel;->setPlaying(Z)V

    goto :goto_0

    :cond_5
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_6
    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$f;->a:Lcom/discord/player/AppMediaPlayer$Event$f;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    if-eqz p1, :cond_7

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/discord/widgets/media/WidgetMediaViewModel;->setPlaying(Z)V

    goto :goto_0

    :cond_7
    invoke-static {v3}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_8
    sget-object v0, Lcom/discord/player/AppMediaPlayer$Event$e;->a:Lcom/discord/player/AppMediaPlayer$Event$e;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->hideLoadingIndicator()V

    :cond_9
    :goto_0
    return-void
.end method

.method private final hideControls()V
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    sget-object v1, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->HIDE:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iput-object v1, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getToolbarTranslationY()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/discord/widgets/media/WidgetMedia;->toolbarHeight:I

    int-to-float v2, v2

    neg-float v2, v2

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-string v1, "this"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/media/WidgetMedia;->configureAndStartControlsAnimation(Landroid/animation/ValueAnimator;)V

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimator:Landroid/animation/ValueAnimator;

    return-void
.end method

.method private final hideLoadingIndicator()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaLoadingIndicator()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final isVideo()Z
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->mediaSource:Lcom/discord/player/MediaSource;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/discord/player/MediaSource;->f:Lcom/discord/player/MediaType;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/discord/player/MediaType;->VIDEO:Lcom/discord/player/MediaType;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public static final launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/ModelMessageEmbed;)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/media/WidgetMedia;->Companion:Lcom/discord/widgets/media/WidgetMedia$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/discord/widgets/media/WidgetMedia$Companion;->launch(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/discord/models/domain/ModelMessageEmbed;)V

    return-void
.end method

.method private final onMediaClick()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getToolbarTranslationY()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->showControls()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->hideControls()V

    :goto_0
    return-void
.end method

.method private final showControls()V
    .locals 12

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->isVideo()Z

    move-result v0

    const/4 v1, 0x2

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->k()V

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsVisibilitySubscription:Lrx/Subscription;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    :cond_1
    const-wide/16 v2, 0xbb8

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v0}, Lrx/Observable;->Y(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object v0

    const-string v2, "Observable.timer(SHOW_CO\u2026S, TimeUnit.MILLISECONDS)"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v3

    const-class v4, Lcom/discord/widgets/media/WidgetMedia;

    const/4 v5, 0x0

    new-instance v6, Lcom/discord/widgets/media/WidgetMedia$showControls$1;

    invoke-direct {v6, p0}, Lcom/discord/widgets/media/WidgetMedia$showControls$1;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, Lcom/discord/widgets/media/WidgetMedia$showControls$2;

    invoke-direct {v9, p0}, Lcom/discord/widgets/media/WidgetMedia$showControls$2;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    const/16 v10, 0x1a

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/ui/PlayerControlView;->b()V

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    sget-object v2, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->SHOW:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    if-ne v0, v2, :cond_4

    return-void

    :cond_4
    iput-object v2, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimationAction:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_5
    new-array v0, v1, [F

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getToolbarTranslationY()F

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-string v1, "this"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/discord/widgets/media/WidgetMedia;->configureAndStartControlsAnimation(Landroid/animation/ValueAnimator;)V

    iput-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimator:Landroid/animation/ValueAnimator;

    return-void
.end method

.method private final showLoadingIndicator()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaLoadingIndicator()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0238

    return v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/player/AppMediaPlayer;->c()V

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    return-void

    :cond_0
    const-string v0, "appMediaPlayer"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 v0, 0x0

    throw v0
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->controlsAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    invoke-super {p0}, Lcom/discord/app/AppFragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/discord/app/AppFragment;->onPause()V

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    const/4 v1, 0x0

    const-string v2, "appMediaPlayer"

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    invoke-virtual {v0}, Lf/h/a/c/s;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/media/WidgetMedia;->playerPausedByFragmentLifecycle:Z

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/discord/player/AppMediaPlayer;->f:Lf/h/a/c/s0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lf/h/a/c/s0;->n(Z)V

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {v2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v1
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    invoke-direct {p1, p0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    const-class v0, Lcom/discord/widgets/media/WidgetMediaViewModel;

    invoke-virtual {p1, v0}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string v0, "ViewModelProvider(this)\n\u2026diaViewModel::class.java)"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/media/WidgetMediaViewModel;

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "requireContext()"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lf/a/g/i;->a(Landroid/content/Context;)Lcom/discord/player/AppMediaPlayer;

    move-result-object p1

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    const p1, 0x7f060029

    invoke-static {p0, p1}, Lcom/discord/utilities/color/ColorCompat;->getColor(Landroidx/fragment/app/Fragment;I)I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/discord/utilities/color/ColorCompat;->setStatusBarColor$default(Landroidx/fragment/app/Fragment;IZILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/exoplayer2/ui/PlayerView;->getVideoSurfaceView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$onViewBound$1;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetMedia$onViewBound$1;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getContainer()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetMedia$onViewBound$2;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getActionBar()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$onViewBound$3;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetMedia$onViewBound$3;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addOnHeightChangedListener(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;

    move-result-object p1

    if-eqz p1, :cond_2

    new-instance v0, Lcom/discord/widgets/media/WidgetMedia$onViewBound$4;

    invoke-direct {v0, p0}, Lcom/discord/widgets/media/WidgetMedia$onViewBound$4;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    invoke-static {p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addOnHeightChangedListener(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V

    :cond_2
    return-void
.end method

.method public onViewBoundOrOnResume()V
    .locals 21

    move-object/from16 v6, p0

    invoke-super/range {p0 .. p0}, Lcom/discord/app/AppFragment;->onViewBoundOrOnResume()V

    iget-object v0, v6, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    const/4 v7, 0x0

    const-string v8, "viewModel"

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lcom/discord/widgets/media/WidgetMediaViewModel;->getShowCoverFrame()Z

    move-result v0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaImage()Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v9, 0x0

    if-eqz v0, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/media/WidgetMedia;->getMediaLoadingIndicator()Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    :cond_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_MEDIA_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_IMAGE_URL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "Uri.parse(mostRecentInte\u2026gExtra(INTENT_IMAGE_URL))"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v6, Lcom/discord/widgets/media/WidgetMedia;->imageUri:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_MEDIA_SOURCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    instance-of v1, v0, Lcom/discord/player/MediaSource;

    if-nez v1, :cond_2

    move-object v0, v7

    :cond_2
    check-cast v0, Lcom/discord/player/MediaSource;

    iput-object v0, v6, Lcom/discord/widgets/media/WidgetMedia;->mediaSource:Lcom/discord/player/MediaSource;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/discord/player/MediaSource;->d:Landroid/net/Uri;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v7

    :goto_1
    iput-object v0, v6, Lcom/discord/widgets/media/WidgetMedia;->videoUrl:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    :cond_4
    iget-object v0, v6, Lcom/discord/widgets/media/WidgetMedia;->imageUri:Landroid/net/Uri;

    if-eqz v0, :cond_11

    :goto_2
    move-object v3, v0

    invoke-virtual/range {p0 .. p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INTENT_TITLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v10, 0x1

    if-eqz v4, :cond_6

    invoke-static {v4}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    :goto_3
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_7

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    goto :goto_5

    :cond_7
    move-object v5, v4

    :goto_5
    invoke-static {v6, v9, v10, v7}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled$default(Lcom/discord/app/AppFragment;ZILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    const v0, 0x7f121a45

    invoke-virtual {v6, v0}, Lcom/discord/app/AppFragment;->setActionBarTitle(I)Lkotlin/Unit;

    invoke-virtual {v6, v5}, Lcom/discord/app/AppFragment;->setActionBarSubtitle(Ljava/lang/CharSequence;)Lkotlin/Unit;

    const v11, 0x7f0e0011

    new-instance v12, Lcom/discord/widgets/media/WidgetMedia$onViewBoundOrOnResume$1;

    move-object v0, v12

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/media/WidgetMedia$onViewBoundOrOnResume$1;-><init>(Lcom/discord/widgets/media/WidgetMedia;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move v1, v11

    move-object v2, v12

    invoke-static/range {v0 .. v5}, Lcom/discord/app/AppFragment;->setActionBarOptionsMenu$default(Lcom/discord/app/AppFragment;ILrx/functions/Action2;Lrx/functions/Action1;ILjava/lang/Object;)Landroidx/appcompat/widget/Toolbar;

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/media/WidgetMedia;->configureMediaImage()V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/media/WidgetMedia;->showControls()V

    iput-boolean v9, v6, Lcom/discord/widgets/media/WidgetMedia;->playerPausedByFragmentLifecycle:Z

    iget-object v14, v6, Lcom/discord/widgets/media/WidgetMedia;->mediaSource:Lcom/discord/player/MediaSource;

    if-eqz v14, :cond_10

    iget-object v0, v14, Lcom/discord/player/MediaSource;->f:Lcom/discord/player/MediaType;

    sget-object v1, Lcom/discord/player/MediaType;->GIFV:Lcom/discord/player/MediaType;

    if-ne v0, v1, :cond_8

    const/4 v9, 0x1

    :cond_8
    iget-object v13, v6, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    const-string v0, "appMediaPlayer"

    if-eqz v13, :cond_f

    iget-object v1, v6, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/discord/widgets/media/WidgetMediaViewModel;->isPlaying()Z

    move-result v15

    iget-object v1, v6, Lcom/discord/widgets/media/WidgetMedia;->viewModel:Lcom/discord/widgets/media/WidgetMediaViewModel;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/discord/widgets/media/WidgetMediaViewModel;->getCurrentPlayerPositionMs()J

    move-result-wide v17

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerView()Lcom/google/android/exoplayer2/ui/PlayerView;

    move-result-object v19

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/media/WidgetMedia;->getPlayerControlView()Lcom/google/android/exoplayer2/ui/PlayerControlView;

    move-result-object v20

    move/from16 v16, v9

    invoke-virtual/range {v13 .. v20}, Lcom/discord/player/AppMediaPlayer;->a(Lcom/discord/player/MediaSource;ZZJLcom/google/android/exoplayer2/ui/PlayerView;Lcom/google/android/exoplayer2/ui/PlayerControlView;)V

    if-eqz v9, :cond_a

    iget-object v1, v6, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v1, :cond_9

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/discord/player/AppMediaPlayer;->d(F)V

    goto :goto_6

    :cond_9
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_a
    iget-object v1, v6, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v1, :cond_c

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/discord/player/AppMediaPlayer;->d(F)V

    :goto_6
    iget-object v1, v6, Lcom/discord/widgets/media/WidgetMedia;->appMediaPlayer:Lcom/discord/player/AppMediaPlayer;

    if-eqz v1, :cond_b

    iget-object v0, v1, Lcom/discord/player/AppMediaPlayer;->a:Lrx/subjects/PublishSubject;

    invoke-static {v0, v6}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->bindToComponentLifecycle(Lrx/Observable;Lcom/discord/app/AppComponent;)Lrx/Observable;

    move-result-object v7

    const-class v8, Lcom/discord/widgets/media/WidgetMedia;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-instance v13, Lcom/discord/widgets/media/WidgetMedia$onViewBoundOrOnResume$$inlined$let$lambda$1;

    invoke-direct {v13, v6}, Lcom/discord/widgets/media/WidgetMedia$onViewBoundOrOnResume$$inlined$let$lambda$1;-><init>(Lcom/discord/widgets/media/WidgetMedia;)V

    const/16 v14, 0x1e

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_7

    :cond_b
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_c
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_d
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_e
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_f
    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_10
    :goto_7
    return-void

    :cond_11
    const-string v0, "imageUri"

    invoke-static {v0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7

    :cond_12
    invoke-static {v8}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v7
.end method
