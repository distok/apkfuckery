.class public final Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;
.super Lf/g/g/c/c;
.source "WidgetMedia.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/media/WidgetMedia;->configureMediaImage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/g/g/c/c<",
        "Lcom/facebook/imagepipeline/image/ImageInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/media/WidgetMedia;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/media/WidgetMedia;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-direct {p0}, Lf/g/g/c/c;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lf/g/g/c/c;->onFailure(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {p1}, Lcom/discord/widgets/media/WidgetMedia;->access$handleImageProgressComplete(Lcom/discord/widgets/media/WidgetMedia;)V

    return-void
.end method

.method public onFinalImageSet(Ljava/lang/String;Lcom/facebook/imagepipeline/image/ImageInfo;Landroid/graphics/drawable/Animatable;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lf/g/g/c/c;->onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {p1}, Lcom/discord/widgets/media/WidgetMedia;->access$handleImageProgressComplete(Lcom/discord/widgets/media/WidgetMedia;)V

    return-void
.end method

.method public bridge synthetic onFinalImageSet(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0

    check-cast p2, Lcom/facebook/imagepipeline/image/ImageInfo;

    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$2;->onFinalImageSet(Ljava/lang/String;Lcom/facebook/imagepipeline/image/ImageInfo;Landroid/graphics/drawable/Animatable;)V

    return-void
.end method
