.class public final enum Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;
.super Ljava/lang/Enum;
.source "WidgetMedia.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/media/WidgetMedia;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControlsAnimationAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

.field public static final enum HIDE:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

.field public static final enum SHOW:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    new-instance v1, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    const-string v2, "SHOW"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->SHOW:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    aput-object v1, v0, v3

    new-instance v1, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    const-string v2, "HIDE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->HIDE:Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->$VALUES:[Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;
    .locals 1

    const-class v0, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    return-object p0
.end method

.method public static values()[Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;
    .locals 1

    sget-object v0, Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->$VALUES:[Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    invoke-virtual {v0}, [Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/discord/widgets/media/WidgetMedia$ControlsAnimationAction;

    return-object v0
.end method
