.class public final Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "WidgetMedia.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/media/WidgetMedia;->configureMediaImage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field private final DOUBLE_TAP_SCROLL_THRESHOLD:J

.field private final DURATION_MS:J

.field private final mDoubleTapImagePoint:Landroid/graphics/PointF;

.field private mDoubleTapScale:F

.field private mDoubleTapScroll:Z

.field private final mDoubleTapViewPoint:Landroid/graphics/PointF;

.field public final synthetic this$0:Lcom/discord/widgets/media/WidgetMedia;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/media/WidgetMedia;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->DURATION_MS:J

    const-wide/16 v0, 0x14

    iput-wide v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->DOUBLE_TAP_SCROLL_THRESHOLD:J

    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    new-instance p1, Landroid/graphics/PointF;

    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    iput-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapImagePoint:Landroid/graphics/PointF;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScale:F

    return-void
.end method

.method private final calcScale(Landroid/graphics/PointF;)F
    .locals 3

    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, v0

    const/4 v0, 0x1

    int-to-float v0, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3a83126f    # 0.001f

    mul-float v1, v1, v2

    add-float/2addr v1, v0

    const/4 v0, 0x0

    int-to-float v0, v0

    cmpg-float p1, p1, v0

    if-gez p1, :cond_0

    iget p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScale:F

    div-float/2addr p1, v1

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScale:F

    mul-float p1, p1, v1

    :goto_0
    return p1
.end method

.method private final shouldStartDoubleTapScroll(Landroid/graphics/PointF;)Z
    .locals 4

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v2

    iget p1, p1, Landroid/graphics/PointF;->y:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, v1

    float-to-double v0, v0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float p1, v0

    iget-wide v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->DOUBLE_TAP_SCROLL_THRESHOLD:J

    long-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public final getMDoubleTapImagePoint()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapImagePoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method public final getMDoubleTapScale()F
    .locals 1

    iget v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScale:F

    return v0
.end method

.method public final getMDoubleTapScroll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScroll:Z

    return v0
.end method

.method public final getMDoubleTapViewPoint()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    const-string v0, "e"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {v0}, Lcom/discord/widgets/media/WidgetMedia;->access$getMediaImage$p(Lcom/discord/widgets/media/WidgetMedia;)Lcom/facebook/samples/zoomable/ZoomableDraweeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/samples/zoomable/ZoomableDraweeView;->getZoomableController()Lf/g/l/b/e;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.facebook.samples.zoomable.AbstractAnimatedZoomableController"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lf/g/l/b/a;

    new-instance v5, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {v5, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v0, v2, Lf/g/l/b/c;->j:[F

    iget v1, v5, Landroid/graphics/PointF;->x:F

    const/4 v12, 0x0

    aput v1, v0, v12

    iget v1, v5, Landroid/graphics/PointF;->y:F

    const/4 v13, 0x1

    aput v1, v0, v13

    iget-object v1, v2, Lf/g/l/b/c;->h:Landroid/graphics/Matrix;

    iget-object v3, v2, Lf/g/l/b/c;->i:Landroid/graphics/Matrix;

    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v6, v2, Lf/g/l/b/c;->i:Landroid/graphics/Matrix;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object v7, v0

    move-object v9, v0

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Matrix;->mapPoints([FI[FII)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v13, :cond_0

    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v4, v3, 0x0

    aget v6, v0, v4

    iget-object v7, v2, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    iget v8, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v8

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    div-float/2addr v6, v7

    aput v6, v0, v4

    add-int/2addr v3, v13

    aget v4, v0, v3

    iget-object v6, v2, Lf/g/l/b/c;->e:Landroid/graphics/RectF;

    iget v7, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v7

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v4, v6

    aput v4, v0, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v4, Landroid/graphics/PointF;

    aget v1, v0, v12

    aget v0, v0, v13

    invoke-direct {v4, v1, v0}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    if-eqz p1, :cond_7

    const/4 v0, 0x2

    if-eq p1, v13, :cond_4

    if-eq p1, v0, :cond_1

    goto :goto_2

    :cond_1
    iget-boolean p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScroll:Z

    if-nez p1, :cond_2

    invoke-direct {p0, v5}, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->shouldStartDoubleTapScroll(Landroid/graphics/PointF;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    const/4 v12, 0x1

    :cond_3
    iput-boolean v12, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScroll:Z

    if-eqz v12, :cond_8

    invoke-direct {p0, v5}, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->calcScale(Landroid/graphics/PointF;)F

    move-result v3

    iget-object v4, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapImagePoint:Landroid/graphics/PointF;

    iget-object v5, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    const/4 v6, 0x7

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lf/g/l/b/a;->p(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    goto :goto_2

    :cond_4
    iget-boolean p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScroll:Z

    if-eqz p1, :cond_5

    invoke-direct {p0, v5}, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->calcScale(Landroid/graphics/PointF;)F

    move-result v3

    iget-object v4, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapImagePoint:Landroid/graphics/PointF;

    iget-object v5, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    const/4 v6, 0x7

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Lf/g/l/b/a;->p(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v2}, Lf/g/l/b/c;->e()F

    move-result p1

    const/high16 v1, 0x40400000    # 3.0f

    int-to-float v0, v0

    div-float/2addr v1, v0

    cmpg-float p1, p1, v1

    if-gez p1, :cond_6

    const/4 v6, 0x7

    iget-wide v7, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->DURATION_MS:J

    const/4 v9, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual/range {v2 .. v9}, Lf/g/l/b/a;->p(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    goto :goto_1

    :cond_6
    const/4 v6, 0x7

    iget-wide v7, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->DURATION_MS:J

    const/4 v9, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual/range {v2 .. v9}, Lf/g/l/b/a;->p(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    :goto_1
    iput-boolean v12, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScroll:Z

    goto :goto_2

    :cond_7
    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapViewPoint:Landroid/graphics/PointF;

    invoke-virtual {p1, v5}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapImagePoint:Landroid/graphics/PointF;

    invoke-virtual {p1, v4}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    invoke-virtual {v2}, Lf/g/l/b/c;->e()F

    move-result p1

    iput p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScale:F

    :cond_8
    :goto_2
    return v13
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    const-string v0, "e"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->this$0:Lcom/discord/widgets/media/WidgetMedia;

    invoke-static {p1}, Lcom/discord/widgets/media/WidgetMedia;->access$onMediaClick(Lcom/discord/widgets/media/WidgetMedia;)V

    const/4 p1, 0x1

    return p1
.end method

.method public final setMDoubleTapScale(F)V
    .locals 0

    iput p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScale:F

    return-void
.end method

.method public final setMDoubleTapScroll(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/media/WidgetMedia$configureMediaImage$1;->mDoubleTapScroll:Z

    return-void
.end method
