.class public final Lcom/discord/widgets/media/WidgetMediaViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "WidgetMediaViewModel.kt"


# instance fields
.field private currentPlayerPositionMs:J

.field private isPlaying:Z

.field private showCoverFrame:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->isPlaying:Z

    iput-boolean v0, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->showCoverFrame:Z

    return-void
.end method


# virtual methods
.method public final getCurrentPlayerPositionMs()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->currentPlayerPositionMs:J

    return-wide v0
.end method

.method public final getShowCoverFrame()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->showCoverFrame:Z

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->isPlaying:Z

    return v0
.end method

.method public final setCurrentPlayerPositionMs(J)V
    .locals 0

    iput-wide p1, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->currentPlayerPositionMs:J

    return-void
.end method

.method public final setPlaying(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->isPlaying:Z

    return-void
.end method

.method public final setShowCoverFrame(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/discord/widgets/media/WidgetMediaViewModel;->showCoverFrame:Z

    return-void
.end method
