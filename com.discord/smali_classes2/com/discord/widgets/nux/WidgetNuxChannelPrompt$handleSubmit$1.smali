.class public final Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;
.super Lx/m/c/k;
.source "WidgetNuxChannelPrompt.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->handleSubmit(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/models/domain/ModelChannel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    iput-wide p2, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->$guildId:J

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelChannel;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->invoke(Lcom/discord/models/domain/ModelChannel;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/models/domain/ModelChannel;)V
    .locals 10

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    invoke-static {p1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->access$getSubmitButton$p(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)Lcom/discord/views/LoadingButton;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    invoke-static {v1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->access$getTopicWrap$p(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object v1

    invoke-static {v1}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v5

    new-instance v1, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelChannel;->getParentId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    iget-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const v2, 0x7f121194

    invoke-virtual {p1, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "getString(R.string.nuf_c\u2026t_channel_topic_template)"

    invoke-static {p1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x1

    new-array v2, v9, [Ljava/lang/Object;

    aput-object v5, v2, v0

    const-string v8, "java.lang.String.format(this, *args)"

    invoke-static {v2, v9, p1, v8}, Lf/e/c/a/a;->D([Ljava/lang/Object;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/discord/restapi/RestAPIParams$CreateGuildChannel;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {p1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object p1

    iget-wide v2, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->$guildId:J

    invoke-virtual {p1, v2, v3, v1}, Lcom/discord/utilities/rest/RestAPI;->createGuildChannel(JLcom/discord/restapi/RestAPIParams$CreateGuildChannel;)Lrx/Observable;

    move-result-object p1

    invoke-static {v0, v9}, Lf/a/b/r;->f(ZI)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    move-result-object p1

    const-string v0, "RestAPI.api\n            \u2026ormers.restSubscribeOn())"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lf/a/b/r;->a:Lf/a/b/r;

    iget-object v1, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$1;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;)V

    new-instance v3, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$2;

    invoke-direct {v3, p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$2;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;)V

    invoke-virtual {v0, v1, v2, v3}, Lf/a/b/r;->i(Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lrx/functions/Action1;)Lrx/Observable$c;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->k(Lrx/Observable$c;)Lrx/Observable;

    return-void
.end method
