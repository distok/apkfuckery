.class public final Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;
.super Ljava/lang/Object;
.source "WidgetNuxGuildTemplates.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-static {v0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->access$getFriendBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->FRIEND_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    :goto_0
    move-object v2, p1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-static {v0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->access$getStudyBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->STUDY_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-static {v0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->access$getGamingBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->GAMING_GROUP:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-static {v0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->access$getCreatorBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CONTENT_CREATOR:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-static {v0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->access$getClubBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CLUB:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-static {v0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->access$getCommunityBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->LOCAL_COMMUNITY:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    goto :goto_0

    :cond_5
    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CREATE:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    goto :goto_0

    :goto_1
    sget-object p1, Lcom/discord/widgets/nux/NuxAnalytics;->INSTANCE:Lcom/discord/widgets/nux/NuxAnalytics;

    const-string v0, "Guild Template"

    const-string v1, "Guild Create"

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/nux/NuxAnalytics;->postRegistrationTransition$app_productionDiscordExternalRelease(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/widgets/guilds/create/WidgetGuildCreate;->Companion:Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;

    iget-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string p1, "requireContext()"

    invoke-static {v1, p1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/widgets/guilds/create/StockGuildTemplate;->CREATE:Lcom/discord/widgets/guilds/create/StockGuildTemplate;

    if-ne v2, p1, :cond_6

    const/4 p1, 0x1

    const/4 v3, 0x1

    goto :goto_2

    :cond_6
    const/4 p1, 0x0

    const/4 v3, 0x0

    :goto_2
    iget-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const v4, 0x7f1209b9

    invoke-virtual {p1, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v4, "Guild Template"

    invoke-virtual/range {v0 .. v5}, Lcom/discord/widgets/guilds/create/WidgetGuildCreate$Companion;->show(Landroid/content/Context;Lcom/discord/widgets/guilds/create/StockGuildTemplate;ZLjava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_7
    return-void
.end method
