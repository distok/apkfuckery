.class public final Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;
.super Lcom/discord/app/AppFragment;
.source "WidgetNuxChannelPrompt.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;


# instance fields
.field private final guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final skip$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final submitButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final topicWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final validationManager$delegate:Lkotlin/Lazy;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v3, "guildIcon"

    const-string v4, "getGuildIcon()Landroid/widget/ImageView;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v6, "guildIconName"

    const-string v7, "getGuildIconName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v6, "guildName"

    const-string v7, "getGuildName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v6, "topicWrap"

    const-string v7, "getTopicWrap()Lcom/google/android/material/textfield/TextInputLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v6, "submitButton"

    const-string v7, "getSubmitButton()Lcom/discord/views/LoadingButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v6, "skip"

    const-string v7, "getSkip()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->Companion:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a0706

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0707

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0708

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a070a

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->topicWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0705

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->submitButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0709

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->skip$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$validationManager$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$validationManager$2;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)V

    invoke-static {v0}, Lf/h/a/f/f/n/g;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->validationManager$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$finishActivity(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->finishActivity(J)V

    return-void
.end method

.method public static final synthetic access$getSubmitButton$p(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)Lcom/discord/views/LoadingButton;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getSubmitButton()Lcom/discord/views/LoadingButton;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getTopicWrap$p(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)Lcom/google/android/material/textfield/TextInputLayout;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getTopicWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleGuild(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;Lcom/discord/models/domain/ModelGuild;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->handleGuild(Lcom/discord/models/domain/ModelGuild;)V

    return-void
.end method

.method public static final synthetic access$handleSubmit(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->handleSubmit(J)V

    return-void
.end method

.method private final finishActivity(J)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare;->Companion:Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "requireContext()"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v6, "Guild Create"

    move-wide v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/guilds/invite/WidgetGuildInviteShare$Companion;->launch(Landroid/content/Context;JLjava/lang/Long;ZLjava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private final getGuildIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGuildIconName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSkip()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->skip$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getSubmitButton()Lcom/discord/views/LoadingButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->submitButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/LoadingButton;

    return-object v0
.end method

.method private final getTopicWrap()Lcom/google/android/material/textfield/TextInputLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->topicWrap$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/textfield/TextInputLayout;

    return-object v0
.end method

.method private final getValidationManager()Lcom/discord/utilities/view/validators/ValidationManager;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->validationManager$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/utilities/view/validators/ValidationManager;

    return-object v0
.end method

.method private final handleGuild(Lcom/discord/models/domain/ModelGuild;)V
    .locals 16

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getGuildIcon()Landroid/widget/ImageView;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1c

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getGuildIcon()Landroid/widget/ImageView;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1c

    const/4 v8, 0x0

    const-string v3, "asset://asset/images/default_icon.jpg"

    invoke-static/range {v2 .. v8}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getGuildName()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final handleSubmit(J)V
    .locals 18

    move-wide/from16 v0, p1

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getSubmitButton()Lcom/discord/views/LoadingButton;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    sget-object v2, Lcom/discord/utilities/channel/ChannelUtils;->Companion:Lcom/discord/utilities/channel/ChannelUtils$Companion;

    invoke-virtual {v2}, Lcom/discord/utilities/channel/ChannelUtils$Companion;->get()Lcom/discord/utilities/channel/ChannelUtils;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/discord/utilities/channel/ChannelUtils;->getDefaultChannel(J)Lrx/Observable;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->takeSingleUntilTimeout$default(Lrx/Observable;JZILjava/lang/Object;)Lrx/Observable;

    move-result-object v9

    const-class v10, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    new-instance v15, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;

    move-object/from16 v2, p0

    invoke-direct {v15, v2, v0, v1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x1e

    const/16 v17, 0x0

    invoke-static/range {v9 .. v17}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public static final launch(Landroid/content/Context;J)V
    .locals 1

    sget-object v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->Companion:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$Companion;->launch(Landroid/content/Context;J)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0243

    return v0
.end method

.method public final handleError(Lcom/discord/utilities/error/Error;)V
    .locals 4

    const-string v0, "error"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getSubmitButton()Lcom/discord/views/LoadingButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v0

    const-string v1, "error.response"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/discord/utilities/error/Error$Response;->getMessages()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getValidationManager()Lcom/discord/utilities/view/validators/ValidationManager;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/utilities/error/Error;->getResponse()Lcom/discord/utilities/error/Error$Response;

    move-result-object v3

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/discord/utilities/error/Error$Response;->getMessages()Ljava/util/Map;

    move-result-object v1

    const-string v3, "error.response.messages"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/discord/utilities/view/validators/ValidationManager;->setErrors(Ljava/util/Map;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    invoke-virtual {p1, v0}, Lcom/discord/utilities/error/Error;->setShowErrorToasts(Z)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 13

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/discord/app/AppFragment;->getMostRecentIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "com.discord.intent.extra.EXTRA_GUILD_ID"

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    sget-object p1, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {p1}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {p1, p0, v2, v3, v2}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v4

    const-class v5, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    new-instance v10, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$1;

    invoke-direct {v10, p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$1;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v11, 0x1e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getSkip()Landroid/widget/TextView;

    move-result-object p1

    new-instance v4, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$2;

    invoke-direct {v4, p0, v0, v1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$2;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$3;

    invoke-direct {p1, p0, v0, v1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$3;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V

    const/4 v4, 0x0

    invoke-static {p0, p1, v4, v3, v2}, Lcom/discord/app/AppFragment;->setOnBackPressed$default(Lcom/discord/app/AppFragment;Lrx/functions/Func0;IILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getSubmitButton()Lcom/discord/views/LoadingButton;

    move-result-object p1

    invoke-virtual {p1, v4}, Lcom/discord/views/LoadingButton;->setIsLoading(Z)V

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getSubmitButton()Lcom/discord/views/LoadingButton;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$4;

    invoke-direct {v2, p0, v0, v1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$4;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;J)V

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->getTopicWrap()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$5;

    invoke-direct {v0, p0}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$onViewBound$5;-><init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;)V

    const/4 v1, 0x1

    invoke-static {p1, v1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setOnImeActionDone(Lcom/google/android/material/textfield/TextInputLayout;ZLkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    return-void
.end method
