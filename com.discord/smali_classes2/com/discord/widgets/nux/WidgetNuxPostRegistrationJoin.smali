.class public final Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;
.super Lcom/discord/widgets/guilds/join/WidgetGuildJoin;
.source "WidgetNuxPostRegistrationJoin.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;

.field private static final NUX_FLOW_TYPE:Ljava/lang/String; = "Mobile NUX Post Reg"

.field private static final NUX_STEP:Ljava/lang/String; = "Ask to join"


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;->Companion:Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildJoin;-><init>()V

    return-void
.end method

.method public static final synthetic access$handleGuildJoin(Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;)V
    .locals 0

    invoke-virtual {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildJoin;->handleGuildJoin()V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0245

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/discord/app/AppFragment;->setActionBarDisplayHomeAsUpEnabled(Z)Landroidx/appcompat/widget/Toolbar;

    sget-object v0, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v1, "Mobile NUX Post Reg"

    const-string v2, "Registration"

    const-string v3, "Ask to join"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/discord/utilities/analytics/AnalyticsTracker;->newUserOnboarding$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZILjava/lang/Object;)V

    return-void
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/widgets/guilds/join/WidgetGuildJoin;->onViewBound(Landroid/view/View;)V

    invoke-static {}, Lcom/discord/utilities/time/ClockFactory;->get()Lcom/discord/utilities/time/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/discord/utilities/time/Clock;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildJoin;->getGuildJoinButton()Landroid/widget/Button;

    move-result-object p1

    new-instance v2, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$1;-><init>(Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;J)V

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/discord/widgets/guilds/join/WidgetGuildJoin;->getGuildJoinInvite()Lcom/google/android/material/textfield/TextInputLayout;

    move-result-object p1

    new-instance v0, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$2;

    invoke-direct {v0, p0}, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$2;-><init>(Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;)V

    invoke-static {p1, p0, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->addBindedTextWatcher(Lcom/google/android/material/textfield/TextInputLayout;Landroidx/fragment/app/Fragment;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    return-void
.end method
