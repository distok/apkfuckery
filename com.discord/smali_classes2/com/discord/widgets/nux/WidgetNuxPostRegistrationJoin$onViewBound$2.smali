.class public final Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$2;
.super Lx/m/c/k;
.source "WidgetNuxPostRegistrationJoin.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/text/Editable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$2;->this$0:Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/text/Editable;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$2;->invoke(Landroid/text/Editable;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "editable"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$onViewBound$2;->this$0:Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;

    invoke-virtual {v0}, Lcom/discord/widgets/guilds/join/WidgetGuildJoin;->getGuildJoinButton()Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f1211cb

    goto :goto_0

    :cond_0
    const p1, 0x7f120e8f

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method
