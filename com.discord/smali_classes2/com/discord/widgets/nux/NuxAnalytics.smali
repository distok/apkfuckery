.class public final Lcom/discord/widgets/nux/NuxAnalytics;
.super Ljava/lang/Object;
.source "NuxAnalytics.kt"


# static fields
.field private static final FLOW_TYPE_POST_REG:Ljava/lang/String; = "Mobile NUX Post Reg"

.field public static final INSTANCE:Lcom/discord/widgets/nux/NuxAnalytics;

.field private static final STEP_FRIENDS_LIST:Ljava/lang/String; = "Friends List"

.field public static final STEP_GUILD_CREATE:Ljava/lang/String; = "Guild Create"

.field public static final STEP_GUILD_JOIN:Ljava/lang/String; = "Guild Join"

.field public static final STEP_GUILD_TEMPLATE:Ljava/lang/String; = "Guild Template"

.field public static final STEP_INTENT_DISCOVERY:Ljava/lang/String; = "Intent Discovery"

.field public static final STEP_REGISTRATION:Ljava/lang/String; = "Registration"


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/nux/NuxAnalytics;

    invoke-direct {v0}, Lcom/discord/widgets/nux/NuxAnalytics;-><init>()V

    sput-object v0, Lcom/discord/widgets/nux/NuxAnalytics;->INSTANCE:Lcom/discord/widgets/nux/NuxAnalytics;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final postRegistrationSkip$app_productionDiscordExternalRelease(Ljava/lang/String;)V
    .locals 9

    const-string v0, "fromStep"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v2, "Mobile NUX Post Reg"

    const-string v4, "Friends List"

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v3, p1

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->newUserOnboarding$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZILjava/lang/Object;)V

    return-void
.end method

.method public final postRegistrationTransition$app_productionDiscordExternalRelease(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const-string v0, "fromStep"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toStep"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/discord/utilities/analytics/AnalyticsTracker;->INSTANCE:Lcom/discord/utilities/analytics/AnalyticsTracker;

    const-string v2, "Mobile NUX Post Reg"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v1 .. v8}, Lcom/discord/utilities/analytics/AnalyticsTracker;->newUserOnboarding$default(Lcom/discord/utilities/analytics/AnalyticsTracker;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZILjava/lang/Object;)V

    return-void
.end method
