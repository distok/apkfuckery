.class public final Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;
.super Lcom/discord/app/AppFragment;
.source "WidgetNuxGuildTemplates.kt"


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final clubBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final communityBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final createBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final creatorBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final friendBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final gamingBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final joinBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final studyBtn$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v3, "createBtn"

    const-string v4, "getCreateBtn()Landroid/view/View;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "friendBtn"

    const-string v7, "getFriendBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "studyBtn"

    const-string v7, "getStudyBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "gamingBtn"

    const-string v7, "getGamingBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "clubBtn"

    const-string v7, "getClubBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "creatorBtn"

    const-string v7, "getCreatorBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "communityBtn"

    const-string v7, "getCommunityBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    const-string v6, "joinBtn"

    const-string v7, "getJoinBtn()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppFragment;-><init>()V

    const v0, 0x7f0a070e

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->createBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0710

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->friendBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0713

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->studyBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0711

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->gamingBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a070c

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->clubBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a070f

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->creatorBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a070d

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->communityBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0712

    invoke-static {p0, v0}, Ly/a/g0;->h(Landroidx/fragment/app/Fragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->joinBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    return-void
.end method

.method public static final synthetic access$getClubBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getClubBtn()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCommunityBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getCommunityBtn()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCreatorBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getCreatorBtn()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFriendBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getFriendBtn()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getGamingBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getGamingBtn()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStudyBtn$p(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getStudyBtn()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final getClubBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->clubBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCommunityBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->communityBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCreateBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->createBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCreatorBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->creatorBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getFriendBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->friendBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getGamingBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->gamingBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getJoinBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->joinBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStudyBtn()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->studyBtn$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0244

    return v0
.end method

.method public onViewBound(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/discord/app/AppFragment;->onViewBound(Landroid/view/View;)V

    sget-object p1, Lcom/discord/widgets/nux/NuxAnalytics;->INSTANCE:Lcom/discord/widgets/nux/NuxAnalytics;

    const-string v0, "Registration"

    const-string v1, "Guild Template"

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/nux/NuxAnalytics;->postRegistrationTransition$app_productionDiscordExternalRelease(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$1;->INSTANCE:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$1;

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/discord/app/AppFragment;->setOnBackPressed$default(Lcom/discord/app/AppFragment;Lrx/functions/Func0;IILjava/lang/Object;)V

    new-instance p1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildCreateClickListener$1;-><init>(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)V

    const/4 v2, 0x7

    new-array v2, v2, [Landroid/view/View;

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getCreateBtn()Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getFriendBtn()Landroid/view/View;

    move-result-object v0

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getStudyBtn()Landroid/view/View;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getGamingBtn()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v2, v1

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getClubBtn()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v2, v1

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getCreatorBtn()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x5

    aput-object v0, v2, v1

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getCommunityBtn()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x6

    aput-object v0, v2, v1

    invoke-static {v2}, Lx/h/f;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildJoinClickListener$1;

    invoke-direct {p1, p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildJoinClickListener$1;-><init>(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)V

    invoke-direct {p0}, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->getJoinBtn()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
