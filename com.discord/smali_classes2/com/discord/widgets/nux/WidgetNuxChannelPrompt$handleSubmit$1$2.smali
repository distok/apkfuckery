.class public final Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$2;
.super Ljava/lang/Object;
.source "WidgetNuxChannelPrompt.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->invoke(Lcom/discord/models/domain/ModelChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/discord/utilities/error/Error;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$2;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/utilities/error/Error;)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$2;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;

    iget-object v0, v0, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;

    const-string v1, "error"

    invoke-static {p1, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt;->handleError(Lcom/discord/utilities/error/Error;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/discord/utilities/error/Error;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/nux/WidgetNuxChannelPrompt$handleSubmit$1$2;->call(Lcom/discord/utilities/error/Error;)V

    return-void
.end method
