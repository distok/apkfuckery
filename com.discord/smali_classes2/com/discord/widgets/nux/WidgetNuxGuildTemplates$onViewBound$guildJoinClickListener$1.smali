.class public final Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildJoinClickListener$1;
.super Ljava/lang/Object;
.source "WidgetNuxGuildTemplates.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;->onViewBound(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildJoinClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    sget-object p1, Lcom/discord/widgets/nux/NuxAnalytics;->INSTANCE:Lcom/discord/widgets/nux/NuxAnalytics;

    const-string v0, "Guild Template"

    const-string v1, "Guild Join"

    invoke-virtual {p1, v0, v1}, Lcom/discord/widgets/nux/NuxAnalytics;->postRegistrationTransition$app_productionDiscordExternalRelease(Ljava/lang/String;Ljava/lang/String;)V

    sget-object p1, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin;->Companion:Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;

    iget-object v0, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildJoinClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "requireContext()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/discord/widgets/nux/WidgetNuxPostRegistrationJoin$Companion;->show(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/discord/widgets/nux/WidgetNuxGuildTemplates$onViewBound$guildJoinClickListener$1;->this$0:Lcom/discord/widgets/nux/WidgetNuxGuildTemplates;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method
