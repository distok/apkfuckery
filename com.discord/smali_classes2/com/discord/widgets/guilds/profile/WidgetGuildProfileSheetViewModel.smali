.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;
.super Lf/a/b/l0;
.source "WidgetGuildProfileSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;,
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf/a/b/l0<",
        "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;",
        ">;"
    }
.end annotation


# instance fields
.field private final eventSubject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final guildId:J

.field private isEmojiSectionExpanded:Z

.field private final messageAck:Lcom/discord/stores/StoreMessageAck;

.field private final restAPI:Lcom/discord/utilities/rest/RestAPI;

.field private final storeAnalytics:Lcom/discord/stores/StoreAnalytics;

.field private final storeLurking:Lcom/discord/stores/StoreLurking;

.field private final storeObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation
.end field

.field private final storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

.field private final storeUserSettings:Lcom/discord/stores/StoreUserSettings;

.field private final viewingGuild:Z


# direct methods
.method public constructor <init>(Lrx/Observable;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreMessageAck;ZLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreAnalytics;J)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;",
            ">;",
            "Lcom/discord/stores/StoreUserSettings;",
            "Lcom/discord/stores/StoreUserGuildSettings;",
            "Lcom/discord/stores/StoreMessageAck;",
            "Z",
            "Lcom/discord/utilities/rest/RestAPI;",
            "Lcom/discord/stores/StoreLurking;",
            "Lcom/discord/stores/StoreAnalytics;",
            "J)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    const-string v8, "storeObservable"

    invoke-static {v1, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "storeUserSettings"

    invoke-static {v2, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "storeUserGuildSettings"

    invoke-static {v3, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "messageAck"

    invoke-static {v4, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "restAPI"

    invoke-static {v5, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "storeLurking"

    invoke-static {v6, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "storeAnalytics"

    invoke-static {v7, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v8, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;

    invoke-direct {v0, v8}, Lf/a/b/l0;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeObservable:Lrx/Observable;

    iput-object v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    iput-object v3, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    iput-object v4, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->messageAck:Lcom/discord/stores/StoreMessageAck;

    move/from16 v2, p5

    iput-boolean v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->viewingGuild:Z

    iput-object v5, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    iput-object v6, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeLurking:Lcom/discord/stores/StoreLurking;

    iput-object v7, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeAnalytics:Lcom/discord/stores/StoreAnalytics;

    move-wide/from16 v2, p9

    iput-wide v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->guildId:J

    invoke-static {}, Lrx/subjects/PublishSubject;->f0()Lrx/subjects/PublishSubject;

    move-result-object v4

    iput-object v4, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    invoke-static/range {p1 .. p1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->computationLatest(Lrx/Observable;)Lrx/Observable;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v1, v0, v4, v5, v4}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v8

    const-class v9, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    new-instance v14, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$1;

    invoke-direct {v14, v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v15, 0x1e

    const/16 v16, 0x0

    invoke-static/range {v8 .. v16}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    invoke-virtual/range {p8 .. p10}, Lcom/discord/stores/StoreAnalytics;->trackGuildProfileOpened(J)V

    return-void
.end method

.method public static final synthetic access$getEventSubject$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;)Lrx/subjects/PublishSubject;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method public static final synthetic access$handleStoreState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->handleStoreState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V

    return-void
.end method

.method private final handleLoadedGuild(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildPreview;Lcom/discord/utilities/channel/GuildChannelsInfo;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/List;ZLjava/util/List;ZZ)V
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/models/domain/ModelGuildPreview;",
            "Lcom/discord/utilities/channel/GuildChannelsInfo;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;Z",
            "Ljava/util/List<",
            "+",
            "Lcom/discord/models/domain/emoji/Emoji;",
            ">;ZZ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getFeatures()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    const v2, 0x7f080488

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_0
    move-object v11, v2

    goto :goto_1

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getFeatures()Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0803c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v11, v3

    :goto_1
    iget-boolean v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->viewingGuild:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;->BANNER:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    goto :goto_2

    :cond_2
    sget-object v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;->SPLASH:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    :goto_2
    sget-object v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;->BANNER:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    if-ne v2, v4, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getBanner()Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getSplash()Ljava/lang/String;

    move-result-object v4

    :goto_3
    new-instance v12, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v5

    invoke-direct {v12, v5, v6, v4, v2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;-><init>(JLjava/lang/String;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getManageGuildContext()Lcom/discord/utilities/permissions/ManageGuildContext;

    move-result-object v2

    const/4 v4, 0x0

    if-nez v1, :cond_5

    new-instance v5, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    invoke-virtual {v2}, Lcom/discord/utilities/permissions/ManageGuildContext;->canManage()Z

    move-result v6

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getAbleToInstantInvite()Z

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getPremiumSubscriptionCount()Ljava/lang/Integer;

    move-result-object v8

    if-eqz v8, :cond_4

    goto :goto_4

    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_4
    const-string v9, "guild.premiumSubscriptionCount ?: 0"

    invoke-static {v8, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;-><init>(ZZI)V

    move-object v15, v5

    goto :goto_5

    :cond_5
    move-object v15, v3

    :goto_5
    const/4 v5, 0x1

    if-nez v1, :cond_7

    new-instance v6, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    invoke-virtual {v2}, Lcom/discord/utilities/permissions/ManageGuildContext;->getCanManageChannels()Z

    move-result v18

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getCanChangeNickname()Z

    move-result v19

    if-eqz p5, :cond_6

    invoke-virtual/range {p5 .. p5}, Lcom/discord/models/domain/ModelGuildMember$Computed;->getNick()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v20, v7

    goto :goto_6

    :cond_6
    move-object/from16 v20, v3

    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v8, p6

    invoke-interface {v8, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    xor-int/lit8 v21, v7, 0x1

    invoke-virtual/range {p3 .. p3}, Lcom/discord/utilities/channel/GuildChannelsInfo;->getHideMutedChannels()Z

    move-result v22

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v7

    move-object/from16 v9, p1

    invoke-virtual {v9, v7, v8}, Lcom/discord/models/domain/ModelGuild;->isOwner(J)Z

    move-result v7

    xor-int/lit8 v23, v7, 0x1

    move-object/from16 v16, v6

    move/from16 v17, p10

    move/from16 v24, p7

    invoke-direct/range {v16 .. v24}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;-><init>(ZZZLjava/lang/String;ZZZZ)V

    goto :goto_7

    :cond_7
    move-object/from16 v9, p1

    move-object/from16 v16, v3

    :goto_7
    if-eqz v1, :cond_9

    if-eqz p2, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuildPreview;->getEmojis()Ljava/util/List;

    move-result-object v6

    goto :goto_8

    :cond_8
    move-object v6, v3

    goto :goto_8

    :cond_9
    move-object/from16 v6, p8

    :goto_8
    new-instance v14, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    if-nez v1, :cond_a

    invoke-virtual {v2}, Lcom/discord/utilities/permissions/ManageGuildContext;->getCanManageEmojis()Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v4, 0x1

    :cond_a
    iget-boolean v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->viewingGuild:Z

    xor-int/2addr v2, v5

    invoke-direct {v14, v4, v1, v2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;-><init>(ZZZ)V

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getId()J

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "guild.name"

    invoke-static {v2, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getShortName()Ljava/lang/String;

    move-result-object v10

    const-string v4, "guild.shortName"

    invoke-static {v10, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getIcon()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuild;->getDescription()Ljava/lang/String;

    move-result-object v17

    if-eqz p2, :cond_b

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuildPreview;->getApproximatePresenceCount()Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v18, v4

    goto :goto_9

    :cond_b
    move-object/from16 v18, v3

    :goto_9
    if-eqz p2, :cond_c

    invoke-virtual/range {p2 .. p2}, Lcom/discord/models/domain/ModelGuildPreview;->getApproximateMemberCount()Ljava/lang/Integer;

    move-result-object v3

    :cond_c
    new-instance v9, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    invoke-virtual/range {p4 .. p4}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v4

    iget-boolean v5, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->isEmojiSectionExpanded:Z

    if-eqz v6, :cond_d

    goto :goto_a

    :cond_d
    sget-object v6, Lx/h/l;->d:Lx/h/l;

    :goto_a
    invoke-direct {v9, v4, v5, v6}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;-><init>(ZZLjava/util/List;)V

    move-object v4, v1

    move-wide v5, v7

    move-object v7, v2

    move-object v8, v10

    move-object v2, v9

    move-object v9, v13

    move-object/from16 v10, v17

    move-object/from16 v13, v18

    move-object/from16 v18, v14

    move-object v14, v3

    move-object/from16 v17, v2

    invoke-direct/range {v4 .. v18}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleLoadedGuildPreview(Lcom/discord/models/domain/ModelGuildPreview;Z)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/ModelGuildPreview;->hasFeature(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f080488

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_0
    move-object v10, v2

    goto :goto_1

    :cond_0
    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/discord/models/domain/ModelGuildPreview;->hasFeature(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0803c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :goto_1
    iget-boolean v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->viewingGuild:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;->BANNER:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    goto :goto_2

    :cond_2
    sget-object v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;->SPLASH:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    :goto_2
    sget-object v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;->BANNER:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    if-ne v2, v3, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getBanner()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getSplash()Ljava/lang/String;

    move-result-object v3

    :goto_3
    new-instance v11, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getId()J

    move-result-wide v4

    invoke-direct {v11, v4, v5, v3, v2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;-><init>(JLjava/lang/String;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;)V

    new-instance v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    const/4 v3, 0x0

    iget-boolean v4, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->viewingGuild:Z

    const/4 v5, 0x1

    xor-int/2addr v4, v5

    invoke-direct {v2, v3, v5, v4}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;-><init>(ZZZ)V

    new-instance v15, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getId()J

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->shortName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getIcon()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getApproximatePresenceCount()Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getApproximateMemberCount()Ljava/lang/Integer;

    move-result-object v13

    const/16 v16, 0x0

    new-instance v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    iget-boolean v14, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->isEmojiSectionExpanded:Z

    invoke-virtual/range {p1 .. p1}, Lcom/discord/models/domain/ModelGuildPreview;->getEmojis()Ljava/util/List;

    move-result-object v1

    move/from16 v0, p2

    invoke-direct {v3, v0, v14, v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;-><init>(ZZLjava/util/List;)V

    move-object v0, v3

    move-object v3, v15

    const/4 v1, 0x0

    move-object v14, v1

    move-object v1, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v0

    move-object/from16 v17, v2

    invoke-direct/range {v3 .. v17}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    return-void
.end method

.method private final handleStoreState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V
    .locals 11

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component1()Lcom/discord/models/domain/ModelGuild;

    move-result-object v1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component2()Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    move-result-object v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component3()Lcom/discord/utilities/channel/GuildChannelsInfo;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component4()Lcom/discord/models/domain/ModelUser;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component5()Lcom/discord/models/domain/ModelGuildMember$Computed;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component6()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component7()Z

    move-result v7

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component8()Ljava/util/List;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component9()Z

    move-result v9

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;->component10()Z

    move-result v10

    const/4 p1, 0x0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getData()Lcom/discord/models/domain/ModelGuildPreview;

    move-result-object p1

    :cond_0
    move-object v2, p1

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->handleLoadedGuild(Lcom/discord/models/domain/ModelGuild;Lcom/discord/models/domain/ModelGuildPreview;Lcom/discord/utilities/channel/GuildChannelsInfo;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/List;ZLjava/util/List;ZZ)V

    goto :goto_3

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getFetchState()Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, p1

    :goto_0
    sget-object v2, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->SUCCEEDED:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getData()Lcom/discord/models/domain/ModelGuildPreview;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getData()Lcom/discord/models/domain/ModelGuildPreview;

    move-result-object p1

    invoke-virtual {v4}, Lcom/discord/models/domain/ModelUser;->isPremium()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->handleLoadedGuildPreview(Lcom/discord/models/domain/ModelGuildPreview;Z)V

    goto :goto_3

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getFetchState()Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v1, p1

    :goto_1
    sget-object v3, Lcom/discord/stores/StoreGuildProfiles$FetchStates;->FAILED:Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    if-eq v1, v3, :cond_7

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getFetchState()Lcom/discord/stores/StoreGuildProfiles$FetchStates;

    move-result-object p1

    :cond_5
    if-ne p1, v2, :cond_6

    invoke-virtual {v0}, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;->getData()Lcom/discord/models/domain/ModelGuildPreview;

    move-result-object p1

    if-nez p1, :cond_6

    goto :goto_2

    :cond_6
    sget-object p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;->INSTANCE:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    goto :goto_3

    :cond_7
    :goto_2
    sget-object p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Invalid;->INSTANCE:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Invalid;

    invoke-virtual {p0, p1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :goto_3
    return-void
.end method


# virtual methods
.method public final observeEvents()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->eventSubject:Lrx/subjects/PublishSubject;

    const-string v1, "eventSubject"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onClickEmoji()V
    .locals 21

    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Lf/a/b/l0;->getViewState()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    if-eqz v2, :cond_1

    iget-boolean v1, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->isEmojiSectionExpanded:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->isEmojiSectionExpanded:Z

    invoke-virtual {v2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->getEmojisData()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    move-result-object v15

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    iget-boolean v1, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->isEmojiSectionExpanded:Z

    const/16 v18, 0x0

    const/16 v19, 0x5

    const/16 v20, 0x0

    move/from16 v17, v1

    invoke-static/range {v15 .. v20}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;->copy$default(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;ZZLjava/util/List;ILjava/lang/Object;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x17ff

    invoke-static/range {v2 .. v18}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->copy$default(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;ILjava/lang/Object;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf/a/b/l0;->updateViewState(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public final onClickJoinServer(J)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeLurking:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1, p2}, Lcom/discord/stores/StoreLurking;->joinGuild(J)V

    return-void
.end method

.method public final onClickLeaveServer(JLkotlin/jvm/functions/Function0;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onSuccess"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    invoke-virtual {v0, p1, p2}, Lcom/discord/utilities/rest/RestAPI;->leaveGuild(J)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    new-instance v8, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickLeaveServer$1;

    invoke-direct {v8, p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickLeaveServer$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final onClickMarkAsRead(Landroid/content/Context;J)V
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->messageAck:Lcom/discord/stores/StoreMessageAck;

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickMarkAsRead$1;

    invoke-direct {v1, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickMarkAsRead$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/discord/stores/StoreMessageAck;->ackGuild(Landroid/content/Context;JLkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final onClickResetNickname(JLkotlin/jvm/functions/Function0;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onSuccess"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Nick;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/discord/restapi/RestAPIParams$Nick;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/utilities/rest/RestAPI;->changeGuildNickname(JLcom/discord/restapi/RestAPIParams$Nick;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v1, p2, v1}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    new-instance v8, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickResetNickname$1;

    invoke-direct {v8, p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickResetNickname$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final onClickSaveNickname(JLjava/lang/String;Lkotlin/jvm/functions/Function0;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "nick"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSuccess"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->restAPI:Lcom/discord/utilities/rest/RestAPI;

    new-instance v1, Lcom/discord/restapi/RestAPIParams$Nick;

    invoke-direct {v1, p3}, Lcom/discord/restapi/RestAPIParams$Nick;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/discord/utilities/rest/RestAPI;->changeGuildNickname(JLcom/discord/restapi/RestAPIParams$Nick;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p3, 0x1

    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->restSubscribeOn$default(Lrx/Observable;ZILjava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const/4 p2, 0x2

    invoke-static {p1, p0, v0, p2, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v1

    const-class v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    new-instance v7, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickSaveNickname$1;

    invoke-direct {v7, p4}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$onClickSaveNickname$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void
.end method

.method public final onClickViewServer(JLjava/lang/Long;)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeLurking:Lcom/discord/stores/StoreLurking;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreLurking;->startLurking(JLjava/lang/Long;)V

    return-void
.end method

.method public final setAllowDM(Lcom/discord/app/AppActivity;JZ)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeUserSettings:Lcom/discord/stores/StoreUserSettings;

    xor-int/lit8 p4, p4, 0x1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/discord/stores/StoreUserSettings;->setRestrictedGuildId(Lcom/discord/app/AppActivity;JZ)V

    return-void
.end method

.method public final setHideMutedChannels(JZ)V
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->storeUserGuildSettings:Lcom/discord/stores/StoreUserGuildSettings;

    invoke-virtual {v0, p1, p2, p3}, Lcom/discord/stores/StoreUserGuildSettings;->setHideMutedChannels(JZ)V

    return-void
.end method
