.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->showChangeNicknameDialog(JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic $nick:Lcom/google/android/material/textfield/TextInputLayout;

.field public final synthetic this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;JLcom/google/android/material/textfield/TextInputLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    iput-wide p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;->$guildId:J

    iput-object p4, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;->$nick:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    invoke-static {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->access$getViewModel$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    move-result-object p1

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;->$guildId:J

    iget-object v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;->$nick:Lcom/google/android/material/textfield/TextInputLayout;

    const-string v3, "nick"

    invoke-static {v2, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/discord/utilities/view/extensions/ViewExtensions;->getTextOrEmpty(Lcom/google/android/material/textfield/TextInputLayout;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2$1;

    invoke-direct {v3, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;)V

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->onClickSaveNickname(JLjava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
