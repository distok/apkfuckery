.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;
.super Lcom/discord/app/AppBottomSheet;
.source "WidgetGuildProfileSheet.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;
    }
.end annotation


# static fields
.field public static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;

.field private static final LOADED_VIEW_INDEX:I = 0x1

.field private static final LOADING_VIEW_INDEX:I = 0x0

.field private static final NUM_ROWS_EMOJIS:I = 0x2


# instance fields
.field private final actionsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final allowDM$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final banner$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final boostsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final bottomActionsLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final bottomContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final changeNickname$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private channelId:Ljava/lang/Long;

.field private final constraintLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final contentContainerBottomDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final copyId$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final createCategory$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final createChannel$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final developerActions$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private dialog:Landroidx/appcompat/app/AlertDialog;

.field private final emojisAdapter:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;

.field private final emojisCardView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojisCountText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojisRecylerView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final emojisUpsellDotSeparator$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final guildProfileFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final hideMutedChannels$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final iconCard$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final inviteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final joinServer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final leaveServer$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final markAsRead$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final markAsReadAction$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final memberCount$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final memberCountTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final nickname$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final notificationsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final onlineCount$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final onlineCountTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final premiumUpsellText$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final primaryActions$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final settingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final tabItemsLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final uploadEmoji$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private final verifiedPartneredIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

.field private viewModel:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

.field private final viewServer$delegate:Lkotlin/properties/ReadOnlyProperty;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x2a

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lx/m/c/s;

    const-class v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v3, "guildProfileFlipper"

    const-string v4, "getGuildProfileFlipper()Lcom/discord/app/AppViewFlipper;"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lx/m/c/u;->a:Lx/m/c/v;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v1, v0, v5

    const/4 v1, 0x1

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "constraintLayout"

    const-string v7, "getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "banner"

    const-string v7, "getBanner()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "iconCard"

    const-string v7, "getIconCard()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x4

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "guildIcon"

    const-string v7, "getGuildIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x5

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "guildIconName"

    const-string v7, "getGuildIconName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "onlineCount"

    const-string v7, "getOnlineCount()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "onlineCountTextView"

    const-string v7, "getOnlineCountTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x8

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "memberCount"

    const-string v7, "getMemberCount()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x9

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "memberCountTextView"

    const-string v7, "getMemberCountTextView()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xa

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "verifiedPartneredIcon"

    const-string v7, "getVerifiedPartneredIcon()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xb

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "guildName"

    const-string v7, "getGuildName()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xc

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "guildDescription"

    const-string v7, "getGuildDescription()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xd

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "contentContainerBottomDivider"

    const-string v7, "getContentContainerBottomDivider()Landroid/view/View;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xe

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "tabItemsLayout"

    const-string v7, "getTabItemsLayout()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0xf

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "boostsButton"

    const-string v7, "getBoostsButton()Lcom/google/android/material/button/MaterialButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x10

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "notificationsButton"

    const-string v7, "getNotificationsButton()Lcom/google/android/material/button/MaterialButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x11

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "settingsButton"

    const-string v7, "getSettingsButton()Lcom/google/android/material/button/MaterialButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x12

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "inviteButton"

    const-string v7, "getInviteButton()Lcom/google/android/material/button/MaterialButton;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x13

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "actionsContainer"

    const-string v7, "getActionsContainer()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x14

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "markAsReadAction"

    const-string v7, "getMarkAsReadAction()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x15

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "markAsRead"

    const-string v7, "getMarkAsRead()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x16

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "primaryActions"

    const-string v7, "getPrimaryActions()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x17

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "createChannel"

    const-string v7, "getCreateChannel()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x18

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "createCategory"

    const-string v7, "getCreateCategory()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x19

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "changeNickname"

    const-string v7, "getChangeNickname()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "nickname"

    const-string v7, "getNickname()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1b

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "allowDM"

    const-string v7, "getAllowDM()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1c

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "hideMutedChannels"

    const-string v7, "getHideMutedChannels()Lcom/discord/views/CheckedSetting;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1d

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "leaveServer"

    const-string v7, "getLeaveServer()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1e

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "developerActions"

    const-string v7, "getDeveloperActions()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x1f

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "copyId"

    const-string v7, "getCopyId()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x20

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "bottomContainer"

    const-string v7, "getBottomContainer()Landroid/widget/LinearLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x21

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "emojisCountText"

    const-string v7, "getEmojisCountText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x22

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "emojisUpsellDotSeparator"

    const-string v7, "getEmojisUpsellDotSeparator()Landroid/widget/ImageView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x23

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "premiumUpsellText"

    const-string v7, "getPremiumUpsellText()Landroid/widget/TextView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x24

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "emojisCardView"

    const-string v7, "getEmojisCardView()Landroidx/cardview/widget/CardView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x25

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "emojisRecylerView"

    const-string v7, "getEmojisRecylerView()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x26

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "bottomActionsLayout"

    const-string v7, "getBottomActionsLayout()Landroidx/constraintlayout/widget/ConstraintLayout;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x27

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "uploadEmoji"

    const-string v7, "getUploadEmoji()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x28

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "joinServer"

    const-string v7, "getJoinServer()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    const/16 v1, 0x29

    new-instance v3, Lx/m/c/s;

    const-class v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const-string v6, "viewServer"

    const-string v7, "getViewServer()Landroid/widget/Button;"

    invoke-direct {v3, v4, v6, v7, v5}, Lx/m/c/s;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    aput-object v3, v0, v1

    sput-object v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->Companion:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/discord/app/AppBottomSheet;-><init>()V

    const v0, 0x7f0a050a

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildProfileFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04fe

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->constraintLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04f9

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->banner$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a050e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->iconCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a050d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a050f

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051a

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->onlineCount$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->onlineCountTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0515

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->memberCount$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0516

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->memberCountTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a050b

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->verifiedPartneredIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0517

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0504

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0500

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->contentContainerBottomDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0521

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->tabItemsLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04fa

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->boostsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0519

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->notificationsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0520

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->settingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0510

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->inviteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04f6

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->actionsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0514

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->markAsReadAction$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0513

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->markAsRead$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051e

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->primaryActions$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0503

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->createChannel$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0502

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->createCategory$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04fd

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->changeNickname$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0518

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->nickname$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04f8

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->allowDM$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a050c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->hideMutedChannels$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0512

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->leaveServer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0505

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->developerActions$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0501

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->copyId$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04fc

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->bottomContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0509

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisCountText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051c

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisUpsellDotSeparator$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a051d

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->premiumUpsellText$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0508

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisCardView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0507

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisRecylerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a04fb

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->bottomActionsLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0522

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->uploadEmoji$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0511

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->joinServer$delegate:Lkotlin/properties/ReadOnlyProperty;

    const v0, 0x7f0a0523

    invoke-static {p0, v0}, Ly/a/g0;->g(Landroidx/fragment/app/DialogFragment;I)Lkotlin/properties/ReadOnlyProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->viewServer$delegate:Lkotlin/properties/ReadOnlyProperty;

    new-instance v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;-><init>()V

    iput-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisAdapter:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;

    return-void
.end method

.method public static final synthetic access$dismissAlert(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)Lkotlin/Unit;
    .locals 0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->dismissAlert()Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getViewModel$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;
    .locals 0

    iget-object p0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->viewModel:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    const-string p0, "viewModel"

    invoke-static {p0}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final synthetic access$handleEvent(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->handleEvent(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;)V

    return-void
.end method

.method public static final synthetic access$handleViewState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->handleViewState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;)V

    return-void
.end method

.method public static final synthetic access$setViewModel$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->viewModel:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    return-void
.end method

.method public static final synthetic access$showChangeNicknameDialog(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->showChangeNicknameDialog(JLjava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$showLeaveServerDialog(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->showLeaveServerDialog(J)V

    return-void
.end method

.method private final configureActions(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;)V
    .locals 16

    move-object/from16 v6, p0

    move-wide/from16 v7, p1

    move-object/from16 v9, p3

    const/16 v10, 0x8

    if-nez v9, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getActionsContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getActionsContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->isUnread()Z

    move-result v0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->getCanManageChannels()Z

    move-result v12

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->getCanChangeNickname()Z

    move-result v13

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->getNick()Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getMarkAsReadAction()Landroidx/cardview/widget/CardView;

    move-result-object v1

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getMarkAsRead()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;

    invoke-direct {v1, v6, v7, v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;J)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getPrimaryActions()Landroidx/cardview/widget/CardView;

    move-result-object v0

    if-eqz v12, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getCreateCategory()Landroid/widget/TextView;

    move-result-object v15

    if-eqz v12, :cond_3

    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$1;

    move-object v0, v4

    move-object v1, v15

    move-object/from16 v2, p0

    move v3, v12

    move-object v10, v4

    move-wide/from16 v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$1;-><init>(Landroid/widget/TextView;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJ)V

    invoke-virtual {v6, v15, v10}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getCreateChannel()Landroid/widget/TextView;

    move-result-object v10

    if-eqz v12, :cond_4

    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    const/16 v0, 0x8

    :goto_3
    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v15, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$2;

    move-object v0, v15

    move-object v1, v10

    move-object/from16 v2, p0

    move v3, v12

    move-wide/from16 v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$2;-><init>(Landroid/widget/TextView;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJ)V

    invoke-virtual {v6, v10, v15}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getChangeNickname()Landroid/widget/LinearLayout;

    move-result-object v10

    if-eqz v13, :cond_5

    const/4 v0, 0x0

    goto :goto_4

    :cond_5
    const/16 v0, 0x8

    :goto_4
    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v12, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;

    move-object v0, v12

    move-object/from16 v1, p0

    move v2, v13

    move-wide/from16 v3, p1

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJLjava/lang/String;)V

    invoke-virtual {v6, v10, v12}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getNickname()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v14, :cond_7

    invoke-interface {v14}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_6

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    goto :goto_6

    :cond_7
    :goto_5
    const/4 v2, 0x1

    :goto_6
    xor-int/2addr v1, v2

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    goto :goto_7

    :cond_8
    const/16 v1, 0x8

    :goto_7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getAllowDM()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->isAllowDMChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$4;

    invoke-direct {v1, v6, v9, v7, v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$4;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;J)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getHideMutedChannels()Lcom/discord/views/CheckedSetting;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->getHideMutedChannels()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setChecked(Z)V

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;

    invoke-direct {v1, v6, v9, v7, v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;J)V

    invoke-virtual {v0, v1}, Lcom/discord/views/CheckedSetting;->setOnCheckedListener(Lrx/functions/Action1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getLeaveServer()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->getCanLeaveGuild()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    goto :goto_8

    :cond_9
    const/16 v1, 0x8

    :goto_8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$6;

    invoke-direct {v1, v6, v9, v7, v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$6;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;J)V

    invoke-virtual {v6, v0, v1}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getDeveloperActions()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->isDeveloper()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v10, 0x0

    goto :goto_9

    :cond_a
    const/16 v10, 0x8

    :goto_9
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getCopyId()Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$9;

    invoke-direct {v1, v6, v7, v8}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$9;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;J)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureBottomActions(JLjava/lang/Long;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V
    .locals 8

    invoke-virtual {p4}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;->getShowUploadEmoji()Z

    move-result v0

    invoke-virtual {p4}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;->getShowJoinServer()Z

    move-result v1

    invoke-virtual {p4}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;->getShowViewServer()Z

    move-result v4

    const/16 p4, 0x8

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBottomActionsLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p1

    invoke-virtual {p1, p4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBottomActionsLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getUploadEmoji()Landroid/widget/Button;

    move-result-object v2

    if-eqz v0, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    const/16 v5, 0x8

    :goto_0
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    new-instance v5, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$1;

    invoke-direct {v5, p0, v0, p1, p2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJ)V

    invoke-virtual {p0, v2, v5}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getJoinServer()Landroid/widget/Button;

    move-result-object v0

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/16 v2, 0x8

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, v1, p1, p2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJ)V

    invoke-virtual {p0, v0, v2}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getViewServer()Landroid/widget/Button;

    move-result-object v0

    if-eqz v4, :cond_3

    const/4 p4, 0x0

    :cond_3
    invoke-virtual {v0, p4}, Landroid/view/View;->setVisibility(I)V

    new-instance p4, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$3;

    move-object v2, p4

    move-object v3, p0

    move-wide v5, p1

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureBottomActions$$inlined$apply$lambda$3;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJLjava/lang/Long;)V

    invoke-virtual {p0, v0, p4}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBottomActionsLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->requestLayout()V

    :goto_2
    return-void
.end method

.method private final configureEmojis(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;)V
    .locals 10

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;->isPremium()Z

    move-result v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;->isExpanded()Z

    move-result v1

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;->getEmojis()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisCountText()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "resources"

    invoke-static {v4, v5}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v6, v8

    const v7, 0x7f10003a

    const v9, 0x7f1210f8

    invoke-static {v4, v7, v9, v2, v6}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisUpsellDotSeparator()Landroid/widget/ImageView;

    move-result-object v3

    xor-int/lit8 v4, v0, 0x1

    const/16 v6, 0x8

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getPremiumUpsellText()Landroid/widget/TextView;

    move-result-object v3

    xor-int/2addr v0, v5

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisCardView()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->maxEmojisPerRow()I

    move-result v0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisRecylerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager"

    invoke-static {v2, v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v2, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanCount(I)V

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_2

    :cond_2
    mul-int/lit8 v0, v0, 0x2

    :goto_2
    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisAdapter:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;

    invoke-virtual {v1, p1, v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;->setData(Ljava/util/List;I)V

    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisCardView()Landroidx/cardview/widget/CardView;

    move-result-object p1

    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void
.end method

.method private final configureGuildBanner(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;)V
    .locals 9

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->getHash()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->getType()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner$Type;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const v1, 0x7f0701b1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->getGuildId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->getHash()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v3, p1, v1}, Lcom/discord/utilities/icon/IconUtils;->getGuildSplashUrl(JLjava/lang/String;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    sget-object v0, Lcom/discord/utilities/icon/IconUtils;->INSTANCE:Lcom/discord/utilities/icon/IconUtils;

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->getGuildId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->getHash()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, p1, v1}, Lcom/discord/utilities/icon/IconUtils;->getBannerForGuild(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object v1, p1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBanner()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x7c

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lcom/discord/utilities/images/MGImages;->setImage$default(Landroid/widget/ImageView;Ljava/lang/String;IIZLkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBanner()Landroid/widget/ImageView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->constrainIconToBanner()V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBanner()Landroid/widget/ImageView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->constrainIconToParent()V

    :goto_1
    return-void
.end method

.method private final configureGuildContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildName()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 p1, 0x8

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildDescription()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildDescription()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildDescription()Landroid/widget/TextView;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    if-eqz p3, :cond_2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getVerifiedPartneredIcon()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getVerifiedPartneredIcon()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getVerifiedPartneredIcon()Landroid/widget/ImageView;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method private final configureGuildIcon(JLjava/lang/String;Ljava/lang/String;)V
    .locals 14

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Lcom/discord/utilities/icon/IconUtils;->getMediaProxySize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object/from16 v1, p3

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/icon/IconUtils;->getForGuild$default(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildIcon()Landroid/widget/ImageView;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1c

    const/4 v13, 0x0

    invoke-static/range {v7 .. v13}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildIcon()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1c

    const/4 v6, 0x0

    const-string v1, "asset://asset/images/default_icon_selected.jpg"

    invoke-static/range {v0 .. v6}, Lcom/discord/utilities/icon/IconUtils;->setIcon$default(Landroid/widget/ImageView;Ljava/lang/String;ILkotlin/jvm/functions/Function1;Lcom/discord/utilities/images/MGImages$ChangeDetector;ILjava/lang/Object;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildIconName()Landroid/widget/TextView;

    move-result-object v0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private final configureMemberCount(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 8

    const/16 v0, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getOnlineCount()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getOnlineCountTextView()Landroid/widget/TextView;

    move-result-object v3

    const v4, 0x7f120d98

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getOnlineCount()Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "onlineCount.context"

    invoke-static {v6, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v6}, Lcom/discord/utilities/string/StringUtilsKt;->format(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v5, v2

    invoke-virtual {p0, v4, v5}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, p1, v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getOnlineCount()Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    if-eqz p2, :cond_1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getMemberCount()Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "requireContext()"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f1000a3

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p2, v5, v2

    invoke-static {p1, v0, v3, v4, v5}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;Landroid/content/Context;II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getMemberCountTextView()Landroid/widget/TextView;

    move-result-object p2

    const v0, 0x7f120d99

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p2, p1, v0}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setTextWithMarkdown(Landroid/widget/TextView;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getMemberCount()Landroid/widget/LinearLayout;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private final configureTabItems(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;)V
    .locals 16

    move-object/from16 v6, p0

    const/16 v7, 0x8

    if-eqz p3, :cond_2

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->getCanAccessSettings()Z

    move-result v8

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->getAbleToInstantInvite()Z

    move-result v9

    invoke-virtual/range {p3 .. p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->getPremiumSubscriptionCount()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getTabItemsLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getContentContainerBottomDivider()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f040158

    invoke-static {v6, v0}, Lcom/discord/utilities/color/ColorCompat;->getThemedColor(Landroidx/fragment/app/Fragment;I)I

    move-result v11

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBoostsButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f1000ce

    const v3, 0x7f121359

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-static {v0, v1, v3, v2, v4}, Lcom/discord/utilities/resources/StringResourceUtilsKt;->getQuantityString(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v12, v11}, Landroid/widget/Button;->setTextColor(I)V

    new-instance v13, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$1;

    move-object v0, v13

    move-object/from16 v1, p0

    move v3, v11

    move-wide/from16 v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;IIJ)V

    invoke-virtual {v6, v12, v13}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getNotificationsButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v0

    invoke-static {v0, v11}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {v0, v11}, Landroid/widget/Button;->setTextColor(I)V

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$2;

    move-wide/from16 v12, p1

    invoke-direct {v1, v6, v11, v12, v13}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$2;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;IJ)V

    invoke-virtual {v6, v0, v1}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getSettingsButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v14

    if-eqz v8, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v14, v11}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {v14, v11}, Landroid/widget/Button;->setTextColor(I)V

    new-instance v15, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$3;

    move-object v0, v15

    move-object/from16 v1, p0

    move v2, v8

    move v3, v11

    move-wide/from16 v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$3;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZIJ)V

    invoke-virtual {v6, v14, v15}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getInviteButton()Lcom/google/android/material/button/MaterialButton;

    move-result-object v8

    if-eqz v9, :cond_1

    const/4 v7, 0x0

    :cond_1
    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v8, v11}, Lcom/discord/utilities/color/ColorCompatKt;->setDrawableColor(Landroid/widget/TextView;I)V

    invoke-virtual {v8, v11}, Landroid/widget/Button;->setTextColor(I)V

    new-instance v7, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$4;

    move-object v0, v7

    move-object/from16 v1, p0

    move v2, v9

    move v3, v11

    move-wide/from16 v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureTabItems$$inlined$apply$lambda$4;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZIJ)V

    invoke-virtual {v6, v8, v7}, Lcom/discord/app/AppBottomSheet;->setOnClickAndDismissListener(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getTabItemsLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-direct/range {p0 .. p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getContentContainerBottomDivider()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private final configureUI(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;)V
    .locals 13

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component1()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component3()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component4()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component5()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component6()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component7()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    move-result-object v7

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component8()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component9()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component10()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    move-result-object v10

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component11()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    move-result-object v11

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component12()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    move-result-object v12

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->component13()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    move-result-object p1

    invoke-direct {p0, v0, v1, v4, v3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureGuildIcon(JLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureGuildBanner(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;)V

    invoke-direct {p0, v2, v5, v6}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureGuildContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-direct {p0, v8, v9}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureMemberCount(Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-direct {p0, v0, v1, v10}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureTabItems(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;)V

    invoke-direct {p0, v0, v1, v11}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureActions(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;)V

    invoke-direct {p0, v12}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureEmojis(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;)V

    iget-object v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->channelId:Ljava/lang/Long;

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureBottomActions(JLjava/lang/Long;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V

    return-void
.end method

.method private final constrainIconToBanner()V
    .locals 5

    new-instance v0, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v0}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintSet;->clear(II)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->clear(II)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBanner()Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getId()I

    move-result v4

    invoke-virtual {v0, v1, v2, v4, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBanner()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIII)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method private final constrainIconToParent()V
    .locals 7

    new-instance v6, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v6}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getId()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {v6, v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->clear(II)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getId()I

    move-result v0

    const/4 v1, 0x4

    invoke-virtual {v6, v0, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->clear(II)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getId()I

    move-result v3

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getIconCard()Landroidx/cardview/widget/CardView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v5, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_0
    const/4 v2, 0x3

    const/4 v4, 0x3

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    return-void
.end method

.method private final dismissAlert()Lkotlin/Unit;
    .locals 2

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->dialog:Landroidx/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatDialog;->dismiss()V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/discord/app/AppBottomSheet;->hideKeyboard(Landroid/view/View;)V

    return-object v0
.end method

.method private final getActionsContainer()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->actionsContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getAllowDM()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->allowDM$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1b

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getBanner()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->banner$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getBoostsButton()Lcom/google/android/material/button/MaterialButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->boostsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    return-object v0
.end method

.method private final getBottomActionsLayout()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->bottomActionsLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x26

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method private final getBottomContainer()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->bottomContainer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x20

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getChangeNickname()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->changeNickname$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->constraintLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method private final getContentContainerBottomDivider()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->contentContainerBottomDivider$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getCopyId()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->copyId$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1f

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCreateCategory()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->createCategory$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCreateChannel()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->createChannel$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getDeveloperActions()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->developerActions$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1e

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getEmojisCardView()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisCardView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x24

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getEmojisCountText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisCountText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x21

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getEmojisRecylerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisRecylerView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x25

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final getEmojisUpsellDotSeparator()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisUpsellDotSeparator$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x22

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGuildDescription()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildDescription$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGuildIconName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildIconName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildName$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getGuildProfileFlipper()Lcom/discord/app/AppViewFlipper;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->guildProfileFlipper$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/app/AppViewFlipper;

    return-object v0
.end method

.method private final getHideMutedChannels()Lcom/discord/views/CheckedSetting;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->hideMutedChannels$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1c

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/discord/views/CheckedSetting;

    return-object v0
.end method

.method private final getIconCard()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->iconCard$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getInviteButton()Lcom/google/android/material/button/MaterialButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->inviteButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    return-object v0
.end method

.method private final getJoinServer()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->joinServer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x28

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getLeaveServer()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->leaveServer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1d

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMarkAsRead()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->markAsRead$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x15

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getMarkAsReadAction()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->markAsReadAction$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getMemberCount()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->memberCount$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getMemberCountTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->memberCountTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNickname()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->nickname$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x1a

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getNotificationsButton()Lcom/google/android/material/button/MaterialButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->notificationsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    return-object v0
.end method

.method private final getOnlineCount()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->onlineCount$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getOnlineCountTextView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->onlineCountTextView$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPremiumUpsellText()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->premiumUpsellText$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x23

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getPrimaryActions()Landroidx/cardview/widget/CardView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->primaryActions$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    return-object v0
.end method

.method private final getSettingsButton()Lcom/google/android/material/button/MaterialButton;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->settingsButton$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/button/MaterialButton;

    return-object v0
.end method

.method private final getTabItemsLayout()Landroid/widget/LinearLayout;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->tabItemsLayout$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private final getUploadEmoji()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->uploadEmoji$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x27

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final getVerifiedPartneredIcon()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->verifiedPartneredIcon$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getViewServer()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->viewServer$delegate:Lkotlin/properties/ReadOnlyProperty;

    sget-object v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/16 v2, 0x29

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadOnlyProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final handleDismissAndShowToast(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;)V
    .locals 2

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;->getStringRes()I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p0, p1, v0, v1}, Lf/a/b/p;->k(Landroidx/fragment/app/Fragment;III)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    return-void
.end method

.method private final handleEvent(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->handleDismissAndShowToast(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Event$DismissAndShowToast;)V

    :cond_0
    return-void
.end method

.method private final handleViewState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;)V
    .locals 1

    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loading;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->showLoadingView()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Invalid;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->dismiss()V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->updateView(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;)V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final maxEmojisPerRow()I
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "resources"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisCardView()Landroidx/cardview/widget/CardView;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/cardview/widget/CardView;->getContentPaddingLeft()I

    move-result v1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisCardView()Landroidx/cardview/widget/CardView;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/cardview/widget/CardView;->getContentPaddingRight()I

    move-result v2

    add-int/2addr v2, v1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBottomContainer()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v1

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getBottomContainer()Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingEnd()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0700cf

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v2

    sub-int/2addr v0, v3

    div-int/2addr v0, v1

    return v0
.end method

.method public static final show(Landroidx/fragment/app/FragmentManager;ZJJ)V
    .locals 7

    sget-object v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->Companion:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$Companion;->show(Landroidx/fragment/app/FragmentManager;ZJJ)V

    return-void
.end method

.method private final showChangeNicknameDialog(JLjava/lang/String;)V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d01a0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a01e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0a01ea

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0a01e9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0a01e8

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/material/textfield/TextInputLayout;

    new-instance v5, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$1;

    invoke-direct {v5, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v1, "nick"

    invoke-static {v4, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, p3}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setText(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)Lkotlin/Unit;

    invoke-static {v4}, Lcom/discord/utilities/view/extensions/ViewExtensions;->setSelectionEnd(Lcom/google/android/material/textfield/TextInputLayout;)Lkotlin/Unit;

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;

    invoke-direct {v1, p0, p1, p2, v4}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$2;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;JLcom/google/android/material/textfield/TextInputLayout;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v1, "reset"

    invoke-static {v3, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p3, :cond_1

    invoke-static {p3}, Lx/s/m;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p3, 0x1

    :goto_1
    xor-int/2addr p3, v2

    if-eqz p3, :cond_2

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance p3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$3;

    invoke-direct {p3, p0, p1, p2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$showChangeNicknameDialog$3;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;J)V

    invoke-virtual {v3, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Landroidx/appcompat/app/AlertDialog$Builder;

    const-string p2, "view"

    invoke-static {v0, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->dialog:Landroidx/appcompat/app/AlertDialog;

    return-void
.end method

.method private final showLeaveServerDialog(J)V
    .locals 3

    sget-object v0, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog;->Companion:Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "parentFragmentManager"

    invoke-static {v1, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1, p2}, Lcom/discord/widgets/guilds/leave/WidgetLeaveGuildDialog$Companion;->show(Landroidx/fragment/app/FragmentManager;J)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->dismissAlert()Lkotlin/Unit;

    return-void
.end method

.method private final showLoadingView()V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildProfileFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    return-void
.end method

.method private final updateView(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;)V
    .locals 2

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getGuildProfileFlipper()Lcom/discord/app/AppViewFlipper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/discord/app/AppViewFlipper;->setDisplayedChild(I)V

    invoke-direct {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureUI(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;)V

    return-void
.end method


# virtual methods
.method public getContentViewResId()I
    .locals 1

    const v0, 0x7f0d0213

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11

    const-string v0, "view"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/discord/app/AppBottomSheet;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "com.discord.intent.extra.EXTRA_GUILD_ID"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide p1

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.discord.intent.extra.EXTRA_VIEWING_GUILD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/discord/app/AppBottomSheet;->getArgumentsOrDefault()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "com.discord.intent.extra.EXTRA_CHANNEL_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->channelId:Ljava/lang/Long;

    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    new-instance v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;

    invoke-direct {v2, p1, p2, v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;-><init>(JZ)V

    invoke-direct {v1, p0, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    const-class p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    invoke-virtual {v1, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    const-string p2, "ViewModelProvider(\n     \u2026del::class.java\n        )"

    invoke-static {p1, p2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->viewModel:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    const-string p2, "viewModel"

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lf/a/b/l0;->observeViewState()Lrx/Observable;

    move-result-object p1

    const/4 v1, 0x2

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$onViewCreated$1;

    invoke-direct {v8, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$onViewCreated$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->viewModel:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->observeEvents()Lrx/Observable;

    move-result-object p1

    invoke-static {p1, p0, v0, v1, v0}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->ui$default(Lrx/Observable;Lcom/discord/app/AppComponent;Lcom/discord/utilities/mg_recycler/MGRecyclerAdapterSimple;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v2

    const-class v3, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$onViewCreated$2;

    invoke-direct {v8, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$onViewCreated$2;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)V

    const/16 v9, 0x1e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/discord/utilities/rx/ObservableExtensionsKt;->appSubscribe$default(Lrx/Observable;Ljava/lang/Class;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisAdapter:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;

    new-instance p2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$onViewCreated$3;

    invoke-direct {p2, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$onViewCreated$3;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)V

    invoke-virtual {p1, p2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;->setOnClickEmoji(Lkotlin/jvm/functions/Function0;)V

    invoke-direct {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->getEmojisRecylerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->emojisAdapter:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetEmojisAdapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void

    :cond_0
    invoke-static {p2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p2}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v0
.end method
