.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;
.super Lx/m/c/k;
.source "WidgetGuildProfileSheet.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureActions(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $canChangeNickname$inlined:Z

.field public final synthetic $guildId$inlined:J

.field public final synthetic $nick$inlined:Ljava/lang/String;

.field public final synthetic this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;ZJLjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    iput-boolean p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->$canChangeNickname$inlined:Z

    iput-wide p3, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->$guildId$inlined:J

    iput-object p5, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->$nick$inlined:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->$guildId$inlined:J

    iget-object v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$3;->$nick$inlined:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->access$showChangeNicknameDialog(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;JLjava/lang/String;)V

    return-void
.end method
