.class public final Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;
.super Lcom/discord/widgets/guilds/profile/EmojiItem;
.source "WidgetGuildProfileSheetEmojisAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/profile/EmojiItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EmojiData"
.end annotation


# instance fields
.field private final emoji:Lcom/discord/models/domain/emoji/Emoji;


# direct methods
.method public constructor <init>(Lcom/discord/models/domain/emoji/Emoji;)V
    .locals 2

    const-string v0, "emoji"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/guilds/profile/EmojiItem;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;Lcom/discord/models/domain/emoji/Emoji;ILjava/lang/Object;)Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->copy(Lcom/discord/models/domain/emoji/Emoji;)Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/discord/models/domain/emoji/Emoji;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    return-object v0
.end method

.method public final copy(Lcom/discord/models/domain/emoji/Emoji;)Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;
    .locals 1

    const-string v0, "emoji"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;

    invoke-direct {v0, p1}, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;-><init>(Lcom/discord/models/domain/emoji/Emoji;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    iget-object p1, p1, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEmoji()Lcom/discord/models/domain/emoji/Emoji;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "EmojiData(emoji="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$EmojiData;->emoji:Lcom/discord/models/domain/emoji/Emoji;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
