.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheet.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureActions(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic $guildId:J

.field public final synthetic this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    iput-wide p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;->$guildId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    invoke-static {p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->access$getViewModel$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    move-result-object p1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$1;->$guildId:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->onClickMarkAsRead(Landroid/content/Context;J)V

    return-void
.end method
