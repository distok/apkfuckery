.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheet.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->configureActions(JLcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic $actions$inlined:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

.field public final synthetic $guildId$inlined:J

.field public final synthetic this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;J)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    iput-object p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;->$actions$inlined:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    iput-wide p3, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;->$guildId$inlined:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;)V
    .locals 4

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;

    invoke-static {v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;->access$getViewModel$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;->$guildId$inlined:J

    const-string v3, "checked"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, v1, v2, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->setHideMutedChannels(JZ)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheet$configureActions$$inlined$apply$lambda$5;->call(Ljava/lang/Boolean;)V

    return-void
.end method
