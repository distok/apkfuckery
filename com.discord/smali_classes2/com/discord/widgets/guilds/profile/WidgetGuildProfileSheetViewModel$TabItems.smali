.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TabItems"
.end annotation


# instance fields
.field private final ableToInstantInvite:Z

.field private final canAccessSettings:Z

.field private final premiumSubscriptionCount:I


# direct methods
.method public constructor <init>(ZZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    iput-boolean p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    iput p3, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;ZZIILjava/lang/Object;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->copy(ZZI)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    return v0
.end method

.method public final copy(ZZI)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    invoke-direct {v0, p1, p2, p3}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;-><init>(ZZI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    iget-boolean v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    iget-boolean v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    iget p1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAbleToInstantInvite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    return v0
.end method

.method public final getCanAccessSettings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    return v0
.end method

.method public final getPremiumSubscriptionCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "TabItems(canAccessSettings="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->canAccessSettings:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ableToInstantInvite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->ableToInstantInvite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", premiumSubscriptionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->premiumSubscriptionCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
