.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheetViewModel.kt"

# interfaces
.implements Lrx/functions/Func9;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "T6:",
        "Ljava/lang/Object;",
        "T7:",
        "Ljava/lang/Object;",
        "T8:",
        "Ljava/lang/Object;",
        "T9:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func9<",
        "Lcom/discord/models/domain/ModelGuild;",
        "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/models/domain/ModelUser;",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        "Ljava/util/List<",
        "Ljava/lang/Long;",
        ">;",
        "Ljava/util/List<",
        "Lcom/discord/models/domain/emoji/Emoji;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lcom/discord/utilities/channel/GuildChannelsInfo;",
        "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;",
        ">;"
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;

    invoke-direct {v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;-><init>()V

    sput-object v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;->INSTANCE:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/discord/utilities/channel/GuildChannelsInfo;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelGuild;",
            "Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;",
            "Ljava/lang/Boolean;",
            "Lcom/discord/models/domain/ModelUser;",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List<",
            "Lcom/discord/models/domain/emoji/Emoji;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/discord/utilities/channel/GuildChannelsInfo;",
            ")",
            "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;"
        }
    .end annotation

    new-instance v11, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;

    const-string v0, "guildChannelsInfo"

    move-object/from16 v3, p9

    invoke-static {v3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "me"

    move-object/from16 v4, p4

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restrictedGuildIds"

    move-object/from16 v6, p6

    invoke-static {v6, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/discord/stores/StoreUserSettings;->getDeveloperMode()Z

    move-result v7

    if-eqz p7, :cond_0

    move-object/from16 v8, p7

    goto :goto_0

    :cond_0
    sget-object v0, Lx/h/l;->d:Lx/h/l;

    move-object v8, v0

    :goto_0
    const-string v0, "isLurking"

    move-object/from16 v1, p8

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p8 .. p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    const-string v0, "isUnread"

    move-object v1, p3

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move-object v0, v11

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p9

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v10}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;-><init>(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;Lcom/discord/utilities/channel/GuildChannelsInfo;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/List;ZLjava/util/List;ZZ)V

    return-object v11
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelGuild;

    check-cast p2, Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;

    check-cast p3, Ljava/lang/Boolean;

    check-cast p4, Lcom/discord/models/domain/ModelUser;

    check-cast p5, Lcom/discord/models/domain/ModelGuildMember$Computed;

    check-cast p6, Ljava/util/List;

    check-cast p7, Ljava/util/List;

    check-cast p8, Ljava/lang/Boolean;

    check-cast p9, Lcom/discord/utilities/channel/GuildChannelsInfo;

    invoke-virtual/range {p0 .. p9}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;->call(Lcom/discord/models/domain/ModelGuild;Lcom/discord/stores/StoreGuildProfiles$GuildProfileData;Ljava/lang/Boolean;Lcom/discord/models/domain/ModelUser;Lcom/discord/models/domain/ModelGuildMember$Computed;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/discord/utilities/channel/GuildChannelsInfo;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;

    move-result-object p1

    return-object p1
.end method
