.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheetViewModel.kt"

# interfaces
.implements Lg0/k/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->observeStores()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lg0/k/b<",
        "Lcom/discord/models/domain/ModelUser;",
        "Lrx/Observable<",
        "+",
        "Lcom/discord/models/domain/ModelGuildMember$Computed;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/models/domain/ModelUser;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1;->call(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/discord/models/domain/ModelUser;)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/discord/models/domain/ModelUser;",
            ")",
            "Lrx/Observable<",
            "+",
            "Lcom/discord/models/domain/ModelGuildMember$Computed;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v0

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;

    invoke-static {v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->access$getGuildId$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;)J

    move-result-wide v1

    const-string v3, "me"

    invoke-static {p1, v3}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/discord/models/domain/ModelUser;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeComputed(JLjava/util/Collection;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1$1;

    invoke-direct {v1, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1$1;-><init>(Lcom/discord/models/domain/ModelUser;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
