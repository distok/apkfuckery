.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$1;
.super Lx/m/c/k;
.source "WidgetGuildProfileSheetViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreMessageAck;ZLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreAnalytics;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;


# direct methods
.method public constructor <init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;)V
    .locals 0

    iput-object p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;

    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$1;->invoke(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V
    .locals 1

    const-string v0, "storeState"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$1;->this$0:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    invoke-static {v0, p1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;->access$handleStoreState(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;)V

    return-void
.end method
