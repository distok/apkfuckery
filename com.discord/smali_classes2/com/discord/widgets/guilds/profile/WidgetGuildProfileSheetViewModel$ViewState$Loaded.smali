.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;
.super Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;
.source "WidgetGuildProfileSheetViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Loaded"
.end annotation


# instance fields
.field private final actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

.field private final banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

.field private final bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

.field private final emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

.field private final guildDescription:Ljava/lang/String;

.field private final guildIcon:Ljava/lang/String;

.field private final guildId:J

.field private final guildName:Ljava/lang/String;

.field private final guildShortName:Ljava/lang/String;

.field private final memberCount:Ljava/lang/Integer;

.field private final onlineCount:Ljava/lang/Integer;

.field private final tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

.field private final verifiedPartneredIconRes:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V
    .locals 1

    const-string v0, "guildName"

    invoke-static {p3, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildShortName"

    invoke-static {p4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "banner"

    invoke-static {p8, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emojisData"

    invoke-static {p13, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomActions"

    invoke-static {p14, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    iput-object p3, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    iput-object p4, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    iput-object p5, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    iput-object p6, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    iput-object p7, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    iput-object p9, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    iput-object p10, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    iput-object p11, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    iput-object p12, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    iput-object p13, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    iput-object p14, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;ILjava/lang/Object;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;
    .locals 15

    move-object v0, p0

    move/from16 v1, p15

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    goto :goto_0

    :cond_0
    move-wide/from16 v2, p1

    :goto_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object/from16 v4, p3

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object/from16 v5, p4

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget-object v6, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object/from16 v6, p5

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget-object v7, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    goto :goto_6

    :cond_6
    move-object/from16 v9, p8

    :goto_6
    and-int/lit16 v10, v1, 0x80

    if-eqz v10, :cond_7

    iget-object v10, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    goto :goto_7

    :cond_7
    move-object/from16 v10, p9

    :goto_7
    and-int/lit16 v11, v1, 0x100

    if-eqz v11, :cond_8

    iget-object v11, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    goto :goto_8

    :cond_8
    move-object/from16 v11, p10

    :goto_8
    and-int/lit16 v12, v1, 0x200

    if-eqz v12, :cond_9

    iget-object v12, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    goto :goto_9

    :cond_9
    move-object/from16 v12, p11

    :goto_9
    and-int/lit16 v13, v1, 0x400

    if-eqz v13, :cond_a

    iget-object v13, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    goto :goto_a

    :cond_a
    move-object/from16 v13, p12

    :goto_a
    and-int/lit16 v14, v1, 0x800

    if-eqz v14, :cond_b

    iget-object v14, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    goto :goto_b

    :cond_b
    move-object/from16 v14, p13

    :goto_b
    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    goto :goto_c

    :cond_c
    move-object/from16 v1, p14

    :goto_c
    move-wide/from16 p1, v2

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p14, v1

    invoke-virtual/range {p0 .. p14}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->copy(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    return-wide v0
.end method

.method public final component10()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    return-object v0
.end method

.method public final component11()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    return-object v0
.end method

.method public final component12()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    return-object v0
.end method

.method public final component13()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component7()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    return-object v0
.end method

.method public final component8()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public final component9()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public final copy(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;
    .locals 16

    const-string v0, "guildName"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "guildShortName"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "banner"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emojisData"

    move-object/from16 v14, p13

    invoke-static {v14, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomActions"

    move-object/from16 v15, p14

    invoke-static {v15, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    move-object v1, v0

    move-wide/from16 v2, p1

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v1 .. v15}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    iget-wide v2, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    iget-object v1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    iget-object p1, p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActions()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    return-object v0
.end method

.method public final getBanner()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    return-object v0
.end method

.method public final getBottomActions()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    return-object v0
.end method

.method public final getEmojisData()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    return-object v0
.end method

.method public final getGuildDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildIcon()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildId()J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    return-wide v0
.end method

.method public final getGuildName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuildShortName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    return-object v0
.end method

.method public final getMemberCount()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getOnlineCount()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getTabItems()Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    return-object v0
.end method

.method public final getVerifiedPartneredIconRes()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    invoke-static {v0, v1}, Ld;->a(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "Loaded(guildId="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", guildName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", guildShortName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildShortName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", guildIcon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildIcon:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", guildDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->guildDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", verifiedPartneredIconRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->verifiedPartneredIconRes:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", banner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->banner:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Banner;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onlineCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->onlineCount:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", memberCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->memberCount:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tabItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->tabItems:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$TabItems;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", actions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->actions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Actions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emojisData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->emojisData:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$EmojisData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bottomActions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$ViewState$Loaded;->bottomActions:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$BottomActions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
