.class public final Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;
.super Lcom/discord/widgets/guilds/profile/EmojiItem;
.source "WidgetGuildProfileSheetEmojisAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/profile/EmojiItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoreEmoji"
.end annotation


# instance fields
.field private final extraEmojiCount:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/discord/widgets/guilds/profile/EmojiItem;-><init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;IILjava/lang/Object;)Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->copy(I)Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    return v0
.end method

.method public final copy(I)Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;
    .locals 1

    new-instance v0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;

    invoke-direct {v0, p1}, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;

    iget v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    iget p1, p1, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getExtraEmojiCount()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "MoreEmoji(extraEmojiCount="

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/discord/widgets/guilds/profile/EmojiItem$MoreEmoji;->extraEmojiCount:I

    const-string v2, ")"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
