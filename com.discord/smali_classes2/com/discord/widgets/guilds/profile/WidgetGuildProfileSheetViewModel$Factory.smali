.class public final Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;
.super Ljava/lang/Object;
.source "WidgetGuildProfileSheetViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# instance fields
.field private final guildId:J

.field private final viewingGuild:Z


# direct methods
.method public constructor <init>(JZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    iput-boolean p3, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->viewingGuild:Z

    return-void
.end method

.method public static final synthetic access$getGuildId$p(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;)J
    .locals 2

    iget-wide v0, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    return-wide v0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;

    invoke-virtual {p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->observeStores()Lrx/Observable;

    move-result-object v2

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v3

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserGuildSettings()Lcom/discord/stores/StoreUserGuildSettings;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getMessageAck()Lcom/discord/stores/StoreMessageAck;

    move-result-object v5

    iget-boolean v6, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->viewingGuild:Z

    sget-object v1, Lcom/discord/utilities/rest/RestAPI;->Companion:Lcom/discord/utilities/rest/RestAPI$Companion;

    invoke-virtual {v1}, Lcom/discord/utilities/rest/RestAPI$Companion;->getApi()Lcom/discord/utilities/rest/RestAPI;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getLurking()Lcom/discord/stores/StoreLurking;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getAnalytics()Lcom/discord/stores/StoreAnalytics;

    move-result-object v9

    iget-wide v10, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    move-object v1, p1

    invoke-direct/range {v1 .. v11}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel;-><init>(Lrx/Observable;Lcom/discord/stores/StoreUserSettings;Lcom/discord/stores/StoreUserGuildSettings;Lcom/discord/stores/StoreMessageAck;ZLcom/discord/utilities/rest/RestAPI;Lcom/discord/stores/StoreLurking;Lcom/discord/stores/StoreAnalytics;J)V

    return-object p1
.end method

.method public final observeStores()Lrx/Observable;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$StoreState;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/discord/stores/StoreStream;->Companion:Lcom/discord/stores/StoreStream$Companion;

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuilds()Lcom/discord/stores/StoreGuilds;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuilds;->observeGuild(J)Lrx/Observable;

    move-result-object v4

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getGuildProfiles()Lcom/discord/stores/StoreGuildProfiles;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreGuildProfiles;->observeGuildProfile(J)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getReadStates()Lcom/discord/stores/StoreReadStates;

    move-result-object v1

    iget-wide v2, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    invoke-virtual {v1, v2, v3}, Lcom/discord/stores/StoreReadStates;->getIsUnread(J)Lrx/Observable;

    move-result-object v6

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v7

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUsers()Lcom/discord/stores/StoreUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUser;->observeMe()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1;

    invoke-direct {v2, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$1;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;)V

    invoke-virtual {v1, v2}, Lrx/Observable;->T(Lg0/k/b;)Lrx/Observable;

    move-result-object v8

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getUserSettings()Lcom/discord/stores/StoreUserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/discord/stores/StoreUserSettings;->getRestrictedGuildIds()Lrx/Observable;

    move-result-object v9

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getEmojis()Lcom/discord/stores/StoreEmoji;

    move-result-object v1

    new-instance v2, Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;

    iget-wide v10, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    invoke-direct {v2, v10, v11}, Lcom/discord/stores/StoreEmoji$EmojiContext$GuildProfile;-><init>(J)V

    const/4 v3, 0x1

    const/4 v10, 0x0

    invoke-virtual {v1, v2, v3, v10}, Lcom/discord/stores/StoreEmoji;->getEmojiSet(Lcom/discord/stores/StoreEmoji$EmojiContext;ZZ)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$2;

    invoke-direct {v2, p0}, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$2;-><init>(Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;)V

    invoke-virtual {v1, v2}, Lrx/Observable;->C(Lg0/k/b;)Lrx/Observable;

    move-result-object v10

    invoke-virtual {v0}, Lcom/discord/stores/StoreStream$Companion;->getLurking()Lcom/discord/stores/StoreLurking;

    move-result-object v0

    iget-wide v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/stores/StoreLurking;->isLurkingObs(J)Lrx/Observable;

    move-result-object v11

    sget-object v0, Lcom/discord/utilities/channel/GuildChannelsInfo;->Companion:Lcom/discord/utilities/channel/GuildChannelsInfo$Companion;

    iget-wide v1, p0, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory;->guildId:J

    invoke-virtual {v0, v1, v2}, Lcom/discord/utilities/channel/GuildChannelsInfo$Companion;->get(J)Lrx/Observable;

    move-result-object v12

    sget-object v13, Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;->INSTANCE:Lcom/discord/widgets/guilds/profile/WidgetGuildProfileSheetViewModel$Factory$observeStores$3;

    invoke-static/range {v4 .. v13}, Lrx/Observable;->c(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;)Lrx/Observable;

    move-result-object v0

    const-string v1, "Observable\n            .\u2026          )\n            }"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
