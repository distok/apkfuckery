.class public final Ly/a/c1;
.super Ljava/lang/Object;
.source "JobSupport.kt"


# static fields
.field public static final a:Ly/a/s1/n;

.field public static final b:Ly/a/s1/n;

.field public static final c:Ly/a/s1/n;

.field public static final d:Ly/a/s1/n;

.field public static final e:Ly/a/s1/n;

.field public static final f:Ly/a/k0;

.field public static final g:Ly/a/k0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ly/a/s1/n;

    const-string v1, "COMPLETING_ALREADY"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/c1;->a:Ly/a/s1/n;

    new-instance v0, Ly/a/s1/n;

    const-string v1, "COMPLETING_WAITING_CHILDREN"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/c1;->b:Ly/a/s1/n;

    new-instance v0, Ly/a/s1/n;

    const-string v1, "COMPLETING_RETRY"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/c1;->c:Ly/a/s1/n;

    new-instance v0, Ly/a/s1/n;

    const-string v1, "TOO_LATE_TO_CANCEL"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/c1;->d:Ly/a/s1/n;

    new-instance v0, Ly/a/s1/n;

    const-string v1, "SEALED"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/c1;->e:Ly/a/s1/n;

    new-instance v0, Ly/a/k0;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ly/a/k0;-><init>(Z)V

    sput-object v0, Ly/a/c1;->f:Ly/a/k0;

    new-instance v0, Ly/a/k0;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ly/a/k0;-><init>(Z)V

    sput-object v0, Ly/a/c1;->g:Ly/a/k0;

    return-void
.end method

.method public static final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    instance-of v0, p0, Ly/a/u0;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p0

    :goto_0
    check-cast v0, Ly/a/u0;

    if-eqz v0, :cond_1

    iget-object v0, v0, Ly/a/u0;->a:Ly/a/t0;

    if-eqz v0, :cond_1

    move-object p0, v0

    :cond_1
    return-object p0
.end method
