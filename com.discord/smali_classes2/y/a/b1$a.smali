.class public final Ly/a/b1$a;
.super Ly/a/a1;
.source "JobSupport.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly/a/b1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly/a/a1<",
        "Lkotlinx/coroutines/Job;",
        ">;"
    }
.end annotation


# instance fields
.field public final h:Ly/a/b1;

.field public final i:Ly/a/b1$b;

.field public final j:Ly/a/k;

.field public final k:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ly/a/b1;Ly/a/b1$b;Ly/a/k;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p3, Ly/a/k;->h:Ly/a/l;

    invoke-direct {p0, v0}, Ly/a/a1;-><init>(Lkotlinx/coroutines/Job;)V

    iput-object p1, p0, Ly/a/b1$a;->h:Ly/a/b1;

    iput-object p2, p0, Ly/a/b1$a;->i:Ly/a/b1$b;

    iput-object p3, p0, Ly/a/b1$a;->j:Ly/a/k;

    iput-object p4, p0, Ly/a/b1$a;->k:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Ly/a/b1$a;->j(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public j(Ljava/lang/Throwable;)V
    .locals 3

    iget-object p1, p0, Ly/a/b1$a;->h:Ly/a/b1;

    iget-object v0, p0, Ly/a/b1$a;->i:Ly/a/b1$b;

    iget-object v1, p0, Ly/a/b1$a;->j:Ly/a/k;

    iget-object v2, p0, Ly/a/b1$a;->k:Ljava/lang/Object;

    invoke-virtual {p1, v1}, Ly/a/b1;->G(Ly/a/s1/g;)Ly/a/k;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0, v1, v2}, Ly/a/b1;->O(Ly/a/b1$b;Ly/a/k;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0, v2}, Ly/a/b1;->t(Ly/a/b1$b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ly/a/b1;->h(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ChildCompletion["

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ly/a/b1$a;->j:Ly/a/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ly/a/b1$a;->k:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
