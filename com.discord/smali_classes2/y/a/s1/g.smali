.class public Ly/a/s1/g;
.super Ljava/lang/Object;
.source "LockFreeLinkedList.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly/a/s1/g$a;
    }
.end annotation


# static fields
.field public static final d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

.field public static final e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

.field public static final f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field public volatile _next:Ljava/lang/Object;

.field public volatile _prev:Ljava/lang/Object;

.field private volatile _removedRef:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-class v0, Ljava/lang/Object;

    const-class v1, Ly/a/s1/g;

    const-string v2, "_next"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v2

    sput-object v2, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const-string v2, "_prev"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v2

    sput-object v2, Ly/a/s1/g;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const-string v2, "_removedRef"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Ly/a/s1/g;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p0, p0, Ly/a/s1/g;->_next:Ljava/lang/Object;

    iput-object p0, p0, Ly/a/s1/g;->_prev:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Ly/a/s1/g;->_removedRef:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final b(Ly/a/s1/k;)Ly/a/s1/g;
    .locals 6

    :goto_0
    iget-object p1, p0, Ly/a/s1/g;->_prev:Ljava/lang/Object;

    check-cast p1, Ly/a/s1/g;

    const/4 v0, 0x0

    move-object v1, p1

    :goto_1
    move-object v2, v0

    :goto_2
    iget-object v3, v1, Ly/a/s1/g;->_next:Ljava/lang/Object;

    if-ne v3, p0, :cond_2

    if-ne p1, v1, :cond_0

    return-object v1

    :cond_0
    sget-object v0, Ly/a/s1/g;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    return-object v1

    :cond_2
    invoke-virtual {p0}, Ly/a/s1/g;->g()Z

    move-result v4

    if-eqz v4, :cond_3

    return-object v0

    :cond_3
    if-nez v3, :cond_4

    return-object v1

    :cond_4
    instance-of v4, v3, Ly/a/s1/k;

    if-eqz v4, :cond_5

    check-cast v3, Ly/a/s1/k;

    invoke-virtual {v3, v1}, Ly/a/s1/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    instance-of v4, v3, Ly/a/s1/l;

    if-eqz v4, :cond_8

    if-eqz v2, :cond_7

    sget-object v4, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    check-cast v3, Ly/a/s1/l;

    iget-object v3, v3, Ly/a/s1/l;->a:Ly/a/s1/g;

    invoke-virtual {v4, v2, v1, v3}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    :cond_6
    move-object v1, v2

    goto :goto_1

    :cond_7
    iget-object v1, v1, Ly/a/s1/g;->_prev:Ljava/lang/Object;

    check-cast v1, Ly/a/s1/g;

    goto :goto_2

    :cond_8
    if-eqz v3, :cond_9

    move-object v2, v3

    check-cast v2, Ly/a/s1/g;

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    goto :goto_2

    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final c(Ly/a/s1/g;)V
    .locals 2

    :cond_0
    iget-object v0, p1, Ly/a/s1/g;->_prev:Ljava/lang/Object;

    check-cast v0, Ly/a/s1/g;

    invoke-virtual {p0}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p1, :cond_1

    return-void

    :cond_1
    sget-object v1, Ly/a/s1/g;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p1, v0, p0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ly/a/s1/g;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ly/a/s1/g;->b(Ly/a/s1/k;)Ly/a/s1/g;

    :cond_2
    return-void
.end method

.method public final d()Ljava/lang/Object;
    .locals 2

    :goto_0
    iget-object v0, p0, Ly/a/s1/g;->_next:Ljava/lang/Object;

    instance-of v1, v0, Ly/a/s1/k;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    check-cast v0, Ly/a/s1/k;

    invoke-virtual {v0, p0}, Ly/a/s1/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final e()Ly/a/s1/g;
    .locals 2

    invoke-virtual {p0}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ly/a/s1/l;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    check-cast v1, Ly/a/s1/l;

    if-eqz v1, :cond_1

    iget-object v1, v1, Ly/a/s1/l;->a:Ly/a/s1/g;

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    move-object v1, v0

    check-cast v1, Ly/a/s1/g;

    :goto_1
    return-object v1

    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Ly/a/s1/g;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ly/a/s1/g;->b(Ly/a/s1/k;)Ly/a/s1/g;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Ly/a/s1/g;->_prev:Ljava/lang/Object;

    check-cast v0, Ly/a/s1/g;

    :goto_0
    invoke-virtual {v0}, Ly/a/s1/g;->g()Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, v0, Ly/a/s1/g;->_prev:Ljava/lang/Object;

    check-cast v0, Ly/a/s1/g;

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    invoke-virtual {p0}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ly/a/s1/l;

    return v0
.end method

.method public i()Z
    .locals 4

    :cond_0
    invoke-virtual {p0}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ly/a/s1/l;

    if-eqz v1, :cond_1

    check-cast v0, Ly/a/s1/l;

    iget-object v0, v0, Ly/a/s1/l;->a:Ly/a/s1/g;

    goto :goto_1

    :cond_1
    if-ne v0, p0, :cond_2

    check-cast v0, Ly/a/s1/g;

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_5

    move-object v1, v0

    check-cast v1, Ly/a/s1/g;

    iget-object v2, v1, Ly/a/s1/g;->_removedRef:Ljava/lang/Object;

    check-cast v2, Ly/a/s1/l;

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    new-instance v2, Ly/a/s1/l;

    invoke-direct {v2, v1}, Ly/a/s1/l;-><init>(Ly/a/s1/g;)V

    sget-object v3, Ly/a/s1/g;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->lazySet(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    sget-object v3, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3, p0, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ly/a/s1/g;->b(Ly/a/s1/k;)Ly/a/s1/g;

    :goto_1
    if-nez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_5
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
