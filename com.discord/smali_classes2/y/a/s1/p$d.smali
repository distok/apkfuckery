.class public final Ly/a/s1/p$d;
.super Lx/m/c/k;
.source "ThreadContext.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly/a/s1/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function2<",
        "Ly/a/s1/s;",
        "Lkotlin/coroutines/CoroutineContext$a;",
        "Ly/a/s1/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Ly/a/s1/p$d;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ly/a/s1/p$d;

    invoke-direct {v0}, Ly/a/s1/p$d;-><init>()V

    sput-object v0, Ly/a/s1/p$d;->d:Ly/a/s1/p$d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, Ly/a/s1/s;

    check-cast p2, Lkotlin/coroutines/CoroutineContext$a;

    instance-of v0, p2, Ly/a/l1;

    if-eqz v0, :cond_0

    check-cast p2, Ly/a/l1;

    iget-object v0, p1, Ly/a/s1/s;->c:Lkotlin/coroutines/CoroutineContext;

    invoke-interface {p2, v0}, Ly/a/l1;->o(Lkotlin/coroutines/CoroutineContext;)Ljava/lang/Object;

    move-result-object p2

    iget-object v0, p1, Ly/a/s1/s;->a:[Ljava/lang/Object;

    iget v1, p1, Ly/a/s1/s;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p1, Ly/a/s1/s;->b:I

    aput-object p2, v0, v1

    :cond_0
    return-object p1
.end method
