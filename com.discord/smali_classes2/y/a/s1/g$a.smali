.class public abstract Ly/a/s1/g$a;
.super Ly/a/s1/c;
.source "LockFreeLinkedList.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly/a/s1/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly/a/s1/c<",
        "Ly/a/s1/g;",
        ">;"
    }
.end annotation


# instance fields
.field public b:Ly/a/s1/g;

.field public final c:Ly/a/s1/g;


# direct methods
.method public constructor <init>(Ly/a/s1/g;)V
    .locals 0

    invoke-direct {p0}, Ly/a/s1/c;-><init>()V

    iput-object p1, p0, Ly/a/s1/g$a;->c:Ly/a/s1/g;

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ly/a/s1/g;

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Ly/a/s1/g$a;->c:Ly/a/s1/g;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Ly/a/s1/g$a;->b:Ly/a/s1/g;

    :goto_1
    if-eqz v0, :cond_3

    sget-object v1, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p1, p0, v0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    iget-object p1, p0, Ly/a/s1/g$a;->c:Ly/a/s1/g;

    iget-object p2, p0, Ly/a/s1/g$a;->b:Ly/a/s1/g;

    if-eqz p2, :cond_2

    invoke-virtual {p1, p2}, Ly/a/s1/g;->c(Ly/a/s1/g;)V

    goto :goto_2

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_3
    :goto_2
    return-void
.end method
