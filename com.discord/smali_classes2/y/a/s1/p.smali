.class public final Ly/a/s1/p;
.super Ljava/lang/Object;
.source "ThreadContext.kt"


# static fields
.field public static final a:Ly/a/s1/n;

.field public static final b:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/CoroutineContext$a;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ly/a/l1<",
            "*>;",
            "Lkotlin/coroutines/CoroutineContext$a;",
            "Ly/a/l1<",
            "*>;>;"
        }
    .end annotation
.end field

.field public static final d:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ly/a/s1/s;",
            "Lkotlin/coroutines/CoroutineContext$a;",
            "Ly/a/s1/s;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Ly/a/s1/s;",
            "Lkotlin/coroutines/CoroutineContext$a;",
            "Ly/a/s1/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ly/a/s1/n;

    const-string v1, "ZERO"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/s1/p;->a:Ly/a/s1/n;

    sget-object v0, Ly/a/s1/p$a;->d:Ly/a/s1/p$a;

    sput-object v0, Ly/a/s1/p;->b:Lkotlin/jvm/functions/Function2;

    sget-object v0, Ly/a/s1/p$b;->d:Ly/a/s1/p$b;

    sput-object v0, Ly/a/s1/p;->c:Lkotlin/jvm/functions/Function2;

    sget-object v0, Ly/a/s1/p$d;->d:Ly/a/s1/p$d;

    sput-object v0, Ly/a/s1/p;->d:Lkotlin/jvm/functions/Function2;

    sget-object v0, Ly/a/s1/p$c;->d:Ly/a/s1/p$c;

    sput-object v0, Ly/a/s1/p;->e:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static final a(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V
    .locals 2

    sget-object v0, Ly/a/s1/p;->a:Ly/a/s1/n;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    instance-of v0, p1, Ly/a/s1/s;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ly/a/s1/s;

    const/4 v1, 0x0

    iput v1, v0, Ly/a/s1/s;->b:I

    sget-object v0, Ly/a/s1/p;->e:Lkotlin/jvm/functions/Function2;

    invoke-interface {p0, p1, v0}, Lkotlin/coroutines/CoroutineContext;->fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    sget-object v1, Ly/a/s1/p;->c:Lkotlin/jvm/functions/Function2;

    invoke-interface {p0, v0, v1}, Lkotlin/coroutines/CoroutineContext;->fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Ly/a/l1;

    invoke-interface {v0, p0, p1}, Ly/a/l1;->i(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final b(Lkotlin/coroutines/CoroutineContext;)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Ly/a/s1/p;->b:Lkotlin/jvm/functions/Function2;

    invoke-interface {p0, v0, v1}, Lkotlin/coroutines/CoroutineContext;->fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p0, 0x0

    throw p0
.end method

.method public static final c(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ly/a/s1/p;->b(Lkotlin/coroutines/CoroutineContext;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-ne p1, v0, :cond_1

    sget-object p0, Ly/a/s1/p;->a:Ly/a/s1/n;

    goto :goto_1

    :cond_1
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    new-instance v0, Ly/a/s1/s;

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-direct {v0, p0, p1}, Ly/a/s1/s;-><init>(Lkotlin/coroutines/CoroutineContext;I)V

    sget-object p1, Ly/a/s1/p;->d:Lkotlin/jvm/functions/Function2;

    invoke-interface {p0, v0, p1}, Lkotlin/coroutines/CoroutineContext;->fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    check-cast p1, Ly/a/l1;

    invoke-interface {p1, p0}, Ly/a/l1;->o(Lkotlin/coroutines/CoroutineContext;)Ljava/lang/Object;

    move-result-object p0

    :goto_1
    return-object p0

    :cond_3
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
