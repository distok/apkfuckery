.class public final Ly/a/j0;
.super Ly/a/d;
.source "CancellableContinuation.kt"


# instance fields
.field public final d:Ly/a/i0;


# direct methods
.method public constructor <init>(Ly/a/i0;)V
    .locals 0

    invoke-direct {p0}, Ly/a/d;-><init>()V

    iput-object p1, p0, Ly/a/j0;->d:Ly/a/i0;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Throwable;)V
    .locals 0

    iget-object p1, p0, Ly/a/j0;->d:Ly/a/i0;

    invoke-interface {p1}, Ly/a/i0;->dispose()V

    return-void
.end method

.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    iget-object p1, p0, Ly/a/j0;->d:Ly/a/i0;

    invoke-interface {p1}, Ly/a/i0;->dispose()V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "DisposeOnCancel["

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ly/a/j0;->d:Ly/a/i0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
