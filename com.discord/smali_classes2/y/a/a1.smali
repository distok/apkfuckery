.class public abstract Ly/a/a1;
.super Ly/a/s;
.source "JobSupport.kt"

# interfaces
.implements Ly/a/i0;
.implements Ly/a/t0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<J::",
        "Lkotlinx/coroutines/Job;",
        ">",
        "Ly/a/s;",
        "Ly/a/i0;",
        "Ly/a/t0;"
    }
.end annotation


# instance fields
.field public final g:Lkotlinx/coroutines/Job;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TJ;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/Job;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TJ;)V"
        }
    .end annotation

    invoke-direct {p0}, Ly/a/s;-><init>()V

    iput-object p1, p0, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public dispose()V
    .locals 4

    iget-object v0, p0, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    if-eqz v0, :cond_4

    check-cast v0, Ly/a/b1;

    :cond_0
    invoke-virtual {v0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ly/a/a1;

    if-eqz v2, :cond_2

    if-eq v1, p0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v2, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    sget-object v3, Ly/a/c1;->g:Ly/a/k0;

    invoke-virtual {v2, v0, v1, v3}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_2
    instance-of v0, v1, Ly/a/t0;

    if-eqz v0, :cond_3

    check-cast v1, Ly/a/t0;

    invoke-interface {v1}, Ly/a/t0;->getList()Ly/a/f1;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ly/a/s1/g;->i()Z

    :cond_3
    :goto_0
    return-void

    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlinx.coroutines.JobSupport"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getList()Ly/a/f1;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
