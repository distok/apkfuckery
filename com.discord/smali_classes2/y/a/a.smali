.class public abstract Ly/a/a;
.super Ly/a/b1;
.source "AbstractCoroutine.kt"

# interfaces
.implements Lkotlinx/coroutines/Job;
.implements Lkotlin/coroutines/Continuation;
.implements Lkotlinx/coroutines/CoroutineScope;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ly/a/b1;",
        "Lkotlinx/coroutines/Job;",
        "Lkotlin/coroutines/Continuation<",
        "TT;>;",
        "Lkotlinx/coroutines/CoroutineScope;"
    }
.end annotation


# instance fields
.field public final e:Lkotlin/coroutines/CoroutineContext;

.field public final f:Lkotlin/coroutines/CoroutineContext;


# direct methods
.method public constructor <init>(Lkotlin/coroutines/CoroutineContext;Z)V
    .locals 0

    invoke-direct {p0, p2}, Ly/a/b1;-><init>(Z)V

    iput-object p1, p0, Ly/a/a;->f:Lkotlin/coroutines/CoroutineContext;

    invoke-interface {p1, p0}, Lkotlin/coroutines/CoroutineContext;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    iput-object p1, p0, Ly/a/a;->e:Lkotlin/coroutines/CoroutineContext;

    return-void
.end method


# virtual methods
.method public final A(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Ly/a/a;->e:Lkotlin/coroutines/CoroutineContext;

    invoke-static {v0, p1}, Lf/h/a/f/f/n/g;->I(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Throwable;)V

    return-void
.end method

.method public F()Ljava/lang/String;
    .locals 1

    sget-boolean v0, Ly/a/t;->a:Z

    invoke-super {p0}, Ly/a/b1;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final I(Ljava/lang/Object;)V
    .locals 1

    instance-of v0, p1, Ly/a/p;

    if-eqz v0, :cond_0

    check-cast p1, Ly/a/p;

    iget-object v0, p1, Ly/a/p;->a:Ljava/lang/Throwable;

    invoke-virtual {p1}, Ly/a/p;->a()Z

    :cond_0
    return-void
.end method

.method public final J()V
    .locals 0

    invoke-virtual {p0}, Ly/a/a;->R()V

    return-void
.end method

.method public P(Ljava/lang/Object;)V
    .locals 0

    invoke-virtual {p0, p1}, Ly/a/b1;->h(Ljava/lang/Object;)V

    return-void
.end method

.method public final Q()V
    .locals 2

    iget-object v0, p0, Ly/a/a;->f:Lkotlin/coroutines/CoroutineContext;

    sget-object v1, Lkotlinx/coroutines/Job;->c:Lkotlinx/coroutines/Job$a;

    invoke-interface {v0, v1}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/Job;

    invoke-virtual {p0, v0}, Ly/a/b1;->B(Lkotlinx/coroutines/Job;)V

    return-void
.end method

.method public R()V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    invoke-super {p0}, Ly/a/b1;->a()Z

    move-result v0

    return v0
.end method

.method public final getContext()Lkotlin/coroutines/CoroutineContext;
    .locals 1

    iget-object v0, p0, Ly/a/a;->e:Lkotlin/coroutines/CoroutineContext;

    return-object v0
.end method

.method public getCoroutineContext()Lkotlin/coroutines/CoroutineContext;
    .locals 1

    iget-object v0, p0, Ly/a/a;->e:Lkotlin/coroutines/CoroutineContext;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " was cancelled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final resumeWith(Ljava/lang/Object;)V
    .locals 1

    invoke-static {p1}, Lf/h/a/f/f/n/g;->d0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Ly/a/b1;->D(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Ly/a/c1;->b:Ly/a/s1/n;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Ly/a/a;->P(Ljava/lang/Object;)V

    return-void
.end method
