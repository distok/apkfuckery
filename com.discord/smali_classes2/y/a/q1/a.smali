.class public final Ly/a/q1/a;
.super Ly/a/q1/b;
.source "HandlerDispatcher.kt"

# interfaces
.implements Ly/a/b0;


# instance fields
.field public volatile _immediate:Ly/a/q1/a;

.field public final d:Ly/a/q1/a;

.field public final e:Landroid/os/Handler;

.field public final f:Ljava/lang/String;

.field public final g:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ly/a/q1/b;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    iput-object p2, p0, Ly/a/q1/a;->f:Ljava/lang/String;

    iput-boolean p3, p0, Ly/a/q1/a;->g:Z

    if-eqz p3, :cond_0

    move-object v0, p0

    :cond_0
    iput-object v0, p0, Ly/a/q1/a;->_immediate:Ly/a/q1/a;

    iget-object p3, p0, Ly/a/q1/a;->_immediate:Ly/a/q1/a;

    if-eqz p3, :cond_1

    goto :goto_0

    :cond_1
    new-instance p3, Ly/a/q1/a;

    const/4 v0, 0x1

    invoke-direct {p3, p1, p2, v0}, Ly/a/q1/a;-><init>(Landroid/os/Handler;Ljava/lang/String;Z)V

    iput-object p3, p0, Ly/a/q1/a;->_immediate:Ly/a/q1/a;

    :goto_0
    iput-object p3, p0, Ly/a/q1/a;->d:Ly/a/q1/a;

    return-void
.end method


# virtual methods
.method public b(JLy/a/f;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ly/a/f<",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ly/a/q1/a$a;

    invoke-direct {v0, p0, p3}, Ly/a/q1/a$a;-><init>(Ly/a/q1/a;Ly/a/f;)V

    iget-object v1, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    const-wide v2, 0x3fffffffffffffffL    # 1.9999999999999998

    invoke-static {p1, p2, v2, v3}, Lx/p/e;->coerceAtMost(JJ)J

    move-result-wide p1

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance p1, Ly/a/q1/a$b;

    invoke-direct {p1, p0, v0}, Ly/a/q1/a$b;-><init>(Ly/a/q1/a;Ljava/lang/Runnable;)V

    check-cast p3, Ly/a/g;

    invoke-virtual {p3, p1}, Ly/a/g;->k(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public dispatch(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 0

    iget-object p1, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Ly/a/q1/a;

    if-eqz v0, :cond_0

    check-cast p1, Ly/a/q1/a;

    iget-object p1, p1, Ly/a/q1/a;->e:Landroid/os/Handler;

    iget-object v0, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isDispatchNeeded(Lkotlin/coroutines/CoroutineContext;)Z
    .locals 2

    iget-boolean p1, p0, Ly/a/q1/a;->g:Z

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    iget-object v1, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method public t()Ly/a/e1;
    .locals 1

    iget-object v0, p0, Ly/a/q1/a;->d:Ly/a/q1/a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ly/a/e1;->v()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Ly/a/q1/a;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ly/a/q1/a;->e:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-boolean v1, p0, Ly/a/q1/a;->g:Z

    if-eqz v1, :cond_2

    const-string v1, ".immediate"

    invoke-static {v0, v1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_1
    return-object v0
.end method
