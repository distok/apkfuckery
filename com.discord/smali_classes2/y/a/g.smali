.class public Ly/a/g;
.super Ly/a/f0;
.source "CancellableContinuationImpl.kt"

# interfaces
.implements Ly/a/f;
.implements Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ly/a/f0<",
        "TT;>;",
        "Ly/a/f<",
        "TT;>;",
        "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;"
    }
.end annotation


# static fields
.field public static final i:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

.field public static final j:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field private volatile _decision:I

.field public volatile _parentHandle:Ljava/lang/Object;

.field private volatile _state:Ljava/lang/Object;

.field public final g:Lkotlin/coroutines/CoroutineContext;

.field public final h:Lkotlin/coroutines/Continuation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/coroutines/Continuation<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-class v0, Ly/a/g;

    const-string v1, "_decision"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Ly/a/g;->i:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const-class v0, Ly/a/g;

    const-class v1, Ljava/lang/Object;

    const-string v2, "_state"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Ly/a/g;->j:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>(Lkotlin/coroutines/Continuation;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Ly/a/f0;-><init>(I)V

    iput-object p1, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    invoke-interface {p1}, Lkotlin/coroutines/Continuation;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    iput-object p1, p0, Ly/a/g;->g:Lkotlin/coroutines/CoroutineContext;

    const/4 p1, 0x0

    iput p1, p0, Ly/a/g;->_decision:I

    sget-object p1, Ly/a/b;->d:Ly/a/b;

    iput-object p1, p0, Ly/a/g;->_state:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-object p1, p0, Ly/a/g;->_parentHandle:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 3

    instance-of p2, p1, Ly/a/r;

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    check-cast p1, Ly/a/r;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    throw p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception p1

    iget-object p2, p0, Ly/a/g;->g:Lkotlin/coroutines/CoroutineContext;

    new-instance v0, Lkotlinx/coroutines/CompletionHandlerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception in cancellation handler for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lkotlinx/coroutines/CompletionHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p2, v0}, Lf/h/a/f/f/n/g;->I(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public b(Ly/a/v;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ly/a/v;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    instance-of v1, v0, Ly/a/c0;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Ly/a/c0;

    if-eqz v0, :cond_1

    iget-object v2, v0, Ly/a/c0;->j:Ly/a/v;

    :cond_1
    if-ne v2, p1, :cond_2

    const/4 p1, 0x2

    goto :goto_0

    :cond_2
    iget p1, p0, Ly/a/f0;->f:I

    :goto_0
    invoke-virtual {p0, p2, p1}, Ly/a/g;->n(Ljava/lang/Object;I)Ly/a/h;

    return-void
.end method

.method public c(Ljava/lang/Throwable;)Z
    .locals 5

    :goto_0
    iget-object v0, p0, Ly/a/g;->_state:Ljava/lang/Object;

    instance-of v1, v0, Ly/a/h1;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    :cond_0
    new-instance v1, Ly/a/h;

    instance-of v3, v0, Ly/a/d;

    invoke-direct {v1, p0, p1, v3}, Ly/a/h;-><init>(Lkotlin/coroutines/Continuation;Ljava/lang/Throwable;Z)V

    sget-object v4, Ly/a/g;->j:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v4, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    :try_start_0
    check-cast v0, Ly/a/d;

    invoke-virtual {v0, p1}, Ly/a/e;->a(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Ly/a/g;->g:Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lkotlinx/coroutines/CompletionHandlerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in cancellation handler for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, p1}, Lkotlinx/coroutines/CompletionHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->I(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Throwable;)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Ly/a/g;->h()V

    invoke-virtual {p0, v2}, Ly/a/g;->i(I)V

    const/4 p1, 0x1

    return p1
.end method

.method public final d()Lkotlin/coroutines/Continuation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/coroutines/Continuation<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    return-object v0
.end method

.method public e(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    instance-of v0, p1, Ly/a/q;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Ly/a/q;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    move-object p1, v1

    goto :goto_1

    :cond_0
    instance-of v0, p1, Ly/a/r;

    if-eqz v0, :cond_1

    check-cast p1, Ly/a/r;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    :goto_1
    return-object p1
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Ly/a/g;->_state:Ljava/lang/Object;

    return-object v0
.end method

.method public getContext()Lkotlin/coroutines/CoroutineContext;
    .locals 1

    iget-object v0, p0, Ly/a/g;->g:Lkotlin/coroutines/CoroutineContext;

    return-object v0
.end method

.method public final h()V
    .locals 1

    invoke-virtual {p0}, Ly/a/g;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ly/a/g;->_parentHandle:Ljava/lang/Object;

    check-cast v0, Ly/a/i0;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ly/a/i0;->dispose()V

    :cond_0
    sget-object v0, Ly/a/g1;->d:Ly/a/g1;

    iput-object v0, p0, Ly/a/g;->_parentHandle:Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public final i(I)V
    .locals 5

    :cond_0
    iget v0, p0, Ly/a/g;->_decision:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Already resumed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    sget-object v0, Ly/a/g;->i:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v0, p0, v2, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    return-void

    :cond_3
    invoke-virtual {p0}, Ly/a/g;->d()Lkotlin/coroutines/Continuation;

    move-result-object v0

    if-eqz p1, :cond_4

    if-ne p1, v3, :cond_5

    :cond_4
    const/4 v2, 0x1

    :cond_5
    if-eqz v2, :cond_9

    instance-of v2, v0, Ly/a/c0;

    if-eqz v2, :cond_9

    invoke-static {p1}, Ly/a/g0;->t(I)Z

    move-result v2

    iget v4, p0, Ly/a/f0;->f:I

    invoke-static {v4}, Ly/a/g0;->t(I)Z

    move-result v4

    if-ne v2, v4, :cond_9

    move-object p1, v0

    check-cast p1, Ly/a/c0;

    iget-object p1, p1, Ly/a/c0;->j:Ly/a/v;

    invoke-interface {v0}, Lkotlin/coroutines/Continuation;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    invoke-virtual {p1, v0}, Ly/a/v;->isDispatchNeeded(Lkotlin/coroutines/CoroutineContext;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p1, v0, p0}, Ly/a/v;->dispatch(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    goto :goto_2

    :cond_6
    sget-object p1, Ly/a/m1;->b:Ly/a/m1;

    invoke-static {}, Ly/a/m1;->a()Ly/a/l0;

    move-result-object p1

    invoke-virtual {p1}, Ly/a/l0;->z()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1, p0}, Ly/a/l0;->w(Ly/a/f0;)V

    goto :goto_2

    :cond_7
    invoke-virtual {p1, v3}, Ly/a/l0;->y(Z)V

    :try_start_0
    invoke-virtual {p0}, Ly/a/g;->d()Lkotlin/coroutines/Continuation;

    move-result-object v0

    invoke-static {p0, v0, v1}, Ly/a/g0;->C(Ly/a/f0;Lkotlin/coroutines/Continuation;I)V

    :cond_8
    invoke-virtual {p1}, Ly/a/l0;->B()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_8

    goto :goto_1

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v0, v1}, Ly/a/f0;->f(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_1
    invoke-virtual {p1, v3}, Ly/a/l0;->t(Z)V

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-virtual {p1, v3}, Ly/a/l0;->t(Z)V

    throw v0

    :cond_9
    invoke-static {p0, v0, p1}, Ly/a/g0;->C(Ly/a/f0;Lkotlin/coroutines/Continuation;I)V

    :goto_2
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 4

    invoke-virtual {p0}, Ly/a/g;->o()V

    :cond_0
    iget v0, p0, Ly/a/g;->_decision:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already suspended"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Ly/a/g;->i:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    sget-object v0, Lx/j/g/a;->d:Lx/j/g/a;

    return-object v0

    :cond_3
    iget-object v0, p0, Ly/a/g;->_state:Ljava/lang/Object;

    instance-of v1, v0, Ly/a/p;

    if-nez v1, :cond_6

    iget v1, p0, Ly/a/f0;->f:I

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Ly/a/g;->g:Lkotlin/coroutines/CoroutineContext;

    sget-object v2, Lkotlinx/coroutines/Job;->c:Lkotlinx/coroutines/Job$a;

    invoke-interface {v1, v2}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v1

    check-cast v1, Lkotlinx/coroutines/Job;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lkotlinx/coroutines/Job;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_1

    :cond_4
    invoke-interface {v1}, Lkotlinx/coroutines/Job;->e()Ljava/util/concurrent/CancellationException;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ly/a/g;->a(Ljava/lang/Object;Ljava/lang/Throwable;)V

    throw v1

    :cond_5
    :goto_1
    invoke-virtual {p0, v0}, Ly/a/g;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_6
    check-cast v0, Ly/a/p;

    iget-object v0, v0, Ly/a/p;->a:Ljava/lang/Throwable;

    throw v0
.end method

.method public k(Lkotlin/jvm/functions/Function1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    move-object v1, v0

    :cond_0
    iget-object v2, p0, Ly/a/g;->_state:Ljava/lang/Object;

    instance-of v3, v2, Ly/a/b;

    if-eqz v3, :cond_3

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    instance-of v1, p1, Ly/a/d;

    if-eqz v1, :cond_2

    move-object v1, p1

    check-cast v1, Ly/a/d;

    goto :goto_0

    :cond_2
    new-instance v1, Ly/a/v0;

    invoke-direct {v1, p1}, Ly/a/v0;-><init>(Lkotlin/jvm/functions/Function1;)V

    :goto_0
    sget-object v3, Ly/a/g;->j:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3, p0, v2, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-void

    :cond_3
    instance-of v1, v2, Ly/a/d;

    if-nez v1, :cond_8

    instance-of v1, v2, Ly/a/h;

    if-eqz v1, :cond_7

    move-object v1, v2

    check-cast v1, Ly/a/h;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Ly/a/p;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v1

    if-eqz v1, :cond_6

    :try_start_0
    instance-of v1, v2, Ly/a/p;

    if-nez v1, :cond_4

    move-object v2, v0

    :cond_4
    check-cast v2, Ly/a/p;

    if-eqz v2, :cond_5

    iget-object v0, v2, Ly/a/p;->a:Ljava/lang/Throwable;

    :cond_5
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Ly/a/g;->g:Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lkotlinx/coroutines/CompletionHandlerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception in cancellation handler for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lkotlinx/coroutines/CompletionHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v0, v1}, Lf/h/a/f/f/n/g;->I(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Throwable;)V

    :goto_1
    return-void

    :cond_6
    invoke-virtual {p0, p1, v2}, Ly/a/g;->m(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    throw v0

    :cond_7
    return-void

    :cond_8
    invoke-virtual {p0, p1, v2}, Ly/a/g;->m(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    throw v0
.end method

.method public final l()Z
    .locals 2

    iget-object v0, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    instance-of v1, v0, Ly/a/c0;

    if-eqz v1, :cond_0

    check-cast v0, Ly/a/c0;

    invoke-virtual {v0, p0}, Ly/a/c0;->j(Ly/a/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final m(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "It\'s prohibited to register multiple handlers, tried to register "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", already has "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final n(Ljava/lang/Object;I)Ly/a/h;
    .locals 3

    :goto_0
    iget-object v0, p0, Ly/a/g;->_state:Ljava/lang/Object;

    instance-of v1, v0, Ly/a/h1;

    if-eqz v1, :cond_1

    sget-object v1, Ly/a/g;->j:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p0, v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ly/a/g;->h()V

    invoke-virtual {p0, p2}, Ly/a/g;->i(I)V

    const/4 p1, 0x0

    return-object p1

    :cond_1
    instance-of p2, v0, Ly/a/h;

    if-eqz p2, :cond_2

    check-cast v0, Ly/a/h;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p2, Ly/a/h;->c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2, v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result p2

    if-eqz p2, :cond_2

    return-object v0

    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Already resumed, but proposed with update "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final o()V
    .locals 8

    iget-object v0, p0, Ly/a/g;->_state:Ljava/lang/Object;

    instance-of v0, v0, Ly/a/h1;

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iget v2, p0, Ly/a/f0;->f:I

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    instance-of v3, v2, Ly/a/c0;

    if-nez v3, :cond_1

    const/4 v2, 0x0

    :cond_1
    check-cast v2, Ly/a/c0;

    if-eqz v2, :cond_3

    invoke-virtual {v2, p0}, Ly/a/c0;->h(Ly/a/f;)Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_3

    if-nez v0, :cond_2

    invoke-virtual {p0, v2}, Ly/a/g;->c(Ljava/lang/Throwable;)Z

    :cond_2
    const/4 v0, 0x1

    :cond_3
    :goto_0
    if-eqz v0, :cond_4

    return-void

    :cond_4
    iget-object v0, p0, Ly/a/g;->_parentHandle:Ljava/lang/Object;

    check-cast v0, Ly/a/i0;

    if-eqz v0, :cond_5

    return-void

    :cond_5
    iget-object v0, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    invoke-interface {v0}, Lkotlin/coroutines/Continuation;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    sget-object v2, Lkotlinx/coroutines/Job;->c:Lkotlinx/coroutines/Job$a;

    invoke-interface {v0, v2}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lkotlinx/coroutines/Job;

    if-eqz v2, :cond_6

    invoke-interface {v2}, Lkotlinx/coroutines/Job;->start()Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v5, Ly/a/i;

    invoke-direct {v5, v2, p0}, Ly/a/i;-><init>(Lkotlinx/coroutines/Job;Ly/a/g;)V

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Ly/a/g0;->r(Lkotlinx/coroutines/Job;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ly/a/i0;

    move-result-object v0

    iput-object v0, p0, Ly/a/g;->_parentHandle:Ljava/lang/Object;

    iget-object v2, p0, Ly/a/g;->_state:Ljava/lang/Object;

    instance-of v2, v2, Ly/a/h1;

    xor-int/2addr v1, v2

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Ly/a/g;->l()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-interface {v0}, Ly/a/i0;->dispose()V

    sget-object v0, Ly/a/g1;->d:Ly/a/g1;

    iput-object v0, p0, Ly/a/g;->_parentHandle:Ljava/lang/Object;

    :cond_6
    return-void
.end method

.method public resumeWith(Ljava/lang/Object;)V
    .locals 3

    invoke-static {p1}, Lx/d;->exceptionOrNull-impl(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x2

    new-instance v2, Ly/a/p;

    invoke-direct {v2, v0, p1, v1}, Ly/a/p;-><init>(Ljava/lang/Throwable;ZI)V

    move-object p1, v2

    :goto_0
    iget v0, p0, Ly/a/f0;->f:I

    invoke-virtual {p0, p1, v0}, Ly/a/g;->n(Ljava/lang/Object;I)Ly/a/h;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CancellableContinuation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    invoke-static {v1}, Lf/h/a/f/f/n/g;->c0(Lkotlin/coroutines/Continuation;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "){"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ly/a/g;->_state:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lf/h/a/f/f/n/g;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
