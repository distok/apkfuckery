.class public final Ly/a/t1/b;
.super Ly/a/t1/c;
.source "Dispatcher.kt"


# static fields
.field public static final i:Ly/a/v;

.field public static final j:Ly/a/t1/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    new-instance v0, Ly/a/t1/b;

    invoke-direct {v0}, Ly/a/t1/b;-><init>()V

    sput-object v0, Ly/a/t1/b;->j:Ly/a/t1/b;

    new-instance v1, Ly/a/t1/e;

    sget v2, Ly/a/s1/o;->a:I

    const/16 v3, 0x40

    if-ge v3, v2, :cond_0

    move v5, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x40

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    const-string v4, "kotlinx.coroutines.io.parallelism"

    invoke-static/range {v4 .. v9}, Ly/a/g0;->M(Ljava/lang/String;IIIILjava/lang/Object;)I

    move-result v2

    const/4 v3, 0x1

    const-string v4, "Dispatchers.IO"

    invoke-direct {v1, v0, v2, v4, v3}, Ly/a/t1/e;-><init>(Ly/a/t1/c;ILjava/lang/String;I)V

    sput-object v1, Ly/a/t1/b;->i:Ly/a/v;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x7

    invoke-direct {p0, v0, v0, v1, v2}, Ly/a/t1/c;-><init>(IILjava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Dispatchers.Default cannot be closed"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "Dispatchers.Default"

    return-object v0
.end method
