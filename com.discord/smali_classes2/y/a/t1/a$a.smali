.class public final Ly/a/t1/a$a;
.super Ljava/lang/Thread;
.source "CoroutineScheduler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly/a/t1/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# static fields
.field public static final k:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;


# instance fields
.field public final d:Ly/a/t1/m;

.field public e:Ly/a/t1/a$b;

.field public f:J

.field public g:J

.field public h:I

.field public i:Z

.field public volatile indexInArray:I

.field public final synthetic j:Ly/a/t1/a;

.field public volatile nextParkedWorker:Ljava/lang/Object;

.field public volatile workerCtl:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Ly/a/t1/a$a;

    const-string/jumbo v1, "workerCtl"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Ly/a/t1/a$a;->k:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(Ly/a/t1/a;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    iput-object p1, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/lang/Thread;->setDaemon(Z)V

    new-instance p1, Ly/a/t1/m;

    invoke-direct {p1}, Ly/a/t1/m;-><init>()V

    iput-object p1, p0, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    sget-object p1, Ly/a/t1/a$b;->g:Ly/a/t1/a$b;

    iput-object p1, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    const/4 p1, 0x0

    iput p1, p0, Ly/a/t1/a$a;->workerCtl:I

    sget-object p1, Ly/a/t1/a;->n:Ly/a/s1/n;

    iput-object p1, p0, Ly/a/t1/a$a;->nextParkedWorker:Ljava/lang/Object;

    sget-object p1, Lx/o/c;->b:Lx/o/c$a;

    invoke-virtual {p1}, Lx/o/c$a;->nextInt()I

    move-result p1

    iput p1, p0, Ly/a/t1/a$a;->h:I

    invoke-virtual {p0, p2}, Ly/a/t1/a$a;->d(I)V

    return-void
.end method


# virtual methods
.method public final a(Z)Ly/a/t1/h;
    .locals 10

    sget-object v0, Ly/a/t1/a$b;->d:Ly/a/t1/a$b;

    iget-object v1, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    :cond_1
    iget-wide v6, v1, Ly/a/t1/a;->controlState:J

    const-wide v4, 0x7ffffc0000000000L

    and-long/2addr v4, v6

    const/16 v8, 0x2a

    shr-long/2addr v4, v8

    long-to-int v5, v4

    if-nez v5, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const-wide v4, 0x40000000000L

    sub-long v8, v6, v4

    sget-object v4, Ly/a/t1/a;->l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-object v5, v1

    invoke-virtual/range {v4 .. v9}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->compareAndSet(Ljava/lang/Object;JJ)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    iput-object v0, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    :goto_1
    const/4 v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_9

    if-eqz p1, :cond_7

    iget-object p1, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget p1, p1, Ly/a/t1/a;->g:I

    mul-int/lit8 p1, p1, 0x2

    invoke-virtual {p0, p1}, Ly/a/t1/a$a;->b(I)I

    move-result p1

    if-nez p1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ly/a/t1/a$a;->c()Ly/a/t1/h;

    move-result-object p1

    if-eqz p1, :cond_5

    goto :goto_4

    :cond_5
    iget-object p1, p0, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    invoke-virtual {p1}, Ly/a/t1/m;->e()Ly/a/t1/h;

    move-result-object p1

    if-eqz p1, :cond_6

    goto :goto_4

    :cond_6
    if-nez v2, :cond_8

    invoke-virtual {p0}, Ly/a/t1/a$a;->c()Ly/a/t1/h;

    move-result-object p1

    if-eqz p1, :cond_8

    goto :goto_4

    :cond_7
    invoke-virtual {p0}, Ly/a/t1/a$a;->c()Ly/a/t1/h;

    move-result-object p1

    if-eqz p1, :cond_8

    goto :goto_4

    :cond_8
    invoke-virtual {p0, v3}, Ly/a/t1/a$a;->f(Z)Ly/a/t1/h;

    move-result-object p1

    :goto_4
    return-object p1

    :cond_9
    if-eqz p1, :cond_b

    iget-object p1, p0, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    invoke-virtual {p1}, Ly/a/t1/m;->e()Ly/a/t1/h;

    move-result-object p1

    if-eqz p1, :cond_a

    goto :goto_5

    :cond_a
    iget-object p1, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object p1, p1, Ly/a/t1/a;->e:Ly/a/t1/d;

    invoke-virtual {p1}, Ly/a/s1/h;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ly/a/t1/h;

    goto :goto_5

    :cond_b
    iget-object p1, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object p1, p1, Ly/a/t1/a;->e:Ly/a/t1/d;

    invoke-virtual {p1}, Ly/a/s1/h;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ly/a/t1/h;

    :goto_5
    if-eqz p1, :cond_c

    goto :goto_6

    :cond_c
    invoke-virtual {p0, v2}, Ly/a/t1/a$a;->f(Z)Ly/a/t1/h;

    move-result-object p1

    :goto_6
    return-object p1
.end method

.method public final b(I)I
    .locals 3

    iget v0, p0, Ly/a/t1/a$a;->h:I

    shl-int/lit8 v1, v0, 0xd

    xor-int/2addr v0, v1

    shr-int/lit8 v1, v0, 0x11

    xor-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x5

    xor-int/2addr v0, v1

    iput v0, p0, Ly/a/t1/a$a;->h:I

    add-int/lit8 v1, p1, -0x1

    and-int v2, v1, p1

    if-nez v2, :cond_0

    and-int p1, v0, v1

    return p1

    :cond_0
    const v1, 0x7fffffff

    and-int/2addr v0, v1

    rem-int/2addr v0, p1

    return v0
.end method

.method public final c()Ly/a/t1/h;
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ly/a/t1/a$a;->b(I)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v0, v0, Ly/a/t1/a;->d:Ly/a/t1/d;

    invoke-virtual {v0}, Ly/a/s1/h;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/a/t1/h;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v0, v0, Ly/a/t1/a;->e:Ly/a/t1/d;

    invoke-virtual {v0}, Ly/a/s1/h;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/a/t1/h;

    return-object v0

    :cond_1
    iget-object v0, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v0, v0, Ly/a/t1/a;->e:Ly/a/t1/d;

    invoke-virtual {v0}, Ly/a/s1/h;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/a/t1/h;

    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    iget-object v0, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v0, v0, Ly/a/t1/a;->d:Ly/a/t1/d;

    invoke-virtual {v0}, Ly/a/s1/h;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/a/t1/h;

    return-object v0
.end method

.method public final d(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v1, v1, Ly/a/t1/a;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-worker-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_0

    const-string v1, "TERMINATED"

    goto :goto_0

    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    iput p1, p0, Ly/a/t1/a$a;->indexInArray:I

    return-void
.end method

.method public final e(Ly/a/t1/a$b;)Z
    .locals 6

    iget-object v0, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    sget-object v1, Ly/a/t1/a$b;->d:Ly/a/t1/a$b;

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v2, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    sget-object v3, Ly/a/t1/a;->l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    const-wide v4, 0x40000000000L

    invoke-virtual {v3, v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->addAndGet(Ljava/lang/Object;J)J

    :cond_1
    if-eq v0, p1, :cond_2

    iput-object p1, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    :cond_2
    return v1
.end method

.method public final f(Z)Ly/a/t1/h;
    .locals 19

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-wide v1, v1, Ly/a/t1/a;->controlState:J

    const-wide/32 v3, 0x1fffff

    and-long/2addr v1, v3

    long-to-int v2, v1

    const/4 v1, 0x0

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0, v2}, Ly/a/t1/a$a;->b(I)I

    move-result v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide v8, 0x7fffffffffffffffL

    :goto_0
    const-wide/16 v10, 0x0

    if-ge v7, v2, :cond_6

    const/4 v12, 0x1

    add-int/2addr v3, v12

    if-le v3, v2, :cond_1

    const/4 v3, 0x1

    :cond_1
    iget-object v12, v0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v12, v12, Ly/a/t1/a;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v12, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ly/a/t1/a$a;

    if-eqz v12, :cond_5

    if-eq v12, v0, :cond_5

    const-wide/16 v13, -0x1

    if-eqz p1, :cond_2

    iget-object v15, v0, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    iget-object v12, v12, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    invoke-virtual {v15, v12}, Ly/a/t1/m;->g(Ly/a/t1/m;)J

    move-result-wide v15

    move-wide v4, v15

    goto :goto_2

    :cond_2
    iget-object v15, v0, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    iget-object v12, v12, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    invoke-static {v15}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v12}, Ly/a/t1/m;->f()Ly/a/t1/h;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v15, v1, v6}, Ly/a/t1/m;->a(Ly/a/t1/h;Z)Ly/a/t1/h;

    move-wide/from16 v17, v13

    goto :goto_1

    :cond_3
    invoke-virtual {v15, v12, v6}, Ly/a/t1/m;->h(Ly/a/t1/m;Z)J

    move-result-wide v17

    :goto_1
    move-wide/from16 v4, v17

    :goto_2
    cmp-long v1, v4, v13

    if-nez v1, :cond_4

    iget-object v1, v0, Ly/a/t1/a$a;->d:Ly/a/t1/m;

    invoke-virtual {v1}, Ly/a/t1/m;->e()Ly/a/t1/h;

    move-result-object v1

    return-object v1

    :cond_4
    cmp-long v1, v4, v10

    if-lez v1, :cond_5

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    :cond_5
    add-int/lit8 v7, v7, 0x1

    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v8, v3

    if-eqz v1, :cond_7

    goto :goto_3

    :cond_7
    move-wide v8, v10

    :goto_3
    iput-wide v8, v0, Ly/a/t1/a$a;->g:J

    const/4 v1, 0x0

    return-object v1
.end method

.method public run()V
    .locals 15

    sget-object v0, Ly/a/t1/a$b;->f:Ly/a/t1/a$b;

    sget-object v1, Ly/a/t1/a$b;->h:Ly/a/t1/a$b;

    :cond_0
    :goto_0
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :cond_1
    :goto_1
    iget-object v5, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v5}, Ly/a/t1/a;->isTerminated()Z

    move-result v5

    if-nez v5, :cond_14

    iget-object v5, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    if-eq v5, v1, :cond_14

    iget-boolean v5, p0, Ly/a/t1/a$a;->i:Z

    invoke-virtual {p0, v5}, Ly/a/t1/a$a;->a(Z)Ly/a/t1/h;

    move-result-object v5

    const-wide/16 v6, 0x0

    if-eqz v5, :cond_6

    iput-wide v6, p0, Ly/a/t1/a$a;->g:J

    sget-object v2, Ly/a/t1/a$b;->e:Ly/a/t1/a$b;

    iget-object v3, v5, Ly/a/t1/h;->e:Ly/a/t1/i;

    invoke-interface {v3}, Ly/a/t1/i;->g()I

    move-result v3

    iput-wide v6, p0, Ly/a/t1/a$a;->f:J

    iget-object v4, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    if-ne v4, v0, :cond_2

    iput-object v2, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    :cond_2
    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v2}, Ly/a/t1/a$a;->e(Ly/a/t1/a$b;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v2}, Ly/a/t1/a;->n()V

    :cond_4
    :goto_2
    iget-object v2, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v2, v5}, Ly/a/t1/a;->m(Ly/a/t1/h;)V

    if-nez v3, :cond_5

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    sget-object v3, Ly/a/t1/a;->l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    const-wide/32 v4, -0x200000

    invoke-virtual {v3, v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->addAndGet(Ljava/lang/Object;J)J

    iget-object v2, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    if-eq v2, v1, :cond_0

    sget-object v2, Ly/a/t1/a$b;->g:Ly/a/t1/a$b;

    iput-object v2, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    goto :goto_0

    :cond_6
    iput-boolean v3, p0, Ly/a/t1/a$a;->i:Z

    iget-wide v8, p0, Ly/a/t1/a$a;->g:J

    cmp-long v5, v8, v6

    if-eqz v5, :cond_8

    if-nez v4, :cond_7

    const/4 v4, 0x1

    goto :goto_1

    :cond_7
    invoke-virtual {p0, v0}, Ly/a/t1/a$a;->e(Ly/a/t1/a$b;)Z

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    iget-wide v2, p0, Ly/a/t1/a$a;->g:J

    invoke-static {v2, v3}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(J)V

    iput-wide v6, p0, Ly/a/t1/a$a;->g:J

    goto :goto_0

    :cond_8
    iget-object v5, p0, Ly/a/t1/a$a;->nextParkedWorker:Ljava/lang/Object;

    sget-object v8, Ly/a/t1/a;->n:Ly/a/s1/n;

    if-eq v5, v8, :cond_9

    const/4 v5, 0x1

    goto :goto_3

    :cond_9
    const/4 v5, 0x0

    :goto_3
    if-nez v5, :cond_a

    iget-object v5, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v5, p0}, Ly/a/t1/a;->g(Ly/a/t1/a$a;)Z

    goto :goto_1

    :cond_a
    const/4 v5, -0x1

    iput v5, p0, Ly/a/t1/a$a;->workerCtl:I

    :cond_b
    :goto_4
    iget-object v8, p0, Ly/a/t1/a$a;->nextParkedWorker:Ljava/lang/Object;

    sget-object v9, Ly/a/t1/a;->n:Ly/a/s1/n;

    if-eq v8, v9, :cond_c

    const/4 v8, 0x1

    goto :goto_5

    :cond_c
    const/4 v8, 0x0

    :goto_5
    if-eqz v8, :cond_1

    iget-object v8, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v8}, Ly/a/t1/a;->isTerminated()Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    if-ne v8, v1, :cond_d

    goto/16 :goto_1

    :cond_d
    invoke-virtual {p0, v0}, Ly/a/t1/a$a;->e(Ly/a/t1/a$b;)Z

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    iget-wide v8, p0, Ly/a/t1/a$a;->f:J

    cmp-long v10, v8, v6

    if-nez v10, :cond_e

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    iget-object v10, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-wide v10, v10, Ly/a/t1/a;->i:J

    add-long/2addr v8, v10

    iput-wide v8, p0, Ly/a/t1/a$a;->f:J

    :cond_e
    iget-object v8, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-wide v8, v8, Ly/a/t1/a;->i:J

    invoke-static {v8, v9}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(J)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    iget-wide v10, p0, Ly/a/t1/a$a;->f:J

    sub-long/2addr v8, v10

    cmp-long v10, v8, v6

    if-ltz v10, :cond_b

    iput-wide v6, p0, Ly/a/t1/a$a;->f:J

    iget-object v8, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v8, v8, Ly/a/t1/a;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    monitor-enter v8

    :try_start_0
    iget-object v9, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v9}, Ly/a/t1/a;->isTerminated()Z

    move-result v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v9, :cond_f

    monitor-exit v8

    goto :goto_4

    :cond_f
    :try_start_1
    iget-object v9, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-wide v9, v9, Ly/a/t1/a;->controlState:J

    const-wide/32 v11, 0x1fffff

    and-long/2addr v9, v11

    long-to-int v10, v9

    iget-object v9, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget v9, v9, Ly/a/t1/a;->g:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v10, v9, :cond_10

    monitor-exit v8

    goto :goto_4

    :cond_10
    :try_start_2
    sget-object v9, Ly/a/t1/a$a;->k:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v9, p0, v5, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v9, :cond_11

    monitor-exit v8

    goto :goto_4

    :cond_11
    :try_start_3
    iget v9, p0, Ly/a/t1/a$a;->indexInArray:I

    invoke-virtual {p0, v3}, Ly/a/t1/a$a;->d(I)V

    iget-object v10, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v10, p0, v9, v3}, Ly/a/t1/a;->i(Ly/a/t1/a$a;II)V

    iget-object v10, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    sget-object v13, Ly/a/t1/a;->l:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v13, v10}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndDecrement(Ljava/lang/Object;)J

    move-result-wide v13

    and-long v10, v13, v11

    long-to-int v11, v10

    const/4 v10, 0x0

    if-eq v11, v9, :cond_13

    iget-object v12, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v12, v12, Ly/a/t1/a;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v12, v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    if-eqz v12, :cond_12

    check-cast v12, Ly/a/t1/a$a;

    iget-object v13, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v13, v13, Ly/a/t1/a;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v13, v9, v12}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    invoke-virtual {v12, v9}, Ly/a/t1/a$a;->d(I)V

    iget-object v13, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    invoke-virtual {v13, v12, v11, v9}, Ly/a/t1/a;->i(Ly/a/t1/a$a;II)V

    goto :goto_6

    :cond_12
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v10

    :cond_13
    :goto_6
    :try_start_4
    iget-object v9, p0, Ly/a/t1/a$a;->j:Ly/a/t1/a;

    iget-object v9, v9, Ly/a/t1/a;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v9, v11, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v8

    iput-object v1, p0, Ly/a/t1/a$a;->e:Ly/a/t1/a$b;

    goto/16 :goto_4

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    :cond_14
    invoke-virtual {p0, v1}, Ly/a/t1/a$a;->e(Ly/a/t1/a$b;)Z

    return-void
.end method
