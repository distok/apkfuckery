.class public Ly/a/t1/c;
.super Ly/a/q0;
.source "Dispatcher.kt"


# instance fields
.field public d:Ly/a/t1/a;

.field public final e:I

.field public final f:I

.field public final g:J

.field public final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;I)V
    .locals 6

    and-int/lit8 p3, p4, 0x1

    if-eqz p3, :cond_0

    sget p1, Ly/a/t1/k;->b:I

    :cond_0
    move v1, p1

    and-int/lit8 p1, p4, 0x2

    if-eqz p1, :cond_1

    sget p2, Ly/a/t1/k;->c:I

    :cond_1
    move v2, p2

    and-int/lit8 p1, p4, 0x4

    if-eqz p1, :cond_2

    const-string p1, "DefaultDispatcher"

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    move-object v5, p1

    sget-wide v3, Ly/a/t1/k;->d:J

    invoke-direct {p0}, Ly/a/q0;-><init>()V

    iput v1, p0, Ly/a/t1/c;->e:I

    iput v2, p0, Ly/a/t1/c;->f:I

    iput-wide v3, p0, Ly/a/t1/c;->g:J

    iput-object v5, p0, Ly/a/t1/c;->h:Ljava/lang/String;

    new-instance p1, Ly/a/t1/a;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Ly/a/t1/a;-><init>(IIJLjava/lang/String;)V

    iput-object p1, p0, Ly/a/t1/c;->d:Ly/a/t1/a;

    return-void
.end method


# virtual methods
.method public dispatch(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 3

    :try_start_0
    iget-object p1, p0, Ly/a/t1/c;->d:Ly/a/t1/a;

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-static {p1, p2, v0, v1, v2}, Ly/a/t1/a;->e(Ly/a/t1/a;Ljava/lang/Runnable;Ly/a/t1/i;ZI)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object p1, Ly/a/z;->j:Ly/a/z;

    invoke-virtual {p1, p2}, Ly/a/m0;->D(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public dispatchYield(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Ly/a/t1/c;->d:Ly/a/t1/a;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v0, p2, v1, v2, v3}, Ly/a/t1/a;->e(Ly/a/t1/a;Ljava/lang/Runnable;Ly/a/t1/i;ZI)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, Ly/a/z;->j:Ly/a/z;

    invoke-virtual {v0, p1, p2}, Ly/a/v;->dispatchYield(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
