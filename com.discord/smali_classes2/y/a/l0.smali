.class public abstract Ly/a/l0;
.super Ly/a/v;
.source "EventLoop.common.kt"


# instance fields
.field public d:J

.field public e:Z

.field public f:Ly/a/s1/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly/a/s1/a<",
            "Ly/a/f0<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ly/a/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final B()Z
    .locals 7

    iget-object v0, p0, Ly/a/l0;->f:Ly/a/s1/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget v2, v0, Ly/a/s1/a;->b:I

    iget v3, v0, Ly/a/s1/a;->c:I

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v3, v0, Ly/a/s1/a;->a:[Ljava/lang/Object;

    aget-object v6, v3, v2

    aput-object v4, v3, v2

    add-int/2addr v2, v5

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    iput v2, v0, Ly/a/s1/a;->b:I

    if-eqz v6, :cond_2

    move-object v4, v6

    :goto_0
    check-cast v4, Ly/a/f0;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ly/a/f0;->run()V

    return v5

    :cond_1
    return v1

    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type T"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return v1
.end method

.method public shutdown()V
    .locals 0

    return-void
.end method

.method public final t(Z)V
    .locals 4

    iget-wide v0, p0, Ly/a/l0;->d:J

    invoke-virtual {p0, p1}, Ly/a/l0;->v(Z)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Ly/a/l0;->d:J

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    return-void

    :cond_0
    iget-boolean p1, p0, Ly/a/l0;->e:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ly/a/l0;->shutdown()V

    :cond_1
    return-void
.end method

.method public final v(Z)J
    .locals 2

    if-eqz p1, :cond_0

    const-wide v0, 0x100000000L

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x1

    :goto_0
    return-wide v0
.end method

.method public final w(Ly/a/f0;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ly/a/f0<",
            "*>;)V"
        }
    .end annotation

    iget-object v0, p0, Ly/a/l0;->f:Ly/a/s1/a;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ly/a/s1/a;

    invoke-direct {v0}, Ly/a/s1/a;-><init>()V

    iput-object v0, p0, Ly/a/l0;->f:Ly/a/s1/a;

    :goto_0
    iget-object v1, v0, Ly/a/s1/a;->a:[Ljava/lang/Object;

    iget v2, v0, Ly/a/s1/a;->c:I

    aput-object p1, v1, v2

    add-int/lit8 v2, v2, 0x1

    array-length p1, v1

    add-int/lit8 p1, p1, -0x1

    and-int/2addr p1, v2

    iput p1, v0, Ly/a/s1/a;->c:I

    iget v4, v0, Ly/a/s1/a;->b:I

    if-ne p1, v4, :cond_1

    array-length p1, v1

    shl-int/lit8 v2, p1, 0x1

    new-array v11, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    move-object v2, v11

    invoke-static/range {v1 .. v6}, Lx/h/f;->copyInto$default([Ljava/lang/Object;[Ljava/lang/Object;IIII)[Ljava/lang/Object;

    iget-object v5, v0, Ly/a/s1/a;->a:[Ljava/lang/Object;

    array-length v1, v5

    iget v9, v0, Ly/a/s1/a;->b:I

    sub-int v7, v1, v9

    const/4 v8, 0x0

    const/4 v10, 0x4

    move-object v6, v11

    invoke-static/range {v5 .. v10}, Lx/h/f;->copyInto$default([Ljava/lang/Object;[Ljava/lang/Object;IIII)[Ljava/lang/Object;

    iput-object v11, v0, Ly/a/s1/a;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    iput v1, v0, Ly/a/s1/a;->b:I

    iput p1, v0, Ly/a/s1/a;->c:I

    :cond_1
    return-void
.end method

.method public final y(Z)V
    .locals 4

    iget-wide v0, p0, Ly/a/l0;->d:J

    invoke-virtual {p0, p1}, Ly/a/l0;->v(Z)J

    move-result-wide v2

    add-long/2addr v2, v0

    iput-wide v2, p0, Ly/a/l0;->d:J

    if-nez p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Ly/a/l0;->e:Z

    :cond_0
    return-void
.end method

.method public final z()Z
    .locals 6

    iget-wide v0, p0, Ly/a/l0;->d:J

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ly/a/l0;->v(Z)J

    move-result-wide v3

    cmp-long v5, v0, v3

    if-ltz v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method
