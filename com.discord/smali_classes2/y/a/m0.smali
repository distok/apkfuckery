.class public abstract Ly/a/m0;
.super Ly/a/n0;
.source "EventLoop.common.kt"

# interfaces
.implements Ly/a/b0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly/a/m0$b;,
        Ly/a/m0$a;,
        Ly/a/m0$c;
    }
.end annotation


# static fields
.field public static final g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

.field public static final h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field private volatile _delayed:Ljava/lang/Object;

.field public volatile _isCompleted:I

.field private volatile _queue:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-class v0, Ljava/lang/Object;

    const-class v1, Ly/a/m0;

    const-string v2, "_queue"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v2

    sput-object v2, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const-string v2, "_delayed"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Ly/a/m0;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ly/a/n0;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    iput-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Ly/a/m0;->_isCompleted:I

    return-void
.end method


# virtual methods
.method public final D(Ljava/lang/Runnable;)V
    .locals 1

    invoke-virtual {p0, p1}, Ly/a/m0;->E(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ly/a/n0;->C()Ljava/lang/Thread;

    move-result-object p1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p1, :cond_1

    invoke-static {p1}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    goto :goto_0

    :cond_0
    sget-object v0, Ly/a/z;->j:Ly/a/z;

    invoke-virtual {v0, p1}, Ly/a/m0;->D(Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final E(Ljava/lang/Runnable;)Z
    .locals 5

    :cond_0
    :goto_0
    iget-object v0, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    iget v1, p0, Ly/a/m0;->_isCompleted:I

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    return v2

    :cond_1
    const/4 v1, 0x1

    if-nez v0, :cond_2

    sget-object v0, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2, p1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_2
    instance-of v3, v0, Ly/a/s1/i;

    if-eqz v3, :cond_6

    move-object v3, v0

    check-cast v3, Ly/a/s1/i;

    invoke-virtual {v3, p1}, Ly/a/s1/i;->a(Ljava/lang/Object;)I

    move-result v4

    if-eqz v4, :cond_5

    if-eq v4, v1, :cond_4

    const/4 v0, 0x2

    if-eq v4, v0, :cond_3

    goto :goto_0

    :cond_3
    return v2

    :cond_4
    sget-object v1, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3}, Ly/a/s1/i;->e()Ly/a/s1/i;

    move-result-object v2

    invoke-virtual {v1, p0, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return v1

    :cond_6
    sget-object v3, Ly/a/o0;->b:Ly/a/s1/n;

    if-ne v0, v3, :cond_7

    return v2

    :cond_7
    new-instance v2, Ly/a/s1/i;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v1}, Ly/a/s1/i;-><init>(IZ)V

    move-object v3, v0

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Ly/a/s1/i;->a(Ljava/lang/Object;)I

    invoke-virtual {v2, p1}, Ly/a/s1/i;->a(Ljava/lang/Object;)I

    sget-object v3, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3, p0, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1
.end method

.method public F()Z
    .locals 4

    iget-object v0, p0, Ly/a/l0;->f:Ly/a/s1/a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget v3, v0, Ly/a/s1/a;->b:I

    iget v0, v0, Ly/a/s1/a;->c:I

    if-ne v3, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    check-cast v0, Ly/a/m0$c;

    if-eqz v0, :cond_4

    iget v0, v0, Ly/a/s1/q;->_size:I

    if-nez v0, :cond_3

    const/4 v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_4

    return v2

    :cond_4
    iget-object v0, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    instance-of v3, v0, Ly/a/s1/i;

    if-eqz v3, :cond_6

    check-cast v0, Ly/a/s1/i;

    invoke-virtual {v0}, Ly/a/s1/i;->d()Z

    move-result v1

    goto :goto_3

    :cond_6
    sget-object v3, Ly/a/o0;->b:Ly/a/s1/n;

    if-ne v0, v3, :cond_7

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    :goto_3
    return v1
.end method

.method public I()J
    .locals 13

    sget-object v0, Ly/a/o0;->b:Ly/a/s1/n;

    invoke-virtual {p0}, Ly/a/l0;->B()Z

    move-result v1

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_0

    return-wide v2

    :cond_0
    iget-object v1, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    check-cast v1, Ly/a/m0$c;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v1, :cond_6

    iget v7, v1, Ly/a/s1/q;->_size:I

    if-nez v7, :cond_1

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    :goto_0
    if-nez v7, :cond_6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v7

    :goto_1
    monitor-enter v1

    :try_start_0
    invoke-virtual {v1}, Ly/a/s1/q;->b()Ly/a/s1/r;

    move-result-object v9

    if-eqz v9, :cond_5

    check-cast v9, Ly/a/m0$b;

    iget-wide v10, v9, Ly/a/m0$b;->f:J

    sub-long v10, v7, v10

    cmp-long v12, v10, v2

    if-ltz v12, :cond_2

    const/4 v10, 0x1

    goto :goto_2

    :cond_2
    const/4 v10, 0x0

    :goto_2
    if-eqz v10, :cond_3

    invoke-virtual {p0, v9}, Ly/a/m0;->E(Ljava/lang/Runnable;)Z

    move-result v9

    goto :goto_3

    :cond_3
    const/4 v9, 0x0

    :goto_3
    if-eqz v9, :cond_4

    invoke-virtual {v1, v6}, Ly/a/s1/q;->c(I)Ly/a/s1/r;

    move-result-object v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :cond_4
    move-object v9, v4

    :goto_4
    monitor-exit v1

    goto :goto_5

    :cond_5
    monitor-exit v1

    move-object v9, v4

    :goto_5
    check-cast v9, Ly/a/m0$b;

    if-eqz v9, :cond_6

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_6
    :goto_6
    iget-object v1, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    if-nez v1, :cond_7

    goto :goto_7

    :cond_7
    instance-of v7, v1, Ly/a/s1/i;

    if-eqz v7, :cond_9

    move-object v7, v1

    check-cast v7, Ly/a/s1/i;

    invoke-virtual {v7}, Ly/a/s1/i;->f()Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Ly/a/s1/i;->g:Ly/a/s1/n;

    if-eq v8, v9, :cond_8

    move-object v4, v8

    check-cast v4, Ljava/lang/Runnable;

    goto :goto_7

    :cond_8
    sget-object v8, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v7}, Ly/a/s1/i;->e()Ly/a/s1/i;

    move-result-object v7

    invoke-virtual {v8, p0, v1, v7}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_6

    :cond_9
    if-ne v1, v0, :cond_a

    goto :goto_7

    :cond_a
    sget-object v7, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v7, p0, v1, v4}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    move-object v4, v1

    check-cast v4, Ljava/lang/Runnable;

    :goto_7
    if-eqz v4, :cond_b

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    return-wide v2

    :cond_b
    iget-object v1, p0, Ly/a/l0;->f:Ly/a/s1/a;

    const-wide v7, 0x7fffffffffffffffL

    if-eqz v1, :cond_e

    iget v4, v1, Ly/a/s1/a;->b:I

    iget v1, v1, Ly/a/s1/a;->c:I

    if-ne v4, v1, :cond_c

    goto :goto_8

    :cond_c
    const/4 v5, 0x0

    :goto_8
    if-eqz v5, :cond_d

    goto :goto_9

    :cond_d
    move-wide v4, v2

    goto :goto_a

    :cond_e
    :goto_9
    move-wide v4, v7

    :goto_a
    cmp-long v1, v4, v2

    if-nez v1, :cond_f

    goto :goto_c

    :cond_f
    iget-object v1, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    if-nez v1, :cond_10

    goto :goto_b

    :cond_10
    instance-of v4, v1, Ly/a/s1/i;

    if-eqz v4, :cond_13

    check-cast v1, Ly/a/s1/i;

    invoke-virtual {v1}, Ly/a/s1/i;->d()Z

    move-result v0

    if-nez v0, :cond_11

    goto :goto_c

    :cond_11
    :goto_b
    iget-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    check-cast v0, Ly/a/m0$c;

    if-eqz v0, :cond_14

    monitor-enter v0

    :try_start_1
    invoke-virtual {v0}, Ly/a/s1/q;->b()Ly/a/s1/r;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v0

    check-cast v1, Ly/a/m0$b;

    if-eqz v1, :cond_14

    iget-wide v0, v1, Ly/a/m0$b;->f:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-gez v4, :cond_12

    goto :goto_c

    :cond_12
    move-wide v2, v0

    goto :goto_c

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_13
    if-ne v1, v0, :cond_15

    :cond_14
    move-wide v2, v7

    :cond_15
    :goto_c
    return-wide v2
.end method

.method public final J()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    iput-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    return-void
.end method

.method public final K(JLy/a/m0$b;)V
    .locals 12

    iget v0, p0, Ly/a/m0;->_isCompleted:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    check-cast v0, Ly/a/m0$c;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Ly/a/m0;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    new-instance v5, Ly/a/m0$c;

    invoke-direct {v5, p1, p2}, Ly/a/m0$c;-><init>(J)V

    invoke-virtual {v0, p0, v1, v5}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    if-eqz v0, :cond_e

    check-cast v0, Ly/a/m0$c;

    :goto_0
    monitor-enter p3

    :try_start_0
    iget-object v5, p3, Ly/a/m0$b;->d:Ljava/lang/Object;

    sget-object v6, Ly/a/o0;->a:Ly/a/s1/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v5, v6, :cond_2

    monitor-exit p3

    const/4 v0, 0x2

    goto :goto_3

    :cond_2
    :try_start_1
    monitor-enter v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    invoke-virtual {v0}, Ly/a/s1/q;->b()Ly/a/s1/r;

    move-result-object v5

    check-cast v5, Ly/a/m0$b;

    iget v6, p0, Ly/a/m0;->_isCompleted:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v6, :cond_3

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    monitor-exit p3

    :goto_1
    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const-wide/16 v6, 0x0

    if-nez v5, :cond_4

    :try_start_4
    iput-wide p1, v0, Ly/a/m0$c;->b:J

    goto :goto_2

    :cond_4
    iget-wide v8, v5, Ly/a/m0$b;->f:J

    sub-long v10, v8, p1

    cmp-long v5, v10, v6

    if-ltz v5, :cond_5

    move-wide v8, p1

    :cond_5
    iget-wide v10, v0, Ly/a/m0$c;->b:J

    sub-long v10, v8, v10

    cmp-long v5, v10, v6

    if-lez v5, :cond_6

    iput-wide v8, v0, Ly/a/m0$c;->b:J

    :cond_6
    :goto_2
    iget-wide v8, p3, Ly/a/m0$b;->f:J

    iget-wide v10, v0, Ly/a/m0$c;->b:J

    sub-long/2addr v8, v10

    cmp-long v5, v8, v6

    if-gez v5, :cond_7

    iput-wide v10, p3, Ly/a/m0$b;->f:J

    :cond_7
    invoke-virtual {v0, p3}, Ly/a/s1/q;->a(Ly/a/s1/r;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    monitor-exit p3

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_a

    if-eq v0, v4, :cond_9

    if-ne v0, v2, :cond_8

    goto :goto_5

    :cond_8
    const-string p1, "unexpected result"

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_9
    sget-object v0, Ly/a/z;->j:Ly/a/z;

    invoke-virtual {v0, p1, p2, p3}, Ly/a/m0;->K(JLy/a/m0$b;)V

    goto :goto_5

    :cond_a
    iget-object p1, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    check-cast p1, Ly/a/m0$c;

    if-eqz p1, :cond_b

    monitor-enter p1

    :try_start_6
    invoke-virtual {p1}, Ly/a/s1/q;->b()Ly/a/s1/r;

    move-result-object p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    monitor-exit p1

    move-object v1, p2

    check-cast v1, Ly/a/m0$b;

    goto :goto_4

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_b
    :goto_4
    if-ne v1, p3, :cond_c

    const/4 v3, 0x1

    :cond_c
    if-eqz v3, :cond_d

    invoke-virtual {p0}, Ly/a/n0;->C()Ljava/lang/Thread;

    move-result-object p1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p2

    if-eq p2, p1, :cond_d

    invoke-static {p1}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    :cond_d
    :goto_5
    return-void

    :catchall_1
    move-exception p1

    :try_start_7
    monitor-exit v0

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p3

    throw p1

    :cond_e
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1
.end method

.method public b(JLy/a/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ly/a/f<",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    goto :goto_0

    :cond_0
    const-wide v0, 0x8637bd05af6L

    cmp-long v2, p1, v0

    if-ltz v2, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :cond_1
    const-wide/32 v0, 0xf4240

    mul-long v0, v0, p1

    :goto_0
    const-wide p1, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v2, v0, p1

    if-gez v2, :cond_2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide p1

    new-instance v2, Ly/a/m0$a;

    add-long/2addr v0, p1

    invoke-direct {v2, p0, v0, v1, p3}, Ly/a/m0$a;-><init>(Ly/a/m0;JLy/a/f;)V

    new-instance v0, Ly/a/j0;

    invoke-direct {v0, v2}, Ly/a/j0;-><init>(Ly/a/i0;)V

    check-cast p3, Ly/a/g;

    invoke-virtual {p3, v0}, Ly/a/g;->k(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p0, p1, p2, v2}, Ly/a/m0;->K(JLy/a/m0$b;)V

    :cond_2
    return-void
.end method

.method public final dispatch(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p2}, Ly/a/m0;->D(Ljava/lang/Runnable;)V

    return-void
.end method

.method public shutdown()V
    .locals 6

    sget-object v0, Ly/a/m1;->b:Ly/a/m1;

    sget-object v0, Ly/a/m1;->a:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput v0, p0, Ly/a/m0;->_isCompleted:I

    sget-object v2, Ly/a/o0;->b:Ly/a/s1/n;

    :cond_0
    iget-object v3, p0, Ly/a/m0;->_queue:Ljava/lang/Object;

    if-nez v3, :cond_1

    sget-object v3, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    instance-of v4, v3, Ly/a/s1/i;

    if-eqz v4, :cond_2

    check-cast v3, Ly/a/s1/i;

    invoke-virtual {v3}, Ly/a/s1/i;->b()Z

    goto :goto_0

    :cond_2
    if-ne v3, v2, :cond_3

    goto :goto_0

    :cond_3
    new-instance v4, Ly/a/s1/i;

    const/16 v5, 0x8

    invoke-direct {v4, v5, v0}, Ly/a/s1/i;-><init>(IZ)V

    move-object v5, v3

    check-cast v5, Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Ly/a/s1/i;->a(Ljava/lang/Object;)I

    sget-object v5, Ly/a/m0;->g:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v5, p0, v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    invoke-virtual {p0}, Ly/a/m0;->I()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_4

    goto :goto_0

    :cond_4
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    :goto_1
    iget-object v0, p0, Ly/a/m0;->_delayed:Ljava/lang/Object;

    check-cast v0, Ly/a/m0$c;

    if-eqz v0, :cond_6

    monitor-enter v0

    :try_start_0
    iget v4, v0, Ly/a/s1/q;->_size:I

    if-lez v4, :cond_5

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ly/a/s1/q;->c(I)Ly/a/s1/r;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :cond_5
    move-object v4, v1

    :goto_2
    monitor-exit v0

    check-cast v4, Ly/a/m0$b;

    if-eqz v4, :cond_6

    sget-object v0, Ly/a/z;->j:Ly/a/z;

    invoke-virtual {v0, v2, v3, v4}, Ly/a/m0;->K(JLy/a/m0$b;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_6
    return-void
.end method
