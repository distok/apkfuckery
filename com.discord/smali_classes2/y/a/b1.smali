.class public Ly/a/b1;
.super Ljava/lang/Object;
.source "JobSupport.kt"

# interfaces
.implements Lkotlinx/coroutines/Job;
.implements Ly/a/l;
.implements Ly/a/i1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly/a/b1$b;,
        Ly/a/b1$a;
    }
.end annotation


# static fields
.field public static final d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field public volatile _parentHandle:Ljava/lang/Object;

.field private volatile _state:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-class v0, Ly/a/b1;

    const-class v1, Ljava/lang/Object;

    const-string v2, "_state"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    sget-object p1, Ly/a/c1;->g:Ly/a/k0;

    goto :goto_0

    :cond_0
    sget-object p1, Ly/a/c1;->f:Ly/a/k0;

    :goto_0
    iput-object p1, p0, Ly/a/b1;->_state:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-object p1, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public A(Ljava/lang/Throwable;)V
    .locals 0

    throw p1
.end method

.method public final B(Lkotlinx/coroutines/Job;)V
    .locals 2

    sget-object v0, Ly/a/g1;->d:Ly/a/g1;

    if-nez p1, :cond_0

    iput-object v0, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    return-void

    :cond_0
    invoke-interface {p1}, Lkotlinx/coroutines/Job;->start()Z

    invoke-interface {p1, p0}, Lkotlinx/coroutines/Job;->q(Ly/a/l;)Ly/a/j;

    move-result-object p1

    iput-object p1, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ly/a/t0;

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ly/a/i0;->dispose()V

    iput-object v0, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public C()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final D(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    :goto_0
    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Ly/a/b1;->N(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ly/a/c1;->a:Ly/a/s1/n;

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Job "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " is already complete or completing, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "but is being completed with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    instance-of v2, p1, Ly/a/p;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    move-object p1, v3

    :cond_0
    check-cast p1, Ly/a/p;

    if-eqz p1, :cond_1

    iget-object v3, p1, Ly/a/p;->a:Ljava/lang/Throwable;

    :cond_1
    invoke-direct {v0, v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_2
    sget-object v1, Ly/a/c1;->c:Ly/a/s1/n;

    if-ne v0, v1, :cond_3

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public final E(Lkotlin/jvm/functions/Function1;Z)Ly/a/a1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;Z)",
            "Ly/a/a1<",
            "*>;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    instance-of p2, p1, Ly/a/y0;

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    check-cast v0, Ly/a/y0;

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    new-instance v0, Ly/a/w0;

    invoke-direct {v0, p0, p1}, Ly/a/w0;-><init>(Lkotlinx/coroutines/Job;Lkotlin/jvm/functions/Function1;)V

    goto :goto_2

    :cond_2
    instance-of p2, p1, Ly/a/a1;

    if-nez p2, :cond_3

    goto :goto_1

    :cond_3
    move-object v0, p1

    :goto_1
    check-cast v0, Ly/a/a1;

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    new-instance v0, Ly/a/x0;

    invoke-direct {v0, p0, p1}, Ly/a/x0;-><init>(Lkotlinx/coroutines/Job;Lkotlin/jvm/functions/Function1;)V

    :goto_2
    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final G(Ly/a/s1/g;)Ly/a/k;
    .locals 1

    :goto_0
    invoke-virtual {p1}, Ly/a/s1/g;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ly/a/s1/g;->f()Ly/a/s1/g;

    move-result-object p1

    goto :goto_0

    :cond_0
    :goto_1
    invoke-virtual {p1}, Ly/a/s1/g;->e()Ly/a/s1/g;

    move-result-object p1

    invoke-virtual {p1}, Ly/a/s1/g;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    instance-of v0, p1, Ly/a/k;

    if-eqz v0, :cond_2

    check-cast p1, Ly/a/k;

    return-object p1

    :cond_2
    instance-of v0, p1, Ly/a/f1;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1
.end method

.method public final H(Ly/a/f1;Ljava/lang/Throwable;)V
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    check-cast v1, Ly/a/s1/g;

    :goto_0
    invoke-static {v1, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    instance-of v2, v1, Ly/a/y0;

    if-eqz v2, :cond_1

    move-object v2, v1

    check-cast v2, Ly/a/a1;

    :try_start_0
    invoke-virtual {v2, p2}, Ly/a/s;->j(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    if-eqz v0, :cond_0

    invoke-static {v0, v3}, Lf/h/a/f/f/n/g;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_0
    new-instance v0, Lkotlinx/coroutines/CompletionHandlerException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in completion handler "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v3}, Lkotlinx/coroutines/CompletionHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    invoke-virtual {v1}, Ly/a/s1/g;->e()Ly/a/s1/g;

    move-result-object v1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Ly/a/b1;->A(Ljava/lang/Throwable;)V

    :cond_3
    invoke-virtual {p0, p2}, Ly/a/b1;->k(Ljava/lang/Throwable;)Z

    return-void

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public I(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public J()V
    .locals 0

    return-void
.end method

.method public final K(Ly/a/a1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ly/a/a1<",
            "*>;)V"
        }
    .end annotation

    new-instance v0, Ly/a/f1;

    invoke-direct {v0}, Ly/a/f1;-><init>()V

    sget-object v1, Ly/a/s1/g;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->lazySet(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v1, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->lazySet(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p1, :cond_1

    goto :goto_0

    :cond_1
    sget-object v1, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p1, p1, v0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Ly/a/s1/g;->c(Ly/a/s1/g;)V

    :goto_0
    invoke-virtual {p1}, Ly/a/s1/g;->e()Ly/a/s1/g;

    move-result-object v0

    sget-object v1, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p0, p1, v0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method public final L(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    instance-of v0, p1, Ly/a/b1$b;

    const-string v1, "Active"

    if-eqz v0, :cond_1

    check-cast p1, Ly/a/b1$b;

    invoke-virtual {p1}, Ly/a/b1$b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "Cancelling"

    goto :goto_0

    :cond_0
    iget p1, p1, Ly/a/b1$b;->_isCompleting:I

    if-eqz p1, :cond_5

    const-string v1, "Completing"

    goto :goto_0

    :cond_1
    instance-of v0, p1, Ly/a/t0;

    if-eqz v0, :cond_3

    check-cast p1, Ly/a/t0;

    invoke-interface {p1}, Ly/a/t0;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const-string v1, "New"

    goto :goto_0

    :cond_3
    instance-of p1, p1, Ly/a/p;

    if-eqz p1, :cond_4

    const-string v1, "Cancelled"

    goto :goto_0

    :cond_4
    const-string v1, "Completed"

    :cond_5
    :goto_0
    return-object v1
.end method

.method public final M(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;
    .locals 1

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    check-cast v0, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    new-instance v0, Lkotlinx/coroutines/JobCancellationException;

    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Ly/a/b1;->l()Ljava/lang/String;

    move-result-object p2

    :goto_1
    invoke-direct {v0, p2, p1, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V

    :goto_2
    return-object v0
.end method

.method public final N(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    sget-object v0, Ly/a/c1;->c:Ly/a/s1/n;

    sget-object v1, Ly/a/c1;->a:Ly/a/s1/n;

    instance-of v2, p1, Ly/a/t0;

    if-nez v2, :cond_0

    return-object v1

    :cond_0
    instance-of v2, p1, Ly/a/k0;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_1

    instance-of v2, p1, Ly/a/a1;

    if-eqz v2, :cond_5

    :cond_1
    instance-of v2, p1, Ly/a/k;

    if-nez v2, :cond_5

    instance-of v2, p2, Ly/a/p;

    if-nez v2, :cond_5

    check-cast p1, Ly/a/t0;

    sget-object v1, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    instance-of v2, p2, Ly/a/t0;

    if-eqz v2, :cond_2

    new-instance v2, Ly/a/u0;

    move-object v5, p2

    check-cast v5, Ly/a/t0;

    invoke-direct {v2, v5}, Ly/a/u0;-><init>(Ly/a/t0;)V

    goto :goto_0

    :cond_2
    move-object v2, p2

    :goto_0
    invoke-virtual {v1, p0, p1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p2}, Ly/a/b1;->I(Ljava/lang/Object;)V

    invoke-virtual {p0, p1, p2}, Ly/a/b1;->r(Ly/a/t0;Ljava/lang/Object;)V

    :goto_1
    if-eqz v3, :cond_4

    return-object p2

    :cond_4
    return-object v0

    :cond_5
    check-cast p1, Ly/a/t0;

    invoke-virtual {p0, p1}, Ly/a/b1;->x(Ly/a/t0;)Ly/a/f1;

    move-result-object v2

    if-eqz v2, :cond_12

    instance-of v5, p1, Ly/a/b1$b;

    const/4 v6, 0x0

    if-nez v5, :cond_6

    move-object v5, v6

    goto :goto_2

    :cond_6
    move-object v5, p1

    :goto_2
    check-cast v5, Ly/a/b1$b;

    if-eqz v5, :cond_7

    goto :goto_3

    :cond_7
    new-instance v5, Ly/a/b1$b;

    invoke-direct {v5, v2, v4, v6}, Ly/a/b1$b;-><init>(Ly/a/f1;ZLjava/lang/Throwable;)V

    :goto_3
    monitor-enter v5

    :try_start_0
    iget v4, v5, Ly/a/b1$b;->_isCompleting:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_8

    monitor-exit v5

    move-object v0, v1

    goto/16 :goto_8

    :cond_8
    :try_start_1
    iput v3, v5, Ly/a/b1$b;->_isCompleting:I

    if-eq v5, p1, :cond_9

    sget-object v1, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p0, p1, v5}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_9

    monitor-exit v5

    goto :goto_8

    :cond_9
    :try_start_2
    invoke-virtual {v5}, Ly/a/b1$b;->d()Z

    move-result v0

    instance-of v1, p2, Ly/a/p;

    if-nez v1, :cond_a

    move-object v1, v6

    goto :goto_4

    :cond_a
    move-object v1, p2

    :goto_4
    check-cast v1, Ly/a/p;

    if-eqz v1, :cond_b

    iget-object v1, v1, Ly/a/p;->a:Ljava/lang/Throwable;

    invoke-virtual {v5, v1}, Ly/a/b1$b;->b(Ljava/lang/Throwable;)V

    :cond_b
    iget-object v1, v5, Ly/a/b1$b;->_rootCause:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Throwable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_c

    goto :goto_5

    :cond_c
    move-object v1, v6

    :goto_5
    monitor-exit v5

    if-eqz v1, :cond_d

    invoke-virtual {p0, v2, v1}, Ly/a/b1;->H(Ly/a/f1;Ljava/lang/Throwable;)V

    :cond_d
    instance-of v0, p1, Ly/a/k;

    if-nez v0, :cond_e

    move-object v0, v6

    goto :goto_6

    :cond_e
    move-object v0, p1

    :goto_6
    check-cast v0, Ly/a/k;

    if-eqz v0, :cond_f

    move-object v6, v0

    goto :goto_7

    :cond_f
    invoke-interface {p1}, Ly/a/t0;->getList()Ly/a/f1;

    move-result-object p1

    if-eqz p1, :cond_10

    invoke-virtual {p0, p1}, Ly/a/b1;->G(Ly/a/s1/g;)Ly/a/k;

    move-result-object v6

    :cond_10
    :goto_7
    if-eqz v6, :cond_11

    invoke-virtual {p0, v5, v6, p2}, Ly/a/b1;->O(Ly/a/b1$b;Ly/a/k;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_11

    sget-object v0, Ly/a/c1;->b:Ly/a/s1/n;

    goto :goto_8

    :cond_11
    invoke-virtual {p0, v5, p2}, Ly/a/b1;->t(Ly/a/b1$b;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_8

    :catchall_0
    move-exception p1

    monitor-exit v5

    throw p1

    :cond_12
    :goto_8
    return-object v0
.end method

.method public final O(Ly/a/b1$b;Ly/a/k;Ljava/lang/Object;)Z
    .locals 6

    :goto_0
    iget-object v0, p2, Ly/a/k;->h:Ly/a/l;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Ly/a/b1$a;

    invoke-direct {v3, p0, p1, p2, p3}, Ly/a/b1$a;-><init>(Ly/a/b1;Ly/a/b1$b;Ly/a/k;Ljava/lang/Object;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Ly/a/g0;->r(Lkotlinx/coroutines/Job;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ly/a/i0;

    move-result-object v0

    sget-object v1, Ly/a/g1;->d:Ly/a/g1;

    if-eq v0, v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p0, p2}, Ly/a/b1;->G(Ly/a/s1/g;)Ly/a/k;

    move-result-object p2

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public a()Z
    .locals 2

    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ly/a/t0;

    if-eqz v1, :cond_0

    check-cast v0, Ly/a/t0;

    invoke-interface {v0}, Ly/a/t0;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final d(ZZLkotlin/jvm/functions/Function1;)Ly/a/i0;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;)",
            "Ly/a/i0;"
        }
    .end annotation

    sget-object v0, Ly/a/g1;->d:Ly/a/g1;

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v3

    instance-of v4, v3, Ly/a/k0;

    if-eqz v4, :cond_4

    move-object v4, v3

    check-cast v4, Ly/a/k0;

    iget-boolean v5, v4, Ly/a/k0;->d:Z

    if-eqz v5, :cond_2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p3, p1}, Ly/a/b1;->E(Lkotlin/jvm/functions/Function1;Z)Ly/a/a1;

    move-result-object v2

    :goto_1
    sget-object v4, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v4, p0, v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_2
    new-instance v3, Ly/a/f1;

    invoke-direct {v3}, Ly/a/f1;-><init>()V

    iget-boolean v5, v4, Ly/a/k0;->d:Z

    if-eqz v5, :cond_3

    goto :goto_2

    :cond_3
    new-instance v5, Ly/a/s0;

    invoke-direct {v5, v3}, Ly/a/s0;-><init>(Ly/a/f1;)V

    move-object v3, v5

    :goto_2
    sget-object v5, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v5, p0, v4, v3}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    instance-of v4, v3, Ly/a/t0;

    if-eqz v4, :cond_10

    move-object v4, v3

    check-cast v4, Ly/a/t0;

    invoke-interface {v4}, Ly/a/t0;->getList()Ly/a/f1;

    move-result-object v4

    if-nez v4, :cond_6

    if-eqz v3, :cond_5

    check-cast v3, Ly/a/a1;

    invoke-virtual {p0, v3}, Ly/a/b1;->K(Ly/a/a1;)V

    goto :goto_0

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    if-eqz p1, :cond_c

    instance-of v5, v3, Ly/a/b1$b;

    if-eqz v5, :cond_c

    monitor-enter v3

    :try_start_0
    move-object v5, v3

    check-cast v5, Ly/a/b1$b;

    iget-object v5, v5, Ly/a/b1$b;->_rootCause:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Throwable;

    if-eqz v5, :cond_8

    instance-of v6, p3, Ly/a/k;

    if-eqz v6, :cond_7

    move-object v6, v3

    check-cast v6, Ly/a/b1$b;

    iget v6, v6, Ly/a/b1$b;->_isCompleting:I

    if-nez v6, :cond_7

    goto :goto_3

    :cond_7
    move-object v6, v0

    goto :goto_5

    :cond_8
    :goto_3
    if-eqz v2, :cond_9

    goto :goto_4

    :cond_9
    invoke-virtual {p0, p3, p1}, Ly/a/b1;->E(Lkotlin/jvm/functions/Function1;Z)Ly/a/a1;

    move-result-object v2

    :goto_4
    invoke-virtual {p0, v3, v4, v2}, Ly/a/b1;->g(Ljava/lang/Object;Ly/a/f1;Ly/a/a1;)Z

    move-result v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v6, :cond_a

    monitor-exit v3

    goto :goto_0

    :cond_a
    if-nez v5, :cond_b

    monitor-exit v3

    return-object v2

    :cond_b
    move-object v6, v2

    :goto_5
    monitor-exit v3

    goto :goto_6

    :catchall_0
    move-exception p1

    monitor-exit v3

    throw p1

    :cond_c
    move-object v6, v0

    move-object v5, v1

    :goto_6
    if-eqz v5, :cond_e

    if-eqz p2, :cond_d

    invoke-interface {p3, v5}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    return-object v6

    :cond_e
    if-eqz v2, :cond_f

    goto :goto_7

    :cond_f
    invoke-virtual {p0, p3, p1}, Ly/a/b1;->E(Lkotlin/jvm/functions/Function1;Z)Ly/a/a1;

    move-result-object v2

    :goto_7
    invoke-virtual {p0, v3, v4, v2}, Ly/a/b1;->g(Ljava/lang/Object;Ly/a/f1;Ly/a/a1;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_10
    if-eqz p2, :cond_13

    instance-of p1, v3, Ly/a/p;

    if-nez p1, :cond_11

    move-object v3, v1

    :cond_11
    check-cast v3, Ly/a/p;

    if-eqz v3, :cond_12

    iget-object v1, v3, Ly/a/p;->a:Ljava/lang/Throwable;

    :cond_12
    invoke-interface {p3, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_13
    return-object v0
.end method

.method public final e()Ljava/util/concurrent/CancellationException;
    .locals 4

    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ly/a/b1$b;

    const-string v2, "Job is still new or active: "

    if-eqz v1, :cond_1

    check-cast v0, Ly/a/b1$b;

    iget-object v0, v0, Ly/a/b1$b;->_rootCause:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is cancelling"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ly/a/b1;->M(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    instance-of v1, v0, Ly/a/t0;

    if-nez v1, :cond_3

    instance-of v1, v0, Ly/a/p;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Ly/a/p;

    iget-object v0, v0, Ly/a/p;->a:Ljava/lang/Throwable;

    invoke-virtual {p0, v0, v2}, Ly/a/b1;->M(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lkotlinx/coroutines/JobCancellationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " has completed normally"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V

    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final f(Ly/a/i1;)V
    .locals 0

    invoke-virtual {p0, p1}, Ly/a/b1;->j(Ljava/lang/Object;)Z

    return-void
.end method

.method public fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lkotlin/jvm/functions/Function2<",
            "-TR;-",
            "Lkotlin/coroutines/CoroutineContext$a;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lkotlin/coroutines/CoroutineContext$a$a;->fold(Lkotlin/coroutines/CoroutineContext$a;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final g(Ljava/lang/Object;Ly/a/f1;Ly/a/a1;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ly/a/f1;",
            "Ly/a/a1<",
            "*>;)Z"
        }
    .end annotation

    new-instance v0, Ly/a/b1$c;

    invoke-direct {v0, p3, p3, p0, p1}, Ly/a/b1$c;-><init>(Ly/a/s1/g;Ly/a/s1/g;Ly/a/b1;Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p2}, Ly/a/s1/g;->f()Ly/a/s1/g;

    move-result-object p1

    sget-object v1, Ly/a/s1/g;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p3, p1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->lazySet(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v1, Ly/a/s1/g;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1, p3, p2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->lazySet(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object p2, v0, Ly/a/s1/g$a;->b:Ly/a/s1/g;

    invoke-virtual {v1, p1, p2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v0, p1}, Ly/a/s1/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x2

    :goto_1
    if-eq p1, v4, :cond_2

    if-eq p1, v3, :cond_3

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    :cond_3
    return v2
.end method

.method public get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lkotlin/coroutines/CoroutineContext$a;",
            ">(",
            "Lkotlin/coroutines/CoroutineContext$b<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Lkotlin/coroutines/CoroutineContext$a$a;->get(Lkotlin/coroutines/CoroutineContext$a;Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object p1

    return-object p1
.end method

.method public final getKey()Lkotlin/coroutines/CoroutineContext$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/coroutines/CoroutineContext$b<",
            "*>;"
        }
    .end annotation

    sget-object v0, Lkotlinx/coroutines/Job;->c:Lkotlinx/coroutines/Job$a;

    return-object v0
.end method

.method public h(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final j(Ljava/lang/Object;)Z
    .locals 13

    sget-object v0, Ly/a/c1;->d:Ly/a/s1/n;

    sget-object v1, Ly/a/c1;->b:Ly/a/s1/n;

    sget-object v2, Ly/a/c1;->c:Ly/a/s1/n;

    sget-object v3, Ly/a/c1;->a:Ly/a/s1/n;

    invoke-virtual {p0}, Ly/a/b1;->w()Z

    move-result v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v4, :cond_3

    :cond_0
    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v4

    instance-of v8, v4, Ly/a/t0;

    if-eqz v8, :cond_2

    instance-of v8, v4, Ly/a/b1$b;

    if-eqz v8, :cond_1

    move-object v8, v4

    check-cast v8, Ly/a/b1$b;

    iget v8, v8, Ly/a/b1$b;->_isCompleting:I

    if-eqz v8, :cond_1

    goto :goto_0

    :cond_1
    new-instance v8, Ly/a/p;

    invoke-virtual {p0, p1}, Ly/a/b1;->s(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v9

    invoke-direct {v8, v9, v6, v5}, Ly/a/p;-><init>(Ljava/lang/Throwable;ZI)V

    invoke-virtual {p0, v4, v8}, Ly/a/b1;->N(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eq v4, v2, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    move-object v4, v3

    :goto_1
    if-ne v4, v1, :cond_4

    return v7

    :cond_3
    move-object v4, v3

    :cond_4
    if-ne v4, v3, :cond_14

    const/4 v4, 0x0

    move-object v8, v4

    :cond_5
    :goto_2
    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v9

    instance-of v10, v9, Ly/a/b1$b;

    if-eqz v10, :cond_b

    monitor-enter v9

    :try_start_0
    move-object v2, v9

    check-cast v2, Ly/a/b1$b;

    invoke-virtual {v2}, Ly/a/b1$b;->e()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_6

    monitor-exit v9

    goto/16 :goto_8

    :cond_6
    :try_start_1
    move-object v2, v9

    check-cast v2, Ly/a/b1$b;

    invoke-virtual {v2}, Ly/a/b1$b;->d()Z

    move-result v2

    if-nez p1, :cond_7

    if-nez v2, :cond_9

    :cond_7
    if-eqz v8, :cond_8

    goto :goto_3

    :cond_8
    invoke-virtual {p0, p1}, Ly/a/b1;->s(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v8

    :goto_3
    move-object p1, v9

    check-cast p1, Ly/a/b1$b;

    invoke-virtual {p1, v8}, Ly/a/b1$b;->b(Ljava/lang/Throwable;)V

    :cond_9
    move-object p1, v9

    check-cast p1, Ly/a/b1$b;

    iget-object p1, p1, Ly/a/b1$b;->_rootCause:Ljava/lang/Object;

    check-cast p1, Ljava/lang/Throwable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    xor-int/2addr v2, v7

    if-eqz v2, :cond_a

    move-object v4, p1

    :cond_a
    monitor-exit v9

    if-eqz v4, :cond_f

    check-cast v9, Ly/a/b1$b;

    iget-object p1, v9, Ly/a/b1$b;->d:Ly/a/f1;

    invoke-virtual {p0, p1, v4}, Ly/a/b1;->H(Ly/a/f1;Ljava/lang/Throwable;)V

    goto :goto_7

    :catchall_0
    move-exception p1

    monitor-exit v9

    throw p1

    :cond_b
    instance-of v10, v9, Ly/a/t0;

    if-eqz v10, :cond_13

    if-eqz v8, :cond_c

    goto :goto_4

    :cond_c
    invoke-virtual {p0, p1}, Ly/a/b1;->s(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v8

    :goto_4
    move-object v10, v9

    check-cast v10, Ly/a/t0;

    invoke-interface {v10}, Ly/a/t0;->a()Z

    move-result v11

    if-eqz v11, :cond_10

    invoke-virtual {p0, v10}, Ly/a/b1;->x(Ly/a/t0;)Ly/a/f1;

    move-result-object v9

    if-eqz v9, :cond_e

    new-instance v11, Ly/a/b1$b;

    invoke-direct {v11, v9, v6, v8}, Ly/a/b1$b;-><init>(Ly/a/f1;ZLjava/lang/Throwable;)V

    sget-object v12, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v12, p0, v10, v11}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    goto :goto_5

    :cond_d
    invoke-virtual {p0, v9, v8}, Ly/a/b1;->H(Ly/a/f1;Ljava/lang/Throwable;)V

    const/4 v9, 0x1

    goto :goto_6

    :cond_e
    :goto_5
    const/4 v9, 0x0

    :goto_6
    if-eqz v9, :cond_5

    :cond_f
    :goto_7
    move-object v4, v3

    goto :goto_9

    :cond_10
    new-instance v10, Ly/a/p;

    invoke-direct {v10, v8, v6, v5}, Ly/a/p;-><init>(Ljava/lang/Throwable;ZI)V

    invoke-virtual {p0, v9, v10}, Ly/a/b1;->N(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eq v10, v3, :cond_12

    if-ne v10, v2, :cond_11

    goto/16 :goto_2

    :cond_11
    move-object v4, v10

    goto :goto_9

    :cond_12
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot happen in "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    :goto_8
    move-object v4, v0

    :cond_14
    :goto_9
    if-ne v4, v3, :cond_15

    :goto_a
    const/4 v6, 0x1

    goto :goto_b

    :cond_15
    if-ne v4, v1, :cond_16

    goto :goto_a

    :cond_16
    if-ne v4, v0, :cond_17

    goto :goto_b

    :cond_17
    invoke-virtual {p0, v4}, Ly/a/b1;->h(Ljava/lang/Object;)V

    goto :goto_a

    :goto_b
    return v6
.end method

.method public final k(Ljava/lang/Throwable;)Z
    .locals 4

    invoke-virtual {p0}, Ly/a/b1;->C()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    iget-object v2, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    check-cast v2, Ly/a/j;

    if-eqz v2, :cond_4

    sget-object v3, Ly/a/g1;->d:Ly/a/g1;

    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {v2, p1}, Ly/a/j;->h(Ljava/lang/Throwable;)Z

    move-result p1

    if-nez p1, :cond_3

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1

    :cond_4
    :goto_1
    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string v0, "Job was cancelled"

    return-object v0
.end method

.method public m()Ljava/util/concurrent/CancellationException;
    .locals 4

    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ly/a/b1$b;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ly/a/b1$b;

    iget-object v1, v1, Ly/a/b1$b;->_rootCause:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Throwable;

    goto :goto_0

    :cond_0
    instance-of v1, v0, Ly/a/p;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ly/a/p;

    iget-object v1, v1, Ly/a/p;->a:Ljava/lang/Throwable;

    goto :goto_0

    :cond_1
    instance-of v1, v0, Ly/a/t0;

    if-nez v1, :cond_4

    move-object v1, v2

    :goto_0
    instance-of v3, v1, Ljava/util/concurrent/CancellationException;

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    check-cast v2, Ljava/util/concurrent/CancellationException;

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    new-instance v2, Lkotlinx/coroutines/JobCancellationException;

    const-string v3, "Parent job is "

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Ly/a/b1;->L(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V

    :goto_2
    return-object v2

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot be cancelling child in this state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public minusKey(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/CoroutineContext$b<",
            "*>;)",
            "Lkotlin/coroutines/CoroutineContext;"
        }
    .end annotation

    invoke-static {p0, p1}, Lkotlin/coroutines/CoroutineContext$a$a;->minusKey(Lkotlin/coroutines/CoroutineContext$a;Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    return-object p1
.end method

.method public n(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    new-instance v0, Lkotlinx/coroutines/JobCancellationException;

    invoke-virtual {p0}, Ly/a/b1;->l()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V

    move-object p1, v0

    :goto_0
    invoke-virtual {p0, p1}, Ly/a/b1;->j(Ljava/lang/Object;)Z

    return-void
.end method

.method public p(Ljava/lang/Throwable;)Z
    .locals 2

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, p1}, Ly/a/b1;->j(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ly/a/b1;->v()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;
    .locals 0

    invoke-static {p0, p1}, Lkotlin/coroutines/CoroutineContext$a$a;->plus(Lkotlin/coroutines/CoroutineContext$a;Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    return-object p1
.end method

.method public final q(Ly/a/l;)Ly/a/j;
    .locals 6

    new-instance v3, Ly/a/k;

    invoke-direct {v3, p0, p1}, Ly/a/k;-><init>(Ly/a/b1;Ly/a/l;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ly/a/g0;->r(Lkotlinx/coroutines/Job;ZZLkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ly/a/i0;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Ly/a/j;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.ChildHandle"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final r(Ly/a/t0;Ljava/lang/Object;)V
    .locals 7

    iget-object v0, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    check-cast v0, Ly/a/j;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ly/a/i0;->dispose()V

    sget-object v0, Ly/a/g1;->d:Ly/a/g1;

    iput-object v0, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    :cond_0
    instance-of v0, p2, Ly/a/p;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    move-object p2, v1

    :cond_1
    check-cast p2, Ly/a/p;

    if-eqz p2, :cond_2

    iget-object p2, p2, Ly/a/p;->a:Ljava/lang/Throwable;

    goto :goto_0

    :cond_2
    move-object p2, v1

    :goto_0
    instance-of v0, p1, Ly/a/a1;

    const-string v2, " for "

    const-string v3, "Exception in completion handler "

    if-eqz v0, :cond_3

    :try_start_0
    move-object v0, p1

    check-cast v0, Ly/a/a1;

    invoke-virtual {v0, p2}, Ly/a/s;->j(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_3

    :catchall_0
    move-exception p2

    new-instance v0, Lkotlinx/coroutines/CompletionHandlerException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lkotlinx/coroutines/CompletionHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v0}, Ly/a/b1;->A(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    invoke-interface {p1}, Ly/a/t0;->getList()Ly/a/f1;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ly/a/s1/g;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    check-cast v0, Ly/a/s1/g;

    :goto_1
    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_6

    instance-of v4, v0, Ly/a/a1;

    if-eqz v4, :cond_5

    move-object v4, v0

    check-cast v4, Ly/a/a1;

    :try_start_1
    invoke-virtual {v4, p2}, Ly/a/s;->j(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v5

    if-eqz v1, :cond_4

    invoke-static {v1, v5}, Lf/h/a/f/f/n/g;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    new-instance v1, Lkotlinx/coroutines/CompletionHandlerException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4, v5}, Lkotlinx/coroutines/CompletionHandlerException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_5
    :goto_2
    invoke-virtual {v0}, Ly/a/s1/g;->e()Ly/a/s1/g;

    move-result-object v0

    goto :goto_1

    :cond_6
    if-eqz v1, :cond_8

    invoke-virtual {p0, v1}, Ly/a/b1;->A(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_3
    return-void
.end method

.method public final s(Ljava/lang/Object;)Ljava/lang/Throwable;
    .locals 2

    if-eqz p1, :cond_0

    instance-of v0, p1, Ljava/lang/Throwable;

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Throwable;

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    new-instance v0, Lkotlinx/coroutines/JobCancellationException;

    invoke-virtual {p0}, Ly/a/b1;->l()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V

    move-object p1, v0

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    check-cast p1, Ly/a/i1;

    invoke-interface {p1}, Ly/a/i1;->m()Ljava/util/concurrent/CancellationException;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.ParentJob"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final start()Z
    .locals 6

    :goto_0
    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ly/a/k0;

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Ly/a/k0;

    iget-boolean v1, v1, Ly/a/k0;->d:Z

    if-eqz v1, :cond_0

    goto :goto_2

    :cond_0
    sget-object v1, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    sget-object v5, Ly/a/c1;->g:Ly/a/k0;

    invoke-virtual {v1, p0, v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_3

    :cond_1
    invoke-virtual {p0}, Ly/a/b1;->J()V

    goto :goto_1

    :cond_2
    instance-of v1, v0, Ly/a/s0;

    if-eqz v1, :cond_4

    sget-object v1, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-object v5, v0

    check-cast v5, Ly/a/s0;

    iget-object v5, v5, Ly/a/s0;->d:Ly/a/f1;

    invoke-virtual {v1, p0, v0, v5}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Ly/a/b1;->J()V

    :goto_1
    const/4 v2, 0x1

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_6

    if-eq v2, v4, :cond_5

    goto :goto_0

    :cond_5
    return v4

    :cond_6
    return v3
.end method

.method public final t(Ly/a/b1$b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    instance-of v0, p2, Ly/a/p;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, p2

    :goto_0
    check-cast v0, Ly/a/p;

    if-eqz v0, :cond_1

    iget-object v1, v0, Ly/a/p;->a:Ljava/lang/Throwable;

    :cond_1
    monitor-enter p1

    :try_start_0
    invoke-virtual {p1}, Ly/a/b1$b;->d()Z

    invoke-virtual {p1, v1}, Ly/a/b1$b;->f(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ly/a/b1;->u(Ly/a/b1$b;Ljava/util/List;)Ljava/lang/Throwable;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-gt v4, v3, :cond_2

    goto :goto_2

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-instance v5, Ljava/util/IdentityHashMap;

    invoke-direct {v5, v4}, Ljava/util/IdentityHashMap;-><init>(I)V

    invoke-static {v5}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Throwable;

    if-eq v5, v2, :cond_3

    if-eq v5, v2, :cond_3

    instance-of v6, v5, Ljava/util/concurrent/CancellationException;

    if-nez v6, :cond_3

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v2, v5}, Lf/h/a/f/f/n/g;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_4
    :goto_2
    monitor-exit p1

    const/4 v0, 0x0

    if-nez v2, :cond_5

    goto :goto_3

    :cond_5
    if-ne v2, v1, :cond_6

    goto :goto_3

    :cond_6
    new-instance p2, Ly/a/p;

    const/4 v1, 0x2

    invoke-direct {p2, v2, v0, v1}, Ly/a/p;-><init>(Ljava/lang/Throwable;ZI)V

    :goto_3
    if-eqz v2, :cond_a

    invoke-virtual {p0, v2}, Ly/a/b1;->k(Ljava/lang/Throwable;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-virtual {p0, v2}, Ly/a/b1;->z(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_7

    goto :goto_4

    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    :cond_8
    :goto_4
    const/4 v1, 0x1

    :goto_5
    if-eqz v1, :cond_a

    if-eqz p2, :cond_9

    move-object v1, p2

    check-cast v1, Ly/a/p;

    sget-object v2, Ly/a/p;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v2, v1, v0, v3}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    goto :goto_6

    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    :goto_6
    invoke-virtual {p0, p2}, Ly/a/b1;->I(Ljava/lang/Object;)V

    sget-object v0, Ly/a/b1;->d:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    instance-of v1, p2, Ly/a/t0;

    if-eqz v1, :cond_b

    new-instance v1, Ly/a/u0;

    move-object v2, p2

    check-cast v2, Ly/a/t0;

    invoke-direct {v1, v2}, Ly/a/u0;-><init>(Ly/a/t0;)V

    goto :goto_7

    :cond_b
    move-object v1, p2

    :goto_7
    invoke-virtual {v0, p0, p1, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    invoke-virtual {p0, p1, p2}, Ly/a/b1;->r(Ly/a/t0;Ljava/lang/Object;)V

    return-object p2

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ly/a/b1;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ly/a/b1;->y()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Ly/a/b1;->L(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lf/h/a/f/f/n/g;->B(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(Ly/a/b1$b;Ljava/util/List;)Ljava/lang/Throwable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ly/a/b1$b;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ly/a/b1$b;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lkotlinx/coroutines/JobCancellationException;

    invoke-virtual {p0}, Ly/a/b1;->l()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V

    return-object p1

    :cond_0
    return-object v1

    :cond_1
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Throwable;

    instance-of v3, v3, Ljava/util/concurrent/CancellationException;

    xor-int/2addr v3, v2

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    :goto_0
    check-cast v0, Ljava/lang/Throwable;

    if-eqz v0, :cond_4

    return-object v0

    :cond_4
    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    instance-of v3, v0, Lkotlinx/coroutines/TimeoutCancellationException;

    if-eqz v3, :cond_8

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/Throwable;

    if-eq v4, v0, :cond_6

    instance-of v4, v4, Lkotlinx/coroutines/TimeoutCancellationException;

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_5

    move-object v1, v3

    :cond_7
    check-cast v1, Ljava/lang/Throwable;

    if-eqz v1, :cond_8

    return-object v1

    :cond_8
    return-object v0
.end method

.method public v()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public w()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final x(Ly/a/t0;)Ly/a/f1;
    .locals 2

    invoke-interface {p1}, Ly/a/t0;->getList()Ly/a/f1;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ly/a/k0;

    if-eqz v0, :cond_1

    new-instance v0, Ly/a/f1;

    invoke-direct {v0}, Ly/a/f1;-><init>()V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Ly/a/a1;

    if-eqz v0, :cond_2

    check-cast p1, Ly/a/a1;

    invoke-virtual {p0, p1}, Ly/a/b1;->K(Ly/a/a1;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State should have list: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final y()Ljava/lang/Object;
    .locals 2

    :goto_0
    iget-object v0, p0, Ly/a/b1;->_state:Ljava/lang/Object;

    instance-of v1, v0, Ly/a/s1/k;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    check-cast v0, Ly/a/s1/k;

    invoke-virtual {v0, p0}, Ly/a/s1/k;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public z(Ljava/lang/Throwable;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
