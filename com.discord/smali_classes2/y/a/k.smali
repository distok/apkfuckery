.class public final Ly/a/k;
.super Ly/a/y0;
.source "JobSupport.kt"

# interfaces
.implements Ly/a/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly/a/y0<",
        "Ly/a/b1;",
        ">;",
        "Ly/a/j;"
    }
.end annotation


# instance fields
.field public final h:Ly/a/l;


# direct methods
.method public constructor <init>(Ly/a/b1;Ly/a/l;)V
    .locals 0

    invoke-direct {p0, p1}, Ly/a/y0;-><init>(Lkotlinx/coroutines/Job;)V

    iput-object p2, p0, Ly/a/k;->h:Ly/a/l;

    return-void
.end method


# virtual methods
.method public h(Ljava/lang/Throwable;)Z
    .locals 1

    iget-object v0, p0, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    check-cast v0, Ly/a/b1;

    invoke-virtual {v0, p1}, Ly/a/b1;->p(Ljava/lang/Throwable;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Ly/a/k;->j(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public j(Ljava/lang/Throwable;)V
    .locals 1

    iget-object p1, p0, Ly/a/k;->h:Ly/a/l;

    iget-object v0, p0, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    check-cast v0, Ly/a/i1;

    invoke-interface {p1, v0}, Ly/a/l;->f(Ly/a/i1;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ChildHandle["

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ly/a/k;->h:Ly/a/l;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
