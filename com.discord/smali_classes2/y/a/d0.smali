.class public final Ly/a/d0;
.super Ljava/lang/Object;
.source "DispatchedContinuation.kt"


# static fields
.field public static final a:Ly/a/s1/n;

.field public static final b:Ly/a/s1/n;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ly/a/s1/n;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/d0;->a:Ly/a/s1/n;

    new-instance v0, Ly/a/s1/n;

    const-string v1, "REUSABLE_CLAIMED"

    invoke-direct {v0, v1}, Ly/a/s1/n;-><init>(Ljava/lang/String;)V

    sput-object v0, Ly/a/d0;->b:Ly/a/s1/n;

    return-void
.end method

.method public static final a(Lkotlin/coroutines/Continuation;Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/coroutines/Continuation<",
            "-TT;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    instance-of v0, p0, Ly/a/c0;

    if-eqz v0, :cond_4

    check-cast p0, Ly/a/c0;

    invoke-static {p1}, Lf/h/a/f/f/n/g;->d0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Ly/a/c0;->j:Ly/a/v;

    invoke-virtual {p0}, Ly/a/c0;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v2

    invoke-virtual {v1, v2}, Ly/a/v;->isDispatchNeeded(Lkotlin/coroutines/CoroutineContext;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iput-object v0, p0, Ly/a/c0;->g:Ljava/lang/Object;

    iput v2, p0, Ly/a/f0;->f:I

    iget-object p1, p0, Ly/a/c0;->j:Ly/a/v;

    invoke-virtual {p0}, Ly/a/c0;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    invoke-virtual {p1, v0, p0}, Ly/a/v;->dispatch(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    goto :goto_3

    :cond_0
    sget-object v1, Ly/a/m1;->b:Ly/a/m1;

    invoke-static {}, Ly/a/m1;->a()Ly/a/l0;

    move-result-object v1

    invoke-virtual {v1}, Ly/a/l0;->z()Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v0, p0, Ly/a/c0;->g:Ljava/lang/Object;

    iput v2, p0, Ly/a/f0;->f:I

    invoke-virtual {v1, p0}, Ly/a/l0;->w(Ly/a/f0;)V

    goto :goto_3

    :cond_1
    invoke-virtual {v1, v2}, Ly/a/l0;->y(Z)V

    :try_start_0
    invoke-virtual {p0}, Ly/a/c0;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    sget-object v3, Lkotlinx/coroutines/Job;->c:Lkotlinx/coroutines/Job$a;

    invoke-interface {v0, v3}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/Job;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lkotlinx/coroutines/Job;->a()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v0}, Lkotlinx/coroutines/Job;->e()Ljava/util/concurrent/CancellationException;

    move-result-object v0

    invoke-static {v0}, Lf/h/a/f/f/n/g;->createFailure(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Ly/a/c0;->resumeWith(Ljava/lang/Object;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p0}, Ly/a/c0;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    iget-object v3, p0, Ly/a/c0;->i:Ljava/lang/Object;

    invoke-static {v0, v3}, Ly/a/s1/p;->c(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v4, p0, Ly/a/c0;->k:Lkotlin/coroutines/Continuation;

    invoke-interface {v4, p1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v0, v3}, Ly/a/s1/p;->a(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception p1

    invoke-static {v0, v3}, Ly/a/s1/p;->a(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {v1}, Ly/a/l0;->B()Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez p1, :cond_3

    goto :goto_2

    :catchall_1
    move-exception p1

    const/4 v0, 0x0

    :try_start_3
    invoke-virtual {p0, p1, v0}, Ly/a/f0;->f(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_2
    invoke-virtual {v1, v2}, Ly/a/l0;->t(Z)V

    goto :goto_3

    :catchall_2
    move-exception p0

    invoke-virtual {v1, v2}, Ly/a/l0;->t(Z)V

    throw p0

    :cond_4
    invoke-interface {p0, p1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V

    :goto_3
    return-void
.end method
