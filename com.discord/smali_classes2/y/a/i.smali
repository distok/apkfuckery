.class public final Ly/a/i;
.super Ly/a/y0;
.source "JobSupport.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly/a/y0<",
        "Lkotlinx/coroutines/Job;",
        ">;"
    }
.end annotation


# instance fields
.field public final h:Ly/a/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly/a/g<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/Job;Ly/a/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/Job;",
            "Ly/a/g<",
            "*>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Ly/a/y0;-><init>(Lkotlinx/coroutines/Job;)V

    iput-object p2, p0, Ly/a/i;->h:Ly/a/g;

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Ly/a/i;->j(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method

.method public j(Ljava/lang/Throwable;)V
    .locals 4

    iget-object p1, p0, Ly/a/i;->h:Ly/a/g;

    iget-object v0, p0, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Lkotlinx/coroutines/Job;->e()Ljava/util/concurrent/CancellationException;

    move-result-object v0

    iget v1, p1, Ly/a/f0;->f:I

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p1, Ly/a/g;->h:Lkotlin/coroutines/Continuation;

    instance-of v3, v1, Ly/a/c0;

    if-nez v3, :cond_1

    const/4 v1, 0x0

    :cond_1
    check-cast v1, Ly/a/c0;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Ly/a/c0;->k(Ljava/lang/Throwable;)Z

    move-result v2

    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v0}, Ly/a/g;->c(Ljava/lang/Throwable;)Z

    invoke-virtual {p1}, Ly/a/g;->h()V

    :goto_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ChildContinuation["

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ly/a/i;->h:Ly/a/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
