.class public Ly/a/z0;
.super Ly/a/b1;
.source "JobSupport.kt"

# interfaces
.implements Ly/a/o;


# instance fields
.field public final e:Z


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/Job;)V
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ly/a/b1;-><init>(Z)V

    invoke-virtual {p0, p1}, Ly/a/b1;->B(Lkotlinx/coroutines/Job;)V

    iget-object p1, p0, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    check-cast p1, Ly/a/j;

    instance-of v1, p1, Ly/a/k;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object p1, v2

    :cond_0
    check-cast p1, Ly/a/k;

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    iget-object p1, p1, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    check-cast p1, Ly/a/b1;

    if-eqz p1, :cond_3

    :goto_0
    invoke-virtual {p1}, Ly/a/b1;->v()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    iget-object p1, p1, Ly/a/b1;->_parentHandle:Ljava/lang/Object;

    check-cast p1, Ly/a/j;

    instance-of v3, p1, Ly/a/k;

    if-nez v3, :cond_2

    move-object p1, v2

    :cond_2
    check-cast p1, Ly/a/k;

    if-eqz p1, :cond_3

    iget-object p1, p1, Ly/a/a1;->g:Lkotlinx/coroutines/Job;

    check-cast p1, Ly/a/b1;

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Ly/a/z0;->e:Z

    return-void
.end method


# virtual methods
.method public v()Z
    .locals 1

    iget-boolean v0, p0, Ly/a/z0;->e:Z

    return v0
.end method

.method public w()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
