.class public Ld0/b/a/c/b$a;
.super Ljava/io/InputStream;
.source "ObjectInputStreamInstantiator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld0/b/a/c/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field public static final i:[I

.field public static j:[B

.field public static k:[B


# instance fields
.field public d:I

.field public e:[B

.field public f:I

.field public g:[[B

.field public final h:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ld0/b/a/c/b$a;->i:[I

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, -0x5313

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    sput-object v0, Ld0/b/a/c/b$a;->j:[B

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0x73

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    const/16 v2, 0x71

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    const/high16 v2, 0x7e0000

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    sput-object v0, Ld0/b/a/c/b$a;->k:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/Error;

    const-string v2, "IOException: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x2
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ld0/b/a/c/b$a;->d:I

    iput v0, p0, Ld0/b/a/c/b$a;->f:I

    sget-object v1, Ld0/b/a/c/b$a;->j:[B

    iput-object v1, p0, Ld0/b/a/c/b$a;->e:[B

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v3, 0x73

    :try_start_0
    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    const/16 v3, 0x72

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/io/ObjectStreamClass;->lookup(Ljava/lang/Class;)Ljava/io/ObjectStreamClass;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/ObjectStreamClass;->getSerialVersionUID()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/io/DataOutputStream;->writeLong(J)V

    const/4 p1, 0x2

    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    const/16 v3, 0x78

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    const/16 v3, 0x70

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iput-object v1, p0, Ld0/b/a/c/b$a;->h:[B

    const/4 v2, 0x3

    new-array v2, v2, [[B

    sget-object v3, Ld0/b/a/c/b$a;->j:[B

    aput-object v3, v2, v0

    const/4 v0, 0x1

    aput-object v1, v2, v0

    sget-object v0, Ld0/b/a/c/b$a;->k:[B

    aput-object v0, v2, p1

    iput-object v2, p0, Ld0/b/a/c/b$a;->g:[[B

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/Error;

    const-string v1, "IOException: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const v0, 0x7fffffff

    return v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Ld0/b/a/c/b$a;->e:[B

    iget v1, p0, Ld0/b/a/c/b$a;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Ld0/b/a/c/b$a;->d:I

    aget-byte v1, v0, v1

    array-length v0, v0

    if-lt v2, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Ld0/b/a/c/b$a;->d:I

    sget-object v0, Ld0/b/a/c/b$a;->i:[I

    iget v2, p0, Ld0/b/a/c/b$a;->f:I

    aget v0, v0, v2

    iput v0, p0, Ld0/b/a/c/b$a;->f:I

    iget-object v2, p0, Ld0/b/a/c/b$a;->g:[[B

    aget-object v0, v2, v0

    iput-object v0, p0, Ld0/b/a/c/b$a;->e:[B

    :cond_0
    return v1
.end method

.method public read([BII)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Ld0/b/a/c/b$a;->e:[B

    array-length v0, v0

    iget v1, p0, Ld0/b/a/c/b$a;->d:I

    sub-int/2addr v0, v1

    move v1, p3

    :goto_0
    if-gt v0, v1, :cond_0

    iget-object v2, p0, Ld0/b/a/c/b$a;->e:[B

    iget v3, p0, Ld0/b/a/c/b$a;->d:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v0

    sub-int/2addr v1, v0

    const/4 v0, 0x0

    iput v0, p0, Ld0/b/a/c/b$a;->d:I

    sget-object v2, Ld0/b/a/c/b$a;->i:[I

    iget v3, p0, Ld0/b/a/c/b$a;->f:I

    aget v2, v2, v3

    iput v2, p0, Ld0/b/a/c/b$a;->f:I

    iget-object v3, p0, Ld0/b/a/c/b$a;->g:[[B

    aget-object v2, v3, v2

    iput-object v2, p0, Ld0/b/a/c/b$a;->e:[B

    array-length v2, v2

    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    if-lez v1, :cond_1

    iget-object v0, p0, Ld0/b/a/c/b$a;->e:[B

    iget v2, p0, Ld0/b/a/c/b$a;->d:I

    invoke-static {v0, v2, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Ld0/b/a/c/b$a;->d:I

    add-int/2addr p1, v1

    iput p1, p0, Ld0/b/a/c/b$a;->d:I

    :cond_1
    return p3
.end method
