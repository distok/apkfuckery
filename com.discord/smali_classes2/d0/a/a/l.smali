.class public final Ld0/a/a/l;
.super Ld0/a/a/k;
.source "FieldWriter.java"


# instance fields
.field public final b:Ld0/a/a/v;

.field public final c:I

.field public final d:I

.field public final e:I

.field public f:I

.field public g:I

.field public h:Ld0/a/a/a;

.field public i:Ld0/a/a/a;

.field public j:Ld0/a/a/a;

.field public k:Ld0/a/a/a;

.field public l:Ld0/a/a/b;


# direct methods
.method public constructor <init>(Ld0/a/a/v;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const/high16 v0, 0x70000

    invoke-direct {p0, v0}, Ld0/a/a/k;-><init>(I)V

    iput-object p1, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    iput p2, p0, Ld0/a/a/l;->c:I

    invoke-virtual {p1, p3}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Ld0/a/a/l;->d:I

    invoke-virtual {p1, p4}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Ld0/a/a/l;->e:I

    if-eqz p5, :cond_0

    invoke-virtual {p1, p5}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p2

    iput p2, p0, Ld0/a/a/l;->f:I

    :cond_0
    if-eqz p6, :cond_1

    invoke-virtual {p1, p6}, Ld0/a/a/v;->b(Ljava/lang/Object;)Ld0/a/a/u;

    move-result-object p1

    iget p1, p1, Ld0/a/a/u;->a:I

    iput p1, p0, Ld0/a/a/l;->g:I

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Z)Ld0/a/a/a;
    .locals 3

    new-instance v0, Ld0/a/a/c;

    invoke-direct {v0}, Ld0/a/a/c;-><init>()V

    iget-object v1, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    invoke-virtual {v1, p1}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    const/4 p1, 0x1

    if-eqz p2, :cond_0

    new-instance p2, Ld0/a/a/a;

    iget-object v1, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    iget-object v2, p0, Ld0/a/a/l;->h:Ld0/a/a/a;

    invoke-direct {p2, v1, p1, v0, v2}, Ld0/a/a/a;-><init>(Ld0/a/a/v;ZLd0/a/a/c;Ld0/a/a/a;)V

    iput-object p2, p0, Ld0/a/a/l;->h:Ld0/a/a/a;

    return-object p2

    :cond_0
    new-instance p2, Ld0/a/a/a;

    iget-object v1, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    iget-object v2, p0, Ld0/a/a/l;->i:Ld0/a/a/a;

    invoke-direct {p2, v1, p1, v0, v2}, Ld0/a/a/a;-><init>(Ld0/a/a/v;ZLd0/a/a/c;Ld0/a/a/a;)V

    iput-object p2, p0, Ld0/a/a/l;->i:Ld0/a/a/a;

    return-object p2
.end method

.method public b(ILd0/a/a/x;Ljava/lang/String;Z)Ld0/a/a/a;
    .locals 1

    new-instance v0, Ld0/a/a/c;

    invoke-direct {v0}, Ld0/a/a/c;-><init>()V

    invoke-static {p1, v0}, Ly/a/g0;->z(ILd0/a/a/c;)V

    invoke-static {p2, v0}, Ld0/a/a/x;->a(Ld0/a/a/x;Ld0/a/a/c;)V

    iget-object p1, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    invoke-virtual {p1, p3}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    const/4 p1, 0x1

    if-eqz p4, :cond_0

    new-instance p2, Ld0/a/a/a;

    iget-object p3, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    iget-object p4, p0, Ld0/a/a/l;->j:Ld0/a/a/a;

    invoke-direct {p2, p3, p1, v0, p4}, Ld0/a/a/a;-><init>(Ld0/a/a/v;ZLd0/a/a/c;Ld0/a/a/a;)V

    iput-object p2, p0, Ld0/a/a/l;->j:Ld0/a/a/a;

    return-object p2

    :cond_0
    new-instance p2, Ld0/a/a/a;

    iget-object p3, p0, Ld0/a/a/l;->b:Ld0/a/a/v;

    iget-object p4, p0, Ld0/a/a/l;->k:Ld0/a/a/a;

    invoke-direct {p2, p3, p1, v0, p4}, Ld0/a/a/a;-><init>(Ld0/a/a/v;ZLd0/a/a/c;Ld0/a/a/a;)V

    iput-object p2, p0, Ld0/a/a/l;->k:Ld0/a/a/a;

    return-object p2
.end method
