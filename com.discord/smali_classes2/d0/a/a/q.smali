.class public abstract Ld0/a/a/q;
.super Ljava/lang/Object;
.source "MethodVisitor.java"


# instance fields
.field public final a:I

.field public b:Ld0/a/a/q;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x60000

    if-eq p1, v0, :cond_1

    const/high16 v0, 0x50000

    if-eq p1, v0, :cond_1

    const/high16 v0, 0x40000

    if-eq p1, v0, :cond_1

    const/high16 v0, 0x70000

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    iput p1, p0, Ld0/a/a/q;->a:I

    const/4 p1, 0x0

    iput-object p1, p0, Ld0/a/a/q;->b:Ld0/a/a/q;

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;Z)Ld0/a/a/a;
.end method

.method public abstract b(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract c(II[Ljava/lang/Object;I[Ljava/lang/Object;)V
.end method

.method public abstract d(II)V
.end method

.method public abstract e(I)V
.end method

.method public abstract f(ILd0/a/a/x;Ljava/lang/String;Z)Ld0/a/a/a;
.end method

.method public abstract g(II)V
.end method

.method public abstract h(ILd0/a/a/p;)V
.end method

.method public abstract i(Ld0/a/a/p;)V
.end method

.method public abstract j(Ljava/lang/Object;)V
.end method

.method public abstract k(ILd0/a/a/p;)V
.end method

.method public abstract l(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ld0/a/a/p;Ld0/a/a/p;I)V
.end method

.method public abstract m(ILd0/a/a/x;[Ld0/a/a/p;[Ld0/a/a/p;[ILjava/lang/String;Z)Ld0/a/a/a;
.end method

.method public abstract n(II)V
.end method

.method public o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget v0, p0, Ld0/a/a/q;->a:I

    const/high16 v1, 0x50000

    if-lt v0, v1, :cond_1

    const/16 v0, 0xb9

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v6, 0x0

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, Ld0/a/a/q;->p(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_1
    iget-object v0, p0, Ld0/a/a/q;->b:Ld0/a/a/q;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1, p2, p3, p4}, Ld0/a/a/q;->o(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public abstract p(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public varargs abstract q(IILd0/a/a/p;[Ld0/a/a/p;)V
.end method

.method public abstract r(ILd0/a/a/x;Ljava/lang/String;Z)Ld0/a/a/a;
.end method

.method public abstract s(ILjava/lang/String;)V
.end method

.method public abstract t(II)V
.end method
