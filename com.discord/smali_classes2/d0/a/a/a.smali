.class public final Ld0/a/a/a;
.super Ljava/lang/Object;
.source "AnnotationWriter.java"


# instance fields
.field public final a:Ld0/a/a/v;

.field public final b:Z

.field public final c:Ld0/a/a/c;

.field public final d:I

.field public e:I

.field public final f:Ld0/a/a/a;

.field public g:Ld0/a/a/a;


# direct methods
.method public constructor <init>(Ld0/a/a/v;ZLd0/a/a/c;Ld0/a/a/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    iput-boolean p2, p0, Ld0/a/a/a;->b:Z

    iput-object p3, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget p1, p3, Ld0/a/a/c;->b:I

    if-nez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 p1, p1, -0x2

    :goto_0
    iput p1, p0, Ld0/a/a/a;->d:I

    iput-object p4, p0, Ld0/a/a/a;->f:Ld0/a/a/a;

    if-eqz p4, :cond_1

    iput-object p0, p4, Ld0/a/a/a;->g:Ld0/a/a/a;

    :cond_1
    return-void
.end method

.method public static b(Ljava/lang/String;[Ld0/a/a/a;I)I
    .locals 4

    mul-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, 0x7

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_1

    aget-object v3, p1, v2

    if-nez v3, :cond_0

    const/4 v3, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v3, p0}, Ld0/a/a/a;->a(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x8

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static d(I[Ld0/a/a/a;ILd0/a/a/c;)V
    .locals 6

    mul-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-ge v2, p2, :cond_1

    aget-object v4, p1, v2

    if-nez v4, :cond_0

    const/4 v3, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v4, v3}, Ld0/a/a/a;->a(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, -0x8

    :goto_1
    add-int/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p3, p0}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    invoke-virtual {p3, v0}, Ld0/a/a/c;->i(I)Ld0/a/a/c;

    invoke-virtual {p3, p2}, Ld0/a/a/c;->g(I)Ld0/a/a/c;

    const/4 p0, 0x0

    :goto_2
    if-ge p0, p2, :cond_4

    aget-object v0, p1, p0

    move-object v4, v3

    const/4 v2, 0x0

    :goto_3
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ld0/a/a/a;->g()V

    add-int/lit8 v2, v2, 0x1

    iget-object v4, v0, Ld0/a/a/a;->f:Ld0/a/a/a;

    move-object v5, v4

    move-object v4, v0

    move-object v0, v5

    goto :goto_3

    :cond_2
    invoke-virtual {p3, v2}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    :goto_4
    if-eqz v4, :cond_3

    iget-object v0, v4, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v2, v0, Ld0/a/a/c;->a:[B

    iget v0, v0, Ld0/a/a/c;->b:I

    invoke-virtual {p3, v2, v1, v0}, Ld0/a/a/c;->h([BII)Ld0/a/a/c;

    iget-object v4, v4, Ld0/a/a/a;->g:Ld0/a/a/a;

    goto :goto_4

    :cond_3
    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v0, p1}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    :cond_0
    const/16 p1, 0x8

    move-object v0, p0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, v0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget v1, v1, Ld0/a/a/c;->b:I

    add-int/2addr p1, v1

    iget-object v0, v0, Ld0/a/a/a;->f:Ld0/a/a/a;

    goto :goto_0

    :cond_1
    return p1
.end method

.method public c(ILd0/a/a/c;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v3, v2

    move-object v2, p0

    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ld0/a/a/a;->g()V

    iget-object v3, v2, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget v3, v3, Ld0/a/a/c;->b:I

    add-int/2addr v1, v3

    add-int/lit8 v4, v4, 0x1

    iget-object v3, v2, Ld0/a/a/a;->f:Ld0/a/a/a;

    move-object v5, v3

    move-object v3, v2

    move-object v2, v5

    goto :goto_0

    :cond_0
    invoke-virtual {p2, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    invoke-virtual {p2, v1}, Ld0/a/a/c;->i(I)Ld0/a/a/c;

    invoke-virtual {p2, v4}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    :goto_1
    if-eqz v3, :cond_1

    iget-object p1, v3, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v1, p1, Ld0/a/a/c;->a:[B

    iget p1, p1, Ld0/a/a/c;->b:I

    invoke-virtual {p2, v1, v0, p1}, Ld0/a/a/c;->h([BII)Ld0/a/a/c;

    iget-object v3, v3, Ld0/a/a/a;->g:Ld0/a/a/a;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 7

    iget v0, p0, Ld0/a/a/a;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ld0/a/a/a;->e:I

    iget-boolean v0, p0, Ld0/a/a/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v1, p1}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    :cond_0
    instance-of p1, p2, Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v0, 0x73

    iget-object v1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v1, p2}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, v0, p2}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    goto/16 :goto_8

    :cond_1
    instance-of p1, p2, Ljava/lang/Byte;

    const/16 v0, 0x42

    if-eqz p1, :cond_2

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    check-cast p2, Ljava/lang/Byte;

    invoke-virtual {p2}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    invoke-virtual {v1, p2}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p2

    iget p2, p2, Ld0/a/a/u;->a:I

    invoke-virtual {p1, v0, p2}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    goto/16 :goto_8

    :cond_2
    instance-of p1, p2, Ljava/lang/Boolean;

    const/16 v1, 0x5a

    if-eqz p1, :cond_3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iget-object p2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v0, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v0, p1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p1

    iget p1, p1, Ld0/a/a/u;->a:I

    invoke-virtual {p2, v1, p1}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    goto/16 :goto_8

    :cond_3
    instance-of p1, p2, Ljava/lang/Character;

    const/16 v2, 0x43

    if-eqz p1, :cond_4

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v0, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    check-cast p2, Ljava/lang/Character;

    invoke-virtual {p2}, Ljava/lang/Character;->charValue()C

    move-result p2

    invoke-virtual {v0, p2}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p2

    iget p2, p2, Ld0/a/a/u;->a:I

    invoke-virtual {p1, v2, p2}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    goto/16 :goto_8

    :cond_4
    instance-of p1, p2, Ljava/lang/Short;

    const/16 v3, 0x53

    if-eqz p1, :cond_5

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v0, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    check-cast p2, Ljava/lang/Short;

    invoke-virtual {p2}, Ljava/lang/Short;->shortValue()S

    move-result p2

    invoke-virtual {v0, p2}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p2

    iget p2, p2, Ld0/a/a/u;->a:I

    invoke-virtual {p1, v3, p2}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    goto/16 :goto_8

    :cond_5
    instance-of p1, p2, Ld0/a/a/w;

    if-eqz p1, :cond_6

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v0, 0x63

    iget-object v1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    check-cast p2, Ld0/a/a/w;

    invoke-virtual {p2}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p2

    invoke-virtual {p1, v0, p2}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    goto/16 :goto_8

    :cond_6
    instance-of p1, p2, [B

    const/4 v4, 0x0

    const/16 v5, 0x5b

    if-eqz p1, :cond_7

    check-cast p2, [B

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v1, p2

    invoke-virtual {p1, v5, v1}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_0
    if-ge v4, p1, :cond_f

    aget-byte v1, p2, v4

    iget-object v2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v3, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v3, v1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object v1

    iget v1, v1, Ld0/a/a/u;->a:I

    invoke-virtual {v2, v0, v1}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_7
    instance-of p1, p2, [Z

    if-eqz p1, :cond_8

    check-cast p2, [Z

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_1
    if-ge v4, p1, :cond_f

    aget-boolean v0, p2, v4

    iget-object v2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v3, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v3, v0}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v2, v1, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_8
    instance-of p1, p2, [S

    if-eqz p1, :cond_9

    check-cast p2, [S

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_2
    if-ge v4, p1, :cond_f

    aget-short v0, p2, v4

    iget-object v1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v2, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v2, v0}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v1, v3, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_9
    instance-of p1, p2, [C

    if-eqz p1, :cond_a

    check-cast p2, [C

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_3
    if-ge v4, p1, :cond_f

    aget-char v0, p2, v4

    iget-object v1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v3, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v3, v0}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v1, v2, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_a
    instance-of p1, p2, [I

    if-eqz p1, :cond_b

    check-cast p2, [I

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_4
    if-ge v4, p1, :cond_f

    aget v0, p2, v4

    iget-object v1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v2, 0x49

    iget-object v3, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v3, v0}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v1, v2, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_b
    instance-of p1, p2, [J

    if-eqz p1, :cond_c

    check-cast p2, [J

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_5
    if-ge v4, p1, :cond_f

    aget-wide v0, p2, v4

    iget-object v2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v3, 0x4a

    iget-object v5, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    const/4 v6, 0x5

    invoke-virtual {v5, v6, v0, v1}, Ld0/a/a/v;->g(IJ)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v2, v3, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_c
    instance-of p1, p2, [F

    if-eqz p1, :cond_d

    check-cast p2, [F

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_6
    if-ge v4, p1, :cond_f

    aget v0, p2, v4

    iget-object v1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v2, 0x46

    iget-object v3, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    const/4 v5, 0x4

    invoke-virtual {v3, v5, v0}, Ld0/a/a/v;->f(II)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v1, v2, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_d
    instance-of p1, p2, [D

    if-eqz p1, :cond_e

    check-cast p2, [D

    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    array-length v0, p2

    invoke-virtual {p1, v5, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    array-length p1, p2

    :goto_7
    if-ge v4, p1, :cond_f

    aget-wide v0, p2, v4

    iget-object v2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v3, 0x44

    iget-object v5, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    const/4 v6, 0x6

    invoke-virtual {v5, v6, v0, v1}, Ld0/a/a/v;->g(IJ)Ld0/a/a/u;

    move-result-object v0

    iget v0, v0, Ld0/a/a/u;->a:I

    invoke-virtual {v2, v3, v0}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_e
    iget-object p1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {p1, p2}, Ld0/a/a/v;->b(Ljava/lang/Object;)Ld0/a/a/u;

    move-result-object p1

    iget-object p2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget v0, p1, Ld0/a/a/u;->b:I

    const-string v1, ".s.IFJDCS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget p1, p1, Ld0/a/a/u;->a:I

    invoke-virtual {p2, v0, p1}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    :cond_f
    :goto_8
    return-void
.end method

.method public f(Ljava/lang/String;)Ld0/a/a/a;
    .locals 4

    iget v0, p0, Ld0/a/a/a;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ld0/a/a/a;->e:I

    iget-boolean v0, p0, Ld0/a/a/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v1, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    invoke-virtual {v1, p1}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v0, p1}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    :cond_0
    iget-object p1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/16 v0, 0x5b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    new-instance p1, Ld0/a/a/a;

    iget-object v0, p0, Ld0/a/a/a;->a:Ld0/a/a/v;

    iget-object v2, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    const/4 v3, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Ld0/a/a/a;-><init>(Ld0/a/a/v;ZLd0/a/a/c;Ld0/a/a/a;)V

    return-object p1
.end method

.method public g()V
    .locals 4

    iget v0, p0, Ld0/a/a/a;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Ld0/a/a/a;->c:Ld0/a/a/c;

    iget-object v1, v1, Ld0/a/a/c;->a:[B

    iget v2, p0, Ld0/a/a/a;->e:I

    ushr-int/lit8 v3, v2, 0x8

    int-to-byte v3, v3

    aput-byte v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    :cond_0
    return-void
.end method
