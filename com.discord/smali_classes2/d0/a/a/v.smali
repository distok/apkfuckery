.class public final Ld0/a/a/v;
.super Ljava/lang/Object;
.source "SymbolTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld0/a/a/v$a;
    }
.end annotation


# instance fields
.field public final a:Ld0/a/a/f;

.field public final b:Ld0/a/a/d;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:[Ld0/a/a/v$a;

.field public g:I

.field public h:Ld0/a/a/c;

.field public i:I

.field public j:Ld0/a/a/c;

.field public k:I

.field public l:[Ld0/a/a/v$a;


# direct methods
.method public constructor <init>(Ld0/a/a/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld0/a/a/v;->a:Ld0/a/a/f;

    const/4 p1, 0x0

    iput-object p1, p0, Ld0/a/a/v;->b:Ld0/a/a/d;

    const/16 p1, 0x100

    new-array p1, p1, [Ld0/a/a/v$a;

    iput-object p1, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    const/4 p1, 0x1

    iput p1, p0, Ld0/a/a/v;->g:I

    new-instance p1, Ld0/a/a/c;

    invoke-direct {p1}, Ld0/a/a/c;-><init>()V

    iput-object p1, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    return-void
.end method

.method public static r(II)I
    .locals 0

    add-int/2addr p0, p1

    const p1, 0x7fffffff

    and-int/2addr p0, p1

    return p0
.end method

.method public static s(ILjava/lang/String;Ljava/lang/String;I)I
    .locals 0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p2

    mul-int p2, p2, p1

    add-int/lit8 p3, p3, 0x1

    mul-int p3, p3, p2

    add-int/2addr p3, p0

    const p0, 0x7fffffff

    and-int/2addr p0, p3

    return p0
.end method

.method public static t(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p2

    mul-int p2, p2, p1

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result p1

    mul-int p1, p1, p2

    add-int/2addr p1, p0

    const p0, 0x7fffffff

    and-int/2addr p0, p1

    return p0
.end method

.method public static u(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 0

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p2

    mul-int p2, p2, p1

    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result p1

    mul-int p1, p1, p2

    mul-int p1, p1, p4

    add-int/2addr p1, p0

    const p0, 0x7fffffff

    and-int/2addr p0, p1

    return p0
.end method


# virtual methods
.method public varargs a(Ld0/a/a/n;[Ljava/lang/Object;)Ld0/a/a/u;
    .locals 9

    iget-object v0, p0, Ld0/a/a/v;->j:Ld0/a/a/c;

    if-nez v0, :cond_0

    new-instance v0, Ld0/a/a/c;

    invoke-direct {v0}, Ld0/a/a/c;-><init>()V

    iput-object v0, p0, Ld0/a/a/v;->j:Ld0/a/a/c;

    :cond_0
    array-length v1, p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, p2, v3

    invoke-virtual {p0, v4}, Ld0/a/a/v;->b(Ljava/lang/Object;)Ld0/a/a/u;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget v1, v0, Ld0/a/a/c;->b:I

    iget v4, p1, Ld0/a/a/n;->a:I

    iget-object v5, p1, Ld0/a/a/n;->b:Ljava/lang/String;

    iget-object v6, p1, Ld0/a/a/n;->c:Ljava/lang/String;

    iget-object v7, p1, Ld0/a/a/n;->d:Ljava/lang/String;

    iget-boolean v8, p1, Ld0/a/a/n;->e:Z

    move-object v3, p0

    invoke-virtual/range {v3 .. v8}, Ld0/a/a/v;->i(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ld0/a/a/u;

    move-result-object v3

    iget v3, v3, Ld0/a/a/u;->a:I

    invoke-virtual {v0, v3}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    array-length v3, p2

    invoke-virtual {v0, v3}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    array-length v3, p2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_2

    aget-object v5, p2, v4

    invoke-virtual {p0, v5}, Ld0/a/a/v;->b(Ljava/lang/Object;)Ld0/a/a/u;

    move-result-object v5

    iget v5, v5, Ld0/a/a/u;->a:I

    invoke-virtual {v0, v5}, Ld0/a/a/c;->j(I)Ld0/a/a/c;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    iget v0, v0, Ld0/a/a/c;->b:I

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Ld0/a/a/n;->hashCode()I

    move-result p1

    array-length v3, p2

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v3, :cond_3

    aget-object v5, p2, v4

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    xor-int/2addr p1, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    const p2, 0x7fffffff

    and-int v8, p1, p2

    iget-object p1, p0, Ld0/a/a/v;->j:Ld0/a/a/c;

    iget-object p1, p1, Ld0/a/a/c;->a:[B

    iget-object p2, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    array-length v3, p2

    rem-int v3, v8, v3

    aget-object p2, p2, v3

    :goto_3
    if-eqz p2, :cond_7

    iget v3, p2, Ld0/a/a/u;->b:I

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    iget v3, p2, Ld0/a/a/v$a;->h:I

    if-ne v3, v8, :cond_6

    iget-wide v3, p2, Ld0/a/a/u;->f:J

    long-to-int v4, v3

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v0, :cond_5

    add-int v5, v1, v3

    aget-byte v5, p1, v5

    add-int v6, v4, v3

    aget-byte v6, p1, v6

    if-eq v5, v6, :cond_4

    const/4 v3, 0x0

    goto :goto_5

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    const/4 v3, 0x1

    :goto_5
    if-eqz v3, :cond_6

    iget-object p1, p0, Ld0/a/a/v;->j:Ld0/a/a/c;

    iput v1, p1, Ld0/a/a/c;->b:I

    goto :goto_6

    :cond_6
    iget-object p2, p2, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_3

    :cond_7
    new-instance p2, Ld0/a/a/v$a;

    iget v4, p0, Ld0/a/a/v;->i:I

    add-int/lit8 p1, v4, 0x1

    iput p1, p0, Ld0/a/a/v;->i:I

    const/16 v5, 0x40

    int-to-long v6, v1

    move-object v3, p2

    invoke-direct/range {v3 .. v8}, Ld0/a/a/v$a;-><init>(IIJI)V

    invoke-virtual {p0, p2}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    :goto_6
    return-object p2
.end method

.method public b(Ljava/lang/Object;)Ld0/a/a/u;
    .locals 6

    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Ljava/lang/Byte;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_1
    instance-of v0, p1, Ljava/lang/Character;

    if-eqz v0, :cond_2

    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_2
    instance-of v0, p1, Ljava/lang/Short;

    if-eqz v0, :cond_3

    check-cast p1, Ljava/lang/Short;

    invoke-virtual {p1}, Ljava/lang/Short;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_3
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->e(I)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_4
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_5

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result p1

    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Ld0/a/a/v;->f(II)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_5
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_6

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const/4 p1, 0x5

    invoke-virtual {p0, p1, v0, v1}, Ld0/a/a/v;->g(IJ)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_6
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_7

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    const/4 p1, 0x6

    invoke-virtual {p0, p1, v0, v1}, Ld0/a/a/v;->g(IJ)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_7
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_8

    check-cast p1, Ljava/lang/String;

    const/16 v0, 0x8

    invoke-virtual {p0, v0, p1}, Ld0/a/a/v;->m(ILjava/lang/String;)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_8
    instance-of v0, p1, Ld0/a/a/w;

    if-eqz v0, :cond_b

    check-cast p1, Ld0/a/a/w;

    invoke-virtual {p1}, Ld0/a/a/w;->i()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_9

    invoke-virtual {p1}, Ld0/a/a/w;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->c(Ljava/lang/String;)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_9
    const/16 v1, 0xb

    if-ne v0, v1, :cond_a

    invoke-virtual {p1}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x10

    invoke-virtual {p0, v0, p1}, Ld0/a/a/v;->m(ILjava/lang/String;)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_a
    invoke-virtual {p1}, Ld0/a/a/w;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->c(Ljava/lang/String;)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_b
    instance-of v0, p1, Ld0/a/a/n;

    if-eqz v0, :cond_c

    check-cast p1, Ld0/a/a/n;

    iget v1, p1, Ld0/a/a/n;->a:I

    iget-object v2, p1, Ld0/a/a/n;->b:Ljava/lang/String;

    iget-object v3, p1, Ld0/a/a/n;->c:Ljava/lang/String;

    iget-object v4, p1, Ld0/a/a/n;->d:Ljava/lang/String;

    iget-boolean v5, p1, Ld0/a/a/n;->e:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ld0/a/a/v;->i(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_c
    instance-of v0, p1, Ld0/a/a/g;

    if-eqz v0, :cond_d

    check-cast p1, Ld0/a/a/g;

    iget-object v0, p1, Ld0/a/a/g;->a:Ljava/lang/String;

    iget-object v1, p1, Ld0/a/a/g;->b:Ljava/lang/String;

    iget-object v2, p1, Ld0/a/a/g;->c:Ld0/a/a/n;

    iget-object p1, p1, Ld0/a/a/g;->d:[Ljava/lang/Object;

    invoke-virtual {p0, v2, p1}, Ld0/a/a/v;->a(Ld0/a/a/n;[Ljava/lang/Object;)Ld0/a/a/u;

    move-result-object p1

    iget p1, p1, Ld0/a/a/u;->a:I

    const/16 v2, 0x11

    invoke-virtual {p0, v2, v0, v1, p1}, Ld0/a/a/v;->d(ILjava/lang/String;Ljava/lang/String;I)Ld0/a/a/u;

    move-result-object p1

    return-object p1

    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Ljava/lang/String;)Ld0/a/a/u;
    .locals 1

    const/4 v0, 0x7

    invoke-virtual {p0, v0, p1}, Ld0/a/a/v;->m(ILjava/lang/String;)Ld0/a/a/u;

    move-result-object p1

    return-object p1
.end method

.method public final d(ILjava/lang/String;Ljava/lang/String;I)Ld0/a/a/u;
    .locals 10

    invoke-static {p1, p2, p3, p4}, Ld0/a/a/v;->s(ILjava/lang/String;Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {p0, v8}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    iget v1, v0, Ld0/a/a/u;->b:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Ld0/a/a/v$a;->h:I

    if-ne v1, v8, :cond_0

    iget-wide v1, v0, Ld0/a/a/u;->f:J

    int-to-long v3, p4

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    iget-object v1, v0, Ld0/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget-object v0, v0, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    invoke-virtual {p0, p2, p3}, Ld0/a/a/v;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, p1, p4, v1}, Ld0/a/a/c;->f(III)Ld0/a/a/c;

    new-instance v9, Ld0/a/a/v$a;

    iget v1, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Ld0/a/a/v;->g:I

    const/4 v3, 0x0

    int-to-long v6, p4

    move-object v0, v9

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v8}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    invoke-virtual {p0, v9}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    return-object v9
.end method

.method public e(I)Ld0/a/a/u;
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Ld0/a/a/v;->f(II)Ld0/a/a/u;

    move-result-object p1

    return-object p1
.end method

.method public final f(II)Ld0/a/a/u;
    .locals 7

    invoke-static {p1, p2}, Ld0/a/a/v;->r(II)I

    move-result v5

    invoke-virtual {p0, v5}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    iget v1, v0, Ld0/a/a/u;->b:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Ld0/a/a/v$a;->h:I

    if-ne v1, v5, :cond_0

    iget-wide v1, v0, Ld0/a/a/u;->f:J

    int-to-long v3, p2

    cmp-long v6, v1, v3

    if-nez v6, :cond_0

    return-object v0

    :cond_0
    iget-object v0, v0, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    invoke-virtual {v0, p1}, Ld0/a/a/c;->g(I)Ld0/a/a/c;

    invoke-virtual {v0, p2}, Ld0/a/a/c;->i(I)Ld0/a/a/c;

    new-instance v6, Ld0/a/a/v$a;

    iget v1, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Ld0/a/a/v;->g:I

    int-to-long v3, p2

    move-object v0, v6

    move v2, p1

    invoke-direct/range {v0 .. v5}, Ld0/a/a/v$a;-><init>(IIJI)V

    invoke-virtual {p0, v6}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    return-object v6
.end method

.method public final g(IJ)Ld0/a/a/u;
    .locals 10

    long-to-int v0, p2

    add-int v1, p1, v0

    const/16 v2, 0x20

    ushr-long v2, p2, v2

    long-to-int v3, v2

    add-int/2addr v1, v3

    const v2, 0x7fffffff

    and-int v9, v1, v2

    invoke-virtual {p0, v9}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    iget v2, v1, Ld0/a/a/u;->b:I

    if-ne v2, p1, :cond_0

    iget v2, v1, Ld0/a/a/v$a;->h:I

    if-ne v2, v9, :cond_0

    iget-wide v4, v1, Ld0/a/a/u;->f:J

    cmp-long v2, v4, p2

    if-nez v2, :cond_0

    return-object v1

    :cond_0
    iget-object v1, v1, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget v5, p0, Ld0/a/a/v;->g:I

    iget-object v1, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    invoke-virtual {v1, p1}, Ld0/a/a/c;->g(I)Ld0/a/a/c;

    iget v2, v1, Ld0/a/a/c;->b:I

    add-int/lit8 v4, v2, 0x8

    iget-object v6, v1, Ld0/a/a/c;->a:[B

    array-length v6, v6

    if-le v4, v6, :cond_2

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Ld0/a/a/c;->b(I)V

    :cond_2
    iget-object v4, v1, Ld0/a/a/c;->a:[B

    add-int/lit8 v6, v2, 0x1

    ushr-int/lit8 v7, v3, 0x18

    int-to-byte v7, v7

    aput-byte v7, v4, v2

    add-int/lit8 v2, v6, 0x1

    ushr-int/lit8 v7, v3, 0x10

    int-to-byte v7, v7

    aput-byte v7, v4, v6

    add-int/lit8 v6, v2, 0x1

    ushr-int/lit8 v7, v3, 0x8

    int-to-byte v7, v7

    aput-byte v7, v4, v2

    add-int/lit8 v2, v6, 0x1

    int-to-byte v3, v3

    aput-byte v3, v4, v6

    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v6, v0, 0x18

    int-to-byte v6, v6

    aput-byte v6, v4, v2

    add-int/lit8 v2, v3, 0x1

    ushr-int/lit8 v6, v0, 0x10

    int-to-byte v6, v6

    aput-byte v6, v4, v3

    add-int/lit8 v3, v2, 0x1

    ushr-int/lit8 v6, v0, 0x8

    int-to-byte v6, v6

    aput-byte v6, v4, v2

    add-int/lit8 v2, v3, 0x1

    int-to-byte v0, v0

    aput-byte v0, v4, v3

    iput v2, v1, Ld0/a/a/c;->b:I

    iget v0, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Ld0/a/a/v;->g:I

    new-instance v0, Ld0/a/a/v$a;

    move-object v4, v0

    move v6, p1

    move-wide v7, p2

    invoke-direct/range {v4 .. v9}, Ld0/a/a/v$a;-><init>(IIJI)V

    invoke-virtual {p0, v0}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    return-object v0
.end method

.method public final h(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ld0/a/a/v$a;
    .locals 10

    invoke-static {p1, p2, p3, p4}, Ld0/a/a/v;->t(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {p0, v8}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    iget v1, v0, Ld0/a/a/u;->b:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Ld0/a/a/v$a;->h:I

    if-ne v1, v8, :cond_0

    iget-object v1, v0, Ld0/a/a/u;->c:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ld0/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget-object v0, v0, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    const/4 v1, 0x7

    invoke-virtual {p0, v1, p2}, Ld0/a/a/v;->m(ILjava/lang/String;)Ld0/a/a/u;

    move-result-object v1

    iget v1, v1, Ld0/a/a/u;->a:I

    invoke-virtual {p0, p3, p4}, Ld0/a/a/v;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Ld0/a/a/c;->f(III)Ld0/a/a/c;

    new-instance v9, Ld0/a/a/v$a;

    iget v1, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Ld0/a/a/v;->g:I

    const-wide/16 v6, 0x0

    move-object v0, v9

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v8}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    invoke-virtual {p0, v9}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    return-object v9
.end method

.method public i(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ld0/a/a/u;
    .locals 10

    const/16 v0, 0xf

    invoke-static {v0, p2, p3, p4, p1}, Ld0/a/a/v;->u(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v9

    invoke-virtual {p0, v9}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    iget v2, v1, Ld0/a/a/u;->b:I

    if-ne v2, v0, :cond_0

    iget v2, v1, Ld0/a/a/v$a;->h:I

    if-ne v2, v9, :cond_0

    iget-wide v2, v1, Ld0/a/a/u;->f:J

    int-to-long v4, p1

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iget-object v2, v1, Ld0/a/a/u;->c:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Ld0/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    iget-object v1, v1, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    if-gt p1, v1, :cond_2

    iget-object p5, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    const/16 v1, 0x9

    invoke-virtual {p0, v1, p2, p3, p4}, Ld0/a/a/v;->h(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ld0/a/a/v$a;

    move-result-object v1

    iget v1, v1, Ld0/a/a/u;->a:I

    invoke-virtual {p5, v0, p1, v1}, Ld0/a/a/c;->d(III)Ld0/a/a/c;

    goto :goto_2

    :cond_2
    iget-object v1, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    if-eqz p5, :cond_3

    const/16 p5, 0xb

    goto :goto_1

    :cond_3
    const/16 p5, 0xa

    :goto_1
    invoke-virtual {p0, p5, p2, p3, p4}, Ld0/a/a/v;->h(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ld0/a/a/v$a;

    move-result-object p5

    iget p5, p5, Ld0/a/a/u;->a:I

    invoke-virtual {v1, v0, p1, p5}, Ld0/a/a/c;->d(III)Ld0/a/a/c;

    :goto_2
    new-instance p5, Ld0/a/a/v$a;

    iget v2, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Ld0/a/a/v;->g:I

    const/16 v3, 0xf

    int-to-long v7, p1

    move-object v1, p5

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v9}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    invoke-virtual {p0, p5}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    return-object p5
.end method

.method public j(Ljava/lang/String;)Ld0/a/a/u;
    .locals 1

    const/16 v0, 0x13

    invoke-virtual {p0, v0, p1}, Ld0/a/a/v;->m(ILjava/lang/String;)Ld0/a/a/u;

    move-result-object p1

    return-object p1
.end method

.method public k(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int v1, v1, v0

    const/16 v0, 0xc

    add-int/2addr v1, v0

    const v2, 0x7fffffff

    and-int v8, v1, v2

    invoke-virtual {p0, v8}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    iget v2, v1, Ld0/a/a/u;->b:I

    if-ne v2, v0, :cond_0

    iget v2, v1, Ld0/a/a/v$a;->h:I

    if-ne v2, v8, :cond_0

    iget-object v2, v1, Ld0/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget p1, v1, Ld0/a/a/u;->a:I

    return p1

    :cond_0
    iget-object v1, v1, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    invoke-virtual {p0, p1}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, p2}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Ld0/a/a/c;->f(III)Ld0/a/a/c;

    new-instance v0, Ld0/a/a/v$a;

    iget v4, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v1, v4, 0x1

    iput v1, p0, Ld0/a/a/v;->g:I

    const/16 v5, 0xc

    move-object v3, v0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v3 .. v8}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    iget p1, v0, Ld0/a/a/u;->a:I

    return p1
.end method

.method public l(Ljava/lang/String;)I
    .locals 10

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    const v2, 0x7fffffff

    and-int/2addr v0, v2

    invoke-virtual {p0, v0}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    iget v3, v2, Ld0/a/a/u;->b:I

    if-ne v3, v1, :cond_0

    iget v3, v2, Ld0/a/a/v$a;->h:I

    if-ne v3, v0, :cond_0

    iget-object v3, v2, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget p1, v2, Ld0/a/a/u;->a:I

    return p1

    :cond_0
    iget-object v2, v2, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    invoke-virtual {v2, v1}, Ld0/a/a/c;->g(I)Ld0/a/a/c;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const v4, 0xffff

    if-gt v3, v4, :cond_5

    iget v5, v2, Ld0/a/a/c;->b:I

    add-int/lit8 v6, v5, 0x2

    add-int/2addr v6, v3

    iget-object v7, v2, Ld0/a/a/c;->a:[B

    array-length v7, v7

    if-le v6, v7, :cond_2

    add-int/lit8 v6, v3, 0x2

    invoke-virtual {v2, v6}, Ld0/a/a/c;->b(I)V

    :cond_2
    iget-object v6, v2, Ld0/a/a/c;->a:[B

    add-int/lit8 v7, v5, 0x1

    ushr-int/lit8 v8, v3, 0x8

    int-to-byte v8, v8

    aput-byte v8, v6, v5

    add-int/lit8 v5, v7, 0x1

    int-to-byte v8, v3

    aput-byte v8, v6, v7

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v3, :cond_4

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-lt v8, v1, :cond_3

    const/16 v9, 0x7f

    if-gt v8, v9, :cond_3

    add-int/lit8 v9, v5, 0x1

    int-to-byte v8, v8

    aput-byte v8, v6, v5

    add-int/lit8 v7, v7, 0x1

    move v5, v9

    goto :goto_1

    :cond_3
    iput v5, v2, Ld0/a/a/c;->b:I

    invoke-virtual {v2, p1, v7, v4}, Ld0/a/a/c;->a(Ljava/lang/String;II)Ld0/a/a/c;

    goto :goto_2

    :cond_4
    iput v5, v2, Ld0/a/a/c;->b:I

    :goto_2
    new-instance v2, Ld0/a/a/v$a;

    iget v3, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Ld0/a/a/v;->g:I

    invoke-direct {v2, v3, v1, p1, v0}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;I)V

    invoke-virtual {p0, v2}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    iget p1, v2, Ld0/a/a/u;->a:I

    return p1

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "UTF8 string too large"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final m(ILjava/lang/String;)Ld0/a/a/u;
    .locals 4

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/2addr v0, p1

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    iget v2, v1, Ld0/a/a/u;->b:I

    if-ne v2, p1, :cond_0

    iget v2, v1, Ld0/a/a/v$a;->h:I

    if-ne v2, v0, :cond_0

    iget-object v2, v1, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    iget-object v1, v1, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ld0/a/a/v;->h:Ld0/a/a/c;

    invoke-virtual {p0, p2}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, p1, v2}, Ld0/a/a/c;->e(II)Ld0/a/a/c;

    new-instance v1, Ld0/a/a/v$a;

    iget v2, p0, Ld0/a/a/v;->g:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Ld0/a/a/v;->g:I

    invoke-direct {v1, v2, p1, p2, v0}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;I)V

    invoke-virtual {p0, v1}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    return-object v1
.end method

.method public n(Ljava/lang/String;)I
    .locals 4

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x80

    add-int/2addr v0, v1

    const v2, 0x7fffffff

    and-int/2addr v0, v2

    invoke-virtual {p0, v0}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    iget v3, v2, Ld0/a/a/u;->b:I

    if-ne v3, v1, :cond_0

    iget v3, v2, Ld0/a/a/v$a;->h:I

    if-ne v3, v0, :cond_0

    iget-object v3, v2, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget p1, v2, Ld0/a/a/u;->a:I

    return p1

    :cond_0
    iget-object v2, v2, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    new-instance v2, Ld0/a/a/v$a;

    iget v3, p0, Ld0/a/a/v;->k:I

    invoke-direct {v2, v3, v1, p1, v0}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;I)V

    invoke-virtual {p0, v2}, Ld0/a/a/v;->o(Ld0/a/a/v$a;)I

    move-result p1

    return p1
.end method

.method public final o(Ld0/a/a/v$a;)I
    .locals 4

    iget-object v0, p0, Ld0/a/a/v;->l:[Ld0/a/a/v$a;

    if-nez v0, :cond_0

    const/16 v0, 0x10

    new-array v0, v0, [Ld0/a/a/v$a;

    iput-object v0, p0, Ld0/a/a/v;->l:[Ld0/a/a/v$a;

    :cond_0
    iget v0, p0, Ld0/a/a/v;->k:I

    iget-object v1, p0, Ld0/a/a/v;->l:[Ld0/a/a/v$a;

    array-length v2, v1

    if-ne v0, v2, :cond_1

    array-length v0, v1

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Ld0/a/a/v$a;

    array-length v2, v1

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Ld0/a/a/v;->l:[Ld0/a/a/v$a;

    :cond_1
    iget-object v0, p0, Ld0/a/a/v;->l:[Ld0/a/a/v$a;

    iget v1, p0, Ld0/a/a/v;->k:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Ld0/a/a/v;->k:I

    aput-object p1, v0, v1

    invoke-virtual {p0, p1}, Ld0/a/a/v;->v(Ld0/a/a/v$a;)Ld0/a/a/v$a;

    iget p1, p1, Ld0/a/a/u;->a:I

    return p1
.end method

.method public p(Ljava/lang/String;I)I
    .locals 10

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0x81

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    const v2, 0x7fffffff

    and-int v9, v0, v2

    invoke-virtual {p0, v9}, Ld0/a/a/v;->q(I)Ld0/a/a/v$a;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    iget v2, v0, Ld0/a/a/u;->b:I

    if-ne v2, v1, :cond_0

    iget v2, v0, Ld0/a/a/v$a;->h:I

    if-ne v2, v9, :cond_0

    iget-wide v2, v0, Ld0/a/a/u;->f:J

    int-to-long v4, p2

    cmp-long v6, v2, v4

    if-nez v6, :cond_0

    iget-object v2, v0, Ld0/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget p1, v0, Ld0/a/a/u;->a:I

    return p1

    :cond_0
    iget-object v0, v0, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    goto :goto_0

    :cond_1
    new-instance v0, Ld0/a/a/v$a;

    iget v4, p0, Ld0/a/a/v;->k:I

    const/16 v5, 0x81

    int-to-long v7, p2

    move-object v3, v0

    move-object v6, p1

    invoke-direct/range {v3 .. v9}, Ld0/a/a/v$a;-><init>(IILjava/lang/String;JI)V

    invoke-virtual {p0, v0}, Ld0/a/a/v;->o(Ld0/a/a/v$a;)I

    move-result p1

    return p1
.end method

.method public final q(I)Ld0/a/a/v$a;
    .locals 2

    iget-object v0, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    array-length v1, v0

    rem-int/2addr p1, v1

    aget-object p1, v0, p1

    return-object p1
.end method

.method public final v(Ld0/a/a/v$a;)Ld0/a/a/v$a;
    .locals 7

    iget v0, p0, Ld0/a/a/v;->e:I

    iget-object v1, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    array-length v2, v1

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    if-le v0, v2, :cond_2

    array-length v0, v1

    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    new-array v2, v1, [Ld0/a/a/v$a;

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v3, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    aget-object v3, v3, v0

    :goto_1
    if-eqz v3, :cond_0

    iget v4, v3, Ld0/a/a/v$a;->h:I

    rem-int/2addr v4, v1

    iget-object v5, v3, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    aget-object v6, v2, v4

    iput-object v6, v3, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    aput-object v3, v2, v4

    move-object v3, v5

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iput-object v2, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    :cond_2
    iget v0, p0, Ld0/a/a/v;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ld0/a/a/v;->e:I

    iget v0, p1, Ld0/a/a/v$a;->h:I

    iget-object v1, p0, Ld0/a/a/v;->f:[Ld0/a/a/v$a;

    array-length v2, v1

    rem-int/2addr v0, v2

    aget-object v2, v1, v0

    iput-object v2, p1, Ld0/a/a/v$a;->i:Ld0/a/a/v$a;

    aput-object p1, v1, v0

    return-object p1
.end method
