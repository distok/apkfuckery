.class public Ld0/a/a/b;
.super Ljava/lang/Object;
.source "Attribute.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld0/a/a/b$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public b:[B

.field public c:Ld0/a/a/b;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld0/a/a/b;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Ld0/a/a/v;)I
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Ld0/a/a/b;->b(Ld0/a/a/v;[BIII)I

    move-result p1

    return p1
.end method

.method public final b(Ld0/a/a/v;[BIII)I
    .locals 0

    iget-object p2, p1, Ld0/a/a/v;->a:Ld0/a/a/f;

    const/4 p2, 0x0

    move-object p3, p0

    :goto_0
    if-eqz p3, :cond_0

    iget-object p4, p3, Ld0/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, p4}, Ld0/a/a/v;->l(Ljava/lang/String;)I

    iget-object p4, p3, Ld0/a/a/b;->b:[B

    array-length p4, p4

    add-int/lit8 p4, p4, 0x6

    add-int/2addr p2, p4

    iget-object p3, p3, Ld0/a/a/b;->c:Ld0/a/a/b;

    goto :goto_0

    :cond_0
    return p2
.end method

.method public final c()I
    .locals 2

    const/4 v0, 0x0

    move-object v1, p0

    :goto_0
    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, v1, Ld0/a/a/b;->c:Ld0/a/a/b;

    goto :goto_0

    :cond_0
    return v0
.end method

.method public final d(Ld0/a/a/v;Ld0/a/a/c;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Ld0/a/a/b;->e(Ld0/a/a/v;[BIIILd0/a/a/c;)V

    return-void
.end method

.method public final e(Ld0/a/a/v;[BIIILd0/a/a/c;)V
    .locals 0

    iget-object p2, p1, Ld0/a/a/v;->a:Ld0/a/a/f;

    move-object p2, p0

    :goto_0
    if-eqz p2, :cond_0

    iget-object p3, p2, Ld0/a/a/b;->b:[B

    array-length p4, p3

    iget-object p5, p2, Ld0/a/a/b;->a:Ljava/lang/String;

    invoke-static {p1, p5, p6, p4}, Lf/e/c/a/a;->Y(Ld0/a/a/v;Ljava/lang/String;Ld0/a/a/c;I)V

    const/4 p5, 0x0

    invoke-virtual {p6, p3, p5, p4}, Ld0/a/a/c;->h([BII)Ld0/a/a/c;

    iget-object p2, p2, Ld0/a/a/b;->c:Ld0/a/a/b;

    goto :goto_0

    :cond_0
    return-void
.end method
