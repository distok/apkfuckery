.class public final Lv/a/a/a;
.super Lx/m/c/k;
.source "Compressor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lx/m/c/k;",
        "Lkotlin/jvm/functions/Function1<",
        "Lv/a/a/e/a;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# static fields
.field public static final d:Lv/a/a/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lv/a/a/a;

    invoke-direct {v0}, Lv/a/a/a;-><init>()V

    sput-object v0, Lv/a/a/a;->d:Lv/a/a/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx/m/c/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Lv/a/a/e/a;

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x264

    const/16 v1, 0x330

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x50

    const-string v4, "$this$default"

    invoke-static {p1, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "format"

    invoke-static {v2, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lv/a/a/e/c;

    invoke-direct {v4, v0, v1, v2, v3}, Lv/a/a/e/c;-><init>(IILandroid/graphics/Bitmap$CompressFormat;I)V

    const-string v0, "constraint"

    invoke-static {v4, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lv/a/a/e/a;->a:Ljava/util/List;

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object p1, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object p1
.end method
