.class public final Lf0/r$b;
.super Lokhttp3/ResponseBody;
.source "OkHttpCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf0/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final f:Lokhttp3/ResponseBody;

.field public final g:Lc0/g;

.field public h:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Lokhttp3/ResponseBody;)V
    .locals 1

    invoke-direct {p0}, Lokhttp3/ResponseBody;-><init>()V

    iput-object p1, p0, Lf0/r$b;->f:Lokhttp3/ResponseBody;

    new-instance v0, Lf0/r$b$a;

    invoke-virtual {p1}, Lokhttp3/ResponseBody;->c()Lc0/g;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lf0/r$b$a;-><init>(Lf0/r$b;Lc0/x;)V

    const-string p1, "$this$buffer"

    invoke-static {v0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lc0/r;

    invoke-direct {p1, v0}, Lc0/r;-><init>(Lc0/x;)V

    iput-object p1, p0, Lf0/r$b;->g:Lc0/g;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lf0/r$b;->f:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Lokhttp3/MediaType;
    .locals 1

    iget-object v0, p0, Lf0/r$b;->f:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->b()Lokhttp3/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public c()Lc0/g;
    .locals 1

    iget-object v0, p0, Lf0/r$b;->g:Lc0/g;

    return-object v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lf0/r$b;->f:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    return-void
.end method
