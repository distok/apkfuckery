.class public final Lf0/j$b;
.super Lf0/j;
.source "HttpServiceMethod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf0/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResponseT:",
        "Ljava/lang/Object;",
        ">",
        "Lf0/j<",
        "TResponseT;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lf0/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf0/e<",
            "TResponseT;",
            "Lf0/d<",
            "TResponseT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf0/y;Lb0/e$a;Lf0/h;Lf0/e;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/y;",
            "Lb0/e$a;",
            "Lf0/h<",
            "Lokhttp3/ResponseBody;",
            "TResponseT;>;",
            "Lf0/e<",
            "TResponseT;",
            "Lf0/d<",
            "TResponseT;>;>;Z)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lf0/j;-><init>(Lf0/y;Lb0/e$a;Lf0/h;)V

    iput-object p4, p0, Lf0/j$b;->d:Lf0/e;

    return-void
.end method


# virtual methods
.method public c(Lf0/d;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/d<",
            "TResponseT;>;[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    iget-object v0, p0, Lf0/j$b;->d:Lf0/e;

    invoke-interface {v0, p1}, Lf0/e;->b(Lf0/d;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf0/d;

    array-length v0, p2

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    aget-object p2, p2, v0

    check-cast p2, Lkotlin/coroutines/Continuation;

    :try_start_0
    new-instance v0, Ly/a/g;

    invoke-static {p2}, Lf/h/a/f/f/n/g;->intercepted(Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ly/a/g;-><init>(Lkotlin/coroutines/Continuation;I)V

    new-instance v1, Lf0/l;

    invoke-direct {v1, p1}, Lf0/l;-><init>(Lf0/d;)V

    invoke-virtual {v0, v1}, Ly/a/g;->k(Lkotlin/jvm/functions/Function1;)V

    new-instance v1, Lf0/m;

    invoke-direct {v1, v0}, Lf0/m;-><init>(Ly/a/f;)V

    invoke-interface {p1, v1}, Lf0/d;->v(Lf0/f;)V

    invoke-virtual {v0}, Ly/a/g;->j()Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Lx/j/g/a;->d:Lx/j/g/a;

    if-ne p1, v0, :cond_0

    const-string v0, "frame"

    invoke-static {p2, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1, p2}, Ly/a/g0;->J(Ljava/lang/Exception;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
