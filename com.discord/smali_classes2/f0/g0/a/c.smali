.class public final Lf0/g0/a/c;
.super Ljava/lang/Object;
.source "CallExecuteOnSubscribe.java"

# interfaces
.implements Lrx/Observable$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$a<",
        "Lf0/z<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field public final d:Lf0/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf0/d<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf0/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/d<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf0/g0/a/c;->d:Lf0/d;

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lrx/Subscriber;

    iget-object v0, p0, Lf0/g0/a/c;->d:Lf0/d;

    invoke-interface {v0}, Lf0/d;->y()Lf0/d;

    move-result-object v0

    new-instance v1, Lf0/g0/a/b;

    invoke-direct {v1, v0, p1}, Lf0/g0/a/b;-><init>(Lf0/d;Lrx/Subscriber;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    invoke-virtual {p1, v1}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    :try_start_0
    invoke-interface {v0}, Lf0/d;->execute()Lf0/z;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1, p1}, Lf0/g0/a/b;->c(Lf0/z;)V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {p1}, Ly/a/g0;->P(Ljava/lang/Throwable;)V

    invoke-virtual {v1, p1}, Lf0/g0/a/b;->b(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
