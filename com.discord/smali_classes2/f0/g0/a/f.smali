.class public final Lf0/g0/a/f;
.super Ljava/lang/Object;
.source "RxJavaCallAdapter.java"

# interfaces
.implements Lf0/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf0/e<",
        "TR;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/reflect/Type;

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Type;Lrx/Scheduler;ZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf0/g0/a/f;->a:Ljava/lang/reflect/Type;

    iput-boolean p4, p0, Lf0/g0/a/f;->b:Z

    iput-boolean p5, p0, Lf0/g0/a/f;->c:Z

    iput-boolean p6, p0, Lf0/g0/a/f;->d:Z

    iput-boolean p7, p0, Lf0/g0/a/f;->e:Z

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/reflect/Type;
    .locals 1

    iget-object v0, p0, Lf0/g0/a/f;->a:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public b(Lf0/d;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/d<",
            "TR;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    new-instance v0, Lf0/g0/a/c;

    invoke-direct {v0, p1}, Lf0/g0/a/c;-><init>(Lf0/d;)V

    iget-boolean p1, p0, Lf0/g0/a/f;->b:Z

    if-eqz p1, :cond_0

    new-instance p1, Lf0/g0/a/e;

    invoke-direct {p1, v0}, Lf0/g0/a/e;-><init>(Lrx/Observable$a;)V

    :goto_0
    move-object v0, p1

    goto :goto_1

    :cond_0
    iget-boolean p1, p0, Lf0/g0/a/f;->c:Z

    if-eqz p1, :cond_1

    new-instance p1, Lf0/g0/a/a;

    invoke-direct {p1, v0}, Lf0/g0/a/a;-><init>(Lrx/Observable$a;)V

    goto :goto_0

    :cond_1
    :goto_1
    new-instance p1, Lrx/Observable;

    invoke-static {v0}, Lg0/o/l;->a(Lrx/Observable$a;)Lrx/Observable$a;

    move-result-object v0

    invoke-direct {p1, v0}, Lrx/Observable;-><init>(Lrx/Observable$a;)V

    iget-boolean v0, p0, Lf0/g0/a/f;->d:Z

    if-eqz v0, :cond_2

    new-instance v0, Lg0/h;

    new-instance v1, Lg0/l/a/h0;

    invoke-direct {v1, p1}, Lg0/l/a/h0;-><init>(Lrx/Observable;)V

    invoke-direct {v0, v1}, Lg0/h;-><init>(Lg0/h$a;)V

    return-object v0

    :cond_2
    iget-boolean v0, p0, Lf0/g0/a/f;->e:Z

    if-eqz v0, :cond_3

    new-instance v0, Lg0/c;

    invoke-direct {v0, p1}, Lg0/c;-><init>(Lrx/Observable;)V

    :try_start_0
    new-instance p1, Lg0/d;

    invoke-direct {p1, v0}, Lg0/d;-><init>(Lg0/d$a;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {p1}, Lg0/o/l;->b(Ljava/lang/Throwable;)V

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Actually not, but can\'t pass out an exception otherwise..."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/NullPointerException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v0

    :catch_0
    move-exception p1

    throw p1

    :cond_3
    return-object p1
.end method
