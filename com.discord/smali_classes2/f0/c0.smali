.class public abstract Lf0/c0;
.super Ljava/lang/Object;
.source "ServiceMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Lf0/b0;Ljava/lang/reflect/Method;)Lf0/c0;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lf0/b0;",
            "Ljava/lang/reflect/Method;",
            ")",
            "Lf0/c0<",
            "TT;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    new-instance v2, Lf0/y$a;

    invoke-direct {v2, v0, v1}, Lf0/y$a;-><init>(Lf0/b0;Ljava/lang/reflect/Method;)V

    iget-object v3, v2, Lf0/y$a;->c:[Ljava/lang/annotation/Annotation;

    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    const-string v7, "HEAD"

    const/4 v8, 0x1

    if-ge v6, v4, :cond_11

    aget-object v9, v3, v6

    instance-of v10, v9, Lf0/i0/b;

    if-eqz v10, :cond_0

    check-cast v9, Lf0/i0/b;

    invoke-interface {v9}, Lf0/i0/b;->value()Ljava/lang/String;

    move-result-object v7

    const-string v8, "DELETE"

    invoke-virtual {v2, v8, v7, v5}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_0
    instance-of v10, v9, Lf0/i0/f;

    if-eqz v10, :cond_1

    check-cast v9, Lf0/i0/f;

    invoke-interface {v9}, Lf0/i0/f;->value()Ljava/lang/String;

    move-result-object v7

    const-string v8, "GET"

    invoke-virtual {v2, v8, v7, v5}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_1
    instance-of v10, v9, Lf0/i0/g;

    if-eqz v10, :cond_2

    check-cast v9, Lf0/i0/g;

    invoke-interface {v9}, Lf0/i0/g;->value()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8, v5}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_2
    instance-of v7, v9, Lf0/i0/n;

    if-eqz v7, :cond_3

    check-cast v9, Lf0/i0/n;

    invoke-interface {v9}, Lf0/i0/n;->value()Ljava/lang/String;

    move-result-object v7

    const-string v9, "PATCH"

    invoke-virtual {v2, v9, v7, v8}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_3
    instance-of v7, v9, Lf0/i0/o;

    if-eqz v7, :cond_4

    check-cast v9, Lf0/i0/o;

    invoke-interface {v9}, Lf0/i0/o;->value()Ljava/lang/String;

    move-result-object v7

    const-string v9, "POST"

    invoke-virtual {v2, v9, v7, v8}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_4
    instance-of v7, v9, Lf0/i0/p;

    if-eqz v7, :cond_5

    check-cast v9, Lf0/i0/p;

    invoke-interface {v9}, Lf0/i0/p;->value()Ljava/lang/String;

    move-result-object v7

    const-string v9, "PUT"

    invoke-virtual {v2, v9, v7, v8}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_5
    instance-of v7, v9, Lf0/i0/m;

    if-eqz v7, :cond_6

    check-cast v9, Lf0/i0/m;

    invoke-interface {v9}, Lf0/i0/m;->value()Ljava/lang/String;

    move-result-object v7

    const-string v8, "OPTIONS"

    invoke-virtual {v2, v8, v7, v5}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_6
    instance-of v7, v9, Lf0/i0/h;

    if-eqz v7, :cond_7

    check-cast v9, Lf0/i0/h;

    invoke-interface {v9}, Lf0/i0/h;->method()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v9}, Lf0/i0/h;->path()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v9}, Lf0/i0/h;->hasBody()Z

    move-result v9

    invoke-virtual {v2, v7, v8, v9}, Lf0/y$a;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :cond_7
    instance-of v7, v9, Lf0/i0/k;

    if-eqz v7, :cond_c

    check-cast v9, Lf0/i0/k;

    invoke-interface {v9}, Lf0/i0/k;->value()[Ljava/lang/String;

    move-result-object v7

    array-length v9, v7

    if-eqz v9, :cond_b

    new-instance v9, Lokhttp3/Headers$a;

    invoke-direct {v9}, Lokhttp3/Headers$a;-><init>()V

    array-length v10, v7

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v10, :cond_a

    aget-object v12, v7, v11

    const/16 v13, 0x3a

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_9

    if-eqz v13, :cond_9

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v14

    sub-int/2addr v14, v8

    if-eq v13, v14, :cond_9

    invoke-virtual {v12, v5, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Content-Type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    :try_start_0
    invoke-static {v12}, Lokhttp3/MediaType;->b(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v13

    iput-object v13, v2, Lf0/y$a;->t:Lokhttp3/MediaType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v2, v8, [Ljava/lang/Object;

    aput-object v12, v2, v5

    const-string v3, "Malformed content type: %s"

    invoke-static {v1, v0, v3, v2}, Lf0/f0;->k(Ljava/lang/reflect/Method;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_8
    invoke-virtual {v9, v14, v12}, Lokhttp3/Headers$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_9
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v8, [Ljava/lang/Object;

    aput-object v12, v1, v5

    const-string v2, "@Headers value must be in the form \"Name: Value\". Found: \"%s\""

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_a
    invoke-virtual {v9}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v7

    iput-object v7, v2, Lf0/y$a;->s:Lokhttp3/Headers;

    goto :goto_3

    :cond_b
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "@Headers annotation is empty."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_c
    instance-of v7, v9, Lf0/i0/l;

    const-string v10, "Only one encoding annotation is allowed."

    if-eqz v7, :cond_e

    iget-boolean v7, v2, Lf0/y$a;->p:Z

    if-nez v7, :cond_d

    iput-boolean v8, v2, Lf0/y$a;->q:Z

    goto :goto_3

    :cond_d
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v10, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_e
    instance-of v7, v9, Lf0/i0/e;

    if-eqz v7, :cond_10

    iget-boolean v7, v2, Lf0/y$a;->q:Z

    if-nez v7, :cond_f

    iput-boolean v8, v2, Lf0/y$a;->p:Z

    goto :goto_3

    :cond_f
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v10, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_10
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_11
    iget-object v3, v2, Lf0/y$a;->n:Ljava/lang/String;

    if-eqz v3, :cond_7d

    iget-boolean v3, v2, Lf0/y$a;->o:Z

    if-nez v3, :cond_14

    iget-boolean v3, v2, Lf0/y$a;->q:Z

    if-nez v3, :cond_13

    iget-boolean v3, v2, Lf0/y$a;->p:Z

    if-nez v3, :cond_12

    goto :goto_4

    :cond_12
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST)."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_13
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "Multipart can only be specified on HTTP methods with request body (e.g., @POST)."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_14
    :goto_4
    iget-object v3, v2, Lf0/y$a;->d:[[Ljava/lang/annotation/Annotation;

    array-length v3, v3

    new-array v4, v3, [Lf0/v;

    iput-object v4, v2, Lf0/y$a;->v:[Lf0/v;

    add-int/lit8 v4, v3, -0x1

    const/4 v5, 0x0

    :goto_5
    if-ge v5, v3, :cond_68

    iget-object v6, v2, Lf0/y$a;->v:[Lf0/v;

    iget-object v8, v2, Lf0/y$a;->e:[Ljava/lang/reflect/Type;

    aget-object v14, v8, v5

    iget-object v8, v2, Lf0/y$a;->d:[[Ljava/lang/annotation/Annotation;

    aget-object v15, v8, v5

    if-ne v5, v4, :cond_15

    const/4 v8, 0x1

    const/16 v16, 0x1

    goto :goto_6

    :cond_15
    const/4 v8, 0x0

    const/16 v16, 0x0

    :goto_6
    if-eqz v15, :cond_65

    array-length v13, v15

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v17, v9

    const/4 v12, 0x0

    :goto_7
    if-ge v12, v13, :cond_64

    aget-object v8, v15, v12

    sget-object v9, Lf0/v$m;->a:Lf0/v$m;

    const-class v10, Ljava/lang/String;

    const-class v11, Lokhttp3/MultipartBody$Part;

    move/from16 v18, v3

    instance-of v3, v8, Lf0/i0/y;

    move/from16 v19, v4

    const-string v4, "@Path parameters may not be used with @Url."

    if-eqz v3, :cond_1e

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v3, v2, Lf0/y$a;->m:Z

    if-nez v3, :cond_1d

    iget-boolean v3, v2, Lf0/y$a;->i:Z

    if-nez v3, :cond_1c

    iget-boolean v3, v2, Lf0/y$a;->j:Z

    if-nez v3, :cond_1b

    iget-boolean v3, v2, Lf0/y$a;->k:Z

    if-nez v3, :cond_1a

    iget-boolean v3, v2, Lf0/y$a;->l:Z

    if-nez v3, :cond_19

    iget-object v3, v2, Lf0/y$a;->r:Ljava/lang/String;

    if-nez v3, :cond_18

    const/4 v3, 0x1

    iput-boolean v3, v2, Lf0/y$a;->m:Z

    const-class v3, Lb0/x;

    if-eq v14, v3, :cond_17

    if-eq v14, v10, :cond_17

    const-class v3, Ljava/net/URI;

    if-eq v14, v3, :cond_17

    instance-of v3, v14, Ljava/lang/Class;

    if-eqz v3, :cond_16

    const-string v3, "android.net.Uri"

    move-object v4, v14

    check-cast v4, Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    goto :goto_8

    :cond_16
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type."

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_17
    :goto_8
    new-instance v9, Lf0/v$n;

    iget-object v3, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v9, v3, v5}, Lf0/v$n;-><init>(Ljava/lang/reflect/Method;I)V

    move/from16 v20, v12

    move/from16 v21, v13

    goto/16 :goto_e

    :cond_18
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v2, Lf0/y$a;->n:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "@Url cannot be used with @%s URL"

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_19
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "A @Url parameter must not come after a @QueryMap."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_1a
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "A @Url parameter must not come after a @QueryName."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_1b
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "A @Url parameter must not come after a @Query."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_1c
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v4, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_1d
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "Multiple @Url method annotations found."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_1e
    instance-of v3, v8, Lf0/i0/s;

    move/from16 v20, v12

    const/4 v12, 0x2

    if-eqz v3, :cond_26

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v3, v2, Lf0/y$a;->j:Z

    if-nez v3, :cond_25

    iget-boolean v3, v2, Lf0/y$a;->k:Z

    if-nez v3, :cond_24

    iget-boolean v3, v2, Lf0/y$a;->l:Z

    if-nez v3, :cond_23

    iget-boolean v3, v2, Lf0/y$a;->m:Z

    if-nez v3, :cond_22

    iget-object v3, v2, Lf0/y$a;->r:Ljava/lang/String;

    if-eqz v3, :cond_21

    const/4 v3, 0x1

    iput-boolean v3, v2, Lf0/y$a;->i:Z

    check-cast v8, Lf0/i0/s;

    invoke-interface {v8}, Lf0/i0/s;->value()Ljava/lang/String;

    move-result-object v11

    sget-object v3, Lf0/y$a;->y:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_20

    iget-object v3, v2, Lf0/y$a;->u:Ljava/util/Set;

    invoke-interface {v3, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    iget-object v3, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v3, v14, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v12

    new-instance v3, Lf0/v$i;

    iget-object v9, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-interface {v8}, Lf0/i0/s;->encoded()Z

    move-result v4

    move-object v8, v3

    move v10, v5

    move/from16 v21, v13

    move v13, v4

    invoke-direct/range {v8 .. v13}, Lf0/v$i;-><init>(Ljava/lang/reflect/Method;ILjava/lang/String;Lf0/h;Z)V

    goto/16 :goto_9

    :cond_1f
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v12, [Ljava/lang/Object;

    iget-object v2, v2, Lf0/y$a;->r:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v11, v1, v2

    const-string v2, "URL \"%s\" does not contain \"{%s}\"."

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_20
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v3, v12, [Ljava/lang/Object;

    sget-object v4, Lf0/y$a;->x:Ljava/util/regex/Pattern;

    invoke-virtual {v4}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    aput-object v11, v3, v0

    const-string v0, "@Path parameter name must match %s. Found: %s"

    invoke-static {v2, v5, v0, v3}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_21
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v2, v2, Lf0/y$a;->n:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "@Path can only be used with relative url on @%s"

    invoke-static {v3, v5, v1, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_22
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v4, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_23
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "A @Path parameter must not come after a @QueryMap."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_24
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "A @Path parameter must not come after a @QueryName."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_25
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "A @Path parameter must not come after a @Query."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_26
    move/from16 v21, v13

    instance-of v3, v8, Lf0/i0/t;

    const-string v4, "<String>)"

    const-string v12, " must include generic type (e.g., "

    if-eqz v3, :cond_2a

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    check-cast v8, Lf0/i0/t;

    invoke-interface {v8}, Lf0/i0/t;->value()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8}, Lf0/i0/t;->encoded()Z

    move-result v8

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v9

    const/4 v10, 0x1

    iput-boolean v10, v2, Lf0/y$a;->j:Z

    const-class v10, Ljava/lang/Iterable;

    invoke-virtual {v10, v9}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v10

    if-eqz v10, :cond_28

    instance-of v10, v14, Ljava/lang/reflect/ParameterizedType;

    if-eqz v10, :cond_27

    move-object v4, v14

    check-cast v4, Ljava/lang/reflect/ParameterizedType;

    const/4 v9, 0x0

    invoke-static {v9, v4}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v4

    iget-object v9, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v9, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v9, Lf0/v$j;

    invoke-direct {v9, v3, v4, v8}, Lf0/v$j;-><init>(Ljava/lang/String;Lf0/h;Z)V

    new-instance v3, Lf0/t;

    invoke-direct {v3, v9}, Lf0/t;-><init>(Lf0/v;)V

    goto :goto_9

    :cond_27
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9, v1, v12, v4}, Lf/e/c/a/a;->i(Ljava/lang/Class;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_28
    invoke-virtual {v9}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-virtual {v9}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lf0/y$a;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v4

    iget-object v9, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v9, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v9, Lf0/v$j;

    invoke-direct {v9, v3, v4, v8}, Lf0/v$j;-><init>(Ljava/lang/String;Lf0/h;Z)V

    new-instance v3, Lf0/u;

    invoke-direct {v3, v9}, Lf0/u;-><init>(Lf0/v;)V

    :goto_9
    move-object v9, v3

    goto/16 :goto_e

    :cond_29
    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v4, v14, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v9, Lf0/v$j;

    invoke-direct {v9, v3, v4, v8}, Lf0/v$j;-><init>(Ljava/lang/String;Lf0/h;Z)V

    goto/16 :goto_e

    :cond_2a
    instance-of v3, v8, Lf0/i0/v;

    if-eqz v3, :cond_2e

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    check-cast v8, Lf0/i0/v;

    invoke-interface {v8}, Lf0/i0/v;->encoded()Z

    move-result v3

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v8

    const/4 v9, 0x1

    iput-boolean v9, v2, Lf0/y$a;->k:Z

    const-class v9, Ljava/lang/Iterable;

    invoke-virtual {v9, v8}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    if-eqz v9, :cond_2c

    instance-of v9, v14, Ljava/lang/reflect/ParameterizedType;

    if-eqz v9, :cond_2b

    move-object v4, v14

    check-cast v4, Ljava/lang/reflect/ParameterizedType;

    const/4 v8, 0x0

    invoke-static {v8, v4}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v4

    iget-object v8, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v8, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v8, Lf0/v$l;

    invoke-direct {v8, v4, v3}, Lf0/v$l;-><init>(Lf0/h;Z)V

    new-instance v9, Lf0/t;

    invoke-direct {v9, v8}, Lf0/t;-><init>(Lf0/v;)V

    goto/16 :goto_e

    :cond_2b
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8, v1, v12, v4}, Lf/e/c/a/a;->i(Ljava/lang/Class;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_2c
    invoke-virtual {v8}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_2d

    invoke-virtual {v8}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lf0/y$a;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v4

    iget-object v8, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v8, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v8, Lf0/v$l;

    invoke-direct {v8, v4, v3}, Lf0/v$l;-><init>(Lf0/h;Z)V

    new-instance v9, Lf0/u;

    invoke-direct {v9, v8}, Lf0/u;-><init>(Lf0/v;)V

    goto/16 :goto_e

    :cond_2d
    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v4, v14, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v8, Lf0/v$l;

    invoke-direct {v8, v4, v3}, Lf0/v$l;-><init>(Lf0/h;Z)V

    :goto_a
    move-object v9, v8

    goto/16 :goto_e

    :cond_2e
    instance-of v3, v8, Lf0/i0/u;

    const-string v13, "Map must include generic types (e.g., Map<String, String>)"

    if-eqz v3, :cond_32

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x1

    iput-boolean v4, v2, Lf0/y$a;->l:Z

    const-class v9, Ljava/util/Map;

    invoke-virtual {v9, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    if-eqz v9, :cond_31

    const-class v9, Ljava/util/Map;

    invoke-static {v14, v3, v9}, Lf0/f0;->g(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v3

    instance-of v9, v3, Ljava/lang/reflect/ParameterizedType;

    if-eqz v9, :cond_30

    check-cast v3, Ljava/lang/reflect/ParameterizedType;

    const/4 v9, 0x0

    invoke-static {v9, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v9

    if-ne v10, v9, :cond_2f

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v3

    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v4, v3, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v3

    new-instance v4, Lf0/v$k;

    iget-object v9, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    check-cast v8, Lf0/i0/u;

    invoke-interface {v8}, Lf0/i0/u;->encoded()Z

    move-result v8

    invoke-direct {v4, v9, v5, v3, v8}, Lf0/v$k;-><init>(Ljava/lang/reflect/Method;ILf0/h;Z)V

    :goto_b
    move-object v9, v4

    goto/16 :goto_e

    :cond_2f
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const-string v1, "@QueryMap keys must be of type String: "

    invoke-static {v1, v9}, Lf/e/c/a/a;->t(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_30
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v13, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_31
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@QueryMap parameter type must be Map."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_32
    instance-of v3, v8, Lf0/i0/i;

    if-eqz v3, :cond_36

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    check-cast v8, Lf0/i0/i;

    invoke-interface {v8}, Lf0/i0/i;->value()Ljava/lang/String;

    move-result-object v3

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v8

    const-class v9, Ljava/lang/Iterable;

    invoke-virtual {v9, v8}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    if-eqz v9, :cond_34

    instance-of v9, v14, Ljava/lang/reflect/ParameterizedType;

    if-eqz v9, :cond_33

    move-object v4, v14

    check-cast v4, Ljava/lang/reflect/ParameterizedType;

    const/4 v8, 0x0

    invoke-static {v8, v4}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v4

    iget-object v8, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v8, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v8, Lf0/v$d;

    invoke-direct {v8, v3, v4}, Lf0/v$d;-><init>(Ljava/lang/String;Lf0/h;)V

    new-instance v9, Lf0/t;

    invoke-direct {v9, v8}, Lf0/t;-><init>(Lf0/v;)V

    goto/16 :goto_e

    :cond_33
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8, v1, v12, v4}, Lf/e/c/a/a;->i(Ljava/lang/Class;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_34
    invoke-virtual {v8}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_35

    invoke-virtual {v8}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lf0/y$a;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v4

    iget-object v8, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v8, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v8, Lf0/v$d;

    invoke-direct {v8, v3, v4}, Lf0/v$d;-><init>(Ljava/lang/String;Lf0/h;)V

    new-instance v9, Lf0/u;

    invoke-direct {v9, v8}, Lf0/u;-><init>(Lf0/v;)V

    goto/16 :goto_e

    :cond_35
    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v4, v14, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v8, Lf0/v$d;

    invoke-direct {v8, v3, v4}, Lf0/v$d;-><init>(Ljava/lang/String;Lf0/h;)V

    goto/16 :goto_a

    :cond_36
    instance-of v3, v8, Lf0/i0/j;

    if-eqz v3, :cond_3b

    const-class v3, Lokhttp3/Headers;

    if-ne v14, v3, :cond_37

    new-instance v9, Lf0/v$f;

    iget-object v3, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v9, v3, v5}, Lf0/v$f;-><init>(Ljava/lang/reflect/Method;I)V

    goto/16 :goto_e

    :cond_37
    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    const-class v4, Ljava/util/Map;

    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_3a

    const-class v4, Ljava/util/Map;

    invoke-static {v14, v3, v4}, Lf0/f0;->g(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v3

    instance-of v4, v3, Ljava/lang/reflect/ParameterizedType;

    if-eqz v4, :cond_39

    check-cast v3, Ljava/lang/reflect/ParameterizedType;

    const/4 v4, 0x0

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v4

    if-ne v10, v4, :cond_38

    const/4 v4, 0x1

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v3

    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v4, v3, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v3

    new-instance v9, Lf0/v$e;

    iget-object v4, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v9, v4, v5, v3}, Lf0/v$e;-><init>(Ljava/lang/reflect/Method;ILf0/h;)V

    goto/16 :goto_e

    :cond_38
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const-string v1, "@HeaderMap keys must be of type String: "

    invoke-static {v1, v4}, Lf/e/c/a/a;->t(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_39
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v13, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_3a
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@HeaderMap parameter type must be Map."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_3b
    instance-of v3, v8, Lf0/i0/c;

    if-eqz v3, :cond_40

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v3, v2, Lf0/y$a;->p:Z

    if-eqz v3, :cond_3f

    check-cast v8, Lf0/i0/c;

    invoke-interface {v8}, Lf0/i0/c;->value()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8}, Lf0/i0/c;->encoded()Z

    move-result v8

    const/4 v9, 0x1

    iput-boolean v9, v2, Lf0/y$a;->f:Z

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v9

    const-class v10, Ljava/lang/Iterable;

    invoke-virtual {v10, v9}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v10

    if-eqz v10, :cond_3d

    instance-of v10, v14, Ljava/lang/reflect/ParameterizedType;

    if-eqz v10, :cond_3c

    move-object v4, v14

    check-cast v4, Ljava/lang/reflect/ParameterizedType;

    const/4 v9, 0x0

    invoke-static {v9, v4}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v4

    iget-object v9, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v9, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v9, Lf0/v$b;

    invoke-direct {v9, v3, v4, v8}, Lf0/v$b;-><init>(Ljava/lang/String;Lf0/h;Z)V

    new-instance v3, Lf0/t;

    invoke-direct {v3, v9}, Lf0/t;-><init>(Lf0/v;)V

    goto/16 :goto_9

    :cond_3c
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9, v1, v12, v4}, Lf/e/c/a/a;->i(Ljava/lang/Class;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_3d
    invoke-virtual {v9}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_3e

    invoke-virtual {v9}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lf0/y$a;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v4

    iget-object v9, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v9, v4, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v9, Lf0/v$b;

    invoke-direct {v9, v3, v4, v8}, Lf0/v$b;-><init>(Ljava/lang/String;Lf0/h;Z)V

    new-instance v3, Lf0/u;

    invoke-direct {v3, v9}, Lf0/u;-><init>(Lf0/v;)V

    goto/16 :goto_9

    :cond_3e
    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v4, v14, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4

    new-instance v9, Lf0/v$b;

    invoke-direct {v9, v3, v4, v8}, Lf0/v$b;-><init>(Ljava/lang/String;Lf0/h;Z)V

    goto/16 :goto_e

    :cond_3f
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "@Field parameters can only be used with form encoding."

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_40
    instance-of v3, v8, Lf0/i0/d;

    if-eqz v3, :cond_45

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v3, v2, Lf0/y$a;->p:Z

    if-eqz v3, :cond_44

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    const-class v4, Ljava/util/Map;

    invoke-virtual {v4, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_43

    const-class v4, Ljava/util/Map;

    invoke-static {v14, v3, v4}, Lf0/f0;->g(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v3

    instance-of v4, v3, Ljava/lang/reflect/ParameterizedType;

    if-eqz v4, :cond_42

    check-cast v3, Ljava/lang/reflect/ParameterizedType;

    const/4 v4, 0x0

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v4

    if-ne v10, v4, :cond_41

    const/4 v4, 0x1

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v3

    iget-object v9, v2, Lf0/y$a;->a:Lf0/b0;

    invoke-virtual {v9, v3, v15}, Lf0/b0;->e(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v3

    iput-boolean v4, v2, Lf0/y$a;->f:Z

    new-instance v4, Lf0/v$c;

    iget-object v9, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    check-cast v8, Lf0/i0/d;

    invoke-interface {v8}, Lf0/i0/d;->encoded()Z

    move-result v8

    invoke-direct {v4, v9, v5, v3, v8}, Lf0/v$c;-><init>(Ljava/lang/reflect/Method;ILf0/h;Z)V

    goto/16 :goto_b

    :cond_41
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const-string v1, "@FieldMap keys must be of type String: "

    invoke-static {v1, v4}, Lf/e/c/a/a;->t(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_42
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v13, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_43
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@FieldMap parameter type must be Map."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_44
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@FieldMap parameters can only be used with form encoding."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_45
    instance-of v3, v8, Lf0/i0/q;

    if-eqz v3, :cond_54

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v3, v2, Lf0/y$a;->q:Z

    if-eqz v3, :cond_53

    check-cast v8, Lf0/i0/q;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lf0/y$a;->g:Z

    invoke-interface {v8}, Lf0/i0/q;->value()Ljava/lang/String;

    move-result-object v3

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_4c

    const-class v3, Ljava/lang/Iterable;

    invoke-virtual {v3, v10}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    const-string v8, "@Part annotation must supply a name or use MultipartBody.Part parameter type."

    if-eqz v3, :cond_48

    instance-of v3, v14, Ljava/lang/reflect/ParameterizedType;

    if-eqz v3, :cond_47

    move-object v3, v14

    check-cast v3, Ljava/lang/reflect/ParameterizedType;

    const/4 v4, 0x0

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v3}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_46

    new-instance v3, Lf0/t;

    invoke-direct {v3, v9}, Lf0/t;-><init>(Lf0/v;)V

    goto/16 :goto_9

    :cond_46
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v5, v8, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_47
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10, v1, v12, v4}, Lf/e/c/a/a;->i(Ljava/lang/Class;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_48
    invoke-virtual {v10}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_4a

    invoke-virtual {v10}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_49

    new-instance v3, Lf0/u;

    invoke-direct {v3, v9}, Lf0/u;-><init>(Lf0/v;)V

    goto/16 :goto_9

    :cond_49
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v5, v8, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_4a
    const/4 v3, 0x0

    invoke-virtual {v11, v10}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_4b

    goto/16 :goto_e

    :cond_4b
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v5, v8, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_4c
    const/4 v9, 0x0

    const/4 v13, 0x4

    new-array v13, v13, [Ljava/lang/String;

    const-string v22, "Content-Disposition"

    aput-object v22, v13, v9

    const-string v9, "form-data; name=\""

    const-string v1, "\""

    invoke-static {v9, v3, v1}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v13, v3

    const-string v1, "Content-Transfer-Encoding"

    const/4 v3, 0x2

    aput-object v1, v13, v3

    const/4 v1, 0x3

    invoke-interface {v8}, Lf0/i0/q;->encoding()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v13, v1

    sget-object v1, Lokhttp3/Headers;->e:Lokhttp3/Headers$b;

    invoke-virtual {v1, v13}, Lokhttp3/Headers$b;->c([Ljava/lang/String;)Lokhttp3/Headers;

    move-result-object v1

    const-class v3, Ljava/lang/Iterable;

    invoke-virtual {v3, v10}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    const-string v8, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation."

    if-eqz v3, :cond_4f

    instance-of v3, v14, Ljava/lang/reflect/ParameterizedType;

    if-eqz v3, :cond_4e

    move-object v3, v14

    check-cast v3, Ljava/lang/reflect/ParameterizedType;

    const/4 v4, 0x0

    invoke-static {v4, v3}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-static {v3}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v11, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_4d

    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    iget-object v8, v2, Lf0/y$a;->c:[Ljava/lang/annotation/Annotation;

    invoke-virtual {v4, v3, v15, v8}, Lf0/b0;->c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v3

    new-instance v4, Lf0/v$g;

    iget-object v8, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v4, v8, v5, v1, v3}, Lf0/v$g;-><init>(Ljava/lang/reflect/Method;ILokhttp3/Headers;Lf0/h;)V

    new-instance v9, Lf0/t;

    invoke-direct {v9, v4}, Lf0/t;-><init>(Lf0/v;)V

    goto/16 :goto_e

    :cond_4d
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v5, v8, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_4e
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10, v1, v12, v4}, Lf/e/c/a/a;->i(Ljava/lang/Class;Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_4f
    invoke-virtual {v10}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_51

    invoke-virtual {v10}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lf0/y$a;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_50

    iget-object v4, v2, Lf0/y$a;->a:Lf0/b0;

    iget-object v8, v2, Lf0/y$a;->c:[Ljava/lang/annotation/Annotation;

    invoke-virtual {v4, v3, v15, v8}, Lf0/b0;->c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v3

    new-instance v4, Lf0/v$g;

    iget-object v8, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v4, v8, v5, v1, v3}, Lf0/v$g;-><init>(Ljava/lang/reflect/Method;ILokhttp3/Headers;Lf0/h;)V

    new-instance v9, Lf0/u;

    invoke-direct {v9, v4}, Lf0/u;-><init>(Lf0/v;)V

    goto/16 :goto_e

    :cond_50
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v5, v8, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_51
    invoke-virtual {v11, v10}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_52

    iget-object v3, v2, Lf0/y$a;->a:Lf0/b0;

    iget-object v4, v2, Lf0/y$a;->c:[Ljava/lang/annotation/Annotation;

    invoke-virtual {v3, v14, v15, v4}, Lf0/b0;->c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v3

    new-instance v4, Lf0/v$g;

    iget-object v8, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v4, v8, v5, v1, v3}, Lf0/v$g;-><init>(Ljava/lang/reflect/Method;ILokhttp3/Headers;Lf0/h;)V

    goto/16 :goto_b

    :cond_52
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v5, v8, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_53
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@Part parameters can only be used with multipart encoding."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_54
    instance-of v1, v8, Lf0/i0/r;

    if-eqz v1, :cond_5a

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v1, v2, Lf0/y$a;->q:Z

    if-eqz v1, :cond_59

    const/4 v1, 0x1

    iput-boolean v1, v2, Lf0/y$a;->g:Z

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    const-class v3, Ljava/util/Map;

    invoke-virtual {v3, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_58

    const-class v3, Ljava/util/Map;

    invoke-static {v14, v1, v3}, Lf0/f0;->g(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v1

    instance-of v3, v1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v3, :cond_57

    check-cast v1, Ljava/lang/reflect/ParameterizedType;

    const/4 v3, 0x0

    invoke-static {v3, v1}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v3

    if-ne v10, v3, :cond_56

    const/4 v3, 0x1

    invoke-static {v3, v1}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_55

    iget-object v3, v2, Lf0/y$a;->a:Lf0/b0;

    iget-object v4, v2, Lf0/y$a;->c:[Ljava/lang/annotation/Annotation;

    invoke-virtual {v3, v1, v15, v4}, Lf0/b0;->c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v1

    check-cast v8, Lf0/i0/r;

    new-instance v3, Lf0/v$h;

    iget-object v4, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-interface {v8}, Lf0/i0/r;->encoding()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v4, v5, v1, v8}, Lf0/v$h;-><init>(Ljava/lang/reflect/Method;ILf0/h;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_55
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead."

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_56
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const-string v2, "@PartMap keys must be of type String: "

    invoke-static {v2, v3}, Lf/e/c/a/a;->t(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_57
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v5, v13, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_58
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@PartMap parameter type must be Map."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_59
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@PartMap parameters can only be used with multipart encoding."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5a
    instance-of v1, v8, Lf0/i0/a;

    if-eqz v1, :cond_5d

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    iget-boolean v1, v2, Lf0/y$a;->p:Z

    if-nez v1, :cond_5c

    iget-boolean v1, v2, Lf0/y$a;->q:Z

    if-nez v1, :cond_5c

    iget-boolean v1, v2, Lf0/y$a;->h:Z

    if-nez v1, :cond_5b

    :try_start_1
    iget-object v1, v2, Lf0/y$a;->a:Lf0/b0;

    iget-object v3, v2, Lf0/y$a;->c:[Ljava/lang/annotation/Annotation;

    invoke-virtual {v1, v14, v15, v3}, Lf0/b0;->c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v3, 0x1

    iput-boolean v3, v2, Lf0/y$a;->h:Z

    new-instance v9, Lf0/v$a;

    iget-object v3, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    invoke-direct {v9, v3, v5, v1}, Lf0/v$a;-><init>(Ljava/lang/reflect/Method;ILf0/h;)V

    goto/16 :goto_e

    :catch_1
    move-exception v0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v14, v2, v3

    const-string v3, "Unable to create @Body converter for %s"

    invoke-static {v1, v0, v5, v3, v2}, Lf0/f0;->m(Ljava/lang/reflect/Method;Ljava/lang/Throwable;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5b
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "Multiple @Body method annotations found."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5c
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "@Body parameters cannot be used with form or multi-part encoding."

    invoke-static {v1, v5, v2, v0}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5d
    instance-of v1, v8, Lf0/i0/x;

    if-eqz v1, :cond_61

    invoke-virtual {v2, v5, v14}, Lf0/y$a;->c(ILjava/lang/reflect/Type;)V

    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    add-int/lit8 v3, v5, -0x1

    :goto_c
    if-ltz v3, :cond_60

    iget-object v4, v2, Lf0/y$a;->v:[Lf0/v;

    aget-object v4, v4, v3

    instance-of v8, v4, Lf0/v$o;

    if-eqz v8, :cond_5f

    check-cast v4, Lf0/v$o;

    iget-object v4, v4, Lf0/v$o;->a:Ljava/lang/Class;

    invoke-virtual {v4, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5e

    goto :goto_d

    :cond_5e
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const-string v2, "@Tag type "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " is duplicate of parameter #"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " and would always overwrite its value."

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v5, v1, v2}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5f
    :goto_d
    add-int/lit8 v3, v3, -0x1

    goto :goto_c

    :cond_60
    new-instance v9, Lf0/v$o;

    invoke-direct {v9, v1}, Lf0/v$o;-><init>(Ljava/lang/Class;)V

    goto :goto_e

    :cond_61
    const/4 v9, 0x0

    :goto_e
    if-nez v9, :cond_62

    goto :goto_f

    :cond_62
    if-nez v17, :cond_63

    move-object/from16 v17, v9

    :goto_f
    add-int/lit8 v12, v20, 0x1

    move-object/from16 v1, p1

    move/from16 v3, v18

    move/from16 v4, v19

    move/from16 v13, v21

    goto/16 :goto_7

    :cond_63
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Multiple Retrofit annotations found, only one allowed."

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_64
    move/from16 v18, v3

    move/from16 v19, v4

    goto :goto_10

    :cond_65
    move/from16 v18, v3

    move/from16 v19, v4

    const/16 v17, 0x0

    :goto_10
    if-nez v17, :cond_67

    if-eqz v16, :cond_66

    :try_start_2
    invoke-static {v14}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    const-class v3, Lkotlin/coroutines/Continuation;

    if-ne v1, v3, :cond_66

    const/4 v1, 0x1

    iput-boolean v1, v2, Lf0/y$a;->w:Z
    :try_end_2
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v17, 0x0

    goto :goto_11

    :catch_2
    :cond_66
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "No Retrofit annotation found."

    invoke-static {v0, v5, v2, v1}, Lf0/f0;->l(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_67
    :goto_11
    aput-object v17, v6, v5

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v1, p1

    move/from16 v3, v18

    move/from16 v4, v19

    goto/16 :goto_5

    :cond_68
    iget-object v1, v2, Lf0/y$a;->r:Ljava/lang/String;

    if-nez v1, :cond_6a

    iget-boolean v1, v2, Lf0/y$a;->m:Z

    if-eqz v1, :cond_69

    goto :goto_12

    :cond_69
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, v2, Lf0/y$a;->n:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Missing either @%s URL or @Url parameter."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_6a
    :goto_12
    iget-boolean v1, v2, Lf0/y$a;->p:Z

    if-nez v1, :cond_6c

    iget-boolean v3, v2, Lf0/y$a;->q:Z

    if-nez v3, :cond_6c

    iget-boolean v3, v2, Lf0/y$a;->o:Z

    if-nez v3, :cond_6c

    iget-boolean v3, v2, Lf0/y$a;->h:Z

    if-nez v3, :cond_6b

    goto :goto_13

    :cond_6b
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Non-body HTTP method cannot contain @Body."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_6c
    :goto_13
    if-eqz v1, :cond_6e

    iget-boolean v1, v2, Lf0/y$a;->f:Z

    if-eqz v1, :cond_6d

    goto :goto_14

    :cond_6d
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Form-encoded method must contain at least one @Field."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_6e
    :goto_14
    const/4 v1, 0x0

    iget-boolean v3, v2, Lf0/y$a;->q:Z

    if-eqz v3, :cond_70

    iget-boolean v3, v2, Lf0/y$a;->g:Z

    if-eqz v3, :cond_6f

    goto :goto_15

    :cond_6f
    iget-object v0, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Multipart method must contain at least one @Part."

    invoke-static {v0, v2, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_70
    :goto_15
    new-instance v3, Lf0/y;

    invoke-direct {v3, v2}, Lf0/y;-><init>(Lf0/y$a;)V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lf0/f0;->h(Ljava/lang/reflect/Type;)Z

    move-result v2

    if-nez v2, :cond_7c

    sget-object v2, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    if-eq v1, v2, :cond_7b

    const-class v1, Lf0/z;

    iget-boolean v2, v3, Lf0/y;->k:Z

    invoke-virtual/range {p1 .. p1}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v4

    if-eqz v2, :cond_74

    invoke-virtual/range {p1 .. p1}, Ljava/lang/reflect/Method;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v5

    array-length v6, v5

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    check-cast v5, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v5}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    instance-of v8, v5, Ljava/lang/reflect/WildcardType;

    if-eqz v8, :cond_71

    check-cast v5, Ljava/lang/reflect/WildcardType;

    invoke-interface {v5}, Ljava/lang/reflect/WildcardType;->getLowerBounds()[Ljava/lang/reflect/Type;

    move-result-object v5

    aget-object v5, v5, v6

    :cond_71
    invoke-static {v5}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v8

    if-ne v8, v1, :cond_72

    instance-of v8, v5, Ljava/lang/reflect/ParameterizedType;

    if-eqz v8, :cond_72

    check-cast v5, Ljava/lang/reflect/ParameterizedType;

    invoke-static {v6, v5}, Lf0/f0;->e(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object v5

    const/4 v8, 0x1

    goto :goto_16

    :cond_72
    const/4 v8, 0x0

    :goto_16
    new-instance v9, Lf0/f0$b;

    const-class v10, Lf0/d;

    const/4 v11, 0x1

    new-array v12, v11, [Ljava/lang/reflect/Type;

    aput-object v5, v12, v6

    const/4 v5, 0x0

    invoke-direct {v9, v5, v10, v12}, Lf0/f0$b;-><init>(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V

    const-class v5, Lf0/d0;

    invoke-static {v4, v5}, Lf0/f0;->i([Ljava/lang/annotation/Annotation;Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_73

    goto :goto_17

    :cond_73
    array-length v5, v4

    add-int/2addr v5, v11

    new-array v5, v5, [Ljava/lang/annotation/Annotation;

    sget-object v10, Lf0/e0;->a:Lf0/d0;

    aput-object v10, v5, v6

    array-length v10, v4

    invoke-static {v4, v6, v5, v11, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v4, v5

    goto :goto_17

    :cond_74
    invoke-virtual/range {p1 .. p1}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v9

    const/4 v8, 0x0

    :goto_17
    :try_start_3
    invoke-virtual {v0, v9, v4}, Lf0/b0;->a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/e;

    move-result-object v5
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4

    invoke-interface {v5}, Lf0/e;->a()Ljava/lang/reflect/Type;

    move-result-object v4

    const-class v6, Lokhttp3/Response;

    if-eq v4, v6, :cond_7a

    if-eq v4, v1, :cond_79

    iget-object v1, v3, Lf0/y;->c:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_76

    const-class v1, Ljava/lang/Void;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_75

    goto :goto_18

    :cond_75
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "HEAD method must use Void as response type."

    move-object/from16 v6, p1

    invoke-static {v6, v1, v0}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_76
    :goto_18
    move-object/from16 v6, p1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    :try_start_4
    invoke-virtual {v0, v4, v1}, Lf0/b0;->d(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lf0/h;

    move-result-object v4
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    iget-object v0, v0, Lf0/b0;->b:Lb0/e$a;

    if-nez v2, :cond_77

    new-instance v1, Lf0/j$a;

    invoke-direct {v1, v3, v0, v4, v5}, Lf0/j$a;-><init>(Lf0/y;Lb0/e$a;Lf0/h;Lf0/e;)V

    goto :goto_19

    :cond_77
    if-eqz v8, :cond_78

    new-instance v1, Lf0/j$c;

    invoke-direct {v1, v3, v0, v4, v5}, Lf0/j$c;-><init>(Lf0/y;Lb0/e$a;Lf0/h;Lf0/e;)V

    goto :goto_19

    :cond_78
    new-instance v7, Lf0/j$b;

    const/4 v6, 0x0

    move-object v1, v7

    move-object v2, v3

    move-object v3, v0

    invoke-direct/range {v1 .. v6}, Lf0/j$b;-><init>(Lf0/y;Lb0/e$a;Lf0/h;Lf0/e;Z)V

    :goto_19
    return-object v1

    :catch_3
    move-exception v0

    move-object v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v0, v2

    const-string v2, "Unable to create converter for %s"

    invoke-static {v6, v1, v2, v0}, Lf0/f0;->k(Ljava/lang/reflect/Method;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_79
    move-object/from16 v6, p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Response must include generic type (e.g., Response<String>)"

    invoke-static {v6, v1, v0}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_7a
    move-object/from16 v6, p1

    const-string v0, "\'"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Lf0/f0;->f(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' is not a valid response body type. Did you mean ResponseBody?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6, v0, v1}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :catch_4
    move-exception v0

    move-object/from16 v6, p1

    move-object v1, v0

    const/4 v0, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v9, v2, v0

    const-string v0, "Unable to create call adapter for %s"

    invoke-static {v6, v1, v0, v2}, Lf0/f0;->k(Ljava/lang/reflect/Method;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_7b
    move-object/from16 v6, p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Service methods cannot return void."

    invoke-static {v6, v1, v0}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_7c
    move-object/from16 v6, p1

    const/4 v0, 0x1

    const/4 v2, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v1, v0, v2

    const-string v1, "Method return type must not include a type variable or wildcard: %s"

    invoke-static {v6, v1, v0}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_7d
    const/4 v0, 0x0

    iget-object v1, v2, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "HTTP method annotation is required (e.g., @GET, @POST, etc.)."

    invoke-static {v1, v2, v0}, Lf0/f0;->j(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public abstract a([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation
.end method
