.class public final Lf0/i$b;
.super Ljava/lang/Object;
.source "DefaultCallAdapterFactory.java"

# interfaces
.implements Lf0/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf0/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf0/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Ljava/util/concurrent/Executor;

.field public final e:Lf0/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf0/d<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lf0/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lf0/d<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf0/i$b;->d:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lf0/i$b;->e:Lf0/d;

    return-void
.end method


# virtual methods
.method public c()Lb0/a0;
    .locals 1

    iget-object v0, p0, Lf0/i$b;->e:Lf0/d;

    invoke-interface {v0}, Lf0/d;->c()Lb0/a0;

    move-result-object v0

    return-object v0
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lf0/i$b;->e:Lf0/d;

    invoke-interface {v0}, Lf0/d;->cancel()V

    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lf0/i$b;->y()Lf0/d;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lf0/i$b;->e:Lf0/d;

    invoke-interface {v0}, Lf0/d;->d()Z

    move-result v0

    return v0
.end method

.method public execute()Lf0/z;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf0/z<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf0/i$b;->e:Lf0/d;

    invoke-interface {v0}, Lf0/d;->execute()Lf0/z;

    move-result-object v0

    return-object v0
.end method

.method public v(Lf0/f;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/f<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lf0/i$b;->e:Lf0/d;

    new-instance v1, Lf0/i$b$a;

    invoke-direct {v1, p0, p1}, Lf0/i$b$a;-><init>(Lf0/i$b;Lf0/f;)V

    invoke-interface {v0, v1}, Lf0/d;->v(Lf0/f;)V

    return-void
.end method

.method public y()Lf0/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf0/d<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lf0/i$b;

    iget-object v1, p0, Lf0/i$b;->d:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lf0/i$b;->e:Lf0/d;

    invoke-interface {v2}, Lf0/d;->y()Lf0/d;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lf0/i$b;-><init>(Ljava/util/concurrent/Executor;Lf0/d;)V

    return-object v0
.end method
