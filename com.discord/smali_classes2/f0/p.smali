.class public final Lf0/p;
.super Ljava/lang/Object;
.source "KotlinExtensions.kt"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic d:Lkotlin/coroutines/Continuation;

.field public final synthetic e:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Lkotlin/coroutines/Continuation;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lf0/p;->d:Lkotlin/coroutines/Continuation;

    iput-object p2, p0, Lf0/p;->e:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    iget-object v0, p0, Lf0/p;->d:Lkotlin/coroutines/Continuation;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->intercepted(Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object v0

    iget-object v1, p0, Lf0/p;->e:Ljava/lang/Exception;

    invoke-static {v1}, Lf/h/a/f/f/n/g;->createFailure(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/coroutines/Continuation;->resumeWith(Ljava/lang/Object;)V

    return-void
.end method
