.class public final Lf0/r;
.super Ljava/lang/Object;
.source "OkHttpCall.java"

# interfaces
.implements Lf0/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf0/r$c;,
        Lf0/r$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lf0/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final d:Lf0/y;

.field public final e:[Ljava/lang/Object;

.field public final f:Lb0/e$a;

.field public final g:Lf0/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf0/h<",
            "Lokhttp3/ResponseBody;",
            "TT;>;"
        }
    .end annotation
.end field

.field public volatile h:Z

.field public i:Lb0/e;

.field public j:Ljava/lang/Throwable;

.field public k:Z


# direct methods
.method public constructor <init>(Lf0/y;[Ljava/lang/Object;Lb0/e$a;Lf0/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/y;",
            "[",
            "Ljava/lang/Object;",
            "Lb0/e$a;",
            "Lf0/h<",
            "Lokhttp3/ResponseBody;",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf0/r;->d:Lf0/y;

    iput-object p2, p0, Lf0/r;->e:[Ljava/lang/Object;

    iput-object p3, p0, Lf0/r;->f:Lb0/e$a;

    iput-object p4, p0, Lf0/r;->g:Lf0/h;

    return-void
.end method


# virtual methods
.method public final b()Lb0/e;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf0/r;->f:Lb0/e$a;

    iget-object v1, p0, Lf0/r;->d:Lf0/y;

    iget-object v2, p0, Lf0/r;->e:[Ljava/lang/Object;

    iget-object v3, v1, Lf0/y;->j:[Lf0/v;

    array-length v4, v2

    array-length v5, v3

    if-ne v4, v5, :cond_a

    new-instance v5, Lf0/x;

    iget-object v7, v1, Lf0/y;->c:Ljava/lang/String;

    iget-object v8, v1, Lf0/y;->b:Lb0/x;

    iget-object v9, v1, Lf0/y;->d:Ljava/lang/String;

    iget-object v10, v1, Lf0/y;->e:Lokhttp3/Headers;

    iget-object v11, v1, Lf0/y;->f:Lokhttp3/MediaType;

    iget-boolean v12, v1, Lf0/y;->g:Z

    iget-boolean v13, v1, Lf0/y;->h:Z

    iget-boolean v14, v1, Lf0/y;->i:Z

    move-object v6, v5

    invoke-direct/range {v6 .. v14}, Lf0/x;-><init>(Ljava/lang/String;Lb0/x;Ljava/lang/String;Lokhttp3/Headers;Lokhttp3/MediaType;ZZZ)V

    iget-boolean v6, v1, Lf0/y;->k:Z

    if-eqz v6, :cond_0

    add-int/lit8 v4, v4, -0x1

    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v4, :cond_1

    aget-object v9, v2, v8

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    aget-object v9, v3, v8

    aget-object v10, v2, v8

    invoke-virtual {v9, v5, v10}, Lf0/v;->a(Lf0/x;Ljava/lang/Object;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, v5, Lf0/x;->d:Lb0/x$a;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lb0/x$a;->b()Lb0/x;

    move-result-object v2

    goto :goto_2

    :cond_2
    iget-object v2, v5, Lf0/x;->b:Lb0/x;

    iget-object v4, v5, Lf0/x;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "link"

    invoke-static {v4, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lb0/x;->g(Ljava/lang/String;)Lb0/x$a;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lb0/x$a;->b()Lb0/x;

    move-result-object v2

    goto :goto_1

    :cond_3
    move-object v2, v3

    :goto_1
    if-eqz v2, :cond_9

    :goto_2
    iget-object v4, v5, Lf0/x;->k:Lokhttp3/RequestBody;

    if-nez v4, :cond_6

    iget-object v8, v5, Lf0/x;->j:Lb0/u$a;

    if-eqz v8, :cond_4

    new-instance v4, Lb0/u;

    iget-object v3, v8, Lb0/u$a;->a:Ljava/util/List;

    iget-object v7, v8, Lb0/u$a;->b:Ljava/util/List;

    invoke-direct {v4, v3, v7}, Lb0/u;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto :goto_3

    :cond_4
    iget-object v8, v5, Lf0/x;->i:Lokhttp3/MultipartBody$a;

    if-eqz v8, :cond_5

    invoke-virtual {v8}, Lokhttp3/MultipartBody$a;->b()Lokhttp3/MultipartBody;

    move-result-object v4

    goto :goto_3

    :cond_5
    iget-boolean v8, v5, Lf0/x;->h:Z

    if-eqz v8, :cond_6

    new-array v4, v7, [B

    invoke-static {v3, v4}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;[B)Lokhttp3/RequestBody;

    move-result-object v4

    :cond_6
    :goto_3
    iget-object v3, v5, Lf0/x;->g:Lokhttp3/MediaType;

    if-eqz v3, :cond_8

    if-eqz v4, :cond_7

    new-instance v7, Lf0/x$a;

    invoke-direct {v7, v4, v3}, Lf0/x$a;-><init>(Lokhttp3/RequestBody;Lokhttp3/MediaType;)V

    move-object v4, v7

    goto :goto_4

    :cond_7
    iget-object v7, v5, Lf0/x;->f:Lokhttp3/Headers$a;

    iget-object v3, v3, Lokhttp3/MediaType;->a:Ljava/lang/String;

    const-string v8, "Content-Type"

    invoke-virtual {v7, v8, v3}, Lokhttp3/Headers$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    :cond_8
    :goto_4
    iget-object v3, v5, Lf0/x;->e:Lb0/a0$a;

    invoke-virtual {v3, v2}, Lb0/a0$a;->g(Lb0/x;)Lb0/a0$a;

    iget-object v2, v5, Lf0/x;->f:Lokhttp3/Headers$a;

    invoke-virtual {v2}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v2

    const-string v7, "headers"

    invoke-static {v2, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v2

    iput-object v2, v3, Lb0/a0$a;->c:Lokhttp3/Headers$a;

    iget-object v2, v5, Lf0/x;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lb0/a0$a;->c(Ljava/lang/String;Lokhttp3/RequestBody;)Lb0/a0$a;

    const-class v2, Lf0/k;

    new-instance v4, Lf0/k;

    iget-object v1, v1, Lf0/y;->a:Ljava/lang/reflect/Method;

    invoke-direct {v4, v1, v6}, Lf0/k;-><init>(Ljava/lang/reflect/Method;Ljava/util/List;)V

    invoke-virtual {v3, v2, v4}, Lb0/a0$a;->e(Ljava/lang/Class;Ljava/lang/Object;)Lb0/a0$a;

    invoke-virtual {v3}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object v1

    invoke-interface {v0, v1}, Lb0/e$a;->b(Lb0/a0;)Lb0/e;

    move-result-object v0

    const-string v1, "Call.Factory returned null."

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-object v0

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Malformed URL. Base: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v5, Lf0/x;->b:Lb0/x;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", Relative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v5, Lf0/x;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument count ("

    const-string v2, ") doesn\'t match expected count ("

    invoke-static {v1, v4, v2}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v3

    const-string v3, ")"

    invoke-static {v1, v2, v3}, Lf/e/c/a/a;->u(Ljava/lang/StringBuilder;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized c()Lb0/a0;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lf0/r;->e()Lb0/e;

    move-result-object v0

    invoke-interface {v0}, Lb0/e;->c()Lb0/a0;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to create request."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    throw v0
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf0/r;->h:Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf0/r;->i:Lb0/e;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lb0/e;->cancel()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    new-instance v0, Lf0/r;

    iget-object v1, p0, Lf0/r;->d:Lf0/y;

    iget-object v2, p0, Lf0/r;->e:[Ljava/lang/Object;

    iget-object v3, p0, Lf0/r;->f:Lb0/e$a;

    iget-object v4, p0, Lf0/r;->g:Lf0/h;

    invoke-direct {v0, v1, v2, v3, v4}, Lf0/r;-><init>(Lf0/y;[Ljava/lang/Object;Lb0/e$a;Lf0/h;)V

    return-object v0
.end method

.method public d()Z
    .locals 2

    iget-boolean v0, p0, Lf0/r;->h:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lf0/r;->i:Lb0/e;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lb0/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()Lb0/e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lf0/r;->i:Lb0/e;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lf0/r;->j:Ljava/lang/Throwable;

    if-eqz v0, :cond_3

    instance-of v1, v0, Ljava/io/IOException;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/Error;

    throw v0

    :cond_2
    check-cast v0, Ljava/io/IOException;

    throw v0

    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lf0/r;->b()Lb0/e;

    move-result-object v0

    iput-object v0, p0, Lf0/r;->i:Lb0/e;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    :goto_0
    invoke-static {v0}, Lf0/f0;->o(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lf0/r;->j:Ljava/lang/Throwable;

    throw v0
.end method

.method public execute()Lf0/z;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lf0/z<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf0/r;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf0/r;->k:Z

    invoke-virtual {p0}, Lf0/r;->e()Lb0/e;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v1, p0, Lf0/r;->h:Z

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lb0/e;->cancel()V

    :cond_0
    invoke-interface {v0}, Lb0/e;->execute()Lokhttp3/Response;

    move-result-object v0

    invoke-virtual {p0, v0}, Lf0/r;->f(Lokhttp3/Response;)Lf0/z;

    move-result-object v0

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already executed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public f(Lokhttp3/Response;)Lf0/z;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/Response;",
            ")",
            "Lf0/z<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p1

    iget-object v1, v0, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    const-string v2, "response"

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v0, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v5, v0, Lokhttp3/Response;->e:Lb0/z;

    iget v7, v0, Lokhttp3/Response;->g:I

    iget-object v6, v0, Lokhttp3/Response;->f:Ljava/lang/String;

    iget-object v8, v0, Lokhttp3/Response;->h:Lb0/w;

    iget-object v2, v0, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v2}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v2

    iget-object v11, v0, Lokhttp3/Response;->k:Lokhttp3/Response;

    iget-object v12, v0, Lokhttp3/Response;->l:Lokhttp3/Response;

    iget-object v13, v0, Lokhttp3/Response;->m:Lokhttp3/Response;

    iget-wide v14, v0, Lokhttp3/Response;->n:J

    iget-wide v9, v0, Lokhttp3/Response;->o:J

    iget-object v0, v0, Lokhttp3/Response;->p:Lb0/g0/g/c;

    new-instance v3, Lf0/r$c;

    move-wide/from16 v16, v9

    invoke-virtual {v1}, Lokhttp3/ResponseBody;->b()Lokhttp3/MediaType;

    move-result-object v9

    move-wide/from16 v19, v14

    invoke-virtual {v1}, Lokhttp3/ResponseBody;->a()J

    move-result-wide v14

    invoke-direct {v3, v9, v14, v15}, Lf0/r$c;-><init>(Lokhttp3/MediaType;J)V

    if-ltz v7, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    :goto_0
    if-eqz v9, :cond_a

    if-eqz v4, :cond_9

    if-eqz v5, :cond_8

    if-eqz v6, :cond_7

    invoke-virtual {v2}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v9

    new-instance v2, Lokhttp3/Response;

    move-object v10, v3

    move-object v3, v2

    move-wide/from16 v14, v19

    move-object/from16 v18, v0

    invoke-direct/range {v3 .. v18}, Lokhttp3/Response;-><init>(Lb0/a0;Lb0/z;Ljava/lang/String;ILb0/w;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLb0/g0/g/c;)V

    iget v0, v2, Lokhttp3/Response;->g:I

    const/16 v3, 0xc8

    const/4 v4, 0x0

    if-lt v0, v3, :cond_5

    const/16 v3, 0x12c

    if-lt v0, v3, :cond_1

    goto :goto_2

    :cond_1
    const/16 v3, 0xcc

    if-eq v0, v3, :cond_4

    const/16 v3, 0xcd

    if-ne v0, v3, :cond_2

    goto :goto_1

    :cond_2
    new-instance v3, Lf0/r$b;

    invoke-direct {v3, v1}, Lf0/r$b;-><init>(Lokhttp3/ResponseBody;)V

    move-object/from16 v5, p0

    :try_start_0
    iget-object v0, v5, Lf0/r;->g:Lf0/h;

    invoke-interface {v0, v3}, Lf0/h;->convert(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, v2}, Lf0/z;->b(Ljava/lang/Object;Lokhttp3/Response;)Lf0/z;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, v3, Lf0/r$b;->h:Ljava/io/IOException;

    if-nez v1, :cond_3

    throw v0

    :cond_3
    throw v1

    :cond_4
    :goto_1
    move-object/from16 v5, p0

    invoke-virtual {v1}, Lokhttp3/ResponseBody;->close()V

    invoke-static {v4, v2}, Lf0/z;->b(Ljava/lang/Object;Lokhttp3/Response;)Lf0/z;

    move-result-object v0

    return-object v0

    :cond_5
    :goto_2
    move-object/from16 v5, p0

    :try_start_1
    invoke-static {v1}, Lf0/f0;->a(Lokhttp3/ResponseBody;)Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v2}, Lokhttp3/Response;->b()Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v3, Lf0/z;

    invoke-direct {v3, v2, v4, v0}, Lf0/z;-><init>(Lokhttp3/Response;Ljava/lang/Object;Lokhttp3/ResponseBody;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lokhttp3/ResponseBody;->close()V

    return-object v3

    :cond_6
    :try_start_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "rawResponse should not be successful response"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lokhttp3/ResponseBody;->close()V

    throw v0

    :cond_7
    move-object/from16 v5, p0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "message == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move-object/from16 v5, p0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "protocol == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move-object/from16 v5, p0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    move-object/from16 v5, p0

    const-string v0, "code < 0: "

    invoke-static {v0, v7}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public v(Lf0/f;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/f<",
            "TT;>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lf0/r;->k:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf0/r;->k:Z

    iget-object v0, p0, Lf0/r;->i:Lb0/e;

    iget-object v1, p0, Lf0/r;->j:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Lf0/r;->b()Lb0/e;

    move-result-object v2

    iput-object v2, p0, Lf0/r;->i:Lb0/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    invoke-static {v1}, Lf0/f0;->o(Ljava/lang/Throwable;)V

    iput-object v1, p0, Lf0/r;->j:Ljava/lang/Throwable;

    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_1

    invoke-interface {p1, p0, v1}, Lf0/f;->a(Lf0/d;Ljava/lang/Throwable;)V

    return-void

    :cond_1
    iget-boolean v1, p0, Lf0/r;->h:Z

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lb0/e;->cancel()V

    :cond_2
    new-instance v1, Lf0/r$a;

    invoke-direct {v1, p0, p1}, Lf0/r$a;-><init>(Lf0/r;Lf0/f;)V

    invoke-interface {v0, v1}, Lb0/e;->t(Lb0/f;)V

    return-void

    :cond_3
    :try_start_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Already executed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public y()Lf0/d;
    .locals 5

    new-instance v0, Lf0/r;

    iget-object v1, p0, Lf0/r;->d:Lf0/y;

    iget-object v2, p0, Lf0/r;->e:[Ljava/lang/Object;

    iget-object v3, p0, Lf0/r;->f:Lb0/e$a;

    iget-object v4, p0, Lf0/r;->g:Lf0/h;

    invoke-direct {v0, v1, v2, v3, v4}, Lf0/r;-><init>(Lf0/y;[Ljava/lang/Object;Lb0/e$a;Lf0/h;)V

    return-object v0
.end method
