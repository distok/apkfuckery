.class public abstract Lf0/j;
.super Lf0/c0;
.source "HttpServiceMethod.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf0/j$a;,
        Lf0/j$c;,
        Lf0/j$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResponseT:",
        "Ljava/lang/Object;",
        "ReturnT:",
        "Ljava/lang/Object;",
        ">",
        "Lf0/c0<",
        "TReturnT;>;"
    }
.end annotation


# instance fields
.field public final a:Lf0/y;

.field public final b:Lb0/e$a;

.field public final c:Lf0/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lf0/h<",
            "Lokhttp3/ResponseBody;",
            "TResponseT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lf0/y;Lb0/e$a;Lf0/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/y;",
            "Lb0/e$a;",
            "Lf0/h<",
            "Lokhttp3/ResponseBody;",
            "TResponseT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lf0/c0;-><init>()V

    iput-object p1, p0, Lf0/j;->a:Lf0/y;

    iput-object p2, p0, Lf0/j;->b:Lb0/e$a;

    iput-object p3, p0, Lf0/j;->c:Lf0/h;

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")TReturnT;"
        }
    .end annotation

    new-instance v0, Lf0/r;

    iget-object v1, p0, Lf0/j;->a:Lf0/y;

    iget-object v2, p0, Lf0/j;->b:Lb0/e$a;

    iget-object v3, p0, Lf0/j;->c:Lf0/h;

    invoke-direct {v0, v1, p1, v2, v3}, Lf0/r;-><init>(Lf0/y;[Ljava/lang/Object;Lb0/e$a;Lf0/h;)V

    invoke-virtual {p0, v0, p1}, Lf0/j;->c(Lf0/d;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public abstract c(Lf0/d;[Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/d<",
            "TResponseT;>;[",
            "Ljava/lang/Object;",
            ")TReturnT;"
        }
    .end annotation
.end method
