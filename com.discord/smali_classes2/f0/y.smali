.class public final Lf0/y;
.super Ljava/lang/Object;
.source "RequestFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf0/y$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/reflect/Method;

.field public final b:Lb0/x;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lokhttp3/Headers;

.field public final f:Lokhttp3/MediaType;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:[Lf0/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lf0/v<",
            "*>;"
        }
    .end annotation
.end field

.field public final k:Z


# direct methods
.method public constructor <init>(Lf0/y$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lf0/y$a;->b:Ljava/lang/reflect/Method;

    iput-object v0, p0, Lf0/y;->a:Ljava/lang/reflect/Method;

    iget-object v0, p1, Lf0/y$a;->a:Lf0/b0;

    iget-object v0, v0, Lf0/b0;->c:Lb0/x;

    iput-object v0, p0, Lf0/y;->b:Lb0/x;

    iget-object v0, p1, Lf0/y$a;->n:Ljava/lang/String;

    iput-object v0, p0, Lf0/y;->c:Ljava/lang/String;

    iget-object v0, p1, Lf0/y$a;->r:Ljava/lang/String;

    iput-object v0, p0, Lf0/y;->d:Ljava/lang/String;

    iget-object v0, p1, Lf0/y$a;->s:Lokhttp3/Headers;

    iput-object v0, p0, Lf0/y;->e:Lokhttp3/Headers;

    iget-object v0, p1, Lf0/y$a;->t:Lokhttp3/MediaType;

    iput-object v0, p0, Lf0/y;->f:Lokhttp3/MediaType;

    iget-boolean v0, p1, Lf0/y$a;->o:Z

    iput-boolean v0, p0, Lf0/y;->g:Z

    iget-boolean v0, p1, Lf0/y$a;->p:Z

    iput-boolean v0, p0, Lf0/y;->h:Z

    iget-boolean v0, p1, Lf0/y$a;->q:Z

    iput-boolean v0, p0, Lf0/y;->i:Z

    iget-object v0, p1, Lf0/y$a;->v:[Lf0/v;

    iput-object v0, p0, Lf0/y;->j:[Lf0/v;

    iget-boolean p1, p1, Lf0/y$a;->w:Z

    iput-boolean p1, p0, Lf0/y;->k:Z

    return-void
.end method
