.class public Lf0/i$b$a;
.super Ljava/lang/Object;
.source "DefaultCallAdapterFactory.java"

# interfaces
.implements Lf0/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf0/i$b;->v(Lf0/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lf0/f<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lf0/f;

.field public final synthetic b:Lf0/i$b;


# direct methods
.method public constructor <init>(Lf0/i$b;Lf0/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lf0/i$b$a;->b:Lf0/i$b;

    iput-object p2, p0, Lf0/i$b$a;->a:Lf0/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf0/d;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/d<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    iget-object p1, p0, Lf0/i$b$a;->b:Lf0/i$b;

    iget-object p1, p1, Lf0/i$b;->d:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lf0/i$b$a;->a:Lf0/f;

    new-instance v1, Lf0/a;

    invoke-direct {v1, p0, v0, p2}, Lf0/a;-><init>(Lf0/i$b$a;Lf0/f;Ljava/lang/Throwable;)V

    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Lf0/d;Lf0/z;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf0/d<",
            "TT;>;",
            "Lf0/z<",
            "TT;>;)V"
        }
    .end annotation

    iget-object p1, p0, Lf0/i$b$a;->b:Lf0/i$b;

    iget-object p1, p1, Lf0/i$b;->d:Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lf0/i$b$a;->a:Lf0/f;

    new-instance v1, Lf0/b;

    invoke-direct {v1, p0, v0, p2}, Lf0/b;-><init>(Lf0/i$b$a;Lf0/f;Lf0/z;)V

    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
