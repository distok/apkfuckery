.class public final Lb0/g0/h/h;
.super Lokhttp3/ResponseBody;
.source "RealResponseBody.kt"


# instance fields
.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:Lc0/g;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLc0/g;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p4, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lokhttp3/ResponseBody;-><init>()V

    iput-object p1, p0, Lb0/g0/h/h;->f:Ljava/lang/String;

    iput-wide p2, p0, Lb0/g0/h/h;->g:J

    iput-object p4, p0, Lb0/g0/h/h;->h:Lc0/g;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lb0/g0/h/h;->g:J

    return-wide v0
.end method

.method public b()Lokhttp3/MediaType;
    .locals 2

    iget-object v0, p0, Lb0/g0/h/h;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v1, Lokhttp3/MediaType;->g:Lokhttp3/MediaType$a;

    invoke-static {v0}, Lokhttp3/MediaType$a;->b(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public c()Lc0/g;
    .locals 1

    iget-object v0, p0, Lb0/g0/h/h;->h:Lc0/g;

    return-object v0
.end method
