.class public final Lb0/g0/h/b;
.super Ljava/lang/Object;
.source "CallServerInterceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field public final b:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lb0/g0/h/b;->b:Z

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 31
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "ioe"

    const-string v2, "call"

    const-string v3, "chain"

    invoke-static {v0, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lb0/g0/h/g;

    iget-object v3, v0, Lb0/g0/h/g;->e:Lb0/g0/g/c;

    const/4 v4, 0x0

    if-eqz v3, :cond_1d

    iget-object v0, v0, Lb0/g0/h/g;->f:Lb0/a0;

    iget-object v5, v0, Lb0/a0;->e:Lokhttp3/RequestBody;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-string v8, "request"

    invoke-static {v0, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v9, v3, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v10, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v10, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v9, v3, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v9, v0}, Lb0/g0/h/d;->b(Lb0/a0;)V

    iget-object v9, v3, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v10, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v9}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v10, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v8, v0, Lb0/a0;->c:Ljava/lang/String;

    invoke-static {v8}, Lb0/g0/h/f;->a(Ljava/lang/String;)Z

    move-result v8

    const-string v9, "$this$buffer"

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-eqz v8, :cond_3

    if-eqz v5, :cond_3

    const-string v8, "Expect"

    invoke-virtual {v0, v8}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v12, "100-continue"

    invoke-static {v12, v8, v11}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3}, Lb0/g0/g/c;->c()V

    invoke-virtual {v3, v11}, Lb0/g0/g/c;->e(Z)Lokhttp3/Response$a;

    move-result-object v8

    invoke-virtual {v3}, Lb0/g0/g/c;->f()V

    const/4 v12, 0x0

    goto :goto_0

    :cond_0
    const/4 v12, 0x1

    move-object v8, v4

    :goto_0
    if-nez v8, :cond_2

    invoke-virtual {v5}, Lokhttp3/RequestBody;->isDuplex()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-virtual {v3}, Lb0/g0/g/c;->c()V

    invoke-virtual {v3, v0, v11}, Lb0/g0/g/c;->b(Lb0/a0;Z)Lc0/v;

    move-result-object v11

    invoke-static {v11, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v13, Lc0/q;

    invoke-direct {v13, v11}, Lc0/q;-><init>(Lc0/v;)V

    invoke-virtual {v5, v13}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v3, v0, v10}, Lb0/g0/g/c;->b(Lb0/a0;Z)Lc0/v;

    move-result-object v11

    invoke-static {v11, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v13, Lc0/q;

    invoke-direct {v13, v11}, Lc0/q;-><init>(Lc0/v;)V

    invoke-virtual {v5, v13}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    invoke-virtual {v13}, Lc0/q;->close()V

    goto :goto_1

    :cond_2
    iget-object v13, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-virtual {v13, v3, v11, v10, v4}, Lb0/g0/g/e;->j(Lb0/g0/g/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    iget-object v11, v3, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    invoke-virtual {v11}, Lb0/g0/g/j;->j()Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, v3, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v11}, Lb0/g0/h/d;->e()Lb0/g0/g/j;

    move-result-object v11

    invoke-virtual {v11}, Lb0/g0/g/j;->l()V

    goto :goto_1

    :cond_3
    iget-object v8, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-virtual {v8, v3, v11, v10, v4}, Lb0/g0/g/e;->j(Lb0/g0/g/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    const/4 v12, 0x1

    move-object v8, v4

    :cond_4
    :goto_1
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lokhttp3/RequestBody;->isDuplex()Z

    move-result v5

    if-nez v5, :cond_6

    :cond_5
    :try_start_1
    iget-object v5, v3, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v5}, Lb0/g0/h/d;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_6
    if-nez v8, :cond_8

    invoke-virtual {v3, v10}, Lb0/g0/g/c;->e(Z)Lokhttp3/Response$a;

    move-result-object v8

    if-eqz v8, :cond_7

    if-eqz v12, :cond_8

    invoke-virtual {v3}, Lb0/g0/g/c;->f()V

    const/4 v12, 0x0

    goto :goto_2

    :cond_7
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_8
    :goto_2
    invoke-virtual {v8, v0}, Lokhttp3/Response$a;->g(Lb0/a0;)Lokhttp3/Response$a;

    iget-object v5, v3, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    iget-object v5, v5, Lb0/g0/g/j;->d:Lb0/w;

    iput-object v5, v8, Lokhttp3/Response$a;->e:Lb0/w;

    iput-wide v6, v8, Lokhttp3/Response$a;->k:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    iput-wide v13, v8, Lokhttp3/Response$a;->l:J

    invoke-virtual {v8}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v5

    iget v8, v5, Lokhttp3/Response;->g:I

    const/16 v11, 0x64

    if-ne v8, v11, :cond_b

    invoke-virtual {v3, v10}, Lb0/g0/g/c;->e(Z)Lokhttp3/Response$a;

    move-result-object v5

    if-eqz v5, :cond_a

    if-eqz v12, :cond_9

    invoke-virtual {v3}, Lb0/g0/g/c;->f()V

    :cond_9
    invoke-virtual {v5, v0}, Lokhttp3/Response$a;->g(Lb0/a0;)Lokhttp3/Response$a;

    iget-object v0, v3, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    iget-object v0, v0, Lb0/g0/g/j;->d:Lb0/w;

    iput-object v0, v5, Lokhttp3/Response$a;->e:Lb0/w;

    iput-wide v6, v5, Lokhttp3/Response$a;->k:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v5, Lokhttp3/Response$a;->l:J

    invoke-virtual {v5}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v5

    iget v8, v5, Lokhttp3/Response;->g:I

    goto :goto_3

    :cond_a
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_b
    :goto_3
    const-string v0, "response"

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v3, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v6, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v6, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v4, p0

    iget-boolean v6, v4, Lb0/g0/h/b;->b:Z

    const-string v7, "message == null"

    const-string v10, "protocol == null"

    const-string v11, "request == null"

    const-string v12, "code < 0: "

    if-eqz v6, :cond_11

    const/16 v6, 0x65

    if-ne v8, v6, :cond_11

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v14, v5, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v15, v5, Lokhttp3/Response;->e:Lb0/z;

    iget v0, v5, Lokhttp3/Response;->g:I

    iget-object v1, v5, Lokhttp3/Response;->f:Ljava/lang/String;

    iget-object v2, v5, Lokhttp3/Response;->h:Lb0/w;

    iget-object v6, v5, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v6}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v6

    iget-object v9, v5, Lokhttp3/Response;->k:Lokhttp3/Response;

    iget-object v13, v5, Lokhttp3/Response;->l:Lokhttp3/Response;

    iget-object v4, v5, Lokhttp3/Response;->m:Lokhttp3/Response;

    move-object/from16 p1, v11

    move-object/from16 v16, v12

    iget-wide v11, v5, Lokhttp3/Response;->n:J

    move-object/from16 v17, v7

    move/from16 v29, v8

    iget-wide v7, v5, Lokhttp3/Response;->o:J

    iget-object v5, v5, Lokhttp3/Response;->p:Lb0/g0/g/c;

    sget-object v20, Lb0/g0/c;->c:Lokhttp3/ResponseBody;

    if-ltz v0, :cond_c

    const/16 v18, 0x1

    goto :goto_4

    :cond_c
    const/16 v18, 0x0

    :goto_4
    if-eqz v18, :cond_10

    if-eqz v14, :cond_f

    if-eqz v15, :cond_e

    if-eqz v1, :cond_d

    invoke-virtual {v6}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v19

    new-instance v6, Lokhttp3/Response;

    move-object v10, v13

    move-object v13, v6

    move-object/from16 v16, v1

    move/from16 v17, v0

    move-object/from16 v18, v2

    move-object/from16 v21, v9

    move-object/from16 v22, v10

    move-object/from16 v23, v4

    move-wide/from16 v24, v11

    move-wide/from16 v26, v7

    move-object/from16 v28, v5

    invoke-direct/range {v13 .. v28}, Lokhttp3/Response;-><init>(Lb0/a0;Lb0/z;Ljava/lang/String;ILb0/w;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLb0/g0/g/c;)V

    goto/16 :goto_6

    :cond_d
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    move-object/from16 v4, v16

    invoke-static {v4, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_11
    move-object/from16 v17, v7

    move/from16 v29, v8

    move-object/from16 p1, v11

    move-object v4, v12

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v5, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v7, v5, Lokhttp3/Response;->e:Lb0/z;

    iget v8, v5, Lokhttp3/Response;->g:I

    iget-object v11, v5, Lokhttp3/Response;->f:Ljava/lang/String;

    iget-object v12, v5, Lokhttp3/Response;->h:Lb0/w;

    iget-object v13, v5, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v13}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v13

    iget-object v14, v5, Lokhttp3/Response;->k:Lokhttp3/Response;

    iget-object v15, v5, Lokhttp3/Response;->l:Lokhttp3/Response;

    move-object/from16 v16, v1

    iget-object v1, v5, Lokhttp3/Response;->m:Lokhttp3/Response;

    move-object/from16 v19, v1

    move-object/from16 v18, v2

    iget-wide v1, v5, Lokhttp3/Response;->n:J

    move-wide/from16 v20, v1

    iget-wide v1, v5, Lokhttp3/Response;->o:J

    move-wide/from16 v22, v1

    iget-object v1, v5, Lokhttp3/Response;->p:Lb0/g0/g/c;

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_2
    const-string v0, "Content-Type"

    const/4 v2, 0x0

    move-object/from16 v24, v4

    const/4 v4, 0x2

    invoke-static {v5, v0, v2, v4}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v3, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    move-object/from16 v25, v1

    invoke-interface {v2, v5}, Lb0/g0/h/d;->g(Lokhttp3/Response;)J

    move-result-wide v1

    iget-object v4, v3, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v4, v5}, Lb0/g0/h/d;->c(Lokhttp3/Response;)Lc0/x;

    move-result-object v4

    new-instance v5, Lb0/g0/g/c$b;

    invoke-direct {v5, v3, v4, v1, v2}, Lb0/g0/g/c$b;-><init>(Lb0/g0/g/c;Lc0/x;J)V

    new-instance v4, Lb0/g0/h/h;

    invoke-static {v5, v9}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v9, Lc0/r;

    invoke-direct {v9, v5}, Lc0/r;-><init>(Lc0/x;)V

    invoke-direct {v4, v0, v1, v2, v9}, Lb0/g0/h/h;-><init>(Ljava/lang/String;JLc0/g;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-ltz v8, :cond_12

    const/4 v0, 0x1

    goto :goto_5

    :cond_12
    const/4 v0, 0x0

    :goto_5
    if-eqz v0, :cond_1c

    if-eqz v6, :cond_1b

    if-eqz v7, :cond_1a

    if-eqz v11, :cond_19

    invoke-virtual {v13}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v10

    new-instance v0, Lokhttp3/Response;

    move-object v1, v4

    move-object v4, v0

    move-object v5, v6

    move-object v6, v7

    move-object v7, v11

    move-object v9, v12

    move-object v11, v1

    move-object v12, v14

    move-object v13, v15

    move-object/from16 v14, v19

    move-wide/from16 v15, v20

    move-wide/from16 v17, v22

    move-object/from16 v19, v25

    invoke-direct/range {v4 .. v19}, Lokhttp3/Response;-><init>(Lb0/a0;Lb0/z;Ljava/lang/String;ILb0/w;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLb0/g0/g/c;)V

    move-object v6, v0

    :goto_6
    iget-object v0, v6, Lokhttp3/Response;->d:Lb0/a0;

    const-string v1, "Connection"

    invoke-virtual {v0, v1}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "close"

    const/4 v4, 0x1

    invoke-static {v2, v0, v4}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_13

    const/4 v0, 0x2

    const/4 v5, 0x0

    invoke-static {v6, v1, v5, v0}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v4}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_13
    iget-object v0, v3, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v0}, Lb0/g0/h/d;->e()Lb0/g0/g/j;

    move-result-object v0

    invoke-virtual {v0}, Lb0/g0/g/j;->l()V

    :cond_14
    const/16 v0, 0xcc

    move/from16 v8, v29

    if-eq v8, v0, :cond_15

    const/16 v0, 0xcd

    if-ne v8, v0, :cond_18

    :cond_15
    iget-object v0, v6, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->a()J

    move-result-wide v0

    goto :goto_7

    :cond_16
    const-wide/16 v0, -0x1

    :goto_7
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_18

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "HTTP "

    const-string v2, " had non-zero Content-Length: "

    invoke-static {v1, v8, v2}, Lf/e/c/a/a;->H(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v6, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v2, :cond_17

    invoke-virtual {v2}, Lokhttp3/ResponseBody;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_8

    :cond_17
    const/4 v2, 0x0

    :goto_8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    return-object v6

    :cond_19
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1a
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    move-object/from16 v0, v24

    invoke-static {v0, v8}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v0

    iget-object v1, v3, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v2, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, v18

    invoke-static {v2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v2, v16

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lb0/g0/g/c;->g(Ljava/io/IOException;)V

    throw v0

    :catch_1
    move-exception v0

    move-object/from16 v30, v2

    move-object v2, v1

    move-object/from16 v1, v30

    iget-object v4, v3, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v5, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v5, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lb0/g0/g/c;->g(Ljava/io/IOException;)V

    throw v0

    :catch_2
    move-exception v0

    move-object/from16 v30, v2

    move-object v2, v1

    move-object/from16 v1, v30

    iget-object v4, v3, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v5, v3, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v5, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lb0/g0/g/c;->g(Ljava/io/IOException;)V

    throw v0

    :cond_1d
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method
