.class public final Lb0/g0/h/a;
.super Ljava/lang/Object;
.source "BridgeInterceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field public final b:Lb0/p;


# direct methods
.method public constructor <init>(Lb0/p;)V
    .locals 1

    const-string v0, "cookieJar"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/h/a;->b:Lb0/p;

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "chain"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lb0/g0/h/g;

    iget-object v2, v1, Lb0/g0/h/g;->f:Lb0/a0;

    new-instance v3, Lb0/a0$a;

    invoke-direct {v3, v2}, Lb0/a0$a;-><init>(Lb0/a0;)V

    iget-object v4, v2, Lb0/a0;->e:Lokhttp3/RequestBody;

    const-string v5, "Content-Type"

    const-wide/16 v6, -0x1

    const-string v8, "Content-Length"

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lokhttp3/RequestBody;->contentType()Lokhttp3/MediaType;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, v9, Lokhttp3/MediaType;->a:Ljava/lang/String;

    invoke-virtual {v3, v5, v9}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    :cond_0
    invoke-virtual {v4}, Lokhttp3/RequestBody;->contentLength()J

    move-result-wide v9

    const-string v4, "Transfer-Encoding"

    cmp-long v11, v9, v6

    if-eqz v11, :cond_1

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    invoke-virtual {v3, v4}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    goto :goto_0

    :cond_1
    const-string v9, "chunked"

    invoke-virtual {v3, v4, v9}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    invoke-virtual {v3, v8}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    :cond_2
    :goto_0
    const-string v4, "Host"

    invoke-virtual {v2, v4}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    if-nez v9, :cond_3

    iget-object v9, v2, Lb0/a0;->b:Lb0/x;

    invoke-static {v9, v10}, Lb0/g0/c;->y(Lb0/x;Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v4, v9}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    :cond_3
    const-string v4, "Connection"

    invoke-virtual {v2, v4}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_4

    const-string v9, "Keep-Alive"

    invoke-virtual {v3, v4, v9}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    :cond_4
    const-string v4, "Accept-Encoding"

    invoke-virtual {v2, v4}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v11, "gzip"

    const/4 v12, 0x1

    if-nez v9, :cond_5

    const-string v9, "Range"

    invoke-virtual {v2, v9}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_5

    invoke-virtual {v3, v4, v11}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    const/4 v4, 0x1

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :goto_1
    iget-object v9, v0, Lb0/g0/h/a;->b:Lb0/p;

    iget-object v13, v2, Lb0/a0;->b:Lb0/x;

    invoke-interface {v9, v13}, Lb0/p;->b(Lb0/x;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->isEmpty()Z

    move-result v13

    xor-int/2addr v13, v12

    const/4 v14, 0x0

    if-eqz v13, :cond_9

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    add-int/lit8 v16, v10, 0x1

    if-ltz v10, :cond_7

    check-cast v15, Lb0/n;

    if-lez v10, :cond_6

    const-string v10, "; "

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    iget-object v10, v15, Lb0/n;->a:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v10, 0x3d

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v10, v15, Lb0/n;->b:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v10, v16

    goto :goto_2

    :cond_7
    invoke-static {}, Lx/h/f;->throwIndexOverflow()V

    throw v14

    :cond_8
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v9, v10}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "Cookie"

    invoke-virtual {v3, v10, v9}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    :cond_9
    const-string v9, "User-Agent"

    invoke-virtual {v2, v9}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_a

    const-string v10, "okhttp/4.8.0"

    invoke-virtual {v3, v9, v10}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    :cond_a
    invoke-virtual {v3}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object v3

    invoke-virtual {v1, v3}, Lb0/g0/h/g;->a(Lb0/a0;)Lokhttp3/Response;

    move-result-object v1

    iget-object v3, v0, Lb0/g0/h/a;->b:Lb0/p;

    iget-object v9, v2, Lb0/a0;->b:Lb0/x;

    iget-object v10, v1, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-static {v3, v9, v10}, Lb0/g0/h/e;->d(Lb0/p;Lb0/x;Lokhttp3/Headers;)V

    new-instance v3, Lokhttp3/Response$a;

    invoke-direct {v3, v1}, Lokhttp3/Response$a;-><init>(Lokhttp3/Response;)V

    invoke-virtual {v3, v2}, Lokhttp3/Response$a;->g(Lb0/a0;)Lokhttp3/Response$a;

    if-eqz v4, :cond_b

    const-string v2, "Content-Encoding"

    const/4 v4, 0x2

    invoke-static {v1, v2, v14, v4}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v11, v9, v12}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-static {v1}, Lb0/g0/h/e;->a(Lokhttp3/Response;)Z

    move-result v9

    if-eqz v9, :cond_b

    iget-object v9, v1, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v9, :cond_b

    new-instance v10, Lc0/l;

    invoke-virtual {v9}, Lokhttp3/ResponseBody;->c()Lc0/g;

    move-result-object v9

    invoke-direct {v10, v9}, Lc0/l;-><init>(Lc0/x;)V

    iget-object v9, v1, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v9}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v9

    invoke-virtual {v9, v2}, Lokhttp3/Headers$a;->d(Ljava/lang/String;)Lokhttp3/Headers$a;

    invoke-virtual {v9, v8}, Lokhttp3/Headers$a;->d(Ljava/lang/String;)Lokhttp3/Headers$a;

    invoke-virtual {v9}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v2

    invoke-virtual {v3, v2}, Lokhttp3/Response$a;->d(Lokhttp3/Headers;)Lokhttp3/Response$a;

    invoke-static {v1, v5, v14, v4}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lb0/g0/h/h;

    const-string v4, "$this$buffer"

    invoke-static {v10, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lc0/r;

    invoke-direct {v4, v10}, Lc0/r;-><init>(Lc0/x;)V

    invoke-direct {v2, v1, v6, v7, v4}, Lb0/g0/h/h;-><init>(Ljava/lang/String;JLc0/g;)V

    iput-object v2, v3, Lokhttp3/Response$a;->g:Lokhttp3/ResponseBody;

    :cond_b
    invoke-virtual {v3}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v1

    return-object v1
.end method
