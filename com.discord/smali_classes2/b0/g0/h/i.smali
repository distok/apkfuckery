.class public final Lb0/g0/h/i;
.super Ljava/lang/Object;
.source "RetryAndFollowUpInterceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field public final b:Lb0/y;


# direct methods
.method public constructor <init>(Lb0/y;)V
    .locals 1

    const-string v0, "client"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/h/i;->b:Lb0/y;

    return-void
.end method


# virtual methods
.method public final a(Lokhttp3/Response;Lb0/g0/g/c;)Lb0/a0;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object v1, p2, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lb0/g0/g/j;->q:Lb0/e0;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iget v2, p1, Lokhttp3/Response;->g:I

    iget-object v3, p1, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v4, v3, Lb0/a0;->c:Ljava/lang/String;

    const/4 v5, 0x0

    const/16 v6, 0x134

    const/16 v7, 0x133

    const/4 v8, 0x1

    if-eq v2, v7, :cond_11

    if-eq v2, v6, :cond_11

    const/16 v9, 0x191

    if-eq v2, v9, :cond_10

    const/16 v9, 0x1a5

    if-eq v2, v9, :cond_c

    const/16 p2, 0x1f7

    if-eq v2, p2, :cond_9

    const/16 p2, 0x197

    if-eq v2, p2, :cond_6

    const/16 p2, 0x198

    if-eq v2, p2, :cond_1

    packed-switch v2, :pswitch_data_0

    return-object v0

    :cond_1
    iget-object v1, p0, Lb0/g0/h/i;->b:Lb0/y;

    iget-boolean v1, v1, Lb0/y;->i:Z

    if-nez v1, :cond_2

    return-object v0

    :cond_2
    iget-object v1, v3, Lb0/a0;->e:Lokhttp3/RequestBody;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lokhttp3/RequestBody;->isOneShot()Z

    move-result v1

    if-eqz v1, :cond_3

    return-object v0

    :cond_3
    iget-object v1, p1, Lokhttp3/Response;->m:Lokhttp3/Response;

    if-eqz v1, :cond_4

    iget v1, v1, Lokhttp3/Response;->g:I

    if-ne v1, p2, :cond_4

    return-object v0

    :cond_4
    invoke-virtual {p0, p1, v5}, Lb0/g0/h/i;->c(Lokhttp3/Response;I)I

    move-result p2

    if-lez p2, :cond_5

    return-object v0

    :cond_5
    iget-object p1, p1, Lokhttp3/Response;->d:Lb0/a0;

    return-object p1

    :cond_6
    if-eqz v1, :cond_8

    iget-object p2, v1, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-virtual {p2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object p2

    sget-object v0, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne p2, v0, :cond_7

    iget-object p2, p0, Lb0/g0/h/i;->b:Lb0/y;

    iget-object p2, p2, Lb0/y;->q:Lb0/c;

    invoke-interface {p2, v1, p1}, Lb0/c;->a(Lb0/e0;Lokhttp3/Response;)Lb0/a0;

    move-result-object p1

    return-object p1

    :cond_7
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v0

    :cond_9
    iget-object v1, p1, Lokhttp3/Response;->m:Lokhttp3/Response;

    if-eqz v1, :cond_a

    iget v1, v1, Lokhttp3/Response;->g:I

    if-ne v1, p2, :cond_a

    return-object v0

    :cond_a
    const p2, 0x7fffffff

    invoke-virtual {p0, p1, p2}, Lb0/g0/h/i;->c(Lokhttp3/Response;I)I

    move-result p2

    if-nez p2, :cond_b

    iget-object p1, p1, Lokhttp3/Response;->d:Lb0/a0;

    return-object p1

    :cond_b
    return-object v0

    :cond_c
    iget-object v1, v3, Lb0/a0;->e:Lokhttp3/RequestBody;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lokhttp3/RequestBody;->isOneShot()Z

    move-result v1

    if-eqz v1, :cond_d

    return-object v0

    :cond_d
    if-eqz p2, :cond_f

    iget-object v1, p2, Lb0/g0/g/c;->e:Lb0/g0/g/d;

    iget-object v1, v1, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v1, v1, Lb0/a;->a:Lb0/x;

    iget-object v1, v1, Lb0/x;->e:Ljava/lang/String;

    iget-object v2, p2, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    iget-object v2, v2, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v2, v2, Lb0/e0;->a:Lb0/a;

    iget-object v2, v2, Lb0/a;->a:Lb0/x;

    iget-object v2, v2, Lb0/x;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v8

    if-nez v1, :cond_e

    goto :goto_1

    :cond_e
    iget-object p2, p2, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    monitor-enter p2

    :try_start_0
    iput-boolean v8, p2, Lb0/g0/g/j;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p2

    iget-object p1, p1, Lokhttp3/Response;->d:Lb0/a0;

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p2

    throw p1

    :cond_f
    :goto_1
    return-object v0

    :cond_10
    iget-object p2, p0, Lb0/g0/h/i;->b:Lb0/y;

    iget-object p2, p2, Lb0/y;->j:Lb0/c;

    invoke-interface {p2, v1, p1}, Lb0/c;->a(Lb0/e0;Lokhttp3/Response;)Lb0/a0;

    move-result-object p1

    return-object p1

    :cond_11
    :pswitch_0
    iget-object p2, p0, Lb0/g0/h/i;->b:Lb0/y;

    iget-boolean p2, p2, Lb0/y;->k:Z

    if-nez p2, :cond_12

    goto/16 :goto_4

    :cond_12
    const/4 p2, 0x2

    const-string v1, "Location"

    invoke-static {p1, v1, v0, p2}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_1b

    iget-object v1, p1, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v1, v1, Lb0/a0;->b:Lb0/x;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "link"

    invoke-static {p2, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lb0/x;->g(Ljava/lang/String;)Lb0/x$a;

    move-result-object p2

    if-eqz p2, :cond_13

    invoke-virtual {p2}, Lb0/x$a;->b()Lb0/x;

    move-result-object p2

    goto :goto_2

    :cond_13
    move-object p2, v0

    :goto_2
    if-eqz p2, :cond_1b

    iget-object v1, p2, Lb0/x;->b:Ljava/lang/String;

    iget-object v2, p1, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v2, v2, Lb0/a0;->b:Lb0/x;

    iget-object v2, v2, Lb0/x;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    iget-object v1, p0, Lb0/g0/h/i;->b:Lb0/y;

    iget-boolean v1, v1, Lb0/y;->l:Z

    if-nez v1, :cond_14

    goto :goto_4

    :cond_14
    iget-object v1, p1, Lokhttp3/Response;->d:Lb0/a0;

    new-instance v2, Lb0/a0$a;

    invoke-direct {v2, v1}, Lb0/a0$a;-><init>(Lb0/a0;)V

    invoke-static {v4}, Lb0/g0/h/f;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    iget v1, p1, Lokhttp3/Response;->g:I

    const-string v3, "method"

    invoke-static {v4, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "PROPFIND"

    invoke-static {v4, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_15

    if-eq v1, v6, :cond_15

    if-ne v1, v7, :cond_16

    :cond_15
    const/4 v5, 0x1

    :cond_16
    invoke-static {v4, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v8

    if-eqz v3, :cond_17

    if-eq v1, v6, :cond_17

    if-eq v1, v7, :cond_17

    const-string v1, "GET"

    invoke-virtual {v2, v1, v0}, Lb0/a0$a;->c(Ljava/lang/String;Lokhttp3/RequestBody;)Lb0/a0$a;

    goto :goto_3

    :cond_17
    if-eqz v5, :cond_18

    iget-object v0, p1, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v0, v0, Lb0/a0;->e:Lokhttp3/RequestBody;

    :cond_18
    invoke-virtual {v2, v4, v0}, Lb0/a0$a;->c(Ljava/lang/String;Lokhttp3/RequestBody;)Lb0/a0$a;

    :goto_3
    if-nez v5, :cond_19

    const-string v0, "Transfer-Encoding"

    invoke-virtual {v2, v0}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    const-string v0, "Content-Length"

    invoke-virtual {v2, v0}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    const-string v0, "Content-Type"

    invoke-virtual {v2, v0}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    :cond_19
    iget-object p1, p1, Lokhttp3/Response;->d:Lb0/a0;

    iget-object p1, p1, Lb0/a0;->b:Lb0/x;

    invoke-static {p1, p2}, Lb0/g0/c;->a(Lb0/x;Lb0/x;)Z

    move-result p1

    if-nez p1, :cond_1a

    const-string p1, "Authorization"

    invoke-virtual {v2, p1}, Lb0/a0$a;->d(Ljava/lang/String;)Lb0/a0$a;

    :cond_1a
    invoke-virtual {v2, p2}, Lb0/a0$a;->g(Lb0/x;)Lb0/a0$a;

    invoke-virtual {v2}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object v0

    :cond_1b
    :goto_4
    return-object v0

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Ljava/io/IOException;Lb0/g0/g/e;Lb0/a0;Z)Z
    .locals 3

    iget-object v0, p0, Lb0/g0/h/i;->b:Lb0/y;

    iget-boolean v0, v0, Lb0/y;->i:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    if-eqz p4, :cond_4

    iget-object p3, p3, Lb0/a0;->e:Lokhttp3/RequestBody;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lokhttp3/RequestBody;->isOneShot()Z

    move-result p3

    if-nez p3, :cond_2

    :cond_1
    instance-of p3, p1, Ljava/io/FileNotFoundException;

    if-eqz p3, :cond_3

    :cond_2
    const/4 p3, 0x1

    goto :goto_0

    :cond_3
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_4

    return v1

    :cond_4
    instance-of p3, p1, Ljava/net/ProtocolException;

    if-eqz p3, :cond_5

    goto :goto_1

    :cond_5
    instance-of p3, p1, Ljava/io/InterruptedIOException;

    if-eqz p3, :cond_6

    instance-of p1, p1, Ljava/net/SocketTimeoutException;

    if-eqz p1, :cond_8

    if-nez p4, :cond_8

    goto :goto_2

    :cond_6
    instance-of p3, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz p3, :cond_7

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p3

    instance-of p3, p3, Ljava/security/cert/CertificateException;

    if-eqz p3, :cond_7

    goto :goto_1

    :cond_7
    instance-of p1, p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    if-eqz p1, :cond_9

    :cond_8
    :goto_1
    const/4 p1, 0x0

    goto :goto_3

    :cond_9
    :goto_2
    const/4 p1, 0x1

    :goto_3
    if-nez p1, :cond_a

    return v1

    :cond_a
    iget-object p1, p2, Lb0/g0/g/e;->i:Lb0/g0/g/d;

    const/4 p2, 0x0

    if-eqz p1, :cond_15

    iget p3, p1, Lb0/g0/g/d;->c:I

    if-nez p3, :cond_b

    iget p4, p1, Lb0/g0/g/d;->d:I

    if-nez p4, :cond_b

    iget p4, p1, Lb0/g0/g/d;->e:I

    if-nez p4, :cond_b

    const/4 p1, 0x0

    goto :goto_6

    :cond_b
    iget-object p4, p1, Lb0/g0/g/d;->f:Lb0/e0;

    if-eqz p4, :cond_c

    goto :goto_5

    :cond_c
    if-gt p3, v0, :cond_10

    iget p3, p1, Lb0/g0/g/d;->d:I

    if-gt p3, v0, :cond_10

    iget p3, p1, Lb0/g0/g/d;->e:I

    if-lez p3, :cond_d

    goto :goto_4

    :cond_d
    iget-object p3, p1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object p3, p3, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz p3, :cond_10

    monitor-enter p3

    :try_start_0
    iget p4, p3, Lb0/g0/g/j;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p4, :cond_e

    monitor-exit p3

    goto :goto_4

    :cond_e
    :try_start_1
    iget-object p4, p3, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object p4, p4, Lb0/e0;->a:Lb0/a;

    iget-object p4, p4, Lb0/a;->a:Lb0/x;

    iget-object v2, p1, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v2, v2, Lb0/a;->a:Lb0/x;

    invoke-static {p4, v2}, Lb0/g0/c;->a(Lb0/x;Lb0/x;)Z

    move-result p4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p4, :cond_f

    monitor-exit p3

    goto :goto_4

    :cond_f
    :try_start_2
    iget-object p2, p3, Lb0/g0/g/j;->q:Lb0/e0;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p3

    goto :goto_4

    :catchall_0
    move-exception p1

    monitor-exit p3

    throw p1

    :cond_10
    :goto_4
    if-eqz p2, :cond_11

    iput-object p2, p1, Lb0/g0/g/d;->f:Lb0/e0;

    goto :goto_5

    :cond_11
    iget-object p2, p1, Lb0/g0/g/d;->a:Lb0/g0/g/m$a;

    if-eqz p2, :cond_12

    invoke-virtual {p2}, Lb0/g0/g/m$a;->a()Z

    move-result p2

    if-ne p2, v0, :cond_12

    goto :goto_5

    :cond_12
    iget-object p1, p1, Lb0/g0/g/d;->b:Lb0/g0/g/m;

    if-eqz p1, :cond_13

    invoke-virtual {p1}, Lb0/g0/g/m;->a()Z

    move-result p1

    goto :goto_6

    :cond_13
    :goto_5
    const/4 p1, 0x1

    :goto_6
    if-nez p1, :cond_14

    return v1

    :cond_14
    return v0

    :cond_15
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw p2
.end method

.method public final c(Lokhttp3/Response;I)I
    .locals 3

    const-string v0, "Retry-After"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    new-instance p2, Lkotlin/text/Regex;

    const-string v0, "\\d+"

    invoke-direct {p2, v0}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    const-string p2, "Integer.valueOf(header)"

    invoke-static {p1, p2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    const p1, 0x7fffffff

    return p1

    :cond_1
    return p2
.end method

.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 48
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    const-string v2, "chain"

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Lb0/g0/h/g;

    iget-object v0, v2, Lb0/g0/h/g;->f:Lb0/a0;

    iget-object v3, v2, Lb0/g0/h/g;->b:Lb0/g0/g/e;

    sget-object v4, Lx/h/l;->d:Lx/h/l;

    const/4 v7, 0x1

    move-object v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v4, v0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v11, "request"

    invoke-static {v4, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v11, v3, Lb0/g0/g/e;->l:Lb0/g0/g/c;

    if-nez v11, :cond_0

    const/4 v11, 0x1

    goto :goto_1

    :cond_0
    const/4 v11, 0x0

    :goto_1
    if-eqz v11, :cond_1c

    monitor-enter v3

    :try_start_0
    iget-boolean v11, v3, Lb0/g0/g/e;->n:Z

    xor-int/2addr v11, v7

    if-eqz v11, :cond_1b

    iget-boolean v11, v3, Lb0/g0/g/e;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    xor-int/2addr v11, v7

    if-eqz v11, :cond_1a

    monitor-exit v3

    if-eqz v0, :cond_3

    new-instance v0, Lb0/g0/g/d;

    iget-object v11, v3, Lb0/g0/g/e;->d:Lb0/g0/g/k;

    iget-object v12, v4, Lb0/a0;->b:Lb0/x;

    iget-boolean v13, v12, Lb0/x;->a:Z

    if-eqz v13, :cond_2

    iget-object v13, v3, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v14, v13, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v14, :cond_1

    iget-object v15, v13, Lb0/y;->w:Ljavax/net/ssl/HostnameVerifier;

    iget-object v13, v13, Lb0/y;->x:Lb0/g;

    move-object/from16 v19, v13

    move-object/from16 v17, v14

    move-object/from16 v18, v15

    goto :goto_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "CLEARTEXT-only client"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    :goto_2
    new-instance v15, Lb0/a;

    iget-object v13, v12, Lb0/x;->e:Ljava/lang/String;

    iget v14, v12, Lb0/x;->f:I

    iget-object v12, v3, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v5, v12, Lb0/y;->n:Lb0/s;

    iget-object v7, v12, Lb0/y;->r:Ljavax/net/SocketFactory;

    iget-object v6, v12, Lb0/y;->q:Lb0/c;

    move-object/from16 v25, v8

    iget-object v8, v12, Lb0/y;->o:Ljava/net/Proxy;

    move/from16 v26, v10

    iget-object v10, v12, Lb0/y;->v:Ljava/util/List;

    iget-object v1, v12, Lb0/y;->u:Ljava/util/List;

    iget-object v12, v12, Lb0/y;->p:Ljava/net/ProxySelector;

    move-object/from16 v24, v12

    move-object v12, v15

    move-object/from16 v27, v9

    move-object v9, v15

    move-object v15, v5

    move-object/from16 v16, v7

    move-object/from16 v20, v6

    move-object/from16 v21, v8

    move-object/from16 v22, v10

    move-object/from16 v23, v1

    invoke-direct/range {v12 .. v24}, Lb0/a;-><init>(Ljava/lang/String;ILb0/s;Ljavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lb0/g;Lb0/c;Ljava/net/Proxy;Ljava/util/List;Ljava/util/List;Ljava/net/ProxySelector;)V

    iget-object v1, v3, Lb0/g0/g/e;->e:Lb0/t;

    invoke-direct {v0, v11, v9, v3, v1}, Lb0/g0/g/d;-><init>(Lb0/g0/g/k;Lb0/a;Lb0/g0/g/e;Lb0/t;)V

    iput-object v0, v3, Lb0/g0/g/e;->i:Lb0/g0/g/d;

    goto :goto_3

    :cond_3
    move-object/from16 v25, v8

    move-object/from16 v27, v9

    move/from16 v26, v10

    :goto_3
    :try_start_1
    iget-boolean v0, v3, Lb0/g0/g/e;->p:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    if-nez v0, :cond_19

    :try_start_2
    invoke-virtual {v2, v4}, Lb0/g0/h/g;->a(Lb0/a0;)Lokhttp3/Response;

    move-result-object v0
    :try_end_2
    .catch Lokhttp3/internal/connection/RouteException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    if-eqz v27, :cond_10

    :try_start_3
    const-string v1, "response"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v0, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v6, v0, Lokhttp3/Response;->e:Lb0/z;

    iget v8, v0, Lokhttp3/Response;->g:I

    iget-object v7, v0, Lokhttp3/Response;->f:Ljava/lang/String;

    iget-object v9, v0, Lokhttp3/Response;->h:Lb0/w;

    iget-object v1, v0, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v1}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v1

    iget-object v11, v0, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    iget-object v12, v0, Lokhttp3/Response;->k:Lokhttp3/Response;

    iget-object v13, v0, Lokhttp3/Response;->l:Lokhttp3/Response;

    iget-wide v14, v0, Lokhttp3/Response;->n:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object/from16 v20, v2

    move-object/from16 v21, v3

    :try_start_4
    iget-wide v2, v0, Lokhttp3/Response;->o:J

    iget-object v0, v0, Lokhttp3/Response;->p:Lb0/g0/g/c;

    const-string v4, "response"

    move-object/from16 v10, v27

    invoke-static {v10, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v10, Lokhttp3/Response;->d:Lb0/a0;

    move-wide/from16 v16, v14

    iget-object v14, v10, Lokhttp3/Response;->e:Lb0/z;

    iget v15, v10, Lokhttp3/Response;->g:I

    move-object/from16 v19, v0

    iget-object v0, v10, Lokhttp3/Response;->f:Ljava/lang/String;

    move-wide/from16 v22, v2

    iget-object v2, v10, Lokhttp3/Response;->h:Lb0/w;

    iget-object v3, v10, Lokhttp3/Response;->i:Lokhttp3/Headers;

    invoke-virtual {v3}, Lokhttp3/Headers;->e()Lokhttp3/Headers$a;

    move-result-object v3

    move-object/from16 v18, v13

    iget-object v13, v10, Lokhttp3/Response;->k:Lokhttp3/Response;

    move-object/from16 v24, v12

    iget-object v12, v10, Lokhttp3/Response;->l:Lokhttp3/Response;

    move-object/from16 v43, v11

    iget-object v11, v10, Lokhttp3/Response;->m:Lokhttp3/Response;

    move-object/from16 v44, v6

    move-object/from16 v45, v7

    iget-wide v6, v10, Lokhttp3/Response;->n:J

    move/from16 v46, v8

    move-object/from16 v47, v9

    iget-wide v8, v10, Lokhttp3/Response;->o:J

    iget-object v10, v10, Lokhttp3/Response;->p:Lb0/g0/g/c;

    const/16 v34, 0x0

    if-ltz v15, :cond_4

    const/16 v27, 0x1

    goto :goto_4

    :cond_4
    const/16 v27, 0x0

    :goto_4
    if-eqz v27, :cond_f

    if-eqz v4, :cond_e

    if-eqz v14, :cond_d

    if-eqz v0, :cond_c

    invoke-virtual {v3}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v33

    new-instance v3, Lokhttp3/Response;

    move-object/from16 v27, v3

    move-object/from16 v28, v4

    move-object/from16 v29, v14

    move-object/from16 v30, v0

    move/from16 v31, v15

    move-object/from16 v32, v2

    move-object/from16 v35, v13

    move-object/from16 v36, v12

    move-object/from16 v37, v11

    move-wide/from16 v38, v6

    move-wide/from16 v40, v8

    move-object/from16 v42, v10

    invoke-direct/range {v27 .. v42}, Lokhttp3/Response;-><init>(Lb0/a0;Lb0/z;Ljava/lang/String;ILb0/w;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLb0/g0/g/c;)V

    iget-object v0, v3, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-nez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    :goto_5
    if-eqz v0, :cond_b

    if-ltz v46, :cond_6

    const/4 v0, 0x1

    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    :goto_6
    if-eqz v0, :cond_a

    if-eqz v5, :cond_9

    if-eqz v44, :cond_8

    if-eqz v45, :cond_7

    invoke-virtual {v1}, Lokhttp3/Headers$a;->c()Lokhttp3/Headers;

    move-result-object v10

    new-instance v0, Lokhttp3/Response;

    move-object v4, v0

    move-object/from16 v6, v44

    move-object/from16 v7, v45

    move/from16 v8, v46

    move-object/from16 v9, v47

    move-object/from16 v11, v43

    move-object/from16 v12, v24

    move-object/from16 v13, v18

    move-wide/from16 v1, v16

    move-object v14, v3

    move-wide v15, v1

    move-wide/from16 v17, v22

    invoke-direct/range {v4 .. v19}, Lokhttp3/Response;-><init>(Lb0/a0;Lb0/z;Ljava/lang/String;ILb0/w;Lokhttp3/Headers;Lokhttp3/ResponseBody;Lokhttp3/Response;Lokhttp3/Response;Lokhttp3/Response;JJLb0/g0/g/c;)V

    goto/16 :goto_8

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "message == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "protocol == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "code < 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "priorResponse.body != null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "message == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "protocol == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request == null"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "code < 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_7

    :catchall_1
    move-exception v0

    move-object/from16 v21, v3

    :goto_7
    move-object/from16 v2, p0

    move-object/from16 v1, v21

    goto/16 :goto_d

    :cond_10
    move-object/from16 v20, v2

    move-object/from16 v21, v3

    :goto_8
    move-object v9, v0

    move-object/from16 v1, v21

    :try_start_5
    iget-object v0, v1, Lb0/g0/g/e;->l:Lb0/g0/g/c;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object/from16 v2, p0

    :try_start_6
    invoke-virtual {v2, v9, v0}, Lb0/g0/h/i;->a(Lokhttp3/Response;Lb0/g0/g/c;)Lb0/a0;

    move-result-object v3

    if-nez v3, :cond_12

    if-eqz v0, :cond_11

    iget-boolean v0, v0, Lb0/g0/g/c;->a:Z

    if-eqz v0, :cond_11

    invoke-virtual {v1}, Lb0/g0/g/e;->m()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :cond_11
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lb0/g0/g/e;->h(Z)V

    return-object v9

    :cond_12
    const/4 v4, 0x0

    :try_start_7
    iget-object v0, v3, Lb0/a0;->e:Lokhttp3/RequestBody;

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lokhttp3/RequestBody;->isOneShot()Z

    move-result v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v0, :cond_13

    invoke-virtual {v1, v4}, Lb0/g0/g/e;->h(Z)V

    return-object v9

    :cond_13
    :try_start_8
    iget-object v0, v9, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v0, :cond_14

    sget-object v4, Lb0/g0/c;->a:[B

    const-string v4, "$this$closeQuietly"

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_9

    :catch_0
    nop

    goto :goto_9

    :catch_1
    move-exception v0

    move-object v3, v0

    :try_start_a
    throw v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :cond_14
    :goto_9
    add-int/lit8 v10, v26, 0x1

    const/16 v0, 0x14

    if-gt v10, v0, :cond_15

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lb0/g0/g/e;->h(Z)V

    move-object v4, v3

    move-object/from16 v8, v25

    const/4 v0, 0x1

    const/4 v6, 0x0

    goto/16 :goto_c

    :cond_15
    :try_start_b
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Too many follow-up requests: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_2
    move-exception v0

    move-object/from16 v2, p0

    goto/16 :goto_d

    :catch_2
    move-exception v0

    move-object/from16 v20, v2

    move-object v1, v3

    move-object/from16 v10, v27

    move-object/from16 v2, p0

    move-object v3, v0

    nop

    instance-of v0, v3, Lokhttp3/internal/http2/ConnectionShutdownException;

    if-nez v0, :cond_16

    const/4 v0, 0x1

    goto :goto_a

    :cond_16
    const/4 v0, 0x0

    :goto_a
    invoke-virtual {v2, v3, v1, v4, v0}, Lb0/g0/h/i;->b(Ljava/io/IOException;Lb0/g0/g/e;Lb0/a0;Z)Z

    move-result v0

    if-eqz v0, :cond_17

    move-object/from16 v5, v25

    invoke-static {v5, v3}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x1

    const/4 v6, 0x0

    goto :goto_b

    :cond_17
    move-object/from16 v5, v25

    invoke-static {v3, v5}, Lb0/g0/c;->C(Ljava/lang/Exception;Ljava/util/List;)Ljava/lang/Throwable;

    throw v3

    :catch_3
    move-exception v0

    move-object/from16 v20, v2

    move-object v1, v3

    move-object/from16 v5, v25

    move-object/from16 v10, v27

    move-object/from16 v2, p0

    move-object v3, v0

    invoke-virtual {v3}, Lokhttp3/internal/connection/RouteException;->c()Ljava/io/IOException;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v2, v0, v1, v4, v6}, Lb0/g0/h/i;->b(Ljava/io/IOException;Lb0/g0/g/e;Lb0/a0;Z)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {v3}, Lokhttp3/internal/connection/RouteException;->b()Ljava/io/IOException;

    move-result-object v0

    invoke-static {v5, v0}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    const/4 v3, 0x1

    :goto_b
    invoke-virtual {v1, v3}, Lb0/g0/g/e;->h(Z)V

    move-object v8, v0

    move-object v9, v10

    move/from16 v10, v26

    const/4 v0, 0x0

    :goto_c
    move-object v3, v1

    move-object v1, v2

    move-object/from16 v2, v20

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_18
    :try_start_c
    invoke-virtual {v3}, Lokhttp3/internal/connection/RouteException;->b()Ljava/io/IOException;

    move-result-object v0

    invoke-static {v0, v5}, Lb0/g0/c;->C(Ljava/lang/Exception;Ljava/util/List;)Ljava/lang/Throwable;

    throw v0

    :cond_19
    move-object/from16 v2, p0

    move-object v1, v3

    new-instance v0, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_d

    :catchall_4
    move-exception v0

    move-object/from16 v2, p0

    move-object v1, v3

    :goto_d
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lb0/g0/g/e;->h(Z)V

    throw v0

    :cond_1a
    move-object v2, v1

    move-object v1, v3

    :try_start_d
    const-string v0, "Check failed."

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1b
    move-object v2, v1

    move-object v1, v3

    const-string v0, "cannot make a new request because the previous response is still open: please call response.close()"

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :catchall_5
    move-exception v0

    goto :goto_e

    :catchall_6
    move-exception v0

    move-object v2, v1

    move-object v1, v3

    :goto_e
    monitor-exit v1

    throw v0

    :cond_1c
    move-object v2, v1

    const-string v0, "Check failed."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
