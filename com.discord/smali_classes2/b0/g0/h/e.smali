.class public final Lb0/g0/h/e;
.super Ljava/lang/Object;
.source "HttpHeaders.kt"


# static fields
.field public static final a:Lokio/ByteString;

.field public static final b:Lokio/ByteString;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    sget-object v0, Lokio/ByteString;->g:Lokio/ByteString$a;

    const-string v1, "\"\\"

    invoke-virtual {v0, v1}, Lokio/ByteString$a;->b(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    sput-object v1, Lb0/g0/h/e;->a:Lokio/ByteString;

    const-string v1, "\t ,="

    invoke-virtual {v0, v1}, Lokio/ByteString$a;->b(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    sput-object v0, Lb0/g0/h/e;->b:Lokio/ByteString;

    return-void
.end method

.method public static final a(Lokhttp3/Response;)Z
    .locals 8

    const-string v0, "$this$promisesBody"

    invoke-static {p0, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lokhttp3/Response;->d:Lb0/a0;

    iget-object v0, v0, Lb0/a0;->c:Ljava/lang/String;

    const-string v1, "HEAD"

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget v0, p0, Lokhttp3/Response;->g:I

    const/16 v2, 0x64

    const/4 v3, 0x1

    if-lt v0, v2, :cond_1

    const/16 v2, 0xc8

    if-lt v0, v2, :cond_2

    :cond_1
    const/16 v2, 0xcc

    if-eq v0, v2, :cond_2

    const/16 v2, 0x130

    if-eq v0, v2, :cond_2

    return v3

    :cond_2
    invoke-static {p0}, Lb0/g0/c;->l(Lokhttp3/Response;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    const/4 v0, 0x0

    const/4 v2, 0x2

    const-string v4, "Transfer-Encoding"

    invoke-static {p0, v4, v0, v2}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    const-string v0, "chunked"

    invoke-static {v0, p0, v3}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_3

    goto :goto_0

    :cond_3
    return v1

    :cond_4
    :goto_0
    return v3
.end method

.method public static final b(Lc0/e;Ljava/util/List;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc0/e;",
            "Ljava/util/List<",
            "Lb0/i;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    :goto_0
    const/4 v2, 0x0

    move-object v3, v2

    :goto_1
    if-nez v2, :cond_0

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->e(Lc0/e;)Z

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->c(Lc0/e;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->e(Lc0/e;)Z

    move-result v4

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->c(Lc0/e;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    invoke-virtual/range {p0 .. p0}, Lc0/e;->H()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Lb0/i;

    sget-object v3, Lx/h/m;->d:Lx/h/m;

    invoke-direct {v0, v2, v3}, Lb0/i;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_2
    const/16 v6, 0x3d

    int-to-byte v6, v6

    invoke-static {v0, v6}, Lb0/g0/c;->u(Lc0/e;B)I

    move-result v7

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->e(Lc0/e;)Z

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-nez v4, :cond_b

    if-nez v8, :cond_3

    invoke-virtual/range {p0 .. p0}, Lc0/e;->H()Z

    move-result v4

    if-eqz v4, :cond_b

    :cond_3
    new-instance v4, Lb0/i;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "="

    const-string v8, "$this$repeat"

    invoke-static {v5, v8}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    if-ltz v7, :cond_4

    const/4 v8, 0x1

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    :goto_2
    if-eqz v8, :cond_a

    if-eqz v7, :cond_9

    if-eq v7, v10, :cond_8

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_9

    if-eq v8, v10, :cond_6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    mul-int v9, v9, v7

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    if-gt v10, v7, :cond_5

    :goto_3
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    if-eq v10, v7, :cond_5

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "sb.toString()"

    invoke-static {v5, v7}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_5

    :cond_6
    invoke-virtual {v5, v9}, Ljava/lang/String;->charAt(I)C

    move-result v5

    new-array v8, v7, [C

    :goto_4
    if-ge v9, v7, :cond_7

    aput-char v5, v8, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_7
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>([C)V

    goto :goto_5

    :cond_8
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_9
    const-string v5, ""

    :goto_5
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    const-string v5, "Collections.singletonMap\u2026ek + \"=\".repeat(eqCount))"

    invoke-static {v3, v5}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v4, v2, v3}, Lb0/i;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Count \'n\' must be non-negative, but was "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-static {v0, v6}, Lb0/g0/c;->u(Lc0/e;B)I

    move-result v8

    add-int/2addr v8, v7

    :goto_6
    if-nez v5, :cond_d

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->c(Lc0/e;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->e(Lc0/e;)Z

    move-result v7

    if-eqz v7, :cond_c

    goto :goto_7

    :cond_c
    invoke-static {v0, v6}, Lb0/g0/c;->u(Lc0/e;B)I

    move-result v7

    move v8, v7

    :cond_d
    if-nez v8, :cond_e

    :goto_7
    new-instance v6, Lb0/i;

    invoke-direct {v6, v2, v4}, Lb0/i;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v5

    goto/16 :goto_1

    :cond_e
    if-le v8, v10, :cond_f

    return-void

    :cond_f
    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->e(Lc0/e;)Z

    move-result v7

    if-eqz v7, :cond_10

    return-void

    :cond_10
    const/16 v7, 0x22

    int-to-byte v7, v7

    invoke-virtual/range {p0 .. p0}, Lc0/e;->H()Z

    move-result v9

    if-nez v9, :cond_11

    const-wide/16 v11, 0x0

    invoke-virtual {v0, v11, v12}, Lc0/e;->g(J)B

    move-result v9

    if-ne v9, v7, :cond_11

    const/4 v9, 0x1

    goto :goto_8

    :cond_11
    const/4 v9, 0x0

    :goto_8
    if-eqz v9, :cond_17

    invoke-virtual/range {p0 .. p0}, Lc0/e;->readByte()B

    move-result v9

    if-ne v9, v7, :cond_12

    const/4 v9, 0x1

    goto :goto_9

    :cond_12
    const/4 v9, 0x0

    :goto_9
    if-eqz v9, :cond_16

    new-instance v9, Lc0/e;

    invoke-direct {v9}, Lc0/e;-><init>()V

    :goto_a
    sget-object v11, Lb0/g0/h/e;->a:Lokio/ByteString;

    invoke-virtual {v0, v11}, Lc0/e;->P(Lokio/ByteString;)J

    move-result-wide v11

    const-wide/16 v13, -0x1

    cmp-long v15, v11, v13

    if-nez v15, :cond_13

    goto :goto_b

    :cond_13
    invoke-virtual {v0, v11, v12}, Lc0/e;->g(J)B

    move-result v3

    if-ne v3, v7, :cond_14

    invoke-virtual {v9, v0, v11, v12}, Lc0/e;->write(Lc0/e;J)V

    invoke-virtual/range {p0 .. p0}, Lc0/e;->readByte()B

    invoke-virtual {v9}, Lc0/e;->B()Ljava/lang/String;

    move-result-object v3

    :goto_b
    move-object v1, v3

    move-object v3, v2

    goto :goto_c

    :cond_14
    iget-wide v13, v0, Lc0/e;->e:J

    move-object v3, v2

    const-wide/16 v1, 0x1

    add-long v15, v11, v1

    cmp-long v17, v13, v15

    if-nez v17, :cond_15

    const/4 v1, 0x0

    goto :goto_c

    :cond_15
    invoke-virtual {v9, v0, v11, v12}, Lc0/e;->write(Lc0/e;J)V

    invoke-virtual/range {p0 .. p0}, Lc0/e;->readByte()B

    invoke-virtual {v9, v0, v1, v2}, Lc0/e;->write(Lc0/e;J)V

    const/4 v1, 0x0

    move-object v2, v3

    move-object v3, v1

    move-object/from16 v1, p1

    goto :goto_a

    :cond_16
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Failed requirement."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    move-object v3, v2

    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->c(Lc0/e;)Ljava/lang/String;

    move-result-object v1

    :goto_c
    if-eqz v1, :cond_1a

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_18

    return-void

    :cond_18
    invoke-static/range {p0 .. p0}, Lb0/g0/h/e;->e(Lc0/e;)Z

    move-result v1

    if-nez v1, :cond_19

    invoke-virtual/range {p0 .. p0}, Lc0/e;->H()Z

    move-result v1

    if-nez v1, :cond_19

    return-void

    :cond_19
    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v2, v3

    move-object v3, v1

    move-object/from16 v1, p1

    goto/16 :goto_6

    :cond_1a
    return-void
.end method

.method public static final c(Lc0/e;)Ljava/lang/String;
    .locals 5

    sget-object v0, Lb0/g0/h/e;->b:Lokio/ByteString;

    invoke-virtual {p0, v0}, Lc0/e;->P(Lokio/ByteString;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lc0/e;->e:J

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    invoke-virtual {p0, v0, v1}, Lc0/e;->C(J)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final d(Lb0/p;Lb0/x;Lokhttp3/Headers;)V
    .locals 40

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v0, p2

    const-string v3, "$this$receiveHeaders"

    invoke-static {v1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "url"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "headers"

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lb0/p;->a:Lb0/p;

    if-ne v1, v5, :cond_0

    return-void

    :cond_0
    sget-object v5, Lb0/n;->n:Lb0/n$a;

    sget-object v6, Lx/h/l;->d:Lx/h/l;

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "Set-Cookie"

    const-string v7, "name"

    invoke-static {v4, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lokhttp3/Headers;->size()I

    move-result v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_0
    const/4 v12, 0x2

    const/4 v13, 0x1

    if-ge v10, v7, :cond_3

    invoke-virtual {v0, v10}, Lokhttp3/Headers;->d(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v4, v14, v13}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_2

    if-nez v11, :cond_1

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v12}, Ljava/util/ArrayList;-><init>(I)V

    :cond_1
    invoke-virtual {v0, v10}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_3
    if-eqz v11, :cond_4

    invoke-static {v11}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    const-string v4, "Collections.unmodifiableList(result)"

    invoke-static {v0, v4}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_1

    :cond_4
    move-object v4, v6

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_2
    if-ge v10, v7, :cond_24

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/lang/String;

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setCookie"

    invoke-static {v14, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v14, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v8, 0x3b

    const/4 v13, 0x6

    invoke-static {v14, v8, v9, v9, v13}, Lb0/g0/c;->h(Ljava/lang/String;CIII)I

    move-result v0

    const/16 v13, 0x3d

    invoke-static {v14, v13, v9, v0, v12}, Lb0/g0/c;->h(Ljava/lang/String;CIII)I

    move-result v8

    if-ne v8, v0, :cond_6

    :cond_5
    :goto_3
    move-object/from16 v38, v3

    move-object/from16 v17, v4

    move-object/from16 v39, v6

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v13, 0x1

    goto/16 :goto_f

    :cond_6
    invoke-static {v14, v9, v8}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_7

    const/16 v18, 0x1

    goto :goto_4

    :cond_7
    const/16 v18, 0x0

    :goto_4
    if-nez v18, :cond_5

    invoke-static/range {v19 .. v19}, Lb0/g0/c;->n(Ljava/lang/String;)I

    move-result v12

    const/4 v9, -0x1

    if-eq v12, v9, :cond_8

    goto :goto_3

    :cond_8
    add-int/lit8 v8, v8, 0x1

    invoke-static {v14, v8, v0}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lb0/g0/c;->n(Ljava/lang/String;)I

    move-result v8

    if-eq v8, v9, :cond_9

    goto :goto_3

    :cond_9
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v8

    const-wide v21, 0xe677d21fdbffL

    const-wide/16 v23, -0x1

    move-wide/from16 v32, v21

    move-wide/from16 v30, v23

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x1

    :goto_5
    const-wide v34, 0x7fffffffffffffffL

    const-wide/high16 v36, -0x8000000000000000L

    if-ge v0, v8, :cond_16

    move-object/from16 v38, v3

    move-object/from16 v17, v4

    const/16 v3, 0x3b

    invoke-static {v14, v3, v0, v8}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v4

    invoke-static {v14, v13, v0, v4}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v3

    invoke-static {v14, v0, v3}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    if-ge v3, v4, :cond_a

    add-int/lit8 v3, v3, 0x1

    invoke-static {v14, v3, v4}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    :cond_a
    const-string v3, ""

    :goto_6
    const-string v13, "expires"

    move-object/from16 v39, v6

    const/4 v6, 0x1

    invoke-static {v0, v13, v6}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_b

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v13, 0x0

    invoke-virtual {v5, v3, v13, v0}, Lb0/n$a;->c(Ljava/lang/String;II)J

    move-result-wide v32
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_8

    :cond_b
    const-string v13, "max-age"

    invoke-static {v0, v13, v6}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v13

    if-eqz v13, :cond_f

    :try_start_1
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v30
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    const-wide/16 v34, 0x0

    cmp-long v0, v30, v34

    if-gtz v0, :cond_c

    goto :goto_7

    :cond_c
    move-wide/from16 v36, v30

    :goto_7
    move-wide/from16 v30, v36

    goto :goto_8

    :catch_0
    move-exception v0

    move-object v6, v0

    :try_start_2
    new-instance v0, Lkotlin/text/Regex;

    const-string v13, "-?\\d+"

    invoke-direct {v0, v13}, Lkotlin/text/Regex;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lkotlin/text/Regex;->matches(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "-"

    const/4 v6, 0x2

    const/4 v13, 0x0

    invoke-static {v3, v0, v13, v6}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v0

    if-eqz v0, :cond_d

    move-wide/from16 v34, v36

    :cond_d
    move-wide/from16 v30, v34

    :goto_8
    const/4 v13, 0x1

    const/16 v27, 0x1

    goto :goto_9

    :cond_e
    throw v6
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_f
    const-string v6, "domain"

    const/4 v13, 0x1

    invoke-static {v0, v6, v13}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_12

    :try_start_3
    const-string v0, "."

    const/4 v6, 0x2

    const/4 v13, 0x0

    invoke-static {v3, v0, v13, v6}, Lx/s/m;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v34

    const/4 v6, 0x1

    xor-int/lit8 v13, v34, 0x1

    if-eqz v13, :cond_11

    invoke-static {v3, v0}, Lx/s/r;->removePrefix(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ly/a/g0;->R(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    move-object v9, v0

    const/4 v13, 0x1

    const/16 v28, 0x0

    goto :goto_9

    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_11
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "Failed requirement."

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    const/4 v13, 0x1

    goto :goto_9

    :cond_12
    const-string v6, "path"

    const/4 v13, 0x1

    invoke-static {v0, v6, v13}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_13

    move-object v12, v3

    goto :goto_9

    :cond_13
    const-string v3, "secure"

    invoke-static {v0, v3, v13}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_14

    const/16 v25, 0x1

    goto :goto_9

    :cond_14
    const-string v3, "httponly"

    invoke-static {v0, v3, v13}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_15

    const/16 v26, 0x1

    :cond_15
    :goto_9
    add-int/lit8 v0, v4, 0x1

    move-object/from16 v4, v17

    move-object/from16 v3, v38

    move-object/from16 v6, v39

    const/16 v13, 0x3d

    goto/16 :goto_5

    :cond_16
    move-object/from16 v38, v3

    move-object/from16 v17, v4

    move-object/from16 v39, v6

    const/4 v13, 0x1

    cmp-long v0, v30, v36

    if-nez v0, :cond_17

    move-wide/from16 v21, v36

    goto :goto_a

    :cond_17
    cmp-long v0, v30, v23

    if-eqz v0, :cond_1a

    const-wide v3, 0x20c49ba5e353f7L

    cmp-long v0, v30, v3

    if-gtz v0, :cond_18

    const/16 v0, 0x3e8

    int-to-long v3, v0

    mul-long v34, v30, v3

    :cond_18
    add-long v34, v15, v34

    cmp-long v0, v34, v15

    if-ltz v0, :cond_1b

    cmp-long v0, v34, v21

    if-lez v0, :cond_19

    goto :goto_a

    :cond_19
    move-wide/from16 v21, v34

    goto :goto_a

    :cond_1a
    move-wide/from16 v21, v32

    :cond_1b
    :goto_a
    iget-object v0, v2, Lb0/x;->e:Ljava/lang/String;

    if-nez v9, :cond_1c

    move-object v9, v0

    goto :goto_b

    :cond_1c
    invoke-virtual {v5, v0, v9}, Lb0/n$a;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1d

    goto :goto_c

    :cond_1d
    :goto_b
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v0, v3, :cond_1e

    sget-object v0, Lokhttp3/internal/publicsuffix/PublicSuffixDatabase;->h:Lokhttp3/internal/publicsuffix/PublicSuffixDatabase$a;

    sget-object v0, Lokhttp3/internal/publicsuffix/PublicSuffixDatabase;->g:Lokhttp3/internal/publicsuffix/PublicSuffixDatabase;

    invoke-virtual {v0, v9}, Lokhttp3/internal/publicsuffix/PublicSuffixDatabase;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1e

    :goto_c
    const/4 v3, 0x2

    const/4 v4, 0x0

    goto :goto_f

    :cond_1e
    const-string v0, "/"

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v12, :cond_20

    invoke-static {v12, v0, v4, v3}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v6

    if-nez v6, :cond_1f

    goto :goto_d

    :cond_1f
    move-object/from16 v24, v12

    goto :goto_e

    :cond_20
    :goto_d
    invoke-virtual/range {p1 .. p1}, Lb0/x;->b()Ljava/lang/String;

    move-result-object v6

    const/16 v8, 0x2f

    const/4 v12, 0x6

    invoke-static {v6, v8, v4, v4, v12}, Lx/s/r;->lastIndexOf$default(Ljava/lang/CharSequence;CIZI)I

    move-result v8

    if-eqz v8, :cond_21

    invoke-virtual {v6, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v6, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v6}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_21
    move-object/from16 v24, v0

    :goto_e
    new-instance v0, Lb0/n;

    const/16 v29, 0x0

    move-object/from16 v18, v0

    move-object/from16 v23, v9

    invoke-direct/range {v18 .. v29}, Lb0/n;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_10

    :goto_f
    const/4 v0, 0x0

    :goto_10
    if-eqz v0, :cond_23

    if-nez v11, :cond_22

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v11, v6

    :cond_22
    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_23
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v4, v17

    move-object/from16 v3, v38

    move-object/from16 v6, v39

    const/4 v9, 0x0

    const/4 v12, 0x2

    goto/16 :goto_2

    :cond_24
    move-object/from16 v39, v6

    if-eqz v11, :cond_25

    invoke-static {v11}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    const-string v0, "Collections.unmodifiableList(cookies)"

    invoke-static {v6, v0}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_11

    :cond_25
    move-object/from16 v6, v39

    :goto_11
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    return-void

    :cond_26
    invoke-interface {v1, v2, v6}, Lb0/p;->a(Lb0/x;Ljava/util/List;)V

    return-void
.end method

.method public static final e(Lc0/e;)Z
    .locals 3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lc0/e;->H()Z

    move-result v1

    if-nez v1, :cond_2

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v1, v2}, Lc0/e;->g(J)B

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const/16 v2, 0x20

    if-eq v1, v2, :cond_1

    const/16 v2, 0x2c

    if-eq v1, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lc0/e;->readByte()B

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lc0/e;->readByte()B

    goto :goto_0

    :cond_2
    :goto_1
    return v0
.end method
