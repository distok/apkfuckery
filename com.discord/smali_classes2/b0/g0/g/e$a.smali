.class public final Lb0/g0/g/e$a;
.super Ljava/lang/Object;
.source "RealCall.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/g/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public volatile d:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final e:Lb0/f;

.field public final synthetic f:Lb0/g0/g/e;


# direct methods
.method public constructor <init>(Lb0/g0/g/e;Lb0/f;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb0/f;",
            ")V"
        }
    .end annotation

    const-string v0, "responseCallback"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lb0/g0/g/e$a;->e:Lb0/f;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Lb0/g0/g/e$a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    iget-object v0, v0, Lb0/g0/g/e;->t:Lb0/a0;

    iget-object v0, v0, Lb0/a0;->b:Lb0/x;

    iget-object v0, v0, Lb0/x;->e:Ljava/lang/String;

    return-object v0
.end method

.method public run()V
    .locals 6

    const-string v0, "OkHttp "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    iget-object v1, v1, Lb0/g0/g/e;->t:Lb0/a0;

    iget-object v1, v1, Lb0/a0;->b:Lb0/x;

    invoke-virtual {v1}, Lb0/x;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "currentThread"

    invoke-static {v1, v2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    iget-object v0, v0, Lb0/g0/g/e;->f:Lb0/g0/g/e$c;

    invoke-virtual {v0}, Lc0/b;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v0, 0x0

    :try_start_1
    iget-object v3, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-virtual {v3}, Lb0/g0/g/e;->i()Lokhttp3/Response;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v3, p0, Lb0/g0/g/e$a;->e:Lb0/f;

    iget-object v4, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-interface {v3, v4, v0}, Lb0/f;->a(Lb0/e;Lokhttp3/Response;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    :goto_0
    iget-object v0, v0, Lb0/g0/g/e;->s:Lb0/y;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_4

    :catchall_0
    move-exception v0

    const/4 v3, 0x1

    move-object v3, v0

    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const/4 v3, 0x1

    move-object v3, v0

    const/4 v0, 0x1

    goto :goto_2

    :catchall_1
    move-exception v3

    :goto_1
    :try_start_4
    iget-object v4, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-virtual {v4}, Lb0/g0/g/e;->cancel()V

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "canceled due to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/io/IOException;->addSuppressed(Ljava/lang/Throwable;)V

    iget-object v4, p0, Lb0/g0/g/e$a;->e:Lb0/f;

    iget-object v5, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-interface {v4, v5, v0}, Lb0/f;->b(Lb0/e;Ljava/io/IOException;)V

    :cond_0
    throw v3

    :catchall_2
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v3

    :goto_2
    if-eqz v0, :cond_1

    sget-object v0, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v0, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Callback failure for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-static {v5}, Lb0/g0/g/e;->b(Lb0/g0/g/e;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v0, v4, v5, v3}, Lb0/g0/k/h;->i(Ljava/lang/String;ILjava/lang/Throwable;)V

    goto :goto_3

    :cond_1
    iget-object v0, p0, Lb0/g0/g/e$a;->e:Lb0/f;

    iget-object v4, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    invoke-interface {v0, v4, v3}, Lb0/f;->b(Lb0/e;Ljava/io/IOException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_3
    :try_start_5
    iget-object v0, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    goto :goto_0

    :goto_4
    iget-object v0, v0, Lb0/y;->d:Lb0/q;

    invoke-virtual {v0, p0}, Lb0/q;->c(Lb0/g0/g/e$a;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    return-void

    :goto_5
    :try_start_6
    iget-object v3, p0, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    iget-object v3, v3, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v3, v3, Lb0/y;->d:Lb0/q;

    invoke-virtual {v3, p0}, Lb0/q;->c(Lb0/g0/g/e$a;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception v0

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v0
.end method
