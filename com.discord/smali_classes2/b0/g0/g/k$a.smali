.class public final Lb0/g0/g/k$a;
.super Lb0/g0/f/a;
.source "RealConnectionPool.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb0/g0/g/k;-><init>(Lb0/g0/f/d;IJLjava/util/concurrent/TimeUnit;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic e:Lb0/g0/g/k;


# direct methods
.method public constructor <init>(Lb0/g0/g/k;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/g/k$a;->e:Lb0/g0/g/k;

    const/4 p1, 0x1

    invoke-direct {p0, p2, p1}, Lb0/g0/f/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 14

    iget-object v0, p0, Lb0/g0/g/k$a;->e:Lb0/g0/g/k;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    iget-object v3, v0, Lb0/g0/g/k;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const-wide/high16 v4, -0x8000000000000000L

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v9, v7

    const/4 v8, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lb0/g0/g/j;

    const-string v11, "connection"

    invoke-static {v10, v11}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter v10

    :try_start_0
    invoke-virtual {v0, v10, v1, v2}, Lb0/g0/g/k;->b(Lb0/g0/g/j;J)I

    move-result v11

    if-lez v11, :cond_0

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    iget-wide v11, v10, Lb0/g0/g/j;->p:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v11, v1, v11

    cmp-long v13, v11, v4

    if-lez v13, :cond_1

    move-object v9, v10

    move-wide v4, v11

    :cond_1
    :goto_1
    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    :cond_2
    iget-wide v10, v0, Lb0/g0/g/k;->a:J

    cmp-long v3, v4, v10

    if-gez v3, :cond_6

    iget v3, v0, Lb0/g0/g/k;->e:I

    if-le v6, v3, :cond_3

    goto :goto_2

    :cond_3
    if-lez v6, :cond_4

    sub-long/2addr v10, v4

    goto :goto_3

    :cond_4
    if-lez v8, :cond_5

    goto :goto_3

    :cond_5
    const-wide/16 v10, -0x1

    goto :goto_3

    :cond_6
    :goto_2
    if-eqz v9, :cond_a

    monitor-enter v9

    :try_start_1
    iget-object v3, v9, Lb0/g0/g/j;->o:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v6, 0x1

    xor-int/2addr v3, v6

    const-wide/16 v10, 0x0

    if-eqz v3, :cond_7

    monitor-exit v9

    goto :goto_3

    :cond_7
    :try_start_2
    iget-wide v7, v9, Lb0/g0/g/j;->p:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-long/2addr v7, v4

    cmp-long v3, v7, v1

    if-eqz v3, :cond_8

    monitor-exit v9

    goto :goto_3

    :cond_8
    :try_start_3
    iput-boolean v6, v9, Lb0/g0/g/j;->i:Z

    iget-object v1, v0, Lb0/g0/g/k;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v9}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v9

    invoke-virtual {v9}, Lb0/g0/g/j;->n()Ljava/net/Socket;

    move-result-object v1

    invoke-static {v1}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    iget-object v1, v0, Lb0/g0/g/k;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v0, v0, Lb0/g0/g/k;->b:Lb0/g0/f/c;

    invoke-virtual {v0}, Lb0/g0/f/c;->a()V

    :cond_9
    :goto_3
    return-wide v10

    :catchall_1
    move-exception v0

    monitor-exit v9

    throw v0

    :cond_a
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v7
.end method
