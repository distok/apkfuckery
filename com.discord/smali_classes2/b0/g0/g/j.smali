.class public final Lb0/g0/g/j;
.super Lb0/g0/j/e$c;
.source "RealConnection.kt"

# interfaces
.implements Lb0/k;


# instance fields
.field public b:Ljava/net/Socket;

.field public c:Ljava/net/Socket;

.field public d:Lb0/w;

.field public e:Lb0/z;

.field public f:Lb0/g0/j/e;

.field public g:Lc0/g;

.field public h:Lokio/BufferedSink;

.field public i:Z

.field public j:Z

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/Reference<",
            "Lb0/g0/g/e;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:J

.field public final q:Lb0/e0;


# direct methods
.method public constructor <init>(Lb0/g0/g/k;Lb0/e0;)V
    .locals 1

    const-string v0, "connectionPool"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "route"

    invoke-static {p2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lb0/g0/j/e$c;-><init>()V

    iput-object p2, p0, Lb0/g0/g/j;->q:Lb0/e0;

    const/4 p1, 0x1

    iput p1, p0, Lb0/g0/g/j;->n:I

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lb0/g0/g/j;->o:Ljava/util/List;

    const-wide p1, 0x7fffffffffffffffL

    iput-wide p1, p0, Lb0/g0/g/j;->p:J

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lb0/g0/j/e;Lb0/g0/j/s;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "settings"

    invoke-static {p2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p1, p2, Lb0/g0/j/s;->a:I

    and-int/lit8 p1, p1, 0x10

    if-eqz p1, :cond_0

    iget-object p1, p2, Lb0/g0/j/s;->b:[I

    const/4 p2, 0x4

    aget p1, p1, p2

    goto :goto_0

    :cond_0
    const p1, 0x7fffffff

    :goto_0
    iput p1, p0, Lb0/g0/g/j;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public b(Lb0/g0/j/n;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "stream"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/j/a;->h:Lb0/g0/j/a;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lb0/g0/j/n;->c(Lb0/g0/j/a;Ljava/io/IOException;)V

    return-void
.end method

.method public final c(IIIIZLb0/e;Lb0/t;)V
    .locals 16

    move-object/from16 v7, p0

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    const-string v10, "proxy"

    const-string v11, "inetSocketAddress"

    const-string v12, "call"

    invoke-static {v8, v12}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {v9, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v7, Lb0/g0/g/j;->e:Lb0/z;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_13

    iget-object v0, v7, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v0, v0, Lb0/e0;->a:Lb0/a;

    iget-object v0, v0, Lb0/a;->c:Ljava/util/List;

    new-instance v13, Lb0/g0/g/b;

    invoke-direct {v13, v0}, Lb0/g0/g/b;-><init>(Ljava/util/List;)V

    iget-object v1, v7, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v1, v1, Lb0/e0;->a:Lb0/a;

    iget-object v2, v1, Lb0/a;->f:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v2, :cond_3

    sget-object v1, Lb0/m;->h:Lb0/m;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v7, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v0, v0, Lb0/e0;->a:Lb0/a;

    iget-object v0, v0, Lb0/a;->a:Lb0/x;

    iget-object v0, v0, Lb0/x;->e:Ljava/lang/String;

    sget-object v1, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v1, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {v1, v0}, Lb0/g0/k/h;->h(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Lokhttp3/internal/connection/RouteException;

    new-instance v2, Ljava/net/UnknownServiceException;

    const-string v3, "CLEARTEXT communication to "

    const-string v4, " not permitted by network security policy"

    invoke-static {v3, v0, v4}, Lf/e/c/a/a;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_2
    new-instance v0, Lokhttp3/internal/connection/RouteException;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "CLEARTEXT communication not enabled for client"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_3
    iget-object v0, v1, Lb0/a;->b:Ljava/util/List;

    sget-object v1, Lb0/z;->h:Lb0/z;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    :goto_1
    const/4 v14, 0x0

    move-object v15, v14

    :goto_2
    :try_start_0
    iget-object v0, v7, Lb0/g0/g/j;->q:Lb0/e0;

    invoke-virtual {v0}, Lb0/e0;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-virtual/range {v1 .. v6}, Lb0/g0/g/j;->f(IIILb0/e;Lb0/t;)V

    iget-object v0, v7, Lb0/g0/g/j;->b:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_4

    goto :goto_5

    :cond_4
    move/from16 v1, p1

    move/from16 v2, p2

    goto :goto_4

    :goto_3
    move/from16 v1, p1

    move/from16 v2, p2

    goto :goto_7

    :cond_5
    move/from16 v1, p1

    move/from16 v2, p2

    :try_start_1
    invoke-virtual {v7, v1, v2, v8, v9}, Lb0/g0/g/j;->e(IILb0/e;Lb0/t;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_4
    move/from16 v3, p4

    :try_start_2
    invoke-virtual {v7, v13, v3, v8, v9}, Lb0/g0/g/j;->g(Lb0/g0/g/b;ILb0/e;Lb0/t;)V

    iget-object v0, v7, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v4, v0, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    iget-object v0, v0, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-static {v8, v12}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_5
    iget-object v0, v7, Lb0/g0/g/j;->q:Lb0/e0;

    invoke-virtual {v0}, Lb0/e0;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v7, Lb0/g0/g/j;->b:Ljava/net/Socket;

    if-eqz v0, :cond_6

    goto :goto_6

    :cond_6
    new-instance v0, Lokhttp3/internal/connection/RouteException;

    new-instance v1, Ljava/net/ProtocolException;

    const-string v2, "Too many tunnel connections attempted: 21"

    invoke-direct {v1, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_7
    :goto_6
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, v7, Lb0/g0/g/j;->p:J

    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    :goto_7
    move/from16 v3, p4

    goto :goto_8

    :catch_2
    move-exception v0

    goto :goto_3

    :goto_8
    iget-object v4, v7, Lb0/g0/g/j;->c:Ljava/net/Socket;

    if-eqz v4, :cond_8

    invoke-static {v4}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_8
    iget-object v4, v7, Lb0/g0/g/j;->b:Ljava/net/Socket;

    if-eqz v4, :cond_9

    invoke-static {v4}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_9
    iput-object v14, v7, Lb0/g0/g/j;->c:Ljava/net/Socket;

    iput-object v14, v7, Lb0/g0/g/j;->b:Ljava/net/Socket;

    iput-object v14, v7, Lb0/g0/g/j;->g:Lc0/g;

    iput-object v14, v7, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    iput-object v14, v7, Lb0/g0/g/j;->d:Lb0/w;

    iput-object v14, v7, Lb0/g0/g/j;->e:Lb0/z;

    iput-object v14, v7, Lb0/g0/g/j;->f:Lb0/g0/j/e;

    const/4 v4, 0x1

    iput v4, v7, Lb0/g0/g/j;->n:I

    iget-object v4, v7, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v5, v4, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    iget-object v4, v4, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-static {v8, v12}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "ioe"

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez v15, :cond_a

    new-instance v15, Lokhttp3/internal/connection/RouteException;

    invoke-direct {v15, v0}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    goto :goto_9

    :cond_a
    invoke-virtual {v15, v0}, Lokhttp3/internal/connection/RouteException;->a(Ljava/io/IOException;)V

    :goto_9
    if-eqz p5, :cond_11

    const-string v4, "e"

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    iput-boolean v4, v13, Lb0/g0/g/b;->c:Z

    iget-boolean v4, v13, Lb0/g0/g/b;->b:Z

    if-nez v4, :cond_b

    goto :goto_a

    :cond_b
    instance-of v4, v0, Ljava/net/ProtocolException;

    if-eqz v4, :cond_c

    goto :goto_a

    :cond_c
    instance-of v4, v0, Ljava/io/InterruptedIOException;

    if-eqz v4, :cond_d

    goto :goto_a

    :cond_d
    instance-of v4, v0, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v4, :cond_e

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v4, v4, Ljava/security/cert/CertificateException;

    if-eqz v4, :cond_e

    goto :goto_a

    :cond_e
    instance-of v4, v0, Ljavax/net/ssl/SSLPeerUnverifiedException;

    if-eqz v4, :cond_f

    goto :goto_a

    :cond_f
    instance-of v0, v0, Ljavax/net/ssl/SSLException;

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    goto :goto_b

    :cond_10
    :goto_a
    const/4 v0, 0x0

    :goto_b
    if-eqz v0, :cond_11

    goto/16 :goto_2

    :cond_11
    throw v15

    :cond_12
    new-instance v0, Lokhttp3/internal/connection/RouteException;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_13
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d(Lb0/y;Lb0/e0;Ljava/io/IOException;)V
    .locals 3

    const-string v0, "client"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failedRoute"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failure"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p2, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p2, Lb0/e0;->a:Lb0/a;

    iget-object v1, v0, Lb0/a;->k:Ljava/net/ProxySelector;

    iget-object v0, v0, Lb0/a;->a:Lb0/x;

    invoke-virtual {v0}, Lb0/x;->j()Ljava/net/URI;

    move-result-object v0

    iget-object v2, p2, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-virtual {v2}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    :cond_0
    iget-object p1, p1, Lb0/y;->F:Lb0/g0/g/l;

    monitor-enter p1

    :try_start_0
    const-string p3, "failedRoute"

    invoke-static {p2, p3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p3, p1, Lb0/g0/g/l;->a:Ljava/util/Set;

    invoke-interface {p3, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2
.end method

.method public final e(IILb0/e;Lb0/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "$this$buffer"

    iget-object v1, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v2, v1, Lb0/e0;->b:Ljava/net/Proxy;

    iget-object v1, v1, Lb0/e0;->a:Lb0/a;

    invoke-virtual {v2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    sget-object v4, Lb0/g0/g/f;->a:[I

    invoke-virtual {v3}, Ljava/net/Proxy$Type;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    :goto_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1, v2}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    goto :goto_1

    :cond_1
    iget-object v1, v1, Lb0/a;->e:Ljavax/net/SocketFactory;

    invoke-virtual {v1}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v1

    if-eqz v1, :cond_3

    :goto_1
    iput-object v1, p0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    iget-object v3, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v3, v3, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p4, "call"

    invoke-static {p3, p4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "inetSocketAddress"

    invoke-static {v3, p3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "proxy"

    invoke-static {v2, p3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    :try_start_0
    sget-object p2, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object p2, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    iget-object p3, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object p3, p3, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {p2, v1, p3, p1}, Lb0/g0/k/h;->e(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-static {v1}, Ly/a/g0;->G(Ljava/net/Socket;)Lc0/x;

    move-result-object p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lc0/r;

    invoke-direct {p2, p1}, Lc0/r;-><init>(Lc0/x;)V

    iput-object p2, p0, Lb0/g0/g/j;->g:Lc0/g;

    invoke-static {v1}, Ly/a/g0;->F(Ljava/net/Socket;)Lc0/v;

    move-result-object p1

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lc0/q;

    invoke-direct {p2, p1}, Lc0/q;-><init>(Lc0/v;)V

    iput-object p2, p0, Lb0/g0/g/j;->h:Lokio/BufferedSink;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object p2

    const-string p3, "throw with null exception"

    invoke-static {p2, p3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    :goto_2
    return-void

    :cond_2
    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Ljava/net/ConnectException;

    const-string p3, "Failed to connect to "

    invoke-static {p3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    iget-object p4, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object p4, p4, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Ljava/net/ConnectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw p2

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1
.end method

.method public final f(IIILb0/e;Lb0/t;)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p4

    new-instance v3, Lb0/a0$a;

    invoke-direct {v3}, Lb0/a0$a;-><init>()V

    iget-object v4, v0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v4, v4, Lb0/e0;->a:Lb0/a;

    iget-object v4, v4, Lb0/a;->a:Lb0/x;

    invoke-virtual {v3, v4}, Lb0/a0$a;->g(Lb0/x;)Lb0/a0$a;

    const-string v4, "CONNECT"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lb0/a0$a;->c(Ljava/lang/String;Lokhttp3/RequestBody;)Lb0/a0$a;

    iget-object v4, v0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v4, v4, Lb0/e0;->a:Lb0/a;

    iget-object v4, v4, Lb0/a;->a:Lb0/x;

    const/4 v6, 0x1

    invoke-static {v4, v6}, Lb0/g0/c;->y(Lb0/x;Z)Ljava/lang/String;

    move-result-object v4

    const-string v7, "Host"

    invoke-virtual {v3, v7, v4}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    const-string v4, "Proxy-Connection"

    const-string v7, "Keep-Alive"

    invoke-virtual {v3, v4, v7}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    const-string v4, "User-Agent"

    const-string v7, "okhttp/4.8.0"

    invoke-virtual {v3, v4, v7}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    invoke-virtual {v3}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object v3

    new-instance v4, Lokhttp3/Response$a;

    invoke-direct {v4}, Lokhttp3/Response$a;-><init>()V

    invoke-virtual {v4, v3}, Lokhttp3/Response$a;->g(Lb0/a0;)Lokhttp3/Response$a;

    sget-object v7, Lb0/z;->e:Lb0/z;

    invoke-virtual {v4, v7}, Lokhttp3/Response$a;->f(Lb0/z;)Lokhttp3/Response$a;

    const/16 v7, 0x197

    iput v7, v4, Lokhttp3/Response$a;->c:I

    const-string v7, "Preemptive Authenticate"

    invoke-virtual {v4, v7}, Lokhttp3/Response$a;->e(Ljava/lang/String;)Lokhttp3/Response$a;

    sget-object v7, Lb0/g0/c;->c:Lokhttp3/ResponseBody;

    iput-object v7, v4, Lokhttp3/Response$a;->g:Lokhttp3/ResponseBody;

    const-wide/16 v7, -0x1

    iput-wide v7, v4, Lokhttp3/Response$a;->k:J

    iput-wide v7, v4, Lokhttp3/Response$a;->l:J

    const-string v7, "Proxy-Authenticate"

    const-string v8, "name"

    invoke-static {v7, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "OkHttp-Preemptive"

    const-string v10, "value"

    invoke-static {v9, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v11, v4, Lokhttp3/Response$a;->f:Lokhttp3/Headers$a;

    invoke-static {v11}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v7, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v9, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v8, Lokhttp3/Headers;->e:Lokhttp3/Headers$b;

    invoke-virtual {v8, v7}, Lokhttp3/Headers$b;->a(Ljava/lang/String;)V

    invoke-virtual {v8, v9, v7}, Lokhttp3/Headers$b;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Lokhttp3/Headers$a;->d(Ljava/lang/String;)Lokhttp3/Headers$a;

    invoke-virtual {v11, v7, v9}, Lokhttp3/Headers$a;->b(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$a;

    invoke-virtual {v4}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v4

    iget-object v7, v0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v8, v7, Lb0/e0;->a:Lb0/a;

    iget-object v8, v8, Lb0/a;->i:Lb0/c;

    invoke-interface {v8, v7, v4}, Lb0/c;->a(Lb0/e0;Lokhttp3/Response;)Lb0/a0;

    move-result-object v4

    if-eqz v4, :cond_0

    move-object v3, v4

    :cond_0
    iget-object v4, v3, Lb0/a0;->b:Lb0/x;

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x15

    if-ge v7, v8, :cond_b

    move/from16 v8, p1

    move-object/from16 v9, p5

    invoke-virtual {v0, v8, v1, v2, v9}, Lb0/g0/g/j;->e(IILb0/e;Lb0/t;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CONNECT "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v4, v6}, Lb0/g0/c;->y(Lb0/x;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " HTTP/1.1"

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_1
    iget-object v10, v0, Lb0/g0/g/j;->g:Lc0/g;

    if-eqz v10, :cond_a

    iget-object v11, v0, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    if-eqz v11, :cond_9

    new-instance v12, Lb0/g0/i/b;

    invoke-direct {v12, v5, v0, v10, v11}, Lb0/g0/i/b;-><init>(Lb0/y;Lb0/g0/g/j;Lc0/g;Lokio/BufferedSink;)V

    invoke-interface {v10}, Lc0/x;->timeout()Lc0/y;

    move-result-object v5

    int-to-long v13, v1

    sget-object v15, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v13, v14, v15}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    invoke-interface {v11}, Lc0/v;->timeout()Lc0/y;

    move-result-object v5

    move/from16 v13, p3

    int-to-long v8, v13

    invoke-virtual {v5, v8, v9, v15}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    iget-object v5, v3, Lb0/a0;->d:Lokhttp3/Headers;

    invoke-virtual {v12, v5, v6}, Lb0/g0/i/b;->k(Lokhttp3/Headers;Ljava/lang/String;)V

    iget-object v5, v12, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    invoke-interface {v5}, Lokio/BufferedSink;->flush()V

    const/4 v5, 0x0

    invoke-virtual {v12, v5}, Lb0/g0/i/b;->d(Z)Lokhttp3/Response$a;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v5, v3}, Lokhttp3/Response$a;->g(Lb0/a0;)Lokhttp3/Response$a;

    invoke-virtual {v5}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v3

    const-string v5, "response"

    invoke-static {v3, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lb0/g0/c;->l(Lokhttp3/Response;)J

    move-result-wide v8

    const-wide/16 v16, -0x1

    cmp-long v5, v8, v16

    if-nez v5, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v12, v8, v9}, Lb0/g0/i/b;->j(J)Lc0/x;

    move-result-object v5

    const v8, 0x7fffffff

    invoke-static {v5, v8, v15}, Lb0/g0/c;->v(Lc0/x;ILjava/util/concurrent/TimeUnit;)Z

    check-cast v5, Lb0/g0/i/b$d;

    invoke-virtual {v5}, Lb0/g0/i/b$d;->close()V

    :goto_2
    iget v5, v3, Lokhttp3/Response;->g:I

    const/16 v8, 0xc8

    if-eq v5, v8, :cond_5

    const/16 v8, 0x197

    if-ne v5, v8, :cond_4

    iget-object v5, v0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v8, v5, Lb0/e0;->a:Lb0/a;

    iget-object v8, v8, Lb0/a;->i:Lb0/c;

    invoke-interface {v8, v5, v3}, Lb0/c;->a(Lb0/e0;Lokhttp3/Response;)Lb0/a0;

    move-result-object v5

    if-eqz v5, :cond_3

    const/4 v8, 0x2

    const-string v9, "Connection"

    const/4 v10, 0x0

    invoke-static {v3, v9, v10, v8}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    const-string v8, "close"

    const/4 v9, 0x1

    invoke-static {v8, v3, v9}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v3, v5

    goto :goto_3

    :cond_2
    const/4 v3, 0x0

    move/from16 v8, p1

    move-object/from16 v9, p5

    move-object/from16 v18, v5

    move-object v5, v3

    move-object/from16 v3, v18

    goto/16 :goto_1

    :cond_3
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to authenticate with proxy"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unexpected response code for CONNECT: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v3, Lokhttp3/Response;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    invoke-interface {v10}, Lc0/g;->h()Lc0/e;

    move-result-object v3

    invoke-virtual {v3}, Lc0/e;->H()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v11}, Lokio/BufferedSink;->h()Lc0/e;

    move-result-object v3

    invoke-virtual {v3}, Lc0/e;->H()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_b

    iget-object v5, v0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    if-eqz v5, :cond_6

    invoke-static {v5}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_6
    const/4 v5, 0x0

    iput-object v5, v0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    iput-object v5, v0, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    iput-object v5, v0, Lb0/g0/g/j;->g:Lc0/g;

    iget-object v5, v0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v6, v5, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    iget-object v5, v5, Lb0/e0;->b:Ljava/net/Proxy;

    const-string v8, "call"

    invoke-static {v2, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "inetSocketAddress"

    invoke-static {v6, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "proxy"

    invoke-static {v5, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/lit8 v7, v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_7
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TLS tunnel buffered too many bytes!"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v1, 0x0

    throw v1

    :cond_9
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v5

    :cond_a
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v5

    :cond_b
    return-void
.end method

.method public final g(Lb0/g0/g/b;ILb0/e;Lb0/t;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object p4, Lb0/z;->e:Lb0/z;

    iget-object v0, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v0, v0, Lb0/e0;->a:Lb0/a;

    iget-object v1, v0, Lb0/a;->f:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v1, :cond_1

    iget-object p1, v0, Lb0/a;->b:Ljava/util/List;

    sget-object p3, Lb0/z;->h:Lb0/z;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    iput-object p1, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    iput-object p3, p0, Lb0/g0/g/j;->e:Lb0/z;

    invoke-virtual {p0, p2}, Lb0/g0/g/j;->o(I)V

    return-void

    :cond_0
    iget-object p1, p0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    iput-object p1, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    iput-object p4, p0, Lb0/g0/g/j;->e:Lb0/z;

    return-void

    :cond_1
    const-string v0, "call"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "$this$buffer"

    iget-object v2, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v2, v2, Lb0/e0;->a:Lb0/a;

    iget-object v3, v2, Lb0/a;->f:Ljavax/net/ssl/SSLSocketFactory;

    const/4 v4, 0x0

    if-eqz v3, :cond_c

    :try_start_0
    iget-object v5, p0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    iget-object v6, v2, Lb0/a;->a:Lb0/x;

    iget-object v7, v6, Lb0/x;->e:Ljava/lang/String;

    iget v6, v6, Lb0/x;->f:I

    const/4 v8, 0x1

    invoke-virtual {v3, v5, v7, v6, v8}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v3

    if-eqz v3, :cond_b

    check-cast v3, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p1, v3}, Lb0/g0/g/b;->a(Ljavax/net/ssl/SSLSocket;)Lb0/m;

    move-result-object p1

    iget-boolean v5, p1, Lb0/m;->b:Z

    if-eqz v5, :cond_2

    sget-object v5, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v5, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    iget-object v6, v2, Lb0/a;->a:Lb0/x;

    iget-object v6, v6, Lb0/x;->e:Ljava/lang/String;

    iget-object v7, v2, Lb0/a;->b:Ljava/util/List;

    invoke-virtual {v5, v3, v6, v7}, Lb0/g0/k/h;->d(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V

    :cond_2
    invoke-virtual {v3}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    invoke-virtual {v3}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v5

    const-string v6, "sslSocketSession"

    invoke-static {v5, v6}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5}, Lb0/w;->a(Ljavax/net/ssl/SSLSession;)Lb0/w;

    move-result-object v6

    iget-object v7, v2, Lb0/a;->g:Ljavax/net/ssl/HostnameVerifier;

    if-eqz v7, :cond_a

    iget-object v9, v2, Lb0/a;->a:Lb0/x;

    iget-object v9, v9, Lb0/x;->e:Ljava/lang/String;

    invoke-interface {v7, v9, v5}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v6}, Lb0/w;->c()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v8

    if-eqz p2, :cond_4

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_3

    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.security.cert.X509Certificate"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    check-cast p1, Ljava/security/cert/X509Certificate;

    new-instance p2, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "\n              |Hostname "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, v2, Lb0/a;->a:Lb0/x;

    iget-object p4, p4, Lb0/x;->e:Ljava/lang/String;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, " not verified:\n              |    certificate: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p4, Lb0/g;->d:Lb0/g$a;

    invoke-virtual {p4, p1}, Lb0/g$a;->a(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, "\n              |    DN: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object p4

    const-string v0, "cert.subjectDN"

    invoke-static {p4, v0}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p4}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, "\n              |    subjectAltNames: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p4, Lb0/g0/m/d;->a:Lb0/g0/m/d;

    const-string v0, "certificate"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x7

    invoke-virtual {p4, p1, v0}, Lb0/g0/m/d;->a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p4, p1, v1}, Lb0/g0/m/d;->a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;

    move-result-object p1

    invoke-static {v0, p1}, Lx/h/f;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\n              "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v4, v8}, Lx/s/i;->trimMargin$default(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_4
    new-instance p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Hostname "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, v2, Lb0/a;->a:Lb0/x;

    iget-object p3, p3, Lb0/x;->e:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " not verified (no certificates)"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    iget-object v4, v2, Lb0/a;->h:Lb0/g;

    if-eqz v4, :cond_9

    new-instance v5, Lb0/w;

    iget-object v7, v6, Lb0/w;->b:Lb0/f0;

    iget-object v8, v6, Lb0/w;->c:Lb0/j;

    iget-object v9, v6, Lb0/w;->d:Ljava/util/List;

    new-instance v10, Lb0/g0/g/g;

    invoke-direct {v10, v4, v6, v2}, Lb0/g0/g/g;-><init>(Lb0/g;Lb0/w;Lb0/a;)V

    invoke-direct {v5, v7, v8, v9, v10}, Lb0/w;-><init>(Lb0/f0;Lb0/j;Ljava/util/List;Lkotlin/jvm/functions/Function0;)V

    iput-object v5, p0, Lb0/g0/g/j;->d:Lb0/w;

    iget-object v2, v2, Lb0/a;->a:Lb0/x;

    iget-object v2, v2, Lb0/x;->e:Ljava/lang/String;

    new-instance v5, Lb0/g0/g/h;

    invoke-direct {v5, p0}, Lb0/g0/g/h;-><init>(Lb0/g0/g/j;)V

    invoke-virtual {v4, v2, v5}, Lb0/g;->a(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    iget-boolean p1, p1, Lb0/m;->b:Z

    if-eqz p1, :cond_6

    sget-object p1, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object p1, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {p1, v3}, Lb0/g0/k/h;->f(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_6
    const/4 p1, 0x0

    :goto_0
    iput-object v3, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    invoke-static {v3}, Ly/a/g0;->G(Ljava/net/Socket;)Lc0/x;

    move-result-object v2

    invoke-static {v2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lc0/r;

    invoke-direct {v4, v2}, Lc0/r;-><init>(Lc0/x;)V

    iput-object v4, p0, Lb0/g0/g/j;->g:Lc0/g;

    invoke-static {v3}, Ly/a/g0;->F(Ljava/net/Socket;)Lc0/v;

    move-result-object v2

    invoke-static {v2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lc0/q;

    invoke-direct {v1, v2}, Lc0/q;-><init>(Lc0/v;)V

    iput-object v1, p0, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    if-eqz p1, :cond_7

    sget-object p4, Lb0/z;->k:Lb0/z$a;

    invoke-virtual {p4, p1}, Lb0/z$a;->a(Ljava/lang/String;)Lb0/z;

    move-result-object p4

    :cond_7
    iput-object p4, p0, Lb0/g0/g/j;->e:Lb0/z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object p1, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object p1, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {p1, v3}, Lb0/g0/k/h;->a(Ljavax/net/ssl/SSLSocket;)V

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lb0/g0/g/j;->e:Lb0/z;

    sget-object p3, Lb0/z;->g:Lb0/z;

    if-ne p1, p3, :cond_8

    invoke-virtual {p0, p2}, Lb0/g0/g/j;->o(I)V

    :cond_8
    return-void

    :cond_9
    :try_start_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 p1, 0x0

    throw p1

    :cond_a
    :try_start_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 p1, 0x0

    throw p1

    :catchall_0
    move-exception p1

    goto :goto_1

    :cond_b
    :try_start_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type javax.net.ssl.SSLSocket"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 p1, 0x0

    throw p1

    :catchall_1
    move-exception p1

    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_d

    sget-object p2, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object p2, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {p2, v3}, Lb0/g0/k/h;->a(Ljavax/net/ssl/SSLSocket;)V

    :cond_d
    if-eqz v3, :cond_e

    invoke-static {v3}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_e
    throw p1
.end method

.method public final h(Lb0/a;Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb0/a;",
            "Ljava/util/List<",
            "Lb0/e0;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "address"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/c;->a:[B

    iget-object v0, p0, Lb0/g0/g/j;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lb0/g0/g/j;->n:I

    const/4 v2, 0x0

    if-ge v0, v1, :cond_12

    iget-boolean v0, p0, Lb0/g0/g/j;->i:Z

    if-eqz v0, :cond_0

    goto/16 :goto_7

    :cond_0
    iget-object v0, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v0, v0, Lb0/e0;->a:Lb0/a;

    invoke-virtual {v0, p1}, Lb0/a;->a(Lb0/a;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p1, Lb0/a;->a:Lb0/x;

    iget-object v0, v0, Lb0/x;->e:Ljava/lang/String;

    iget-object v1, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v1, v1, Lb0/e0;->a:Lb0/a;

    iget-object v1, v1, Lb0/a;->a:Lb0/x;

    iget-object v1, v1, Lb0/x;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    return v1

    :cond_2
    iget-object v0, p0, Lb0/g0/g/j;->f:Lb0/g0/j/e;

    if-nez v0, :cond_3

    return v2

    :cond_3
    if-eqz p2, :cond_12

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_1

    :cond_4
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb0/e0;

    iget-object v3, v0, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_6

    iget-object v3, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v3, v3, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_6

    iget-object v3, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v3, v3, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    iget-object v0, v0, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    invoke-static {v3, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    const/4 p2, 0x1

    goto :goto_2

    :cond_7
    :goto_1
    const/4 p2, 0x0

    :goto_2
    if-nez p2, :cond_8

    goto/16 :goto_7

    :cond_8
    iget-object p2, p1, Lb0/a;->g:Ljavax/net/ssl/HostnameVerifier;

    sget-object v0, Lb0/g0/m/d;->a:Lb0/g0/m/d;

    if-eq p2, v0, :cond_9

    return v2

    :cond_9
    iget-object p2, p1, Lb0/a;->a:Lb0/x;

    sget-object v3, Lb0/g0/c;->a:[B

    iget-object v3, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v3, v3, Lb0/e0;->a:Lb0/a;

    iget-object v3, v3, Lb0/a;->a:Lb0/x;

    iget v4, p2, Lb0/x;->f:I

    iget v5, v3, Lb0/x;->f:I

    if-eq v4, v5, :cond_a

    goto :goto_5

    :cond_a
    iget-object v4, p2, Lb0/x;->e:Ljava/lang/String;

    iget-object v3, v3, Lb0/x;->e:Ljava/lang/String;

    invoke-static {v4, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    goto :goto_4

    :cond_b
    iget-boolean v3, p0, Lb0/g0/g/j;->j:Z

    if-nez v3, :cond_e

    iget-object v3, p0, Lb0/g0/g/j;->d:Lb0/w;

    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lb0/w;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v4, v1

    if-eqz v4, :cond_d

    iget-object p2, p2, Lb0/x;->e:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_c

    check-cast v3, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0, p2, v3}, Lb0/g0/m/d;->b(Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    move-result p2

    if-eqz p2, :cond_d

    const/4 p2, 0x1

    goto :goto_3

    :cond_c
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.security.cert.X509Certificate"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_d
    const/4 p2, 0x0

    :goto_3
    if-eqz p2, :cond_e

    :goto_4
    const/4 p2, 0x1

    goto :goto_6

    :cond_e
    :goto_5
    const/4 p2, 0x0

    :goto_6
    if-nez p2, :cond_f

    return v2

    :cond_f
    :try_start_0
    iget-object p2, p1, Lb0/a;->h:Lb0/g;

    const/4 v0, 0x0

    if-eqz p2, :cond_11

    iget-object p1, p1, Lb0/a;->a:Lb0/x;

    iget-object p1, p1, Lb0/x;->e:Ljava/lang/String;

    iget-object v3, p0, Lb0/g0/g/j;->d:Lb0/w;

    if-eqz v3, :cond_10

    invoke-virtual {v3}, Lb0/w;->c()Ljava/util/List;

    move-result-object v0

    const-string v3, "hostname"

    invoke-static {p1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "peerCertificates"

    invoke-static {v0, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lb0/h;

    invoke-direct {v3, p2, v0, p1}, Lb0/h;-><init>(Lb0/g;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p2, p1, v3}, Lb0/g;->a(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return v1

    :cond_10
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    throw v0

    :cond_11
    :try_start_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_1
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_1 .. :try_end_1} :catch_0

    throw v0

    :catch_0
    :cond_12
    :goto_7
    return v2
.end method

.method public final i(Z)Z
    .locals 9

    sget-object v0, Lb0/g0/c;->a:[B

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    const/4 v3, 0x0

    if-eqz v2, :cond_8

    iget-object v4, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    if-eqz v4, :cond_7

    iget-object v5, p0, Lb0/g0/g/j;->g:Lc0/g;

    if-eqz v5, :cond_6

    invoke-virtual {v2}, Ljava/net/Socket;->isClosed()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_5

    invoke-virtual {v4}, Ljava/net/Socket;->isClosed()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v4}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v4}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_2

    :cond_0
    iget-object v2, p0, Lb0/g0/g/j;->f:Lb0/g0/j/e;

    const/4 v6, 0x1

    if-eqz v2, :cond_3

    monitor-enter v2

    :try_start_0
    iget-boolean p1, v2, Lb0/g0/j/e;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    monitor-exit v2

    goto :goto_0

    :cond_1
    :try_start_1
    iget-wide v4, v2, Lb0/g0/j/e;->s:J

    iget-wide v7, v2, Lb0/g0/j/e;->r:J

    cmp-long p1, v4, v7

    if-gez p1, :cond_2

    iget-wide v4, v2, Lb0/g0/j/e;->u:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long p1, v0, v4

    if-ltz p1, :cond_2

    monitor-exit v2

    goto :goto_0

    :cond_2
    monitor-exit v2

    const/4 v3, 0x1

    :goto_0
    return v3

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1

    :cond_3
    monitor-enter p0

    :try_start_2
    iget-wide v7, p0, Lb0/g0/g/j;->p:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    sub-long/2addr v0, v7

    monitor-exit p0

    const-wide v7, 0x2540be400L

    cmp-long v2, v0, v7

    if-ltz v2, :cond_4

    if-eqz p1, :cond_4

    const-string p1, "$this$isHealthy"

    invoke-static {v4, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "source"

    invoke-static {v5, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_3
    invoke-virtual {v4}, Ljava/net/Socket;->getSoTimeout()I

    move-result p1
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :try_start_4
    invoke-virtual {v4, v6}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-interface {v5}, Lc0/g;->H()Z

    move-result v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    xor-int/2addr v0, v6

    :try_start_5
    invoke-virtual {v4, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    move v3, v0

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-virtual {v4, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_0
    const/4 v3, 0x1

    :catch_1
    :goto_1
    return v3

    :cond_4
    return v6

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_5
    :goto_2
    return v3

    :cond_6
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v3

    :cond_7
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v3

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v3
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lb0/g0/g/j;->f:Lb0/g0/j/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final k(Lb0/y;Lb0/g0/h/g;)Lb0/g0/h/d;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    const-string v0, "client"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lb0/g0/g/j;->g:Lc0/g;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lb0/g0/g/j;->f:Lb0/g0/j/e;

    if-eqz v1, :cond_0

    new-instance v0, Lb0/g0/j/l;

    invoke-direct {v0, p1, p0, p2, v1}, Lb0/g0/j/l;-><init>(Lb0/y;Lb0/g0/g/j;Lb0/g0/h/g;Lb0/g0/j/e;)V

    goto :goto_0

    :cond_0
    iget v1, p2, Lb0/g0/h/g;->h:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-interface {v2}, Lc0/x;->timeout()Lc0/y;

    move-result-object v0

    iget v1, p2, Lb0/g0/h/g;->h:I

    int-to-long v4, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    invoke-interface {v3}, Lc0/v;->timeout()Lc0/y;

    move-result-object v0

    iget p2, p2, Lb0/g0/h/g;->i:I

    int-to-long v4, p2

    invoke-virtual {v0, v4, v5, v1}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    new-instance v0, Lb0/g0/i/b;

    invoke-direct {v0, p1, p0, v2, v3}, Lb0/g0/i/b;-><init>(Lb0/y;Lb0/g0/g/j;Lc0/g;Lokio/BufferedSink;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1
.end method

.method public final declared-synchronized l()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lb0/g0/g/j;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public m()Lb0/z;
    .locals 1

    iget-object v0, p0, Lb0/g0/g/j;->e:Lb0/z;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public n()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public final o(I)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    const/4 v1, 0x0

    if-eqz v0, :cond_e

    iget-object v2, p0, Lb0/g0/g/j;->g:Lc0/g;

    if-eqz v2, :cond_d

    iget-object v3, p0, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    if-eqz v3, :cond_c

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    new-instance v4, Lb0/g0/j/e$b;

    sget-object v5, Lb0/g0/f/d;->h:Lb0/g0/f/d;

    const/4 v6, 0x1

    invoke-direct {v4, v6, v5}, Lb0/g0/j/e$b;-><init>(ZLb0/g0/f/d;)V

    iget-object v7, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v7, v7, Lb0/e0;->a:Lb0/a;

    iget-object v7, v7, Lb0/a;->a:Lb0/x;

    iget-object v7, v7, Lb0/x;->e:Ljava/lang/String;

    const-string v8, "socket"

    invoke-static {v0, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "peerName"

    invoke-static {v7, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "source"

    invoke-static {v2, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "sink"

    invoke-static {v3, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v4, Lb0/g0/j/e$b;->a:Ljava/net/Socket;

    iget-boolean v0, v4, Lb0/g0/j/e$b;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lb0/g0/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v8, 0x20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "MockWebServer "

    invoke-static {v0, v7}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, v4, Lb0/g0/j/e$b;->b:Ljava/lang/String;

    iput-object v2, v4, Lb0/g0/j/e$b;->c:Lc0/g;

    iput-object v3, v4, Lb0/g0/j/e$b;->d:Lokio/BufferedSink;

    const-string v0, "listener"

    invoke-static {p0, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p0, v4, Lb0/g0/j/e$b;->e:Lb0/g0/j/e$c;

    iput p1, v4, Lb0/g0/j/e$b;->g:I

    new-instance p1, Lb0/g0/j/e;

    invoke-direct {p1, v4}, Lb0/g0/j/e;-><init>(Lb0/g0/j/e$b;)V

    iput-object p1, p0, Lb0/g0/g/j;->f:Lb0/g0/j/e;

    sget-object v0, Lb0/g0/j/e;->G:Lb0/g0/j/e;

    sget-object v0, Lb0/g0/j/e;->F:Lb0/g0/j/s;

    iget v2, v0, Lb0/g0/j/s;->a:I

    and-int/lit8 v2, v2, 0x10

    const/4 v3, 0x4

    if-eqz v2, :cond_1

    iget-object v0, v0, Lb0/g0/j/s;->b:[I

    aget v0, v0, v3

    goto :goto_1

    :cond_1
    const v0, 0x7fffffff

    :goto_1
    iput v0, p0, Lb0/g0/g/j;->n:I

    const-string v0, "taskRunner"

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p1, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    monitor-enter v0

    :try_start_0
    iget-boolean v2, v0, Lb0/g0/j/o;->f:Z

    if-nez v2, :cond_b

    iget-boolean v2, v0, Lb0/g0/j/o;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v2, :cond_2

    monitor-exit v0

    goto :goto_2

    :cond_2
    :try_start_1
    sget-object v2, Lb0/g0/j/o;->j:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v2, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ">> CONNECTION "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lb0/g0/j/d;->a:Lokio/ByteString;

    invoke-virtual {v7}, Lokio/ByteString;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v7, v1, [Ljava/lang/Object;

    invoke-static {v4, v7}, Lb0/g0/c;->j(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :cond_3
    iget-object v2, v0, Lb0/g0/j/o;->h:Lokio/BufferedSink;

    sget-object v4, Lb0/g0/j/d;->a:Lokio/ByteString;

    invoke-interface {v2, v4}, Lokio/BufferedSink;->r0(Lokio/ByteString;)Lokio/BufferedSink;

    iget-object v2, v0, Lb0/g0/j/o;->h:Lokio/BufferedSink;

    invoke-interface {v2}, Lokio/BufferedSink;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v0

    :goto_2
    iget-object v0, p1, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    iget-object v2, p1, Lb0/g0/j/e;->v:Lb0/g0/j/s;

    monitor-enter v0

    :try_start_2
    const-string v4, "settings"

    invoke-static {v2, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v4, v0, Lb0/g0/j/o;->f:Z

    if-nez v4, :cond_a

    iget v4, v2, Lb0/g0/j/s;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->bitCount(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x6

    invoke-virtual {v0, v1, v4, v3, v1}, Lb0/g0/j/o;->c(IIII)V

    const/16 v4, 0xa

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v4, :cond_8

    shl-int v8, v6, v7

    iget v9, v2, Lb0/g0/j/s;->a:I

    and-int/2addr v8, v9

    if-eqz v8, :cond_4

    const/4 v8, 0x1

    goto :goto_4

    :cond_4
    const/4 v8, 0x0

    :goto_4
    if-nez v8, :cond_5

    goto :goto_6

    :cond_5
    if-eq v7, v3, :cond_7

    const/4 v8, 0x7

    if-eq v7, v8, :cond_6

    move v8, v7

    goto :goto_5

    :cond_6
    const/4 v8, 0x4

    goto :goto_5

    :cond_7
    const/4 v8, 0x3

    :goto_5
    iget-object v9, v0, Lb0/g0/j/o;->h:Lokio/BufferedSink;

    invoke-interface {v9, v8}, Lokio/BufferedSink;->writeShort(I)Lokio/BufferedSink;

    iget-object v8, v0, Lb0/g0/j/o;->h:Lokio/BufferedSink;

    iget-object v9, v2, Lb0/g0/j/s;->b:[I

    aget v9, v9, v7

    invoke-interface {v8, v9}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    :goto_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_8
    iget-object v2, v0, Lb0/g0/j/o;->h:Lokio/BufferedSink;

    invoke-interface {v2}, Lokio/BufferedSink;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    iget-object v0, p1, Lb0/g0/j/e;->v:Lb0/g0/j/s;

    invoke-virtual {v0}, Lb0/g0/j/s;->a()I

    move-result v0

    const v2, 0xffff

    if-eq v0, v2, :cond_9

    iget-object v3, p1, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    sub-int/2addr v0, v2

    int-to-long v6, v0

    invoke-virtual {v3, v1, v6, v7}, Lb0/g0/j/o;->i(IJ)V

    :cond_9
    invoke-virtual {v5}, Lb0/g0/f/d;->f()Lb0/g0/f/c;

    move-result-object v0

    iget-object v5, p1, Lb0/g0/j/e;->g:Ljava/lang/String;

    iget-object v2, p1, Lb0/g0/j/e;->D:Lb0/g0/j/e$d;

    const-wide/16 v7, 0x0

    const/4 v6, 0x1

    new-instance p1, Lb0/g0/f/b;

    move-object v1, p1

    move-object v3, v5

    move v4, v6

    invoke-direct/range {v1 .. v6}, Lb0/g0/f/b;-><init>(Lkotlin/jvm/functions/Function0;Ljava/lang/String;ZLjava/lang/String;Z)V

    invoke-virtual {v0, p1, v7, v8}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void

    :cond_a
    :try_start_3
    new-instance p1, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {p1, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_b
    :try_start_4
    new-instance p1, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {p1, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_c
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_d
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_e
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "Connection{"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v1, v1, Lb0/e0;->a:Lb0/a;

    iget-object v1, v1, Lb0/a;->a:Lb0/x;

    iget-object v1, v1, Lb0/x;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v1, v1, Lb0/e0;->a:Lb0/a;

    iget-object v1, v1, Lb0/a;->a:Lb0/x;

    iget v1, v1, Lb0/x;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, " proxy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v1, v1, Lb0/e0;->b:Ljava/net/Proxy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " hostAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v1, v1, Lb0/e0;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " cipherSuite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/g0/g/j;->d:Lb0/w;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lb0/w;->c:Lb0/j;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "none"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/g0/g/j;->e:Lb0/z;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
