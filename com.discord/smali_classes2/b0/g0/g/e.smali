.class public final Lb0/g0/g/e;
.super Ljava/lang/Object;
.source "RealCall.kt"

# interfaces
.implements Lb0/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/g/e$a;,
        Lb0/g0/g/e$b;
    }
.end annotation


# instance fields
.field public final d:Lb0/g0/g/k;

.field public final e:Lb0/t;

.field public final f:Lb0/g0/g/e$c;

.field public final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public h:Ljava/lang/Object;

.field public i:Lb0/g0/g/d;

.field public j:Lb0/g0/g/j;

.field public k:Z

.field public l:Lb0/g0/g/c;

.field public m:Z

.field public n:Z

.field public o:Z

.field public volatile p:Z

.field public volatile q:Lb0/g0/g/c;

.field public volatile r:Lb0/g0/g/j;

.field public final s:Lb0/y;

.field public final t:Lb0/a0;

.field public final u:Z


# direct methods
.method public constructor <init>(Lb0/y;Lb0/a0;Z)V
    .locals 2

    const-string v0, "client"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalRequest"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/g/e;->s:Lb0/y;

    iput-object p2, p0, Lb0/g0/g/e;->t:Lb0/a0;

    iput-boolean p3, p0, Lb0/g0/g/e;->u:Z

    iget-object p2, p1, Lb0/y;->e:Lb0/l;

    iget-object p2, p2, Lb0/l;->a:Lb0/g0/g/k;

    iput-object p2, p0, Lb0/g0/g/e;->d:Lb0/g0/g/k;

    iget-object p2, p1, Lb0/y;->h:Lb0/t$b;

    invoke-interface {p2, p0}, Lb0/t$b;->a(Lb0/e;)Lb0/t;

    move-result-object p2

    iput-object p2, p0, Lb0/g0/g/e;->e:Lb0/t;

    new-instance p2, Lb0/g0/g/e$c;

    invoke-direct {p2, p0}, Lb0/g0/g/e$c;-><init>(Lb0/g0/g/e;)V

    iget p1, p1, Lb0/y;->z:I

    int-to-long v0, p1

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v0, v1, p1}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    iput-object p2, p0, Lb0/g0/g/e;->f:Lb0/g0/g/e$c;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object p1, p0, Lb0/g0/g/e;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lb0/g0/g/e;->o:Z

    return-void
.end method

.method public static final b(Lb0/g0/g/e;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v1, p0, Lb0/g0/g/e;->p:Z

    if-eqz v1, :cond_0

    const-string v1, "canceled "

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lb0/g0/g/e;->u:Z

    if-eqz v1, :cond_1

    const-string v1, "web socket"

    goto :goto_1

    :cond_1
    const-string v1, "call"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lb0/g0/g/e;->t:Lb0/a0;

    iget-object p0, p0, Lb0/a0;->b:Lb0/x;

    invoke-virtual {p0}, Lb0/x;->i()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public c()Lb0/a0;
    .locals 1

    iget-object v0, p0, Lb0/g0/g/e;->t:Lb0/a0;

    return-object v0
.end method

.method public cancel()V
    .locals 1

    iget-boolean v0, p0, Lb0/g0/g/e;->p:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb0/g0/g/e;->p:Z

    iget-object v0, p0, Lb0/g0/g/e;->q:Lb0/g0/g/c;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v0}, Lb0/g0/h/d;->cancel()V

    :cond_1
    iget-object v0, p0, Lb0/g0/g/e;->r:Lb0/g0/g/j;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lb0/g0/g/j;->b:Ljava/net/Socket;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_2
    iget-object v0, p0, Lb0/g0/g/e;->e:Lb0/t;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "call"

    invoke-static {p0, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    new-instance v0, Lb0/g0/g/e;

    iget-object v1, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v2, p0, Lb0/g0/g/e;->t:Lb0/a0;

    iget-boolean v3, p0, Lb0/g0/g/e;->u:Z

    invoke-direct {v0, v1, v2, v3}, Lb0/g0/g/e;-><init>(Lb0/y;Lb0/a0;Z)V

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lb0/g0/g/e;->p:Z

    return v0
.end method

.method public final e(Lb0/g0/g/j;)V
    .locals 2

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/c;->a:[B

    iget-object v0, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iput-object p1, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    iget-object p1, p1, Lb0/g0/g/j;->o:Ljava/util/List;

    new-instance v0, Lb0/g0/g/e$b;

    iget-object v1, p0, Lb0/g0/g/e;->h:Ljava/lang/Object;

    invoke-direct {v0, p0, v1}, Lb0/g0/g/e$b;-><init>(Lb0/g0/g/e;Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public execute()Lokhttp3/Response;
    .locals 3

    iget-object v0, p0, Lb0/g0/g/e;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb0/g0/g/e;->f:Lb0/g0/g/e$c;

    invoke-virtual {v0}, Lc0/b;->i()V

    invoke-virtual {p0}, Lb0/g0/g/e;->g()V

    :try_start_0
    iget-object v0, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v0, v0, Lb0/y;->d:Lb0/q;

    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v1, "call"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lb0/q;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1, p0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    invoke-virtual {p0}, Lb0/g0/g/e;->i()Lokhttp3/Response;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v1, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v1, v1, Lb0/y;->d:Lb0/q;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "call"

    invoke-static {p0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v1, Lb0/q;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v2, p0}, Lb0/q;->b(Ljava/util/Deque;Ljava/lang/Object;)V

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v1, v1, Lb0/y;->d:Lb0/q;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "call"

    invoke-static {p0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v1, Lb0/q;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v2, p0}, Lb0/q;->b(Ljava/util/Deque;Ljava/lang/Object;)V

    throw v0

    :cond_0
    const-string v0, "Already Executed"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final f(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(TE;)TE;"
        }
    .end annotation

    sget-object v0, Lb0/g0/c;->a:[B

    iget-object v0, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz v0, :cond_4

    monitor-enter v0

    :try_start_0
    invoke-virtual {p0}, Lb0/g0/g/e;->l()Ljava/net/Socket;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    iget-object v2, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-nez v2, :cond_1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_0
    iget-object v1, p0, Lb0/g0/g/e;->e:Lb0/t;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "call"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "connection"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    if-nez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const-string p1, "Check failed."

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_4
    :goto_1
    iget-boolean v0, p0, Lb0/g0/g/e;->k:Z

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lb0/g0/g/e;->f:Lb0/g0/g/e$c;

    invoke-virtual {v0}, Lc0/b;->j()Z

    move-result v0

    if-nez v0, :cond_6

    :goto_2
    move-object v0, p1

    goto :goto_3

    :cond_6
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_7

    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_7
    :goto_3
    if-eqz p1, :cond_9

    iget-object p1, p0, Lb0/g0/g/e;->e:Lb0/t;

    if-eqz v0, :cond_8

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "call"

    invoke-static {p0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "ioe"

    invoke-static {v0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_9
    iget-object p1, p0, Lb0/g0/g/e;->e:Lb0/t;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "call"

    invoke-static {p0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4
    return-object v0
.end method

.method public final g()V
    .locals 2

    sget-object v0, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v0, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    const-string v1, "response.body().close()"

    invoke-virtual {v0, v1}, Lb0/g0/k/h;->g(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lb0/g0/g/e;->h:Ljava/lang/Object;

    iget-object v0, p0, Lb0/g0/g/e;->e:Lb0/t;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "call"

    invoke-static {p0, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final h(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/g/e;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    monitor-exit p0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lb0/g0/g/e;->q:Lb0/g0/g/c;

    if-eqz p1, :cond_0

    iget-object v1, p1, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v1}, Lb0/g0/h/d;->cancel()V

    iget-object v1, p1, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v2, v0}, Lb0/g0/g/e;->j(Lb0/g0/g/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    :cond_0
    iput-object v0, p0, Lb0/g0/g/e;->l:Lb0/g0/g/c;

    return-void

    :cond_1
    :try_start_1
    const-string p1, "released"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final i()Lokhttp3/Response;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v0, v0, Lb0/y;->f:Ljava/util/List;

    invoke-static {v2, v0}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    new-instance v0, Lb0/g0/h/i;

    iget-object v1, p0, Lb0/g0/g/e;->s:Lb0/y;

    invoke-direct {v0, v1}, Lb0/g0/h/i;-><init>(Lb0/y;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lb0/g0/h/a;

    iget-object v1, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v1, v1, Lb0/y;->m:Lb0/p;

    invoke-direct {v0, v1}, Lb0/g0/h/a;-><init>(Lb0/p;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lb0/g0/e/a;

    iget-object v1, p0, Lb0/g0/g/e;->s:Lb0/y;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {v0}, Lb0/g0/e/a;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lb0/g0/g/a;->b:Lb0/g0/g/a;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lb0/g0/g/e;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v0, v0, Lb0/y;->g:Ljava/util/List;

    invoke-static {v2, v0}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    :cond_0
    new-instance v0, Lb0/g0/h/b;

    iget-boolean v1, p0, Lb0/g0/g/e;->u:Z

    invoke-direct {v0, v1}, Lb0/g0/h/b;-><init>(Z)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lb0/g0/h/g;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lb0/g0/g/e;->t:Lb0/a0;

    iget-object v0, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget v6, v0, Lb0/y;->A:I

    iget v7, v0, Lb0/y;->B:I

    iget v8, v0, Lb0/y;->C:I

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lb0/g0/h/g;-><init>(Lb0/g0/g/e;Ljava/util/List;ILb0/g0/g/c;Lb0/a0;III)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lb0/g0/g/e;->t:Lb0/a0;

    invoke-virtual {v9, v2}, Lb0/g0/h/g;->a(Lb0/a0;)Lokhttp3/Response;

    move-result-object v2

    iget-boolean v3, p0, Lb0/g0/g/e;->p:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    invoke-virtual {p0, v1}, Lb0/g0/g/e;->k(Ljava/io/IOException;)Ljava/io/IOException;

    return-object v2

    :cond_1
    :try_start_1
    const-string v3, "$this$closeQuietly"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Lokhttp3/Response;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    :try_start_3
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_1
    move-exception v2

    throw v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-virtual {p0, v0}, Lb0/g0/g/e;->k(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type kotlin.Throwable"

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    move-object v2, v0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, Lb0/g0/g/e;->k(Ljava/io/IOException;)Ljava/io/IOException;

    :cond_3
    throw v2
.end method

.method public final j(Lb0/g0/g/c;ZZLjava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(",
            "Lb0/g0/g/c;",
            "ZZTE;)TE;"
        }
    .end annotation

    const-string v0, "exchange"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/g/e;->q:Lb0/g0/g/c;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    if-eqz p1, :cond_0

    return-object p4

    :cond_0
    monitor-enter p0

    const/4 p1, 0x0

    if-eqz p2, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lb0/g0/g/e;->m:Z

    if-nez v1, :cond_2

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :cond_1
    :goto_0
    if-eqz p3, :cond_7

    iget-boolean v1, p0, Lb0/g0/g/e;->n:Z

    if-eqz v1, :cond_7

    :cond_2
    if-eqz p2, :cond_3

    iput-boolean p1, p0, Lb0/g0/g/e;->m:Z

    :cond_3
    if-eqz p3, :cond_4

    iput-boolean p1, p0, Lb0/g0/g/e;->n:Z

    :cond_4
    iget-boolean p2, p0, Lb0/g0/g/e;->m:Z

    if-nez p2, :cond_5

    iget-boolean p3, p0, Lb0/g0/g/e;->n:Z

    if-nez p3, :cond_5

    const/4 p3, 0x1

    goto :goto_1

    :cond_5
    const/4 p3, 0x0

    :goto_1
    if-nez p2, :cond_6

    iget-boolean p2, p0, Lb0/g0/g/e;->n:Z

    if-nez p2, :cond_6

    iget-boolean p2, p0, Lb0/g0/g/e;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_6

    const/4 p1, 0x1

    :cond_6
    move p2, p1

    move p1, p3

    goto :goto_3

    :goto_2
    monitor-exit p0

    throw p1

    :cond_7
    const/4 p2, 0x0

    :goto_3
    monitor-exit p0

    if-eqz p1, :cond_8

    const/4 p1, 0x0

    iput-object p1, p0, Lb0/g0/g/e;->q:Lb0/g0/g/c;

    iget-object p1, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz p1, :cond_8

    monitor-enter p1

    :try_start_1
    iget p3, p1, Lb0/g0/g/j;->l:I

    add-int/2addr p3, v0

    iput p3, p1, Lb0/g0/g/j;->l:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p1

    goto :goto_4

    :catchall_1
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_8
    :goto_4
    if-eqz p2, :cond_9

    invoke-virtual {p0, p4}, Lb0/g0/g/e;->f(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1

    :cond_9
    return-object p4
.end method

.method public final k(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/g/e;->o:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lb0/g0/g/e;->o:Z

    iget-boolean v0, p0, Lb0/g0/g/e;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb0/g0/g/e;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x1

    :cond_0
    monitor-exit p0

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Lb0/g0/g/e;->f(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    :cond_1
    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final l()Ljava/net/Socket;
    .locals 8

    iget-object v0, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    sget-object v2, Lb0/g0/c;->a:[B

    iget-object v2, v0, Lb0/g0/g/j;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, -0x1

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/Reference;

    invoke-virtual {v6}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lb0/g0/g/e;

    invoke-static {v6, p0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, -0x1

    :goto_1
    const/4 v3, 0x1

    if-eq v5, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-eqz v6, :cond_7

    invoke-interface {v2, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iput-object v1, p0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v5

    iput-wide v5, v0, Lb0/g0/g/j;->p:J

    iget-object v2, p0, Lb0/g0/g/e;->d:Lb0/g0/g/k;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "connection"

    invoke-static {v0, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lb0/g0/c;->a:[B

    iget-boolean v5, v0, Lb0/g0/g/j;->i:Z

    if-nez v5, :cond_4

    iget v5, v2, Lb0/g0/g/k;->e:I

    if-nez v5, :cond_3

    goto :goto_3

    :cond_3
    iget-object v3, v2, Lb0/g0/g/k;->b:Lb0/g0/f/c;

    iget-object v2, v2, Lb0/g0/g/k;->c:Lb0/g0/g/k$a;

    const-wide/16 v5, 0x0

    const/4 v7, 0x2

    invoke-static {v3, v2, v5, v6, v7}, Lb0/g0/f/c;->d(Lb0/g0/f/c;Lb0/g0/f/a;JI)V

    goto :goto_4

    :cond_4
    :goto_3
    iput-boolean v3, v0, Lb0/g0/g/j;->i:Z

    iget-object v3, v2, Lb0/g0/g/k;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    iget-object v3, v2, Lb0/g0/g/k;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v2, v2, Lb0/g0/g/k;->b:Lb0/g0/f/c;

    invoke-virtual {v2}, Lb0/g0/f/c;->a()V

    :cond_5
    const/4 v4, 0x1

    :goto_4
    if-eqz v4, :cond_6

    invoke-virtual {v0}, Lb0/g0/g/j;->n()Ljava/net/Socket;

    move-result-object v0

    return-object v0

    :cond_6
    return-object v1

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1
.end method

.method public final m()V
    .locals 2

    iget-boolean v0, p0, Lb0/g0/g/e;->k:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lb0/g0/g/e;->k:Z

    iget-object v0, p0, Lb0/g0/g/e;->f:Lb0/g0/g/e$c;

    invoke-virtual {v0}, Lc0/b;->j()Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public t(Lb0/f;)V
    .locals 5

    const-string v0, "responseCallback"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/g/e;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lb0/g0/g/e;->g()V

    iget-object v0, p0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v0, v0, Lb0/y;->d:Lb0/q;

    new-instance v1, Lb0/g0/g/e$a;

    invoke-direct {v1, p0, p1}, Lb0/g0/g/e$a;-><init>(Lb0/g0/g/e;Lb0/f;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "call"

    invoke-static {v1, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter v0

    :try_start_0
    iget-object p1, v0, Lb0/q;->b:Ljava/util/ArrayDeque;

    invoke-virtual {p1, v1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    iget-object p1, v1, Lb0/g0/g/e$a;->f:Lb0/g0/g/e;

    iget-boolean p1, p1, Lb0/g0/g/e;->u:Z

    if-nez p1, :cond_4

    invoke-virtual {v1}, Lb0/g0/g/e$a;->a()Ljava/lang/String;

    move-result-object p1

    iget-object v2, v0, Lb0/q;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lb0/g0/g/e$a;

    invoke-virtual {v3}, Lb0/g0/g/e$a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lb0/q;->b:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lb0/g0/g/e$a;

    invoke-virtual {v3}, Lb0/g0/g/e$a;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_4

    const-string p1, "other"

    invoke-static {v3, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, v3, Lb0/g0/g/e$a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, v1, Lb0/g0/g/e$a;->d:Ljava/util/concurrent/atomic/AtomicInteger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    monitor-exit v0

    invoke-virtual {v0}, Lb0/q;->d()Z

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_5
    const-string p1, "Already Executed"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
