.class public final Lb0/g0/g/a;
.super Ljava/lang/Object;
.source "ConnectInterceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# static fields
.field public static final b:Lb0/g0/g/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lb0/g0/g/a;

    invoke-direct {v0}, Lb0/g0/g/a;-><init>()V

    sput-object v0, Lb0/g0/g/a;->b:Lb0/g0/g/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "chain"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lb0/g0/h/g;

    iget-object v0, p1, Lb0/g0/h/g;->b:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "chain"

    invoke-static {p1, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lb0/g0/g/e;->o:Z

    if-eqz v1, :cond_4

    iget-boolean v1, v0, Lb0/g0/g/e;->n:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lb0/g0/g/e;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_2

    monitor-exit v0

    iget-object v1, v0, Lb0/g0/g/e;->i:Lb0/g0/g/d;

    if-eqz v1, :cond_1

    iget-object v10, v0, Lb0/g0/g/e;->s:Lb0/y;

    const-string v3, "client"

    invoke-static {v10, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "chain"

    invoke-static {p1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_1
    iget v4, p1, Lb0/g0/h/g;->g:I

    iget v5, p1, Lb0/g0/h/g;->h:I

    iget v6, p1, Lb0/g0/h/g;->i:I

    iget v7, v10, Lb0/y;->D:I

    iget-boolean v8, v10, Lb0/y;->i:Z

    iget-object v3, p1, Lb0/g0/h/g;->f:Lb0/a0;

    iget-object v3, v3, Lb0/a0;->c:Ljava/lang/String;

    const-string v9, "GET"

    invoke-static {v3, v9}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/lit8 v9, v3, 0x1

    move-object v3, v1

    invoke-virtual/range {v3 .. v9}, Lb0/g0/g/d;->a(IIIIZZ)Lb0/g0/g/j;

    move-result-object v3

    invoke-virtual {v3, v10, p1}, Lb0/g0/g/j;->k(Lb0/y;Lb0/g0/h/g;)Lb0/g0/h/d;

    move-result-object v3
    :try_end_1
    .catch Lokhttp3/internal/connection/RouteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    new-instance v4, Lb0/g0/g/c;

    iget-object v5, v0, Lb0/g0/g/e;->e:Lb0/t;

    invoke-direct {v4, v0, v5, v1, v3}, Lb0/g0/g/c;-><init>(Lb0/g0/g/e;Lb0/t;Lb0/g0/g/d;Lb0/g0/h/d;)V

    iput-object v4, v0, Lb0/g0/g/e;->l:Lb0/g0/g/c;

    iput-object v4, v0, Lb0/g0/g/e;->q:Lb0/g0/g/c;

    monitor-enter v0

    :try_start_2
    iput-boolean v2, v0, Lb0/g0/g/e;->m:Z

    iput-boolean v2, v0, Lb0/g0/g/e;->n:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    iget-boolean v0, v0, Lb0/g0/g/e;->p:Z

    if-nez v0, :cond_0

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3d

    move-object v1, p1

    move-object v3, v4

    move-object v4, v0

    invoke-static/range {v1 .. v8}, Lb0/g0/h/g;->d(Lb0/g0/h/g;ILb0/g0/g/c;Lb0/a0;IIII)Lb0/g0/h/g;

    move-result-object v0

    iget-object p1, p1, Lb0/g0/h/g;->f:Lb0/a0;

    invoke-virtual {v0, p1}, Lb0/g0/h/g;->a(Lb0/a0;)Lokhttp3/Response;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Canceled"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1

    :catch_0
    move-exception p1

    invoke-virtual {v1, p1}, Lb0/g0/g/d;->c(Ljava/io/IOException;)V

    new-instance v0, Lokhttp3/internal/connection/RouteException;

    invoke-direct {v0, p1}, Lokhttp3/internal/connection/RouteException;-><init>(Ljava/io/IOException;)V

    throw v0

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Lokhttp3/internal/connection/RouteException;->c()Ljava/io/IOException;

    move-result-object v0

    invoke-virtual {v1, v0}, Lb0/g0/g/d;->c(Ljava/io/IOException;)V

    throw p1

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_2
    :try_start_3
    const-string p1, "Check failed."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string p1, "Check failed."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const-string p1, "released"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1
.end method
