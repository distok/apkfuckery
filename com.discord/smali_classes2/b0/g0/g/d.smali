.class public final Lb0/g0/g/d;
.super Ljava/lang/Object;
.source "ExchangeFinder.kt"


# instance fields
.field public a:Lb0/g0/g/m$a;

.field public b:Lb0/g0/g/m;

.field public c:I

.field public d:I

.field public e:I

.field public f:Lb0/e0;

.field public final g:Lb0/g0/g/k;

.field public final h:Lb0/a;

.field public final i:Lb0/g0/g/e;

.field public final j:Lb0/t;


# direct methods
.method public constructor <init>(Lb0/g0/g/k;Lb0/a;Lb0/g0/g/e;Lb0/t;)V
    .locals 1

    const-string v0, "connectionPool"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {p4, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/g/d;->g:Lb0/g0/g/k;

    iput-object p2, p0, Lb0/g0/g/d;->h:Lb0/a;

    iput-object p3, p0, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iput-object p4, p0, Lb0/g0/g/d;->j:Lb0/t;

    return-void
.end method


# virtual methods
.method public final a(IIIIZZ)Lb0/g0/g/j;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object v1, p0

    :goto_0
    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-boolean v0, v0, Lb0/g0/g/e;->p:Z

    if-nez v0, :cond_27

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v2, v0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_6

    monitor-enter v2

    :try_start_0
    iget-boolean v5, v2, Lb0/g0/g/j;->i:Z

    if-nez v5, :cond_1

    iget-object v5, v2, Lb0/g0/g/j;->q:Lb0/e0;

    iget-object v5, v5, Lb0/e0;->a:Lb0/a;

    iget-object v5, v5, Lb0/a;->a:Lb0/x;

    invoke-virtual {p0, v5}, Lb0/g0/g/d;->b(Lb0/x;)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    :cond_0
    move-object v5, v4

    goto :goto_2

    :cond_1
    :goto_1
    iget-object v5, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-virtual {v5}, Lb0/g0/g/e;->l()Ljava/net/Socket;

    move-result-object v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    monitor-exit v2

    iget-object v6, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v6, v6, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz v6, :cond_4

    if-nez v5, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v0, :cond_3

    goto :goto_4

    :cond_3
    const-string v0, "Check failed."

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    if-eqz v5, :cond_5

    invoke-static {v5}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    :cond_5
    iget-object v5, v1, Lb0/g0/g/d;->j:Lb0/t;

    iget-object v6, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "call"

    invoke-static {v6, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "connection"

    invoke-static {v2, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_6
    :goto_3
    iput v0, v1, Lb0/g0/g/d;->c:I

    iput v0, v1, Lb0/g0/g/d;->d:I

    iput v0, v1, Lb0/g0/g/d;->e:I

    iget-object v2, v1, Lb0/g0/g/d;->g:Lb0/g0/g/k;

    iget-object v5, v1, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v6, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-virtual {v2, v5, v6, v4, v0}, Lb0/g0/g/k;->a(Lb0/a;Lb0/g0/g/e;Ljava/util/List;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v2, v0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz v2, :cond_7

    iget-object v4, v1, Lb0/g0/g/d;->j:Lb0/t;

    invoke-virtual {v4, v0, v2}, Lb0/t;->a(Lb0/e;Lb0/k;)V

    :goto_4
    move/from16 v0, p6

    goto/16 :goto_e

    :cond_7
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_8
    iget-object v2, v1, Lb0/g0/g/d;->f:Lb0/e0;

    if-eqz v2, :cond_9

    iput-object v4, v1, Lb0/g0/g/d;->f:Lb0/e0;

    goto :goto_5

    :cond_9
    iget-object v2, v1, Lb0/g0/g/d;->a:Lb0/g0/g/m$a;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lb0/g0/g/m$a;->a()Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v0, v1, Lb0/g0/g/d;->a:Lb0/g0/g/m$a;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lb0/g0/g/m$a;->b()Lb0/e0;

    move-result-object v2

    :goto_5
    move-object v5, v4

    goto/16 :goto_d

    :cond_a
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_b
    iget-object v2, v1, Lb0/g0/g/d;->b:Lb0/g0/g/m;

    if-nez v2, :cond_c

    new-instance v2, Lb0/g0/g/m;

    iget-object v5, v1, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v6, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v7, v6, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v7, v7, Lb0/y;->F:Lb0/g0/g/l;

    iget-object v8, v1, Lb0/g0/g/d;->j:Lb0/t;

    invoke-direct {v2, v5, v7, v6, v8}, Lb0/g0/g/m;-><init>(Lb0/a;Lb0/g0/g/l;Lb0/e;Lb0/t;)V

    iput-object v2, v1, Lb0/g0/g/d;->b:Lb0/g0/g/m;

    :cond_c
    invoke-virtual {v2}, Lb0/g0/g/m;->a()Z

    move-result v5

    if-eqz v5, :cond_26

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :cond_d
    invoke-virtual {v2}, Lb0/g0/g/m;->b()Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-virtual {v2}, Lb0/g0/g/m;->b()Z

    move-result v6

    const-string v7, "No route to "

    if-eqz v6, :cond_18

    iget-object v6, v2, Lb0/g0/g/m;->a:Ljava/util/List;

    iget v8, v2, Lb0/g0/g/m;->b:I

    add-int/lit8 v9, v8, 0x1

    iput v9, v2, Lb0/g0/g/m;->b:I

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/net/Proxy;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, v2, Lb0/g0/g/m;->c:Ljava/util/List;

    invoke-virtual {v6}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v9

    sget-object v10, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v9, v10, :cond_11

    invoke-virtual {v6}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v9

    sget-object v10, Ljava/net/Proxy$Type;->SOCKS:Ljava/net/Proxy$Type;

    if-ne v9, v10, :cond_e

    goto :goto_7

    :cond_e
    invoke-virtual {v6}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v9

    instance-of v10, v9, Ljava/net/InetSocketAddress;

    if-eqz v10, :cond_10

    check-cast v9, Ljava/net/InetSocketAddress;

    const-string v10, "$this$socketHost"

    invoke-static {v9, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v10

    if-eqz v10, :cond_f

    invoke-virtual {v10}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    const-string v11, "address.hostAddress"

    invoke-static {v10, v11}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    :cond_f
    invoke-virtual {v9}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "hostName"

    invoke-static {v10, v11}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    invoke-virtual {v9}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v9

    goto :goto_8

    :cond_10
    const-string v0, "Proxy.address() is not an InetSocketAddress: "

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    :goto_7
    iget-object v9, v2, Lb0/g0/g/m;->e:Lb0/a;

    iget-object v9, v9, Lb0/a;->a:Lb0/x;

    iget-object v10, v9, Lb0/x;->e:Ljava/lang/String;

    iget v9, v9, Lb0/x;->f:I

    :goto_8
    const v11, 0xffff

    if-gt v3, v9, :cond_17

    if-lt v11, v9, :cond_17

    invoke-virtual {v6}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v7

    sget-object v11, Ljava/net/Proxy$Type;->SOCKS:Ljava/net/Proxy$Type;

    if-ne v7, v11, :cond_12

    invoke-static {v10, v9}, Ljava/net/InetSocketAddress;->createUnresolved(Ljava/lang/String;I)Ljava/net/InetSocketAddress;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_12
    iget-object v7, v2, Lb0/g0/g/m;->h:Lb0/t;

    iget-object v11, v2, Lb0/g0/g/m;->g:Lb0/e;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "call"

    invoke-static {v11, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v11, "domainName"

    invoke-static {v10, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v12, v2, Lb0/g0/g/m;->e:Lb0/a;

    iget-object v12, v12, Lb0/a;->d:Lb0/s;

    invoke-interface {v12, v10}, Lb0/s;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_16

    iget-object v13, v2, Lb0/g0/g/m;->h:Lb0/t;

    iget-object v14, v2, Lb0/g0/g/m;->g:Lb0/e;

    invoke-static {v13}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v14, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10, v11}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "inetAddressList"

    invoke-static {v12, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_13

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/net/InetAddress;

    new-instance v11, Ljava/net/InetSocketAddress;

    invoke-direct {v11, v10, v9}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_13
    :goto_a
    iget-object v7, v2, Lb0/g0/g/m;->c:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_b
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_15

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/net/InetSocketAddress;

    new-instance v9, Lb0/e0;

    iget-object v10, v2, Lb0/g0/g/m;->e:Lb0/a;

    invoke-direct {v9, v10, v6, v8}, Lb0/e0;-><init>(Lb0/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;)V

    iget-object v8, v2, Lb0/g0/g/m;->f:Lb0/g0/g/l;

    monitor-enter v8

    :try_start_1
    const-string v10, "route"

    invoke-static {v9, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v10, v8, Lb0/g0/g/l;->a:Ljava/util/Set;

    invoke-interface {v10, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v8

    if-eqz v10, :cond_14

    iget-object v8, v2, Lb0/g0/g/m;->d:Ljava/util/List;

    invoke-interface {v8, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_14
    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :catchall_1
    move-exception v0

    monitor-exit v8

    throw v0

    :cond_15
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    xor-int/2addr v6, v3

    if-eqz v6, :cond_d

    goto :goto_c

    :cond_16
    new-instance v0, Ljava/net/UnknownHostException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v2, Lb0/g0/g/m;->e:Lb0/a;

    iget-object v2, v2, Lb0/a;->d:Lb0/s;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " returned no addresses for "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    new-instance v0, Ljava/net/SocketException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "; port is out of range"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    new-instance v0, Ljava/net/SocketException;

    invoke-static {v7}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lb0/g0/g/m;->e:Lb0/a;

    iget-object v4, v4, Lb0/a;->a:Lb0/x;

    iget-object v4, v4, Lb0/x;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "; exhausted proxy configurations: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lb0/g0/g/m;->a:Ljava/util/List;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/net/SocketException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_19
    :goto_c
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1a

    iget-object v6, v2, Lb0/g0/g/m;->d:Ljava/util/List;

    invoke-static {v5, v6}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    iget-object v2, v2, Lb0/g0/g/m;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :cond_1a
    new-instance v2, Lb0/g0/g/m$a;

    invoke-direct {v2, v5}, Lb0/g0/g/m$a;-><init>(Ljava/util/List;)V

    iput-object v2, v1, Lb0/g0/g/d;->a:Lb0/g0/g/m$a;

    iget-object v5, v2, Lb0/g0/g/m$a;->b:Ljava/util/List;

    iget-object v6, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-boolean v6, v6, Lb0/g0/g/e;->p:Z

    if-nez v6, :cond_25

    iget-object v6, v1, Lb0/g0/g/d;->g:Lb0/g0/g/k;

    iget-object v7, v1, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v8, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-virtual {v6, v7, v8, v5, v0}, Lb0/g0/g/k;->a(Lb0/a;Lb0/g0/g/e;Ljava/util/List;Z)Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v2, v0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz v2, :cond_1b

    iget-object v4, v1, Lb0/g0/g/d;->j:Lb0/t;

    invoke-virtual {v4, v0, v2}, Lb0/t;->a(Lb0/e;Lb0/k;)V

    goto/16 :goto_4

    :cond_1b
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_1c
    invoke-virtual {v2}, Lb0/g0/g/m$a;->b()Lb0/e0;

    move-result-object v2

    :goto_d
    new-instance v14, Lb0/g0/g/j;

    iget-object v0, v1, Lb0/g0/g/d;->g:Lb0/g0/g/k;

    invoke-direct {v14, v0, v2}, Lb0/g0/g/j;-><init>(Lb0/g0/g/k;Lb0/e0;)V

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iput-object v14, v0, Lb0/g0/g/e;->r:Lb0/g0/g/j;

    :try_start_2
    iget-object v12, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v13, v1, Lb0/g0/g/d;->j:Lb0/t;

    move-object v6, v14

    move/from16 v7, p1

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-virtual/range {v6 .. v13}, Lb0/g0/g/j;->c(IIIIZLb0/e;Lb0/t;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iput-object v4, v0, Lb0/g0/g/e;->r:Lb0/g0/g/j;

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v0, v0, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v6, v0, Lb0/y;->F:Lb0/g0/g/l;

    iget-object v0, v14, Lb0/g0/g/j;->q:Lb0/e0;

    monitor-enter v6

    :try_start_3
    const-string v7, "route"

    invoke-static {v0, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v7, v6, Lb0/g0/g/l;->a:Ljava/util/Set;

    invoke-interface {v7, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    monitor-exit v6

    iget-object v0, v1, Lb0/g0/g/d;->g:Lb0/g0/g/k;

    iget-object v6, v1, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v7, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-virtual {v0, v6, v7, v5, v3}, Lb0/g0/g/k;->a(Lb0/a;Lb0/g0/g/e;Ljava/util/List;Z)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iget-object v0, v0, Lb0/g0/g/e;->j:Lb0/g0/g/j;

    if-eqz v0, :cond_1d

    iput-object v2, v1, Lb0/g0/g/d;->f:Lb0/e0;

    invoke-virtual {v14}, Lb0/g0/g/j;->n()Ljava/net/Socket;

    move-result-object v2

    invoke-static {v2}, Lb0/g0/c;->e(Ljava/net/Socket;)V

    iget-object v2, v1, Lb0/g0/g/d;->j:Lb0/t;

    iget-object v4, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-virtual {v2, v4, v0}, Lb0/t;->a(Lb0/e;Lb0/k;)V

    move-object v2, v0

    goto/16 :goto_4

    :cond_1d
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_1e
    monitor-enter v14

    :try_start_4
    iget-object v0, v1, Lb0/g0/g/d;->g:Lb0/g0/g/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "connection"

    invoke-static {v14, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lb0/g0/c;->a:[B

    iget-object v2, v0, Lb0/g0/g/k;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, v14}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lb0/g0/g/k;->b:Lb0/g0/f/c;

    iget-object v0, v0, Lb0/g0/g/k;->c:Lb0/g0/g/k$a;

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    invoke-static {v2, v0, v4, v5, v6}, Lb0/g0/f/c;->d(Lb0/g0/f/c;Lb0/g0/f/a;JI)V

    iget-object v0, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-virtual {v0, v14}, Lb0/g0/g/e;->e(Lb0/g0/g/j;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    monitor-exit v14

    iget-object v0, v1, Lb0/g0/g/d;->j:Lb0/t;

    iget-object v2, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "call"

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connection"

    invoke-static {v14, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v0, p6

    move-object v2, v14

    :goto_e
    invoke-virtual {v2, v0}, Lb0/g0/g/j;->i(Z)Z

    move-result v4

    if-eqz v4, :cond_1f

    return-object v2

    :cond_1f
    invoke-virtual {v2}, Lb0/g0/g/j;->l()V

    iget-object v2, v1, Lb0/g0/g/d;->f:Lb0/e0;

    if-eqz v2, :cond_20

    goto/16 :goto_0

    :cond_20
    iget-object v2, v1, Lb0/g0/g/d;->a:Lb0/g0/g/m$a;

    if-eqz v2, :cond_21

    invoke-virtual {v2}, Lb0/g0/g/m$a;->a()Z

    move-result v2

    goto :goto_f

    :cond_21
    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_22

    goto/16 :goto_0

    :cond_22
    iget-object v2, v1, Lb0/g0/g/d;->b:Lb0/g0/g/m;

    if-eqz v2, :cond_23

    invoke-virtual {v2}, Lb0/g0/g/m;->a()Z

    move-result v3

    :cond_23
    if-eqz v3, :cond_24

    goto/16 :goto_0

    :cond_24
    new-instance v0, Ljava/io/IOException;

    const-string v2, "exhausted all routes"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v14

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit v6

    throw v0

    :catchall_4
    move-exception v0

    iget-object v2, v1, Lb0/g0/g/d;->i:Lb0/g0/g/e;

    iput-object v4, v2, Lb0/g0/g/e;->r:Lb0/g0/g/j;

    throw v0

    :cond_25
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_26
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_27
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Lb0/x;)Z
    .locals 3

    const-string v0, "url"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/g/d;->h:Lb0/a;

    iget-object v0, v0, Lb0/a;->a:Lb0/x;

    iget v1, p1, Lb0/x;->f:I

    iget v2, v0, Lb0/x;->f:I

    if-ne v1, v2, :cond_0

    iget-object p1, p1, Lb0/x;->e:Ljava/lang/String;

    iget-object v0, v0, Lb0/x;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final c(Ljava/io/IOException;)V
    .locals 2

    const-string v0, "e"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lb0/g0/g/d;->f:Lb0/e0;

    instance-of v0, p1, Lokhttp3/internal/http2/StreamResetException;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lokhttp3/internal/http2/StreamResetException;

    iget-object v0, v0, Lokhttp3/internal/http2/StreamResetException;->errorCode:Lb0/g0/j/a;

    sget-object v1, Lb0/g0/j/a;->h:Lb0/g0/j/a;

    if-ne v0, v1, :cond_0

    iget p1, p0, Lb0/g0/g/d;->c:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lb0/g0/g/d;->c:I

    goto :goto_0

    :cond_0
    instance-of p1, p1, Lokhttp3/internal/http2/ConnectionShutdownException;

    if-eqz p1, :cond_1

    iget p1, p0, Lb0/g0/g/d;->d:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lb0/g0/g/d;->d:I

    goto :goto_0

    :cond_1
    iget p1, p0, Lb0/g0/g/d;->e:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lb0/g0/g/d;->e:I

    :goto_0
    return-void
.end method
