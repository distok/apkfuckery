.class public final Lb0/g0/g/m;
.super Ljava/lang/Object;
.source "RouteSelector.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/g/m$a;
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Ljava/net/Proxy;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Ljava/net/InetSocketAddress;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/e0;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lb0/a;

.field public final f:Lb0/g0/g/l;

.field public final g:Lb0/e;

.field public final h:Lb0/t;


# direct methods
.method public constructor <init>(Lb0/a;Lb0/g0/g/l;Lb0/e;Lb0/t;)V
    .locals 2

    const-string v0, "address"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "routeDatabase"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "eventListener"

    invoke-static {p4, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/g/m;->e:Lb0/a;

    iput-object p2, p0, Lb0/g0/g/m;->f:Lb0/g0/g/l;

    iput-object p3, p0, Lb0/g0/g/m;->g:Lb0/e;

    iput-object p4, p0, Lb0/g0/g/m;->h:Lb0/t;

    sget-object p2, Lx/h/l;->d:Lx/h/l;

    iput-object p2, p0, Lb0/g0/g/m;->a:Ljava/util/List;

    iput-object p2, p0, Lb0/g0/g/m;->c:Ljava/util/List;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lb0/g0/g/m;->d:Ljava/util/List;

    iget-object p2, p1, Lb0/a;->a:Lb0/x;

    iget-object p1, p1, Lb0/a;->j:Ljava/net/Proxy;

    new-instance p4, Lb0/g0/g/n;

    invoke-direct {p4, p0, p1, p2}, Lb0/g0/g/n;-><init>(Lb0/g0/g/m;Ljava/net/Proxy;Lb0/x;)V

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "url"

    invoke-static {p2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Lb0/g0/g/n;->invoke()Ljava/util/List;

    move-result-object p4

    iput-object p4, p0, Lb0/g0/g/m;->a:Ljava/util/List;

    const/4 v1, 0x0

    iput v1, p0, Lb0/g0/g/m;->b:I

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "proxies"

    invoke-static {p4, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    invoke-virtual {p0}, Lb0/g0/g/m;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lb0/g0/g/m;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public final b()Z
    .locals 2

    iget v0, p0, Lb0/g0/g/m;->b:I

    iget-object v1, p0, Lb0/g0/g/m;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
