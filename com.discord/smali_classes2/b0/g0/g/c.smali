.class public final Lb0/g0/g/c;
.super Ljava/lang/Object;
.source "Exchange.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/g/c$a;,
        Lb0/g0/g/c$b;
    }
.end annotation


# instance fields
.field public a:Z

.field public final b:Lb0/g0/g/j;

.field public final c:Lb0/g0/g/e;

.field public final d:Lb0/t;

.field public final e:Lb0/g0/g/d;

.field public final f:Lb0/g0/h/d;


# direct methods
.method public constructor <init>(Lb0/g0/g/e;Lb0/t;Lb0/g0/g/d;Lb0/g0/h/d;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "finder"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codec"

    invoke-static {p4, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    iput-object p2, p0, Lb0/g0/g/c;->d:Lb0/t;

    iput-object p3, p0, Lb0/g0/g/c;->e:Lb0/g0/g/d;

    iput-object p4, p0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {p4}, Lb0/g0/h/d;->e()Lb0/g0/g/j;

    move-result-object p1

    iput-object p1, p0, Lb0/g0/g/c;->b:Lb0/g0/g/j;

    return-void
.end method


# virtual methods
.method public final a(JZZLjava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(JZZTE;)TE;"
        }
    .end annotation

    if-eqz p5, :cond_0

    invoke-virtual {p0, p5}, Lb0/g0/g/c;->g(Ljava/io/IOException;)V

    :cond_0
    const-string p1, "ioe"

    const-string p2, "call"

    if-eqz p4, :cond_2

    if-eqz p5, :cond_1

    iget-object v0, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p5, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    :goto_0
    if-eqz p3, :cond_4

    if-eqz p5, :cond_3

    iget-object v0, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p5, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v0, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_4
    :goto_1
    iget-object p1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-virtual {p1, p0, p4, p3, p5}, Lb0/g0/g/e;->j(Lb0/g0/g/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1
.end method

.method public final b(Lb0/a0;Z)Lc0/v;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean p2, p0, Lb0/g0/g/c;->a:Z

    iget-object p2, p1, Lb0/a0;->e:Lokhttp3/RequestBody;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lokhttp3/RequestBody;->contentLength()J

    move-result-wide v0

    iget-object p2, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v2, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "call"

    invoke-static {v2, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {p2, p1, v0, v1}, Lb0/g0/h/d;->h(Lb0/a0;J)Lc0/v;

    move-result-object p1

    new-instance p2, Lb0/g0/g/c$a;

    invoke-direct {p2, p0, p1, v0, v1}, Lb0/g0/g/c$a;-><init>(Lb0/g0/g/c;Lc0/v;J)V

    return-object p2

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1
.end method

.method public final c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v0}, Lb0/g0/h/d;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v2, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "call"

    invoke-static {v2, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "ioe"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lb0/g0/g/c;->g(Ljava/io/IOException;)V

    throw v0
.end method

.method public final d()Lb0/g0/n/d$c;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-virtual {v0}, Lb0/g0/g/e;->m()V

    iget-object v0, p0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v0}, Lb0/g0/h/d;->e()Lb0/g0/g/j;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "exchange"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lb0/g0/g/j;->c:Ljava/net/Socket;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iget-object v8, v0, Lb0/g0/g/j;->g:Lc0/g;

    if-eqz v8, :cond_1

    iget-object v9, v0, Lb0/g0/g/j;->h:Lokio/BufferedSink;

    if-eqz v9, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-virtual {v0}, Lb0/g0/g/j;->l()V

    new-instance v0, Lb0/g0/g/i;

    const/4 v7, 0x1

    move-object v3, v0

    move-object v4, p0

    move-object v5, v8

    move-object v6, v9

    invoke-direct/range {v3 .. v9}, Lb0/g0/g/i;-><init>(Lb0/g0/g/c;Lc0/g;Lokio/BufferedSink;ZLc0/g;Lokio/BufferedSink;)V

    return-object v0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2
.end method

.method public final e(Z)Lokhttp3/Response$a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v0, p1}, Lb0/g0/h/d;->d(Z)Lokhttp3/Response$a;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "deferredTrailers"

    invoke-static {p0, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p0, p1, Lokhttp3/Response$a;->m:Lb0/g0/g/c;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "call"

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioe"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lb0/g0/g/c;->g(Ljava/io/IOException;)V

    throw p1
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lb0/g0/g/c;->d:Lb0/t;

    iget-object v1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "call"

    invoke-static {v1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public final g(Ljava/io/IOException;)V
    .locals 5

    iget-object v0, p0, Lb0/g0/g/c;->e:Lb0/g0/g/d;

    invoke-virtual {v0, p1}, Lb0/g0/g/d;->c(Ljava/io/IOException;)V

    iget-object v0, p0, Lb0/g0/g/c;->f:Lb0/g0/h/d;

    invoke-interface {v0}, Lb0/g0/h/d;->e()Lb0/g0/g/j;

    move-result-object v0

    iget-object v1, p0, Lb0/g0/g/c;->c:Lb0/g0/g/e;

    monitor-enter v0

    :try_start_0
    const-string v2, "call"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v2, p1, Lokhttp3/internal/http2/StreamResetException;

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    move-object v2, p1

    check-cast v2, Lokhttp3/internal/http2/StreamResetException;

    iget-object v2, v2, Lokhttp3/internal/http2/StreamResetException;->errorCode:Lb0/g0/j/a;

    sget-object v4, Lb0/g0/j/a;->h:Lb0/g0/j/a;

    if-ne v2, v4, :cond_0

    iget p1, v0, Lb0/g0/g/j;->m:I

    add-int/2addr p1, v3

    iput p1, v0, Lb0/g0/g/j;->m:I

    if-le p1, v3, :cond_4

    iput-boolean v3, v0, Lb0/g0/g/j;->i:Z

    iget p1, v0, Lb0/g0/g/j;->k:I

    add-int/2addr p1, v3

    iput p1, v0, Lb0/g0/g/j;->k:I

    goto :goto_0

    :cond_0
    check-cast p1, Lokhttp3/internal/http2/StreamResetException;

    iget-object p1, p1, Lokhttp3/internal/http2/StreamResetException;->errorCode:Lb0/g0/j/a;

    sget-object v2, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    if-ne p1, v2, :cond_1

    iget-boolean p1, v1, Lb0/g0/g/e;->p:Z

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    iput-boolean v3, v0, Lb0/g0/g/j;->i:Z

    iget p1, v0, Lb0/g0/g/j;->k:I

    add-int/2addr p1, v3

    iput p1, v0, Lb0/g0/g/j;->k:I

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lb0/g0/g/j;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    instance-of v2, p1, Lokhttp3/internal/http2/ConnectionShutdownException;

    if-eqz v2, :cond_4

    :cond_3
    iput-boolean v3, v0, Lb0/g0/g/j;->i:Z

    iget v2, v0, Lb0/g0/g/j;->l:I

    if-nez v2, :cond_4

    iget-object v1, v1, Lb0/g0/g/e;->s:Lb0/y;

    iget-object v2, v0, Lb0/g0/g/j;->q:Lb0/e0;

    invoke-virtual {v0, v1, v2, p1}, Lb0/g0/g/j;->d(Lb0/y;Lb0/e0;Ljava/io/IOException;)V

    iget p1, v0, Lb0/g0/g/j;->k:I

    add-int/2addr p1, v3

    iput p1, v0, Lb0/g0/g/j;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method
