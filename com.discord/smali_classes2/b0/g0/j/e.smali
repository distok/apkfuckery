.class public final Lb0/g0/j/e;
.super Ljava/lang/Object;
.source "Http2Connection.kt"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/j/e$b;,
        Lb0/g0/j/e$d;,
        Lb0/g0/j/e$c;
    }
.end annotation


# static fields
.field public static final F:Lb0/g0/j/s;

.field public static final G:Lb0/g0/j/e;


# instance fields
.field public A:J

.field public final B:Ljava/net/Socket;

.field public final C:Lb0/g0/j/o;

.field public final D:Lb0/g0/j/e$d;

.field public final E:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Lb0/g0/j/e$c;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lb0/g0/j/n;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/String;

.field public h:I

.field public i:I

.field public j:Z

.field public final k:Lb0/g0/f/d;

.field public final l:Lb0/g0/f/c;

.field public final m:Lb0/g0/f/c;

.field public final n:Lb0/g0/f/c;

.field public final o:Lb0/g0/j/r;

.field public p:J

.field public q:J

.field public r:J

.field public s:J

.field public t:J

.field public u:J

.field public final v:Lb0/g0/j/s;

.field public w:Lb0/g0/j/s;

.field public x:J

.field public y:J

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lb0/g0/j/s;

    invoke-direct {v0}, Lb0/g0/j/s;-><init>()V

    const/4 v1, 0x7

    const v2, 0xffff

    invoke-virtual {v0, v1, v2}, Lb0/g0/j/s;->c(II)Lb0/g0/j/s;

    const/4 v1, 0x5

    const/16 v2, 0x4000

    invoke-virtual {v0, v1, v2}, Lb0/g0/j/s;->c(II)Lb0/g0/j/s;

    sput-object v0, Lb0/g0/j/e;->F:Lb0/g0/j/s;

    return-void
.end method

.method public constructor <init>(Lb0/g0/j/e$b;)V
    .locals 11

    const-string v0, "builder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-boolean v0, p1, Lb0/g0/j/e$b;->h:Z

    iput-boolean v0, p0, Lb0/g0/j/e;->d:Z

    iget-object v1, p1, Lb0/g0/j/e$b;->e:Lb0/g0/j/e$c;

    iput-object v1, p0, Lb0/g0/j/e;->e:Lb0/g0/j/e$c;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    iget-object v1, p1, Lb0/g0/j/e$b;->b:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    iput-object v1, p0, Lb0/g0/j/e;->g:Ljava/lang/String;

    iget-boolean v3, p1, Lb0/g0/j/e$b;->h:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x3

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    :goto_0
    iput v3, p0, Lb0/g0/j/e;->i:I

    iget-object v3, p1, Lb0/g0/j/e$b;->i:Lb0/g0/f/d;

    iput-object v3, p0, Lb0/g0/j/e;->k:Lb0/g0/f/d;

    invoke-virtual {v3}, Lb0/g0/f/d;->f()Lb0/g0/f/c;

    move-result-object v4

    iput-object v4, p0, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    invoke-virtual {v3}, Lb0/g0/f/d;->f()Lb0/g0/f/c;

    move-result-object v5

    iput-object v5, p0, Lb0/g0/j/e;->m:Lb0/g0/f/c;

    invoke-virtual {v3}, Lb0/g0/f/d;->f()Lb0/g0/f/c;

    move-result-object v3

    iput-object v3, p0, Lb0/g0/j/e;->n:Lb0/g0/f/c;

    iget-object v3, p1, Lb0/g0/j/e$b;->f:Lb0/g0/j/r;

    iput-object v3, p0, Lb0/g0/j/e;->o:Lb0/g0/j/r;

    new-instance v3, Lb0/g0/j/s;

    invoke-direct {v3}, Lb0/g0/j/s;-><init>()V

    iget-boolean v5, p1, Lb0/g0/j/e$b;->h:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x7

    const/high16 v6, 0x1000000

    invoke-virtual {v3, v5, v6}, Lb0/g0/j/s;->c(II)Lb0/g0/j/s;

    :cond_1
    iput-object v3, p0, Lb0/g0/j/e;->v:Lb0/g0/j/s;

    sget-object v3, Lb0/g0/j/e;->F:Lb0/g0/j/s;

    iput-object v3, p0, Lb0/g0/j/e;->w:Lb0/g0/j/s;

    invoke-virtual {v3}, Lb0/g0/j/s;->a()I

    move-result v3

    int-to-long v5, v3

    iput-wide v5, p0, Lb0/g0/j/e;->A:J

    iget-object v3, p1, Lb0/g0/j/e$b;->a:Ljava/net/Socket;

    if-eqz v3, :cond_5

    iput-object v3, p0, Lb0/g0/j/e;->B:Ljava/net/Socket;

    new-instance v3, Lb0/g0/j/o;

    iget-object v5, p1, Lb0/g0/j/e$b;->d:Lokio/BufferedSink;

    if-eqz v5, :cond_4

    invoke-direct {v3, v5, v0}, Lb0/g0/j/o;-><init>(Lokio/BufferedSink;Z)V

    iput-object v3, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    new-instance v3, Lb0/g0/j/e$d;

    new-instance v5, Lb0/g0/j/m;

    iget-object v6, p1, Lb0/g0/j/e$b;->c:Lc0/g;

    if-eqz v6, :cond_3

    invoke-direct {v5, v6, v0}, Lb0/g0/j/m;-><init>(Lc0/g;Z)V

    invoke-direct {v3, p0, v5}, Lb0/g0/j/e$d;-><init>(Lb0/g0/j/e;Lb0/g0/j/m;)V

    iput-object v3, p0, Lb0/g0/j/e;->D:Lb0/g0/j/e$d;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lb0/g0/j/e;->E:Ljava/util/Set;

    iget p1, p1, Lb0/g0/j/e$b;->g:I

    if-eqz p1, :cond_2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    const-string p1, " ping"

    invoke-static {v1, p1}, Lf/e/c/a/a;->r(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance p1, Lb0/g0/j/e$a;

    move-object v5, p1

    move-object v6, v7

    move-object v8, p0

    move-wide v9, v2

    invoke-direct/range {v5 .. v10}, Lb0/g0/j/e$a;-><init>(Ljava/lang/String;Ljava/lang/String;Lb0/g0/j/e;J)V

    invoke-virtual {v4, p1, v2, v3}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    :cond_2
    return-void

    :cond_3
    const-string p1, "source"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_4
    const-string p1, "sink"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_5
    const-string p1, "socket"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2

    :cond_6
    const-string p1, "connectionName"

    invoke-static {p1}, Lx/m/c/j;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public final a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V
    .locals 3

    const-string v0, "connectionCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "streamCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/c;->a:[B

    :try_start_0
    invoke-virtual {p0, p1}, Lb0/g0/j/e;->e(Lb0/g0/j/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 p1, 0x0

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    new-array v0, v1, [Lb0/g0/j/n;

    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, [Lb0/g0/j/n;

    iget-object v0, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    if-eqz p1, :cond_2

    array-length v0, p1

    :goto_1
    if-ge v1, v0, :cond_2

    aget-object v2, p1, v1

    :try_start_2
    invoke-virtual {v2, p2, p3}, Lb0/g0/j/n;->c(Lb0/g0/j/a;Ljava/io/IOException;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    :try_start_3
    iget-object p1, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {p1}, Lb0/g0/j/o;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    :try_start_4
    iget-object p1, p0, Lb0/g0/j/e;->B:Ljava/net/Socket;

    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    iget-object p1, p0, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    invoke-virtual {p1}, Lb0/g0/f/c;->f()V

    iget-object p1, p0, Lb0/g0/j/e;->m:Lb0/g0/f/c;

    invoke-virtual {p1}, Lb0/g0/f/c;->f()V

    iget-object p1, p0, Lb0/g0/j/e;->n:Lb0/g0/f/c;

    invoke-virtual {p1}, Lb0/g0/f/c;->f()V

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized b(I)Lb0/g0/j/n;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lb0/g0/j/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final c(I)Z
    .locals 1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    and-int/2addr p1, v0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public close()V
    .locals 3

    sget-object v0, Lb0/g0/j/a;->d:Lb0/g0/j/a;

    sget-object v1, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    return-void
.end method

.method public final declared-synchronized d(I)Lb0/g0/j/n;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lb0/g0/j/n;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final e(Lb0/g0/j/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "statusCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    monitor-enter v0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v1, p0, Lb0/g0/j/e;->j:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_3
    iput-boolean v1, p0, Lb0/g0/j/e;->j:Z

    iget v1, p0, Lb0/g0/j/e;->h:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit p0

    iget-object v2, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    sget-object v3, Lb0/g0/c;->a:[B

    invoke-virtual {v2, v1, p1, v3}, Lb0/g0/j/o;->d(ILb0/g0/j/a;[B)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    :try_start_5
    monitor-exit p0

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final declared-synchronized f(J)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lb0/g0/j/e;->x:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lb0/g0/j/e;->x:J

    iget-wide p1, p0, Lb0/g0/j/e;->y:J

    sub-long/2addr v0, p1

    iget-object p1, p0, Lb0/g0/j/e;->v:Lb0/g0/j/s;

    invoke-virtual {p1}, Lb0/g0/j/s;->a()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    int-to-long p1, p1

    cmp-long v2, v0, p1

    if-ltz v2, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lb0/g0/j/e;->n(IJ)V

    iget-wide p1, p0, Lb0/g0/j/e;->y:J

    add-long/2addr p1, v0

    iput-wide p1, p0, Lb0/g0/j/e;->y:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final g(IZLc0/e;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmp-long v3, p4, v1

    if-nez v3, :cond_0

    iget-object p4, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {p4, p2, p1, p3, v0}, Lb0/g0/j/o;->b(ZILc0/e;I)V

    return-void

    :cond_0
    :goto_0
    cmp-long v3, p4, v1

    if-lez v3, :cond_4

    new-instance v3, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v3}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    monitor-enter p0

    :goto_1
    :try_start_0
    iget-wide v4, p0, Lb0/g0/j/e;->z:J

    iget-wide v6, p0, Lb0/g0/j/e;->A:J

    cmp-long v8, v4, v6

    if-ltz v8, :cond_2

    iget-object v4, p0, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "stream closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    sub-long/2addr v6, v4

    :try_start_1
    invoke-static {p4, p5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v5, v4

    iput v5, v3, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iget-object v4, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    iget v4, v4, Lb0/g0/j/o;->e:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, v3, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iget-wide v5, p0, Lb0/g0/j/e;->z:J

    int-to-long v7, v4

    add-long/2addr v5, v7

    iput-wide v5, p0, Lb0/g0/j/e;->z:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    int-to-long v5, v4

    sub-long/2addr p4, v5

    iget-object v3, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    if-eqz p2, :cond_3

    cmp-long v5, p4, v1

    if-nez v5, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v3, v5, p1, p3, v4}, Lb0/g0/j/o;->b(ZILc0/e;I)V

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    new-instance p1, Ljava/io/InterruptedIOException;

    invoke-direct {p1}, Ljava/io/InterruptedIOException;-><init>()V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_3
    monitor-exit p0

    throw p1

    :cond_4
    return-void
.end method

.method public final i(ZII)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {v0, p1, p2, p3}, Lb0/g0/j/o;->f(ZII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object p2, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    invoke-virtual {p0, p2, p2, p1}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    :goto_0
    return-void
.end method

.method public final m(ILb0/g0/j/a;)V
    .locals 11

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "] writeSynReset"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lb0/g0/j/e$e;

    const/4 v7, 0x1

    move-object v3, v1

    move-object v4, v6

    move v5, v7

    move-object v8, p0

    move v9, p1

    move-object v10, p2

    invoke-direct/range {v3 .. v10}, Lb0/g0/j/e$e;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILb0/g0/j/a;)V

    const-wide/16 p1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void
.end method

.method public final n(IJ)V
    .locals 12

    iget-object v0, p0, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "] windowUpdate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lb0/g0/j/e$f;

    const/4 v7, 0x1

    move-object v3, v1

    move-object v4, v6

    move v5, v7

    move-object v8, p0

    move v9, p1

    move-wide v10, p2

    invoke-direct/range {v3 .. v11}, Lb0/g0/j/e$f;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;IJ)V

    const-wide/16 p1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void
.end method
