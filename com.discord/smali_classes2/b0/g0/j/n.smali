.class public final Lb0/g0/j/n;
.super Ljava/lang/Object;
.source "Http2Stream.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/j/n$b;,
        Lb0/g0/j/n$a;,
        Lb0/g0/j/n$c;
    }
.end annotation


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public final e:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lokhttp3/Headers;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public final g:Lb0/g0/j/n$b;

.field public final h:Lb0/g0/j/n$a;

.field public final i:Lb0/g0/j/n$c;

.field public final j:Lb0/g0/j/n$c;

.field public k:Lb0/g0/j/a;

.field public l:Ljava/io/IOException;

.field public final m:I

.field public final n:Lb0/g0/j/e;


# direct methods
.method public constructor <init>(ILb0/g0/j/e;ZZLokhttp3/Headers;)V
    .locals 3

    const-string v0, "connection"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lb0/g0/j/n;->m:I

    iput-object p2, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget-object p1, p2, Lb0/g0/j/e;->w:Lb0/g0/j/s;

    invoke-virtual {p1}, Lb0/g0/j/s;->a()I

    move-result p1

    int-to-long v0, p1

    iput-wide v0, p0, Lb0/g0/j/n;->d:J

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lb0/g0/j/n;->e:Ljava/util/ArrayDeque;

    new-instance v0, Lb0/g0/j/n$b;

    iget-object p2, p2, Lb0/g0/j/e;->v:Lb0/g0/j/s;

    invoke-virtual {p2}, Lb0/g0/j/s;->a()I

    move-result p2

    int-to-long v1, p2

    invoke-direct {v0, p0, v1, v2, p4}, Lb0/g0/j/n$b;-><init>(Lb0/g0/j/n;JZ)V

    iput-object v0, p0, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    new-instance p2, Lb0/g0/j/n$a;

    invoke-direct {p2, p0, p3}, Lb0/g0/j/n$a;-><init>(Lb0/g0/j/n;Z)V

    iput-object p2, p0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    new-instance p2, Lb0/g0/j/n$c;

    invoke-direct {p2, p0}, Lb0/g0/j/n$c;-><init>(Lb0/g0/j/n;)V

    iput-object p2, p0, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    new-instance p2, Lb0/g0/j/n$c;

    invoke-direct {p2, p0}, Lb0/g0/j/n$c;-><init>(Lb0/g0/j/n;)V

    iput-object p2, p0, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lb0/g0/j/n;->h()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p1, p5}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "locally-initiated streams shouldn\'t have headers yet"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {p0}, Lb0/g0/j/n;->h()Z

    move-result p1

    if-eqz p1, :cond_2

    :goto_0
    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "remotely-initiated streams should have headers"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lb0/g0/c;->a:[B

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    iget-boolean v1, v0, Lb0/g0/j/n$b;->h:Z

    if-nez v1, :cond_1

    iget-boolean v0, v0, Lb0/g0/j/n$b;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    iget-boolean v1, v0, Lb0/g0/j/n$a;->f:Z

    if-nez v1, :cond_0

    iget-boolean v0, v0, Lb0/g0/j/n$a;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lb0/g0/j/n;->i()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    if-eqz v0, :cond_2

    sget-object v0, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lb0/g0/j/n;->c(Lb0/g0/j/a;Ljava/io/IOException;)V

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    iget-object v0, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget v1, p0, Lb0/g0/j/n;->m:I

    invoke-virtual {v0, v1}, Lb0/g0/j/e;->d(I)Lb0/g0/j/n;

    :cond_3
    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    iget-boolean v1, v0, Lb0/g0/j/n$a;->e:Z

    if-nez v1, :cond_4

    iget-boolean v0, v0, Lb0/g0/j/n$a;->f:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb0/g0/j/n;->l:Ljava/io/IOException;

    if-nez v0, :cond_1

    new-instance v0, Lokhttp3/internal/http2/StreamResetException;

    iget-object v1, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    if-nez v1, :cond_0

    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0

    :cond_0
    invoke-direct {v0, v1}, Lokhttp3/internal/http2/StreamResetException;-><init>(Lb0/g0/j/a;)V

    :cond_1
    throw v0

    :cond_2
    return-void

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Lb0/g0/j/a;Ljava/io/IOException;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "rstStatusCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lb0/g0/j/n;->d(Lb0/g0/j/a;Ljava/io/IOException;)Z

    move-result p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object p2, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget v0, p0, Lb0/g0/j/n;->m:I

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "statusCode"

    invoke-static {p1, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p2, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {p2, v0, p1}, Lb0/g0/j/o;->g(ILb0/g0/j/a;)V

    return-void
.end method

.method public final d(Lb0/g0/j/a;Ljava/io/IOException;)Z
    .locals 2

    sget-object v0, Lb0/g0/c;->a:[B

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    monitor-exit p0

    return v1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    iget-boolean v0, v0, Lb0/g0/j/n$b;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    iget-boolean v0, v0, Lb0/g0/j/n$a;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    monitor-exit p0

    return v1

    :cond_1
    :try_start_2
    iput-object p1, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    iput-object p2, p0, Lb0/g0/j/n;->l:Ljava/io/IOException;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    iget-object p1, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget p2, p0, Lb0/g0/j/n;->m:I

    invoke-virtual {p1, p2}, Lb0/g0/j/e;->d(I)Lb0/g0/j/n;

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final e(Lb0/g0/j/a;)V
    .locals 2

    const-string v0, "errorCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lb0/g0/j/n;->d(Lb0/g0/j/a;Ljava/io/IOException;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget v1, p0, Lb0/g0/j/n;->m:I

    invoke-virtual {v0, v1, p1}, Lb0/g0/j/e;->m(ILb0/g0/j/a;)V

    return-void
.end method

.method public final declared-synchronized f()Lb0/g0/j/a;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Lc0/v;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/j/n;->f:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lb0/g0/j/n;->h()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    monitor-exit p0

    iget-object v0, p0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    return-object v0

    :cond_2
    :try_start_1
    const-string v0, "reply before requesting the sink"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()Z
    .locals 4

    iget v0, p0, Lb0/g0/j/n;->m:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget-boolean v3, v3, Lb0/g0/j/e;->d:Z

    if-ne v3, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final declared-synchronized i()Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    monitor-exit p0

    return v1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    iget-boolean v2, v0, Lb0/g0/j/n$b;->h:Z

    if-nez v2, :cond_1

    iget-boolean v0, v0, Lb0/g0/j/n$b;->f:Z

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    iget-boolean v2, v0, Lb0/g0/j/n$a;->f:Z

    if-nez v2, :cond_2

    iget-boolean v0, v0, Lb0/g0/j/n$a;->e:Z

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lb0/g0/j/n;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_3

    monitor-exit p0

    return v1

    :cond_3
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j(Lokhttp3/Headers;Z)V
    .locals 2

    const-string v0, "headers"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/c;->a:[B

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/j/n;->f:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    :goto_0
    iput-boolean v1, p0, Lb0/g0/j/n;->f:Z

    iget-object v0, p0, Lb0/g0/j/n;->e:Ljava/util/ArrayDeque;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :goto_1
    if-eqz p2, :cond_2

    iget-object p1, p0, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    iput-boolean v1, p1, Lb0/g0/j/n$b;->h:Z

    :cond_2
    invoke-virtual {p0}, Lb0/g0/j/n;->i()Z

    move-result p1

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    if-nez p1, :cond_3

    iget-object p1, p0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget p2, p0, Lb0/g0/j/n;->m:I

    invoke-virtual {p1, p2}, Lb0/g0/j/e;->d(I)Lb0/g0/j/n;

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized k(Lb0/g0/j/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "errorCode"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    if-nez v0, :cond_0

    iput-object p1, p0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final l()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
.end method
