.class public final Lb0/g0/j/e$e;
.super Lb0/g0/f/a;
.source "TaskQueue.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb0/g0/j/e;->m(ILb0/g0/j/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic e:Lb0/g0/j/e;

.field public final synthetic f:I

.field public final synthetic g:Lb0/g0/j/a;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILb0/g0/j/a;)V
    .locals 0

    iput-object p5, p0, Lb0/g0/j/e$e;->e:Lb0/g0/j/e;

    iput p6, p0, Lb0/g0/j/e$e;->f:I

    iput-object p7, p0, Lb0/g0/j/e$e;->g:Lb0/g0/j/a;

    invoke-direct {p0, p3, p4}, Lb0/g0/f/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/e$e;->e:Lb0/g0/j/e;

    iget v1, p0, Lb0/g0/j/e$e;->f:I

    iget-object v2, p0, Lb0/g0/j/e$e;->g:Lb0/g0/j/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "statusCode"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {v0, v1, v2}, Lb0/g0/j/o;->g(ILb0/g0/j/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lb0/g0/j/e$e;->e:Lb0/g0/j/e;

    sget-object v2, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    invoke-virtual {v1, v2, v2, v0}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    :goto_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method
