.class public final Lb0/g0/j/e$d;
.super Ljava/lang/Object;
.source "Http2Connection.kt"

# interfaces
.implements Lb0/g0/j/m$b;
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/j/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb0/g0/j/m$b;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:Lb0/g0/j/m;

.field public final synthetic e:Lb0/g0/j/e;


# direct methods
.method public constructor <init>(Lb0/g0/j/e;Lb0/g0/j/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb0/g0/j/m;",
            ")V"
        }
    .end annotation

    const-string v0, "reader"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lb0/g0/j/e$d;->d:Lb0/g0/j/m;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public b(ZLb0/g0/j/s;)V
    .locals 12

    const-string v0, "settings"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v2, v2, Lb0/g0/j/e;->g:Ljava/lang/String;

    const-string v3, " applyAndAckSettings"

    invoke-static {v1, v2, v3}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v1, Lb0/g0/j/e$d$c;

    const/4 v8, 0x1

    move-object v4, v1

    move-object v5, v7

    move v6, v8

    move-object v9, p0

    move v10, p1

    move-object v11, p2

    invoke-direct/range {v4 .. v11}, Lb0/g0/j/e$d$c;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e$d;ZLb0/g0/j/s;)V

    const-wide/16 p1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void
.end method

.method public c(ZIILjava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII",
            "Ljava/util/List<",
            "Lb0/g0/j/b;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v12, p0

    move/from16 v0, p2

    move-object/from16 v10, p4

    const-string v1, "headerBlock"

    invoke-static {v10, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v1, v0}, Lb0/g0/j/e;->c(I)Z

    move-result v1

    const-wide/16 v13, 0x0

    const/16 v7, 0x5b

    if-eqz v1, :cond_0

    iget-object v6, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-static {v6}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "requestHeaders"

    invoke-static {v10, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v11, v6, Lb0/g0/j/e;->m:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v6, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "] onHeaders"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v15, Lb0/g0/j/h;

    const/4 v5, 0x1

    move-object v1, v15

    move-object v2, v4

    move v3, v5

    move/from16 v7, p2

    move-object/from16 v8, p4

    move/from16 v9, p1

    invoke-direct/range {v1 .. v9}, Lb0/g0/j/h;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILjava/util/List;Z)V

    invoke-virtual {v11, v15, v13, v14}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void

    :cond_0
    iget-object v15, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    monitor-enter v15

    :try_start_0
    iget-object v1, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v1, v0}, Lb0/g0/j/e;->b(I)Lb0/g0/j/n;

    move-result-object v8

    if-nez v8, :cond_4

    iget-object v1, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-boolean v2, v1, Lb0/g0/j/e;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    monitor-exit v15

    return-void

    :cond_1
    :try_start_1
    iget v2, v1, Lb0/g0/j/e;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-gt v0, v2, :cond_2

    monitor-exit v15

    return-void

    :cond_2
    :try_start_2
    rem-int/lit8 v2, v0, 0x2

    iget v1, v1, Lb0/g0/j/e;->i:I

    rem-int/lit8 v1, v1, 0x2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v2, v1, :cond_3

    monitor-exit v15

    return-void

    :cond_3
    :try_start_3
    invoke-static/range {p4 .. p4}, Lb0/g0/c;->w(Ljava/util/List;)Lokhttp3/Headers;

    move-result-object v6

    new-instance v9, Lb0/g0/j/n;

    iget-object v3, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    const/4 v4, 0x0

    move-object v1, v9

    move/from16 v2, p2

    move/from16 v5, p1

    invoke-direct/range {v1 .. v6}, Lb0/g0/j/n;-><init>(ILb0/g0/j/e;ZZLokhttp3/Headers;)V

    iget-object v1, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iput v0, v1, Lb0/g0/j/e;->h:I

    iget-object v1, v1, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v1, v1, Lb0/g0/j/e;->k:Lb0/g0/f/d;

    invoke-virtual {v1}, Lb0/g0/f/d;->f()Lb0/g0/f/c;

    move-result-object v11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v12, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v2, v2, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "] onStream"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-instance v7, Lb0/g0/j/e$d$a;

    move-object v1, v7

    move-object v2, v4

    move v3, v5

    move-object v6, v9

    move-object v9, v7

    move-object/from16 v7, p0

    move-object v13, v9

    move/from16 v9, p2

    move-object/from16 v10, p4

    move-object v0, v11

    move/from16 v11, p1

    invoke-direct/range {v1 .. v11}, Lb0/g0/j/e$d$a;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/n;Lb0/g0/j/e$d;Lb0/g0/j/n;ILjava/util/List;Z)V

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v13, v1, v2}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v15

    return-void

    :cond_4
    monitor-exit v15

    invoke-static/range {p4 .. p4}, Lb0/g0/c;->w(Ljava/util/List;)Lokhttp3/Headers;

    move-result-object v0

    move/from16 v1, p1

    invoke-virtual {v8, v0, v1}, Lb0/g0/j/n;->j(Lokhttp3/Headers;Z)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v15

    throw v0
.end method

.method public d(IJ)V
    .locals 3

    if-nez p1, :cond_0

    iget-object p1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    monitor-enter p1

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-wide v1, v0, Lb0/g0/j/e;->A:J

    add-long/2addr v1, p2

    iput-wide v1, v0, Lb0/g0/j/e;->A:J

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    goto :goto_0

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_0
    iget-object v0, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v0, p1}, Lb0/g0/j/e;->b(I)Lb0/g0/j/n;

    move-result-object p1

    if-eqz p1, :cond_2

    monitor-enter p1

    :try_start_1
    iget-wide v0, p1, Lb0/g0/j/n;->d:J

    add-long/2addr v0, p2

    iput-wide v0, p1, Lb0/g0/j/n;->d:J

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    monitor-exit p1

    goto :goto_0

    :catchall_1
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_2
    :goto_0
    return-void
.end method

.method public e(ZILc0/g;I)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    move/from16 v0, p2

    move-object/from16 v2, p3

    move/from16 v10, p4

    const-string v3, "source"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v1, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v3, v0}, Lb0/g0/j/e;->c(I)Z

    move-result v3

    const-wide/16 v12, 0x0

    if-eqz v3, :cond_0

    iget-object v7, v1, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "source"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v9, Lc0/e;

    invoke-direct {v9}, Lc0/e;-><init>()V

    int-to-long v3, v10

    invoke-interface {v2, v3, v4}, Lc0/g;->E0(J)V

    invoke-interface {v2, v9, v3, v4}, Lc0/x;->v0(Lc0/e;J)J

    iget-object v14, v7, Lb0/g0/j/e;->m:Lb0/g0/f/c;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v7, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "] onData"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v15, Lb0/g0/j/g;

    const/4 v6, 0x1

    move-object v2, v15

    move-object v3, v5

    move v4, v6

    move/from16 v8, p2

    move/from16 v10, p4

    move/from16 v11, p1

    invoke-direct/range {v2 .. v11}, Lb0/g0/j/g;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILc0/e;IZ)V

    invoke-virtual {v14, v15, v12, v13}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void

    :cond_0
    iget-object v3, v1, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v3, v0}, Lb0/g0/j/e;->b(I)Lb0/g0/j/n;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v3, v1, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    sget-object v4, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    invoke-virtual {v3, v0, v4}, Lb0/g0/j/e;->m(ILb0/g0/j/a;)V

    iget-object v0, v1, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    int-to-long v3, v10

    invoke-virtual {v0, v3, v4}, Lb0/g0/j/e;->f(J)V

    invoke-interface {v2, v3, v4}, Lc0/g;->skip(J)V

    return-void

    :cond_1
    const-string v0, "source"

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/c;->a:[B

    iget-object v0, v3, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    int-to-long v4, v10

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "source"

    invoke-static {v2, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    :goto_0
    const/4 v6, 0x1

    cmp-long v7, v4, v12

    if-lez v7, :cond_b

    iget-object v7, v0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    monitor-enter v7

    :try_start_0
    iget-boolean v8, v0, Lb0/g0/j/n$b;->h:Z

    iget-object v9, v0, Lb0/g0/j/n$b;->e:Lc0/e;

    iget-wide v9, v9, Lc0/e;->e:J

    add-long/2addr v9, v4

    iget-wide v14, v0, Lb0/g0/j/n$b;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v11, 0x0

    cmp-long v16, v9, v14

    if-lez v16, :cond_3

    const/4 v9, 0x1

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    :goto_1
    monitor-exit v7

    if-eqz v9, :cond_4

    invoke-interface {v2, v4, v5}, Lc0/g;->skip(J)V

    iget-object v0, v0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    sget-object v2, Lb0/g0/j/a;->g:Lb0/g0/j/a;

    invoke-virtual {v0, v2}, Lb0/g0/j/n;->e(Lb0/g0/j/a;)V

    goto :goto_5

    :cond_4
    if-eqz v8, :cond_5

    invoke-interface {v2, v4, v5}, Lc0/g;->skip(J)V

    goto :goto_5

    :cond_5
    iget-object v7, v0, Lb0/g0/j/n$b;->d:Lc0/e;

    invoke-interface {v2, v7, v4, v5}, Lc0/x;->v0(Lc0/e;J)J

    move-result-wide v7

    const-wide/16 v9, -0x1

    cmp-long v14, v7, v9

    if-eqz v14, :cond_a

    sub-long/2addr v4, v7

    iget-object v7, v0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    monitor-enter v7

    :try_start_1
    iget-boolean v8, v0, Lb0/g0/j/n$b;->f:Z

    if-eqz v8, :cond_6

    iget-object v6, v0, Lb0/g0/j/n$b;->d:Lc0/e;

    iget-wide v8, v6, Lc0/e;->e:J

    invoke-virtual {v6, v8, v9}, Lc0/e;->skip(J)V

    goto :goto_4

    :cond_6
    iget-object v8, v0, Lb0/g0/j/n$b;->e:Lc0/e;

    iget-wide v9, v8, Lc0/e;->e:J

    cmp-long v14, v9, v12

    if-nez v14, :cond_7

    goto :goto_2

    :cond_7
    const/4 v6, 0x0

    :goto_2
    iget-object v9, v0, Lb0/g0/j/n$b;->d:Lc0/e;

    invoke-virtual {v8, v9}, Lc0/e;->a0(Lc0/x;)J

    if-eqz v6, :cond_9

    iget-object v6, v0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    goto :goto_3

    :cond_8
    new-instance v0, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type java.lang.Object"

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_9
    :goto_3
    move-wide v8, v12

    :goto_4
    monitor-exit v7

    cmp-long v6, v8, v12

    if-lez v6, :cond_2

    invoke-virtual {v0, v8, v9}, Lb0/g0/j/n$b;->a(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_a
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_b
    :goto_5
    if-eqz p1, :cond_c

    sget-object v0, Lb0/g0/c;->b:Lokhttp3/Headers;

    invoke-virtual {v3, v0, v6}, Lb0/g0/j/n;->j(Lokhttp3/Headers;Z)V

    :cond_c
    return-void
.end method

.method public f(ZII)V
    .locals 11

    if-eqz p1, :cond_3

    iget-object p1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    monitor-enter p1

    const/4 p3, 0x1

    const-wide/16 v0, 0x1

    if-eq p2, p3, :cond_2

    const/4 p3, 0x2

    if-eq p2, p3, :cond_1

    const/4 p3, 0x3

    if-eq p2, p3, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object p2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-wide v2, p2, Lb0/g0/j/e;->t:J

    add-long/2addr v2, v0

    iput-wide v2, p2, Lb0/g0/j/e;->t:J

    invoke-virtual {p2}, Ljava/lang/Object;->notifyAll()V

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-wide v2, p2, Lb0/g0/j/e;->s:J

    add-long/2addr v2, v0

    iput-wide v2, p2, Lb0/g0/j/e;->s:J

    goto :goto_0

    :cond_2
    iget-object p2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-wide v2, p2, Lb0/g0/j/e;->q:J

    add-long/2addr v2, v0

    iput-wide v2, p2, Lb0/g0/j/e;->q:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p1

    goto :goto_1

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2

    :cond_3
    iget-object p1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object p1, p1, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v1, v1, Lb0/g0/j/e;->g:Ljava/lang/String;

    const-string v2, " ping"

    invoke-static {v0, v1, v2}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-wide/16 v0, 0x0

    const/4 v7, 0x1

    new-instance v2, Lb0/g0/j/e$d$b;

    move-object v3, v2

    move-object v4, v6

    move v5, v7

    move-object v8, p0

    move v9, p2

    move v10, p3

    invoke-direct/range {v3 .. v10}, Lb0/g0/j/e$d$b;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e$d;II)V

    invoke-virtual {p1, v2, v0, v1}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    :goto_1
    return-void
.end method

.method public g(IIIZ)V
    .locals 0

    return-void
.end method

.method public h(ILb0/g0/j/a;)V
    .locals 10

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v1, p1}, Lb0/g0/j/e;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v7, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-static {v7}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v7, Lb0/g0/j/e;->m:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v7, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "] onReset"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v1, Lb0/g0/j/j;

    const/4 v6, 0x1

    move-object v2, v1

    move-object v3, v5

    move v4, v6

    move v8, p1

    move-object v9, p2

    invoke-direct/range {v2 .. v9}, Lb0/g0/j/j;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILb0/g0/j/a;)V

    const-wide/16 p1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    return-void

    :cond_0
    iget-object v0, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v0, p1}, Lb0/g0/j/e;->d(I)Lb0/g0/j/n;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Lb0/g0/j/n;->k(Lb0/g0/j/a;)V

    :cond_1
    return-void
.end method

.method public i(IILjava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List<",
            "Lb0/g0/j/b;",
            ">;)V"
        }
    .end annotation

    const-string p1, "requestHeaders"

    invoke-static {p3, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "requestHeaders"

    invoke-static {p3, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter v5

    :try_start_0
    iget-object p1, v5, Lb0/g0/j/e;->E:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    invoke-virtual {v5, p2, p1}, Lb0/g0/j/e;->m(ILb0/g0/j/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v5

    goto :goto_0

    :cond_0
    :try_start_1
    iget-object p1, v5, Lb0/g0/j/e;->E:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v5

    iget-object p1, v5, Lb0/g0/j/e;->m:Lb0/g0/f/c;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v5, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "] onRequest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v8, 0x0

    const/4 v4, 0x1

    new-instance v10, Lb0/g0/j/i;

    move-object v0, v10

    move-object v1, v3

    move v2, v4

    move v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lb0/g0/j/i;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILjava/util/List;)V

    invoke-virtual {p1, v10, v8, v9}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    monitor-exit v5

    throw p1
.end method

.method public invoke()Ljava/lang/Object;
    .locals 5

    sget-object v0, Lb0/g0/j/a;->f:Lb0/g0/j/a;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lb0/g0/j/e$d;->d:Lb0/g0/j/m;

    invoke-virtual {v2, p0}, Lb0/g0/j/m;->b(Lb0/g0/j/m$b;)V

    :goto_0
    iget-object v2, p0, Lb0/g0/j/e$d;->d:Lb0/g0/j/m;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, p0}, Lb0/g0/j/m;->a(ZLb0/g0/j/m$b;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lb0/g0/j/a;->d:Lb0/g0/j/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Lb0/g0/j/a;->i:Lb0/g0/j/a;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v3, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v3, v2, v0, v1}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v2

    move-object v3, v2

    move-object v2, v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v2, v0

    :goto_1
    :try_start_2
    sget-object v0, Lb0/g0/j/a;->e:Lb0/g0/j/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v2, v0, v0, v1}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    :goto_2
    iget-object v0, p0, Lb0/g0/j/e$d;->d:Lb0/g0/j/m;

    invoke-static {v0}, Lb0/g0/c;->d(Ljava/io/Closeable;)V

    sget-object v0, Lkotlin/Unit;->a:Lkotlin/Unit;

    return-object v0

    :catchall_1
    move-exception v3

    :goto_3
    iget-object v4, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    invoke-virtual {v4, v2, v0, v1}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    iget-object v0, p0, Lb0/g0/j/e$d;->d:Lb0/g0/j/m;

    invoke-static {v0}, Lb0/g0/c;->d(Ljava/io/Closeable;)V

    throw v3
.end method

.method public j(ILb0/g0/j/a;Lokio/ByteString;)V
    .locals 3

    const-string v0, "errorCode"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "debugData"

    invoke-static {p3, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lokio/ByteString;->j()I

    iget-object p2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    monitor-enter p2

    :try_start_0
    iget-object p3, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object p3, p3, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p3

    const/4 v0, 0x0

    new-array v1, v0, [Lb0/g0/j/n;

    invoke-interface {p3, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_2

    check-cast p3, [Lb0/g0/j/n;

    iget-object v1, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lb0/g0/j/e;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p2

    array-length p2, p3

    :goto_0
    if-ge v0, p2, :cond_1

    aget-object v1, p3, v0

    iget v2, v1, Lb0/g0/j/n;->m:I

    if-le v2, p1, :cond_0

    invoke-virtual {v1}, Lb0/g0/j/n;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lb0/g0/j/a;->h:Lb0/g0/j/a;

    invoke-virtual {v1, v2}, Lb0/g0/j/n;->k(Lb0/g0/j/a;)V

    iget-object v2, p0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget v1, v1, Lb0/g0/j/n;->m:I

    invoke-virtual {v2, v1}, Lb0/g0/j/e;->d(I)Lb0/g0/j/n;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    :try_start_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p3, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p2

    throw p1
.end method
