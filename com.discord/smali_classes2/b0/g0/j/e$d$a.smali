.class public final Lb0/g0/j/e$d$a;
.super Lb0/g0/f/a;
.source "TaskQueue.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb0/g0/j/e$d;->c(ZIILjava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic e:Lb0/g0/j/n;

.field public final synthetic f:Lb0/g0/j/e$d;

.field public final synthetic g:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/n;Lb0/g0/j/e$d;Lb0/g0/j/n;ILjava/util/List;Z)V
    .locals 0

    iput-object p5, p0, Lb0/g0/j/e$d$a;->e:Lb0/g0/j/n;

    iput-object p6, p0, Lb0/g0/j/e$d$a;->f:Lb0/g0/j/e$d;

    iput-object p9, p0, Lb0/g0/j/e$d$a;->g:Ljava/util/List;

    invoke-direct {p0, p3, p4}, Lb0/g0/f/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/e$d$a;->f:Lb0/g0/j/e$d;

    iget-object v0, v0, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->e:Lb0/g0/j/e$c;

    iget-object v1, p0, Lb0/g0/j/e$d$a;->e:Lb0/g0/j/n;

    invoke-virtual {v0, v1}, Lb0/g0/j/e$c;->b(Lb0/g0/j/n;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v1, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    const-string v2, "Http2Connection.Listener failure for "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb0/g0/j/e$d$a;->f:Lb0/g0/j/e$d;

    iget-object v3, v3, Lb0/g0/j/e$d;->e:Lb0/g0/j/e;

    iget-object v3, v3, Lb0/g0/j/e;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3, v0}, Lb0/g0/k/h;->i(Ljava/lang/String;ILjava/lang/Throwable;)V

    :try_start_1
    iget-object v1, p0, Lb0/g0/j/e$d$a;->e:Lb0/g0/j/n;

    sget-object v2, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    invoke-virtual {v1, v2, v0}, Lb0/g0/j/n;->c(Lb0/g0/j/a;Ljava/io/IOException;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :goto_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method
