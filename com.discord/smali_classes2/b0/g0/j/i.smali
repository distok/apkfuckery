.class public final Lb0/g0/j/i;
.super Lb0/g0/f/a;
.source "TaskQueue.kt"


# instance fields
.field public final synthetic e:Lb0/g0/j/e;

.field public final synthetic f:I

.field public final synthetic g:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;ILjava/util/List;)V
    .locals 0

    iput-object p5, p0, Lb0/g0/j/i;->e:Lb0/g0/j/e;

    iput p6, p0, Lb0/g0/j/i;->f:I

    iput-object p7, p0, Lb0/g0/j/i;->g:Ljava/util/List;

    invoke-direct {p0, p3, p4}, Lb0/g0/f/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 3

    iget-object v0, p0, Lb0/g0/j/i;->e:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->o:Lb0/g0/j/r;

    iget v1, p0, Lb0/g0/j/i;->f:I

    iget-object v2, p0, Lb0/g0/j/i;->g:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lb0/g0/j/r;->a(ILjava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lb0/g0/j/i;->e:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    iget v1, p0, Lb0/g0/j/i;->f:I

    sget-object v2, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    invoke-virtual {v0, v1, v2}, Lb0/g0/j/o;->g(ILb0/g0/j/a;)V

    iget-object v0, p0, Lb0/g0/j/i;->e:Lb0/g0/j/e;

    monitor-enter v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, p0, Lb0/g0/j/i;->e:Lb0/g0/j/e;

    iget-object v1, v1, Lb0/g0/j/e;->E:Ljava/util/Set;

    iget v2, p0, Lb0/g0/j/i;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_0
    :goto_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method
