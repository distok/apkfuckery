.class public final Lb0/g0/j/n$c;
.super Lc0/b;
.source "Http2Stream.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/j/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field public final synthetic l:Lb0/g0/j/n;


# direct methods
.method public constructor <init>(Lb0/g0/j/n;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/j/n$c;->l:Lb0/g0/j/n;

    invoke-direct {p0}, Lc0/b;-><init>()V

    return-void
.end method


# virtual methods
.method public k(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    new-instance v0, Ljava/net/SocketTimeoutException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/net/SocketTimeoutException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_0
    return-object v0
.end method

.method public l()V
    .locals 10

    iget-object v0, p0, Lb0/g0/j/n$c;->l:Lb0/g0/j/n;

    sget-object v1, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    invoke-virtual {v0, v1}, Lb0/g0/j/n;->e(Lb0/g0/j/a;)V

    iget-object v0, p0, Lb0/g0/j/n$c;->l:Lb0/g0/j/n;

    iget-object v6, v0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    monitor-enter v6

    :try_start_0
    iget-wide v0, v6, Lb0/g0/j/e;->s:J

    iget-wide v2, v6, Lb0/g0/j/e;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    monitor-exit v6

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x1

    add-long/2addr v2, v0

    :try_start_1
    iput-wide v2, v6, Lb0/g0/j/e;->r:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const v2, 0x3b9aca00

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, v6, Lb0/g0/j/e;->u:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v6

    iget-object v0, v6, Lb0/g0/j/e;->l:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v6, Lb0/g0/j/e;->g:Ljava/lang/String;

    const-string v3, " ping"

    invoke-static {v1, v2, v3}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v7, 0x0

    const/4 v5, 0x1

    new-instance v9, Lb0/g0/j/k;

    move-object v1, v9

    move-object v2, v4

    move v3, v5

    invoke-direct/range {v1 .. v6}, Lb0/g0/j/k;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/j/e;)V

    invoke-virtual {v0, v9, v7, v8}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final m()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lc0/b;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/net/SocketTimeoutException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
