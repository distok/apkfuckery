.class public abstract Lb0/g0/j/e$c;
.super Ljava/lang/Object;
.source "Http2Connection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/j/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation


# static fields
.field public static final a:Lb0/g0/j/e$c;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lb0/g0/j/e$c$a;

    invoke-direct {v0}, Lb0/g0/j/e$c$a;-><init>()V

    sput-object v0, Lb0/g0/j/e$c;->a:Lb0/g0/j/e$c;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lb0/g0/j/e;Lb0/g0/j/s;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "settings"

    invoke-static {p2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public abstract b(Lb0/g0/j/n;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
