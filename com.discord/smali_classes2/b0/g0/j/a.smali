.class public final enum Lb0/g0/j/a;
.super Ljava/lang/Enum;
.source "ErrorCode.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/j/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lb0/g0/j/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lb0/g0/j/a;

.field public static final enum e:Lb0/g0/j/a;

.field public static final enum f:Lb0/g0/j/a;

.field public static final enum g:Lb0/g0/j/a;

.field public static final enum h:Lb0/g0/j/a;

.field public static final enum i:Lb0/g0/j/a;

.field public static final synthetic j:[Lb0/g0/j/a;

.field public static final k:Lb0/g0/j/a$a;


# instance fields
.field private final httpCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xe

    new-array v0, v0, [Lb0/g0/j/a;

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "NO_ERROR"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lb0/g0/j/a;->d:Lb0/g0/j/a;

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "PROTOCOL_ERROR"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "INTERNAL_ERROR"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lb0/g0/j/a;->f:Lb0/g0/j/a;

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "FLOW_CONTROL_ERROR"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lb0/g0/j/a;->g:Lb0/g0/j/a;

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "SETTINGS_TIMEOUT"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "STREAM_CLOSED"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "FRAME_SIZE_ERROR"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "REFUSED_STREAM"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lb0/g0/j/a;->h:Lb0/g0/j/a;

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "CANCEL"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "COMPRESSION_ERROR"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "CONNECT_ERROR"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "ENHANCE_YOUR_CALM"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "INADEQUATE_SECURITY"

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    new-instance v1, Lb0/g0/j/a;

    const-string v2, "HTTP_1_1_REQUIRED"

    const/16 v3, 0xd

    invoke-direct {v1, v2, v3, v3}, Lb0/g0/j/a;-><init>(Ljava/lang/String;II)V

    aput-object v1, v0, v3

    sput-object v0, Lb0/g0/j/a;->j:[Lb0/g0/j/a;

    new-instance v0, Lb0/g0/j/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lb0/g0/j/a$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lb0/g0/j/a;->k:Lb0/g0/j/a$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lb0/g0/j/a;->httpCode:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lb0/g0/j/a;
    .locals 1

    const-class v0, Lb0/g0/j/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lb0/g0/j/a;

    return-object p0
.end method

.method public static values()[Lb0/g0/j/a;
    .locals 1

    sget-object v0, Lb0/g0/j/a;->j:[Lb0/g0/j/a;

    invoke-virtual {v0}, [Lb0/g0/j/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb0/g0/j/a;

    return-object v0
.end method


# virtual methods
.method public final f()I
    .locals 1

    iget v0, p0, Lb0/g0/j/a;->httpCode:I

    return v0
.end method
