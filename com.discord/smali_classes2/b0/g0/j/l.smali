.class public final Lb0/g0/j/l;
.super Ljava/lang/Object;
.source "Http2ExchangeCodec.kt"

# interfaces
.implements Lb0/g0/h/d;


# static fields
.field public static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:Lb0/g0/j/n;

.field public final b:Lb0/z;

.field public volatile c:Z

.field public final d:Lb0/g0/g/j;

.field public final e:Lb0/g0/h/g;

.field public final f:Lb0/g0/j/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    const-string v0, "connection"

    const-string v1, "host"

    const-string v2, "keep-alive"

    const-string v3, "proxy-connection"

    const-string v4, "te"

    const-string v5, "transfer-encoding"

    const-string v6, "encoding"

    const-string v7, "upgrade"

    const-string v8, ":method"

    const-string v9, ":path"

    const-string v10, ":scheme"

    const-string v11, ":authority"

    filled-new-array/range {v0 .. v11}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb0/g0/c;->m([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lb0/g0/j/l;->g:Ljava/util/List;

    const-string v1, "connection"

    const-string v2, "host"

    const-string v3, "keep-alive"

    const-string v4, "proxy-connection"

    const-string v5, "te"

    const-string v6, "transfer-encoding"

    const-string v7, "encoding"

    const-string v8, "upgrade"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lb0/g0/c;->m([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lb0/g0/j/l;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lb0/y;Lb0/g0/g/j;Lb0/g0/h/g;Lb0/g0/j/e;)V
    .locals 1

    const-string v0, "client"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connection"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "http2Connection"

    invoke-static {p4, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lb0/g0/j/l;->d:Lb0/g0/g/j;

    iput-object p3, p0, Lb0/g0/j/l;->e:Lb0/g0/h/g;

    iput-object p4, p0, Lb0/g0/j/l;->f:Lb0/g0/j/e;

    iget-object p1, p1, Lb0/y;->v:Ljava/util/List;

    sget-object p2, Lb0/z;->h:Lb0/z;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Lb0/z;->g:Lb0/z;

    :goto_0
    iput-object p2, p0, Lb0/g0/j/l;->b:Lb0/z;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb0/g0/j/n;->g()Lc0/v;

    move-result-object v0

    check-cast v0, Lb0/g0/j/n$a;

    invoke-virtual {v0}, Lb0/g0/j/n$a;->close()V

    return-void

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public b(Lb0/a0;)V
    .locals 14

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p1, Lb0/a0;->e:Lokhttp3/RequestBody;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const-string v3, "request"

    invoke-static {p1, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p1, Lb0/a0;->d:Lokhttp3/Headers;

    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v3}, Lokhttp3/Headers;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x4

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Lb0/g0/j/b;

    sget-object v6, Lb0/g0/j/b;->f:Lokio/ByteString;

    iget-object v7, p1, Lb0/a0;->c:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lb0/g0/j/b;

    sget-object v6, Lb0/g0/j/b;->g:Lokio/ByteString;

    iget-object v7, p1, Lb0/a0;->b:Lb0/x;

    const-string v8, "url"

    invoke-static {v7, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Lb0/x;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7}, Lb0/x;->d()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v8, 0x3f

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :cond_2
    invoke-direct {v5, v6, v8}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v5, "Host"

    invoke-virtual {p1, v5}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    new-instance v6, Lb0/g0/j/b;

    sget-object v7, Lb0/g0/j/b;->i:Lokio/ByteString;

    invoke-direct {v6, v7, v5}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v5, Lb0/g0/j/b;

    sget-object v6, Lb0/g0/j/b;->h:Lokio/ByteString;

    iget-object p1, p1, Lb0/a0;->b:Lb0/x;

    iget-object p1, p1, Lb0/x;->b:Ljava/lang/String;

    invoke-direct {v5, v6, p1}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lokhttp3/Headers;->size()I

    move-result p1

    const/4 v5, 0x0

    :goto_1
    if-ge v5, p1, :cond_7

    invoke-virtual {v3, v5}, Lokhttp3/Headers;->d(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Locale.US"

    invoke-static {v7, v8}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v6, :cond_6

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v6, v7}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v7, Lb0/g0/j/l;->g:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "te"

    invoke-static {v6, v7}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v3, v5}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "trailers"

    invoke-static {v7, v8}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_4
    new-instance v7, Lb0/g0/j/b;

    invoke-virtual {v3, v5}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v6, v8}, Lb0/g0/j/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    iget-object p1, p0, Lb0/g0/j/l;->f:Lb0/g0/j/e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "requestHeaders"

    invoke-static {v4, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    xor-int/lit8 v3, v0, 0x1

    const/4 v9, 0x0

    iget-object v11, p1, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    monitor-enter v11

    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget v5, p1, Lb0/g0/j/e;->i:I

    const v6, 0x3fffffff    # 1.9999999f

    if-le v5, v6, :cond_8

    sget-object v5, Lb0/g0/j/a;->h:Lb0/g0/j/a;

    invoke-virtual {p1, v5}, Lb0/g0/j/e;->e(Lb0/g0/j/a;)V

    :cond_8
    iget-boolean v5, p1, Lb0/g0/j/e;->j:Z

    if-nez v5, :cond_11

    iget v12, p1, Lb0/g0/j/e;->i:I

    add-int/lit8 v5, v12, 0x2

    iput v5, p1, Lb0/g0/j/e;->i:I

    new-instance v13, Lb0/g0/j/n;

    const/4 v10, 0x0

    move-object v5, v13

    move v6, v12

    move-object v7, p1

    move v8, v3

    invoke-direct/range {v5 .. v10}, Lb0/g0/j/n;-><init>(ILb0/g0/j/e;ZZLokhttp3/Headers;)V

    if-eqz v0, :cond_9

    iget-wide v5, p1, Lb0/g0/j/e;->z:J

    iget-wide v7, p1, Lb0/g0/j/e;->A:J

    cmp-long v0, v5, v7

    if-gez v0, :cond_9

    iget-wide v5, v13, Lb0/g0/j/n;->c:J

    iget-wide v7, v13, Lb0/g0/j/n;->d:J

    cmp-long v0, v5, v7

    if-ltz v0, :cond_a

    :cond_9
    const/4 v1, 0x1

    :cond_a
    invoke-virtual {v13}, Lb0/g0/j/n;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p1, Lb0/g0/j/e;->f:Ljava/util/Map;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_b
    :try_start_2
    monitor-exit p1

    iget-object v0, p1, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {v0, v3, v12, v4}, Lb0/g0/j/o;->e(ZILjava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v11

    if-eqz v1, :cond_c

    iget-object p1, p1, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {p1}, Lb0/g0/j/o;->flush()V

    :cond_c
    iput-object v13, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    iget-boolean p1, p0, Lb0/g0/j/l;->c:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_e

    iget-object p1, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-nez p1, :cond_d

    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v0

    :cond_d
    sget-object v0, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    invoke-virtual {p1, v0}, Lb0/g0/j/n;->e(Lb0/g0/j/a;)V

    new-instance p1, Ljava/io/IOException;

    const-string v0, "Canceled"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    iget-object p1, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz p1, :cond_10

    iget-object p1, p1, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    iget-object v1, p0, Lb0/g0/j/l;->e:Lb0/g0/h/g;

    iget v1, v1, Lb0/g0/h/g;->h:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1, v2, v3}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    iget-object p1, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz p1, :cond_f

    iget-object p1, p1, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    iget-object v0, p0, Lb0/g0/j/l;->e:Lb0/g0/h/g;

    iget v0, v0, Lb0/g0/h/g;->i:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1, v3}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    return-void

    :cond_f
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v0

    :cond_10
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v0

    :cond_11
    :try_start_3
    new-instance v0, Lokhttp3/internal/http2/ConnectionShutdownException;

    invoke-direct {v0}, Lokhttp3/internal/http2/ConnectionShutdownException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p1

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v11

    throw p1
.end method

.method public c(Lokhttp3/Response;)Lc0/x;
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lb0/g0/j/n;->g:Lb0/g0/j/n$b;

    return-object p1

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb0/g0/j/l;->c:Z

    iget-object v0, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz v0, :cond_0

    sget-object v1, Lb0/g0/j/a;->i:Lb0/g0/j/a;

    invoke-virtual {v0, v1}, Lb0/g0/j/n;->e(Lb0/g0/j/a;)V

    :cond_0
    return-void
.end method

.method public d(Z)Lokhttp3/Response$a;
    .locals 11

    iget-object v0, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    const/4 v1, 0x0

    if-eqz v0, :cond_a

    monitor-enter v0

    :try_start_0
    iget-object v2, v0, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    invoke-virtual {v2}, Lc0/b;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    iget-object v2, v0, Lb0/g0/j/n;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lb0/g0/j/n;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v2, v0, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    invoke-virtual {v2}, Lb0/g0/j/n$c;->m()V

    iget-object v2, v0, Lb0/g0/j/n;->e:Ljava/util/ArrayDeque;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_7

    iget-object v2, v0, Lb0/g0/j/n;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "headersQueue.removeFirst()"

    invoke-static {v2, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lokhttp3/Headers;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v0

    iget-object v0, p0, Lb0/g0/j/l;->b:Lb0/z;

    const-string v3, "headerBlock"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "protocol"

    invoke-static {v0, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0x14

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2}, Lokhttp3/Headers;->size()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, v1

    :goto_1
    if-ge v6, v4, :cond_3

    invoke-virtual {v2, v6}, Lokhttp3/Headers;->d(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, ":status"

    invoke-static {v8, v10}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HTTP/1.1 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lb0/g0/h/j;->a(Ljava/lang/String;)Lb0/g0/h/j;

    move-result-object v7

    goto :goto_2

    :cond_1
    sget-object v10, Lb0/g0/j/l;->h:Ljava/util/List;

    invoke-interface {v10, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "name"

    invoke-static {v8, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "value"

    invoke-static {v9, v10}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v9}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_3
    if-eqz v7, :cond_6

    new-instance v2, Lokhttp3/Response$a;

    invoke-direct {v2}, Lokhttp3/Response$a;-><init>()V

    invoke-virtual {v2, v0}, Lokhttp3/Response$a;->f(Lb0/z;)Lokhttp3/Response$a;

    iget v0, v7, Lb0/g0/h/j;->b:I

    iput v0, v2, Lokhttp3/Response$a;->c:I

    iget-object v0, v7, Lb0/g0/h/j;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lokhttp3/Response$a;->e(Ljava/lang/String;)Lokhttp3/Response$a;

    new-array v0, v5, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, [Ljava/lang/String;

    new-instance v3, Lokhttp3/Headers;

    invoke-direct {v3, v0, v1}, Lokhttp3/Headers;-><init>([Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v2, v3}, Lokhttp3/Response$a;->d(Lokhttp3/Headers;)Lokhttp3/Response$a;

    if-eqz p1, :cond_4

    iget p1, v2, Lokhttp3/Response$a;->c:I

    const/16 v0, 0x64

    if-ne p1, v0, :cond_4

    goto :goto_3

    :cond_4
    move-object v1, v2

    :goto_3
    return-object v1

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Ljava/net/ProtocolException;

    const-string v0, "Expected \':status\' header not present"

    invoke-direct {p1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :try_start_3
    iget-object p1, v0, Lb0/g0/j/n;->l:Ljava/io/IOException;

    if-nez p1, :cond_9

    new-instance p1, Lokhttp3/internal/http2/StreamResetException;

    iget-object v2, v0, Lb0/g0/j/n;->k:Lb0/g0/j/a;

    if-nez v2, :cond_8

    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    :cond_8
    :try_start_4
    invoke-direct {p1, v2}, Lokhttp3/internal/http2/StreamResetException;-><init>(Lb0/g0/j/a;)V

    :cond_9
    throw p1

    :catchall_0
    move-exception p1

    iget-object v1, v0, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    invoke-virtual {v1}, Lb0/g0/j/n$c;->m()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1

    :cond_a
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1
.end method

.method public e()Lb0/g0/g/j;
    .locals 1

    iget-object v0, p0, Lb0/g0/j/l;->d:Lb0/g0/g/j;

    return-object v0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lb0/g0/j/l;->f:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {v0}, Lb0/g0/j/o;->flush()V

    return-void
.end method

.method public g(Lokhttp3/Response;)J
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lb0/g0/h/e;->a(Lokhttp3/Response;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lb0/g0/c;->l(Lokhttp3/Response;)J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public h(Lb0/a0;J)Lc0/v;
    .locals 0

    const-string p2, "request"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lb0/g0/j/l;->a:Lb0/g0/j/n;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lb0/g0/j/n;->g()Lc0/v;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1
.end method
