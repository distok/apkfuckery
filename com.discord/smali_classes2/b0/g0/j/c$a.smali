.class public final Lb0/g0/j/c$a;
.super Ljava/lang/Object;
.source "Hpack.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/j/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/g0/j/b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lc0/g;

.field public c:[Lb0/g0/j/b;

.field public d:I

.field public e:I

.field public f:I

.field public final g:I

.field public h:I


# direct methods
.method public constructor <init>(Lc0/x;III)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    move p3, p2

    :cond_0
    const-string p4, "source"

    invoke-static {p1, p4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lb0/g0/j/c$a;->g:I

    iput p3, p0, Lb0/g0/j/c$a;->h:I

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lb0/g0/j/c$a;->a:Ljava/util/List;

    const-string p2, "$this$buffer"

    invoke-static {p1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lc0/r;

    invoke-direct {p2, p1}, Lc0/r;-><init>(Lc0/x;)V

    iput-object p2, p0, Lb0/g0/j/c$a;->b:Lc0/g;

    const/16 p1, 0x8

    new-array p1, p1, [Lb0/g0/j/b;

    iput-object p1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    const/4 p1, 0x7

    iput p1, p0, Lb0/g0/j/c$a;->d:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v1, v0

    const-string v2, "$this$fill"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    iget-object v0, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lb0/g0/j/c$a;->d:I

    iput v2, p0, Lb0/g0/j/c$a;->e:I

    iput v2, p0, Lb0/g0/j/c$a;->f:I

    return-void
.end method

.method public final b(I)I
    .locals 1

    iget v0, p0, Lb0/g0/j/c$a;->d:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, p1

    return v0
.end method

.method public final c(I)I
    .locals 4

    const/4 v0, 0x0

    if-lez p1, :cond_2

    iget-object v1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    iget v2, p0, Lb0/g0/j/c$a;->d:I

    if-lt v1, v2, :cond_1

    if-lez p1, :cond_1

    iget-object v2, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    iget v2, v2, Lb0/g0/j/b;->a:I

    sub-int/2addr p1, v2

    iget v3, p0, Lb0/g0/j/c$a;->f:I

    sub-int/2addr v3, v2

    iput v3, p0, Lb0/g0/j/c$a;->f:I

    iget v2, p0, Lb0/g0/j/c$a;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lb0/g0/j/c$a;->e:I

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_1
    iget-object p1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    add-int/lit8 v1, v2, 0x1

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v0

    iget v3, p0, Lb0/g0/j/c$a;->e:I

    invoke-static {p1, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lb0/g0/j/c$a;->d:I

    add-int/2addr p1, v0

    iput p1, p0, Lb0/g0/j/c$a;->d:I

    :cond_2
    return v0
.end method

.method public final d(I)Lokio/ByteString;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-ltz p1, :cond_0

    sget-object v0, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    sget-object v0, Lb0/g0/j/c;->a:[Lb0/g0/j/b;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    sget-object v0, Lb0/g0/j/c;->a:[Lb0/g0/j/b;

    aget-object p1, v0, p1

    iget-object p1, p1, Lb0/g0/j/b;->b:Lokio/ByteString;

    goto :goto_1

    :cond_1
    sget-object v0, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    sget-object v0, Lb0/g0/j/c;->a:[Lb0/g0/j/b;

    array-length v0, v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lb0/g0/j/c$a;->b(I)I

    move-result v0

    if-ltz v0, :cond_3

    iget-object v1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v2, v1

    if-ge v0, v2, :cond_3

    aget-object p1, v1, v0

    if-eqz p1, :cond_2

    iget-object p1, p1, Lb0/g0/j/b;->b:Lokio/ByteString;

    :goto_1
    return-object p1

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Header index too large "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e(ILb0/g0/j/b;)V
    .locals 6

    iget-object v0, p0, Lb0/g0/j/c$a;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p2, Lb0/g0/j/b;->a:I

    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    iget-object v2, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    iget v3, p0, Lb0/g0/j/c$a;->d:I

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, p1

    aget-object v2, v2, v3

    if-eqz v2, :cond_0

    iget v2, v2, Lb0/g0/j/b;->a:I

    sub-int/2addr v0, v2

    goto :goto_0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_1
    :goto_0
    iget v2, p0, Lb0/g0/j/c$a;->h:I

    if-le v0, v2, :cond_2

    invoke-virtual {p0}, Lb0/g0/j/c$a;->a()V

    return-void

    :cond_2
    iget v3, p0, Lb0/g0/j/c$a;->f:I

    add-int/2addr v3, v0

    sub-int/2addr v3, v2

    invoke-virtual {p0, v3}, Lb0/g0/j/c$a;->c(I)I

    move-result v2

    if-ne p1, v1, :cond_4

    iget p1, p0, Lb0/g0/j/c$a;->e:I

    add-int/lit8 p1, p1, 0x1

    iget-object v2, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v3, v2

    if-le p1, v3, :cond_3

    array-length p1, v2

    mul-int/lit8 p1, p1, 0x2

    new-array p1, p1, [Lb0/g0/j/b;

    const/4 v3, 0x0

    array-length v4, v2

    array-length v5, v2

    invoke-static {v2, v3, p1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v2, v2

    add-int/2addr v2, v1

    iput v2, p0, Lb0/g0/j/c$a;->d:I

    iput-object p1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    :cond_3
    iget p1, p0, Lb0/g0/j/c$a;->d:I

    add-int/lit8 v1, p1, -0x1

    iput v1, p0, Lb0/g0/j/c$a;->d:I

    iget-object v1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    aput-object p2, v1, p1

    iget p1, p0, Lb0/g0/j/c$a;->e:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lb0/g0/j/c$a;->e:I

    goto :goto_1

    :cond_4
    iget v1, p0, Lb0/g0/j/c$a;->d:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v1, p1

    add-int/2addr v1, v2

    add-int/2addr v1, p1

    iget-object p1, p0, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    aput-object p2, p1, v1

    :goto_1
    iget p1, p0, Lb0/g0/j/c$a;->f:I

    add-int/2addr p1, v0

    iput p1, p0, Lb0/g0/j/c$a;->f:I

    return-void
.end method

.method public final f()Lokio/ByteString;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/c$a;->b:Lc0/g;

    invoke-interface {v0}, Lc0/g;->readByte()B

    move-result v0

    sget-object v1, Lb0/g0/c;->a:[B

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v0, 0x80

    const/16 v2, 0x80

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0x7f

    invoke-virtual {p0, v0, v2}, Lb0/g0/j/c$a;->g(II)I

    move-result v0

    int-to-long v4, v0

    if-eqz v1, :cond_a

    new-instance v0, Lc0/e;

    invoke-direct {v0}, Lc0/e;-><init>()V

    sget-object v1, Lb0/g0/j/p;->d:Lb0/g0/j/p;

    iget-object v1, p0, Lb0/g0/j/c$a;->b:Lc0/g;

    sget-object v2, Lb0/g0/j/p;->c:Lb0/g0/j/p$a;

    const-string v6, "source"

    invoke-static {v1, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "sink"

    invoke-static {v0, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v6, 0x0

    move-object v9, v2

    move-wide v7, v6

    const/4 v6, 0x0

    :goto_1
    const/4 v10, 0x0

    cmp-long v11, v7, v4

    if-gez v11, :cond_5

    invoke-interface {v1}, Lc0/g;->readByte()B

    move-result v11

    sget-object v12, Lb0/g0/c;->a:[B

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v3, v11

    add-int/lit8 v6, v6, 0x8

    :goto_2
    const/16 v11, 0x8

    if-lt v6, v11, :cond_4

    add-int/lit8 v11, v6, -0x8

    ushr-int v12, v3, v11

    and-int/lit16 v12, v12, 0xff

    iget-object v9, v9, Lb0/g0/j/p$a;->a:[Lb0/g0/j/p$a;

    if-eqz v9, :cond_3

    aget-object v9, v9, v12

    if-eqz v9, :cond_2

    iget-object v12, v9, Lb0/g0/j/p$a;->a:[Lb0/g0/j/p$a;

    if-nez v12, :cond_1

    iget v11, v9, Lb0/g0/j/p$a;->b:I

    invoke-virtual {v0, v11}, Lc0/e;->N(I)Lc0/e;

    iget v9, v9, Lb0/g0/j/p$a;->c:I

    sub-int/2addr v6, v9

    move-object v9, v2

    goto :goto_2

    :cond_1
    move v6, v11

    goto :goto_2

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v10

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v10

    :cond_4
    const-wide/16 v10, 0x1

    add-long/2addr v7, v10

    goto :goto_1

    :cond_5
    :goto_3
    if-lez v6, :cond_9

    rsub-int/lit8 v1, v6, 0x8

    shl-int v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    iget-object v4, v9, Lb0/g0/j/p$a;->a:[Lb0/g0/j/p$a;

    if-eqz v4, :cond_8

    aget-object v1, v4, v1

    if-eqz v1, :cond_7

    iget-object v4, v1, Lb0/g0/j/p$a;->a:[Lb0/g0/j/p$a;

    if-nez v4, :cond_9

    iget v4, v1, Lb0/g0/j/p$a;->c:I

    if-le v4, v6, :cond_6

    goto :goto_4

    :cond_6
    iget v4, v1, Lb0/g0/j/p$a;->b:I

    invoke-virtual {v0, v4}, Lc0/e;->N(I)Lc0/e;

    iget v1, v1, Lb0/g0/j/p$a;->c:I

    sub-int/2addr v6, v1

    move-object v9, v2

    goto :goto_3

    :cond_7
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v10

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v10

    :cond_9
    :goto_4
    invoke-virtual {v0}, Lc0/e;->o()Lokio/ByteString;

    move-result-object v0

    goto :goto_5

    :cond_a
    iget-object v0, p0, Lb0/g0/j/c$a;->b:Lc0/g;

    invoke-interface {v0, v4, v5}, Lc0/g;->u(J)Lokio/ByteString;

    move-result-object v0

    :goto_5
    return-object v0
.end method

.method public final g(II)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    and-int/2addr p1, p2

    if-ge p1, p2, :cond_0

    return p1

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lb0/g0/j/c$a;->b:Lc0/g;

    invoke-interface {v0}, Lc0/g;->readByte()B

    move-result v0

    sget-object v1, Lb0/g0/c;->a:[B

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_1

    and-int/lit8 v0, v0, 0x7f

    shl-int/2addr v0, p1

    add-int/2addr p2, v0

    add-int/lit8 p1, p1, 0x7

    goto :goto_0

    :cond_1
    shl-int p1, v0, p1

    add-int/2addr p2, p1

    return p2
.end method
