.class public final Lb0/g0/j/n$a;
.super Ljava/lang/Object;
.source "Http2Stream.kt"

# interfaces
.implements Lc0/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/j/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final d:Lc0/e;

.field public e:Z

.field public f:Z

.field public final synthetic g:Lb0/g0/j/n;


# direct methods
.method public constructor <init>(Lb0/g0/j/n;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Lb0/g0/j/n$a;->f:Z

    new-instance p1, Lc0/e;

    invoke-direct {p1}, Lc0/e;-><init>()V

    iput-object p1, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v1, v1, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    invoke-virtual {v1}, Lc0/b;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :goto_0
    :try_start_1
    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-wide v2, v1, Lb0/g0/j/n;->c:J

    iget-wide v4, v1, Lb0/g0/j/n;->d:J

    cmp-long v6, v2, v4

    if-ltz v6, :cond_0

    iget-boolean v2, p0, Lb0/g0/j/n$a;->f:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lb0/g0/j/n$a;->e:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lb0/g0/j/n;->f()Lb0/g0/j/a;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    invoke-virtual {v1}, Lb0/g0/j/n;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v1, v1, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    invoke-virtual {v1}, Lb0/g0/j/n$c;->m()V

    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    invoke-virtual {v1}, Lb0/g0/j/n;->b()V

    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-wide v2, v1, Lb0/g0/j/n;->d:J

    iget-wide v4, v1, Lb0/g0/j/n;->c:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    iget-wide v4, v1, Lc0/e;->e:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-wide v2, v1, Lb0/g0/j/n;->c:J

    add-long/2addr v2, v10

    iput-wide v2, v1, Lb0/g0/j/n;->c:J

    if-eqz p1, :cond_1

    iget-object p1, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    iget-wide v2, p1, Lc0/e;->e:J

    cmp-long p1, v10, v2

    if-nez p1, :cond_1

    invoke-virtual {v1}, Lb0/g0/j/n;->f()Lb0/g0/j/a;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-nez p1, :cond_1

    const/4 p1, 0x1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    const/4 v8, 0x0

    :goto_1
    monitor-exit v0

    iget-object p1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object p1, p1, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    invoke-virtual {p1}, Lc0/b;->i()V

    :try_start_3
    iget-object p1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v6, p1, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget v7, p1, Lb0/g0/j/n;->m:I

    iget-object v9, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    invoke-virtual/range {v6 .. v11}, Lb0/g0/j/e;->g(IZLc0/e;J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object p1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object p1, p1, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    invoke-virtual {p1}, Lb0/g0/j/n$c;->m()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v0, v0, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    invoke-virtual {v0}, Lb0/g0/j/n$c;->m()V

    throw p1

    :catchall_1
    move-exception p1

    :try_start_4
    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v1, v1, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    invoke-virtual {v1}, Lb0/g0/j/n$c;->m()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public close()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    sget-object v1, Lb0/g0/c;->a:[B

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lb0/g0/j/n$a;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    invoke-virtual {v1}, Lb0/g0/j/n;->f()Lb0/g0/j/a;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v4, v0, Lb0/g0/j/n;->h:Lb0/g0/j/n$a;

    iget-boolean v4, v4, Lb0/g0/j/n$a;->f:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    iget-wide v4, v4, Lc0/e;->e:J

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_2

    const/4 v2, 0x1

    :cond_2
    if-eqz v2, :cond_3

    :goto_1
    iget-object v0, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    iget-wide v0, v0, Lc0/e;->e:J

    cmp-long v2, v0, v6

    if-lez v2, :cond_4

    invoke-virtual {p0, v3}, Lb0/g0/j/n$a;->a(Z)V

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    iget-object v8, v0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget v9, v0, Lb0/g0/j/n;->m:I

    const/4 v10, 0x1

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    invoke-virtual/range {v8 .. v13}, Lb0/g0/j/e;->g(IZLc0/e;J)V

    :cond_4
    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    monitor-enter v0

    :try_start_2
    iput-boolean v3, p0, Lb0/g0/j/n$a;->e:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v0, v0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {v0}, Lb0/g0/j/o;->flush()V

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    invoke-virtual {v0}, Lb0/g0/j/n;->a()V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public flush()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    sget-object v1, Lb0/g0/c;->a:[B

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    invoke-virtual {v1}, Lb0/g0/j/n;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    :goto_0
    iget-object v0, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    iget-wide v0, v0, Lc0/e;->e:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb0/g0/j/n$a;->a(Z)V

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v0, v0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget-object v0, v0, Lb0/g0/j/e;->C:Lb0/g0/j/o;

    invoke-virtual {v0}, Lb0/g0/j/o;->flush()V

    goto :goto_0

    :cond_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lb0/g0/j/n$a;->g:Lb0/g0/j/n;

    iget-object v0, v0, Lb0/g0/j/n;->j:Lb0/g0/j/n$c;

    return-object v0
.end method

.method public write(Lc0/e;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lb0/g0/c;->a:[B

    iget-object v0, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    invoke-virtual {v0, p1, p2, p3}, Lc0/e;->write(Lc0/e;J)V

    :goto_0
    iget-object p1, p0, Lb0/g0/j/n$a;->d:Lc0/e;

    iget-wide p1, p1, Lc0/e;->e:J

    const-wide/16 v0, 0x4000

    cmp-long p3, p1, v0

    if-ltz p3, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lb0/g0/j/n$a;->a(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method
