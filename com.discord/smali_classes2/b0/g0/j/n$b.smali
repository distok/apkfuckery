.class public final Lb0/g0/j/n$b;
.super Ljava/lang/Object;
.source "Http2Stream.kt"

# interfaces
.implements Lc0/x;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/j/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final d:Lc0/e;

.field public final e:Lc0/e;

.field public f:Z

.field public final g:J

.field public h:Z

.field public final synthetic i:Lb0/g0/j/n;


# direct methods
.method public constructor <init>(Lb0/g0/j/n;JZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ)V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lb0/g0/j/n$b;->g:J

    iput-boolean p4, p0, Lb0/g0/j/n$b;->h:Z

    new-instance p1, Lc0/e;

    invoke-direct {p1}, Lc0/e;-><init>()V

    iput-object p1, p0, Lb0/g0/j/n$b;->d:Lc0/e;

    new-instance p1, Lc0/e;

    invoke-direct {p1}, Lc0/e;-><init>()V

    iput-object p1, p0, Lb0/g0/j/n$b;->e:Lc0/e;

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 2

    iget-object v0, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    sget-object v1, Lb0/g0/c;->a:[B

    iget-object v0, v0, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    invoke-virtual {v0, p1, p2}, Lb0/g0/j/e;->f(J)V

    return-void
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lb0/g0/j/n$b;->f:Z

    iget-object v1, p0, Lb0/g0/j/n$b;->e:Lc0/e;

    iget-wide v2, v1, Lc0/e;->e:J

    invoke-virtual {v1, v2, v3}, Lc0/e;->skip(J)V

    iget-object v1, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    const-wide/16 v0, 0x0

    cmp-long v4, v2, v0

    if-lez v4, :cond_0

    invoke-virtual {p0, v2, v3}, Lb0/g0/j/n$b;->a(J)V

    :cond_0
    iget-object v0, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    invoke-virtual {v0}, Lb0/g0/j/n;->a()V

    return-void

    :cond_1
    :try_start_1
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type java.lang.Object"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-object v0, v0, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    return-object v0
.end method

.method public v0(Lc0/e;J)J
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "sink"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-ltz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_b

    :goto_1
    iget-object v2, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-object v3, v3, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    invoke-virtual {v3}, Lc0/b;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    invoke-virtual {v3}, Lb0/g0/j/n;->f()Lb0/g0/j/a;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_3

    iget-object v3, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-object v3, v3, Lb0/g0/j/n;->l:Ljava/io/IOException;

    if-eqz v3, :cond_1

    :goto_2
    move-object v4, v3

    goto :goto_3

    :cond_1
    new-instance v3, Lokhttp3/internal/http2/StreamResetException;

    iget-object v5, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    invoke-virtual {v5}, Lb0/g0/j/n;->f()Lb0/g0/j/a;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-direct {v3, v5}, Lokhttp3/internal/http2/StreamResetException;-><init>(Lb0/g0/j/a;)V

    goto :goto_2

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :cond_3
    :goto_3
    :try_start_2
    iget-boolean v3, p0, Lb0/g0/j/n$b;->f:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lb0/g0/j/n$b;->e:Lc0/e;

    iget-wide v5, v3, Lc0/e;->e:J

    const-wide/16 v7, -0x1

    cmp-long v9, v5, v0

    if-lez v9, :cond_4

    invoke-static {p2, p3, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-virtual {v3, p1, v0, v1}, Lc0/e;->v0(Lc0/e;J)J

    move-result-wide v0

    iget-object v3, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-wide v5, v3, Lb0/g0/j/n;->a:J

    add-long/2addr v5, v0

    iput-wide v5, v3, Lb0/g0/j/n;->a:J

    iget-wide v9, v3, Lb0/g0/j/n;->b:J

    sub-long/2addr v5, v9

    if-nez v4, :cond_6

    iget-object v3, v3, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget-object v3, v3, Lb0/g0/j/e;->v:Lb0/g0/j/s;

    invoke-virtual {v3}, Lb0/g0/j/s;->a()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v9, v3

    cmp-long v3, v5, v9

    if-ltz v3, :cond_6

    iget-object v3, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-object v9, v3, Lb0/g0/j/n;->n:Lb0/g0/j/e;

    iget v3, v3, Lb0/g0/j/n;->m:I

    invoke-virtual {v9, v3, v5, v6}, Lb0/g0/j/e;->n(IJ)V

    iget-object v3, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-wide v5, v3, Lb0/g0/j/n;->a:J

    iput-wide v5, v3, Lb0/g0/j/n;->b:J

    goto :goto_4

    :cond_4
    iget-boolean v0, p0, Lb0/g0/j/n$b;->h:Z

    if-nez v0, :cond_5

    if-nez v4, :cond_5

    iget-object v0, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    invoke-virtual {v0}, Lb0/g0/j/n;->l()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x1

    move-wide v5, v7

    goto :goto_5

    :cond_5
    move-wide v0, v7

    :cond_6
    :goto_4
    const/4 v3, 0x0

    move-wide v5, v0

    const/4 v0, 0x0

    :goto_5
    :try_start_3
    iget-object v1, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-object v1, v1, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    invoke-virtual {v1}, Lb0/g0/j/n$c;->m()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v2

    if-eqz v0, :cond_7

    const-wide/16 v0, 0x0

    goto/16 :goto_1

    :cond_7
    cmp-long p1, v5, v7

    if-eqz p1, :cond_8

    invoke-virtual {p0, v5, v6}, Lb0/g0/j/n$b;->a(J)V

    return-wide v5

    :cond_8
    if-nez v4, :cond_9

    return-wide v7

    :cond_9
    throw v4

    :cond_a
    :try_start_4
    new-instance p1, Ljava/io/IOException;

    const-string p2, "stream closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    :try_start_5
    iget-object p2, p0, Lb0/g0/j/n$b;->i:Lb0/g0/j/n;

    iget-object p2, p2, Lb0/g0/j/n;->i:Lb0/g0/j/n$c;

    invoke-virtual {p2}, Lb0/g0/j/n$c;->m()V

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v2

    throw p1

    :cond_b
    const-string p1, "byteCount < 0: "

    invoke-static {p1, p2, p3}, Lf/e/c/a/a;->o(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
