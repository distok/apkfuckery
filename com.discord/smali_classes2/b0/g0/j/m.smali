.class public final Lb0/g0/j/m;
.super Ljava/lang/Object;
.source "Http2Reader.kt"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/j/m$a;,
        Lb0/g0/j/m$b;
    }
.end annotation


# static fields
.field public static final h:Ljava/util/logging/Logger;

.field public static final i:Lb0/g0/j/m;


# instance fields
.field public final d:Lb0/g0/j/m$a;

.field public final e:Lb0/g0/j/c$a;

.field public final f:Lc0/g;

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-class v0, Lb0/g0/j/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    const-string v1, "Logger.getLogger(Http2::class.java.name)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lb0/g0/j/m;->h:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lc0/g;Z)V
    .locals 3

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb0/g0/j/m;->f:Lc0/g;

    iput-boolean p2, p0, Lb0/g0/j/m;->g:Z

    new-instance p2, Lb0/g0/j/m$a;

    invoke-direct {p2, p1}, Lb0/g0/j/m$a;-><init>(Lc0/g;)V

    iput-object p2, p0, Lb0/g0/j/m;->d:Lb0/g0/j/m$a;

    new-instance p1, Lb0/g0/j/c$a;

    const/16 v0, 0x1000

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p1, p2, v0, v1, v2}, Lb0/g0/j/c$a;-><init>(Lc0/x;III)V

    iput-object p1, p0, Lb0/g0/j/m;->e:Lb0/g0/j/c$a;

    return-void
.end method


# virtual methods
.method public final a(ZLb0/g0/j/m$b;)Z
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-string v2, "handler"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, v0, Lb0/g0/j/m;->f:Lc0/g;

    const-wide/16 v4, 0x9

    invoke-interface {v3, v4, v5}, Lc0/g;->E0(J)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-static {v3}, Lb0/g0/c;->t(Lc0/g;)I

    move-result v3

    const/16 v10, 0x4000

    if-gt v3, v10, :cond_36

    iget-object v4, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v4}, Lc0/g;->readByte()B

    move-result v4

    and-int/lit16 v11, v4, 0xff

    iget-object v4, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v4}, Lc0/g;->readByte()B

    move-result v4

    and-int/lit16 v12, v4, 0xff

    iget-object v4, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v4}, Lc0/g;->readInt()I

    move-result v4

    const v13, 0x7fffffff

    and-int v14, v4, v13

    sget-object v15, Lb0/g0/j/m;->h:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v15, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lb0/g0/j/d;->e:Lb0/g0/j/d;

    const/4 v5, 0x1

    move v6, v14

    move v7, v3

    move v8, v11

    move v9, v12

    invoke-virtual/range {v4 .. v9}, Lb0/g0/j/d;->b(ZIIII)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :cond_0
    const/4 v4, 0x4

    if-eqz p1, :cond_2

    if-ne v11, v4, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Expected a SETTINGS frame but was "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lb0/g0/j/d;->e:Lb0/g0/j/d;

    invoke-virtual {v3, v11}, Lb0/g0/j/d;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    const/16 v5, 0xe

    const/4 v6, 0x5

    const-string v7, " > remaining length "

    const-string v8, "PROTOCOL_ERROR padding "

    const/16 v9, 0x8

    const/4 v15, 0x1

    packed-switch v11, :pswitch_data_0

    iget-object v1, v0, Lb0/g0/j/m;->f:Lc0/g;

    int-to-long v2, v3

    invoke-interface {v1, v2, v3}, Lc0/g;->skip(J)V

    goto/16 :goto_c

    :pswitch_0
    if-ne v3, v4, :cond_4

    iget-object v2, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readInt()I

    move-result v2

    const-wide/32 v3, 0x7fffffff

    sget-object v5, Lb0/g0/c;->a:[B

    int-to-long v5, v2

    and-long v2, v5, v3

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_3

    invoke-interface {v1, v14, v2, v3}, Lb0/g0/j/m$b;->d(IJ)V

    goto/16 :goto_c

    :cond_3
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "windowSizeIncrement was 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_WINDOW_UPDATE length !=4: "

    invoke-static {v2, v3}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    if-lt v3, v9, :cond_b

    if-nez v14, :cond_a

    iget-object v2, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readInt()I

    move-result v2

    iget-object v4, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v4}, Lc0/g;->readInt()I

    move-result v4

    sub-int/2addr v3, v9

    invoke-static {}, Lb0/g0/j/a;->values()[Lb0/g0/j/a;

    move-result-object v6

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v5, :cond_7

    aget-object v8, v6, v7

    invoke-virtual {v8}, Lb0/g0/j/a;->f()I

    move-result v9

    if-ne v9, v4, :cond_5

    const/4 v9, 0x1

    goto :goto_2

    :cond_5
    const/4 v9, 0x0

    :goto_2
    if-eqz v9, :cond_6

    goto :goto_3

    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_7
    const/4 v8, 0x0

    :goto_3
    if-eqz v8, :cond_9

    sget-object v4, Lokio/ByteString;->f:Lokio/ByteString;

    if-lez v3, :cond_8

    iget-object v4, v0, Lb0/g0/j/m;->f:Lc0/g;

    int-to-long v5, v3

    invoke-interface {v4, v5, v6}, Lc0/g;->u(J)Lokio/ByteString;

    move-result-object v4

    :cond_8
    invoke-interface {v1, v2, v8, v4}, Lb0/g0/j/m$b;->j(ILb0/g0/j/a;Lokio/ByteString;)V

    goto/16 :goto_c

    :cond_9
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_GOAWAY unexpected error code: "

    invoke-static {v2, v4}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_GOAWAY streamId != 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_GOAWAY length < 8: "

    invoke-static {v2, v3}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_2
    if-ne v3, v9, :cond_e

    if-nez v14, :cond_d

    iget-object v3, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v3}, Lc0/g;->readInt()I

    move-result v3

    iget-object v4, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v4}, Lc0/g;->readInt()I

    move-result v4

    and-int/lit8 v5, v12, 0x1

    if-eqz v5, :cond_c

    const/4 v2, 0x1

    :cond_c
    invoke-interface {v1, v2, v3, v4}, Lb0/g0/j/m$b;->f(ZII)V

    goto/16 :goto_c

    :cond_d
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_PING streamId != 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_e
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_PING length != 8: "

    invoke-static {v2, v3}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_3
    if-eqz v14, :cond_12

    and-int/lit8 v4, v12, 0x8

    if-eqz v4, :cond_f

    iget-object v2, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readByte()B

    move-result v2

    sget-object v5, Lb0/g0/c;->a:[B

    and-int/lit16 v2, v2, 0xff

    :cond_f
    iget-object v5, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v5}, Lc0/g;->readInt()I

    move-result v5

    and-int/2addr v5, v13

    add-int/lit8 v3, v3, -0x4

    if-eqz v4, :cond_10

    add-int/lit8 v3, v3, -0x1

    :cond_10
    if-gt v2, v3, :cond_11

    sub-int/2addr v3, v2

    invoke-virtual {v0, v3, v2, v12, v14}, Lb0/g0/j/m;->c(IIII)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v14, v5, v2}, Lb0/g0/j/m$b;->i(IILjava/util/List;)V

    goto/16 :goto_c

    :cond_11
    new-instance v1, Ljava/io/IOException;

    invoke-static {v8, v2, v7, v3}, Lf/e/c/a/a;->l(Ljava/lang/String;ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_12
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_4
    if-nez v14, :cond_20

    and-int/lit8 v5, v12, 0x1

    if-eqz v5, :cond_14

    if-nez v3, :cond_13

    invoke-interface/range {p2 .. p2}, Lb0/g0/j/m$b;->a()V

    goto/16 :goto_c

    :cond_13
    new-instance v1, Ljava/io/IOException;

    const-string v2, "FRAME_SIZE_ERROR ack frame should be empty!"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_14
    rem-int/lit8 v5, v3, 0x6

    if-nez v5, :cond_1f

    new-instance v5, Lb0/g0/j/s;

    invoke-direct {v5}, Lb0/g0/j/s;-><init>()V

    invoke-static {v2, v3}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    const/4 v7, 0x6

    invoke-static {v3, v7}, Lx/p/e;->step(Lkotlin/ranges/IntProgression;I)Lkotlin/ranges/IntProgression;

    move-result-object v3

    iget v7, v3, Lkotlin/ranges/IntProgression;->d:I

    iget v8, v3, Lkotlin/ranges/IntProgression;->e:I

    iget v3, v3, Lkotlin/ranges/IntProgression;->f:I

    if-ltz v3, :cond_15

    if-gt v7, v8, :cond_1e

    goto :goto_4

    :cond_15
    if-lt v7, v8, :cond_1e

    :goto_4
    iget-object v9, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v9}, Lc0/g;->readShort()S

    move-result v9

    const v11, 0xffff

    sget-object v12, Lb0/g0/c;->a:[B

    and-int/2addr v9, v11

    iget-object v11, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v11}, Lc0/g;->readInt()I

    move-result v11

    const/4 v12, 0x2

    if-eq v9, v12, :cond_1b

    const/4 v12, 0x3

    if-eq v9, v12, :cond_1a

    if-eq v9, v4, :cond_18

    if-eq v9, v6, :cond_16

    goto :goto_5

    :cond_16
    if-lt v11, v10, :cond_17

    const v12, 0xffffff

    if-gt v11, v12, :cond_17

    goto :goto_5

    :cond_17
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: "

    invoke-static {v2, v11}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_18
    const/4 v9, 0x7

    if-ltz v11, :cond_19

    goto :goto_5

    :cond_19
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1a
    const/4 v9, 0x4

    goto :goto_5

    :cond_1b
    if-eqz v11, :cond_1d

    if-ne v11, v15, :cond_1c

    goto :goto_5

    :cond_1c
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1d
    :goto_5
    invoke-virtual {v5, v9, v11}, Lb0/g0/j/s;->c(II)Lb0/g0/j/s;

    if-eq v7, v8, :cond_1e

    add-int/2addr v7, v3

    goto :goto_4

    :cond_1e
    invoke-interface {v1, v2, v5}, Lb0/g0/j/m$b;->b(ZLb0/g0/j/s;)V

    goto/16 :goto_c

    :cond_1f
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_SETTINGS length % 6 != 0: "

    invoke-static {v2, v3}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_20
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_SETTINGS streamId != 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_5
    if-ne v3, v4, :cond_26

    if-eqz v14, :cond_25

    iget-object v2, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readInt()I

    move-result v2

    invoke-static {}, Lb0/g0/j/a;->values()[Lb0/g0/j/a;

    move-result-object v3

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v5, :cond_23

    aget-object v6, v3, v4

    invoke-virtual {v6}, Lb0/g0/j/a;->f()I

    move-result v7

    if-ne v7, v2, :cond_21

    const/4 v7, 0x1

    goto :goto_7

    :cond_21
    const/4 v7, 0x0

    :goto_7
    if-eqz v7, :cond_22

    goto :goto_8

    :cond_22
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_23
    const/4 v6, 0x0

    :goto_8
    if-eqz v6, :cond_24

    invoke-interface {v1, v14, v6}, Lb0/g0/j/m$b;->h(ILb0/g0/j/a;)V

    goto/16 :goto_c

    :cond_24
    new-instance v1, Ljava/io/IOException;

    const-string v3, "TYPE_RST_STREAM unexpected error code: "

    invoke-static {v3, v2}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_25
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_RST_STREAM streamId == 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_26
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_RST_STREAM length: "

    const-string v4, " != 4"

    invoke-static {v2, v3, v4}, Lf/e/c/a/a;->k(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_6
    if-ne v3, v6, :cond_28

    if-eqz v14, :cond_27

    invoke-virtual {v0, v1, v14}, Lb0/g0/j/m;->d(Lb0/g0/j/m$b;I)V

    goto/16 :goto_c

    :cond_27
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_PRIORITY streamId == 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_28
    new-instance v1, Ljava/io/IOException;

    const-string v2, "TYPE_PRIORITY length: "

    const-string v4, " != 5"

    invoke-static {v2, v3, v4}, Lf/e/c/a/a;->k(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_7
    if-eqz v14, :cond_2e

    and-int/lit8 v4, v12, 0x1

    if-eqz v4, :cond_29

    const/4 v4, 0x1

    goto :goto_9

    :cond_29
    const/4 v4, 0x0

    :goto_9
    and-int/lit8 v5, v12, 0x8

    if-eqz v5, :cond_2a

    iget-object v2, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readByte()B

    move-result v2

    sget-object v6, Lb0/g0/c;->a:[B

    and-int/lit16 v2, v2, 0xff

    :cond_2a
    and-int/lit8 v6, v12, 0x20

    if-eqz v6, :cond_2b

    invoke-virtual {v0, v1, v14}, Lb0/g0/j/m;->d(Lb0/g0/j/m$b;I)V

    add-int/lit8 v3, v3, -0x5

    :cond_2b
    if-eqz v5, :cond_2c

    add-int/lit8 v3, v3, -0x1

    :cond_2c
    if-gt v2, v3, :cond_2d

    sub-int/2addr v3, v2

    invoke-virtual {v0, v3, v2, v12, v14}, Lb0/g0/j/m;->c(IIII)Ljava/util/List;

    move-result-object v2

    const/4 v3, -0x1

    invoke-interface {v1, v4, v14, v3, v2}, Lb0/g0/j/m$b;->c(ZIILjava/util/List;)V

    goto :goto_c

    :cond_2d
    new-instance v1, Ljava/io/IOException;

    invoke-static {v8, v2, v7, v3}, Lf/e/c/a/a;->l(Ljava/lang/String;ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2e
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_8
    if-eqz v14, :cond_35

    and-int/lit8 v4, v12, 0x1

    if-eqz v4, :cond_2f

    const/4 v4, 0x1

    goto :goto_a

    :cond_2f
    const/4 v4, 0x0

    :goto_a
    and-int/lit8 v5, v12, 0x20

    if-eqz v5, :cond_30

    const/4 v5, 0x1

    goto :goto_b

    :cond_30
    const/4 v5, 0x0

    :goto_b
    if-nez v5, :cond_34

    and-int/lit8 v5, v12, 0x8

    if-eqz v5, :cond_31

    iget-object v2, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readByte()B

    move-result v2

    sget-object v6, Lb0/g0/c;->a:[B

    and-int/lit16 v2, v2, 0xff

    :cond_31
    if-eqz v5, :cond_32

    add-int/lit8 v3, v3, -0x1

    :cond_32
    if-gt v2, v3, :cond_33

    sub-int/2addr v3, v2

    iget-object v5, v0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v1, v4, v14, v5, v3}, Lb0/g0/j/m$b;->e(ZILc0/g;I)V

    iget-object v1, v0, Lb0/g0/j/m;->f:Lc0/g;

    int-to-long v2, v2

    invoke-interface {v1, v2, v3}, Lc0/g;->skip(J)V

    goto :goto_c

    :cond_33
    new-instance v1, Ljava/io/IOException;

    invoke-static {v8, v2, v7, v3}, Lf/e/c/a/a;->l(Ljava/lang/String;ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_34
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_35
    new-instance v1, Ljava/io/IOException;

    const-string v2, "PROTOCOL_ERROR: TYPE_DATA streamId == 0"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :goto_c
    return v15

    :cond_36
    new-instance v1, Ljava/io/IOException;

    const-string v2, "FRAME_SIZE_ERROR: "

    invoke-static {v2, v3}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lb0/g0/j/m$b;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lb0/g0/j/m;->g:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1, p1}, Lb0/g0/j/m;->a(ZLb0/g0/j/m$b;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Required SETTINGS preface not received"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object p1, p0, Lb0/g0/j/m;->f:Lc0/g;

    sget-object v0, Lb0/g0/j/d;->a:Lokio/ByteString;

    invoke-virtual {v0}, Lokio/ByteString;->j()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {p1, v2, v3}, Lc0/g;->u(J)Lokio/ByteString;

    move-result-object p1

    sget-object v2, Lb0/g0/j/m;->h:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "<< CONNECTION "

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lokio/ByteString;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lb0/g0/c;->j(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-nez v0, :cond_3

    :goto_0
    return-void

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Expected a connection header but was "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lokio/ByteString;->q()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(IIII)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)",
            "Ljava/util/List<",
            "Lb0/g0/j/b;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/m;->d:Lb0/g0/j/m$a;

    iput p1, v0, Lb0/g0/j/m$a;->g:I

    iput p1, v0, Lb0/g0/j/m$a;->d:I

    iput p2, v0, Lb0/g0/j/m$a;->h:I

    iput p3, v0, Lb0/g0/j/m$a;->e:I

    iput p4, v0, Lb0/g0/j/m$a;->f:I

    iget-object p1, p0, Lb0/g0/j/m;->e:Lb0/g0/j/c$a;

    :cond_0
    :goto_0
    iget-object p2, p1, Lb0/g0/j/c$a;->b:Lc0/g;

    invoke-interface {p2}, Lc0/g;->H()Z

    move-result p2

    if-nez p2, :cond_e

    iget-object p2, p1, Lb0/g0/j/c$a;->b:Lc0/g;

    invoke-interface {p2}, Lc0/g;->readByte()B

    move-result p2

    sget-object p3, Lb0/g0/c;->a:[B

    and-int/lit16 p2, p2, 0xff

    const/16 p3, 0x80

    if-eq p2, p3, :cond_d

    and-int/lit16 p4, p2, 0x80

    if-ne p4, p3, :cond_5

    const/16 p3, 0x7f

    invoke-virtual {p1, p2, p3}, Lb0/g0/j/c$a;->g(II)I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    if-ltz p2, :cond_1

    sget-object p3, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    sget-object p3, Lb0/g0/j/c;->a:[Lb0/g0/j/b;

    array-length p3, p3

    add-int/lit8 p3, p3, -0x1

    if-gt p2, p3, :cond_1

    const/4 p3, 0x1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    :goto_1
    if-eqz p3, :cond_2

    sget-object p3, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    sget-object p3, Lb0/g0/j/c;->a:[Lb0/g0/j/b;

    aget-object p2, p3, p2

    iget-object p3, p1, Lb0/g0/j/c$a;->a:Ljava/util/List;

    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    sget-object p3, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    sget-object p3, Lb0/g0/j/c;->a:[Lb0/g0/j/b;

    array-length p3, p3

    sub-int p3, p2, p3

    invoke-virtual {p1, p3}, Lb0/g0/j/c$a;->b(I)I

    move-result p3

    if-ltz p3, :cond_4

    iget-object p4, p1, Lb0/g0/j/c$a;->c:[Lb0/g0/j/b;

    array-length v0, p4

    if-ge p3, v0, :cond_4

    iget-object p2, p1, Lb0/g0/j/c$a;->a:Ljava/util/List;

    aget-object p3, p4, p3

    if-eqz p3, :cond_3

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p1, 0x0

    throw p1

    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string p3, "Header index too large "

    invoke-static {p3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    const/4 p3, -0x1

    const/16 p4, 0x40

    if-ne p2, p4, :cond_6

    sget-object p2, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    invoke-virtual {p1}, Lb0/g0/j/c$a;->f()Lokio/ByteString;

    move-result-object p4

    invoke-virtual {p2, p4}, Lb0/g0/j/c;->a(Lokio/ByteString;)Lokio/ByteString;

    invoke-virtual {p1}, Lb0/g0/j/c$a;->f()Lokio/ByteString;

    move-result-object p2

    new-instance v0, Lb0/g0/j/b;

    invoke-direct {v0, p4, p2}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    invoke-virtual {p1, p3, v0}, Lb0/g0/j/c$a;->e(ILb0/g0/j/b;)V

    goto/16 :goto_0

    :cond_6
    and-int/lit8 v0, p2, 0x40

    if-ne v0, p4, :cond_7

    const/16 p4, 0x3f

    invoke-virtual {p1, p2, p4}, Lb0/g0/j/c$a;->g(II)I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p1, p2}, Lb0/g0/j/c$a;->d(I)Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1}, Lb0/g0/j/c$a;->f()Lokio/ByteString;

    move-result-object p4

    new-instance v0, Lb0/g0/j/b;

    invoke-direct {v0, p2, p4}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    invoke-virtual {p1, p3, v0}, Lb0/g0/j/c$a;->e(ILb0/g0/j/b;)V

    goto/16 :goto_0

    :cond_7
    and-int/lit8 p3, p2, 0x20

    const/16 p4, 0x20

    if-ne p3, p4, :cond_a

    const/16 p3, 0x1f

    invoke-virtual {p1, p2, p3}, Lb0/g0/j/c$a;->g(II)I

    move-result p2

    iput p2, p1, Lb0/g0/j/c$a;->h:I

    if-ltz p2, :cond_9

    iget p3, p1, Lb0/g0/j/c$a;->g:I

    if-gt p2, p3, :cond_9

    iget p3, p1, Lb0/g0/j/c$a;->f:I

    if-ge p2, p3, :cond_0

    if-nez p2, :cond_8

    invoke-virtual {p1}, Lb0/g0/j/c$a;->a()V

    goto/16 :goto_0

    :cond_8
    sub-int/2addr p3, p2

    invoke-virtual {p1, p3}, Lb0/g0/j/c$a;->c(I)I

    goto/16 :goto_0

    :cond_9
    new-instance p2, Ljava/io/IOException;

    const-string p3, "Invalid dynamic table size update "

    invoke-static {p3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    iget p1, p1, Lb0/g0/j/c$a;->h:I

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_a
    const/16 p3, 0x10

    if-eq p2, p3, :cond_c

    if-nez p2, :cond_b

    goto :goto_2

    :cond_b
    const/16 p3, 0xf

    invoke-virtual {p1, p2, p3}, Lb0/g0/j/c$a;->g(II)I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p1, p2}, Lb0/g0/j/c$a;->d(I)Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1}, Lb0/g0/j/c$a;->f()Lokio/ByteString;

    move-result-object p3

    iget-object p4, p1, Lb0/g0/j/c$a;->a:Ljava/util/List;

    new-instance v0, Lb0/g0/j/b;

    invoke-direct {v0, p2, p3}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_c
    :goto_2
    sget-object p2, Lb0/g0/j/c;->c:Lb0/g0/j/c;

    invoke-virtual {p1}, Lb0/g0/j/c$a;->f()Lokio/ByteString;

    move-result-object p3

    invoke-virtual {p2, p3}, Lb0/g0/j/c;->a(Lokio/ByteString;)Lokio/ByteString;

    invoke-virtual {p1}, Lb0/g0/j/c$a;->f()Lokio/ByteString;

    move-result-object p2

    iget-object p4, p1, Lb0/g0/j/c$a;->a:Ljava/util/List;

    new-instance v0, Lb0/g0/j/b;

    invoke-direct {v0, p3, p2}, Lb0/g0/j/b;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_d
    new-instance p1, Ljava/io/IOException;

    const-string p2, "index == 0"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    iget-object p1, p0, Lb0/g0/j/m;->e:Lb0/g0/j/c$a;

    iget-object p2, p1, Lb0/g0/j/c$a;->a:Ljava/util/List;

    invoke-static {p2}, Lx/h/f;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    iget-object p1, p1, Lb0/g0/j/c$a;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-object p2
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/x;->close()V

    return-void
.end method

.method public final d(Lb0/g0/j/m$b;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v0}, Lc0/g;->readInt()I

    move-result v0

    const-wide v1, 0x80000000L

    long-to-int v2, v1

    and-int v1, v0, v2

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const v3, 0x7fffffff

    and-int/2addr v0, v3

    iget-object v3, p0, Lb0/g0/j/m;->f:Lc0/g;

    invoke-interface {v3}, Lc0/g;->readByte()B

    move-result v3

    sget-object v4, Lb0/g0/c;->a:[B

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v3, v2

    invoke-interface {p1, p2, v0, v3, v1}, Lb0/g0/j/m$b;->g(IIIZ)V

    return-void
.end method
