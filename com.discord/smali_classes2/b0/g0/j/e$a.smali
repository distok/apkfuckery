.class public final Lb0/g0/j/e$a;
.super Lb0/g0/f/a;
.source "TaskQueue.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb0/g0/j/e;-><init>(Lb0/g0/j/e$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic e:Lb0/g0/j/e;

.field public final synthetic f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lb0/g0/j/e;J)V
    .locals 0

    iput-object p3, p0, Lb0/g0/j/e$a;->e:Lb0/g0/j/e;

    iput-wide p4, p0, Lb0/g0/j/e$a;->f:J

    const/4 p1, 0x1

    invoke-direct {p0, p2, p1}, Lb0/g0/f/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 9

    iget-object v0, p0, Lb0/g0/j/e$a;->e:Lb0/g0/j/e;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lb0/g0/j/e$a;->e:Lb0/g0/j/e;

    iget-wide v2, v1, Lb0/g0/j/e;->q:J

    iget-wide v4, v1, Lb0/g0/j/e;->p:J

    const/4 v6, 0x0

    const/4 v7, 0x1

    cmp-long v8, v2, v4

    if-gez v8, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x1

    add-long/2addr v4, v2

    iput-wide v4, v1, Lb0/g0/j/e;->p:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :goto_0
    monitor-exit v0

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    sget-object v2, Lb0/g0/j/a;->e:Lb0/g0/j/a;

    invoke-virtual {v1, v2, v2, v0}, Lb0/g0/j/e;->a(Lb0/g0/j/a;Lb0/g0/j/a;Ljava/io/IOException;)V

    const-wide/16 v0, -0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v6, v7, v6}, Lb0/g0/j/e;->i(ZII)V

    iget-wide v0, p0, Lb0/g0/j/e$a;->f:J

    :goto_1
    return-wide v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
