.class public final Lb0/g0/e/a;
.super Ljava/lang/Object;
.source "CacheInterceptor.kt"

# interfaces
.implements Lokhttp3/Interceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/e/a$a;
    }
.end annotation


# static fields
.field public static final b:Lb0/g0/e/a$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lb0/g0/e/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lb0/g0/e/a$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lb0/g0/e/a;->b:Lb0/g0/e/a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p1

    sget-object v1, Lb0/g0/e/a;->b:Lb0/g0/e/a$a;

    const-string v2, "chain"

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lb0/g0/h/g;

    iget-object v2, v0, Lb0/g0/h/g;->b:Lb0/g0/g/e;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iget-object v3, v0, Lb0/g0/h/g;->f:Lb0/a0;

    const-string v4, "request"

    invoke-static {v3, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v4, Lb0/g0/e/b;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Lb0/g0/e/b;-><init>(Lb0/a0;Lokhttp3/Response;)V

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lb0/a0;->a()Lb0/d;

    move-result-object v3

    iget-boolean v3, v3, Lb0/d;->j:Z

    if-eqz v3, :cond_0

    new-instance v4, Lb0/g0/e/b;

    invoke-direct {v4, v5, v5}, Lb0/g0/e/b;-><init>(Lb0/a0;Lokhttp3/Response;)V

    :cond_0
    iget-object v3, v4, Lb0/g0/e/b;->a:Lb0/a0;

    iget-object v4, v4, Lb0/g0/e/b;->b:Lokhttp3/Response;

    instance-of v6, v2, Lb0/g0/g/e;

    if-nez v6, :cond_1

    move-object v6, v5

    goto :goto_0

    :cond_1
    move-object v6, v2

    :goto_0
    const-string v6, "response"

    const-string v7, "call"

    if-nez v3, :cond_2

    if-nez v4, :cond_2

    new-instance v1, Lokhttp3/Response$a;

    invoke-direct {v1}, Lokhttp3/Response$a;-><init>()V

    iget-object v0, v0, Lb0/g0/h/g;->f:Lb0/a0;

    invoke-virtual {v1, v0}, Lokhttp3/Response$a;->g(Lb0/a0;)Lokhttp3/Response$a;

    sget-object v0, Lb0/z;->e:Lb0/z;

    invoke-virtual {v1, v0}, Lokhttp3/Response$a;->f(Lb0/z;)Lokhttp3/Response$a;

    const/16 v0, 0x1f8

    iput v0, v1, Lokhttp3/Response$a;->c:I

    const-string v0, "Unsatisfiable Request (only-if-cached)"

    invoke-virtual {v1, v0}, Lokhttp3/Response$a;->e(Ljava/lang/String;)Lokhttp3/Response$a;

    sget-object v0, Lb0/g0/c;->c:Lokhttp3/ResponseBody;

    iput-object v0, v1, Lokhttp3/Response$a;->g:Lokhttp3/ResponseBody;

    const-wide/16 v3, -0x1

    iput-wide v3, v1, Lokhttp3/Response$a;->k:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v1, Lokhttp3/Response$a;->l:J

    invoke-virtual {v1}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v0

    invoke-static {v2, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    if-nez v3, :cond_4

    if-eqz v4, :cond_3

    new-instance v0, Lokhttp3/Response$a;

    invoke-direct {v0, v4}, Lokhttp3/Response$a;-><init>(Lokhttp3/Response;)V

    invoke-static {v1, v4}, Lb0/g0/e/a$a;->a(Lb0/g0/e/a$a;Lokhttp3/Response;)Lokhttp3/Response;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$a;->b(Lokhttp3/Response;)Lokhttp3/Response$a;

    invoke-virtual {v0}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v0

    invoke-static {v2, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v6}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v5

    :cond_4
    if-eqz v4, :cond_5

    invoke-static {v2, v7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "cachedResponse"

    invoke-static {v4, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_5
    :try_start_0
    invoke-virtual {v0, v3}, Lb0/g0/h/g;->a(Lb0/a0;)Lokhttp3/Response;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "networkResponse"

    if-eqz v4, :cond_10

    iget v3, v0, Lokhttp3/Response;->g:I

    const/16 v6, 0x130

    if-ne v3, v6, :cond_f

    new-instance v3, Lokhttp3/Response$a;

    invoke-direct {v3, v4}, Lokhttp3/Response$a;-><init>(Lokhttp3/Response;)V

    iget-object v6, v4, Lokhttp3/Response;->i:Lokhttp3/Headers;

    iget-object v7, v0, Lokhttp3/Response;->i:Lokhttp3/Headers;

    new-instance v8, Ljava/util/ArrayList;

    const/16 v9, 0x14

    invoke-direct {v8, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v6}, Lokhttp3/Headers;->size()I

    move-result v9

    const/4 v11, 0x0

    :goto_1
    const-string v12, "value"

    const-string v13, "name"

    if-ge v11, v9, :cond_a

    invoke-virtual {v6, v11}, Lokhttp3/Headers;->d(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v11}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v15

    const-string v5, "Warning"

    const/4 v10, 0x1

    invoke-static {v5, v14, v10}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x2

    const-string v10, "1"

    move-object/from16 v16, v6

    const/4 v6, 0x0

    invoke-static {v15, v10, v6, v5}, Lx/s/m;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v5

    if-eqz v5, :cond_7

    goto :goto_2

    :cond_6
    move-object/from16 v16, v6

    :cond_7
    invoke-virtual {v1, v14}, Lb0/g0/e/a$a;->b(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-virtual {v1, v14}, Lb0/g0/e/a$a;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v7, v14}, Lokhttp3/Headers;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_9

    :cond_8
    invoke-static {v14, v13}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v15, v12}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v15}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    :goto_2
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v6, v16

    const/4 v5, 0x0

    goto :goto_1

    :cond_a
    invoke-virtual {v7}, Lokhttp3/Headers;->size()I

    move-result v5

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v5, :cond_c

    invoke-virtual {v7, v6}, Lokhttp3/Headers;->d(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lb0/g0/e/a$a;->b(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_b

    invoke-virtual {v1, v9}, Lb0/g0/e/a$a;->c(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    invoke-virtual {v7, v6}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v13}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v10, v12}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v10}, Lx/s/r;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_b
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_c
    const/4 v6, 0x0

    new-array v5, v6, [Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_e

    check-cast v5, [Ljava/lang/String;

    new-instance v6, Lokhttp3/Headers;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v7}, Lokhttp3/Headers;-><init>([Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v3, v6}, Lokhttp3/Response$a;->d(Lokhttp3/Headers;)Lokhttp3/Response$a;

    iget-wide v5, v0, Lokhttp3/Response;->n:J

    iput-wide v5, v3, Lokhttp3/Response$a;->k:J

    iget-wide v5, v0, Lokhttp3/Response;->o:J

    iput-wide v5, v3, Lokhttp3/Response$a;->l:J

    invoke-static {v1, v4}, Lb0/g0/e/a$a;->a(Lb0/g0/e/a$a;Lokhttp3/Response;)Lokhttp3/Response;

    move-result-object v4

    invoke-virtual {v3, v4}, Lokhttp3/Response$a;->b(Lokhttp3/Response;)Lokhttp3/Response$a;

    invoke-static {v1, v0}, Lb0/g0/e/a$a;->a(Lb0/g0/e/a$a;Lokhttp3/Response;)Lokhttp3/Response;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lokhttp3/Response$a;->c(Ljava/lang/String;Lokhttp3/Response;)V

    iput-object v1, v3, Lokhttp3/Response$a;->h:Lokhttp3/Response;

    invoke-virtual {v3}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    iget-object v0, v0, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0

    :cond_d
    const/4 v0, 0x0

    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v0

    :cond_e
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v3, v4, Lokhttp3/Response;->j:Lokhttp3/ResponseBody;

    if-eqz v3, :cond_10

    sget-object v5, Lb0/g0/c;->a:[B

    const-string v5, "$this$closeQuietly"

    invoke-static {v3, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_1
    invoke-interface {v3}, Ljava/io/Closeable;->close()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v1, v0

    throw v1

    :catch_1
    :cond_10
    :goto_4
    new-instance v3, Lokhttp3/Response$a;

    invoke-direct {v3, v0}, Lokhttp3/Response$a;-><init>(Lokhttp3/Response;)V

    invoke-static {v1, v4}, Lb0/g0/e/a$a;->a(Lb0/g0/e/a$a;Lokhttp3/Response;)Lokhttp3/Response;

    move-result-object v4

    invoke-virtual {v3, v4}, Lokhttp3/Response$a;->b(Lokhttp3/Response;)Lokhttp3/Response$a;

    invoke-static {v1, v0}, Lb0/g0/e/a$a;->a(Lb0/g0/e/a$a;Lokhttp3/Response;)Lokhttp3/Response;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lokhttp3/Response$a;->c(Ljava/lang/String;Lokhttp3/Response;)V

    iput-object v0, v3, Lokhttp3/Response$a;->h:Lokhttp3/Response;

    invoke-virtual {v3}, Lokhttp3/Response$a;->a()Lokhttp3/Response;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    move-object v1, v0

    throw v1
.end method
