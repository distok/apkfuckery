.class public abstract Lb0/g0/i/b$a;
.super Ljava/lang/Object;
.source "Http1ExchangeCodec.kt"

# interfaces
.implements Lc0/x;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/i/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "a"
.end annotation


# instance fields
.field public final d:Lc0/k;

.field public e:Z

.field public final synthetic f:Lb0/g0/i/b;


# direct methods
.method public constructor <init>(Lb0/g0/i/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/i/b$a;->f:Lb0/g0/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lc0/k;

    iget-object p1, p1, Lb0/g0/i/b;->f:Lc0/g;

    invoke-interface {p1}, Lc0/x;->timeout()Lc0/y;

    move-result-object p1

    invoke-direct {v0, p1}, Lc0/k;-><init>(Lc0/y;)V

    iput-object v0, p0, Lb0/g0/i/b$a;->d:Lc0/k;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lb0/g0/i/b$a;->f:Lb0/g0/i/b;

    iget v1, v0, Lb0/g0/i/b;->a:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    const/4 v3, 0x5

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lb0/g0/i/b$a;->d:Lc0/k;

    invoke-static {v0, v1}, Lb0/g0/i/b;->i(Lb0/g0/i/b;Lc0/k;)V

    iget-object v0, p0, Lb0/g0/i/b$a;->f:Lb0/g0/i/b;

    iput v2, v0, Lb0/g0/i/b;->a:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "state: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb0/g0/i/b$a;->f:Lb0/g0/i/b;

    iget v2, v2, Lb0/g0/i/b;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lb0/g0/i/b$a;->d:Lc0/k;

    return-object v0
.end method

.method public v0(Lc0/e;J)J
    .locals 1

    const-string v0, "sink"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lb0/g0/i/b$a;->f:Lb0/g0/i/b;

    iget-object v0, v0, Lb0/g0/i/b;->f:Lc0/g;

    invoke-interface {v0, p1, p2, p3}, Lc0/x;->v0(Lc0/e;J)J

    move-result-wide p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    move-exception p1

    iget-object p2, p0, Lb0/g0/i/b$a;->f:Lb0/g0/i/b;

    iget-object p2, p2, Lb0/g0/i/b;->e:Lb0/g0/g/j;

    invoke-virtual {p2}, Lb0/g0/g/j;->l()V

    invoke-virtual {p0}, Lb0/g0/i/b$a;->a()V

    throw p1
.end method
