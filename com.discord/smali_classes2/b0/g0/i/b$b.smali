.class public final Lb0/g0/i/b$b;
.super Ljava/lang/Object;
.source "Http1ExchangeCodec.kt"

# interfaces
.implements Lc0/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/i/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field public final d:Lc0/k;

.field public e:Z

.field public final synthetic f:Lb0/g0/i/b;


# direct methods
.method public constructor <init>(Lb0/g0/i/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lc0/k;

    iget-object p1, p1, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    invoke-interface {p1}, Lc0/v;->timeout()Lc0/y;

    move-result-object p1

    invoke-direct {v0, p1}, Lc0/k;-><init>(Lc0/y;)V

    iput-object v0, p0, Lb0/g0/i/b$b;->d:Lc0/k;

    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/i/b$b;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lb0/g0/i/b$b;->e:Z

    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object v0, v0, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    const-string v1, "0\r\n\r\n"

    invoke-interface {v0, v1}, Lokio/BufferedSink;->W(Ljava/lang/String;)Lokio/BufferedSink;

    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object v1, p0, Lb0/g0/i/b$b;->d:Lc0/k;

    invoke-static {v0, v1}, Lb0/g0/i/b;->i(Lb0/g0/i/b;Lc0/k;)V

    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    const/4 v1, 0x3

    iput v1, v0, Lb0/g0/i/b;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized flush()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/i/b$b;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object v0, v0, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public timeout()Lc0/y;
    .locals 1

    iget-object v0, p0, Lb0/g0/i/b$b;->d:Lc0/k;

    return-object v0
.end method

.method public write(Lc0/e;J)V
    .locals 3

    const-string v0, "source"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lb0/g0/i/b$b;->e:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object v0, v0, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    invoke-interface {v0, p2, p3}, Lokio/BufferedSink;->b0(J)Lokio/BufferedSink;

    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object v0, v0, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lokio/BufferedSink;->W(Ljava/lang/String;)Lokio/BufferedSink;

    iget-object v0, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object v0, v0, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    invoke-interface {v0, p1, p2, p3}, Lc0/v;->write(Lc0/e;J)V

    iget-object p1, p0, Lb0/g0/i/b$b;->f:Lb0/g0/i/b;

    iget-object p1, p1, Lb0/g0/i/b;->g:Lokio/BufferedSink;

    invoke-interface {p1, v1}, Lokio/BufferedSink;->W(Ljava/lang/String;)Lokio/BufferedSink;

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
