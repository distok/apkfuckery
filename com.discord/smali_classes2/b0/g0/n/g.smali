.class public final Lb0/g0/n/g;
.super Ljava/lang/Object;
.source "WebSocketProtocol.kt"


# direct methods
.method public static final a(Lc0/e$a;[B)V
    .locals 8

    const-string v0, "cursor"

    invoke-static {p0, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, p1

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lc0/e$a;->h:[B

    iget v3, p0, Lc0/e$a;->i:I

    iget v4, p0, Lc0/e$a;->j:I

    const/4 v5, 0x1

    if-eqz v2, :cond_1

    :goto_0
    if-ge v3, v4, :cond_1

    rem-int/2addr v1, v0

    aget-byte v6, v2, v3

    aget-byte v7, p1, v1

    xor-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v5

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lc0/e$a;->g:J

    iget-object v4, p0, Lc0/e$a;->d:Lc0/e;

    if-eqz v4, :cond_5

    iget-wide v6, v4, Lc0/e;->e:J

    cmp-long v4, v2, v6

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_4

    const-wide/16 v4, -0x1

    cmp-long v6, v2, v4

    if-nez v6, :cond_3

    const-wide/16 v2, 0x0

    goto :goto_2

    :cond_3
    iget v4, p0, Lc0/e$a;->j:I

    iget v5, p0, Lc0/e$a;->i:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    add-long/2addr v2, v4

    :goto_2
    invoke-virtual {p0, v2, v3}, Lc0/e$a;->b(J)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    return-void

    :cond_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "no more bytes"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_5
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 p0, 0x0

    throw p0
.end method
