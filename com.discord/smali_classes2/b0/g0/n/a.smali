.class public final Lb0/g0/n/a;
.super Ljava/lang/Object;
.source "MessageDeflater.kt"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final d:Lc0/e;

.field public final e:Ljava/util/zip/Deflater;

.field public final f:Lc0/h;

.field public final g:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lb0/g0/n/a;->g:Z

    new-instance p1, Lc0/e;

    invoke-direct {p1}, Lc0/e;-><init>()V

    iput-object p1, p0, Lb0/g0/n/a;->d:Lc0/e;

    new-instance v0, Ljava/util/zip/Deflater;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/util/zip/Deflater;-><init>(IZ)V

    iput-object v0, p0, Lb0/g0/n/a;->e:Ljava/util/zip/Deflater;

    new-instance v1, Lc0/h;

    invoke-direct {v1, p1, v0}, Lc0/h;-><init>(Lc0/v;Ljava/util/zip/Deflater;)V

    iput-object v1, p0, Lb0/g0/n/a;->f:Lc0/h;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/n/a;->f:Lc0/h;

    invoke-virtual {v0}, Lc0/h;->close()V

    return-void
.end method
