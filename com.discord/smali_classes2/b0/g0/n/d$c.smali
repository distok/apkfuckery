.class public abstract Lb0/g0/n/d$c;
.super Ljava/lang/Object;
.source "RealWebSocket.kt"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/n/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation


# instance fields
.field public final d:Z

.field public final e:Lc0/g;

.field public final f:Lokio/BufferedSink;


# direct methods
.method public constructor <init>(ZLc0/g;Lokio/BufferedSink;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sink"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lb0/g0/n/d$c;->d:Z

    iput-object p2, p0, Lb0/g0/n/d$c;->e:Lc0/g;

    iput-object p3, p0, Lb0/g0/n/d$c;->f:Lokio/BufferedSink;

    return-void
.end method
