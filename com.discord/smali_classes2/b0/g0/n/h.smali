.class public final Lb0/g0/n/h;
.super Ljava/lang/Object;
.source "WebSocketReader.kt"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/n/h$a;
    }
.end annotation


# instance fields
.field public d:Z

.field public e:I

.field public f:J

.field public g:Z

.field public h:Z

.field public i:Z

.field public final j:Lc0/e;

.field public final k:Lc0/e;

.field public l:Lb0/g0/n/c;

.field public final m:[B

.field public final n:Lc0/e$a;

.field public final o:Z

.field public final p:Lc0/g;

.field public final q:Lb0/g0/n/h$a;

.field public final r:Z

.field public final s:Z


# direct methods
.method public constructor <init>(ZLc0/g;Lb0/g0/n/h$a;ZZ)V
    .locals 1

    const-string v0, "source"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "frameCallback"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lb0/g0/n/h;->o:Z

    iput-object p2, p0, Lb0/g0/n/h;->p:Lc0/g;

    iput-object p3, p0, Lb0/g0/n/h;->q:Lb0/g0/n/h$a;

    iput-boolean p4, p0, Lb0/g0/n/h;->r:Z

    iput-boolean p5, p0, Lb0/g0/n/h;->s:Z

    new-instance p2, Lc0/e;

    invoke-direct {p2}, Lc0/e;-><init>()V

    iput-object p2, p0, Lb0/g0/n/h;->j:Lc0/e;

    new-instance p2, Lc0/e;

    invoke-direct {p2}, Lc0/e;-><init>()V

    iput-object p2, p0, Lb0/g0/n/h;->k:Lc0/e;

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    move-object p3, p2

    goto :goto_0

    :cond_0
    const/4 p3, 0x4

    new-array p3, p3, [B

    :goto_0
    iput-object p3, p0, Lb0/g0/n/h;->m:[B

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p2, Lc0/e$a;

    invoke-direct {p2}, Lc0/e$a;-><init>()V

    :goto_1
    iput-object p2, p0, Lb0/g0/n/h;->n:Lc0/e$a;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Lb0/g0/n/h;->f:J

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    cmp-long v5, v0, v3

    if-lez v5, :cond_2

    iget-object v5, p0, Lb0/g0/n/h;->p:Lc0/g;

    iget-object v6, p0, Lb0/g0/n/h;->j:Lc0/e;

    invoke-interface {v5, v6, v0, v1}, Lc0/g;->M(Lc0/e;J)V

    iget-boolean v0, p0, Lb0/g0/n/h;->o:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lb0/g0/n/h;->j:Lc0/e;

    iget-object v1, p0, Lb0/g0/n/h;->n:Lc0/e$a;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lc0/e;->n(Lc0/e$a;)Lc0/e$a;

    iget-object v0, p0, Lb0/g0/n/h;->n:Lc0/e$a;

    invoke-virtual {v0, v3, v4}, Lc0/e$a;->b(J)I

    iget-object v0, p0, Lb0/g0/n/h;->n:Lc0/e$a;

    iget-object v1, p0, Lb0/g0/n/h;->m:[B

    if-eqz v1, :cond_0

    invoke-static {v0, v1}, Lb0/g0/n/g;->a(Lc0/e$a;[B)V

    iget-object v0, p0, Lb0/g0/n/h;->n:Lc0/e$a;

    invoke-virtual {v0}, Lc0/e$a;->close()V

    goto :goto_0

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_2
    :goto_0
    iget v0, p0, Lb0/g0/n/h;->e:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Unknown control opcode: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lb0/g0/n/h;->e:I

    invoke-static {v2}, Lb0/g0/c;->x(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lb0/g0/n/h;->q:Lb0/g0/n/h$a;

    iget-object v1, p0, Lb0/g0/n/h;->j:Lc0/e;

    invoke-virtual {v1}, Lc0/e;->o()Lokio/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lb0/g0/n/h$a;->f(Lokio/ByteString;)V

    goto :goto_4

    :pswitch_1
    iget-object v0, p0, Lb0/g0/n/h;->q:Lb0/g0/n/h$a;

    iget-object v1, p0, Lb0/g0/n/h;->j:Lc0/e;

    invoke-virtual {v1}, Lc0/e;->o()Lokio/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lb0/g0/n/h$a;->d(Lokio/ByteString;)V

    goto :goto_4

    :pswitch_2
    const/16 v0, 0x3ed

    iget-object v1, p0, Lb0/g0/n/h;->j:Lc0/e;

    iget-wide v5, v1, Lc0/e;->e:J

    const-wide/16 v7, 0x1

    cmp-long v9, v5, v7

    if-eqz v9, :cond_b

    cmp-long v7, v5, v3

    if-eqz v7, :cond_a

    invoke-virtual {v1}, Lc0/e;->readShort()S

    move-result v0

    iget-object v1, p0, Lb0/g0/n/h;->j:Lc0/e;

    invoke-virtual {v1}, Lc0/e;->B()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x3e8

    if-lt v0, v3, :cond_7

    const/16 v3, 0x1388

    if-lt v0, v3, :cond_3

    goto :goto_1

    :cond_3
    const/16 v3, 0x3ee

    const/16 v4, 0x3ec

    if-gt v4, v0, :cond_4

    if-ge v3, v0, :cond_6

    :cond_4
    const/16 v3, 0xbb7

    const/16 v4, 0x3f7

    if-le v4, v0, :cond_5

    goto :goto_2

    :cond_5
    if-lt v3, v0, :cond_8

    :cond_6
    const-string v2, "Code "

    const-string v3, " is reserved and may not be used."

    invoke-static {v2, v0, v3}, Lf/e/c/a/a;->k(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_7
    :goto_1
    const-string v2, "Code must be in range [1000,5000): "

    invoke-static {v2, v0}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    :cond_8
    :goto_2
    if-nez v2, :cond_9

    goto :goto_3

    :cond_9
    new-instance v0, Ljava/net/ProtocolException;

    invoke-direct {v0, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    const-string v1, ""

    :goto_3
    iget-object v2, p0, Lb0/g0/n/h;->q:Lb0/g0/n/h$a;

    invoke-interface {v2, v0, v1}, Lb0/g0/n/h$a;->g(ILjava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb0/g0/n/h;->d:Z

    :goto_4
    return-void

    :cond_b
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Malformed close payload length of 1."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/ProtocolException;
        }
    .end annotation

    iget-boolean v0, p0, Lb0/g0/n/h;->d:Z

    if-nez v0, :cond_17

    iget-object v0, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v0}, Lc0/x;->timeout()Lc0/y;

    move-result-object v0

    invoke-virtual {v0}, Lc0/y;->h()J

    move-result-wide v0

    iget-object v2, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v2}, Lc0/x;->timeout()Lc0/y;

    move-result-object v2

    invoke-virtual {v2}, Lc0/y;->b()Lc0/y;

    :try_start_0
    iget-object v2, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v2}, Lc0/g;->readByte()B

    move-result v2

    sget-object v3, Lb0/g0/c;->a:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit16 v2, v2, 0xff

    iget-object v3, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v3}, Lc0/x;->timeout()Lc0/y;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1, v4}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    and-int/lit8 v0, v2, 0xf

    iput v0, p0, Lb0/g0/n/h;->e:I

    and-int/lit16 v1, v2, 0x80

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lb0/g0/n/h;->g:Z

    and-int/lit8 v5, v2, 0x8

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    iput-boolean v5, p0, Lb0/g0/n/h;->h:Z

    if-eqz v5, :cond_3

    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Control frames must be final."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_2
    and-int/lit8 v1, v2, 0x40

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    const-string v5, "Unexpected rsv1 flag"

    if-eq v0, v4, :cond_6

    const/4 v6, 0x2

    if-eq v0, v6, :cond_6

    if-nez v1, :cond_5

    goto :goto_4

    :cond_5
    new-instance v0, Ljava/net/ProtocolException;

    invoke-direct {v0, v5}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-eqz v1, :cond_8

    iget-boolean v0, p0, Lb0/g0/n/h;->r:Z

    if-eqz v0, :cond_7

    iput-boolean v4, p0, Lb0/g0/n/h;->i:Z

    goto :goto_4

    :cond_7
    new-instance v0, Ljava/net/ProtocolException;

    invoke-direct {v0, v5}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iput-boolean v3, p0, Lb0/g0/n/h;->i:Z

    :goto_4
    and-int/lit8 v0, v2, 0x20

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_5

    :cond_9
    const/4 v0, 0x0

    :goto_5
    if-nez v0, :cond_16

    and-int/lit8 v0, v2, 0x10

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_6

    :cond_a
    const/4 v0, 0x0

    :goto_6
    if-nez v0, :cond_15

    iget-object v0, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v0}, Lc0/g;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_b

    const/4 v3, 0x1

    :cond_b
    iget-boolean v1, p0, Lb0/g0/n/h;->o:Z

    if-ne v3, v1, :cond_d

    new-instance v0, Ljava/net/ProtocolException;

    iget-boolean v1, p0, Lb0/g0/n/h;->o:Z

    if-eqz v1, :cond_c

    const-string v1, "Server-sent frames must not be masked."

    goto :goto_7

    :cond_c
    const-string v1, "Client-sent frames must be masked."

    :goto_7
    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    const/16 v1, 0x7f

    and-int/2addr v0, v1

    int-to-long v4, v0

    iput-wide v4, p0, Lb0/g0/n/h;->f:J

    const/16 v0, 0x7e

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-nez v0, :cond_e

    iget-object v0, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v0}, Lc0/g;->readShort()S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lb0/g0/n/h;->f:J

    goto :goto_8

    :cond_e
    int-to-long v0, v1

    cmp-long v2, v4, v0

    if-nez v2, :cond_10

    iget-object v0, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v0}, Lc0/g;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lb0/g0/n/h;->f:J

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-ltz v2, :cond_f

    goto :goto_8

    :cond_f
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Frame length 0x"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lb0/g0/n/h;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "java.lang.Long.toHexString(this)"

    invoke-static {v2, v3}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " > 0x7FFFFFFFFFFFFFFF"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    :goto_8
    iget-boolean v0, p0, Lb0/g0/n/h;->h:Z

    if-eqz v0, :cond_12

    iget-wide v0, p0, Lb0/g0/n/h;->f:J

    const-wide/16 v4, 0x7d

    cmp-long v2, v0, v4

    if-gtz v2, :cond_11

    goto :goto_9

    :cond_11
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Control frame must be less than 125B."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    :goto_9
    if-eqz v3, :cond_14

    iget-object v0, p0, Lb0/g0/n/h;->p:Lc0/g;

    iget-object v1, p0, Lb0/g0/n/h;->m:[B

    if-eqz v1, :cond_13

    invoke-interface {v0, v1}, Lc0/g;->readFully([B)V

    goto :goto_a

    :cond_13
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0

    :cond_14
    :goto_a
    return-void

    :cond_15
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Unexpected rsv3 flag"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_16
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Unexpected rsv2 flag"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lb0/g0/n/h;->p:Lc0/g;

    invoke-interface {v3}, Lc0/x;->timeout()Lc0/y;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1, v4}, Lc0/y;->g(JLjava/util/concurrent/TimeUnit;)Lc0/y;

    throw v2

    :cond_17
    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/n/h;->l:Lb0/g0/n/c;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lb0/g0/n/c;->f:Lc0/m;

    invoke-virtual {v0}, Lc0/m;->close()V

    :cond_0
    return-void
.end method
