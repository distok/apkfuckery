.class public final Lb0/g0/n/c;
.super Ljava/lang/Object;
.source "MessageInflater.kt"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final d:Lc0/e;

.field public final e:Ljava/util/zip/Inflater;

.field public final f:Lc0/m;

.field public final g:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lb0/g0/n/c;->g:Z

    new-instance p1, Lc0/e;

    invoke-direct {p1}, Lc0/e;-><init>()V

    iput-object p1, p0, Lb0/g0/n/c;->d:Lc0/e;

    new-instance v0, Ljava/util/zip/Inflater;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/zip/Inflater;-><init>(Z)V

    iput-object v0, p0, Lb0/g0/n/c;->e:Ljava/util/zip/Inflater;

    new-instance v1, Lc0/m;

    invoke-direct {v1, p1, v0}, Lc0/m;-><init>(Lc0/x;Ljava/util/zip/Inflater;)V

    iput-object v1, p0, Lb0/g0/n/c;->f:Lc0/m;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lb0/g0/n/c;->f:Lc0/m;

    invoke-virtual {v0}, Lc0/m;->close()V

    return-void
.end method
