.class public final Lb0/g0/n/i;
.super Ljava/lang/Object;
.source "WebSocketWriter.kt"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final d:Lc0/e;

.field public final e:Lc0/e;

.field public f:Z

.field public g:Lb0/g0/n/a;

.field public final h:[B

.field public final i:Lc0/e$a;

.field public final j:Z

.field public final k:Lokio/BufferedSink;

.field public final l:Ljava/util/Random;

.field public final m:Z

.field public final n:Z

.field public final o:J


# direct methods
.method public constructor <init>(ZLokio/BufferedSink;Ljava/util/Random;ZZJ)V
    .locals 1

    const-string v0, "sink"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "random"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lb0/g0/n/i;->j:Z

    iput-object p2, p0, Lb0/g0/n/i;->k:Lokio/BufferedSink;

    iput-object p3, p0, Lb0/g0/n/i;->l:Ljava/util/Random;

    iput-boolean p4, p0, Lb0/g0/n/i;->m:Z

    iput-boolean p5, p0, Lb0/g0/n/i;->n:Z

    iput-wide p6, p0, Lb0/g0/n/i;->o:J

    new-instance p3, Lc0/e;

    invoke-direct {p3}, Lc0/e;-><init>()V

    iput-object p3, p0, Lb0/g0/n/i;->d:Lc0/e;

    invoke-interface {p2}, Lokio/BufferedSink;->h()Lc0/e;

    move-result-object p2

    iput-object p2, p0, Lb0/g0/n/i;->e:Lc0/e;

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    const/4 p3, 0x4

    new-array p3, p3, [B

    goto :goto_0

    :cond_0
    move-object p3, p2

    :goto_0
    iput-object p3, p0, Lb0/g0/n/i;->h:[B

    if-eqz p1, :cond_1

    new-instance p2, Lc0/e$a;

    invoke-direct {p2}, Lc0/e$a;-><init>()V

    :cond_1
    iput-object p2, p0, Lb0/g0/n/i;->i:Lc0/e$a;

    return-void
.end method


# virtual methods
.method public final a(ILokio/ByteString;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lokio/ByteString;->f:Lokio/ByteString;

    const/4 v1, 0x1

    if-nez p1, :cond_0

    if-eqz p2, :cond_b

    :cond_0
    if-eqz p1, :cond_9

    const/16 v0, 0x3e8

    const/4 v2, 0x0

    if-lt p1, v0, :cond_6

    const/16 v0, 0x1388

    if-lt p1, v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 v0, 0x3ee

    const/16 v3, 0x3ec

    if-gt v3, p1, :cond_2

    if-ge v0, p1, :cond_4

    :cond_2
    const/16 v0, 0xbb7

    const/16 v3, 0x3f7

    if-le v3, p1, :cond_3

    goto :goto_0

    :cond_3
    if-lt v0, p1, :cond_5

    :cond_4
    const-string v0, "Code "

    const-string v3, " is reserved and may not be used."

    invoke-static {v0, p1, v3}, Lf/e/c/a/a;->k(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    :goto_0
    move-object v0, v2

    goto :goto_2

    :cond_6
    :goto_1
    const-string v0, "Code must be in range [1000,5000): "

    invoke-static {v0, p1}, Lf/e/c/a/a;->j(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_7

    const/4 v3, 0x1

    goto :goto_3

    :cond_7
    const/4 v3, 0x0

    :goto_3
    if-nez v3, :cond_9

    if-nez v0, :cond_8

    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    new-instance v0, Lc0/e;

    invoke-direct {v0}, Lc0/e;-><init>()V

    invoke-virtual {v0, p1}, Lc0/e;->U(I)Lc0/e;

    if-eqz p2, :cond_a

    invoke-virtual {v0, p2}, Lc0/e;->I(Lokio/ByteString;)Lc0/e;

    :cond_a
    invoke-virtual {v0}, Lc0/e;->o()Lokio/ByteString;

    move-result-object v0

    :cond_b
    const/16 p1, 0x8

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lb0/g0/n/i;->b(ILokio/ByteString;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v1, p0, Lb0/g0/n/i;->f:Z

    return-void

    :catchall_0
    move-exception p1

    iput-boolean v1, p0, Lb0/g0/n/i;->f:Z

    throw p1
.end method

.method public final b(ILokio/ByteString;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lb0/g0/n/i;->f:Z

    if-nez v0, :cond_6

    invoke-virtual {p2}, Lokio/ByteString;->j()I

    move-result v0

    int-to-long v1, v0

    const-wide/16 v3, 0x7d

    cmp-long v5, v1, v3

    if-gtz v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_5

    or-int/lit16 p1, p1, 0x80

    iget-object v1, p0, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {v1, p1}, Lc0/e;->N(I)Lc0/e;

    iget-boolean p1, p0, Lb0/g0/n/i;->j:Z

    if-eqz p1, :cond_3

    or-int/lit16 p1, v0, 0x80

    iget-object v1, p0, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {v1, p1}, Lc0/e;->N(I)Lc0/e;

    iget-object p1, p0, Lb0/g0/n/i;->l:Ljava/util/Random;

    iget-object v1, p0, Lb0/g0/n/i;->h:[B

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/Random;->nextBytes([B)V

    iget-object p1, p0, Lb0/g0/n/i;->e:Lc0/e;

    iget-object v1, p0, Lb0/g0/n/i;->h:[B

    invoke-virtual {p1, v1}, Lc0/e;->J([B)Lc0/e;

    if-lez v0, :cond_4

    iget-object p1, p0, Lb0/g0/n/i;->e:Lc0/e;

    iget-wide v0, p1, Lc0/e;->e:J

    invoke-virtual {p1, p2}, Lc0/e;->I(Lokio/ByteString;)Lc0/e;

    iget-object p1, p0, Lb0/g0/n/i;->e:Lc0/e;

    iget-object p2, p0, Lb0/g0/n/i;->i:Lc0/e$a;

    if-eqz p2, :cond_1

    invoke-virtual {p1, p2}, Lc0/e;->n(Lc0/e$a;)Lc0/e$a;

    iget-object p1, p0, Lb0/g0/n/i;->i:Lc0/e$a;

    invoke-virtual {p1, v0, v1}, Lc0/e$a;->b(J)I

    iget-object p1, p0, Lb0/g0/n/i;->i:Lc0/e$a;

    iget-object p2, p0, Lb0/g0/n/i;->h:[B

    invoke-static {p1, p2}, Lb0/g0/n/g;->a(Lc0/e$a;[B)V

    iget-object p1, p0, Lb0/g0/n/i;->i:Lc0/e$a;

    invoke-virtual {p1}, Lc0/e$a;->close()V

    goto :goto_1

    :cond_1
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v2

    :cond_3
    iget-object p1, p0, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {p1, v0}, Lc0/e;->N(I)Lc0/e;

    iget-object p1, p0, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {p1, p2}, Lc0/e;->I(Lokio/ByteString;)Lc0/e;

    :cond_4
    :goto_1
    iget-object p1, p0, Lb0/g0/n/i;->k:Lokio/BufferedSink;

    invoke-interface {p1}, Lokio/BufferedSink;->flush()V

    return-void

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Payload size must be less than or equal to 125"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Ljava/io/IOException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final c(ILokio/ByteString;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    const-string v2, "data"

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v2, v1, Lb0/g0/n/i;->f:Z

    if-nez v2, :cond_10

    iget-object v2, v1, Lb0/g0/n/i;->d:Lc0/e;

    invoke-virtual {v2, v0}, Lc0/e;->I(Lokio/ByteString;)Lc0/e;

    move/from16 v2, p1

    or-int/lit16 v2, v2, 0x80

    iget-boolean v3, v1, Lb0/g0/n/i;->m:Z

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    if-eqz v3, :cond_9

    invoke-virtual/range {p2 .. p2}, Lokio/ByteString;->j()I

    move-result v0

    int-to-long v8, v0

    iget-wide v10, v1, Lb0/g0/n/i;->o:J

    cmp-long v0, v8, v10

    if-ltz v0, :cond_9

    iget-object v0, v1, Lb0/g0/n/i;->g:Lb0/g0/n/a;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lb0/g0/n/a;

    iget-boolean v3, v1, Lb0/g0/n/i;->n:Z

    invoke-direct {v0, v3}, Lb0/g0/n/a;-><init>(Z)V

    iput-object v0, v1, Lb0/g0/n/i;->g:Lb0/g0/n/a;

    :goto_0
    iget-object v3, v1, Lb0/g0/n/i;->d:Lc0/e;

    const-string v8, "buffer"

    invoke-static {v3, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v8, v0, Lb0/g0/n/a;->d:Lc0/e;

    iget-wide v8, v8, Lc0/e;->e:J

    cmp-long v11, v8, v5

    if-nez v11, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    :goto_1
    if-eqz v8, :cond_8

    iget-boolean v8, v0, Lb0/g0/n/a;->g:Z

    if-eqz v8, :cond_2

    iget-object v8, v0, Lb0/g0/n/a;->e:Ljava/util/zip/Deflater;

    invoke-virtual {v8}, Ljava/util/zip/Deflater;->reset()V

    :cond_2
    iget-object v8, v0, Lb0/g0/n/a;->f:Lc0/h;

    iget-wide v11, v3, Lc0/e;->e:J

    invoke-virtual {v8, v3, v11, v12}, Lc0/h;->write(Lc0/e;J)V

    iget-object v8, v0, Lb0/g0/n/a;->f:Lc0/h;

    invoke-virtual {v8}, Lc0/h;->flush()V

    iget-object v8, v0, Lb0/g0/n/a;->d:Lc0/e;

    sget-object v9, Lb0/g0/n/b;->a:Lokio/ByteString;

    iget-wide v11, v8, Lc0/e;->e:J

    invoke-virtual {v9}, Lokio/ByteString;->j()I

    move-result v13

    int-to-long v13, v13

    sub-long/2addr v11, v13

    const-string v13, "bytes"

    invoke-static {v9, v13}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Lokio/ByteString;->j()I

    move-result v14

    invoke-static {v9, v13}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    cmp-long v13, v11, v5

    if-ltz v13, :cond_6

    if-ltz v14, :cond_6

    iget-wide v5, v8, Lc0/e;->e:J

    sub-long/2addr v5, v11

    move-wide v15, v11

    int-to-long v10, v14

    cmp-long v12, v5, v10

    if-ltz v12, :cond_6

    invoke-virtual {v9}, Lokio/ByteString;->j()I

    move-result v5

    sub-int/2addr v5, v7

    if-ge v5, v14, :cond_3

    goto :goto_3

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-ge v5, v14, :cond_5

    int-to-long v10, v5

    add-long/2addr v10, v15

    invoke-virtual {v8, v10, v11}, Lc0/e;->g(J)B

    move-result v6

    add-int v10, v7, v5

    invoke-virtual {v9, v10}, Lokio/ByteString;->m(I)B

    move-result v10

    if-eq v6, v10, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    const/4 v10, 0x1

    goto :goto_4

    :cond_6
    :goto_3
    const/4 v10, 0x0

    :goto_4
    if-eqz v10, :cond_7

    iget-object v5, v0, Lb0/g0/n/a;->d:Lc0/e;

    iget-wide v6, v5, Lc0/e;->e:J

    const/4 v8, 0x4

    int-to-long v8, v8

    sub-long/2addr v6, v8

    new-instance v8, Lc0/e$a;

    invoke-direct {v8}, Lc0/e$a;-><init>()V

    invoke-virtual {v5, v8}, Lc0/e;->n(Lc0/e$a;)Lc0/e$a;

    :try_start_0
    invoke-virtual {v8, v6, v7}, Lc0/e$a;->a(J)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v8, v4}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object v2, v0

    :try_start_1
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v0

    move-object v3, v0

    invoke-static {v8, v2}, Lf/h/a/f/f/n/g;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v3

    :cond_7
    iget-object v5, v0, Lb0/g0/n/a;->d:Lc0/e;

    invoke-virtual {v5, v7}, Lc0/e;->N(I)Lc0/e;

    :goto_5
    iget-object v0, v0, Lb0/g0/n/a;->d:Lc0/e;

    iget-wide v5, v0, Lc0/e;->e:J

    invoke-virtual {v3, v0, v5, v6}, Lc0/e;->write(Lc0/e;J)V

    or-int/lit8 v2, v2, 0x40

    goto :goto_6

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "Failed requirement."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    :goto_6
    iget-object v0, v1, Lb0/g0/n/i;->d:Lc0/e;

    iget-wide v5, v0, Lc0/e;->e:J

    iget-object v0, v1, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {v0, v2}, Lc0/e;->N(I)Lc0/e;

    iget-boolean v0, v1, Lb0/g0/n/i;->j:Z

    if-eqz v0, :cond_a

    const/16 v0, 0x80

    goto :goto_7

    :cond_a
    const/4 v0, 0x0

    :goto_7
    const-wide/16 v2, 0x7d

    cmp-long v7, v5, v2

    if-gtz v7, :cond_b

    long-to-int v2, v5

    or-int/2addr v0, v2

    iget-object v2, v1, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {v2, v0}, Lc0/e;->N(I)Lc0/e;

    goto/16 :goto_8

    :cond_b
    const-wide/32 v2, 0xffff

    cmp-long v7, v5, v2

    if-gtz v7, :cond_c

    or-int/lit8 v0, v0, 0x7e

    iget-object v2, v1, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {v2, v0}, Lc0/e;->N(I)Lc0/e;

    iget-object v0, v1, Lb0/g0/n/i;->e:Lc0/e;

    long-to-int v2, v5

    invoke-virtual {v0, v2}, Lc0/e;->U(I)Lc0/e;

    goto :goto_8

    :cond_c
    or-int/lit8 v0, v0, 0x7f

    iget-object v2, v1, Lb0/g0/n/i;->e:Lc0/e;

    invoke-virtual {v2, v0}, Lc0/e;->N(I)Lc0/e;

    iget-object v0, v1, Lb0/g0/n/i;->e:Lc0/e;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lc0/e;->F(I)Lc0/s;

    move-result-object v3

    iget-object v7, v3, Lc0/s;->a:[B

    iget v8, v3, Lc0/s;->c:I

    add-int/lit8 v9, v8, 0x1

    const/16 v10, 0x38

    ushr-long v10, v5, v10

    const-wide/16 v12, 0xff

    and-long/2addr v10, v12

    long-to-int v11, v10

    int-to-byte v10, v11

    aput-byte v10, v7, v8

    add-int/lit8 v8, v9, 0x1

    const/16 v10, 0x30

    ushr-long v10, v5, v10

    and-long/2addr v10, v12

    long-to-int v11, v10

    int-to-byte v10, v11

    aput-byte v10, v7, v9

    add-int/lit8 v9, v8, 0x1

    const/16 v10, 0x28

    ushr-long v10, v5, v10

    and-long/2addr v10, v12

    long-to-int v11, v10

    int-to-byte v10, v11

    aput-byte v10, v7, v8

    add-int/lit8 v8, v9, 0x1

    const/16 v10, 0x20

    ushr-long v10, v5, v10

    and-long/2addr v10, v12

    long-to-int v11, v10

    int-to-byte v10, v11

    aput-byte v10, v7, v9

    add-int/lit8 v9, v8, 0x1

    const/16 v10, 0x18

    ushr-long v10, v5, v10

    and-long/2addr v10, v12

    long-to-int v11, v10

    int-to-byte v10, v11

    aput-byte v10, v7, v8

    add-int/lit8 v8, v9, 0x1

    const/16 v10, 0x10

    ushr-long v10, v5, v10

    and-long/2addr v10, v12

    long-to-int v11, v10

    int-to-byte v10, v11

    aput-byte v10, v7, v9

    add-int/lit8 v9, v8, 0x1

    ushr-long v10, v5, v2

    and-long/2addr v10, v12

    long-to-int v2, v10

    int-to-byte v2, v2

    aput-byte v2, v7, v8

    add-int/lit8 v2, v9, 0x1

    and-long v10, v5, v12

    long-to-int v8, v10

    int-to-byte v8, v8

    aput-byte v8, v7, v9

    iput v2, v3, Lc0/s;->c:I

    iget-wide v2, v0, Lc0/e;->e:J

    const-wide/16 v7, 0x8

    add-long/2addr v2, v7

    iput-wide v2, v0, Lc0/e;->e:J

    :goto_8
    iget-boolean v0, v1, Lb0/g0/n/i;->j:Z

    if-eqz v0, :cond_f

    iget-object v0, v1, Lb0/g0/n/i;->l:Ljava/util/Random;

    iget-object v2, v1, Lb0/g0/n/i;->h:[B

    if-eqz v2, :cond_e

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextBytes([B)V

    iget-object v0, v1, Lb0/g0/n/i;->e:Lc0/e;

    iget-object v2, v1, Lb0/g0/n/i;->h:[B

    invoke-virtual {v0, v2}, Lc0/e;->J([B)Lc0/e;

    const-wide/16 v2, 0x0

    cmp-long v0, v5, v2

    if-lez v0, :cond_f

    iget-object v0, v1, Lb0/g0/n/i;->d:Lc0/e;

    iget-object v7, v1, Lb0/g0/n/i;->i:Lc0/e$a;

    if-eqz v7, :cond_d

    invoke-virtual {v0, v7}, Lc0/e;->n(Lc0/e$a;)Lc0/e$a;

    iget-object v0, v1, Lb0/g0/n/i;->i:Lc0/e$a;

    invoke-virtual {v0, v2, v3}, Lc0/e$a;->b(J)I

    iget-object v0, v1, Lb0/g0/n/i;->i:Lc0/e$a;

    iget-object v2, v1, Lb0/g0/n/i;->h:[B

    invoke-static {v0, v2}, Lb0/g0/n/g;->a(Lc0/e$a;[B)V

    iget-object v0, v1, Lb0/g0/n/i;->i:Lc0/e$a;

    invoke-virtual {v0}, Lc0/e$a;->close()V

    goto :goto_9

    :cond_d
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_e
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v4

    :cond_f
    :goto_9
    iget-object v0, v1, Lb0/g0/n/i;->e:Lc0/e;

    iget-object v2, v1, Lb0/g0/n/i;->d:Lc0/e;

    invoke-virtual {v0, v2, v5, v6}, Lc0/e;->write(Lc0/e;J)V

    iget-object v0, v1, Lb0/g0/n/i;->k:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->x()Lokio/BufferedSink;

    return-void

    :cond_10
    new-instance v0, Ljava/io/IOException;

    const-string v2, "closed"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lb0/g0/n/i;->g:Lb0/g0/n/a;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lb0/g0/n/a;->f:Lc0/h;

    invoke-virtual {v0}, Lc0/h;->close()V

    :cond_0
    return-void
.end method
