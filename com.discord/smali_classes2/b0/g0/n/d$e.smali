.class public final Lb0/g0/n/d$e;
.super Lb0/g0/f/a;
.source "TaskQueue.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lb0/g0/n/d;->j(Ljava/lang/String;Lb0/g0/n/d$c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic e:J

.field public final synthetic f:Lb0/g0/n/d;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLb0/g0/n/d;Ljava/lang/String;Lb0/g0/n/d$c;Lb0/g0/n/f;)V
    .locals 0

    iput-wide p3, p0, Lb0/g0/n/d$e;->e:J

    iput-object p5, p0, Lb0/g0/n/d$e;->f:Lb0/g0/n/d;

    const/4 p1, 0x1

    invoke-direct {p0, p2, p1}, Lb0/g0/f/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 8

    iget-object v0, p0, Lb0/g0/n/d$e;->f:Lb0/g0/n/d;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lb0/g0/n/d;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit v0

    goto :goto_1

    :cond_0
    :try_start_1
    iget-object v1, v0, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    if-eqz v1, :cond_3

    iget-boolean v2, v0, Lb0/g0/n/d;->s:Z

    const/4 v3, -0x1

    if-eqz v2, :cond_1

    iget v2, v0, Lb0/g0/n/d;->p:I

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_0
    iget v4, v0, Lb0/g0/n/d;->p:I

    const/4 v5, 0x1

    add-int/2addr v4, v5

    iput v4, v0, Lb0/g0/n/d;->p:I

    iput-boolean v5, v0, Lb0/g0/n/d;->s:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    const/4 v4, 0x0

    if-eq v2, v3, :cond_2

    new-instance v1, Ljava/net/SocketTimeoutException;

    const-string v3, "sent ping but didn\'t receive pong within "

    invoke-static {v3}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v6, v0, Lb0/g0/n/d;->w:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v6, "ms (after "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-int/2addr v2, v5

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " successful ping/pongs)"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v4}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    goto :goto_1

    :cond_2
    :try_start_2
    sget-object v2, Lokio/ByteString;->f:Lokio/ByteString;

    const-string v3, "payload"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0x9

    invoke-virtual {v1, v3, v2}, Lb0/g0/n/i;->b(ILokio/ByteString;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v0, v1, v4}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    goto :goto_1

    :cond_3
    monitor-exit v0

    :goto_1
    iget-wide v0, p0, Lb0/g0/n/d$e;->e:J

    return-wide v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
