.class public final Lb0/g0/n/d$d;
.super Lb0/g0/f/a;
.source "RealWebSocket.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/g0/n/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "d"
.end annotation


# instance fields
.field public final synthetic e:Lb0/g0/n/d;


# direct methods
.method public constructor <init>(Lb0/g0/n/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/n/d$d;->e:Lb0/g0/n/d;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, p1, Lb0/g0/n/d;->g:Ljava/lang/String;

    const-string v1, " writer"

    invoke-static {v0, p1, v1}, Lf/e/c/a/a;->z(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lb0/g0/f/a;-><init>(Ljava/lang/String;ZI)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 3

    :try_start_0
    iget-object v0, p0, Lb0/g0/n/d$d;->e:Lb0/g0/n/d;

    invoke-virtual {v0}, Lb0/g0/n/d;->m()Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lb0/g0/n/d$d;->e:Lb0/g0/n/d;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    :cond_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method
