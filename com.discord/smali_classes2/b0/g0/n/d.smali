.class public final Lb0/g0/n/d;
.super Ljava/lang/Object;
.source "RealWebSocket.kt"

# interfaces
.implements Lokhttp3/WebSocket;
.implements Lb0/g0/n/h$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/g0/n/d$b;,
        Lb0/g0/n/d$a;,
        Lb0/g0/n/d$c;,
        Lb0/g0/n/d$d;
    }
.end annotation


# static fields
.field public static final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/z;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Lb0/e;

.field public c:Lb0/g0/f/a;

.field public d:Lb0/g0/n/h;

.field public e:Lb0/g0/n/i;

.field public f:Lb0/g0/f/c;

.field public g:Ljava/lang/String;

.field public h:Lb0/g0/n/d$c;

.field public final i:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public k:J

.field public l:Z

.field public m:I

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:I

.field public q:I

.field public r:I

.field public s:Z

.field public final t:Lb0/a0;

.field public final u:Lokhttp3/WebSocketListener;

.field public final v:Ljava/util/Random;

.field public final w:J

.field public x:Lb0/g0/n/f;

.field public y:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lb0/z;->e:Lb0/z;

    invoke-static {v0}, Lf/h/a/f/f/n/g;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lb0/g0/n/d;->z:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lb0/g0/f/d;Lb0/a0;Lokhttp3/WebSocketListener;Ljava/util/Random;JLb0/g0/n/f;J)V
    .locals 0

    const-string p7, "taskRunner"

    invoke-static {p1, p7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p7, "originalRequest"

    invoke-static {p2, p7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p7, "listener"

    invoke-static {p3, p7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p7, "random"

    invoke-static {p4, p7}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lb0/g0/n/d;->t:Lb0/a0;

    iput-object p3, p0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    iput-object p4, p0, Lb0/g0/n/d;->v:Ljava/util/Random;

    iput-wide p5, p0, Lb0/g0/n/d;->w:J

    const/4 p3, 0x0

    iput-object p3, p0, Lb0/g0/n/d;->x:Lb0/g0/n/f;

    iput-wide p8, p0, Lb0/g0/n/d;->y:J

    invoke-virtual {p1}, Lb0/g0/f/d;->f()Lb0/g0/f/c;

    move-result-object p1

    iput-object p1, p0, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lb0/g0/n/d;->i:Ljava/util/ArrayDeque;

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    const/4 p1, -0x1

    iput p1, p0, Lb0/g0/n/d;->m:I

    iget-object p1, p2, Lb0/a0;->c:Ljava/lang/String;

    const-string p3, "GET"

    invoke-static {p3, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lokio/ByteString;->g:Lokio/ByteString$a;

    const/16 p2, 0x10

    new-array p2, p2, [B

    invoke-virtual {p4, p2}, Ljava/util/Random;->nextBytes([B)V

    const/4 p3, 0x3

    const/4 p4, 0x0

    invoke-static {p1, p2, p4, p4, p3}, Lokio/ByteString$a;->c(Lokio/ByteString$a;[BIII)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->f()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lb0/g0/n/d;->a:Ljava/lang/String;

    return-void

    :cond_0
    const-string p1, "Request must be GET: "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object p2, p2, Lb0/a0;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method


# virtual methods
.method public a(Ljava/lang/String;)Z
    .locals 7

    const-string v0, "text"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lokio/ByteString;->g:Lokio/ByteString$a;

    invoke-virtual {v0, p1}, Lokio/ByteString$a;->b(Ljava/lang/String;)Lokio/ByteString;

    move-result-object p1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/n/d;->o:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lb0/g0/n/d;->l:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-wide v3, p0, Lb0/g0/n/d;->k:J

    invoke-virtual {p1}, Lokio/ByteString;->j()I

    move-result v0

    int-to-long v5, v0

    add-long/2addr v3, v5

    const-wide/32 v5, 0x1000000

    cmp-long v0, v3, v5

    if-lez v0, :cond_1

    const/16 p1, 0x3e9

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lb0/g0/n/d;->e(ILjava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    goto :goto_1

    :cond_1
    :try_start_1
    iget-wide v0, p0, Lb0/g0/n/d;->k:J

    invoke-virtual {p1}, Lokio/ByteString;->j()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v0, v3

    iput-wide v0, p0, Lb0/g0/n/d;->k:J

    iget-object v0, p0, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    new-instance v1, Lb0/g0/n/d$b;

    invoke-direct {v1, v2, p1}, Lb0/g0/n/d$b;-><init>(ILokio/ByteString;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lb0/g0/n/d;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    monitor-exit p0

    :goto_1
    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public b(Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    invoke-virtual {v0, p0, p1}, Lokhttp3/WebSocketListener;->onMessage(Lokhttp3/WebSocket;Lokio/ByteString;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "text"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    invoke-virtual {v0, p0, p1}, Lokhttp3/WebSocketListener;->onMessage(Lokhttp3/WebSocket;Ljava/lang/String;)V

    return-void
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lb0/g0/n/d;->b:Lb0/e;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lb0/e;->cancel()V

    return-void

    :cond_0
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public declared-synchronized d(Lokio/ByteString;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lb0/g0/n/d;->o:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lb0/g0/n/d;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lb0/g0/n/d;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lb0/g0/n/d;->l()V

    iget p1, p0, Lb0/g0/n/d;->q:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lb0/g0/n/d;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public e(ILjava/lang/String;)Z
    .locals 8

    monitor-enter p0

    const/16 v0, 0x3e8

    const/4 v1, 0x0

    if-lt p1, v0, :cond_5

    const/16 v0, 0x1388

    if-lt p1, v0, :cond_0

    goto :goto_1

    :cond_0
    const/16 v0, 0x3ee

    const/16 v2, 0x3ec

    if-gt v2, p1, :cond_1

    if-ge v0, p1, :cond_3

    :cond_1
    const/16 v0, 0xbb7

    const/16 v2, 0x3f7

    if-le v2, p1, :cond_2

    goto :goto_0

    :cond_2
    if-lt v0, p1, :cond_4

    :cond_3
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Code "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " is reserved and may not be used."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    :goto_0
    move-object v0, v1

    goto :goto_2

    :cond_5
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Code must be in range [1000,5000): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_6

    const/4 v4, 0x1

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    :goto_3
    if-nez v4, :cond_8

    if-nez v0, :cond_7

    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_7
    :try_start_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    if-eqz p2, :cond_b

    sget-object v0, Lokio/ByteString;->g:Lokio/ByteString$a;

    invoke-virtual {v0, p2}, Lokio/ByteString$a;->b(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lokio/ByteString;->j()I

    move-result v0

    int-to-long v4, v0

    const-wide/16 v6, 0x7b

    cmp-long v0, v4, v6

    if-gtz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_a

    goto :goto_5

    :cond_a
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "reason.size() > 123: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_b
    :goto_5
    iget-boolean p2, p0, Lb0/g0/n/d;->o:Z

    if-nez p2, :cond_d

    iget-boolean p2, p0, Lb0/g0/n/d;->l:Z

    if-eqz p2, :cond_c

    goto :goto_6

    :cond_c
    iput-boolean v2, p0, Lb0/g0/n/d;->l:Z

    iget-object p2, p0, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    new-instance v0, Lb0/g0/n/d$a;

    const-wide/32 v3, 0xea60

    invoke-direct {v0, p1, v1, v3, v4}, Lb0/g0/n/d$a;-><init>(ILokio/ByteString;J)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lb0/g0/n/d;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    goto :goto_7

    :cond_d
    :goto_6
    monitor-exit p0

    const/4 v2, 0x0

    :goto_7
    return v2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized f(Lokio/ByteString;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    const-string v0, "payload"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p1, p0, Lb0/g0/n/d;->r:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lb0/g0/n/d;->r:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lb0/g0/n/d;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public g(ILjava/lang/String;)V
    .locals 4

    const-string v0, "reason"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_b

    monitor-enter p0

    :try_start_0
    iget v3, p0, Lb0/g0/n/d;->m:I

    if-ne v3, v2, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_a

    iput p1, p0, Lb0/g0/n/d;->m:I

    iput-object p2, p0, Lb0/g0/n/d;->n:Ljava/lang/String;

    iget-boolean v0, p0, Lb0/g0/n/d;->l:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    iput-object v1, p0, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    iget-object v2, p0, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    iput-object v1, p0, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    iget-object v3, p0, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iput-object v1, p0, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iget-object v1, p0, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    invoke-virtual {v1}, Lb0/g0/f/c;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object v1, v0

    goto :goto_1

    :cond_2
    move-object v2, v1

    move-object v3, v2

    :goto_1
    monitor-exit p0

    :try_start_1
    iget-object v0, p0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    invoke-virtual {v0, p0, p1, p2}, Lokhttp3/WebSocketListener;->onClosing(Lokhttp3/WebSocket;ILjava/lang/String;)V

    if-eqz v1, :cond_3

    iget-object v0, p0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    invoke-virtual {v0, p0, p1, p2}, Lokhttp3/WebSocketListener;->onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    if-eqz v1, :cond_4

    sget-object p1, Lb0/g0/c;->a:[B

    const-string p1, "$this$closeQuietly"

    invoke-static {v1, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_2
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    nop

    goto :goto_2

    :catch_1
    move-exception p1

    throw p1

    :cond_4
    :goto_2
    if-eqz v2, :cond_5

    sget-object p1, Lb0/g0/c;->a:[B

    const-string p1, "$this$closeQuietly"

    invoke-static {v2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_3
    invoke-virtual {v2}, Lb0/g0/n/h;->close()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    nop

    goto :goto_3

    :catch_3
    move-exception p1

    throw p1

    :cond_5
    :goto_3
    if-eqz v3, :cond_6

    sget-object p1, Lb0/g0/c;->a:[B

    const-string p1, "$this$closeQuietly"

    invoke-static {v3, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_4
    invoke-virtual {v3}, Lb0/g0/n/i;->close()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_4

    :catch_4
    move-exception p1

    throw p1

    :catch_5
    :cond_6
    :goto_4
    return-void

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_7

    sget-object p2, Lb0/g0/c;->a:[B

    const-string p2, "$this$closeQuietly"

    invoke-static {v1, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_5
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    goto :goto_5

    :catch_6
    nop

    goto :goto_5

    :catch_7
    move-exception p1

    throw p1

    :cond_7
    :goto_5
    if-eqz v2, :cond_8

    sget-object p2, Lb0/g0/c;->a:[B

    const-string p2, "$this$closeQuietly"

    invoke-static {v2, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_6
    invoke-virtual {v2}, Lb0/g0/n/h;->close()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8

    goto :goto_6

    :catch_8
    nop

    goto :goto_6

    :catch_9
    move-exception p1

    throw p1

    :cond_8
    :goto_6
    if-eqz v3, :cond_9

    sget-object p2, Lb0/g0/c;->a:[B

    const-string p2, "$this$closeQuietly"

    invoke-static {v3, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_7
    invoke-virtual {v3}, Lb0/g0/n/i;->close()V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_b

    goto :goto_7

    :catch_a
    move-exception p1

    throw p1

    :catch_b
    :cond_9
    :goto_7
    throw p1

    :cond_a
    :try_start_8
    const-string p1, "already closed"

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1

    :cond_b
    const-string p1, "Failed requirement."

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final h(Lokhttp3/Response;Lb0/g0/g/c;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p1, Lokhttp3/Response;->g:I

    const/16 v1, 0x65

    const/16 v2, 0x27

    if-ne v0, v1, :cond_4

    const/4 v0, 0x0

    const/4 v1, 0x2

    const-string v3, "Connection"

    invoke-static {p1, v3, v0, v1}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Upgrade"

    const/4 v5, 0x1

    invoke-static {v4, v3, v5}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {p1, v4, v0, v1}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "websocket"

    invoke-static {v4, v3, v5}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v3, "Sec-WebSocket-Accept"

    invoke-static {p1, v3, v0, v1}, Lokhttp3/Response;->a(Lokhttp3/Response;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lokio/ByteString;->g:Lokio/ByteString$a;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lb0/g0/n/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokio/ByteString$a;->b(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    const-string v1, "SHA-1"

    invoke-virtual {v0, v1}, Lokio/ByteString;->g(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v5

    if-nez v1, :cond_1

    if-eqz p2, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Web Socket exchange missing: bad interceptor?"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected \'Sec-WebSocket-Accept\' header value \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' but was \'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    new-instance p1, Ljava/net/ProtocolException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected \'Upgrade\' header value \'websocket\' but was \'"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/net/ProtocolException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected \'Connection\' header value \'Upgrade\' but was \'"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p2, Ljava/net/ProtocolException;

    const-string v0, "Expected HTTP 101 response but was \'"

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lokhttp3/Response;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lokhttp3/Response;->f:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final i(Ljava/lang/Exception;Lokhttp3/Response;)V
    .locals 4

    const-string v0, "e"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb0/g0/n/d;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lb0/g0/n/d;->o:Z

    iget-object v0, p0, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    const/4 v1, 0x0

    iput-object v1, p0, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    iget-object v2, p0, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    iput-object v1, p0, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    iget-object v3, p0, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iput-object v1, p0, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iget-object v1, p0, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    invoke-virtual {v1}, Lb0/g0/f/c;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    :try_start_2
    iget-object v1, p0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    invoke-virtual {v1, p0, p1, p2}, Lokhttp3/WebSocketListener;->onFailure(Lokhttp3/WebSocket;Ljava/lang/Throwable;Lokhttp3/Response;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    sget-object p1, Lb0/g0/c;->a:[B

    const-string p1, "$this$closeQuietly"

    invoke-static {v0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_3
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    :catch_1
    move-exception p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    sget-object p1, Lb0/g0/c;->a:[B

    const-string p1, "$this$closeQuietly"

    invoke-static {v2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_4
    invoke-virtual {v2}, Lb0/g0/n/h;->close()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    nop

    goto :goto_1

    :catch_3
    move-exception p1

    throw p1

    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    sget-object p1, Lb0/g0/c;->a:[B

    const-string p1, "$this$closeQuietly"

    invoke-static {v3, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_5
    invoke-virtual {v3}, Lb0/g0/n/i;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_2

    :catch_4
    move-exception p1

    throw p1

    :catch_5
    :cond_3
    :goto_2
    return-void

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_4

    sget-object p2, Lb0/g0/c;->a:[B

    const-string p2, "$this$closeQuietly"

    invoke-static {v0, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_6
    invoke-interface {v0}, Ljava/io/Closeable;->close()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_3

    :catch_6
    nop

    goto :goto_3

    :catch_7
    move-exception p1

    throw p1

    :cond_4
    :goto_3
    if-eqz v2, :cond_5

    sget-object p2, Lb0/g0/c;->a:[B

    const-string p2, "$this$closeQuietly"

    invoke-static {v2, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_7
    invoke-virtual {v2}, Lb0/g0/n/h;->close()V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_9
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8

    goto :goto_4

    :catch_8
    nop

    goto :goto_4

    :catch_9
    move-exception p1

    throw p1

    :cond_5
    :goto_4
    if-eqz v3, :cond_6

    sget-object p2, Lb0/g0/c;->a:[B

    const-string p2, "$this$closeQuietly"

    invoke-static {v3, p2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_8
    invoke-virtual {v3}, Lb0/g0/n/i;->close()V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_a
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_b

    goto :goto_5

    :catch_a
    move-exception p1

    throw p1

    :catch_b
    :cond_6
    :goto_5
    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final j(Ljava/lang/String;Lb0/g0/n/d$c;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v0, p1

    move-object/from16 v11, p2

    const-string v1, "name"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "streams"

    invoke-static {v11, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v12, v10, Lb0/g0/n/d;->x:Lb0/g0/n/f;

    if-eqz v12, :cond_4

    monitor-enter p0

    :try_start_0
    iput-object v0, v10, Lb0/g0/n/d;->g:Ljava/lang/String;

    iput-object v11, v10, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    new-instance v9, Lb0/g0/n/i;

    iget-boolean v2, v11, Lb0/g0/n/d$c;->d:Z

    iget-object v3, v11, Lb0/g0/n/d$c;->f:Lokio/BufferedSink;

    iget-object v4, v10, Lb0/g0/n/d;->v:Ljava/util/Random;

    iget-boolean v5, v12, Lb0/g0/n/f;->a:Z

    if-eqz v2, :cond_0

    iget-boolean v1, v12, Lb0/g0/n/f;->c:Z

    goto :goto_0

    :cond_0
    iget-boolean v1, v12, Lb0/g0/n/f;->e:Z

    :goto_0
    move v6, v1

    iget-wide v7, v10, Lb0/g0/n/d;->y:J

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lb0/g0/n/i;-><init>(ZLokio/BufferedSink;Ljava/util/Random;ZZJ)V

    iput-object v9, v10, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    new-instance v1, Lb0/g0/n/d$d;

    invoke-direct {v1, v10}, Lb0/g0/n/d$d;-><init>(Lb0/g0/n/d;)V

    iput-object v1, v10, Lb0/g0/n/d;->c:Lb0/g0/f/a;

    iget-wide v1, v10, Lb0/g0/n/d;->w:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v13

    iget-object v15, v10, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " ping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v9, Lb0/g0/n/d$e;

    move-object v1, v9

    move-object v2, v3

    move-wide v4, v13

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object v0, v9

    move-object v9, v12

    invoke-direct/range {v1 .. v9}, Lb0/g0/n/d$e;-><init>(Ljava/lang/String;Ljava/lang/String;JLb0/g0/n/d;Ljava/lang/String;Lb0/g0/n/d$c;Lb0/g0/n/f;)V

    invoke-virtual {v15, v0, v13, v14}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    :cond_1
    iget-object v0, v10, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    invoke-virtual/range {p0 .. p0}, Lb0/g0/n/d;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    new-instance v0, Lb0/g0/n/h;

    iget-boolean v2, v11, Lb0/g0/n/d$c;->d:Z

    iget-object v3, v11, Lb0/g0/n/d$c;->e:Lc0/g;

    iget-boolean v5, v12, Lb0/g0/n/f;->a:Z

    xor-int/lit8 v1, v2, 0x1

    if-eqz v1, :cond_3

    iget-boolean v1, v12, Lb0/g0/n/f;->c:Z

    goto :goto_1

    :cond_3
    iget-boolean v1, v12, Lb0/g0/n/f;->e:Z

    :goto_1
    move v6, v1

    move-object v1, v0

    move-object/from16 v4, p0

    invoke-direct/range {v1 .. v6}, Lb0/g0/n/h;-><init>(ZLc0/g;Lb0/g0/n/h$a;ZZ)V

    iput-object v0, v10, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public final k()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    iget v0, p0, Lb0/g0/n/d;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_13

    iget-object v0, p0, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_12

    invoke-virtual {v0}, Lb0/g0/n/h;->b()V

    iget-boolean v2, v0, Lb0/g0/n/h;->h:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lb0/g0/n/h;->a()V

    goto :goto_0

    :cond_0
    iget v2, v0, Lb0/g0/n/h;->e:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Unknown opcode: "

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2}, Lb0/g0/c;->x(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_1
    iget-boolean v4, v0, Lb0/g0/n/h;->d:Z

    if-nez v4, :cond_11

    iget-wide v4, v0, Lb0/g0/n/h;->f:J

    const-wide/16 v6, 0x0

    cmp-long v8, v4, v6

    if-lez v8, :cond_5

    iget-object v8, v0, Lb0/g0/n/h;->p:Lc0/g;

    iget-object v9, v0, Lb0/g0/n/h;->k:Lc0/e;

    invoke-interface {v8, v9, v4, v5}, Lc0/g;->M(Lc0/e;J)V

    iget-boolean v4, v0, Lb0/g0/n/h;->o:Z

    if-nez v4, :cond_5

    iget-object v4, v0, Lb0/g0/n/h;->k:Lc0/e;

    iget-object v5, v0, Lb0/g0/n/h;->n:Lc0/e$a;

    if-eqz v5, :cond_4

    invoke-virtual {v4, v5}, Lc0/e;->n(Lc0/e$a;)Lc0/e$a;

    iget-object v4, v0, Lb0/g0/n/h;->n:Lc0/e$a;

    iget-object v5, v0, Lb0/g0/n/h;->k:Lc0/e;

    iget-wide v8, v5, Lc0/e;->e:J

    iget-wide v10, v0, Lb0/g0/n/h;->f:J

    sub-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, Lc0/e$a;->b(J)I

    iget-object v4, v0, Lb0/g0/n/h;->n:Lc0/e$a;

    iget-object v5, v0, Lb0/g0/n/h;->m:[B

    if-eqz v5, :cond_3

    invoke-static {v4, v5}, Lb0/g0/n/g;->a(Lc0/e$a;[B)V

    iget-object v4, v0, Lb0/g0/n/h;->n:Lc0/e$a;

    invoke-virtual {v4}, Lc0/e$a;->close()V

    goto :goto_2

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_4
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_5
    :goto_2
    iget-boolean v4, v0, Lb0/g0/n/h;->g:Z

    if-eqz v4, :cond_d

    iget-boolean v1, v0, Lb0/g0/n/h;->i:Z

    if-eqz v1, :cond_b

    iget-object v1, v0, Lb0/g0/n/h;->l:Lb0/g0/n/c;

    if-eqz v1, :cond_6

    goto :goto_3

    :cond_6
    new-instance v1, Lb0/g0/n/c;

    iget-boolean v4, v0, Lb0/g0/n/h;->s:Z

    invoke-direct {v1, v4}, Lb0/g0/n/c;-><init>(Z)V

    iput-object v1, v0, Lb0/g0/n/h;->l:Lb0/g0/n/c;

    :goto_3
    iget-object v4, v0, Lb0/g0/n/h;->k:Lc0/e;

    const-string v5, "buffer"

    invoke-static {v4, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v5, v1, Lb0/g0/n/c;->d:Lc0/e;

    iget-wide v8, v5, Lc0/e;->e:J

    cmp-long v5, v8, v6

    if-nez v5, :cond_7

    const/4 v5, 0x1

    goto :goto_4

    :cond_7
    const/4 v5, 0x0

    :goto_4
    if-eqz v5, :cond_a

    iget-boolean v5, v1, Lb0/g0/n/c;->g:Z

    if-eqz v5, :cond_8

    iget-object v5, v1, Lb0/g0/n/c;->e:Ljava/util/zip/Inflater;

    invoke-virtual {v5}, Ljava/util/zip/Inflater;->reset()V

    :cond_8
    iget-object v5, v1, Lb0/g0/n/c;->d:Lc0/e;

    invoke-virtual {v5, v4}, Lc0/e;->a0(Lc0/x;)J

    iget-object v5, v1, Lb0/g0/n/c;->d:Lc0/e;

    const v6, 0xffff

    invoke-virtual {v5, v6}, Lc0/e;->T(I)Lc0/e;

    iget-object v5, v1, Lb0/g0/n/c;->e:Ljava/util/zip/Inflater;

    invoke-virtual {v5}, Ljava/util/zip/Inflater;->getBytesRead()J

    move-result-wide v5

    iget-object v7, v1, Lb0/g0/n/c;->d:Lc0/e;

    iget-wide v7, v7, Lc0/e;->e:J

    add-long/2addr v5, v7

    :cond_9
    iget-object v7, v1, Lb0/g0/n/c;->f:Lc0/m;

    const-wide v8, 0x7fffffffffffffffL

    invoke-virtual {v7, v4, v8, v9}, Lc0/m;->a(Lc0/e;J)J

    iget-object v7, v1, Lb0/g0/n/c;->e:Ljava/util/zip/Inflater;

    invoke-virtual {v7}, Ljava/util/zip/Inflater;->getBytesRead()J

    move-result-wide v7

    cmp-long v9, v7, v5

    if-ltz v9, :cond_9

    goto :goto_5

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Failed requirement."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    :goto_5
    if-ne v2, v3, :cond_c

    iget-object v1, v0, Lb0/g0/n/h;->q:Lb0/g0/n/h$a;

    iget-object v0, v0, Lb0/g0/n/h;->k:Lc0/e;

    invoke-virtual {v0}, Lc0/e;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lb0/g0/n/h$a;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-object v1, v0, Lb0/g0/n/h;->q:Lb0/g0/n/h$a;

    iget-object v0, v0, Lb0/g0/n/h;->k:Lc0/e;

    invoke-virtual {v0}, Lc0/e;->o()Lokio/ByteString;

    move-result-object v0

    invoke-interface {v1, v0}, Lb0/g0/n/h$a;->b(Lokio/ByteString;)V

    goto/16 :goto_0

    :cond_d
    :goto_6
    iget-boolean v4, v0, Lb0/g0/n/h;->d:Z

    if-nez v4, :cond_f

    invoke-virtual {v0}, Lb0/g0/n/h;->b()V

    iget-boolean v4, v0, Lb0/g0/n/h;->h:Z

    if-nez v4, :cond_e

    goto :goto_7

    :cond_e
    invoke-virtual {v0}, Lb0/g0/n/h;->a()V

    goto :goto_6

    :cond_f
    :goto_7
    iget v4, v0, Lb0/g0/n/h;->e:I

    if-nez v4, :cond_10

    goto/16 :goto_1

    :cond_10
    new-instance v1, Ljava/net/ProtocolException;

    const-string v2, "Expected continuation opcode. Got: "

    invoke-static {v2}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lb0/g0/n/h;->e:I

    invoke-static {v0}, Lb0/g0/c;->x(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_11
    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_13
    return-void
.end method

.method public final l()V
    .locals 4

    sget-object v0, Lb0/g0/c;->a:[B

    iget-object v0, p0, Lb0/g0/n/d;->c:Lb0/g0/f/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    :cond_0
    return-void
.end method

.method public final m()Z
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v15, p0

    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    const/4 v14, 0x0

    iput-object v14, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v13, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v13}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    const/4 v1, -0x1

    iput v1, v13, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    new-instance v12, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v12}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v14, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v11, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v11}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v14, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v10, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v10}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v14, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    new-instance v9, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v9}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    iput-object v14, v9, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iget-boolean v2, v15, Lb0/g0/n/d;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    monitor-exit p0

    return v3

    :cond_0
    :try_start_1
    iget-object v8, v15, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iget-object v2, v15, Lb0/g0/n/d;->i:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lokio/ByteString;

    if-nez v7, :cond_4

    iget-object v2, v15, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    instance-of v4, v2, Lb0/g0/n/d$a;

    if-eqz v4, :cond_3

    iget v2, v15, Lb0/g0/n/d;->m:I

    iput v2, v13, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    iget-object v3, v15, Lb0/g0/n/d;->n:Ljava/lang/String;

    iput-object v3, v12, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-eq v2, v1, :cond_1

    iget-object v1, v15, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    iput-object v1, v11, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    iput-object v14, v15, Lb0/g0/n/d;->h:Lb0/g0/n/d$c;

    iget-object v1, v15, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    iput-object v1, v10, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    iput-object v14, v15, Lb0/g0/n/d;->d:Lb0/g0/n/h;

    iget-object v1, v15, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iput-object v1, v9, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    iput-object v14, v15, Lb0/g0/n/d;->e:Lb0/g0/n/i;

    iget-object v1, v15, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    invoke-virtual {v1}, Lb0/g0/f/c;->f()V

    goto/16 :goto_0

    :cond_1
    iget-object v1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-eqz v1, :cond_2

    check-cast v1, Lb0/g0/n/d$a;

    iget-wide v1, v1, Lb0/g0/n/d$a;->c:J

    iget-object v6, v15, Lb0/g0/n/d;->f:Lb0/g0/f/c;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v15, Lb0/g0/n/d;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " cancel"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    const/4 v5, 0x1

    new-instance v1, Lb0/g0/n/d$f;

    move-object/from16 v16, v1

    move-object/from16 v1, v16

    move-wide/from16 v17, v2

    move-object v2, v4

    move v3, v5

    move-object/from16 v19, v6

    move-object/from16 v6, p0

    move-object/from16 v20, v7

    move-object v7, v8

    move-object/from16 v21, v8

    move-object/from16 v8, v20

    move-object/from16 v22, v9

    move-object v9, v0

    move-object/from16 v23, v10

    move-object v10, v13

    move-object/from16 v24, v11

    move-object v11, v12

    move-object/from16 v25, v12

    move-object/from16 v12, v24

    move-object/from16 v26, v13

    move-object/from16 v13, v23

    move-object/from16 v27, v14

    move-object/from16 v14, v22

    invoke-direct/range {v1 .. v14}, Lb0/g0/n/d$f;-><init>(Ljava/lang/String;ZLjava/lang/String;ZLb0/g0/n/d;Lb0/g0/n/i;Lokio/ByteString;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$IntRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    move-object/from16 v4, v16

    move-wide/from16 v2, v17

    move-object/from16 v1, v19

    invoke-virtual {v1, v4, v2, v3}, Lb0/g0/f/c;->c(Lb0/g0/f/a;J)V

    goto :goto_1

    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Close"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    :cond_3
    move-object/from16 v20, v7

    move-object/from16 v21, v8

    move-object/from16 v22, v9

    move-object/from16 v23, v10

    move-object/from16 v24, v11

    move-object/from16 v25, v12

    move-object/from16 v26, v13

    move-object/from16 v27, v14

    if-nez v2, :cond_5

    monitor-exit p0

    return v3

    :cond_4
    :goto_0
    move-object/from16 v20, v7

    move-object/from16 v21, v8

    move-object/from16 v22, v9

    move-object/from16 v23, v10

    move-object/from16 v24, v11

    move-object/from16 v25, v12

    move-object/from16 v26, v13

    move-object/from16 v27, v14

    :cond_5
    :goto_1
    monitor-exit p0

    move-object/from16 v2, v20

    if-eqz v2, :cond_7

    move-object/from16 v1, v21

    if-eqz v1, :cond_6

    :try_start_2
    const-string v0, "payload"

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0, v2}, Lb0/g0/n/i;->b(ILokio/ByteString;)V

    goto :goto_2

    :cond_6
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    throw v27

    :cond_7
    move-object/from16 v1, v21

    :try_start_3
    iget-object v0, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    instance-of v2, v0, Lb0/g0/n/d$b;

    if-eqz v2, :cond_a

    if-eqz v0, :cond_9

    check-cast v0, Lb0/g0/n/d$b;

    if-eqz v1, :cond_8

    iget v2, v0, Lb0/g0/n/d$b;->a:I

    iget-object v3, v0, Lb0/g0/n/d$b;->b:Lokio/ByteString;

    invoke-virtual {v1, v2, v3}, Lb0/g0/n/i;->c(ILokio/ByteString;)V

    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    iget-wide v1, v15, Lb0/g0/n/d;->k:J

    iget-object v0, v0, Lb0/g0/n/d$b;->b:Lokio/ByteString;

    invoke-virtual {v0}, Lokio/ByteString;->j()I

    move-result v0

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, v15, Lb0/g0/n/d;->k:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    monitor-exit p0

    :goto_2
    move-object/from16 v1, v24

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v27

    :cond_9
    :try_start_6
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Message"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    instance-of v2, v0, Lb0/g0/n/d$a;

    if-eqz v2, :cond_12

    if-eqz v0, :cond_11

    check-cast v0, Lb0/g0/n/d$a;

    if-eqz v1, :cond_10

    iget v2, v0, Lb0/g0/n/d$a;->a:I

    iget-object v0, v0, Lb0/g0/n/d$a;->b:Lokio/ByteString;

    invoke-virtual {v1, v2, v0}, Lb0/g0/n/i;->a(ILokio/ByteString;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-object/from16 v1, v24

    :try_start_7
    iget-object v0, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v0, Lb0/g0/n/d$c;

    if-eqz v0, :cond_c

    iget-object v0, v15, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    move-object/from16 v2, v26

    iget v2, v2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    move-object/from16 v3, v25

    iget-object v3, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_b

    invoke-virtual {v0, v15, v2, v3}, Lokhttp3/WebSocketListener;->onClosed(Lokhttp3/WebSocket;ILjava/lang/String;)V

    goto :goto_3

    :cond_b
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v27

    :cond_c
    :goto_3
    const/4 v0, 0x1

    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lb0/g0/n/d$c;

    if-eqz v1, :cond_d

    sget-object v2, Lb0/g0/c;->a:[B

    const-string v2, "$this$closeQuietly"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_8
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v1, v0

    throw v1

    :catch_1
    :cond_d
    :goto_4
    move-object/from16 v2, v23

    iget-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lb0/g0/n/h;

    if-eqz v1, :cond_e

    sget-object v2, Lb0/g0/c;->a:[B

    const-string v2, "$this$closeQuietly"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_9
    invoke-virtual {v1}, Lb0/g0/n/h;->close()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_5

    :catch_2
    move-exception v0

    move-object v1, v0

    throw v1

    :catch_3
    :cond_e
    :goto_5
    move-object/from16 v3, v22

    iget-object v1, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lb0/g0/n/i;

    if-eqz v1, :cond_f

    sget-object v2, Lb0/g0/c;->a:[B

    const-string v2, "$this$closeQuietly"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_a
    invoke-virtual {v1}, Lb0/g0/n/i;->close()V
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto :goto_6

    :catch_4
    move-exception v0

    move-object v1, v0

    throw v1

    :catch_5
    :cond_f
    :goto_6
    return v0

    :catchall_1
    move-exception v0

    move-object/from16 v3, v22

    move-object/from16 v2, v23

    goto :goto_7

    :cond_10
    move-object/from16 v3, v22

    move-object/from16 v2, v23

    move-object/from16 v1, v24

    :try_start_b
    invoke-static {}, Lx/m/c/j;->throwNpe()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v27

    :cond_11
    move-object/from16 v3, v22

    move-object/from16 v2, v23

    move-object/from16 v1, v24

    :try_start_c
    new-instance v0, Lkotlin/TypeCastException;

    const-string v4, "null cannot be cast to non-null type okhttp3.internal.ws.RealWebSocket.Close"

    invoke-direct {v0, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    move-object/from16 v3, v22

    move-object/from16 v2, v23

    move-object/from16 v1, v24

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catchall_2
    move-exception v0

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object/from16 v3, v22

    move-object/from16 v2, v23

    move-object/from16 v1, v24

    :goto_7
    iget-object v1, v1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lb0/g0/n/d$c;

    if-eqz v1, :cond_13

    sget-object v4, Lb0/g0/c;->a:[B

    const-string v4, "$this$closeQuietly"

    invoke-static {v1, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_d
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_8

    :catch_6
    nop

    goto :goto_8

    :catch_7
    move-exception v0

    move-object v1, v0

    throw v1

    :cond_13
    :goto_8
    iget-object v1, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lb0/g0/n/h;

    if-eqz v1, :cond_14

    sget-object v2, Lb0/g0/c;->a:[B

    const-string v2, "$this$closeQuietly"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_e
    invoke-virtual {v1}, Lb0/g0/n/h;->close()V
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_9
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_8

    goto :goto_9

    :catch_8
    nop

    goto :goto_9

    :catch_9
    move-exception v0

    move-object v1, v0

    throw v1

    :cond_14
    :goto_9
    iget-object v1, v3, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast v1, Lb0/g0/n/i;

    if-eqz v1, :cond_15

    sget-object v2, Lb0/g0/c;->a:[B

    const-string v2, "$this$closeQuietly"

    invoke-static {v1, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_f
    invoke-virtual {v1}, Lb0/g0/n/i;->close()V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_a
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    goto :goto_a

    :catch_a
    move-exception v0

    move-object v1, v0

    throw v1

    :catch_b
    :cond_15
    :goto_a
    throw v0

    :catchall_4
    move-exception v0

    monitor-exit p0

    throw v0
.end method
