.class public final Lb0/g0/n/e;
.super Ljava/lang/Object;
.source "RealWebSocket.kt"

# interfaces
.implements Lb0/f;


# instance fields
.field public final synthetic a:Lb0/g0/n/d;

.field public final synthetic b:Lb0/a0;


# direct methods
.method public constructor <init>(Lb0/g0/n/d;Lb0/a0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb0/a0;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    iput-object p2, p0, Lb0/g0/n/e;->b:Lb0/a0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lb0/e;Lokhttp3/Response;)V
    .locals 21

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    const-string v0, "call"

    move-object/from16 v3, p1

    invoke-static {v3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, v2, Lokhttp3/Response;->p:Lb0/g0/g/c;

    :try_start_0
    iget-object v0, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    invoke-virtual {v0, v2, v3}, Lb0/g0/n/d;->h(Lokhttp3/Response;Lb0/g0/g/c;)V

    invoke-virtual {v3}, Lb0/g0/g/c;->d()Lb0/g0/n/d$c;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v3, v2, Lokhttp3/Response;->i:Lokhttp3/Headers;

    const-string v4, "responseHeaders"

    invoke-static {v3, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lokhttp3/Headers;->size()I

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    :goto_0
    const/4 v14, 0x1

    if-ge v7, v4, :cond_15

    invoke-virtual {v3, v7}, Lokhttp3/Headers;->d(I)Ljava/lang/String;

    move-result-object v8

    const-string v10, "Sec-WebSocket-Extensions"

    invoke-static {v8, v10, v14}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    move-object/from16 v20, v3

    const/4 v3, 0x0

    goto/16 :goto_b

    :cond_1
    invoke-virtual {v3, v7}, Lokhttp3/Headers;->h(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v10, v12, :cond_0

    const/16 v12, 0x2c

    const/4 v5, 0x4

    invoke-static {v8, v12, v10, v6, v5}, Lb0/g0/c;->h(Ljava/lang/String;CIII)I

    move-result v5

    const/16 v12, 0x3b

    invoke-static {v8, v12, v10, v5}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v6

    invoke-static {v8, v10, v6}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v10

    add-int/2addr v6, v14

    const-string v12, "permessage-deflate"

    invoke-static {v10, v12, v14}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_14

    if-eqz v9, :cond_2

    const/16 v17, 0x1

    :cond_2
    :goto_2
    if-ge v6, v5, :cond_13

    const/16 v9, 0x3b

    invoke-static {v8, v9, v6, v5}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v10

    const/16 v12, 0x3d

    invoke-static {v8, v12, v6, v10}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v12

    invoke-static {v8, v6, v12}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    if-ge v12, v10, :cond_4

    add-int/lit8 v12, v12, 0x1

    invoke-static {v8, v12, v10}, Lb0/g0/c;->B(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v12

    const-string v9, "\""

    const-string v14, "$this$removeSurrounding"

    invoke-static {v12, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v20, v3

    const-string v3, "delimiter"

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v12, v14}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "prefix"

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "suffix"

    invoke-static {v9, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v14, 0x2

    if-lt v3, v14, :cond_3

    const/4 v3, 0x0

    invoke-static {v12, v9, v3, v14}, Lx/s/r;->startsWith$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-static {v12, v9, v3, v14}, Lx/s/r;->endsWith$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZI)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v14, 0x1

    sub-int/2addr v9, v14

    invoke-virtual {v12, v14, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const-string v9, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {v12, v9}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    :cond_4
    move-object/from16 v20, v3

    const/4 v3, 0x0

    const/4 v12, 0x0

    :cond_5
    :goto_3
    add-int/lit8 v9, v10, 0x1

    const-string v10, "client_max_window_bits"

    const/4 v14, 0x1

    invoke-static {v6, v10, v14}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_9

    if-eqz v15, :cond_6

    const/16 v17, 0x1

    :cond_6
    if-eqz v12, :cond_7

    invoke-static {v12}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    move-object v15, v6

    goto :goto_4

    :cond_7
    const/4 v15, 0x0

    :goto_4
    if-nez v15, :cond_8

    goto :goto_7

    :cond_8
    move/from16 v14, v17

    goto :goto_8

    :cond_9
    const-string v10, "client_no_context_takeover"

    const/4 v14, 0x1

    invoke-static {v6, v10, v14}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_c

    if-eqz v11, :cond_a

    const/16 v17, 0x1

    :cond_a
    if-eqz v12, :cond_b

    const/16 v19, 0x1

    goto :goto_5

    :cond_b
    move/from16 v19, v17

    :goto_5
    move/from16 v17, v19

    const/4 v11, 0x1

    goto :goto_9

    :cond_c
    const-string v10, "server_max_window_bits"

    invoke-static {v6, v10, v14}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_f

    if-eqz v16, :cond_d

    const/16 v17, 0x1

    :cond_d
    if-eqz v12, :cond_e

    invoke-static {v12}, Lx/s/l;->toIntOrNull(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v16, v6

    goto :goto_6

    :cond_e
    const/16 v16, 0x0

    :goto_6
    if-nez v16, :cond_8

    :goto_7
    const/4 v14, 0x1

    :goto_8
    move/from16 v17, v14

    const/4 v14, 0x1

    goto :goto_9

    :cond_f
    const-string v10, "server_no_context_takeover"

    const/4 v14, 0x1

    invoke-static {v6, v10, v14}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_12

    if-eqz v13, :cond_10

    const/16 v17, 0x1

    :cond_10
    if-eqz v12, :cond_11

    const/16 v17, 0x1

    :cond_11
    const/4 v13, 0x1

    goto :goto_9

    :cond_12
    const/16 v17, 0x1

    :goto_9
    move v6, v9

    move-object/from16 v3, v20

    goto/16 :goto_2

    :cond_13
    move-object/from16 v20, v3

    const/4 v3, 0x0

    move v10, v6

    const/4 v9, 0x1

    goto :goto_a

    :cond_14
    move-object/from16 v20, v3

    const/4 v3, 0x0

    move v10, v6

    const/16 v17, 0x1

    :goto_a
    move-object/from16 v3, v20

    const/4 v6, 0x0

    goto/16 :goto_1

    :goto_b
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v3, v20

    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_15
    const/4 v3, 0x0

    new-instance v4, Lb0/g0/n/f;

    move-object v8, v4

    move-object v10, v15

    move-object/from16 v12, v16

    const/4 v5, 0x1

    move/from16 v14, v17

    invoke-direct/range {v8 .. v14}, Lb0/g0/n/f;-><init>(ZLjava/lang/Integer;ZLjava/lang/Integer;ZZ)V

    iget-object v6, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    iput-object v4, v6, Lb0/g0/n/d;->x:Lb0/g0/n/f;

    if-eqz v17, :cond_16

    goto :goto_c

    :cond_16
    if-eqz v15, :cond_17

    goto :goto_c

    :cond_17
    if-eqz v16, :cond_19

    const/16 v4, 0xf

    const/16 v6, 0x8

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-gt v6, v7, :cond_18

    if-ge v4, v7, :cond_19

    :cond_18
    :goto_c
    const/4 v6, 0x0

    goto :goto_d

    :cond_19
    const/4 v6, 0x1

    :goto_d
    if-nez v6, :cond_1a

    iget-object v3, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    monitor-enter v3

    :try_start_1
    iget-object v4, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    iget-object v4, v4, Lb0/g0/n/d;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->clear()V

    iget-object v4, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    const/16 v5, 0x3f2

    const-string v6, "unexpected Sec-WebSocket-Extensions in response header"

    invoke-virtual {v4, v5, v6}, Lb0/g0/n/d;->e(ILjava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v3

    goto :goto_e

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1a
    :goto_e
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lb0/g0/c;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " WebSocket "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v1, Lb0/g0/n/e;->b:Lb0/a0;

    iget-object v4, v4, Lb0/a0;->b:Lb0/x;

    invoke-virtual {v4}, Lb0/x;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    invoke-virtual {v4, v3, v0}, Lb0/g0/n/d;->j(Ljava/lang/String;Lb0/g0/n/d$c;)V

    iget-object v0, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    iget-object v3, v0, Lb0/g0/n/d;->u:Lokhttp3/WebSocketListener;

    invoke-virtual {v3, v0, v2}, Lokhttp3/WebSocketListener;->onOpen(Lokhttp3/WebSocket;Lokhttp3/Response;)V

    iget-object v0, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    invoke-virtual {v0}, Lb0/g0/n/d;->k()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_f

    :catch_0
    move-exception v0

    iget-object v2, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    :goto_f
    return-void

    :catch_1
    move-exception v0

    if-eqz v3, :cond_1b

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lb0/g0/g/c;->a(JZZLjava/io/IOException;)Ljava/io/IOException;

    :cond_1b
    iget-object v3, v1, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    invoke-virtual {v3, v0, v2}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    sget-object v0, Lb0/g0/c;->a:[B

    const-string v0, "$this$closeQuietly"

    invoke-static {v2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_3
    invoke-virtual/range {p2 .. p2}, Lokhttp3/Response;->close()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    return-void

    :catch_3
    move-exception v0

    move-object v2, v0

    throw v2
.end method

.method public b(Lb0/e;Ljava/io/IOException;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "e"

    invoke-static {p2, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lb0/g0/n/e;->a:Lb0/g0/n/d;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    return-void
.end method
