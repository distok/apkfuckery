.class public final Lb0/l;
.super Ljava/lang/Object;
.source "ConnectionPool.kt"


# instance fields
.field public final a:Lb0/g0/g/k;


# direct methods
.method public constructor <init>()V
    .locals 7

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-string v0, "timeUnit"

    invoke-static {v5, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Lb0/g0/g/k;

    sget-object v1, Lb0/g0/f/d;->h:Lb0/g0/f/d;

    const/4 v2, 0x5

    const-wide/16 v3, 0x5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lb0/g0/g/k;-><init>(Lb0/g0/f/d;IJLjava/util/concurrent/TimeUnit;)V

    const-string v0, "delegate"

    invoke-static {v6, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v6, p0, Lb0/l;->a:Lb0/g0/g/k;

    return-void
.end method
