.class public final Lb0/y$a;
.super Ljava/lang/Object;
.source "OkHttpClient.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public A:I

.field public B:J

.field public C:Lb0/g0/g/l;

.field public a:Lb0/q;

.field public b:Lb0/l;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lb0/t$b;

.field public f:Z

.field public g:Lb0/c;

.field public h:Z

.field public i:Z

.field public j:Lb0/p;

.field public k:Lb0/s;

.field public l:Ljava/net/Proxy;

.field public m:Ljava/net/ProxySelector;

.field public n:Lb0/c;

.field public o:Ljavax/net/SocketFactory;

.field public p:Ljavax/net/ssl/SSLSocketFactory;

.field public q:Ljavax/net/ssl/X509TrustManager;

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/m;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lb0/z;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljavax/net/ssl/HostnameVerifier;

.field public u:Lb0/g;

.field public v:Lb0/g0/m/c;

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lb0/q;

    invoke-direct {v0}, Lb0/q;-><init>()V

    iput-object v0, p0, Lb0/y$a;->a:Lb0/q;

    new-instance v0, Lb0/l;

    invoke-direct {v0}, Lb0/l;-><init>()V

    iput-object v0, p0, Lb0/y$a;->b:Lb0/l;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb0/y$a;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb0/y$a;->d:Ljava/util/List;

    sget-object v0, Lb0/t;->a:Lb0/t;

    const-string v1, "$this$asFactory"

    invoke-static {v0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lb0/g0/a;

    invoke-direct {v1, v0}, Lb0/g0/a;-><init>(Lb0/t;)V

    iput-object v1, p0, Lb0/y$a;->e:Lb0/t$b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb0/y$a;->f:Z

    sget-object v1, Lb0/c;->a:Lb0/c;

    iput-object v1, p0, Lb0/y$a;->g:Lb0/c;

    iput-boolean v0, p0, Lb0/y$a;->h:Z

    iput-boolean v0, p0, Lb0/y$a;->i:Z

    sget-object v0, Lb0/p;->a:Lb0/p;

    iput-object v0, p0, Lb0/y$a;->j:Lb0/p;

    sget-object v0, Lb0/s;->a:Lb0/s;

    iput-object v0, p0, Lb0/y$a;->k:Lb0/s;

    iput-object v1, p0, Lb0/y$a;->n:Lb0/c;

    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    const-string v1, "SocketFactory.getDefault()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lb0/y$a;->o:Ljavax/net/SocketFactory;

    sget-object v0, Lb0/y;->I:Lb0/y$b;

    sget-object v0, Lb0/y;->H:Ljava/util/List;

    iput-object v0, p0, Lb0/y$a;->r:Ljava/util/List;

    sget-object v0, Lb0/y;->G:Ljava/util/List;

    iput-object v0, p0, Lb0/y$a;->s:Ljava/util/List;

    sget-object v0, Lb0/g0/m/d;->a:Lb0/g0/m/d;

    iput-object v0, p0, Lb0/y$a;->t:Ljavax/net/ssl/HostnameVerifier;

    sget-object v0, Lb0/g;->c:Lb0/g;

    iput-object v0, p0, Lb0/y$a;->u:Lb0/g;

    const/16 v0, 0x2710

    iput v0, p0, Lb0/y$a;->x:I

    iput v0, p0, Lb0/y$a;->y:I

    iput v0, p0, Lb0/y$a;->z:I

    const-wide/16 v0, 0x400

    iput-wide v0, p0, Lb0/y$a;->B:J

    return-void
.end method


# virtual methods
.method public final a(JLjava/util/concurrent/TimeUnit;)Lb0/y$a;
    .locals 1

    const-string v0, "unit"

    invoke-static {p3, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Lb0/g0/c;->b(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lb0/y$a;->y:I

    return-object p0
.end method

.method public final b(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)Lb0/y$a;
    .locals 2

    const-string v0, "sslSocketFactory"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trustManager"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lb0/y$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    iget-object v1, p0, Lb0/y$a;->q:Ljavax/net/ssl/X509TrustManager;

    invoke-static {p2, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lb0/y$a;->C:Lb0/g0/g/l;

    :cond_1
    iput-object p1, p0, Lb0/y$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object p1, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object p1, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {p1, p2}, Lb0/g0/k/h;->b(Ljavax/net/ssl/X509TrustManager;)Lb0/g0/m/c;

    move-result-object p1

    iput-object p1, p0, Lb0/y$a;->v:Lb0/g0/m/c;

    iput-object p2, p0, Lb0/y$a;->q:Ljavax/net/ssl/X509TrustManager;

    return-object p0
.end method
