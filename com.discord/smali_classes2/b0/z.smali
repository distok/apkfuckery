.class public final enum Lb0/z;
.super Ljava/lang/Enum;
.source "Protocol.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/z$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lb0/z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Lb0/z;

.field public static final enum e:Lb0/z;

.field public static final enum f:Lb0/z;

.field public static final enum g:Lb0/z;

.field public static final enum h:Lb0/z;

.field public static final enum i:Lb0/z;

.field public static final synthetic j:[Lb0/z;

.field public static final k:Lb0/z$a;


# instance fields
.field private final protocol:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lb0/z;

    new-instance v1, Lb0/z;

    const-string v2, "HTTP_1_0"

    const/4 v3, 0x0

    const-string v4, "http/1.0"

    invoke-direct {v1, v2, v3, v4}, Lb0/z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lb0/z;->d:Lb0/z;

    aput-object v1, v0, v3

    new-instance v1, Lb0/z;

    const-string v2, "HTTP_1_1"

    const/4 v3, 0x1

    const-string v4, "http/1.1"

    invoke-direct {v1, v2, v3, v4}, Lb0/z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lb0/z;->e:Lb0/z;

    aput-object v1, v0, v3

    new-instance v1, Lb0/z;

    const-string v2, "SPDY_3"

    const/4 v3, 0x2

    const-string v4, "spdy/3.1"

    invoke-direct {v1, v2, v3, v4}, Lb0/z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lb0/z;->f:Lb0/z;

    aput-object v1, v0, v3

    new-instance v1, Lb0/z;

    const-string v2, "HTTP_2"

    const/4 v3, 0x3

    const-string v4, "h2"

    invoke-direct {v1, v2, v3, v4}, Lb0/z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lb0/z;->g:Lb0/z;

    aput-object v1, v0, v3

    new-instance v1, Lb0/z;

    const-string v2, "H2_PRIOR_KNOWLEDGE"

    const/4 v3, 0x4

    const-string v4, "h2_prior_knowledge"

    invoke-direct {v1, v2, v3, v4}, Lb0/z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lb0/z;->h:Lb0/z;

    aput-object v1, v0, v3

    new-instance v1, Lb0/z;

    const-string v2, "QUIC"

    const/4 v3, 0x5

    const-string v4, "quic"

    invoke-direct {v1, v2, v3, v4}, Lb0/z;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lb0/z;->i:Lb0/z;

    aput-object v1, v0, v3

    sput-object v0, Lb0/z;->j:[Lb0/z;

    new-instance v0, Lb0/z$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lb0/z$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lb0/z;->k:Lb0/z$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lb0/z;->protocol:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic f(Lb0/z;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lb0/z;->protocol:Ljava/lang/String;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lb0/z;
    .locals 1

    const-class v0, Lb0/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lb0/z;

    return-object p0
.end method

.method public static values()[Lb0/z;
    .locals 1

    sget-object v0, Lb0/z;->j:[Lb0/z;

    invoke-virtual {v0}, [Lb0/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lb0/z;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lb0/z;->protocol:Ljava/lang/String;

    return-object v0
.end method
