.class public Lb0/y;
.super Ljava/lang/Object;
.source "OkHttpClient.kt"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lb0/e$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/y$a;,
        Lb0/y$b;
    }
.end annotation


# static fields
.field public static final G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/z;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/m;",
            ">;"
        }
    .end annotation
.end field

.field public static final I:Lb0/y$b;


# instance fields
.field public final A:I

.field public final B:I

.field public final C:I

.field public final D:I

.field public final E:J

.field public final F:Lb0/g0/g/l;

.field public final d:Lb0/q;

.field public final e:Lb0/l;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokhttp3/Interceptor;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lb0/t$b;

.field public final i:Z

.field public final j:Lb0/c;

.field public final k:Z

.field public final l:Z

.field public final m:Lb0/p;

.field public final n:Lb0/s;

.field public final o:Ljava/net/Proxy;

.field public final p:Ljava/net/ProxySelector;

.field public final q:Lb0/c;

.field public final r:Ljavax/net/SocketFactory;

.field public final s:Ljavax/net/ssl/SSLSocketFactory;

.field public final t:Ljavax/net/ssl/X509TrustManager;

.field public final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/m;",
            ">;"
        }
    .end annotation
.end field

.field public final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb0/z;",
            ">;"
        }
    .end annotation
.end field

.field public final w:Ljavax/net/ssl/HostnameVerifier;

.field public final x:Lb0/g;

.field public final y:Lb0/g0/m/c;

.field public final z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lb0/y$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lb0/y$b;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lb0/y;->I:Lb0/y$b;

    const/4 v0, 0x2

    new-array v1, v0, [Lb0/z;

    sget-object v2, Lb0/z;->g:Lb0/z;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lb0/z;->e:Lb0/z;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v1}, Lb0/g0/c;->m([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lb0/y;->G:Ljava/util/List;

    new-array v0, v0, [Lb0/m;

    sget-object v1, Lb0/m;->g:Lb0/m;

    aput-object v1, v0, v3

    sget-object v1, Lb0/m;->h:Lb0/m;

    aput-object v1, v0, v4

    invoke-static {v0}, Lb0/g0/c;->m([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lb0/y;->H:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Lb0/y$a;

    invoke-direct {v0}, Lb0/y$a;-><init>()V

    invoke-direct {p0, v0}, Lb0/y;-><init>(Lb0/y$a;)V

    return-void
.end method

.method public constructor <init>(Lb0/y$a;)V
    .locals 4

    const-string v0, "builder"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lb0/y$a;->a:Lb0/q;

    iput-object v0, p0, Lb0/y;->d:Lb0/q;

    iget-object v0, p1, Lb0/y$a;->b:Lb0/l;

    iput-object v0, p0, Lb0/y;->e:Lb0/l;

    iget-object v0, p1, Lb0/y$a;->c:Ljava/util/List;

    invoke-static {v0}, Lb0/g0/c;->z(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lb0/y;->f:Ljava/util/List;

    iget-object v0, p1, Lb0/y$a;->d:Ljava/util/List;

    invoke-static {v0}, Lb0/g0/c;->z(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lb0/y;->g:Ljava/util/List;

    iget-object v0, p1, Lb0/y$a;->e:Lb0/t$b;

    iput-object v0, p0, Lb0/y;->h:Lb0/t$b;

    iget-boolean v0, p1, Lb0/y$a;->f:Z

    iput-boolean v0, p0, Lb0/y;->i:Z

    iget-object v0, p1, Lb0/y$a;->g:Lb0/c;

    iput-object v0, p0, Lb0/y;->j:Lb0/c;

    iget-boolean v0, p1, Lb0/y$a;->h:Z

    iput-boolean v0, p0, Lb0/y;->k:Z

    iget-boolean v0, p1, Lb0/y$a;->i:Z

    iput-boolean v0, p0, Lb0/y;->l:Z

    iget-object v0, p1, Lb0/y$a;->j:Lb0/p;

    iput-object v0, p0, Lb0/y;->m:Lb0/p;

    iget-object v0, p1, Lb0/y$a;->k:Lb0/s;

    iput-object v0, p0, Lb0/y;->n:Lb0/s;

    iget-object v0, p1, Lb0/y$a;->l:Ljava/net/Proxy;

    iput-object v0, p0, Lb0/y;->o:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    sget-object v0, Lb0/g0/l/a;->a:Lb0/g0/l/a;

    goto :goto_1

    :cond_0
    iget-object v0, p1, Lb0/y$a;->m:Ljava/net/ProxySelector;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lb0/g0/l/a;->a:Lb0/g0/l/a;

    :goto_1
    iput-object v0, p0, Lb0/y;->p:Ljava/net/ProxySelector;

    iget-object v0, p1, Lb0/y$a;->n:Lb0/c;

    iput-object v0, p0, Lb0/y;->q:Lb0/c;

    iget-object v0, p1, Lb0/y$a;->o:Ljavax/net/SocketFactory;

    iput-object v0, p0, Lb0/y;->r:Ljavax/net/SocketFactory;

    iget-object v0, p1, Lb0/y$a;->r:Ljava/util/List;

    iput-object v0, p0, Lb0/y;->u:Ljava/util/List;

    iget-object v1, p1, Lb0/y$a;->s:Ljava/util/List;

    iput-object v1, p0, Lb0/y;->v:Ljava/util/List;

    iget-object v1, p1, Lb0/y$a;->t:Ljavax/net/ssl/HostnameVerifier;

    iput-object v1, p0, Lb0/y;->w:Ljavax/net/ssl/HostnameVerifier;

    iget v1, p1, Lb0/y$a;->w:I

    iput v1, p0, Lb0/y;->z:I

    iget v1, p1, Lb0/y$a;->x:I

    iput v1, p0, Lb0/y;->A:I

    iget v1, p1, Lb0/y$a;->y:I

    iput v1, p0, Lb0/y;->B:I

    iget v1, p1, Lb0/y$a;->z:I

    iput v1, p0, Lb0/y;->C:I

    iget v1, p1, Lb0/y$a;->A:I

    iput v1, p0, Lb0/y;->D:I

    iget-wide v1, p1, Lb0/y$a;->B:J

    iput-wide v1, p0, Lb0/y;->E:J

    iget-object v1, p1, Lb0/y$a;->C:Lb0/g0/g/l;

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    new-instance v1, Lb0/g0/g/l;

    invoke-direct {v1}, Lb0/g0/g/l;-><init>()V

    :goto_2
    iput-object v1, p0, Lb0/y;->F:Lb0/g0/g/l;

    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb0/m;

    iget-boolean v1, v1, Lb0/m;->a:Z

    if-eqz v1, :cond_5

    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    :goto_3
    const/4 v0, 0x1

    :goto_4
    const/4 v1, 0x0

    if-eqz v0, :cond_7

    iput-object v1, p0, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v1, p0, Lb0/y;->y:Lb0/g0/m/c;

    iput-object v1, p0, Lb0/y;->t:Ljavax/net/ssl/X509TrustManager;

    sget-object p1, Lb0/g;->c:Lb0/g;

    iput-object p1, p0, Lb0/y;->x:Lb0/g;

    goto :goto_5

    :cond_7
    iget-object v0, p1, Lb0/y$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_a

    iput-object v0, p0, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v0, p1, Lb0/y$a;->v:Lb0/g0/m/c;

    if-eqz v0, :cond_9

    iput-object v0, p0, Lb0/y;->y:Lb0/g0/m/c;

    iget-object v3, p1, Lb0/y$a;->q:Ljavax/net/ssl/X509TrustManager;

    if-eqz v3, :cond_8

    iput-object v3, p0, Lb0/y;->t:Ljavax/net/ssl/X509TrustManager;

    iget-object p1, p1, Lb0/y$a;->u:Lb0/g;

    invoke-virtual {p1, v0}, Lb0/g;->b(Lb0/g0/m/c;)Lb0/g;

    move-result-object p1

    iput-object p1, p0, Lb0/y;->x:Lb0/g;

    goto :goto_5

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_9
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_a
    sget-object v0, Lb0/g0/k/h;->c:Lb0/g0/k/h$a;

    sget-object v0, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {v0}, Lb0/g0/k/h;->n()Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    iput-object v0, p0, Lb0/y;->t:Ljavax/net/ssl/X509TrustManager;

    sget-object v3, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    if-eqz v0, :cond_1e

    invoke-virtual {v3, v0}, Lb0/g0/k/h;->m(Ljavax/net/ssl/X509TrustManager;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    iput-object v3, p0, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    const-string v3, "trustManager"

    invoke-static {v0, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lb0/g0/k/h;->a:Lb0/g0/k/h;

    invoke-virtual {v3, v0}, Lb0/g0/k/h;->b(Ljavax/net/ssl/X509TrustManager;)Lb0/g0/m/c;

    move-result-object v0

    iput-object v0, p0, Lb0/y;->y:Lb0/g0/m/c;

    iget-object p1, p1, Lb0/y$a;->u:Lb0/g;

    if-eqz v0, :cond_1d

    invoke-virtual {p1, v0}, Lb0/g;->b(Lb0/g0/m/c;)Lb0/g;

    move-result-object p1

    iput-object p1, p0, Lb0/y;->x:Lb0/g;

    :goto_5
    iget-object p1, p0, Lb0/y;->f:Ljava/util/List;

    const-string v0, "null cannot be cast to non-null type kotlin.collections.List<okhttp3.Interceptor?>"

    if-eqz p1, :cond_1c

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1b

    iget-object p1, p0, Lb0/y;->g:Ljava/util/List;

    if-eqz p1, :cond_1a

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_19

    iget-object p1, p0, Lb0/y;->u:Ljava/util/List;

    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_b

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    goto :goto_6

    :cond_b
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb0/m;

    iget-boolean v0, v0, Lb0/m;->a:Z

    if-eqz v0, :cond_c

    const/4 p1, 0x0

    goto :goto_7

    :cond_d
    :goto_6
    const/4 p1, 0x1

    :goto_7
    if-eqz p1, :cond_15

    iget-object p1, p0, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    if-nez p1, :cond_e

    const/4 p1, 0x1

    goto :goto_8

    :cond_e
    const/4 p1, 0x0

    :goto_8
    const-string v0, "Check failed."

    if-eqz p1, :cond_14

    iget-object p1, p0, Lb0/y;->y:Lb0/g0/m/c;

    if-nez p1, :cond_f

    const/4 p1, 0x1

    goto :goto_9

    :cond_f
    const/4 p1, 0x0

    :goto_9
    if-eqz p1, :cond_13

    iget-object p1, p0, Lb0/y;->t:Ljavax/net/ssl/X509TrustManager;

    if-nez p1, :cond_10

    const/4 v2, 0x1

    :cond_10
    if-eqz v2, :cond_12

    iget-object p1, p0, Lb0/y;->x:Lb0/g;

    sget-object v1, Lb0/g;->c:Lb0/g;

    invoke-static {p1, v1}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_11

    goto :goto_a

    :cond_11
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_12
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_13
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_14
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_15
    iget-object p1, p0, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz p1, :cond_18

    iget-object p1, p0, Lb0/y;->y:Lb0/g0/m/c;

    if-eqz p1, :cond_17

    iget-object p1, p0, Lb0/y;->t:Ljavax/net/ssl/X509TrustManager;

    if-eqz p1, :cond_16

    :goto_a
    return-void

    :cond_16
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string/jumbo v0, "x509TrustManager == null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_17
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "certificateChainCleaner == null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_18
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "sslSocketFactory == null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_19
    const-string p1, "Null network interceptor: "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object v0, p0, Lb0/y;->g:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1a
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1b
    const-string p1, "Null interceptor: "

    invoke-static {p1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-object v0, p0, Lb0/y;->f:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1d
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1

    :cond_1e
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v1
.end method


# virtual methods
.method public b(Lb0/a0;)Lb0/e;
    .locals 2

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lb0/g0/g/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lb0/g0/g/e;-><init>(Lb0/y;Lb0/a0;Z)V

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public e()Lb0/y$a;
    .locals 3

    new-instance v0, Lb0/y$a;

    const-string v1, "okHttpClient"

    invoke-static {p0, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0}, Lb0/y$a;-><init>()V

    iget-object v1, p0, Lb0/y;->d:Lb0/q;

    iput-object v1, v0, Lb0/y$a;->a:Lb0/q;

    iget-object v1, p0, Lb0/y;->e:Lb0/l;

    iput-object v1, v0, Lb0/y$a;->b:Lb0/l;

    iget-object v1, v0, Lb0/y$a;->c:Ljava/util/List;

    iget-object v2, p0, Lb0/y;->f:Ljava/util/List;

    invoke-static {v1, v2}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    iget-object v1, v0, Lb0/y$a;->d:Ljava/util/List;

    iget-object v2, p0, Lb0/y;->g:Ljava/util/List;

    invoke-static {v1, v2}, Lx/h/f;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    iget-object v1, p0, Lb0/y;->h:Lb0/t$b;

    iput-object v1, v0, Lb0/y$a;->e:Lb0/t$b;

    iget-boolean v1, p0, Lb0/y;->i:Z

    iput-boolean v1, v0, Lb0/y$a;->f:Z

    iget-object v1, p0, Lb0/y;->j:Lb0/c;

    iput-object v1, v0, Lb0/y$a;->g:Lb0/c;

    iget-boolean v1, p0, Lb0/y;->k:Z

    iput-boolean v1, v0, Lb0/y$a;->h:Z

    iget-boolean v1, p0, Lb0/y;->l:Z

    iput-boolean v1, v0, Lb0/y$a;->i:Z

    iget-object v1, p0, Lb0/y;->m:Lb0/p;

    iput-object v1, v0, Lb0/y$a;->j:Lb0/p;

    iget-object v1, p0, Lb0/y;->n:Lb0/s;

    iput-object v1, v0, Lb0/y$a;->k:Lb0/s;

    iget-object v1, p0, Lb0/y;->o:Ljava/net/Proxy;

    iput-object v1, v0, Lb0/y$a;->l:Ljava/net/Proxy;

    iget-object v1, p0, Lb0/y;->p:Ljava/net/ProxySelector;

    iput-object v1, v0, Lb0/y$a;->m:Ljava/net/ProxySelector;

    iget-object v1, p0, Lb0/y;->q:Lb0/c;

    iput-object v1, v0, Lb0/y$a;->n:Lb0/c;

    iget-object v1, p0, Lb0/y;->r:Ljavax/net/SocketFactory;

    iput-object v1, v0, Lb0/y$a;->o:Ljavax/net/SocketFactory;

    iget-object v1, p0, Lb0/y;->s:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v1, v0, Lb0/y$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v1, p0, Lb0/y;->t:Ljavax/net/ssl/X509TrustManager;

    iput-object v1, v0, Lb0/y$a;->q:Ljavax/net/ssl/X509TrustManager;

    iget-object v1, p0, Lb0/y;->u:Ljava/util/List;

    iput-object v1, v0, Lb0/y$a;->r:Ljava/util/List;

    iget-object v1, p0, Lb0/y;->v:Ljava/util/List;

    iput-object v1, v0, Lb0/y$a;->s:Ljava/util/List;

    iget-object v1, p0, Lb0/y;->w:Ljavax/net/ssl/HostnameVerifier;

    iput-object v1, v0, Lb0/y$a;->t:Ljavax/net/ssl/HostnameVerifier;

    iget-object v1, p0, Lb0/y;->x:Lb0/g;

    iput-object v1, v0, Lb0/y$a;->u:Lb0/g;

    iget-object v1, p0, Lb0/y;->y:Lb0/g0/m/c;

    iput-object v1, v0, Lb0/y$a;->v:Lb0/g0/m/c;

    iget v1, p0, Lb0/y;->z:I

    iput v1, v0, Lb0/y$a;->w:I

    iget v1, p0, Lb0/y;->A:I

    iput v1, v0, Lb0/y$a;->x:I

    iget v1, p0, Lb0/y;->B:I

    iput v1, v0, Lb0/y$a;->y:I

    iget v1, p0, Lb0/y;->C:I

    iput v1, v0, Lb0/y$a;->z:I

    iget v1, p0, Lb0/y;->D:I

    iput v1, v0, Lb0/y$a;->A:I

    iget-wide v1, p0, Lb0/y;->E:J

    iput-wide v1, v0, Lb0/y$a;->B:J

    iget-object v1, p0, Lb0/y;->F:Lb0/g0/g/l;

    iput-object v1, v0, Lb0/y$a;->C:Lb0/g0/g/l;

    return-object v0
.end method

.method public f(Lb0/a0;Lokhttp3/WebSocketListener;)Lokhttp3/WebSocket;
    .locals 11

    const-string v0, "request"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p2, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lb0/g0/n/d;

    sget-object v2, Lb0/g0/f/d;->h:Lb0/g0/f/d;

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5}, Ljava/util/Random;-><init>()V

    iget v1, p0, Lb0/y;->D:I

    int-to-long v6, v1

    iget-wide v9, p0, Lb0/y;->E:J

    const/4 v8, 0x0

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v10}, Lb0/g0/n/d;-><init>(Lb0/g0/f/d;Lb0/a0;Lokhttp3/WebSocketListener;Ljava/util/Random;JLb0/g0/n/f;J)V

    const-string p1, "client"

    invoke-static {p0, p1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, v0, Lb0/g0/n/d;->t:Lb0/a0;

    const-string p2, "Sec-WebSocket-Extensions"

    invoke-virtual {p1, p2}, Lb0/a0;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Request header not permitted: \'Sec-WebSocket-Extensions\'"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lb0/g0/n/d;->i(Ljava/lang/Exception;Lokhttp3/Response;)V

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0}, Lb0/y;->e()Lb0/y$a;

    move-result-object p1

    sget-object v2, Lb0/t;->a:Lb0/t;

    const-string v3, "eventListener"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lb0/g0/c;->a:[B

    const-string v3, "$this$asFactory"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lb0/g0/a;

    invoke-direct {v3, v2}, Lb0/g0/a;-><init>(Lb0/t;)V

    iput-object v3, p1, Lb0/y$a;->e:Lb0/t$b;

    sget-object v2, Lb0/g0/n/d;->z:Ljava/util/List;

    const-string v3, "protocols"

    invoke-static {v2, v3}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lx/h/f;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lb0/z;->h:Lb0/z;

    move-object v4, v2

    check-cast v4, Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-nez v5, :cond_2

    sget-object v5, Lb0/z;->e:Lb0/z;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_9

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v3, v7, :cond_4

    :cond_3
    const/4 v6, 0x1

    :cond_4
    if-eqz v6, :cond_8

    sget-object v3, Lb0/z;->d:Lb0/z;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_7

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_6

    sget-object v3, Lb0/z;->f:Lb0/z;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v3, p1, Lb0/y$a;->s:Ljava/util/List;

    invoke-static {v2, v3}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v7

    if-eqz v3, :cond_5

    iput-object v1, p1, Lb0/y$a;->C:Lb0/g0/g/l;

    :cond_5
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Collections.unmodifiableList(protocolsCopy)"

    invoke-static {v1, v2}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p1, Lb0/y$a;->s:Ljava/util/List;

    new-instance v1, Lb0/y;

    invoke-direct {v1, p1}, Lb0/y;-><init>(Lb0/y$a;)V

    iget-object p1, v0, Lb0/g0/n/d;->t:Lb0/a0;

    new-instance v2, Lb0/a0$a;

    invoke-direct {v2, p1}, Lb0/a0$a;-><init>(Lb0/a0;)V

    const-string p1, "Upgrade"

    const-string v3, "websocket"

    invoke-virtual {v2, p1, v3}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    const-string v3, "Connection"

    invoke-virtual {v2, v3, p1}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    iget-object p1, v0, Lb0/g0/n/d;->a:Ljava/lang/String;

    const-string v3, "Sec-WebSocket-Key"

    invoke-virtual {v2, v3, p1}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    const-string p1, "Sec-WebSocket-Version"

    const-string v3, "13"

    invoke-virtual {v2, p1, v3}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    const-string p1, "permessage-deflate"

    invoke-virtual {v2, p2, p1}, Lb0/a0$a;->b(Ljava/lang/String;Ljava/lang/String;)Lb0/a0$a;

    invoke-virtual {v2}, Lb0/a0$a;->a()Lb0/a0;

    move-result-object p1

    new-instance p2, Lb0/g0/g/e;

    invoke-direct {p2, v1, p1, v7}, Lb0/g0/g/e;-><init>(Lb0/y;Lb0/a0;Z)V

    iput-object p2, v0, Lb0/g0/n/d;->b:Lb0/e;

    new-instance v1, Lb0/g0/n/e;

    invoke-direct {v1, v0, p1}, Lb0/g0/n/e;-><init>(Lb0/g0/n/d;Lb0/a0;)V

    invoke-virtual {p2, v1}, Lb0/g0/g/e;->t(Lb0/f;)V

    :goto_2
    return-object v0

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "protocols must not contain null"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "protocols must not contain http/1.0: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "protocols containing h2_prior_knowledge cannot use other protocols: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_9
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "protocols must contain h2_prior_knowledge or http/1.1: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
