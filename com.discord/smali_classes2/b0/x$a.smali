.class public final Lb0/x$a;
.super Ljava/lang/Object;
.source "HttpUrl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb0/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb0/x$a$a;
    }
.end annotation


# static fields
.field public static final i:Lb0/x$a$a;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lb0/x$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lb0/x$a$a;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lb0/x$a;->i:Lb0/x$a$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lb0/x$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lb0/x$a;->c:Ljava/lang/String;

    const/4 v1, -0x1

    iput v1, p0, Lb0/x$a;->e:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lb0/x$a;
    .locals 15

    move-object v0, p0

    const-string v1, "encodedName"

    move-object/from16 v3, p1

    invoke-static {v3, v1}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lb0/x$a;->g:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lb0/x$a;->g:Ljava/util/List;

    :cond_0
    iget-object v1, v0, Lb0/x$a;->g:Ljava/util/List;

    const/4 v13, 0x0

    if-eqz v1, :cond_3

    sget-object v14, Lb0/x;->l:Lb0/x$b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xd3

    const-string v6, " \"\'<>#&="

    move-object v2, v14

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v12}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lb0/x$a;->g:Ljava/util/List;

    if-eqz v1, :cond_2

    if-eqz p2, :cond_1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xd3

    const-string v6, " \"\'<>#&="

    move-object v2, v14

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v12}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v13

    :cond_1
    invoke-interface {v1, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0

    :cond_2
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v13

    :cond_3
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v13
.end method

.method public final b()Lb0/x;
    .locals 18

    move-object/from16 v0, p0

    sget-object v7, Lb0/x;->l:Lb0/x$b;

    iget-object v9, v0, Lb0/x$a;->a:Ljava/lang/String;

    if-eqz v9, :cond_6

    iget-object v2, v0, Lb0/x$a;->b:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    move-object v1, v7

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v10

    iget-object v2, v0, Lb0/x$a;->c:Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lb0/x$a;->d:Ljava/lang/String;

    if-eqz v12, :cond_5

    invoke-virtual/range {p0 .. p0}, Lb0/x$a;->c()I

    move-result v13

    iget-object v1, v0, Lb0/x$a;->f:Ljava/util/List;

    new-instance v14, Ljava/util/ArrayList;

    const/16 v8, 0xa

    invoke-static {v1, v8}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    move-object v1, v7

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lb0/x$a;->g:Ljava/util/List;

    if-eqz v1, :cond_3

    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v1, v8}, Lf/h/a/f/f/n/g;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/16 v16, 0x3

    move-object v1, v7

    move-object v15, v6

    move/from16 v6, v16

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_1
    move-object v15, v6

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v15, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v6, v15

    goto :goto_1

    :cond_2
    move-object v15, v6

    goto :goto_3

    :cond_3
    const/4 v15, 0x0

    :goto_3
    iget-object v2, v0, Lb0/x$a;->h:Ljava/lang/String;

    if-eqz v2, :cond_4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    move-object v1, v7

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v16, v1

    goto :goto_4

    :cond_4
    const/16 v16, 0x0

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lb0/x$a;->toString()Ljava/lang/String;

    move-result-object v17

    new-instance v1, Lb0/x;

    move-object v8, v1

    invoke-direct/range {v8 .. v17}, Lb0/x;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_5
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "host == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "scheme == null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final c()I
    .locals 4

    iget v0, p0, Lb0/x$a;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lb0/x$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v2, "scheme"

    invoke-static {v0, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0x310888    # 4.503E-39f

    if-eq v2, v3, :cond_2

    const v3, 0x5f008eb

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x1bb

    goto :goto_1

    :cond_2
    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x50

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    return v0

    :cond_4
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0
.end method

.method public final d(Ljava/lang/String;)Lb0/x$a;
    .locals 12

    if-eqz p1, :cond_0

    sget-object v11, Lb0/x;->l:Lb0/x$b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd3

    const-string v4, " \"\'<>#"

    move-object v0, v11

    move-object v1, p1

    invoke-static/range {v0 .. v10}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v11, p1}, Lb0/x$b;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lb0/x$a;->g:Ljava/util/List;

    return-object p0
.end method

.method public final e(Lb0/x;Ljava/lang/String;)Lb0/x$a;
    .locals 26

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v12, p2

    sget-object v13, Lb0/x;->l:Lb0/x$b;

    const-string v2, "input"

    invoke-static {v12, v2}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lb0/g0/c;->a:[B

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v12, v3, v2}, Lb0/g0/c;->o(Ljava/lang/String;II)I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v12, v2, v4}, Lb0/g0/c;->p(Ljava/lang/String;II)I

    move-result v14

    sub-int v4, v14, v2

    const/4 v5, 0x2

    const/16 v6, 0x3a

    const/4 v7, -0x1

    const/4 v8, 0x1

    if-ge v4, v5, :cond_0

    goto :goto_5

    :cond_0
    invoke-virtual {v12, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5a

    const/16 v9, 0x7a

    const/16 v10, 0x41

    const/16 v11, 0x61

    if-lt v4, v11, :cond_1

    if-le v4, v9, :cond_2

    :cond_1
    if-lt v4, v10, :cond_c

    if-le v4, v5, :cond_2

    goto :goto_5

    :cond_2
    move v4, v2

    :goto_0
    add-int/2addr v4, v8

    if-ge v4, v14, :cond_c

    invoke-virtual {v12, v4}, Ljava/lang/String;->charAt(I)C

    move-result v15

    if-le v11, v15, :cond_3

    goto :goto_1

    :cond_3
    if-lt v9, v15, :cond_4

    goto :goto_4

    :cond_4
    :goto_1
    if-le v10, v15, :cond_5

    goto :goto_2

    :cond_5
    if-lt v5, v15, :cond_6

    goto :goto_4

    :cond_6
    :goto_2
    const/16 v5, 0x39

    const/16 v9, 0x30

    if-le v9, v15, :cond_7

    goto :goto_3

    :cond_7
    if-lt v5, v15, :cond_8

    goto :goto_4

    :cond_8
    :goto_3
    const/16 v5, 0x2b

    if-ne v15, v5, :cond_9

    goto :goto_4

    :cond_9
    const/16 v5, 0x2d

    if-ne v15, v5, :cond_a

    goto :goto_4

    :cond_a
    const/16 v5, 0x2e

    if-ne v15, v5, :cond_b

    :goto_4
    const/16 v5, 0x5a

    const/16 v9, 0x7a

    goto :goto_0

    :cond_b
    if-ne v15, v6, :cond_c

    goto :goto_6

    :cond_c
    :goto_5
    const/4 v4, -0x1

    :goto_6
    const-string v15, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    if-eq v4, v7, :cond_f

    const-string v5, "https:"

    invoke-static {v12, v5, v2, v8}, Lx/s/m;->startsWith(Ljava/lang/String;Ljava/lang/String;IZ)Z

    move-result v5

    if-eqz v5, :cond_d

    const-string v3, "https"

    iput-object v3, v0, Lb0/x$a;->a:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x6

    goto :goto_7

    :cond_d
    const-string v5, "http:"

    invoke-static {v12, v5, v2, v8}, Lx/s/m;->startsWith(Ljava/lang/String;Ljava/lang/String;IZ)Z

    move-result v5

    if-eqz v5, :cond_e

    const-string v3, "http"

    iput-object v3, v0, Lb0/x$a;->a:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x5

    goto :goto_7

    :cond_e
    const-string v1, "Expected URL scheme \'http\' or \'https\' but was \'"

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v15}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_f
    if-eqz v1, :cond_3b

    iget-object v3, v1, Lb0/x;->b:Ljava/lang/String;

    iput-object v3, v0, Lb0/x$a;->a:Ljava/lang/String;

    :goto_7
    const/4 v3, 0x0

    move v4, v2

    :goto_8
    const/16 v5, 0x2f

    const/16 v6, 0x5c

    if-ge v4, v14, :cond_11

    invoke-virtual {v12, v4}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-eq v9, v6, :cond_10

    if-ne v9, v5, :cond_11

    :cond_10
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_11
    const/16 v4, 0x3f

    const/16 v9, 0x23

    const/4 v10, 0x2

    if-ge v3, v10, :cond_15

    if-eqz v1, :cond_15

    iget-object v10, v1, Lb0/x;->b:Ljava/lang/String;

    iget-object v11, v0, Lb0/x$a;->a:Ljava/lang/String;

    invoke-static {v10, v11}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    xor-int/2addr v8, v10

    if-eqz v8, :cond_12

    goto :goto_9

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lb0/x;->e()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lb0/x$a;->b:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lb0/x;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lb0/x$a;->c:Ljava/lang/String;

    iget-object v3, v1, Lb0/x;->e:Ljava/lang/String;

    iput-object v3, v0, Lb0/x$a;->d:Ljava/lang/String;

    iget v3, v1, Lb0/x;->f:I

    iput v3, v0, Lb0/x$a;->e:I

    iget-object v3, v0, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v3, v0, Lb0/x$a;->f:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lb0/x;->c()Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eq v2, v14, :cond_13

    invoke-virtual {v12, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v9, :cond_14

    :cond_13
    invoke-virtual/range {p1 .. p1}, Lb0/x;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lb0/x$a;->d(Ljava/lang/String;)Lb0/x$a;

    :cond_14
    const/4 v1, 0x1

    move/from16 v18, v14

    goto/16 :goto_16

    :cond_15
    :goto_9
    add-int/2addr v2, v3

    const/4 v1, 0x0

    const/4 v3, 0x0

    move v11, v2

    const/16 v16, 0x0

    const/16 v17, 0x0

    :goto_a
    const-string v1, "@/\\?#"

    invoke-static {v12, v1, v11, v14}, Lb0/g0/c;->g(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v10

    if-eq v10, v14, :cond_16

    invoke-virtual {v12, v10}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_b

    :cond_16
    const/4 v1, -0x1

    :goto_b
    if-eq v1, v7, :cond_1b

    if-eq v1, v9, :cond_1b

    if-eq v1, v5, :cond_1b

    if-eq v1, v6, :cond_1b

    if-eq v1, v4, :cond_1b

    const/16 v2, 0x40

    if-eq v1, v2, :cond_17

    move/from16 v18, v14

    goto/16 :goto_e

    :cond_17
    const-string v9, "%40"

    if-nez v16, :cond_1a

    const/16 v1, 0x3a

    invoke-static {v12, v1, v11, v10}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v8

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0xf0

    const-string v5, " \"\':;<=>@[]^`{}|/\\?#"

    move-object v1, v13

    move-object/from16 v2, p2

    move v3, v11

    move v4, v8

    move v11, v8

    move/from16 v8, v18

    move-object/from16 v22, v9

    move/from16 v9, v19

    move/from16 v23, v10

    move-object/from16 v10, v20

    move/from16 v18, v14

    move v14, v11

    move/from16 v11, v21

    invoke-static/range {v1 .. v11}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v17, :cond_18

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Lb0/x$a;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v3, v22

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_18
    iput-object v1, v0, Lb0/x$a;->b:Ljava/lang/String;

    move/from16 v11, v23

    if-eq v14, v11, :cond_19

    add-int/lit8 v3, v14, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v14, 0xf0

    const-string v5, " \"\':;<=>@[]^`{}|/\\?#"

    move-object v1, v13

    move-object/from16 v2, p2

    move v4, v11

    move/from16 v23, v11

    move v11, v14

    invoke-static/range {v1 .. v11}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lb0/x$a;->c:Ljava/lang/String;

    const/16 v16, 0x1

    goto :goto_c

    :cond_19
    move/from16 v23, v11

    :goto_c
    const/16 v17, 0x1

    goto :goto_d

    :cond_1a
    move-object v3, v9

    move/from16 v23, v10

    move/from16 v18, v14

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v0, Lb0/x$a;->c:Ljava/lang/String;

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v19, 0xf0

    const-string v5, " \"\':;<=>@[]^`{}|/\\?#"

    move-object v1, v13

    move-object/from16 v2, p2

    move v3, v11

    move/from16 v4, v23

    move/from16 v11, v19

    invoke-static/range {v1 .. v11}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lb0/x$a;->c:Ljava/lang/String;

    :goto_d
    move/from16 v14, v23

    add-int/lit8 v11, v14, 0x1

    :goto_e
    const/16 v9, 0x23

    const/16 v4, 0x3f

    const/16 v6, 0x5c

    const/16 v5, 0x2f

    const/4 v7, -0x1

    move/from16 v14, v18

    goto/16 :goto_a

    :cond_1b
    move/from16 v18, v14

    move v14, v10

    move v10, v11

    :goto_f
    if-ge v10, v14, :cond_1e

    invoke-virtual {v12, v10}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3a

    if-eq v1, v2, :cond_1f

    const/16 v2, 0x5b

    if-eq v1, v2, :cond_1c

    goto :goto_10

    :cond_1c
    add-int/lit8 v10, v10, 0x1

    if-ge v10, v14, :cond_1d

    invoke-virtual {v12, v10}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x5d

    if-ne v1, v2, :cond_1c

    :cond_1d
    :goto_10
    add-int/lit8 v10, v10, 0x1

    goto :goto_f

    :cond_1e
    move v10, v14

    :cond_1f
    add-int/lit8 v9, v10, 0x1

    const/16 v7, 0x22

    if-ge v9, v14, :cond_24

    const/4 v5, 0x0

    const/4 v6, 0x4

    move-object v1, v13

    move-object/from16 v2, p2

    move v3, v11

    move v4, v10

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ly/a/g0;->R(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lb0/x$a;->d:Ljava/lang/String;

    :try_start_0
    const-string v5, ""
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0xf8

    move-object v1, v13

    move-object/from16 v2, p2

    move v3, v9

    move v4, v14

    move/from16 v24, v9

    move/from16 v9, v16

    move/from16 p1, v10

    move-object/from16 v10, v17

    move/from16 v16, v11

    move/from16 v11, v19

    :try_start_1
    invoke-static/range {v1 .. v11}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    const v2, 0xffff

    const/4 v3, 0x1

    if-le v3, v1, :cond_20

    goto :goto_11

    :cond_20
    if-lt v2, v1, :cond_21

    goto :goto_12

    :catch_0
    move/from16 v24, v9

    move/from16 p1, v10

    move/from16 v16, v11

    :catch_1
    const/4 v3, 0x1

    :cond_21
    :goto_11
    const/4 v1, -0x1

    :goto_12
    iput v1, v0, Lb0/x$a;->e:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_22

    const/4 v1, 0x1

    goto :goto_13

    :cond_22
    const/4 v1, 0x0

    :goto_13
    if-eqz v1, :cond_23

    const/16 v7, 0x22

    move v1, v3

    goto :goto_14

    :cond_23
    const-string v1, "Invalid URL port: \""

    invoke-static {v1}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v10, v24

    invoke-virtual {v12, v10, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v15}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_24
    move/from16 p1, v10

    move/from16 v16, v11

    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x4

    move-object v1, v13

    move-object/from16 v2, p2

    move/from16 v3, v16

    move/from16 v4, p1

    invoke-static/range {v1 .. v6}, Lb0/x$b;->d(Lb0/x$b;Ljava/lang/String;IIZI)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ly/a/g0;->R(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lb0/x$a;->d:Ljava/lang/String;

    iget-object v1, v0, Lb0/x$a;->a:Ljava/lang/String;

    if-eqz v1, :cond_3a

    invoke-virtual {v13, v1}, Lb0/x$b;->b(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lb0/x$a;->e:I

    const/4 v1, 0x1

    :goto_14
    iget-object v2, v0, Lb0/x$a;->d:Ljava/lang/String;

    if-eqz v2, :cond_25

    const/4 v2, 0x1

    goto :goto_15

    :cond_25
    const/4 v2, 0x0

    :goto_15
    if-eqz v2, :cond_39

    move v2, v14

    :goto_16
    const-string v3, "?#"

    move/from16 v4, v18

    invoke-static {v12, v3, v2, v4}, Lb0/g0/c;->g(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v3

    if-ne v2, v3, :cond_26

    move-object v15, v0

    move v14, v4

    move-object v11, v12

    goto/16 :goto_23

    :cond_26
    invoke-virtual {v12, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const-string v6, ""

    const/16 v7, 0x2f

    if-eq v5, v7, :cond_28

    const/16 v7, 0x5c

    if-ne v5, v7, :cond_27

    goto :goto_17

    :cond_27
    iget-object v5, v0, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v1

    invoke-interface {v5, v7, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-object v8, v0

    move-object v9, v8

    move v5, v3

    move v10, v4

    move-object v7, v6

    move-object v1, v12

    move-object v4, v13

    move v6, v5

    move-object v3, v1

    goto :goto_19

    :cond_28
    :goto_17
    iget-object v5, v0, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v5, v0, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v8, v0

    move-object v9, v8

    move v5, v3

    move v14, v4

    move-object v7, v6

    move v4, v2

    move v6, v5

    move-object v2, v12

    move v3, v1

    move-object v1, v2

    :goto_18
    add-int/2addr v3, v4

    move-object v4, v13

    move v10, v14

    move-object/from16 v25, v12

    move-object v12, v2

    move v2, v3

    move-object/from16 v3, v25

    :cond_29
    :goto_19
    move v15, v2

    if-ge v15, v5, :cond_36

    const-string v2, "/\\"

    invoke-static {v3, v2, v15, v5}, Lb0/g0/c;->g(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v2

    if-ge v2, v5, :cond_2a

    const/4 v11, 0x1

    goto :goto_1a

    :cond_2a
    const/4 v11, 0x0

    :goto_1a
    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0xf0

    const-string v17, " \"<>^`{}|/\\?#"

    const/4 v14, 0x1

    move-object v13, v4

    const/4 v0, 0x1

    move-object v14, v3

    move/from16 v16, v2

    invoke-static/range {v13 .. v23}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "."

    invoke-static {v13, v14}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2c

    const-string v14, "%2e"

    invoke-static {v13, v14, v0}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_2b

    goto :goto_1b

    :cond_2b
    const/4 v14, 0x0

    goto :goto_1c

    :cond_2c
    :goto_1b
    const/4 v14, 0x1

    :goto_1c
    if-eqz v14, :cond_2d

    goto/16 :goto_22

    :cond_2d
    const-string v14, ".."

    invoke-static {v13, v14}, Lx/m/c/j;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2f

    const-string v14, "%2e."

    invoke-static {v13, v14, v0}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_2f

    const-string v14, ".%2e"

    invoke-static {v13, v14, v0}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_2f

    const-string v14, "%2e%2e"

    invoke-static {v13, v14, v0}, Lx/s/m;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_2e

    goto :goto_1d

    :cond_2e
    const/4 v14, 0x0

    goto :goto_1e

    :cond_2f
    :goto_1d
    const/4 v14, 0x1

    :goto_1e
    if-eqz v14, :cond_32

    iget-object v13, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    sub-int/2addr v14, v0

    invoke-interface {v13, v14}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    if-nez v13, :cond_30

    const/4 v13, 0x1

    goto :goto_1f

    :cond_30
    const/4 v13, 0x0

    :goto_1f
    if-eqz v13, :cond_31

    iget-object v13, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/Collection;->isEmpty()Z

    move-result v13

    xor-int/2addr v13, v0

    if-eqz v13, :cond_31

    iget-object v13, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v14

    sub-int/2addr v14, v0

    invoke-interface {v13, v14, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_22

    :cond_31
    iget-object v0, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_22

    :cond_32
    iget-object v14, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    sub-int/2addr v15, v0

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/CharSequence;

    invoke-interface {v14}, Ljava/lang/CharSequence;->length()I

    move-result v14

    if-nez v14, :cond_33

    const/4 v14, 0x1

    goto :goto_20

    :cond_33
    const/4 v14, 0x0

    :goto_20
    if-eqz v14, :cond_34

    iget-object v14, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    sub-int/2addr v15, v0

    invoke-interface {v14, v15, v13}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_21

    :cond_34
    iget-object v0, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_21
    if-eqz v11, :cond_35

    iget-object v0, v8, Lb0/x$a;->f:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_35
    :goto_22
    const/4 v0, 0x1

    move-object/from16 v0, p0

    if-eqz v11, :cond_29

    move-object v13, v4

    move v14, v10

    move v4, v2

    move-object v2, v12

    move-object v12, v3

    const/4 v3, 0x1

    goto/16 :goto_18

    :cond_36
    move-object v11, v1

    move-object v13, v4

    move v3, v6

    move-object v15, v9

    move v14, v10

    :goto_23
    if-ge v3, v14, :cond_37

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_37

    const/16 v0, 0x23

    invoke-static {v12, v0, v3, v14}, Lb0/g0/c;->f(Ljava/lang/String;CII)I

    move-result v16

    add-int/lit8 v2, v3, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd0

    const-string v4, " \"\'<>#"

    const/16 v17, 0x23

    move-object v0, v13

    move-object v1, v11

    move/from16 v3, v16

    invoke-static/range {v0 .. v10}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lb0/x$b;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v15, Lb0/x$a;->g:Ljava/util/List;

    goto :goto_24

    :cond_37
    const/16 v17, 0x23

    :goto_24
    const/16 v0, 0x23

    if-ge v3, v14, :cond_38

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v0, :cond_38

    add-int/lit8 v2, v3, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/16 v10, 0xb0

    const-string v4, ""

    move-object v0, v13

    move-object v1, v11

    move v3, v14

    invoke-static/range {v0 .. v10}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v15, Lb0/x$a;->h:Ljava/lang/String;

    :cond_38
    return-object v15

    :cond_39
    const-string v0, "Invalid URL host: \""

    invoke-static {v0}, Lf/e/c/a/a;->G(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v14, p1

    move/from16 v2, v16

    invoke-virtual {v12, v2, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v15}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3a
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    const/4 v0, 0x0

    throw v0

    :cond_3b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected URL scheme \'http\' or \'https\' but no colon was found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f(Ljava/lang/String;)Lb0/x$a;
    .locals 12

    const-string v0, "password"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lb0/x;->l:Lb0/x$b;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xfb

    move-object v2, p1

    invoke-static/range {v1 .. v11}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lb0/x$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lb0/x$a;
    .locals 12

    const-string v0, "username"

    invoke-static {p1, v0}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lb0/x;->l:Lb0/x$b;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xfb

    move-object v2, p1

    invoke-static/range {v1 .. v11}, Lb0/x$b;->a(Lb0/x$b;Ljava/lang/String;IILjava/lang/String;ZZZZLjava/nio/charset/Charset;I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lb0/x$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lb0/x$a;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    iget-object v1, p0, Lb0/x$a;->b:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    const/16 v4, 0x3a

    if-nez v1, :cond_3

    iget-object v1, p0, Lb0/x$a;->c:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_6

    :cond_3
    iget-object v1, p0, Lb0/x$a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/x$a;->c:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_4

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/x$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_6
    iget-object v1, p0, Lb0/x$a;->d:Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v5, 0x0

    if-eqz v1, :cond_9

    if-eqz v1, :cond_8

    invoke-static {v1, v4, v3, v2}, Lx/s/r;->contains$default(Ljava/lang/CharSequence;CZI)Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/x$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_7
    iget-object v1, p0, Lb0/x$a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_8
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v5

    :cond_9
    :goto_4
    iget v1, p0, Lb0/x$a;->e:I

    const/4 v6, -0x1

    if-ne v1, v6, :cond_a

    iget-object v1, p0, Lb0/x$a;->a:Ljava/lang/String;

    if-eqz v1, :cond_10

    :cond_a
    invoke-virtual {p0}, Lb0/x$a;->c()I

    move-result v1

    iget-object v7, p0, Lb0/x$a;->a:Ljava/lang/String;

    if-eqz v7, :cond_f

    if-eqz v7, :cond_e

    const-string v8, "scheme"

    invoke-static {v7, v8}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    const v9, 0x310888    # 4.503E-39f

    if-eq v8, v9, :cond_c

    const v9, 0x5f008eb

    if-eq v8, v9, :cond_b

    goto :goto_5

    :cond_b
    const-string v8, "https"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const/16 v6, 0x1bb

    goto :goto_5

    :cond_c
    const-string v8, "http"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const/16 v6, 0x50

    :cond_d
    :goto_5
    if-eq v1, v6, :cond_10

    goto :goto_6

    :cond_e
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v5

    :cond_f
    :goto_6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_10
    iget-object v1, p0, Lb0/x$a;->f:Ljava/util/List;

    const-string v4, "$this$toPathString"

    invoke-static {v1, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "out"

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x0

    :goto_7
    if-ge v7, v6, :cond_11

    const/16 v8, 0x2f

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_11
    iget-object v1, p0, Lb0/x$a;->g:Ljava/util/List;

    if-eqz v1, :cond_16

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/x$a;->g:Ljava/util/List;

    if-eqz v1, :cond_15

    const-string v5, "$this$toQueryString"

    invoke-static {v1, v5}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v4}, Lx/m/c/j;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v4}, Lx/p/e;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    invoke-static {v3, v2}, Lx/p/e;->step(Lkotlin/ranges/IntProgression;I)Lkotlin/ranges/IntProgression;

    move-result-object v2

    iget v3, v2, Lkotlin/ranges/IntProgression;->d:I

    iget v4, v2, Lkotlin/ranges/IntProgression;->e:I

    iget v2, v2, Lkotlin/ranges/IntProgression;->f:I

    if-ltz v2, :cond_12

    if-gt v3, v4, :cond_16

    goto :goto_8

    :cond_12
    if-lt v3, v4, :cond_16

    :goto_8
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    add-int/lit8 v6, v3, 0x1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-lez v3, :cond_13

    const/16 v7, 0x26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_13
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v6, :cond_14

    const/16 v5, 0x3d

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_14
    if-eq v3, v4, :cond_16

    add-int/2addr v3, v2

    goto :goto_8

    :cond_15
    invoke-static {}, Lx/m/c/j;->throwNpe()V

    throw v5

    :cond_16
    iget-object v1, p0, Lb0/x$a;->h:Ljava/lang/String;

    if-eqz v1, :cond_17

    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lb0/x$a;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lx/m/c/j;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
