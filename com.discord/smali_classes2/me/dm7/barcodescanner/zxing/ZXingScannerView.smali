.class public Lme/dm7/barcodescanner/zxing/ZXingScannerView;
.super La0/a/a/a/a;
.source "ZXingScannerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;
    }
.end annotation


# static fields
.field public static final A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/f/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public x:Lf/h/f/g;

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf/h/f/a;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->A:Ljava/util/List;

    sget-object v1, Lf/h/f/a;->d:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->e:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->f:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->g:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->h:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->i:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->j:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->k:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->l:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->m:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->n:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->o:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->p:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->q:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->r:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->s:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lf/h/f/a;->t:Lf/h/f/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, La0/a/a/a/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->c()V

    return-void
.end method


# virtual methods
.method public b([BII)Lf/h/f/h;
    .locals 12

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La0/a/a/a/a;->g:Landroid/graphics/Rect;

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v0}, La0/a/a/a/f;->getFramingRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v2, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v2}, La0/a/a/a/f;->getWidth()I

    move-result v2

    iget-object v3, p0, La0/a/a/a/a;->f:La0/a/a/a/f;

    invoke-interface {v3}, La0/a/a/a/f;->getHeight()I

    move-result v3

    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    if-ge p2, v2, :cond_1

    iget v0, v4, Landroid/graphics/Rect;->left:I

    mul-int v0, v0, p2

    div-int/2addr v0, v2

    iput v0, v4, Landroid/graphics/Rect;->left:I

    iget v0, v4, Landroid/graphics/Rect;->right:I

    mul-int v0, v0, p2

    div-int/2addr v0, v2

    iput v0, v4, Landroid/graphics/Rect;->right:I

    :cond_1
    if-ge p3, v3, :cond_2

    iget v0, v4, Landroid/graphics/Rect;->top:I

    mul-int v0, v0, p3

    div-int/2addr v0, v3

    iput v0, v4, Landroid/graphics/Rect;->top:I

    iget v0, v4, Landroid/graphics/Rect;->bottom:I

    mul-int v0, v0, p3

    div-int/2addr v0, v3

    iput v0, v4, Landroid/graphics/Rect;->bottom:I

    :cond_2
    iput-object v4, p0, La0/a/a/a/a;->g:Landroid/graphics/Rect;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_3
    :goto_0
    monitor-exit p0

    move-object v0, v1

    goto :goto_2

    :cond_4
    :goto_1
    :try_start_1
    iget-object v0, p0, La0/a/a/a/a;->g:Landroid/graphics/Rect;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    :goto_2
    if-nez v0, :cond_5

    return-object v1

    :cond_5
    :try_start_2
    new-instance v11, Lf/h/f/h;

    iget v6, v0, Landroid/graphics/Rect;->left:I

    iget v7, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v9

    const/4 v10, 0x0

    move-object v2, v11

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v2 .. v10}, Lf/h/f/h;-><init>([BIIIIIIZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v11

    :catch_0
    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final c()V
    .locals 3

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lf/h/f/d;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v1, Lf/h/f/d;->f:Lf/h/f/d;

    invoke-virtual {p0}, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->getFormats()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lf/h/f/g;

    invoke-direct {v1}, Lf/h/f/g;-><init>()V

    iput-object v1, p0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v1, v0}, Lf/h/f/g;->c(Ljava/util/Map;)V

    return-void
.end method

.method public getFormats()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lf/h/f/a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->y:Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->A:Ljava/util/List;

    :cond_0
    return-object v0
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 16

    move-object/from16 v1, p0

    iget-object v0, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->z:Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v0

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Ly/a/g0;->q(Landroid/content/Context;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    invoke-virtual/range {p0 .. p0}, La0/a/a/a/a;->getRotationCount()I

    move-result v3

    const/4 v5, 0x3

    if-eq v3, v4, :cond_2

    if-ne v3, v5, :cond_1

    goto :goto_0

    :cond_1
    move v15, v2

    move v2, v0

    move v0, v15

    :cond_2
    :goto_0
    invoke-virtual/range {p2 .. p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v3

    iget v6, v3, Landroid/hardware/Camera$Size;->width:I

    iget v3, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual/range {p0 .. p0}, La0/a/a/a/a;->getRotationCount()I

    move-result v7

    if-eq v7, v4, :cond_4

    if-ne v7, v5, :cond_3

    goto :goto_1

    :cond_3
    move v15, v2

    move v2, v0

    move v0, v15

    goto :goto_5

    :cond_4
    :goto_1
    const/4 v5, 0x0

    move v8, v3

    const/4 v9, 0x0

    move-object/from16 v3, p1

    :goto_2
    if-ge v9, v7, :cond_8

    array-length v10, v3

    new-array v10, v10, [B

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v8, :cond_6

    const/4 v12, 0x0

    :goto_4
    if-ge v12, v6, :cond_5

    mul-int v13, v12, v8

    add-int/2addr v13, v8

    sub-int/2addr v13, v11

    sub-int/2addr v13, v4

    mul-int v14, v11, v6

    add-int/2addr v14, v12

    aget-byte v14, v3, v14

    aput-byte v14, v10, v13

    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_6
    add-int/lit8 v9, v9, 0x1

    move-object v3, v10

    move v15, v8

    move v8, v6

    move v6, v15

    goto :goto_2

    :cond_7
    :goto_5
    move-object/from16 v3, p1

    move v15, v2

    move v2, v0

    move v0, v15

    :cond_8
    invoke-virtual {v1, v3, v0, v2}, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->b([BII)Lf/h/f/h;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_c

    new-instance v3, Lf/h/f/c;

    new-instance v4, Lf/h/f/n/h;

    invoke-direct {v4, v0}, Lf/h/f/n/h;-><init>(Lf/h/f/f;)V

    invoke-direct {v3, v4}, Lf/h/f/c;-><init>(Lf/h/f/b;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v4, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    iget-object v5, v4, Lf/h/f/g;->b:[Lf/h/f/i;

    if-nez v5, :cond_9

    invoke-virtual {v4, v2}, Lf/h/f/g;->c(Ljava/util/Map;)V

    :cond_9
    invoke-virtual {v4, v3}, Lf/h/f/g;->b(Lf/h/f/c;)Lcom/google/zxing/Result;

    move-result-object v3
    :try_end_1
    .catch Lcom/google/zxing/ReaderException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v4, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v4}, Lf/h/f/g;->reset()V

    goto :goto_6

    :catchall_0
    move-exception v0

    iget-object v2, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v2}, Lf/h/f/g;->reset()V

    throw v0

    :catch_0
    iget-object v3, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v3}, Lf/h/f/g;->reset()V

    move-object v3, v2

    :goto_6
    if-nez v3, :cond_b

    new-instance v4, Lf/h/f/e;

    invoke-direct {v4, v0}, Lf/h/f/e;-><init>(Lf/h/f/f;)V

    new-instance v0, Lf/h/f/c;

    new-instance v5, Lf/h/f/n/h;

    invoke-direct {v5, v4}, Lf/h/f/n/h;-><init>(Lf/h/f/f;)V

    invoke-direct {v0, v5}, Lf/h/f/c;-><init>(Lf/h/f/b;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    iget-object v4, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    iget-object v5, v4, Lf/h/f/g;->b:[Lf/h/f/i;

    if-nez v5, :cond_a

    invoke-virtual {v4, v2}, Lf/h/f/g;->c(Ljava/util/Map;)V

    :cond_a
    invoke-virtual {v4, v0}, Lf/h/f/g;->b(Lf/h/f/c;)Lcom/google/zxing/Result;

    move-result-object v2
    :try_end_3
    .catch Lcom/google/zxing/NotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v0, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v0}, Lf/h/f/g;->reset()V

    goto :goto_7

    :catchall_1
    move-exception v0

    iget-object v2, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v2}, Lf/h/f/g;->reset()V

    throw v0

    :catch_1
    iget-object v0, v1, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->x:Lf/h/f/g;

    invoke-virtual {v0}, Lf/h/f/g;->reset()V

    :cond_b
    move-object v2, v3

    :cond_c
    :goto_7
    if-eqz v2, :cond_d

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lme/dm7/barcodescanner/zxing/ZXingScannerView$a;

    invoke-direct {v3, v1, v2}, Lme/dm7/barcodescanner/zxing/ZXingScannerView$a;-><init>(Lme/dm7/barcodescanner/zxing/ZXingScannerView;Lcom/google/zxing/Result;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_8

    :cond_d
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_8

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ZXingScannerView"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_8
    return-void
.end method

.method public setFormats(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf/h/f/a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->y:Ljava/util/List;

    invoke-virtual {p0}, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->c()V

    return-void
.end method

.method public setResultHandler(Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;)V
    .locals 0

    iput-object p1, p0, Lme/dm7/barcodescanner/zxing/ZXingScannerView;->z:Lme/dm7/barcodescanner/zxing/ZXingScannerView$b;

    return-void
.end method
