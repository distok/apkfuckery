.class public final Lrx/subscriptions/SerialSubscription;
.super Ljava/lang/Object;
.source "SerialSubscription.java"

# interfaces
.implements Lrx/Subscription;


# instance fields
.field public final d:Lg0/l/d/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lg0/l/d/a;

    invoke-direct {v0}, Lg0/l/d/a;-><init>()V

    iput-object v0, p0, Lrx/subscriptions/SerialSubscription;->d:Lg0/l/d/a;

    return-void
.end method


# virtual methods
.method public a(Lrx/Subscription;)V
    .locals 3

    if-eqz p1, :cond_3

    iget-object v0, p0, Lrx/subscriptions/SerialSubscription;->d:Lg0/l/d/a;

    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/Subscription;

    sget-object v2, Lg0/l/d/b;->d:Lg0/l/d/b;

    if-ne v1, v2, :cond_1

    invoke-interface {p1}, Lrx/Subscription;->unsubscribe()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lrx/Subscription;->unsubscribe()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Subscription can not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isUnsubscribed()Z
    .locals 1

    iget-object v0, p0, Lrx/subscriptions/SerialSubscription;->d:Lg0/l/d/a;

    invoke-virtual {v0}, Lg0/l/d/a;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public unsubscribe()V
    .locals 1

    iget-object v0, p0, Lrx/subscriptions/SerialSubscription;->d:Lg0/l/d/a;

    invoke-virtual {v0}, Lg0/l/d/a;->unsubscribe()V

    return-void
.end method
