.class public final Lrx/subjects/BehaviorSubject;
.super Lrx/subjects/Subject;
.source "BehaviorSubject.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/subjects/Subject<",
        "TT;TT;>;"
    }
.end annotation


# static fields
.field public static final f:[Ljava/lang/Object;


# instance fields
.field public final e:Lg0/q/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lg0/q/c<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Lrx/subjects/BehaviorSubject;->f:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lrx/Observable$a;Lg0/q/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable$a<",
            "TT;>;",
            "Lg0/q/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lrx/subjects/Subject;-><init>(Lrx/Observable$a;)V

    iput-object p2, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    return-void
.end method

.method public static f0()Lrx/subjects/BehaviorSubject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/subjects/BehaviorSubject<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lrx/subjects/BehaviorSubject;->h0(Ljava/lang/Object;Z)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    return-object v0
.end method

.method public static g0(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lrx/subjects/BehaviorSubject<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lrx/subjects/BehaviorSubject;->h0(Ljava/lang/Object;Z)Lrx/subjects/BehaviorSubject;

    move-result-object p0

    return-object p0
.end method

.method public static h0(Ljava/lang/Object;Z)Lrx/subjects/BehaviorSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;Z)",
            "Lrx/subjects/BehaviorSubject<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lg0/q/c;

    invoke-direct {v0}, Lg0/q/c;-><init>()V

    if-eqz p1, :cond_1

    if-nez p0, :cond_0

    sget-object p0, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_0
    iput-object p0, v0, Lg0/q/c;->latest:Ljava/lang/Object;

    :cond_1
    new-instance p0, Lrx/subjects/BehaviorSubject$a;

    invoke-direct {p0, v0}, Lrx/subjects/BehaviorSubject$a;-><init>(Lg0/q/c;)V

    iput-object p0, v0, Lg0/q/c;->onAdded:Lrx/functions/Action1;

    iput-object p0, v0, Lg0/q/c;->onTerminated:Lrx/functions/Action1;

    new-instance p0, Lrx/subjects/BehaviorSubject;

    invoke-direct {p0, v0, v0}, Lrx/subjects/BehaviorSubject;-><init>(Lrx/Observable$a;Lg0/q/c;)V

    return-object p0
.end method


# virtual methods
.method public i0()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-object v0, v0, Lg0/q/c;->latest:Ljava/lang/Object;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lg0/l/a/h$c;

    if-nez v1, :cond_0

    invoke-static {v0}, Lg0/l/a/h;->c(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v0}, Lg0/l/a/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCompleted()V
    .locals 5

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-object v0, v0, Lg0/q/c;->latest:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-boolean v0, v0, Lg0/q/c;->active:Z

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lg0/l/a/h;->a:Ljava/lang/Object;

    iget-object v1, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    invoke-virtual {v1, v0}, Lg0/q/c;->b(Ljava/lang/Object;)[Lg0/q/c$b;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    invoke-virtual {v4, v0}, Lg0/q/c$b;->a(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 5

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-object v0, v0, Lg0/q/c;->latest:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-boolean v0, v0, Lg0/q/c;->active:Z

    if-eqz v0, :cond_3

    :cond_0
    new-instance v0, Lg0/l/a/h$c;

    invoke-direct {v0, p1}, Lg0/l/a/h$c;-><init>(Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    iget-object v1, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    invoke-virtual {v1, v0}, Lg0/q/c;->b(Ljava/lang/Object;)[Lg0/q/c$b;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    :try_start_0
    invoke-virtual {v4, v0}, Lg0/q/c$b;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    if-nez p1, :cond_1

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-static {p1}, Ly/a/g0;->O(Ljava/util/List;)V

    :cond_3
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-object v0, v0, Lg0/q/c;->latest:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iget-boolean v0, v0, Lg0/q/c;->active:Z

    if-eqz v0, :cond_2

    :cond_0
    if-nez p1, :cond_1

    sget-object p1, Lg0/l/a/h;->b:Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lrx/subjects/BehaviorSubject;->e:Lg0/q/c;

    iput-object p1, v0, Lg0/q/c;->latest:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg0/q/c$a;

    iget-object v0, v0, Lg0/q/c$a;->b:[Lg0/q/c$b;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    invoke-virtual {v3, p1}, Lg0/q/c$b;->a(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
