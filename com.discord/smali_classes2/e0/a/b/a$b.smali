.class public final Le0/a/b/a$b;
.super Ljava/lang/Object;
.source "ApngDecoder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le0/a/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# direct methods
.method public constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Le0/a/b/a$b;Landroid/content/Context;Ljava/io/InputStream;FLandroid/graphics/Bitmap$Config;I)Landroid/graphics/drawable/Drawable;
    .locals 40

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    and-int/lit8 v2, p5, 0x4

    if-eqz v2, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    move/from16 v2, p3

    :goto_0
    const/16 v3, 0x8

    and-int/lit8 v4, p5, 0x8

    if-eqz v4, :cond_1

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    sget-object v5, Le0/a/c/a;->d:Le0/a/c/a;

    const-string v6, "context"

    move-object/from16 v7, p1

    invoke-static {v7, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "inStream"

    invoke-static {v1, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "config"

    invoke-static {v4, v6}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-array v1, v3, [B

    invoke-virtual {v6, v3}, Ljava/io/BufferedInputStream;->mark(I)V

    invoke-virtual {v6, v1}, Ljava/io/BufferedInputStream;->read([B)I

    sget-object v3, Le0/a/c/c;->j:Le0/a/c/c;

    const-string v3, "byteArray"

    invoke-static {v1, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Le0/a/c/c;->c()[B

    move-result-object v8

    invoke-static {v1, v8}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_25

    sget-object v1, Le0/a/c/b;->d:Le0/a/c/b;

    new-instance v8, Le0/a/a/a;

    invoke-direct {v8}, Le0/a/a/a;-><init>()V

    new-instance v9, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v9}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    const/4 v10, 0x4

    new-array v10, v10, [B

    const/4 v11, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/high16 v14, -0x40800000    # -1.0f

    const/4 v15, 0x0

    const/16 v16, -0x1

    const/16 v17, 0x0

    const/16 v18, -0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 p3, v1

    move/from16 v16, v2

    move-object/from16 p2, v3

    move-object/from16 p4, v9

    move-object/from16 v1, v19

    const/high16 p5, -0x40800000    # -1.0f

    const/4 v3, 0x0

    const/4 v7, -0x1

    move-object v2, v0

    move-object v9, v5

    const/4 v0, -0x1

    :goto_2
    invoke-virtual {v6, v10}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v14

    if-ne v14, v11, :cond_2

    move-object/from16 v1, p4

    move-object/from16 v23, v6

    goto/16 :goto_11

    :cond_2
    sget-object v11, Le0/a/c/c;->j:Le0/a/c/c;

    invoke-static {v10}, Le0/a/c/c;->d([B)I

    move-result v11

    add-int/lit8 v11, v11, 0x8

    new-array v14, v11, [B

    move-object/from16 v17, v4

    invoke-virtual {v6, v14}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4

    move/from16 v18, v4

    const-string v4, "$this$plus"

    invoke-static {v10, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "elements"

    invoke-static {v14, v4}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    add-int/lit8 v4, v11, 0x4

    invoke-static {v10, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    move-object/from16 v19, v10

    const/4 v10, 0x0

    move-object/from16 v23, v6

    const/4 v6, 0x4

    invoke-static {v14, v10, v4, v6, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v10, "result"

    invoke-static {v4, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v10, v4

    sub-int/2addr v10, v6

    array-length v11, v4

    invoke-static {v4, v10, v11}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v10

    invoke-static {v10}, Le0/a/c/c;->d([B)I

    move-result v10

    new-instance v11, Ljava/util/zip/CRC32;

    invoke-direct {v11}, Ljava/util/zip/CRC32;-><init>()V

    array-length v14, v4

    sub-int/2addr v14, v6

    invoke-static {v4, v6, v14}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v6

    invoke-virtual {v11, v6}, Ljava/util/zip/CRC32;->update([B)V

    move v14, v7

    invoke-virtual {v11}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    long-to-int v7, v6

    if-ne v10, v7, :cond_24

    const/16 v6, 0x8

    const/4 v7, 0x4

    invoke-static {v4, v7, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v6

    sget-object v7, Le0/a/c/c;->b:Lkotlin/Lazy;

    invoke-interface {v7}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    invoke-static {v6, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v7

    const-string v10, "btm"

    const-string v11, "decoded"

    if-eqz v7, :cond_f

    if-nez v15, :cond_8

    if-eqz v1, :cond_3

    sget-object v0, Le0/a/b/a;->a:Ljava/util/List;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v6

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v7

    array-length v7, v7

    const/4 v9, 0x0

    invoke-virtual {v0, v6, v9, v7}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v6

    invoke-static {v6}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    long-to-int v0, v6

    invoke-static {v0}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v0

    invoke-static {v1, v0}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_3
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Le0/a/a/b;

    invoke-direct {v0}, Le0/a/a/b;-><init>()V

    invoke-virtual {v0, v4}, Le0/a/a/b;->a([B)V

    iget v4, v0, Le0/a/a/b;->c:F

    iget v6, v0, Le0/a/a/b;->e:I

    iget v7, v0, Le0/a/a/b;->d:I

    iget-object v9, v0, Le0/a/a/b;->f:Le0/a/c/a;

    iget-object v10, v0, Le0/a/a/b;->g:Le0/a/c/b;

    iget v11, v0, Le0/a/a/b;->a:I

    iget v0, v0, Le0/a/a/b;->b:I

    add-int v14, v7, v11

    if-gt v14, v3, :cond_7

    add-int v14, v6, v0

    if-gt v14, v13, :cond_6

    invoke-static {}, Le0/a/c/c;->c()[B

    move-result-object v14

    invoke-static {v14}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v14

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v2, v8, v11, v0}, Le0/a/b/a$b;->b(Le0/a/a/a;II)[B

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    if-eqz v21, :cond_4

    invoke-static/range {v21 .. v21}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_4
    if-eqz v22, :cond_5

    invoke-static/range {v22 .. v22}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    move-object/from16 v0, p2

    move-object/from16 v32, v5

    move v14, v7

    move-object v7, v8

    move-object/from16 v5, p0

    move v8, v4

    move-object v4, v1

    move-object/from16 v1, p4

    move/from16 v39, v3

    move-object v3, v2

    move-object/from16 v2, v17

    move/from16 v17, v6

    move/from16 v6, v39

    goto/16 :goto_d

    :cond_6
    new-instance v0, Loupson/apng/exceptions/BadApng;

    const-string v1, "`yOffset` + `height` must be <= `IHDR` height"

    invoke-direct {v0, v1}, Loupson/apng/exceptions/BadApng;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Loupson/apng/exceptions/BadApng;

    const-string v1, "`xOffset` + `width` must be <= `IHDR` width"

    invoke-direct {v0, v1}, Loupson/apng/exceptions/BadApng;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    sget-object v2, Le0/a/b/a;->a:Ljava/util/List;

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v6

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v7

    array-length v7, v7

    move/from16 v24, v14

    const/4 v14, 0x0

    invoke-virtual {v2, v6, v14, v7}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v6

    invoke-static {v6}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    long-to-int v2, v6

    invoke-static {v2}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v2

    invoke-static {v15, v2}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v13, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v15}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object v6

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v6, v14, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v6

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {v12}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v7, v12, v15, v15, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    if-ne v9, v5, :cond_9

    move/from16 v14, v24

    int-to-float v9, v14

    int-to-float v15, v0

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v24, v1

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float v28, v9, v1

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    add-float v29, v15, v1

    invoke-virtual/range {p0 .. p0}, Le0/a/b/a$b;->c()Landroid/graphics/Paint;

    move-result-object v30

    move-object/from16 v25, v7

    move/from16 v26, v9

    move/from16 v27, v15

    invoke-virtual/range {v25 .. v30}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_3

    :cond_9
    move/from16 v14, v24

    move-object/from16 v24, v1

    :goto_3
    int-to-float v1, v14

    int-to-float v0, v0

    const/4 v9, 0x0

    invoke-virtual {v7, v6, v1, v0, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v2, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v10

    move-object/from16 v14, v17

    if-eq v10, v14, :cond_a

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v10

    invoke-virtual {v2, v14, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    goto :goto_4

    :cond_a
    move-object v10, v2

    :goto_4
    invoke-direct {v7, v9, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    div-float v9, p5, v16

    float-to-int v9, v9

    move-object/from16 v10, p4

    invoke-virtual {v10, v7, v9}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    const/4 v9, 0x1

    if-eq v7, v9, :cond_b

    const/4 v0, 0x2

    if-eq v7, v0, :cond_c

    move-object v12, v2

    goto :goto_5

    :cond_b
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v13, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v9, 0x0

    const/4 v15, 0x0

    invoke-virtual {v7, v2, v9, v9, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float v34, v1, v2

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float v35, v0, v2

    invoke-virtual/range {p0 .. p0}, Le0/a/b/a$b;->c()Landroid/graphics/Paint;

    move-result-object v36

    move-object/from16 v31, v7

    move/from16 v32, v1

    move/from16 v33, v0

    invoke-virtual/range {v31 .. v36}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_c
    :goto_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Le0/a/a/b;

    invoke-direct {v1}, Le0/a/a/b;-><init>()V

    invoke-virtual {v1, v4}, Le0/a/a/b;->a([B)V

    iget v2, v1, Le0/a/a/b;->c:F

    iget v4, v1, Le0/a/a/b;->e:I

    iget v6, v1, Le0/a/a/b;->d:I

    iget-object v9, v1, Le0/a/a/b;->f:Le0/a/c/a;

    iget-object v7, v1, Le0/a/a/b;->g:Le0/a/c/b;

    iget v11, v1, Le0/a/a/b;->a:I

    iget v1, v1, Le0/a/a/b;->b:I

    invoke-static {}, Le0/a/c/c;->c()[B

    move-result-object v15

    invoke-static {v15}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v15

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v15, p0

    invoke-virtual {v15, v8, v11, v1}, Le0/a/b/a$b;->b(Le0/a/a/a;II)[B

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    if-eqz v21, :cond_d

    invoke-static/range {v21 .. v21}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_d
    if-eqz v22, :cond_e

    invoke-static/range {v22 .. v22}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_e
    const/4 v1, -0x1

    move-object/from16 v31, v0

    move/from16 v17, v4

    move-object/from16 v32, v5

    move-object v1, v10

    move-object v5, v15

    move-object/from16 v4, v24

    const/4 v11, -0x1

    move-object/from16 v0, p2

    move-object v10, v7

    move-object v7, v8

    move v8, v2

    move-object v2, v14

    move v14, v6

    goto/16 :goto_f

    :cond_f
    move-object/from16 v7, p4

    move-object/from16 v24, v1

    move-object/from16 p4, v4

    move-object/from16 v2, v17

    move-object/from16 v1, p0

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v4

    invoke-static {v6, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_15

    if-eqz v20, :cond_13

    if-eqz v15, :cond_13

    sget-object v4, Le0/a/b/a;->a:Ljava/util/List;

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v6

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v1

    array-length v1, v1

    move-object/from16 v17, v8

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v8, v1}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v1, v7

    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    long-to-int v4, v6

    invoke-static {v4}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v4

    invoke-static {v15, v4}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v13, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-static {v15}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object v6

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v6, v8, v7}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v6

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {v12}, Lx/m/c/j;->checkNotNull(Ljava/lang/Object;)V

    const/4 v8, 0x0

    move-object/from16 v31, v15

    const/4 v15, 0x0

    invoke-virtual {v7, v12, v8, v8, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    if-ne v9, v5, :cond_10

    int-to-float v8, v14

    int-to-float v15, v0

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v32, v5

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float v28, v8, v5

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float v29, v15, v5

    invoke-virtual/range {p0 .. p0}, Le0/a/b/a$b;->c()Landroid/graphics/Paint;

    move-result-object v30

    move-object/from16 v25, v7

    move/from16 v26, v8

    move/from16 v27, v15

    invoke-virtual/range {v25 .. v30}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_6

    :cond_10
    move-object/from16 v32, v5

    :goto_6
    int-to-float v5, v14

    int-to-float v8, v0

    const/4 v15, 0x0

    invoke-virtual {v7, v6, v5, v8, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    invoke-static {v4, v10}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v10

    if-eq v10, v2, :cond_11

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v10

    invoke-virtual {v4, v2, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v10

    goto :goto_7

    :cond_11
    move-object v10, v4

    :goto_7
    invoke-direct {v7, v15, v10}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    div-float v10, p5, v16

    float-to-int v10, v10

    invoke-virtual {v1, v7, v10}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    const/4 v10, 0x1

    if-eq v7, v10, :cond_12

    const/4 v5, 0x2

    if-eq v7, v5, :cond_14

    move-object/from16 v5, p0

    move-object v6, v5

    move-object/from16 v10, p3

    move/from16 v8, p5

    move-object v12, v4

    move-object/from16 v7, v17

    move-object/from16 v4, v24

    move-object/from16 v15, v31

    move/from16 v17, v0

    move-object/from16 v0, p2

    goto/16 :goto_e

    :cond_12
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v13, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v12, 0x0

    const/4 v15, 0x0

    invoke-virtual {v10, v4, v15, v15, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-static {v6, v11}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    add-float v36, v5, v4

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    add-float v37, v8, v4

    invoke-virtual/range {p0 .. p0}, Le0/a/b/a$b;->c()Landroid/graphics/Paint;

    move-result-object v38

    move-object/from16 v33, v10

    move/from16 v34, v5

    move/from16 v35, v8

    invoke-virtual/range {v33 .. v38}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const/4 v4, -0x1

    move-object/from16 v5, p0

    move-object v15, v5

    move-object/from16 v10, p3

    move/from16 v8, p5

    move-object v12, v7

    move-object/from16 v7, v17

    move/from16 v6, v18

    move-object/from16 v4, v24

    const/4 v11, -0x1

    move/from16 v17, v0

    goto/16 :goto_9

    :cond_13
    move-object/from16 v32, v5

    move-object v1, v7

    move-object/from16 v17, v8

    move-object/from16 v31, v15

    if-eqz v24, :cond_14

    sget-object v0, Le0/a/b/a;->a:Ljava/util/List;

    move-object/from16 v4, v24

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v1

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v2

    array-length v2, v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-static {}, Le0/a/c/c;->b()[B

    move-result-object v1

    invoke-static {v1}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    long-to-int v1, v0

    invoke-static {v1}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v0

    invoke-static {v4, v0}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedInputStream;->close()V

    new-instance v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v4}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object v1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v1, v3, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v9, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto/16 :goto_13

    :cond_14
    move-object/from16 v4, v24

    move-object/from16 v5, p0

    move-object/from16 v7, v17

    move-object/from16 v15, v31

    move/from16 v17, v0

    goto/16 :goto_a

    :cond_15
    move-object/from16 v32, v5

    move-object v1, v7

    move-object/from16 v17, v8

    move-object/from16 v31, v15

    move-object/from16 v4, v24

    invoke-static {}, Le0/a/c/c;->a()[B

    move-result-object v5

    invoke-static {v6, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-eqz v5, :cond_18

    if-nez v31, :cond_17

    if-nez v4, :cond_16

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Le0/a/c/c;->c()[B

    move-result-object v5

    invoke-static {v5}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v5, p0

    move-object/from16 v7, v17

    invoke-virtual {v5, v7, v3, v13}, Le0/a/b/a$b;->b(Le0/a/a/a;II)[B

    move-result-object v6

    invoke-static {v6}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_8

    :cond_16
    move-object/from16 v5, p0

    move-object/from16 v7, v17

    :goto_8
    const/4 v6, 0x4

    const/4 v8, 0x0

    move-object/from16 v10, p4

    invoke-static {v10, v8, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v11

    invoke-static {v11}, Le0/a/c/c;->d([B)I

    move-result v11

    invoke-static {v10, v8, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v6

    invoke-static {v6}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Le0/a/c/c;->a()[B

    move-result-object v8

    invoke-static {v8}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/16 v8, 0x8

    add-int/2addr v11, v8

    invoke-static {v10, v8, v11}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v8

    invoke-static {v8}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v8, Ljava/util/zip/CRC32;

    invoke-direct {v8}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {v6}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object v10

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v15, 0x0

    invoke-virtual {v8, v10, v15, v11}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v8}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v10

    long-to-int v6, v10

    invoke-static {v6}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v6

    invoke-static {v4, v6}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    const/4 v6, -0x1

    move-object/from16 v10, p3

    move/from16 v8, p5

    move/from16 v17, v0

    move-object v15, v5

    move/from16 v6, v18

    const/4 v11, -0x1

    :goto_9
    move-object/from16 v0, p2

    goto/16 :goto_10

    :cond_17
    move-object/from16 v5, p0

    move-object/from16 v10, p4

    move-object/from16 v7, v17

    const/4 v6, 0x4

    const/4 v8, 0x0

    invoke-static {v10, v8, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v11

    invoke-static {v11}, Le0/a/c/c;->d([B)I

    move-result v11

    invoke-static {v10, v8, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v6

    invoke-static {v6}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v6

    move-object/from16 v15, v31

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Le0/a/c/c;->a()[B

    move-result-object v8

    invoke-static {v8}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/16 v8, 0x8

    add-int/2addr v11, v8

    invoke-static {v10, v8, v11}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v8

    invoke-static {v8}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v8, Ljava/util/zip/CRC32;

    invoke-direct {v8}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {v6}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object v10

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    move/from16 v17, v0

    const/4 v0, 0x0

    invoke-virtual {v8, v10, v0, v11}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v8}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v10

    long-to-int v0, v10

    invoke-static {v0}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v0

    invoke-static {v15, v0}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto/16 :goto_a

    :cond_18
    move-object/from16 v5, p0

    move-object/from16 v10, p4

    move-object/from16 v7, v17

    move-object/from16 v15, v31

    move/from16 v17, v0

    sget-object v0, Le0/a/c/c;->e:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v6, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x0

    const/4 v6, 0x4

    invoke-static {v10, v0, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Le0/a/c/c;->d([B)I

    move-result v0

    if-eqz v15, :cond_19

    add-int/lit8 v6, v0, -0x4

    invoke-static {v6}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v6

    invoke-static {v6}, Lx/h/f;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_19
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Le0/a/c/c;->a()[B

    move-result-object v8

    invoke-static {v8}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v0, 0x8

    const/16 v8, 0xc

    invoke-static {v10, v8, v0}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v0

    invoke-static {v0}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {v6}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object v8

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v0, v8, v11, v10}, Ljava/util/zip/CRC32;->update([BII)V

    if-eqz v15, :cond_1a

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1a
    if-eqz v15, :cond_1d

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v10

    long-to-int v0, v10

    invoke-static {v0}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v0

    invoke-static {v15, v0}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_a

    :cond_1b
    sget-object v0, Le0/a/c/c;->f:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v6, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1c

    move-object/from16 v21, v10

    goto :goto_a

    :cond_1c
    sget-object v0, Le0/a/c/c;->g:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v6, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1e

    move-object/from16 v22, v10

    :cond_1d
    :goto_a
    move-object/from16 v0, p2

    goto/16 :goto_c

    :cond_1e
    sget-object v0, Le0/a/c/c;->h:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v6, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_21

    move-object/from16 v0, p2

    invoke-static {v10, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v3, v10

    const/4 v6, 0x0

    :goto_b
    if-ge v6, v3, :cond_20

    aget-byte v8, v10, v6

    const/16 v11, 0x49

    int-to-byte v11, v11

    if-ne v8, v11, :cond_1f

    add-int/lit8 v8, v6, 0x1

    aget-byte v8, v10, v8

    const/16 v11, 0x48

    int-to-byte v11, v11

    if-ne v8, v11, :cond_1f

    add-int/lit8 v8, v6, 0x2

    aget-byte v8, v10, v8

    const/16 v11, 0x44

    int-to-byte v11, v11

    if-ne v8, v11, :cond_1f

    add-int/lit8 v8, v6, 0x3

    aget-byte v8, v10, v8

    const/16 v11, 0x52

    int-to-byte v11, v11

    if-ne v8, v11, :cond_1f

    sget-object v8, Le0/a/c/c;->j:Le0/a/c/c;

    add-int/lit8 v8, v6, -0x4

    invoke-static {v10, v8, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v8

    invoke-static {v8}, Le0/a/c/c;->d([B)I

    move-result v8

    add-int/lit8 v11, v6, 0x4

    add-int/lit8 v12, v6, 0x8

    invoke-static {v10, v11, v12}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v13

    invoke-static {v13}, Le0/a/c/c;->d([B)I

    move-result v13

    iput v13, v7, Le0/a/a/a;->b:I

    add-int/lit8 v13, v6, 0xc

    invoke-static {v10, v12, v13}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v12

    invoke-static {v12}, Le0/a/c/c;->d([B)I

    move-result v12

    iput v12, v7, Le0/a/a/a;->c:I

    add-int/2addr v8, v6

    add-int/lit8 v8, v8, 0x4

    invoke-static {v10, v11, v8}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v8

    const-string v11, "<set-?>"

    invoke-static {v8, v11}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v8, v7, Le0/a/a/a;->a:[B

    :cond_1f
    add-int/lit8 v6, v6, 0x1

    goto :goto_b

    :cond_20
    iget v3, v7, Le0/a/a/a;->b:I

    iget v6, v7, Le0/a/a/a;->c:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v6, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    move v13, v6

    goto :goto_c

    :cond_21
    move-object/from16 v0, p2

    sget-object v8, Le0/a/c/c;->i:Lkotlin/Lazy;

    invoke-interface {v8}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    invoke-static {v6, v8}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-eqz v6, :cond_22

    const/16 v20, 0x1

    :cond_22
    :goto_c
    move-object/from16 v10, p3

    move/from16 v8, p5

    move v6, v3

    move-object v3, v5

    :goto_d
    move/from16 v39, v6

    move-object v6, v3

    move/from16 v3, v39

    :goto_e
    const/4 v11, -0x1

    move-object/from16 v31, v15

    move-object v15, v6

    :goto_f
    move/from16 v6, v18

    :goto_10
    if-ne v6, v11, :cond_23

    :goto_11
    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedInputStream;->close()V

    move-object v9, v1

    goto/16 :goto_13

    :cond_23
    const/4 v11, -0x1

    move-object/from16 p2, v0

    move-object/from16 p4, v1

    move-object v1, v4

    move/from16 p5, v8

    move-object/from16 p3, v10

    move/from16 v0, v17

    move-object/from16 v10, v19

    move-object/from16 v6, v23

    move-object/from16 v5, v32

    move-object v4, v2

    move-object v8, v7

    move v7, v14

    move-object v2, v15

    move-object/from16 v15, v31

    goto/16 :goto_2

    :cond_24
    new-instance v0, Loupson/apng/exceptions/BadCRC;

    invoke-direct {v0}, Loupson/apng/exceptions/BadCRC;-><init>()V

    throw v0

    :cond_25
    move-object/from16 v23, v6

    invoke-virtual/range {v23 .. v23}, Ljava/io/BufferedInputStream;->reset()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_26

    const-string v0, "$this$readBytes"

    move-object/from16 v1, v23

    invoke-static {v1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v2

    const/16 v3, 0x2000

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {v1, v0, v3}, Lf/h/a/f/f/n/g;->copyTo(Ljava/io/InputStream;Ljava/io/OutputStream;I)J

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v2, "buffer.toByteArray()"

    invoke-static {v0, v2}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/ImageDecoder;->createSource(Ljava/nio/ByteBuffer;)Landroid/graphics/ImageDecoder$Source;

    move-result-object v0

    const-string v1, "ImageDecoder.createSource(buf)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/graphics/ImageDecoder;->decodeDrawable(Landroid/graphics/ImageDecoder$Source;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string v1, "ImageDecoder.decodeDrawable(source)"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_12

    :cond_26
    move-object/from16 v1, v23

    const/4 v0, 0x0

    invoke-static {v1, v0}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    const-string v1, "drawable"

    invoke-static {v0, v1}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_12
    move-object v9, v0

    :goto_13
    return-object v9
.end method


# virtual methods
.method public final b(Le0/a/a/a;II)[B
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Le0/a/c/c;->j:Le0/a/c/c;

    iget-object v2, p1, Le0/a/a/a;->a:[B

    array-length v2, v2

    invoke-static {v2}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object v2

    invoke-static {v0, v2}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Byte;

    const/16 v3, 0x49

    int-to-byte v3, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/16 v3, 0x48

    int-to-byte v3, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    const/4 v5, 0x1

    aput-object v3, v2, v5

    const/16 v3, 0x44

    int-to-byte v3, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    const/4 v5, 0x2

    aput-object v3, v2, v5

    const/16 v3, 0x52

    int-to-byte v3, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    const/4 v5, 0x3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-static {p2}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object p2

    invoke-static {v1, p2}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-static {p3}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object p2

    invoke-static {v1, p2}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iget-object p1, p1, Le0/a/a/a;->a:[B

    const/16 p2, 0x8

    const/16 p3, 0xd

    invoke-static {p1, p2, p3}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object p1

    invoke-static {p1}, Lx/h/f;->asList([B)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance p1, Ljava/util/zip/CRC32;

    invoke-direct {p1}, Ljava/util/zip/CRC32;-><init>()V

    invoke-static {v1}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object p2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p3

    invoke-virtual {p1, p2, v4, p3}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide p1

    long-to-int p2, p1

    invoke-static {p2}, Le0/a/c/c;->e(I)[Ljava/lang/Byte;

    move-result-object p1

    invoke-static {v0, p1}, Lx/h/f;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    invoke-static {v0}, Lx/h/f;->toByteArray(Ljava/util/Collection;)[B

    move-result-object p1

    return-object p1
.end method

.method public final c()Landroid/graphics/Paint;
    .locals 2

    sget-object v0, Le0/a/b/a;->b:Lkotlin/Lazy;

    sget-object v1, Le0/a/b/a;->c:Le0/a/b/a$b;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method
