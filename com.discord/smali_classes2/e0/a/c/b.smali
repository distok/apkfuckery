.class public final enum Le0/a/c/b;
.super Ljava/lang/Enum;
.source "Utils.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Le0/a/c/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum d:Le0/a/c/b;

.field public static final enum e:Le0/a/c/b;

.field public static final enum f:Le0/a/c/b;

.field public static final synthetic g:[Le0/a/c/b;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Le0/a/c/b;

    new-instance v1, Le0/a/c/b;

    const-string v2, "APNG_DISPOSE_OP_NONE"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Le0/a/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Le0/a/c/b;->d:Le0/a/c/b;

    aput-object v1, v0, v3

    new-instance v1, Le0/a/c/b;

    const-string v2, "APNG_DISPOSE_OP_BACKGROUND"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Le0/a/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Le0/a/c/b;->e:Le0/a/c/b;

    aput-object v1, v0, v3

    new-instance v1, Le0/a/c/b;

    const-string v2, "APNG_DISPOSE_OP_PREVIOUS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Le0/a/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Le0/a/c/b;->f:Le0/a/c/b;

    aput-object v1, v0, v3

    sput-object v0, Le0/a/c/b;->g:[Le0/a/c/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Le0/a/c/b;
    .locals 1

    const-class v0, Le0/a/c/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Le0/a/c/b;

    return-object p0
.end method

.method public static values()[Le0/a/c/b;
    .locals 1

    sget-object v0, Le0/a/c/b;->g:[Le0/a/c/b;

    invoke-virtual {v0}, [Le0/a/c/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Le0/a/c/b;

    return-object v0
.end method
