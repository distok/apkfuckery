.class public final Le0/a/a/b;
.super Ljava/lang/Object;
.source "fcTL.kt"


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:I

.field public e:I

.field public f:Le0/a/c/a;

.field public g:Le0/a/c/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Le0/a/a/b;->a:I

    iput v0, p0, Le0/a/a/b;->b:I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Le0/a/a/b;->c:F

    sget-object v0, Le0/a/c/a;->d:Le0/a/c/a;

    iput-object v0, p0, Le0/a/a/b;->f:Le0/a/c/a;

    sget-object v0, Le0/a/c/b;->d:Le0/a/c/b;

    iput-object v0, p0, Le0/a/a/b;->g:Le0/a/c/b;

    return-void
.end method


# virtual methods
.method public a([B)V
    .locals 11

    const-string v0, "byteArray"

    invoke-static {p1, v0}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    aget-byte v1, p1, v0

    const/16 v2, 0x66

    int-to-byte v2, v2

    if-ne v1, v2, :cond_6

    const/4 v1, 0x5

    aget-byte v1, p1, v1

    const/16 v2, 0x63

    int-to-byte v2, v2

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    aget-byte v1, p1, v1

    const/16 v2, 0x54

    int-to-byte v2, v2

    if-ne v1, v2, :cond_6

    const/4 v1, 0x7

    aget-byte v1, p1, v1

    const/16 v2, 0x4c

    int-to-byte v2, v2

    if-ne v1, v2, :cond_6

    sget-object v1, Le0/a/c/c;->j:Le0/a/c/c;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v1, v2}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v3

    invoke-static {v3}, Le0/a/c/c;->d([B)I

    move-result v3

    const/16 v4, 0xc

    const/16 v5, 0x10

    invoke-static {p1, v4, v5}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v4

    invoke-static {v4}, Le0/a/c/c;->d([B)I

    move-result v4

    iput v4, p0, Le0/a/a/b;->a:I

    const/16 v4, 0x14

    invoke-static {p1, v5, v4}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v6

    invoke-static {v6}, Le0/a/c/c;->d([B)I

    move-result v6

    iput v6, p0, Le0/a/a/b;->b:I

    const/16 v6, 0x1c

    const/16 v7, 0x1e

    invoke-static {p1, v6, v7}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v8

    invoke-static {v8}, Le0/a/c/c;->d([B)I

    move-result v8

    int-to-float v8, v8

    const/16 v9, 0x20

    invoke-static {p1, v7, v9}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v7

    invoke-static {v7}, Le0/a/c/c;->d([B)I

    move-result v7

    int-to-float v7, v7

    const/4 v10, 0x0

    cmpg-float v10, v7, v10

    if-nez v10, :cond_0

    const/high16 v7, 0x42c80000    # 100.0f

    :cond_0
    div-float/2addr v8, v7

    const/16 v7, 0x3e8

    int-to-float v7, v7

    mul-float v8, v8, v7

    iput v8, p0, Le0/a/a/b;->c:F

    const/16 v7, 0x18

    invoke-static {p1, v4, v7}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v4

    invoke-static {v4}, Le0/a/c/c;->d([B)I

    move-result v4

    iput v4, p0, Le0/a/a/b;->d:I

    invoke-static {p1, v7, v6}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v4

    invoke-static {v4}, Le0/a/c/c;->d([B)I

    move-result v4

    iput v4, p0, Le0/a/a/b;->e:I

    const/16 v4, 0x8

    add-int/2addr v3, v0

    add-int/2addr v3, v0

    invoke-static {p1, v4, v3}, Lx/h/f;->copyOfRange([BII)[B

    move-result-object v0

    const-string v3, "<set-?>"

    invoke-static {v0, v3}, Lx/m/c/j;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v0, v2, [Ljava/lang/Object;

    const/16 v3, 0x21

    aget-byte v3, p1, v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v3, "%02x"

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "java.lang.String.format(format, *args)"

    invoke-static {v0, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5}, Lf/h/a/f/f/n/g;->checkRadix(I)I

    invoke-static {v0, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v6

    long-to-int v0, v6

    sget-object v6, Le0/a/c/a;->d:Le0/a/c/a;

    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_1

    goto :goto_0

    :cond_1
    sget-object v6, Le0/a/c/a;->e:Le0/a/c/a;

    :cond_2
    :goto_0
    iput-object v6, p0, Le0/a/a/b;->f:Le0/a/c/a;

    new-array v0, v2, [Ljava/lang/Object;

    aget-byte p1, p1, v9

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    invoke-static {v3, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v4}, Lx/m/c/j;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5}, Lf/h/a/f/f/n/g;->checkRadix(I)I

    invoke-static {p1, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    long-to-int p1, v0

    sget-object v0, Le0/a/c/b;->d:Le0/a/c/b;

    if-eqz p1, :cond_5

    if-eq p1, v2, :cond_4

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    goto :goto_1

    :cond_3
    sget-object v0, Le0/a/c/b;->f:Le0/a/c/b;

    goto :goto_1

    :cond_4
    sget-object v0, Le0/a/c/b;->e:Le0/a/c/b;

    :cond_5
    :goto_1
    iput-object v0, p0, Le0/a/a/b;->g:Le0/a/c/b;

    :cond_6
    return-void
.end method
