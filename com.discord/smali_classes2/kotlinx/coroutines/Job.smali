.class public interface abstract Lkotlinx/coroutines/Job;
.super Ljava/lang/Object;
.source "Job.kt"

# interfaces
.implements Lkotlin/coroutines/CoroutineContext$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlinx/coroutines/Job$a;
    }
.end annotation


# static fields
.field public static final c:Lkotlinx/coroutines/Job$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lkotlinx/coroutines/Job$a;->a:Lkotlinx/coroutines/Job$a;

    sput-object v0, Lkotlinx/coroutines/Job;->c:Lkotlinx/coroutines/Job$a;

    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public abstract d(ZZLkotlin/jvm/functions/Function1;)Ly/a/i0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;)",
            "Ly/a/i0;"
        }
    .end annotation
.end method

.method public abstract e()Ljava/util/concurrent/CancellationException;
.end method

.method public abstract n(Ljava/util/concurrent/CancellationException;)V
.end method

.method public abstract q(Ly/a/l;)Ly/a/j;
.end method

.method public abstract start()Z
.end method
